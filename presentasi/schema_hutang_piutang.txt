# index
  status data :
  1. hutang / piutang baru
  2. hutang / piutang setengah pembayaran
  2. hutang / piutang selesai

  isi data di table :
  1. nama sdm
  2. tgl transaksi
  3. tgl tenggang waktu
  4. deskripsi
  5. jumlah
  6. terbayar


# form create piutang / kasbon
  - step :
  1. cari nama sdm, jika belum ada maka create baru
  2. menampilkan data hutang perusahaan pada sdm tersebut (jika ada)
  3. cairkan / bayarkan hutang perusahaan terlebih dahulu (jika ada)
  4. input kasbon

# form create hutang perusahaan
  - step :
  1. cari nama sdm, jika belum ada maka create baru
  2. menampilkan data piutang perusahaan pada sdm tersebut (jika ada)
  3. pelunasan piutang perusahaan terlebih dahulu (jika ada)
  4. input hutang



