ALTER TABLE `prop_ref_unit`
ADD `jenis_rumah` ENUM('SUBSIDI','NONSUBSIDI') NOT NULL DEFAULT 'SUBSIDI' AFTER `marketing_fee`,
ADD `nop` VARCHAR(100) NULL AFTER `jenis_rumah`;

select a.account_name, SUM(c.finance_item_amount) as jml_operasional
from bmm_app.fin_ref_account_accounting as a
left join bmm_app.fin_ref_account as b on a.account_id = b.account_accounting_id
left join bmm_app.fin_trnsct_item as c on b.account_id = c.finance_item_account_id
left join bmm_app.fin_trnsct as d on c.finance_id = d.finance_id
where d.finance_cancel = '0' and d.finance_date between '2021-01-01' and '2022-12-30'
group by c.finance_item_account_id
