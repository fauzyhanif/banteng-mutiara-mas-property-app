<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Benteng Mutiara Mas') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

    @include('system.gate.css')
    @include('system.gate.cssResponsive')
</head>
<body>
    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card main-card">
                            <div class="card-header text-center">
                                <div class="company-name">
                                    <h1 class="font-weight-bold">Benteng Mutiara Mas</h1>
                                </div>
                                {{-- <img src="{{ url('/public/img/logo_km_3.png') }}" class="logo" alt="logo"> --}}
                            </div>

                            <div class="card-body">
                                <form method="POST" action="{{ route('login.doauth') }}">
                                    @csrf

                                    <div class="form-group">
                                        <label for="email" class="col-form-label">{{ __('Email / No Handphone') }}</label>

                                        <input id="email" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="password" class="col-form-label">{{ __('Password') }}</label>

                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            {{ __('LOGIN') }}
                                        </button>
                                        <a href="{{ url('/booking_online') }}" class="btn btn-secondary btn-block">
                                            BOOKING ONLINE
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
