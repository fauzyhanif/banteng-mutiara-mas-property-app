<div class="form-group">
    <label>Nama Akun <span class="text-red">*</span></label>
    <input
        type="text"
        name="account_name"
        class="form-control"
        required
        @isset($account)
            value="{{ $account->account_name }}"
        @endisset
    >
</div>
