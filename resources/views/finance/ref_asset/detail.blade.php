@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.asset') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Aset
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Aset</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title-m-0">Detail Aset</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td class="text-secondary" width="25%">Nama Aset</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ $asset->asset_name }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Nomor Aset</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ $asset->asset_num }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Deskripsi</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ $asset->asset_desc }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Tanggal Beli</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ GeneralHelper::konversiTgl($asset->asset_purchase_date, 'ttd') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Masa Pemakaian</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ $asset->asset_usage_time }} Tahun</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Harga Beli</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ GeneralHelper::rupiah($asset->asset_price) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Jumlah Penyusutan</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ GeneralHelper::rupiah($asset->asset_price - $asset->asset_book_value) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Nilai buku saat ini</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ GeneralHelper::rupiah($asset->asset_book_value) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="25%">Penyusutan Terakhir</td>
                                    <td class="text-right" width="5%">:</td>
                                    <td width="70%">{{ GeneralHelper::konversiTgl($asset->asset_last_depreciation, 'ttd') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if ($asset->depreciations->count() > 0)
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title-m-0">Data Penyusutan</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-hover table-sm">
                                <thead class="bg-info">
                                    <th>Periode</th>
                                    <th class="text-right">Jumlah Penyusutan</th>
                                </thead>
                                <tbody>
                                    @foreach ($asset->depreciations as $item)
                                        <tr>
                                            <td>{{ GeneralHelper::konversiTgl($item->depreciation_start_date, 'ttd') }} - {{ GeneralHelper::konversiTgl($item->depreciation_end_date, 'ttd') }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($item->depreciation_amount) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>

@include('finance.ref_asset.asset.js')
@endsection
