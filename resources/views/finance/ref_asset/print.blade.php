<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Laporan Akuntansi - Aset Perusahaan</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
        <style>
            @page {
                size: A4 landscape
            }
        </style>
    </head>


    <body class="A4 landscape" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">

                {{-- heading --}}
                <div class="row">
                    <div class="col-12">
                        <p class="font-weight-bold my-0">DATA ASET DAN PERHITUNGAN PENYUSUTAN/AMORTASI</p>
                        <p class="font-weight-bold my-0">PT. LAN SENA JAYA</p>
                        <p class="font-weight-bold my-0">NPWP : 76.273.251.9-409.000</p>
                        <p class="font-weight-bold my-0">PER 31 DESEMBER {{ date('Y') }}</p>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-12">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">NO</th>
                                    <th class="text-center" rowspan="2">NAMA HARTA</th>
                                    <th width="25%" class="text-center" colspan="2">PEROLEHAN</th>
                                    <th width="10%" class="text-center" rowspan="2">MASA MANFAAT</th>
                                    <th width="15%" class="text-center" rowspan="2">JUMLAH PENYUSUTAN</th>
                                    <th width="15%" class="text-center" rowspan="2">NILAI BUKU</th>
                                </tr>
                                <tr>
                                    <th class="text-center">TAHUN</th>
                                    <th class="text-center">NILAI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                    $total_nilai = 0;
                                    $total_penyusutan = 0;
                                    $total_nilai_buku = 0;
                                @endphp
                                @foreach ($assets as $asset)
                                    @php
                                        $nilai_penyusutan = 100 / $asset->asset_usage_time;
                                        $angka_penyusutan = ($asset->asset_price * $nilai_penyusutan) / 100;
                                        $thn_beli = (int) substr($asset->asset_purchase_date,0,4);
                                        $thn_beli = $thn_beli + $asset->asset_usage_time;
                                        $thn_skrg = (int) date('Y');
                                        if($thn_beli >= $thn_skrg) {
                                            $angka_penyusutan = ($asset->asset_price * $nilai_penyusutan) / 100;
                                        } else {
                                            $angka_penyusutan = 0;
                                        }
                                    @endphp
                                    <tr>
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ $asset->asset_name }}</td>
                                        <td class="text-center">{{ substr($asset->asset_purchase_date,0,4) }}</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($asset->asset_price) }}</td>
                                        <td class="text-center">{{ $asset->asset_usage_time }} Th</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($angka_penyusutan) }}</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($asset->asset_book_value) }}</td>
                                    </tr>
                                @php
                                    $no += 1;
                                    $total_nilai += $asset->asset_price;
                                    $total_penyusutan += $angka_penyusutan;
                                    $total_nilai_buku += $asset->asset_book_value;
                                @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th></th>
                                <th>JUMLAH</th>
                                <th></th>
                                <th class="text-right">{{ GeneralHelper::rupiah($total_nilai) }}</th>
                                <th></th>
                                <th class="text-right">{{ GeneralHelper::rupiah($total_penyusutan) }}</th>
                                <th class="text-right">{{ GeneralHelper::rupiah($total_nilai_buku) }}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-4"></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        Purwakarta, 31 Desember {{ date('Y') }}

                        <br>
                        <br>
                        <br>
                        <br>

                        <u>Alan Suharlan</u> <br>
                        Direktur
                    </div>
                </div>
            </div>
        </section>
    </body>

</html>
