@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.asset') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Aset
            </a>
        </li>
        <li class="breadcrumb-item active">Penyusutan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Form Penyusutan</h5>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ url('finance/asset/penyusutan') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="periode" value="{{ $this_year }}">

                                    <table class="table table-bordered table-sm table-hover">
                                        <thead class="bg-info">
                                            <th class="text-center" width="5%"><input type="checkbox" id="check-all"></th>
                                            <th>Nama aset</th>
                                            <th>Nomor</th>
                                            <th>Periode</th>
                                            <th width="20%">Nilai penyusutan</th>
                                            <th class="text-right" width="15%">Jml penyusutan</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($assets as $asset)
                                                @php
                                                    $date1 = $asset->asset_last_depreciation;
                                                    $date2 = $this_year;
                                                    $diff = abs(strtotime($date2) - strtotime($date1));
                                                    $years = floor($diff / (365*60*60*24));
                                                    $nilai_penyusutan = 100 / $asset->asset_usage_time;
                                                    $angka_penyusutan = ($asset->asset_price * $nilai_penyusutan / 100) * $years;
                                                @endphp
                                                <tr>
                                                    <input type="hidden" name="asset_name[{{ $asset->asset_id }}]" value="{{ $asset->asset_name }}">
                                                    <input type="hidden" name="depreciation_amount[{{ $asset->asset_id }}]" value="{{ $angka_penyusutan }}">
                                                    <input type="hidden" name="depreciation_start_date[{{ $asset->asset_id }}]" value="{{ $asset->asset_last_depreciation }}">
                                                    <input type="hidden" name="depreciation_end_date[{{ $asset->asset_id }}]" value="{{ $this_year }}">

                                                    <td class="text-center"><input type="checkbox" name="asset_id[]" value="{{ $asset->asset_id }}" id="check-all"></td>
                                                    <td>{{ $asset->asset_name }}</td>
                                                    <td>{{ $asset->asset_num }}</td>
                                                    <td>{{ date("d/m/Y", strtotime($asset->asset_last_depreciation)) }} - {{ date("d/m/Y", strtotime($this_year)) }}</td>
                                                    <td>{{ $nilai_penyusutan }}%</td>
                                                    <td class="text-right">{{ GeneralHelper::rupiah($angka_penyusutan) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    <div class="my-3">
                                        <span class="text-red">*</span>) Silahkan ceklis aset yang akan dijalankan penyusutan
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fas fa-play"></i> &nbsp;
                                        Jalankan Penyusutan
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.ref_asset.asset.js')
@endsection
