<div class="form-group">
    <label>Nomor Aset</label>
    <input type="text" name="asset_num" class="form-control" required @isset($asset) value="{{ $asset->asset_num }}" @endisset>
</div>

<div class="form-group">
    <label>Nama Aset <span class="text-red">*</span></label>
    <input type="text" name="asset_name" class="form-control" required @isset($asset) value="{{ $asset->asset_name }}" @endisset>
</div>

<div class="form-group">
    <label>Keterangan</label>
    <input type="text" name="asset_desc" class="form-control" @isset($asset) value="{{ $asset->asset_desc }}" @endisset>
</div>

<div class="form-group">
    <label>Harga Beli <span class="text-red">*</span></label>
    <input type="text" name="asset_price" class="form-control money" required @isset($asset) value="{{ $asset->asset_price }}"
        @endisset>
</div>

@if (!isset($asset))
    <div class="form-group">
        <label>Nilai Buku Terakhir <span class="text-red">*</span></label>
        <input type="text" name="asset_book_value" class="form-control money" required>
    </div>
@endif

<div class="form-group">
    <label>Tanggal Beli <span class="text-red">*</span></label>
    <input type="date" name="asset_purchase_date" class="form-control" required @isset($asset) value="{{ $asset->asset_purchase_date }}" @endisset>
</div>

@if (!isset($asset))
    <div class="form-group">
        <label>Tanggal penyusutan terakhir <span class="text-red">*</span></label>
        <input type="date" name="asset_last_depreciation" class="form-control" required>
    </div>
@endif

<div class="form-group">
    <label>Ada penyusutan?</label>
    <select name="asset_depreciation" id="" class="form-control">
        <option value="Y" @isset($asset) {{ ($asset->asset_depreciation == 'Y') ? 'selected' : '' }} @endisset>Ada</option>
        <option value="N" @isset($asset) {{ ($asset->asset_depreciation == 'N') ? 'selected' : '' }} @endisset>Tidak Ada</option>
    </select>
</div>

<div class="form-group">
    <label>Masa Manfaat (dalam tahun)</label>
    <input type="number" name="asset_usage_time" class="form-control" required @isset($asset) value="{{ $asset->asset_usage_time }}" @endisset>
</div>
