<script>
$(function() {
    loadData();

    // check all
    $("#check-all").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update' && res.status == "success") {
                    form.trigger("reset");
                }

                if (res.status == "success") {
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('finance.asset.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'asset_num', name: 'asset_num', searchable: true },
            { data: 'asset_name', name: 'asset_name', searchable: true },
            { data: 'asset_desc', name: 'asset_desc', searchable: true },
            { data: 'asset_purchase_date_html', name: 'asset_purchase_date', searchable: true },
            { data: 'asset_price_html', name: 'asset_price_html', searchable: true },
            { data: 'asset_book_value_html', name: 'asset_book_value_html', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
