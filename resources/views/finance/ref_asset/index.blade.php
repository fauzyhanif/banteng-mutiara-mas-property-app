@extends('index')

@section('content')
<section class="content-header">
    <h1>Aset Perusahaan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Aset
                        <div class="card-tools">
                            <a href="{{ route('finance.asset.print') }}" target="_blank" class="btn btn-default btn-sm">
                                <i class="fas fa-print"></i> &nbsp;
                                Cetak
                            </a>
                            <a href="{{ route('finance.asset.form_penyusutan') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-play"></i> &nbsp;
                                Jalankan Penyusutan
                            </a>
                            <a href="{{ route('finance.asset.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Aset Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered datatable">
                            <thead class="bg-info">
                                <th>Nomor Aset</th>
                                <th>Nama Aset</th>
                                <th>Keterangan</th>
                                <th>Tanggal Beli</th>
                                <th>Harga Beli</th>
                                <th>Nilai Buku</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('finance.ref_asset.asset.js')
@endsection

