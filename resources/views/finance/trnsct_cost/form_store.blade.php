@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.cost') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Operasional
            </a>
        </li>
        <li class="breadcrumb-item active">Input Operasional Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <form name="cost_store" id="form-add" action="{{ route('finance.cost.store') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="add">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tanggal <span class="text-red">*</span></label>
                                        <input type="date" name="finance_date" class="form-control"
                                            value="{{ date('Y-m-d') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Penerima <span class="text-red">*</span></label>
                                        <select name="finance_sdm_id" class="form-control" id="select-sdm" placeholder="Cari nama orang" aria-label="Cari nama orang"
                                            aria-describedby="basic-addon2"></select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Deskripsi <span class="text-red">*</span></label>
                                        <textarea name="finance_desc" class="form-control" required></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <table class="table">
                                    <thead class="bg-info">
                                        <th width="5%">Hapus</th>
                                        <th>Akun Operasional</th>
                                        <th width="20%">Jumlah</th>
                                    </thead>
                                    <tbody class="form-wrapper">
                                        <tr class="form-content">
                                            <td class="text-center">
                                                <button type="button" class="btn btn-xs btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </td>
                                            <td>
                                                <select name="finance_item_account_id[]" class="form-control form-control-sm" required>
                                                    <option value="">-- Pilih Akun --</option>
                                                    @foreach ($accounts as $account)
                                                        <option value="{{ $account->account_id }}">{{ $account->account_name }}</option>
                                                    @endforeach %}
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="finance_item_amount[]" class="form-control form-control-sm amount money" value="0" required>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-primary btn-clone">
                                                    <i class="fas fa-plus"></i> Tambah Data
                                                </button>
                                            </td>
                                            <td width="30%" class="text-right">
                                                <h4 class="font-weight-bold">Total</h4>
                                            </td>
                                            <td width="20%">
                                                <input type="hidden" name="finance_amount">
                                                <input type="text" name="ttl_amount_text" class="form-control form-control-sm" disabled>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-right">
                                                <h5 class="font-weight-bold">Dibayar</h5>
                                            </td>
                                            <td width="20%">
                                                <input type="text" name="finance_amount_paid" class="form-control form-control-sm">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-right">
                                                <h5 class="font-weight-bold">Dibayar Via</h5>
                                            </td>
                                            <td width="20%">
                                                <select name="finance_paid_cash_id" id="" class="form-control form-control-sm">
                                                    @foreach ($cashes as $cash)
                                                        <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-body text-right" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success btn-save">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.trnsct_cost.asset.js')
@endsection
