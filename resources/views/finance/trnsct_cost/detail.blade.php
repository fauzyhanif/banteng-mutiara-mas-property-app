@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.cost') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Operasional
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Operasional</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Info Operasional</h3>

                        @if ($finance->finance_cancel == '0')
                            <div class="card-tools">
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-cancel-cost">
                                    Batalkan Operasional
                                </button>

                                <div class="modal" id="modal-cancel-cost">
                                    <div class="modal-dialog">
                                        <div class="modal-content">

                                            <form action="{{ url('finance/cost/cancel') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="finance_id" value="{{ $finance->finance_id }}">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Konfirmasi Pembatalan</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    Anda yakin ingin membatalkan transaksi Operasional ini?
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger">Batalkan Operasional</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2 font-weight-bold">Tanggal</div>
                            <div class="col-lg-10">: {{ GeneralHelper::konversiTgl($finance->finance_date) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 font-weight-bold">Deskripsi</div>
                            <div class="col-lg-10">: {{ $finance->finance_desc }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 font-weight-bold">Sejumlah</div>
                            <div class="col-lg-10">: Rp {{ GeneralHelper::rupiah($finance->finance_amount) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 font-weight-bold">Sudah dibayar</div>
                            <div class="col-lg-10">: Rp {{ GeneralHelper::rupiah($finance->finance_amount_paid) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 font-weight-bold">Status</div>
                            <div class="col-lg-10">: {{ ($finance->finance_cancel == '0') ? 'Berhasil' : 'Batal' }}</div>
                        </div>

                        @if ($finance->finance_amount > $finance->finance_amount_paid && $finance->finance_cancel == 0)
                            <hr>

                            <form action="{{ route('finance.cost.pay') }}" method="POST">
                                @csrf
                                <input type="hidden" name="finance_id" value="{{ $finance->finance_id }}">
                                <input type="hidden" name="finance_paid_sdm_id" value="{{ $finance->finance_sdm_id }}">

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Tanggal Bayar</label>
                                            <input type="date" name="finance_paid_date" class="form-control" required value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Jumlah Bayar</label>
                                            <input type="text" name="finance_paid_amount" class="form-control money" required value="{{ $finance->finance_amount - $finance->finance_amount_paid }}">
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Dibayar Via</label>
                                            <select name="finance_paid_cash_id" class="form-control" required>
                                                @foreach ($cashes as $cash)
                                                <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-sm btn-success">Bayar</button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item Operasional</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" style="width: 100%">
                            <thead class="bg-info">
                                <th width="5%">No</th>
                                <th>Keterangan</th>
                                <th class="text-right" width="15%">Jumlah</th>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                    $total = 0;
                                @endphp
                                @foreach ($finance_items as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>
                                            {{ $item->account->account_name }}
                                            @if ($item->finance_item_account_id != '3' && $item->finance_item_account_id != '8')
                                                &nbsp;
                                                <button class="btn btn-xs btn-outline-primary" data-toggle="modal" data-target="#form-update-item-{{ $item->finance_item_id }}">
                                                    <i class="fas fa-edit"></i> Ubah
                                                </button>
                                                @include('finance.trnsct_cost.form_update_item')
                                            @else
                                                <i class="fas fa-lock"></i>
                                            @endif    
                                        </td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($item->finance_item_amount) }}</td>
                                    </tr>
                                    @php
                                        $no += 1;
                                        $total += $item->finance_item_amount;
                                    @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="2" class="text-center">Total</th>
                                <th class="text-right">{{ GeneralHelper::rupiah($total) }}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sejarah Pembayaran</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" style="width: 100%">
                            <thead class="bg-info">
                                <th width="25%">Tanggal</th>
                                <th>No Kwitansi</th>
                                <th width="15%">Via</th>
                                <th class="text-right" width="15%">Jumlah</th>
                                <th class="text-center" width="20%">Aksi</th>
                            </thead>
                            <tbody>
                                @php
                                    $total_paid = 0;
                                @endphp
                                @foreach ($finance_paids as $item)
                                    <tr>
                                        <td>{{ GeneralHelper::konversiTgl($item->finance_paid_date) }}</td>
                                        <td>{{ $item->finance_paid_num }}</td>
                                        <td>{{ $item->cash_bank->cash_name }}</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($item->finance_paid_amount) }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-cancel-{{ $item->finance_paid_id }}">
                                                <i class="fas fa-trash"></i> Batalkan
                                            </button>

                                            <a href="{{ url('finance/cost/print_payment', $item->finance_paid_id) }}" class="btn btn-xs bg-purple" target="_blank">
                                                <i class="fas fa-print"></i> Cetak
                                            </a>

                                            <div class="modal" id="modal-cancel-{{ $item->finance_paid_id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">

                                                        <form action="{{ url('finance/cost/cancel_paid') }}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="finance_paid_id" value="{{ $item->finance_paid_id }}">
                                                            <input type="hidden" name="finance_paid_amont" value="{{ $item->finance_paid_amount }}">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Konfirmasi Pembatalan</h4>
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">
                                                                Anda yakin ingin membatalkan transaksi pembayaran ini?
                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-danger">Batalkan Transaksi</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                        $total_paid += $item->finance_paid_amount;
                                    @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="3" class="text-center">Total</th>
                                <th class="text-right">{{ GeneralHelper::rupiah($total_paid) }}</th>
                                <th></th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.trnsct_cost.asset.js')
@endsection
