<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice Operasional</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
    <style>
        @page {
            size: A5 landscape;
            font-family: "Times New Roman", Times, serif;
            letter-spacing: 1.5px;
        }
    </style>
</head>
<body>
    <body class="A5 landscape" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                <div class="row kop-surat">
                    <div class="col-md-5 col-print-5 text-right">
                        <img src="{{ url('public/img/logo_bmm.jpeg') }}" style="max-width: 50px; margin-top: -10px; margin-left: 70px"
                            alt="" class="logo-kop">
                    </div>
                    <div class="col-md-7 col-print-7">
                        <h4><b>PT LAN SENA JAYA</b></h4>
                        <p style="margin-top: -10px; font-size: 11px">DEVELOPER & CONTRACTOR</p>
                    </div>
                </div>

                <hr style="margin-top: -10px">

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold mb-4">KWITANSI</p>

                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td width="35%">No Kwitansi</td>
                                    <td class="font-weight-bold">: {{ $payment->finance_paid_num }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Tgl pembayaran</td>
                                    <td class="font-weight-bold">: {{ GeneralHelper::konversiTgl($payment->finance_paid_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Telah terima dari</td>
                                    <td class="font-weight-bold">: PT LAN SENA JAYA</td>
                                </tr>
                                <tr>
                                    <td width="35%">Uang sejumlah</td>
                                    <td class="font-weight-bold">: Rp {{ GeneralHelper::rupiah($payment->finance_paid_amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Terbilang</td>
                                    <td class="font-weight-bold">: {{ GeneralHelper::terbilang($payment->finance_paid_amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Keterangan</td>
                                    <td class="font-weight-bold">: {{ $payment->trnsct->finance_desc }}</td>
                                </tr>
                                @if ($payment->trnsct->is_marketing_fee == '1' && $payment->trnsct)
                                    <tr>
                                        <td width="35%">Nama Konsumen</td>
                                        <td class="font-weight-bold">: {{ $payment->trnsct->sales->customer->name }}</td>
                                    </tr>

                                    <tr>
                                        <td width="35%">Unit</td>
                                        <td class="font-weight-bold">: {{ $payment->trnsct->sales->block->name }} No {{ $payment->trnsct->sales->unit_id }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, {{ GeneralHelper::konversiTgl($payment->finance_paid_date, 'ttd') }}</div>

                    <div class="col-md-6 col-print-6 text-center mb-4"></div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Penerima,</div>

                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2"></div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $payment->sdm->name }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
