@extends('index')

@section('content')
<section class="content-header">
    <h1>Operasional</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-12">
                <a href="{{ url('/finance/cost?status=1') }}"
                    class="btn {{ ($status == 1) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Belum Dibayar
                </a>

                <a href="{{ url('/finance/cost?status=2') }}"
                    class="btn {{ ($status == 2) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Pembayaran Sebagian
                </a>

                <a href="{{ url('/finance/cost?status=3') }}"
                    class="btn {{ ($status == 3) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Sudah Lunas
                </a>

                <a href="{{ url('/finance/cost?status=4') }}"
                    class="btn {{ ($status == 4) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Dibatalkan
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Operasional
                        <div class="card-tools">
                            <a href="{{ route('finance.cost.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Input Operasional Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th>Tgl Transaksi</th>
                                <th>Terkait</th>
                                <th>Deskripsi</th>
                                <th width="20%">Jumlah</th>
                                <th width="20%">Sisa</th>
                                <th width="10%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.trnsct_cost.list_payments')

<script>
    $(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(url('/finance/cost/list_data?status='.$status)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'date', name: 'date', searchable: true },
            { data: 'sdm.name', name: 'sdm.name', searchable: true },
            { data: 'finance_desc', name: 'finance_desc', searchable: true },
            { data: 'jumlah', name: 'jumlah', searchable: true },
            { data: 'sisa', name: 'sisa', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function show_list_payment(finance_id, finance_date, finance_user, finance_desc, finance_amount, finance_amount_paid) {
    if(isNaN(finance_amount - finance_amount_paid)) {var finance_sisa = 0;}
    $('#modal-list-payments').modal('show');
    $('#modal-list-payments-finance-date').text(finance_date);
    $('#modal-list-payments-finance-user').text(finance_user);
    $('#modal-list-payments-finance-desc').text(finance_desc);
    $('#modal-list-payments-finance-amount').text(finance_amount);
    $('#modal-list-payments-finance-amount-paid').text(finance_amount_paid);
    $('#modal-list-payments-finance-amount-sisa').text(finance_sisa);


    var url = "{{ url('finance/cost/list_payments') }}"+"/"+finance_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#view-list-payments').html(res);
            }
        });
}
</script>
@endsection

