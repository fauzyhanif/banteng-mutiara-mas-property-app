<div class="modal" id="form-update-item-{{ $item->finance_item_id }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="{{ url('finance/cost/update_item') }}" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{ $item->finance_item_id }}">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Form Update Item Operasional</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label for="">Akun Operasional</label>
            <select name="finance_item_account_id" id="" class="form-control" required>
              <option value="">-- Pilih Akun Operasional --</option>
              @foreach ($accounts as $account)
                <option value="{{ $account->account_id }}" {{ ($account->account_id == $item->finance_item_account_id) ? 'selected' : '' }}>{{ $account->account_name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Simpan Perubahan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>

    </div>
  </div>
</div>