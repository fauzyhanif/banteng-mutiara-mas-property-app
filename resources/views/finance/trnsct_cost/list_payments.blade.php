<div class="modal" id="modal-list-payments">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sejarah Pembayaran</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td width="25%">Tgl transaksi</td>
                            <td id="modal-list-payments-finance-date"></td>
                        </tr>
                        <tr>
                            <td width="25%">Terkait</td>
                            <td id="modal-list-payments-finance-user"></td>
                        </tr>
                        <tr>
                            <td width="25%">Deskripsi</td>
                            <td id="modal-list-payments-finance-desc"></td>
                        </tr>
                        <tr>
                            <td width="25%">Sejumlah</td>
                            <td id="modal-list-payments-finance-amount"></td>
                        </tr>
                        <tr>
                            <td width="25%">Sudah dibayar</td>
                            <td id="modal-list-payments-finance-amount-paid"></td>
                        </tr>
                        <tr>
                            <td width="25%">Belum dibayar</td>
                            <td id="modal-list-payments-finance-amount-sisa"></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-hover">
                    <thead class="bg-info">
                        <th width="5%">No</th>
                        <th width="15%">Tanggal</th>
                        <th>Dibayar dari</th>
                        <th>Sejumlah</th>
                        <th width="15%">Print</th>
                    </thead>
                    <tbody id="view-list-payments"></tbody>
                </table>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
