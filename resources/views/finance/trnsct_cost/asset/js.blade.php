<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document).ready(function(){
    $("#select-sdm").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_sdm') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });

    $('.money').mask('000.000.000.000.000', {reverse: true});
    /* For add action */
    // clone table row
    $('.btn-clone').on('click', function () {
        var form = $(".form-content:last").clone(true).appendTo(".form-wrapper");
        form.find('select[name="finance_item_account_id[]"]').val("");
        form.find('input[name="finance_item_amount[]"]').val("0");


        // add attr button remove row
        form.find('.btn-danger').addClass("btn-remove");
    });

    // remove table row
    $('table').on('click', '.btn-remove',function () {
        $(this).closest(".form-content").remove();
        sumTotal();
    });

    $('input[name="finance_item_amount[]"]').on('keyup', function() {
        sumTotal();
    });

});

function sumTotal() {
    var ttl = 0;
    $('input[name="finance_item_amount[]"]').each(function(){
        ttl += parseFloat(this.value.replace(/\./g, ''));
    });

    $('input[name="finance_amount"]').val(formatRupiah(ttl));
    $('input[name="ttl_amount_text"]').val(formatRupiah(ttl));
    $('input[name="finance_amount_paid"]').val(formatRupiah(ttl));
    checkBalance();
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="cost_store"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/cost/detail')) !!}+"/"+res.finance_id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
