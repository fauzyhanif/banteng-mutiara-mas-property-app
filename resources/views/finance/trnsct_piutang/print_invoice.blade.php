<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice {{ $payment->invoice_id }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')

</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold mb-4">KWITANSI PENERIMAAN PEMBAYARAN PIUTANG</p>

                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td width="35%">No Kwitansi</td>
                                    <td class="font-weight-bold">: {{ $payment->jurnal_id }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Tgl Pembayaran</td>
                                    <td class="font-weight-bold">: {{ GeneralHelper::konversiTgl($payment->trnsct_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Telah Diterima Dari</td>
                                    <td class="font-weight-bold">: {{ $payment->related_person }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Sebesar</td>
                                    <td class="font-weight-bold">: Rp {{ GeneralHelper::rupiah($payment->amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Keterangan</td>
                                    <td class="font-weight-bold">: {{ $payment->description }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, {{ GeneralHelper::konversiTgl($payment->trnsct_date, 'ttd') }}</div>

                    <div class="col-md-6 col-print-6 text-center mb-4">Penyetor,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Petugas,</div>

                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $payment->related_person }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $payment->user }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
