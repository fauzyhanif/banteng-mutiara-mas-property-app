@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.piutang') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kasbon
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Kasbon</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-lg-10">
                                    <select name="search" class="form-control" id="select-sdm" placeholder="Cari nama orang" aria-label="Cari nama orang"
                                        aria-describedby="basic-addon2"></select>
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-primary btn-block">
                                        <i class="fa fa-search"></i> &nbsp;
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mb-3">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <hr>
            </div>
        </div>

        @if ($sdm != "")
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    Nama / No. Handphone
                                </div>
                                <div class="col-lg-9">
                                    : <b>{{ $sdm->name }} / {{ $sdm->phone_num }}</b>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-lg-3">
                                    Alamat
                                </div>
                                <div class="col-lg-9">
                                    : <b>{{ $sdm->address }}</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card" style="background-color: #d4edda">
                                        <div class="card-body">
                                            <p style="margin-bottom: -2px; color: #155724;">Total Perusahaan Hutang</p>
                                            <h2 class="font-weight-bold" style="color: #155724;">Rp {{ GeneralHelper::rupiah($ttl_tabungan) }} ,-</h2>

                                            <span class="text-secondary">
                                                Untuk pencairan tabungan mandor, silahkan buka menu operasional lalu ketik nama "{{ $sdm->name }}" di form pencairan
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="card" style="background-color: #f8d7da">
                                        <div class="card-body">
                                            <p style="margin-bottom: -2px; color: #721c24;">Total {{ $sdm->name }} Hutang</p>
                                            <h2 class="font-weight-bold" style="color: #721c24;">Rp {{ GeneralHelper::rupiah($ttl_kasbon) }} ,-</h2>
                                            <form action="{{ route('finance.piutang.form_pay') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="search" value="{{ $sdm->sdm_id }}">
                                                <button class="card-link btn btn-secondary btn-sm mt-2 {{ ($ttl_kasbon <= 0) ? 'disabled' : '' }}">Bayar</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Form input piutang</h3>
                        </div>
                        <div class="card-body">

                            @if ($ttl_tabungan > 0)
                                <h3 class="text-center font-weight-bold">Silahkan cairkan tabungan terlebih dahulu, lalu buat piutang baru</h3>

                                <span class="text-secondary">
                                    Untuk pencairan tabungan mandor, silahkan buka menu operasional lalu ketik nama "{{ $sdm->name }}" di form pencairan
                                </span>
                            @else
                                <form action="{{ route('finance.piutang.store') }}" method="POST" data-remote>
                                    @csrf
                                    <input type="hidden" name="sdm_id" value="{{ $sdm->sdm_id }}">
                                    <input type="hidden" name="sdm_name" value="{{ $sdm->name }}">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Tanggal Piutang <span class="text-red">*</span></label>
                                                <input type="date" name="trnsct_date" class="form-control" required value="{{ date('Y-m-d') }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Tempo <span class="text-red">*</span></label>
                                                <input type="date" name="due_date" class="form-control" required value="{{ date('Y-m-d') }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Uang Diambil dari <span class="text-red">*</span></label>
                                                <select name="account_id" class="form-control" required>
                                                    <option value="">** Pilih Akun Kas</option>
                                                    @foreach ($accounts as $account)
                                                    <option value="{{ $account->cash_id }}">{{ $account->cash_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Sebesar <span class="text-red">*</span></label>
                                                <input type="text" name="amount" class="form-control money" required value="0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Keterangan <span class="text-red">*</span></label>
                                                <textarea name="description" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                Simpan
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>

@include('finance.trnsct_piutang.asset.js')
@endsection
