<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function() {
    $("#select-sdm").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_sdm') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });

    $('.remaining-amount').keyup(function() {
        var amount = this.value.replace(/\./g, '');
        var row = $(this).closest("tr");

        var sum = 0;
        $('.remaining-amount').each(function(){
            sum += parseFloat(this.value.replace(/\./g, ''));
        });

        $('#total-bayar').text(formatRupiah(sum))
        $('input[name="ttl_amount"]').val(sum)
    });
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/piutang/form_store')) !!};
                    $.redirect(baseUrl, {search: res.sdm_id, _token: CSRF_TOKEN}, "POST", "");

                    var baseUrlPrint = {!! json_encode(url('/finance/piutang/print_kasbon')) !!};
                        baseUrlPrint = baseUrlPrint + "/" + res.kasbon_id;
                    window.open(baseUrlPrint, '_blank');
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="form_pay"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/piutang/form_pay')) !!};
                    $.redirect(baseUrl, {search: res.sdm_id, _token: CSRF_TOKEN}, "POST", "");
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
