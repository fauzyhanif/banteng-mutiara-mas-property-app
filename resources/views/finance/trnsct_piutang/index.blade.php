@extends('index')

@section('content')
<section class="content-header">
    <h1>Kasbon Mandor</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-12">
                <a href="{{ url('/finance/piutang?status=1') }}"
                    class="btn {{ ($status == 1) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Kasbon Baru
                </a>

                <a href="{{ url('/finance/piutang?status=2') }}"
                    class="btn {{ ($status == 2) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Pembayaran Sebagian
                </a>

                <a href="{{ url('/finance/piutang?status=3') }}"
                    class="btn {{ ($status == 3) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Selesai
                </a>

                <a href="{{ url('/finance/piutang?status=4') }}"
                    class="btn {{ ($status == 4) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Dibatalkan
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Kasbon Mandor
                        <div class="card-tools">
                            <a href="{{ route('finance.piutang.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Kasbon Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th>Nama</th>
                                <th>Tgl Transaksi</th>
                                <th>Deskripsi</th>
                                <th width="20%">Sisa</th>
                                <th width="10%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(url('/finance/piutang/list_data?status='.$status)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'sdm.name', name: 'sdm.name', searchable: true },
            { data: 'date', name: 'date', searchable: true },
            { data: 'kasbon_desc', name: 'kasbon_desc', searchable: true },
            { data: 'sisa', name: 'sisa', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@endsection

