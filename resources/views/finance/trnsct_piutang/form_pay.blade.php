@extends('index')
@section('content')

@php $ttl_piutang_blm_bayar = 0; @endphp
@if ($piutangs != "")
    @foreach ($piutangs as $piutang)
        @if ($piutang->kasbon_amount > $piutang->kasbon_amount_paid)
            @php $ttl_piutang_blm_bayar = 1 @endphp
        @endif
    @endforeach
@endif

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.piutang') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Piutang
            </a>
        </li>
        <li class="breadcrumb-item active">Pembayaran Piutang</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-lg-10">
                                    <select name="search" class="form-control" id="select-sdm" placeholder="Cari nama orang" aria-label="Cari nama orang"
                                        aria-describedby="basic-addon2"></select>
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-primary btn-block">
                                        <i class="fa fa-search"></i> &nbsp;
                                        Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mb-3">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <hr>
            </div>
        </div>

        @if ($sdm != "")
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <form name="form_pay" action="{{ route('finance.piutang.pay') }}" method="POST">

                            @csrf
                            <input type="hidden" name="sdm_id" value="{{ $sdm->sdm_id }}">
                            <input type="hidden" name="sdm_name" value="{{ $sdm->name }}">

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Nama / No. Handphone
                                    </div>
                                    <div class="col-lg-9">
                                        : <b>{{ $sdm->name }} / {{ $sdm->phone_num }}</b>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-lg-3">
                                        Alamat
                                    </div>
                                    <div class="col-lg-9">
                                        : <b>{{ $sdm->address }}</b>
                                    </div>
                                </div>

                                <hr>

                                @if ($ttl_piutang_blm_bayar == 0)
                                    <h3 class="text-center font-weight-bold">{{ $sdm->name }} tidak punya hutang ke Perusahaan </h3>
                                @else
                                    <div class="row mb-4">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tanggal <span class="text-red">*</span></label>
                                                <input type="date" name="trnsct_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Uang Masuk Kas <span class="text-red">*</span></label>
                                                <select name="account_id" class="form-control" required>
                                                    <option value="">** Pilih Akun Kas</option>
                                                    @foreach ($accounts as $account)
                                                        <option value="{{ $account->cash_id }}">{{ $account->cash_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <label>Keterangan <span class="text-red">*</span></label>
                                            <textarea name="description" class="form-control" required></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-sm">
                                                <thead class="bg-info">
                                                    <th width="5%">No</th>
                                                    <th>Nama Piutang</th>
                                                    <th width="15%" class="text-right">Jumlah</th>
                                                    <th width="15%" class="text-right">Terbayar</th>
                                                    <th width="15%" class="text-right">Sisa</th>
                                                </thead>
                                                <tbody>
                                                    @php $no = 1 @endphp
                                                    @php $ttl_amount = 0 @endphp
                                                    @php $ttl_amount_paid = 0 @endphp
                                                    @foreach ($piutangs as $piutang)
                                                        @if ($piutang->kasbon_amount > $piutang->kasbon_amount_paid)
                                                            <tr>
                                                                <td>{{ $no }}.</td>
                                                                <td>{{ $piutang->kasbon_desc }}</td>
                                                                <td class="text-right">{{ GeneralHelper::rupiah($piutang->kasbon_amount) }}</td>
                                                                <td class="text-right">{{ GeneralHelper::rupiah($piutang->kasbon_amount_paid) }}</td>
                                                                <td>
                                                                    <input type="text" name="amount[{{ $piutang->kasbon_id }}]" class="form-control form-control-sm money text-right remaining-amount" value="{{ $piutang->kasbon_amount - $piutang->kasbon_amount_paid }}">
                                                                </td>
                                                            </tr>

                                                            @php $ttl_amount += $piutang->kasbon_amount @endphp
                                                            @php $ttl_amount_paid += $piutang->kasbon_amount_paid @endphp
                                                            @php $no += 1 @endphp
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <th colspan="2" class="text-center">TOTAL</th>
                                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_amount) }}</th>
                                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_amount_paid) }}</th>
                                                    <th class="text-right" id="total-bayar">{{ GeneralHelper::rupiah($ttl_amount - $ttl_amount_paid) }}</th>

                                                    <input type="hidden" name="ttl_amount" value="{{ $ttl_amount - $ttl_amount_paid }}">

                                                </tfoot>
                                            </table>
                                    </div>

                                    <div class="col-md-12">
                                        <button class="btn btn-primary btn-block">
                                            Simpan Pembayaran
                                        </button>
                                    </div>
                                @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Piutang Lunas</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-sm">
                                <thead class="bg-info">
                                    <th width="5%">No</th>
                                    <th>Nama Piutang</th>
                                    <th class="text-right" width="15%">Jumlah</th>
                                    <th class="text-right" width="15%">Terbayar</th>
                                    <th class="text-right" width="15%">Sisa</th>
                                </thead>
                                <tbody>
                                    @php $no = 1 @endphp
                                    @php $end_ttl_amount = 0 @endphp
                                    @php $end_ttl_amount_paid = 0 @endphp
                                    @foreach ($piutangs as $piutang)
                                        @if ($piutang->kasbon_amount == $piutang->kasbon_amount_paid)
                                            <tr>
                                                <td>{{ $no }}.</td>
                                                <td>{{ $piutang->kasbon_desc }}</td>
                                                <td class="text-right">{{ GeneralHelper::rupiah($piutang->kasbon_amount) }}</td>
                                                <td class="text-right">{{ GeneralHelper::rupiah($piutang->kasbon_amount_paid) }}</td>
                                                <td class="text-right">{{ GeneralHelper::rupiah($piutang->kasbon_amount - $piutang->kasbon_amount_paid) }}</td>
                                            </tr>
                                            @php $end_ttl_amount += $piutang->kasbon_amount @endphp
                                            @php $end_ttl_amount_paid += $piutang->kasbon_amount_paid @endphp
                                            @php $no += 1 @endphp
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>

@include('finance.trnsct_piutang.asset.js')
@endsection
