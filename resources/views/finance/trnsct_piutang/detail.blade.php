@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.piutang') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kasbon
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Kasbon</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Informasi Kasbon</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal">
                                <i class="fas fa-trash"></i> Batalkan Kasbon
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                Status
                            </div>
                            <div class="col-lg-9">
                                :   @if ($piutang->kasbon_cancel == '0')
                                        <span class="badge badge-success">OK</span>
                                    @else
                                        <span class="badge badge-danger">BATAL</span>
                                    @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                Nama / No. Handphone
                            </div>
                            <div class="col-lg-9">
                                : <b>{{ $piutang->sdm->name }} / {{ $piutang->sdm->phone_num }}</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                Alamat
                            </div>
                            <div class="col-lg-9">
                                : <b>{{ $piutang->sdm->address }}</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                Tanggal Transaksi
                            </div>
                            <div class="col-lg-9">
                                : <b>{{ GeneralHelper::konversiTgl($piutang->kasbon_date) }}</b>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-3">
                                Keterangan
                            </div>
                            <div class="col-lg-9">
                                : <b>{{ $piutang->kasbon_desc }}</b>
                            </div>
                        </div>

                        <div class="row">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th width="33%">Jumlah</th>
                                        <th width="33%">Terbayar</th>
                                        <th width="33%" class="bg-warning">Sisa</th>
                                    </tr>
                                    <tr>
                                        <th>{{ GeneralHelper::rupiah($piutang->kasbon_amount) }}</th>
                                        <th>{{ GeneralHelper::rupiah($piutang->kasbon_amount_paid) }}</th>
                                        <th class="bg-info">{{ GeneralHelper::rupiah($piutang->kasbon_amount - $piutang->kasbon_amount_paid) }}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                        @if (($piutang->kasbon_amount - $piutang->kasbon_amount_paid) > 0)
                            @if ($piutang->kasbon_cancel == '0')
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{ route('finance.piutang.form_pay') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="search" value="{{ $piutang->kasbon_sdm_id }}">
                                            <button type="submit" class="btn btn-block btn-sm btn-success">BAYAR</button>
                                        </form>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sejarah Pembayaran</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-sm">
                            <thead class="bg-info">
                                <th width="5%">No</th>
                                <th>Tanggal Bayar</th>
                                <th width="25%">Via</th>
                                <th class="text-right" width="15%">Jumlah</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @foreach ($payments as $payment)
                                <tr>
                                    <td>{{ $no }}.</td>
                                    <td>{{ GeneralHelper::konversiTgl($payment->kasbon_paid_date) }}</td>
                                    <td>{{ $payment->cash_bank->cash_name }}</td>
                                    <td class="text-right">{{ GeneralHelper::rupiah($payment->kasbon_paid_amount) }}</td>
                                </tr>
                                @php $no += 1 @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Yakin ingin batalkan kasbon ini?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a type="button" href="{{ url('finance/piutang/cancel', $piutang->kasbon_id) }}" class="btn btn-danger">Ya, Batalkan Kasbon!</a>
            </div>

        </div>
    </div>
</div>

@include('finance.trnsct_piutang.asset.js')
@endsection
