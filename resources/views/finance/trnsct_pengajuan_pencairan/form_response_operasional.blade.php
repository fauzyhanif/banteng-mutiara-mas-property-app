@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.pengajuan_pencairan') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengajuan Pencairan
            </a>
        </li>
        <li class="breadcrumb-item active">Tanggapan Pengajuan Pencairan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail pengajuan</h5>
                    </div>
                    <div class="card-body" id="view-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="font-weight-bold">Hutang perusahaan yang dipilih</h5>
                                <table class="table table-sm table-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="15%" class="text-secondary">No Invoice</td>
                                            <td width="35%">#{{ $data_support->finance_id }}</td>
                                            <td width="15%" class="text-secondary">Jumlah</td>
                                            <td width="35%">{{  GeneralHelper::rupiah($data_support->finance_amount) }}</td>
                                        </tr>
                                        <tr>
                                            <td width="15%" class="text-secondary">Tgl Transaksi</td>
                                            <td width="35%">{{  GeneralHelper::konversiTgl($data_support->finance_date) }}</td>
                                            <td width="15%" class="text-secondary">Terbayar</td>
                                            <td width="35%">{{  GeneralHelper::rupiah($data_support->finance_amount_paid) }}</td>
                                        </tr>
                                        <tr>
                                            <td width="15%" class="text-secondary">User</td>
                                            <td width="35%">{{ $data_support->sdm->name }}</td>
                                            <td width="15%" class="text-secondary">Sisa</td>
                                            <td width="35%">{{  GeneralHelper::rupiah($data_support->finance_amount - $data_support->finance_amount_paid) }}</td>
                                        </tr>
                                        <tr>
                                            <td width="15%" class="text-secondary">Keterangan</td>
                                            <td colspan="3">{{ $data_support->finance_desc }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-lg-12">
                                <h5 class="font-weight-bold">Keterangan Pengajuan</h5>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="15%" class="text-secondary">Department</td>
                                            <td width="35%">{{ $submission->submission_department }}</td>
                                            <td width="15%" class="text-secondary">Bank</td>
                                            <td width="35%">{{ $submission->submission_bank }}</td>
                                        </tr>
                                        <tr>
                                            <td width="15%" class="text-secondary">Deadline</td>
                                            <td width="35%">{{ GeneralHelper::konversiTgl($submission->submission_deadline) }}</td>
                                            <td width="15%" class="text-secondary">Nomor Rekening</td>
                                            <td width="35%">{{ $submission->submission_rek_number }}</td>
                                        </tr>
                                        <tr>
                                            <td width="15%" class="text-secondary">Jumlah Pengajuan</td>
                                            <td width="35%">{{ GeneralHelper::rupiah($submission->submission_amount) }}</td>
                                            <td width="15%" class="text-secondary"></td>
                                            <td width="35%"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div id="box_form_store">
                            <form name="form_response" action="{{ url('/finance/pengajuan_pencairan/response') }}" method="POST">

                                @csrf
                                <input type="hidden" name="submission_id" value="{{ $submission->submission_id }}">
                                <input type="hidden" name="submission_type" value="OPERASIONAL">
                                <input type="hidden" name="finance_sdm_id" value="{{ $submission->finance->finance_sdm_id }}">

                                <div class="row mt-3">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Tanggal Pencairan</label>
                                            <input type="date" name="finance_paid_date" class="form-control" required value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Jumlah Pencairan</label>
                                            <input type="text" name="finance_paid_amount" class="form-control money" required
                                                value="{{ $submission->submission_amount }}">
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Dicairkan Via</label>
                                            <select name="finance_paid_cash_id" class="form-control" required>
                                                @foreach ($cashes as $cash)
                                                <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            <i class="fas fa-save"></i>
                                            SIMPAN TANGGAPAN
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function(){
        $('.money').mask('000.000.000.000.000', {reverse: true});
    })

    $('form[name="form_response"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/pengajuan_pencairan/detail')) !!}+"/"+res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            }
        });
    });
</script>
@endsection
