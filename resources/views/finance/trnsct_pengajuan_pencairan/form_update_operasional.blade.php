@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.pengajuan_pencairan') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengajuan Pencairan
            </a>
        </li>
        <li class="breadcrumb-item active">Edit Pengajuan Pencairan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail pengajuan</h5>
                    </div>
                    <div class="card-body" id="view-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="m-2">
                                    <h5 class="font-weight-bold float-left mb02">Hutang perusahaan yang dipilih</h5>
                                    <table class="table table-sm table-bordered">
                                        <tbody>
                                            <tr>
                                                <td width="15%" class="text-secondary">No Invoice</td>
                                                <td>#{{ $data_support->finance_id }}</td>
                                                <td width="15%" class="text-secondary">Jumlah</td>
                                                <td>{{  GeneralHelper::rupiah($data_support->finance_amount) }}</td>
                                            </tr>
                                            <tr>
                                                <td width="15%" class="text-secondary">Tgl Transaksi</td>
                                                <td>{{  GeneralHelper::konversiTgl($data_support->finance_date) }}</td>
                                                <td width="15%" class="text-secondary">Terbayar</td>
                                                <td>{{  GeneralHelper::rupiah($data_support->finance_amount_paid) }}</td>
                                            </tr>
                                            <tr>
                                                <td width="15%" class="text-secondary">User</td>
                                                <td>{{ $data_support->sdm->name }}</td>
                                                <td width="15%" class="text-secondary">Sisa</td>
                                                <td>{{  GeneralHelper::rupiah($data_support->finance_amount - $data_support->finance_amount_paid) }}</td>
                                            </tr>
                                            <tr>
                                                <td width="15%" class="text-secondary">Keterangan</td>
                                                <td>{{ $data_support->finance_desc }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="box_form_store">
                            <form name="form_submission" action="{{ url('/finance/pengajuan_pencairan/update') }}" method="POST">

                                @csrf
                                <input type="hidden" name="submission_id" value="{{ $submission->submission_id }}">
                                <input type="hidden" name="submission_type" value="OPERASIONAL">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Department</label>
                                            <input type="text" name="submission_department" class="form-control" value="{{ $submission->submission_department }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Deadline</label>
                                            <input type="date" name="submission_deadline" class="form-control" value="{{ $submission->submission_deadline }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Jumlah Pengajuan</label>
                                            <input type="text" name="submission_amount" class="form-control money" value="{{ $submission->submission_amount }}" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Bank</label>
                                            <input type="text" name="submission_bank" class="form-control" value="{{ $submission->submission_bank }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label>No Rekening</label>
                                            <input type="text" name="submission_rek_number" class="form-control" value="{{ $submission->submission_rek_number }}" required>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-sm btn-success">
                                        <i class="fas fa-save"></i>
                                        SIMPAN PENGAJUAN
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function(){
        $('.money').mask('000.000.000.000.000', {reverse: true});
    })

    $('form[name="form_submission"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/pengajuan_pencairan/detail')) !!}+"/"+res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            }
        });
    });
</script>
@endsection
