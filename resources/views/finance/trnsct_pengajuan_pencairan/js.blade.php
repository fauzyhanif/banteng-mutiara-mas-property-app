<script>
    $(document).ready(function(){
        first_step();
    })

    function first_step() {
        var url = "{{ url('finance/pengajuan_pencairan/form_store_first_step') }}";
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#view-form').html(res);
            }
        });
    }

    function second_step(parameter) {
        var url = "{{ url('finance/pengajuan_pencairan/form_store_second_step?step=') }}"+parameter;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#view-form').html(res);
            }
        });
    }

    (function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


})();
</script>
