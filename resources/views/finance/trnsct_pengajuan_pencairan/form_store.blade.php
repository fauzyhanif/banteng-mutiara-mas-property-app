@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.pengajuan_pencairan') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengajuan Pencairan
            </a>
        </li>
        <li class="breadcrumb-item active">Buat Pengajuan Pencairan Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Pilih jenis pengajuan (klik item dibawah)</h5>
                    </div>
                    <div class="card-body" id="view-form">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.trnsct_pengajuan_pencairan.js')
@endsection
