<table class="table table-sm table-bordered table-hover">
    <thead class="bg-info">
        <th>Invoice</th>
        <th>User</th>
        <th>keterangan</th>
        <th>Jumlah</th>
        <th>Sisa</th>
        <th>Aksi</th>
    </thead>
    <tbody>
        @foreach ($datas as $data)
            <tr>
                <td>
                    <span class="font-weight-bold">#{{ $data->finance_id }}</span> <br>
                    <span class="text-secondary">{{ date("d/m/Y", strtotime($data->finance_date)) }}</span>
                </td>
                <td>{{ $data->sdm->name }}</td>
                <td>{{ $data->finance_desc }}</td>
                <td>{{ GeneralHelper::rupiah($data->finance_amount) }}</td>
                <td>{{ GeneralHelper::rupiah($data->finance_amount - $data->finance_amount_paid) }}</td>
                <td>
                    <button
                        class="btn btn-xs btn-primary"
                        onclick="pin_hutang_perusahaan(
                            '{{ $data->finance_id }}',
                            '{{ $data->finance_date }}',
                            '{{ $data->sdm->name }}',
                            '{{ $data->finance_desc }}',
                            '{{ $data->finance_amount }}',
                            '{{ $data->finance_amount_paid }}',
                            '{{ $data->sdm->rek_bank }}',
                            '{{ $data->sdm->rek_number }}'
                        )">
                        <i class="fas fa-check"></i>
                        Pilih
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="list_hutang_perusahaan('{{ $key }}',1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $datas->lastPage(); $i++)
        <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="list_hutang_perusahaan('{{ $key }}', {{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="list_hutang_perusahaan('{{ $key }}', {{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif
