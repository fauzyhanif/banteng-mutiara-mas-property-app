@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.pengajuan_pencairan') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengajuan Pencairan
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Pengajuan Pencairan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail Pengajuan</h5>

                        <div class="card-tools">
                            @if ($data->submission_status == 'OPEN')
                                <a href="{{ url('finance/pengajuan_pencairan/form_response', $data->submission_id) }}" class="btn btn-sm btn-success">
                                    <i class="fas fa-check"></i> Tanggapi Pengajuan
                                </a>
                                <a href="{{ url('finance/pengajuan_pencairan/form_update', $data->submission_id) }}" class="btn btn-sm btn-primary">
                                    <i class="fas fa-edit"></i> Edit Pengajuan
                                </a>
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-delete">
                                    <i class="fas fa-trash"></i> Hapus Pengajuan
                                </button>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <td width="25%" class="text-secondary">Jenis</td>
                                    <td>Operasional</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Deadline Pencairan</td>
                                    <td>{{ GeneralHelper::konversiTgl($data->submission_deadline) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Status</td>
                                    <td>{{ $data->submission_status }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Department</td>
                                    <td>{{ $data->submission_department }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">User</td>
                                    <td>{{ $data->finance->sdm->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Keterangan</td>
                                    <td>{{ $data->finance->finance_desc }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Total Transaksi</td>
                                    <td>{{ GeneralHelper::rupiah($data->finance->finance_amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Belum Dibayar</td>
                                    <td>{{ GeneralHelper::rupiah($data->finance->finance_amount - $data->finance->finance_amount_paid) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Pengajuan Pencairan</td>
                                    <td>{{ GeneralHelper::rupiah($data->submission_amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Bank</td>
                                    <td>{{ $data->submission_bank }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Nomor Rekening</td>
                                    <td>{{ $data->submission_rek_number }}</td>
                                </tr>
                            </tbody>
                        </table>

                        @if ($data->submission_status == 'CLOSE')
                            <h5 class="mt-4">Pencairan</h5>
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td width="25%" class="text-secondary">Tgl Pencairan</td>
                                        <td>{{ GeneralHelper::konversiTgl($data->submission_resp_date) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="text-secondary">Jml Pencairan</td>
                                        <td>{{ GeneralHelper::rupiah($data->submission_amount_paid) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- The Modal -->
<div class="modal" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ url('finance/pengajuan_pencairan/delete') }}" method="POST">
                @csrf
                <input type="hidden" name="submission_id" value="{{ $data->submission_id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus Pengajuan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Yakin ingin hapus pengajuan ini?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Ya, Hapus Sekarang</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
