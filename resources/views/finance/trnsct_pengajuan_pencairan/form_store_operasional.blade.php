<div class="row mb-2">
    <div class="col-lg-12 mb-2">
        <button type="button" class="btn btn-sm btn-outline-secondary" onclick="first_step()">
            <i class="fas fa-arrow-left"></i> Kembali
        </button>

        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">
            <i class="fas fa-search"></i> Cari hutang perusahaan
        </button>
    </div>
</div>

<div class="row" id="box_ket_hutang_perusahaan" style="display: none">
    <div class="col-lg-12">
        <div class="m-2">
            <h5 class="font-weight-bold float-left">Hutang perusahaan yang dipilih</h5>
            <button type="button" class="btn btn-xs btn-danger float-right" onclick="hide_box_ket_hutang_perusahaan()">
                <i class="fas fa-trash"></i> Batalkan
            </button>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td width="15%" class="text-secondary">No Invoice</td>
                        <td id="pin_finance_id"></td>
                        <td width="15%" class="text-secondary">Jumlah</td>
                        <td id="pin_finance_amount"></td>
                    </tr>
                    <tr>
                        <td width="15%" class="text-secondary">Tgl Transaksi</td>
                        <td id="pin_finance_date"></td>
                        <td width="15%" class="text-secondary">Terbayar</td>
                        <td id="pin_finance_amount_paid"></td>
                    </tr>
                    <tr>
                        <td width="15%" class="text-secondary">User</td>
                        <td id="pin_finance_user"></td>
                        <td width="15%" class="text-secondary">Sisa</td>
                        <td id="pin_finance_amount_remainder"></td>
                    </tr>
                    <tr>
                        <td width="15%" class="text-secondary">Keterangan</td>
                        <td colspan="3" id="pin_finance_desc"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="box_form_store" style="display: none">
    <form name="form_submission" action="{{ url('/finance/pengajuan_pencairan/store') }}" method="POST">

        @csrf
        <input type="hidden" name="sdm_id" value="">
        <input type="hidden" name="finance_id">
        <input type="hidden" name="submission_type" value="OPERASIONAL">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Department</label>
                    <input type="text" name="submission_department" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Deadline</label>
                    <input type="date" name="submission_deadline" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Jumlah Pengajuan</label>
                    <input type="text" name="submission_amount" class="form-control money" required>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Bank</label>
                    <input type="text" name="submission_bank" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>No Rekening</label>
                    <input type="text" name="submission_rek_number" class="form-control" required>
                </div>
            </div>

            <button type="submit" class="btn btn-sm btn-success">
                <i class="fas fa-save"></i>
                SIMPAN PENGAJUAN
            </button>
        </div>
    </form>
</div>

{{-- modal cari transaksi --}}
<div class="modal" id="myModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pencarian operasional belum terbayar</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" id="search-hutang-perusahaan" placeholder="Silahkan ketik no invoice / nama / keterangan operasional">
                </div>

                <div id="view-list-hutang-perusahaan">

                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.money').mask('000.000.000.000.000', {reverse: true});

        list_hutang_perusahaan(null, 1);

        $("#search-hutang-perusahaan").keyup(function(){
            list_hutang_perusahaan(this.value);
        });
    })


    function list_hutang_perusahaan(key, current_page) {
        if (key == '') { key = null }
        var url = "{{ url('finance/pengajuan_pencairan/list_hutang_perusahaan?page=') }}"+current_page;
        var _token = '<?php echo csrf_token() ?>';
        var data = "key="+key+"&_token="+_token;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType:'html',
            success: function (res) {
                $('#view-list-hutang-perusahaan').html(res);
            }
        });
    }

    function pin_hutang_perusahaan(finance_id, finance_date, finance_user, finance_desc, finance_amount, finance_amount_paid, finance_rek_bank, finance_rek_number) {
        $('#pin_finance_id').text("#"+finance_id);
        $('#pin_finance_date').text(GetFormattedDate(finance_date));
        $('#pin_finance_user').text(finance_user);
        $('#pin_finance_desc').text(finance_desc);
        $('#pin_finance_amount').text(formatRupiah(finance_amount));
        $('#pin_finance_amount_paid').text(formatRupiah(finance_amount_paid));
        $('#pin_finance_amount_remainder').text(formatRupiah(finance_amount - finance_amount_paid));
        $('input[name="finance_id"]').val(finance_id);
        $('input[name="submission_amount"]').val(formatRupiah(finance_amount - finance_amount_paid));
        $('input[name="submission_bank"]').val(finance_rek_bank);
        $('input[name="submission_rek_number"]').val(finance_rek_number);

        $('#box_ket_hutang_perusahaan').css('display', 'block');
        $('#box_form_store').css('display', 'block');
        $('#myModal').modal('hide');
    }

    function hide_box_ket_hutang_perusahaan() {
        $('#box_ket_hutang_perusahaan').css('display', 'none');
        $('#box_form_store').css('display', 'none');
        $('input[name="finance_id"]').val("");
        $('input[name="submission_department"]').val("");
        $('input[name="submission_date"]').val("");
        $('input[name="submission_amount"]').val("");
        $('input[name="submission_bank"]').val("");
        $('input[name="submission_rek_number"]').val("");
    }

    $('form[name="form_submission"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/pengajuan_pencairan/detail')) !!}+"/"+res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            }
        });
    });
</script>
