@extends('index')

@section('content')
<section class="content-header">
    <h1>Pengajuan Pencairan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-12">
                <a href="{{ url('/finance/pengajuan_pencairan?type=OPERASIONAL&status=OPEN') }}"
                    class="btn {{ ($type == 'OPERASIONAL' && $status == 'OPEN') ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Operasional Open
                </a>

                <a href="{{ url('/finance/pengajuan_pencairan?type=OPERASIONAL&status=CLOSE') }}"
                    class="btn {{ ($type == 'OPERASIONAL' && $status == 'CLOSE') ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Operasional Close
                </a>

                <a href="{{ url('/finance/pengajuan_pencairan?type=CASHBOND&status=OPEN') }}"
                    class="btn {{ ($type == 'CASHBOND' && $status == 'OPEN') ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Cashbond Open
                </a>

                <a href="{{ url('/finance/pengajuan_pencairan?type=CASHBOND&status=CLOSE') }}"
                    class="btn {{ ($type == 'CASHBOND' && $status == 'CLOSE') ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Cashbond Close
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Pengajuan Pencairan
                        <div class="card-tools">
                            <a href="{{ route('finance.pengajuan_pencairan.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Buat Pengajuan Pencairan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th width="10%">Jenis</th>
                                <th width="10%">Deadline</th>
                                <th width="15%">Department</th>
                                <th>Keterangan</th>
                                <th width="15%">Pengajuan</th>
                                <th width="10%">Status</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(url('/finance/pengajuan_pencairan/list_data?type='.$type.'&status='.$status)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'submission_type', name: 'submission_type', searchable: true },
            { data: 'deadline', name: 'deadline', searchable: true },
            { data: 'submission_department', name: 'submission_department', searchable: true },
            { data: 'ket', name: 'ket', searchable: true },
            { data: 'jumlah', name: 'jumlah', searchable: true },
            { data: 'status', name: 'status', searchable: true },
        ]
    });
}
</script>
@endsection
