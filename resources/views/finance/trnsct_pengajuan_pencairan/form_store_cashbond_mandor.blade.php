<div class="row mb-2">
    <div class="col-lg-12 mb-2">
        <button type="button" class="btn btn-sm btn-outline-secondary" onclick="first_step()">
            <i class="fas fa-arrow-left"></i> Kembali
        </button>

        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">
            <i class="fas fa-search"></i> Cari mandor
        </button>
    </div>
</div>

<div class="row" id="box_ket_sdm" style="display: none">
    <div class="col-lg-12">
        <div class="m-2">
            <h5 class="font-weight-bold float-left">Mandor yang dipilih</h5>
            <button type="button" class="btn btn-xs btn-danger float-right" onclick="hide_box_ket_cashbond()">
                <i class="fas fa-trash"></i> Batalkan
            </button>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td width="20%" class="text-secondary">Nama</td>
                        <td id="pin_sdm_name"></td>
                    </tr>
                    <tr>
                        <td width="20%" class="text-secondary">No Telp</td>
                        <td id="pin_sdm_phone_num"></td>
                    </tr>
                    <tr>
                        <td width="20%" class="text-secondary">Alamat</td>
                        <td id="pin_sdm_address"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="box_form_store" style="display: none">
    <form name="form_submission" action="{{ url('/finance/pengajuan_pencairan/store') }}" method="POST">

        @csrf
        <input type="hidden" name="finance_id" value="">
        <input type="hidden" name="sdm_id">
        <input type="hidden" name="submission_type" value="CASHBOND">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Department</label>
                    <input type="text" name="submission_department" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Deadline</label>
                    <input type="date" name="submission_deadline" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Jumlah Pengajuan</label>
                    <input type="text" name="submission_amount" class="form-control money" required>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Bank</label>
                    <input type="text" name="submission_bank" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>No Rekening</label>
                    <input type="text" name="submission_rek_number" class="form-control" required>
                </div>
            </div>

            <button type="submit" class="btn btn-sm btn-success">
                <i class="fas fa-save"></i>
                SIMPAN PENGAJUAN
            </button>
        </div>
    </form>
</div>

{{-- modal cari transaksi --}}
<div class="modal" id="myModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pencarian mandor</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div id="view-list-sdm">

                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.money').mask('000.000.000.000.000', {reverse: true});

        list_sdm(null, 1);

        $("#search-sdm").keyup(function(){
            list_sdm(this.value,1);
        });
    })


    function list_sdm(key, current_page) {
        if (key == '') { key = null }
        var url = "{{ url('finance/pengajuan_pencairan/list_sdm?page=') }}"+current_page;
        var _token = '<?php echo csrf_token() ?>';
        var data = "key="+key+"&_token="+_token;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType:'html',
            success: function (res) {
                $('#view-list-sdm').html(res);
            }
        });
    }

    function pin_sdm(sdm_id, sdm_name, sdm_phone_num, sdm_address, sdm_bank, sdm_rek_num) {
        $('#pin_sdm_id').text(sdm_id);
        $('#pin_sdm_name').text(sdm_name);
        $('#pin_sdm_address').text(sdm_address);
        $('#pin_sdm_phone_num').text(sdm_phone_num);
        $('input[name="sdm_id"]').val(sdm_id);
        $('input[name="submission_bank"]').val(sdm_bank);
        $('input[name="submission_rek_number"]').val(sdm_rek_num);

        $('#box_ket_sdm').css('display', 'block');
        $('#box_form_store').css('display', 'block');
        $('#myModal').modal('hide');
    }

    function hide_box_ket_cashbond() {
        $('#box_ket_sdm').css('display', 'none');
        $('#box_form_store').css('display', 'none');
        $('input[name="sdm_id"]').val("");
        $('input[name="submission_department"]').val("");
        $('input[name="submission_date"]').val("");
        $('input[name="submission_amount"]').val("");
        $('input[name="submission_bank"]').val("");
        $('input[name="submission_rek_number"]').val("");
    }

    $('form[name="form_submission"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/finance/pengajuan_pencairan/detail')) !!}+"/"+res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            }
        });
    });
</script>
