<table class="table table-sm table-bordered table-hover">
    <thead class="bg-info">
        <th>Nama</th>
        <th>No Handphone</th>
        <th>Alamat</th>
        <th>Aksi</th>
    </thead>
    <tbody>
        @foreach ($datas as $data)
            <tr>
                <td>{{ $data->sdm->name }}</td>
                <td>{{ $data->sdm->phone_num }}</td>
                <td>{{ $data->sdm->address }}</td>
                <td>
                    <button
                        class="btn btn-xs btn-primary"
                        onclick="pin_sdm(
                            '{{ $data->sdm->sdm_id }}',
                            '{{ $data->sdm->name }}',
                            '{{ $data->sdm->phone_num }}',
                            '{{ $data->sdm->address }}',
                            '{{ $data->sdm->rek_bank }}',
                            '{{ $data->sdm->rek_number }}',
                        )">
                        <i class="fas fa-check"></i>
                        Pilih
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="list_sdm('{{ $key }}',1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $datas->lastPage(); $i++)
        <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="list_sdm('{{ $key }}', {{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="list_sdm('{{ $key }}', {{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif
