<div class="form-group">
    <label>Nama Akun <span class="text-red">*</span></label>
    <input
        type="text"
        name="account_name"
        class="form-control"
        required
        @isset($account)
            value="{{ $account->account_name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Pemasukan/Pengeluaran?</label>
    <select name="account_type" class="form-control">
        <option value="PEMASUKAN" @isset($account) @if ($account->account_type == 'PEMASUKAN')
            selected
            @endif
            @endisset>
            PEMASUKAN
        </option>
        <option value="PENGELUARAN" @isset($account) @if ($account->account_type == 'PENGELUARAN')
            selected
            @endif
            @endisset>
            PENGELUARAN
        </option>
    </select>
</div>

<div class="form-group">
    <label>Pos Akuntansi <span class="text-red">*</span></label>
    <select name="account_accounting_id" class="form-control" required>
        <option value="">-- Pilih Pos Akuntansi --</option>
        @foreach ($accounts as $item)
            <option value="{{ $item->account_id }}" @isset($account) {{ ($account->account_accounting_id == $item->account_id) ? 'selected' : '' }} @endisset>{{ $item->account_name }}</option>
        @endforeach
    </select>
</div>
