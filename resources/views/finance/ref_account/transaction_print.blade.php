<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dokumen</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    <style>
        body {
            font-size: 11px
        }
    </style>
</head>
<body>
    <body onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                <div class="row kop-surat">
                    <div class="col-md-1 col-print-1">
                        <img src="{{ url('public/img/logo_bmm.jpeg') }}" style="max-width: 50px; margin-top: -10px;"
                            alt="" class="logo-kop">
                    </div>
                    <div class="col-md-11 col-print-11">
                        <h4><b>PT LAN SENA JAYA</b></h4>
                        <p style="margin-top: -10px; font-size: 11px">DEVELOPER & CONTRACTOR</p>
                    </div>
                </div>

                <hr style="margin-top: -10px">

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="font-weight-bold mb-4">
                            DAFTAR TRANSAKSI - {{ $account->account_name }} <br>
                            PERIODE {{ date("d-m-Y", strtotime($start_date)) }} - {{ date("d-m-Y", strtotime($end_date)) }}
                        </p>

                        <table class="table table-sm table-bordered">
                            <thead>
                                <th>Tgl Transaksi</th>
                                <th>Terkait</th>
                                <th>Deskripsi</th>
                                <th width="20%" class="text-right">Jumlah</th>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ date("d-m-Y", strtotime($data->finance_date)) }}</td>
                                        <td>{{ $data->sdm_name }}</td>
                                        <td>{{ $data->finance_desc }}</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($data->finance_item_amount) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </body>
</body>
</html>
