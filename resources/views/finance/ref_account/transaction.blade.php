@extends('index')

@section('content')

<input type="hidden" name="start_date" value="{{ $start_date }}">
<input type="hidden" name="end_date" value="{{ $end_date }}">

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.account') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Akun
            </a>
        </li>
        <li class="breadcrumb-item active">Sejarah Transaksi</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <form action="" method="POST">
            @csrf
            <div class="row mb-3">
                <div class="col-md-4">
                    <input type="text" name="range_date" class="form-control">
                </div>
                <div class="col-md-8">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-filter"></i> FILTER</button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Sejarah Transaksi - <b>{{ $account->account_name }} - Rp {{ GeneralHelper::rupiah($jumlah) }}</b>

                        <div class="card-tools">
                            <button class="btn btn-sm bg-purple" data-toggle="modal" data-target="#modal-print">
                                <i class="fas fa-print"></i> Cetak
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th>Tgl Transaksi</th>
                                <th>Terkait</th>
                                <th>Deskripsi</th>
                                <th width="20%">Jumlah</th>
                                <th width="10%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-print">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cetak Sejarah Transaksi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="{{ route('finance.account.transaction_print') }}" method="POST" target="_blank">
                @csrf
                <input type="hidden" name="account_id" value="{{ $account->account_id }}">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>periode</label>
                        <input type="text" name="range_date" class="form-control">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Cetak</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var start_date = $('input[name="start_date"]').val();
        var end_date = $('input[name="end_date"]').val();
        $('input[name="range_date"]').daterangepicker({
            startDate: start_date,
            endDate: end_date,
            locale : {
                format : 'YYYY-MM-DD'
            }
        });
    });

    $(function() {
        loadData();
    });

    function loadData() {
        var date = $('input[name="range_date"]').val();
        var split_date = date.split(" - ");
        var start_date = split_date[0];
        var end_date = split_date[1];

        var base = {!! json_encode(url('/finance/account/transaction_list_data?account_id='.$account_id)) !!}+'&start_date='+start_date+'&end_date='+end_date;
        $('.datatable').DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: base,

            columns: [
                { data: 'date', name: 'date', searchable: true },
                { data: 'sdm_name', name: 'sdm_name', searchable: true },
                { data: 'finance_desc', name: 'finance_desc', searchable: true },
                { data: 'jumlah', name: 'jumlah', searchable: true },
                { data: 'actions_link', name: 'actions_link', searchable: true },
            ]
        });
    }
</script>
@endsection
