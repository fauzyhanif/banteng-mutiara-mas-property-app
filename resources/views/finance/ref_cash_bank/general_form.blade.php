<div class="form-group">
    <label>Nama Kas / Bank <span class="text-red">*</span></label>
    <input
        type="text"
        name="cash_name"
        class="form-control"
        required
        @isset($cash)
            value="{{ $cash->cash_name }}"
        @endisset
    >
</div>
