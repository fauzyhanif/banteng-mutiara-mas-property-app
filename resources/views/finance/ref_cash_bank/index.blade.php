@extends('index')

@section('content')
<section class="content-header">
    <h1>Kas & Bank</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Akun
                        <div class="card-tools">
                            <a href="{{ route('finance.cash_bank.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Akun Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered datatable">
                            <thead class="bg-info">
                                <th>Nama Kas / Bank</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('finance.ref_cash_bank.asset.js')
@endsection

