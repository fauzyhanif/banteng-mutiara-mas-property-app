@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.cash_bank') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Akun
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Akun Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <form id="form-add" action="{{ route('finance.cash_bank.store') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="add">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('finance.ref_cash_bank.general_form')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-sm btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-sm btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.ref_cash_bank.asset.js')
@endsection
