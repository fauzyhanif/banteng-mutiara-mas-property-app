@extends('index')
@section('content')

<input type="hidden" name="start_date" value="{{ $start_date }}">
<input type="hidden" name="end_date" value="{{ $end_date }}">
<input type="hidden" name="cash_id" value="{{ $cash_id }}">

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('finance.cash_bank') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Akun
            </a>
        </li>
        <li class="breadcrumb-item active">Cashflow</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">

        <form action="{{ url('finance/cash_bank/cashflow', $cash_id) }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-4 col-md-8 col-sm-12">
                    <input type="text" name="date" class="form-control">
                </div>
                <div class="col-lg-8 col-md-4 col-sm-12">
                    <button type="submit" class="btn btn-primary">
                        TAMPILKAN
                    </button>
                </div>
            </div>
        </form>

        <div class="row mt-4">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3 id="cashflow-uang-masuk">0</h3>

                        <p>Uang Masuk</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-md-6 col-sm-12+">
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3 id="cashflow-uang-keluar">0</h3>

                        <p>Uang Keluar</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Kasbon Mandor & Pembayaran Kasbon</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered" id="cashflow-kasbon" style="width:100%">
                            <thead class="bg-info">
                                <th width="15%">Tanggal</th>
                                <th width="25%">Mandor</th>
                                <th>Keterangan</th>
                                <th width="15%">Uang Masuk</th>
                                <th width="15%">Uang Keluar</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Uang Masuk</th>
                                <th width="50%">Total Uang Keluar</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="cashflow-kasbon-total-uang-masuk"></td>
                                    <td id="cashflow-kasbon-total-uang-keluar"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Penjualan Unit & Return</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered" id="cashflow-penjualan" style="width:100%">
                            <thead class="bg-info">
                                <th width="15%">Tanggal</th>
                                <th width="25%">Customer</th>
                                <th>Keterangan</th>
                                <th width="15%">Uang Masuk</th>
                                <th width="15%">Uang Keluar</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Uang Masuk</th>
                                <th width="50%">Total Uang Keluar</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="cashflow-penjualan-total-uang-masuk"></td>
                                    <td id="cashflow-penjualan-total-uang-keluar"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Operasional Perusahaan</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered" id="cashflow-operasional" style="width:100%">
                            <thead class="bg-info">
                                <th width="15%">Tanggal</th>
                                <th width="25%">User</th>
                                <th>Keterangan</th>
                                <th width="15%">Uang Masuk</th>
                                <th width="15%">Uang Keluar</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Uang Masuk</th>
                                <th width="50%">Total Uang Keluar</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="cashflow-operasional-total-uang-masuk"></td>
                                    <td id="cashflow-operasional-total-uang-keluar"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
    var start_date = $('input[name="start_date"]').val();
    var end_date = $('input[name="end_date"]').val();
    $('input[name="date"]').daterangepicker({
        startDate: start_date,
        endDate: end_date,
		locale : {
			format : 'YYYY-MM-DD'
		}
    });

    cashflow_total();
    cashflow_kasbon();
    cashflow_penjualan();
    cashflow_operasional();
});

function cashflow_total() {
    var cash_id = $('input[name="cash_id"]').val();
    var date = $('input[name="date"]').val();
    var split_date = date.split(" - ");
    var start_date = split_date[0];
    var end_date = split_date[1];

    var url = {!! json_encode(url('/finance/cash_bank/cashflow_total?cash_id=')) !!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('#cashflow-uang-masuk').text(res.uang_masuk);
            $('#cashflow-uang-keluar').text(res.uang_keluar);
        }
    })
}

function cashflow_kasbon() {
    var cash_id = $('input[name="cash_id"]').val();
    var date = $('input[name="date"]').val();
    var split_date = date.split(" - ");
    var start_date = split_date[0];
    var end_date = split_date[1];

    // data table
    var url = {!! json_encode(url('/finance/cash_bank/cashflow_kasbon?cash_id='))!!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $('#cashflow-kasbon').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'date', name: 'date', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'desc', name: 'desc', searchable: true },
            { data: 'uang_masuk', name: 'uang_masuk', searchable: true },
            { data: 'uang_keluar', name: 'uang_keluar', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/finance/cash_bank/cashflow_kasbon_total?cash_id=')) !!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#cashflow-kasbon-total-uang-masuk').text(res.uang_masuk);
            $('#cashflow-kasbon-total-uang-keluar').text(res.uang_keluar);
        }
    })
}

function cashflow_penjualan() {
    var cash_id = $('input[name="cash_id"]').val();
    var date = $('input[name="date"]').val();
    var split_date = date.split(" - ");
    var start_date = split_date[0];
    var end_date = split_date[1];

    // data table
    var url = {!! json_encode(url('/finance/cash_bank/cashflow_penjualan?cash_id='))!!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $('#cashflow-penjualan').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'date', name: 'date', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'desc', name: 'desc', searchable: true },
            { data: 'uang_masuk', name: 'uang_masuk', searchable: true },
            { data: 'uang_keluar', name: 'uang_keluar', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/finance/cash_bank/cashflow_penjualan_total?cash_id=')) !!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#cashflow-penjualan-total-uang-masuk').text(res.uang_masuk);
            $('#cashflow-penjualan-total-uang-keluar').text(res.uang_keluar);
        }
    })
}

function cashflow_operasional() {
    var cash_id = $('input[name="cash_id"]').val();
    var date = $('input[name="date"]').val();
    var split_date = date.split(" - ");
    var start_date = split_date[0];
    var end_date = split_date[1];

    // data table
    var url = {!! json_encode(url('/finance/cash_bank/cashflow_operasional?cash_id='))!!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $('#cashflow-operasional').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'date', name: 'date', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'desc', name: 'desc', searchable: true },
            { data: 'uang_masuk', name: 'uang_masuk', searchable: true },
            { data: 'uang_keluar', name: 'uang_keluar', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/finance/cash_bank/cashflow_operasional_total?cash_id=')) !!}+cash_id+'&start_date='+start_date+'&end_date='+end_date;
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#cashflow-operasional-total-uang-masuk').text(res.uang_masuk);
            $('#cashflow-operasional-total-uang-keluar').text(res.uang_keluar);
        }
    })
}
</script>
@endsection
