@extends('index')

@section('content')
<section class="content-header">
    <h1>Hutang Bank</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Hutang
                        <div class="card-tools">
                            <a href="{{ route('finance.hutang_bank.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Hutang Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered datatable">
                            <thead class="bg-info">
                                <th>Nama Bank</th>
                                <th>Keterangan</th>
                                <th width="20%">Total</th>
                                <th width="20%">Total Terbayar</th>
                                <th width="20%">Total Sisa</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('finance.ref_debt_to_bank.asset.js')
@endsection

