<script>
$(function() {
    loadData();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update' && res.status == "success") {
                    form.trigger("reset");
                }

                if (res.status == "success") {
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('finance.hutang_bank.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'sdm.name', name: 'sdm.name', searchable: true },
            { data: 'debt_desc', name: 'debt_desc', searchable: true },
            { data: 'debt_total_html', name: 'debt_total_html', searchable: true },
            { data: 'debt_total_paid_html', name: 'debt_total_paid_html', searchable: true },
            { data: 'total_remainder_html', name: 'total_remainder_html', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
