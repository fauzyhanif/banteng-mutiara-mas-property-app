<div class="form-group">
    <label>Nama Bank <span class="text-red">*</span></label>
    <select name="sdm_id" class="form-control" required>
        <option value="">-- Pilih Bank --</option>
        @foreach ($banks as $bank)
            <option
                value="{{ $bank->sdm_id }}"
                @isset($debt)
                    {{ ($debt->sdm_id == $bank->sdm_id) }} ? 'selected' : ''
                @endisset
            >{{ $bank->sdm->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Keterangan <span class="text-red">*</span></label>
    <input
        type="text"
        name="debt_desc"
        class="form-control"
        required
        @isset($debt)
            value="{{ $debt->debt_desc }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Total Hutang <span class="text-red">*</span></label>
    <input
        type="text"
        name="debt_total"
        class="form-control money"
        required
        @isset($debt)
            value="{{ $debt->debt_total }}"
        @endisset
    >
</div>
