@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('reports.foreman_job') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Laporan Pekerjaan Mandor
            </a>
        </li>
        <li class="breadcrumb-item active">List Pekerjaan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Nama Mandor</td>
                                    <td>: {{ $foreman->sdm->name }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">No Handphone</td>
                                    <td>: {{ $foreman->sdm->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Spesialis</td>
                                    <td>: {{ $foreman->specialist }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Rekening</td>
                                    <td>: {{ $foreman->sdm->rek_bank }} {{ $foreman->sdm->rek_number }} A/N {{ $foreman->sdm->rek_name }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Alamat</td>
                                    <td>: Pondok Bali, Subang</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Pekerjaan</span>
                        <span class="info-box-number">
                            @if (count($infografis) > 0)
                                {{ $infografis[0]->total }}
                            @else
                                0
                            @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-check-circle"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Selesai</span>
                        <span class="info-box-number">
                            @if (count($infografis) > 0)
                                {{ $infografis[0]->selesai }}
                            @else
                                0
                            @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-spinner"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Belum Selesai</span>
                        <span class="info-box-number">
                            @if (count($infografis) > 0)
                                {{ $infografis[0]->blm_selesai }}
                            @else
                                0
                            @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-wallet"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Penghasilan</span>
                        <span class="info-box-number">
                            Rp {{ GeneralHelper::rupiah($foreman->ttl_salary) }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Pekerjaan Mandor A</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered table-striped">
                            <thead>
                                <th>Pekerjaan</th>
                                <th width="35%">Lokasi</th>
                                <th width="10%">Progress</th>
                                <th width="15%">Biaya</th>
                            </thead>
                            <tbody>
                                @foreach ($jobs as $job)
                                    <tr>
                                        <td>
                                            <span class="font-weight-bold">{{ $job->todo }}</span> <br>
                                            {{ $job->brief }}
                                        </td>
                                        <td>{{ $job->unit_id }} Blok {{ $job->block->name }} {{ $job->location->name }}</td>
                                        <td>{{ $job->progress }}%</td>
                                        <td>
                                            Rp {{ GeneralHelper::rupiah($job->total) }} <br>
                                            @if ($job->st_transfer == '1')
                                                <span class="text-green">Sudah Cair</span>
                                            @else
                                                <span class="text-red">Belum Cair</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_bank.asset.js')
@endsection
