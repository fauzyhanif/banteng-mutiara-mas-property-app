@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Pekerjaan Mandor</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Laporan
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th>Nama Mandor</th>
                                <th width="20%">Spesialis</th>
                                <th width="15%" class="text-center">Total Pekerjaan</th>
                                <th width="15%" class="text-center">Belum Selesai</th>
                                <th width="15%" class="text-center">Selesai</th>
                            </thead>
                            <tbody>
                                @foreach ($foremans as $foreman)
                                    <tr>
                                        <td class="font-weight-bold">
                                            <a href="{{ url('reports/foreman_job/list_job?mandor_id='.$foreman->sdm_id) }}">
                                                {{ $foreman->sdm->name }}
                                            </a>
                                        </td>
                                        <td>{{ $foreman->specialist }}</td>
                                        <td class="text-center">
                                            @if (array_key_exists($foreman->sdm_id, $arr_data))
                                                {{ $arr_data[$foreman->sdm_id]['total'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($foreman->sdm_id, $arr_data))
                                                {{ $arr_data[$foreman->sdm_id]['blm_selesai'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($foreman->sdm_id, $arr_data))
                                                {{ $arr_data[$foreman->sdm_id]['selesai'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
