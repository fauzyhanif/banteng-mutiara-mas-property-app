@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Penjualan Cash</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Laporan
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered table-hover">
                            <thead>
                                <th width="25%">LOKASI</th>
                                <th width="25%">BLOK</th>
                                <th class="text-center">TERJUAL</th>
                                <th class="text-center">BELUM LUNAS</th>
                                <th class="text-center">SUDAH LUNAS</th>
                                @foreach ($steps as $step)
                                    <th class="text-center">{{ $step->name }}</th>
                                @endforeach
                            </thead>
                            <tbody>
                                @foreach ($blocks as $block)
                                    <tr>
                                        <td>{{ $block->location->name }}</td>
                                        <td>{{ $block->name }}</td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sold))
                                                <a href="{{ url('reports/cash_sales/list_unit?type=sold&location_id='.$block->location_id.'&block_id='.$block->block_id) }}">
                                                    {{ $res_sold[$block->location_id.'|'.$block->block_id]['sold'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sold))
                                                <a href="{{ url('reports/cash_sales/list_unit?type=blm_lunas&location_id='.$block->location_id.'&block_id='.$block->block_id) }}">
                                                    {{ $res_sold[$block->location_id.'|'.$block->block_id]['blm_lunas'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sold))
                                                <a href="{{ url('reports/cash_sales/list_unit?type=lunas&location_id='.$block->location_id.'&block_id='.$block->block_id) }}">
                                                    {{ $res_sold[$block->location_id.'|'.$block->block_id]['lunas'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        @foreach ($steps as $step)
                                            <td class="text-center">
                                                @if (array_key_exists($block->location_id.'|'.$block->block_id."|".$step->step_id, $res_step))
                                                    <a href="{{ url('reports/cash_sales/list_unit?type=step&location_id='.$block->location_id.'&block_id='.$block->block_id.'&step_id='.$step->step_id) }}">
                                                        {{ $res_step[$block->location_id.'|'.$block->block_id."|".$step->step_id] }} Unit
                                                    </a>
                                                @else
                                                    0
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{-- <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th>Keterangan</th>
                                <th class="text-center" width="15%">Banyak Unit</th>
                            </thead>
                            <tbody>
                                @foreach ($blocks as $block)
                                    <tr>
                                        <td class="bg-info font-weight-bold" colspan="2">{{ $block->name }}</td>
                                    </tr>
                                    <tr>
                                        <td><a href="{{ url('reports/cash_sales/list_unit?type=sold&block_id='.$block->block_id) }}">Jumlah Terjual</a></td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sold))
                                                {{ $res_sold[$block->block_id]['sold'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="{{ url('reports/cash_sales/list_unit?type=blm_lunas&block_id='.$block->block_id) }}">Belum Lunas</a></td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sold))
                                                {{ $res_sold[$block->block_id]['blm_lunas'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="{{ url('reports/cash_sales/list_unit?type=lunas&block_id='.$block->block_id) }}">Sudah Lunas</a></td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sold))
                                                {{ $res_sold[$block->block_id]['lunas'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    @foreach ($steps as $step)
                                        <tr>
                                            <td><a href="{{ url('reports/cash_sales/list_unit?type=step&block_id='.$block->block_id.'&step_id='.$step->step_id) }}">{{ $step->name }}</a></td>
                                            <td class="text-center">
                                                @if (array_key_exists($block->block_id."|".$step->step_id, $res_step))
                                                    {{ $res_step[$block->block_id."|".$step->step_id] }}
                                                @else
                                                    0
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
