@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('reports.hutang_piutang') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Laporan Hutang Piutang
            </a>
        </li>
        <li class="breadcrumb-item active">List Hutang Piutang</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Nama</td>
                                    <td>: {{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Instansi</td>
                                    <td>: {{ $user->institute }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">No Handphone</td>
                                    <td>: {{ $user->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Alamat</td>
                                    <td>: {{ $user->address }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-receipt"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Perusahaan Hutang</span>
                        <span class="info-box-number">
                            Rp {{ GeneralHelper::rupiah($perusahaan_hutang) }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="info-box">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-receipt"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Dia Hutang</span>
                        <span class="info-box-number">
                            Rp {{ GeneralHelper::rupiah($mereka_hutang) }}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Kasbon Mandor
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-info">
                                <th>Tgl Transaksi</th>
                                <th width="20%">Sisa Kasbon</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_kasbon as $kasbon)
                                    <tr>
                                        <td>{{ GeneralHelper::konversiTgl($kasbon->kasbon_date) }}</td>
                                        <td>{{ GeneralHelper::rupiah($kasbon->jumlah) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Operasional
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-info">
                                <th width="15%">Tgl Transaksi</th>
                                <th>Keterangan</th>
                                <th width="20%">Sisa Nominal</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_operasional as $operasional)
                                    <tr>
                                        <td>{{ GeneralHelper::konversiTgl($operasional->finance_date) }}</td>
                                        <td>{{ $operasional->finance_desc }}</td>
                                        <td>{{ GeneralHelper::rupiah($operasional->jumlah) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        penjualan Unit
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-info">
                                <th width="15%">Tgl Transaksi</th>
                                <th>Unit</th>
                                <th>Komitmen Pembayaran</th>
                                <th width="20%">Sisa Nominal</th>
                                <th width="10%">Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_penjualan as $penjualan)
                                    <tr>
                                        <td>{{ GeneralHelper::konversiTgl($penjualan['tgl_transaksi']) }}</td>
                                        <td>{{ $penjualan['unit'] }}</td>
                                        <td>{{ $penjualan['payment_commitment'] }}</td>
                                        <td>{{ GeneralHelper::rupiah($penjualan['sisa']) }}</td>
                                        <td>
                                            <a href="{{ url('prop/sales/detail', $penjualan['id']) }}" class="btn btn-xs btn-info" target="_blank">
                                                <i class="fas fa-eye"></i> Detail
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@include('prop.ref_bank.asset.js')
@endsection
