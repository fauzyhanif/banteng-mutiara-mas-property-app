@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Hutang Piutang</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3 id="total-mereka-hutang">0</h3>

                                <p>Mereka Hutang</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-md-6 col-sm-12+">
                        <!-- small box -->
                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3 id="total-perusahaan-hutang">0</h3>

                                <p>Perusahaan Hutang</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Kasbon Mandor
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" id="table-kasbon" style="width: 100%">
                            <thead class="bg-info">
                                <th>User</th>
                                <th>No Telp</th>
                                <th>Institusi</th>
                                <th width="20%">Mereka Hutang</th>
                                <th width="20%">Perusahaan Hutang</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Mereka Hutang</th>
                                <th width="50%">Total Perusahaan Hutang</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="table-kasbon-total-mereka-hutang"></td>
                                    <td id="table-kasbon-total-perusahaan-hutang"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Penjualan Unit Cash
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover datatable" id="table-penjualan-cash" style="width: 100%">
                            <thead class="bg-info">
                                <th>User</th>
                                <th>No Telp</th>
                                <th>Institusi</th>
                                <th width="15%">Terakhir Bayar</th>
                                <th width="15%">Mereka Hutang</th>
                                <th width="15%">Perusa. Hutang</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Mereka Hutang</th>
                                <th width="50%">Total Perusahaan Hutang</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="table-penjualan-total-mereka-hutang-cash"></td>
                                    <td id="table-penjualan-total-perusahaan-hutang-cash"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Penjualan Unit KPR (Cust)
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" id="table-penjualan-kpr-cust" style="width: 100%">
                            <thead class="bg-info">
                                <th>User</th>
                                <th>No Telp</th>
                                <th>Institusi</th>
                                <th width="15%">Terakhir Bayar</th>
                                <th width="15%">Mereka Hutang</th>
                                <th width="15%">Perusa. Hutang</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Mereka Hutang</th>
                                <th width="50%">Total Perusahaan Hutang</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="table-penjualan-total-mereka-hutang-kpr-cust"></td>
                                    <td id="table-penjualan-total-perusahaan-hutang-kpr-cust"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Penjualan Unit KPR (Bank)
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" id="table-penjualan-kpr-bank" style="width: 100%">
                            <thead class="bg-info">
                                <th>User</th>
                                <th>No Telp</th>
                                <th>Institusi</th>
                                <th width="20%">Mereka Hutang</th>
                                <th width="20%">Perusahaan Hutang</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Mereka Hutang</th>
                                <th width="50%">Total Perusahaan Hutang</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="table-penjualan-total-mereka-hutang-kpr-bank"></td>
                                    <td id="table-penjualan-total-perusahaan-hutang-kpr-bank"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Operasional
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable" id="table-operasional" style="width: 100%">
                            <thead class="bg-info">
                                <th>User</th>
                                <th>No Telp</th>
                                <th>Institusi</th>
                                <th width="20%">Mereka Hutang</th>
                                <th width="20%">Perusahaan Hutang</th>
                            </thead>
                        </table>

                        <table class="table table-sm table-bordered mt-3">
                            <thead class="bg-warning">
                                <th width="50%">Total Mereka Hutang</th>
                                <th width="50%">Total Perusahaan Hutang</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="table-operasional-total-mereka-hutang"></td>
                                    <td id="table-operasional-total-perusahaan-hutang"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
    sangkutan_total();
    sangkutan_kasbon();
    penjualan_cash();
    penjualan_kpr_cust();
    penjualan_kpr_bank();
    operasional();
});

function sangkutan_total() {
    var url = {!! json_encode(url('/reports/hutang_piutang/total')) !!};
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('#total-mereka-hutang').text(res.mereka_hutang);
            $('#total-perusahaan-hutang').text(res.perusahaan_hutang);
        }
    })
}

function sangkutan_kasbon() {
    // data table
    var url = {!! json_encode(url('/reports/hutang_piutang/kasbon'))!!};
    $('#table-kasbon').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'name_link', name: 'name_link', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'institute', name: 'institute', searchable: true },
            { data: 'mereka_hutang', name: 'mereka_hutang', searchable: true },
            { data: 'perusahaan_hutang', name: 'perusahaan_hutang', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/reports/hutang_piutang/kasbon_total')) !!};
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#table-kasbon-total-mereka-hutang').text(res.mereka_hutang);
            $('#table-kasbon-total-perusahaan-hutang').text(res.perusahaan_hutang);
        }
    })
}

function penjualan_cash() {
    // data table
    var url = {!! json_encode(url('/reports/hutang_piutang/penjualan_cash'))!!};
    $('#table-penjualan-cash').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'name_link', name: 'name_link', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'institute', name: 'institute', searchable: true },
            { data: 'html_tgl_bayar_terakhir', name: 'html_tgl_bayar_terakhir', searchable: true },
            { data: 'mereka_hutang', name: 'mereka_hutang', searchable: true },
            { data: 'perusahaan_hutang', name: 'perusahaan_hutang', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/reports/hutang_piutang/penjualan_total_cash')) !!};
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#table-penjualan-total-mereka-hutang-cash').text(res.mereka_hutang);
            $('#table-penjualan-total-perusahaan-hutang-cash').text(res.perusahaan_hutang);
        }
    })
}

function penjualan_kpr_cust() {
    // data table
    var url = {!! json_encode(url('/reports/hutang_piutang/penjualan_kpr/1'))!!};
    $('#table-penjualan-kpr-cust').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'name_link', name: 'name_link', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'institute', name: 'institute', searchable: true },
            { data: 'html_tgl_bayar_terakhir', name: 'html_tgl_bayar_terakhir', searchable: true },
            { data: 'mereka_hutang', name: 'mereka_hutang', searchable: true },
            { data: 'perusahaan_hutang', name: 'perusahaan_hutang', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/reports/hutang_piutang/penjualan_total_kpr/1')) !!};
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#table-penjualan-total-mereka-hutang-kpr-cust').text(res.mereka_hutang);
            $('#table-penjualan-total-perusahaan-hutang-kpr-cust').text(res.perusahaan_hutang);
        }
    })
}

function penjualan_kpr_bank() {
    // data table
    var url = {!! json_encode(url('/reports/hutang_piutang/penjualan_kpr/2'))!!};
    $('#table-penjualan-kpr-bank').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'name_link', name: 'name_link', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'institute', name: 'institute', searchable: true },
            { data: 'mereka_hutang', name: 'mereka_hutang', searchable: true },
            { data: 'perusahaan_hutang', name: 'perusahaan_hutang', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/reports/hutang_piutang/penjualan_total_kpr/2')) !!};
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#table-penjualan-total-mereka-hutang-kpr-bank').text(res.mereka_hutang);
            $('#table-penjualan-total-perusahaan-hutang-kpr-bank').text(res.perusahaan_hutang);
        }
    })
}

function operasional() {
    // data table
    var url = {!! json_encode(url('/reports/hutang_piutang/operasional'))!!};
    $('#table-operasional').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: url,

        columns: [
            { data: 'name_link', name: 'name_link', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'institute', name: 'institute', searchable: true },
            { data: 'mereka_hutang', name: 'mereka_hutang', searchable: true },
            { data: 'perusahaan_hutang', name: 'perusahaan_hutang', searchable: true },
        ]
    });

    // data total
    var url2 = {!! json_encode(url('/reports/hutang_piutang/operasional_total')) !!};
    $.ajax({
        type: 'GET',
        url: url2,
        dataType:'json',
        success: function (res) {
            $('#table-operasional-total-mereka-hutang').text(res.mereka_hutang);
            $('#table-operasional-total-perusahaan-hutang').text(res.perusahaan_hutang);
        }
    })
}
</script>
@endsection
