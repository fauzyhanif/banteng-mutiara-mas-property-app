@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('reports.unit') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Laporan Unit
            </a>
        </li>
        <li class="breadcrumb-item active">List Unit</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Unit Blok A</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered table-striped datatable">
                            <thead>
                                <th width="15%">Nomor Unit</th>
                                <th width="10%">Status</th>
                                <th>Konsumen</th>
                                <th width="15%">No Telp</th>
                                <th width="12%">Harga Jual</th>
                                <th width="12%">Uang Masuk</th>
                                <th width="12%">Sisa</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    var base = {!! json_encode(url('/reports/unit/list_unit_json?type='.$type."&location_id=".$location_id."&block_id=".$block_id)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'type_number', name: 'type_number', searchable: true },
            { data: 'status', name: 'status', searchable: true },
            { data: 'customer', name: 'customer', searchable: true },
            { data: 'customer_phone', name: 'customer_phone', searchable: true },
            { data: 'selling_price', name: 'selling_price', searchable: true },
            { data: 'ttl_trnsct_paid', name: 'ttl_trnsct_paid', searchable: true },
            { data: 'sisa', name: 'sisa', searchable: true },
        ]
    });
});
</script>
@endsection
