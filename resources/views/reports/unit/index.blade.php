@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Unit</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Laporan
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Lokasi</th>
                                <th>Blok</th>
                                <th class="text-center">Jumlah Unit</th>
                                <th class="text-center">Unit Tersedia</th>
                                <th class="text-center">Unit Terjual</th>
                                <th class="text-center">Unit Progress Pembangunan</th>
                            </thead>
                            <tbody>
                                @foreach ($blocks as $block)
                                    <tr>
                                        <td>{{ $block->location->name }}</td>
                                        <td>{{ $block->name }}</td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sales_status))
                                                <a href="{{ url('/reports/unit/list_unit?type=all&location_id='. $block->location_id .'&block_id=' . $block->block_id) }}">
                                                    {{ $res_sales_status[$block->location_id.'|'.$block->block_id]['total'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sales_status))
                                                <a href="{{ url('/reports/unit/list_unit?type=READY&location_id='. $block->location_id .'&block_id=' . $block->block_id) }}">
                                                    {{ $res_sales_status[$block->location_id.'|'.$block->block_id]['ready'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sales_status))
                                                <a href="{{ url('/reports/unit/list_unit?type=SOLD&location_id='. $block->location_id .'&block_id=' . $block->block_id) }}">
                                                    {{ $res_sales_status[$block->location_id.'|'.$block->block_id]['sold'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->location_id.'|'.$block->block_id, $res_sales_status))
                                                <a href="{{ url('/reports/unit/list_unit?type=ON_PROGRESS&location_id='. $block->location_id .'&block_id=' . $block->block_id) }}">
                                                    {{ $res_development_status[$block->location_id.'|'.$block->block_id]['blm_selesai'] }} Unit
                                                </a>
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th>Keterangan</th>
                                <th class="text-center" width="15%">Banyak Unit</th>
                            </thead>
                            <tbody>
                                @foreach ($blocks as $block)

                                    <tr>
                                        <td class="bg-info font-weight-bold" colspan="2">Blok {{ $block->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{{ url('/reports/unit/list_unit?type=all&block_id=' . $block->block_id) }}">
                                                Jumlah Unit
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sales_status))
                                                {{ $res_sales_status[$block->block_id]['total'] }}
                                            @else
                                            0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{{ url('/reports/unit/list_unit?type=READY&block_id=' . $block->block_id) }}">
                                                Jumlah Unit Tersedia
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sales_status))
                                                {{ $res_sales_status[$block->block_id]['ready'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{{ url('/reports/unit/list_unit?type=BOOKING&block_id=' . $block->block_id) }}">
                                                Jumlah Unit Terbooking
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sales_status))
                                                {{ $res_sales_status[$block->block_id]['booking'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{{ url('/reports/unit/list_unit?type=SOLD&block_id=' . $block->block_id) }}">
                                                Jumlah Unit Terjual
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_sales_status))
                                                {{ $res_sales_status[$block->block_id]['sold'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{{ url('/reports/unit/list_unit?type=ON_PROGRESS&block_id=' . $block->block_id) }}">
                                                Jumlah Unit Dalam Pembangunan
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if (array_key_exists($block->block_id, $res_development_status))
                                                {{ $res_development_status[$block->block_id]['blm_selesai'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
