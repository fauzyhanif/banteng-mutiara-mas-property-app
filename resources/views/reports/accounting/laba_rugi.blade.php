@extends('index')
@section('content')

<input type="hidden" name="start_date" value="{{ $start_date }}">
<input type="hidden" name="end_date" value="{{ $end_date }}">

<section class="content-header">
    <h1>Laporan Akuntansi - Laba Rugi</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <form action="{{ route('reports.laporan_akuntansi.laba_rugi.detail') }}" method="POST">
            @csrf
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-8 col-sm-12">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="text" name="date" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Jenis Laporan</label>
                                <select name="jenis_laporan" class="form-control">
                                    <option>PAJAK</option>
                                    <option>INHOUSE/BANK</option>
                                </select>
                            </div>

                            <hr>

                            <button type="submit" class="btn btn-block btn-primary">LIHAT</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-8 col-sm-12">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="m-0">Lihat Laporan Lain</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="{{ route('reports.laporan_akuntansi.neraca')}}">Neraca</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{{ route('finance.asset.print')}}" target="_blank">Aset Perusahaan</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script>
    $(document).ready(function() {
    var start_date = $('input[name="start_date"]').val();
    var end_date = $('input[name="end_date"]').val();
    $('input[name="date"]').daterangepicker({
        startDate: start_date,
        endDate: end_date,
		locale : {
			format : 'YYYY-MM-DD'
		}
    });
});
</script>
@endsection
