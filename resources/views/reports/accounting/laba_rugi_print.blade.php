<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Laporan Akuntansi - {{ $jenis_laporan }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
        <style>
            @page {
                size: A4
            }
        </style>
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    {{-- heading --}}
                    <div class="row">
                        <div class="col-12">
                            <p class="font-weight-bold my-0">PERHITUNGAN LABA / RUGI ({{ $jenis_laporan }})</p>
                            <p class="font-weight-bold my-0">PT. LAN SENA JAYA</p>
                            <p class="font-weight-bold my-0">NPWP : 76.273.251.9-409.000</p>
                            <p class="font-weight-bold my-0">PERIODE {{ strtoupper(GeneralHelper::konversiTgl($start_date, 'ttd')) }} - {{ strtoupper(GeneralHelper::konversiTgl($end_date, 'ttd')) }}</p>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-12">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <th width="3%">I</th>
                                        <th colspan="4">PEREDARAN USAHA</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Penjualan Rumah Subsidi</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">{{ GeneralHelper::rupiah($penjualan['jml_penjualan_rumah_subsidi']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Penjualan Rumah Non Subsidi</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">{{ GeneralHelper::rupiah($penjualan['jml_penjualan_rumah_nonsubsidi']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2" class="text-right">Jumlah</td>
                                        <td width="5%" class="text-right border-top">Rp</td>
                                        <td width="20%" class="text-right border-top">{{ GeneralHelper::rupiah($penjualan['jml_penjualan_rumah_subsidi'] + $penjualan['jml_penjualan_rumah_nonsubsidi']) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-12">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <th width="3%">II</th>
                                        <th colspan="4">HARGA POKOK PENJUALAN</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Persediaan Awal</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">{{ GeneralHelper::rupiah($hpp['persediaan_awal']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Pengadaan Material</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">{{ GeneralHelper::rupiah($hpp['pengadaan_material']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2" class="text-right">Jumlah</td>
                                        <td width="5%" class="text-right border-top">Rp</td>
                                        <td width="20%" class="text-right border-top">{{ GeneralHelper::rupiah($hpp['persediaan_awal'] + $hpp['pengadaan_material']) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Persediaan Akhir</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">{{ GeneralHelper::rupiah($persediaan_akhir) }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2" class="text-right">Jumlah</td>
                                        <td width="5%" class="text-right border-top">Rp</td>
                                        <td width="20%" class="text-right border-top">
                                            {{ GeneralHelper::rupiah(($hpp['persediaan_awal'] + $hpp['pengadaan_material']) - $persediaan_akhir) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2"><b>Penghasilan Bruto</b></td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">
                                            @php
                                                $penghasilan_bruto = ($penjualan['jml_penjualan_rumah_subsidi'] + $penjualan['jml_penjualan_rumah_nonsubsidi']) - (($hpp['persediaan_awal'] + $hpp['pengadaan_material']) - $persediaan_akhir);
                                            @endphp
                                            {{ GeneralHelper::rupiah($penghasilan_bruto) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-12">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <th width="3%">III</th>
                                        <th colspan="4">BIAYA UMUM DAN ADMINISTRASI</th>
                                    </tr>
                                    @php $total_biaya = 0; @endphp
                                    @foreach ($biaya as $item)
                                        <tr>
                                            <td></td>
                                            <td colspan="2">{{ $item['account_name'] }}</td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">{{ GeneralHelper::rupiah($item['account_amount']) }}</td>
                                        </tr>
                                    @php $total_biaya += $item['account_amount']; @endphp
                                    @endforeach

                                    <tr>
                                        <td></td>
                                        <td colspan="2" class="text-right">Jumlah</td>
                                        <td width="5%" class="text-right border-top">Rp</td>
                                        <td width="20%" class="text-right border-top">{{ GeneralHelper::rupiah($total_biaya) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-12">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width="3%"></td>
                                        <td><b>Penghasilan Neto</b></td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">
                                            <b>{{ GeneralHelper::rupiah($penghasilan_bruto - $total_biaya) }}</b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>
</html>
