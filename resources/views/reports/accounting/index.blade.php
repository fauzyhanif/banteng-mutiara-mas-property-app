@extends('index')
@section('content')
<section class="content-header">
    <h1>Laporan Akuntansi</h1>
</section>

<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-4">
                <div class="card shadow-none">
                    <div class="card-body">
                        <h4 class="font-weight-bold">Laporan Laba Rugi</h4>

                        <p>Lihat laporan laba rugi per periode disini.</p>

                        <a href="{{ url('reports/laporan_akuntansi/laba_rugi') }}" class="btn btn-primary mt-3">
                            <i class="fas fa-file"></i> Lihat Laporan
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card shadow-none">
                    <div class="card-body">
                        <h4 class="font-weight-bold">Laporan Neraca</h4>

                        <p>Lihat laporan neraca per periode disini.</p>

                        <a href="{{ route('reports.laporan_akuntansi.neraca')}}" class="btn btn-primary mt-3">
                            <i class="fas fa-file"></i> Lihat Laporan
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card shadow-none">
                    <div class="card-body">
                        <h4 class="font-weight-bold">Laporan Aset Perusahaan</h4>

                        <p>Lihat laporan aset perusahaan.</p>

                        <a href="{{ route('finance.asset.print')}}" class="btn btn-primary mt-3" target="_blank">
                            <i class="fas fa-file"></i> Lihat Laporan
                        </a>
                    </div>
                </div>
            </div>
        </div>

        {{-- <form action="{{ route('reports.laporan_akuntansi.print') }}" method="POST" target="_blank">
            @csrf
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-8 col-sm-12">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="text" name="date" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Jenis Laporan</label>
                                <select name="jenis_laporan" class="form-control">
                                    <option>PAJAK</option>
                                    <option>INHOUSE/BANK</option>
                                </select>
                            </div>

                            <hr>

                            <button type="submit" class="btn btn-block btn-primary">CETAK</button>
                        </div>
                    </div>
                </div>
            </div>
        </form> --}}
    </div>
</section>

<script>
$(document).ready(function() {
    var start_date = $('input[name="start_date"]').val();
    var end_date = $('input[name="end_date"]').val();
    $('input[name="date"]').daterangepicker({
        startDate: start_date,
        endDate: end_date,
		locale : {
			format : 'YYYY-MM-DD'
		}
    });
});
</script>
@endsection
