@extends('index')
@section('content')
<section class="content-header">
    <h1>Detail Transaksi</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center mb-3">
            <div class="col-md-12">
                <div class="border border-info">
                    <div class="py-2">
                        <table class="table table-borderless table-sm">
                            <tr>
                                <td width="25%">Data</td>
                                <td width="3%" class="text-right">:</td>
                                <td width="73%">Persediaan Akhir</td>
                            </tr>
                            <tr>
                                <td width="25%">Jenis Laporan</td>
                                <td width="3%" class="text-right">:</td>
                                <td width="73%">{{ $jenis_laporan }}</td>
                            </tr>
                            <tr>
                                <td width="25%">Periode</td>
                                <td width="3%" class="text-right">:</td>
                                <td width="73%">{{ strtoupper(GeneralHelper::konversiTgl($start_date, 'ttd')) }} - {{ strtoupper(GeneralHelper::konversiTgl($end_date, 'ttd')) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Data</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered table-hover" style="overflow-x: scroll">
                            <thead class="bg-info">
                                <th>Tgl Transaksi</th>
                                <th>Unit</th>
                                <th>Type</th>
                                <th>Konsumen</th>
                                <th class="text-right">Harga</th>
                            </thead>
                            <tbody>
                                @php $ttl_cash = 0 @endphp
                                @foreach ($data as $item_cash)
                                <tr>
                                    <td>{{ GeneralHelper::konversiTgl($item_cash->sales_date, 'ttd') }}</td>
                                    <td>{{ $item_cash->block_name . ' No ' . $item_cash->unit_id }}</td>
                                    <td>{{ $item_cash->type_name }}</td>
                                    <td>{{ $item_cash->sdm_name }}</td>
                                    <td class="text-right">
                                        @if ($jenis_laporan == 'PAJAK')
                                            {{ GeneralHelper::rupiah($hpp_pajak) }}
                                            @php $ttl_cash += $hpp_pajak @endphp
                                        @else
                                            {{ GeneralHelper::rupiah($item_cash->ttl_trnsct) }}
                                            @php $ttl_cash += $item_cash->ttl_trnsct @endphp
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="4" class="text-right">Total</th>
                                <th class="text-right">{{ GeneralHelper::rupiah($ttl_cash) }}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
