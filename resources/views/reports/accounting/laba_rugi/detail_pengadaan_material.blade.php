@extends('index')
@section('content')
<section class="content-header">
    <h1>Detail Transaksi</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center mb-3">
            <div class="col-md-12">
                <div class="border border-info">
                    <div class="py-2">
                        <table class="table table-borderless table-sm">
                            <tr>
                                <td width="25%">Data</td>
                                <td width="3%" class="text-right">:</td>
                                <td width="73%">{{ str_replace("_", " ", $type) }}</td>
                            </tr>
                            <tr>
                                <td width="25%">Jenis Laporan</td>
                                <td width="3%" class="text-right">:</td>
                                <td width="73%">{{ $jenis_laporan }}</td>
                            </tr>
                            <tr>
                                <td width="25%">Periode</td>
                                <td width="3%" class="text-right">:</td>
                                <td width="73%">{{ strtoupper(GeneralHelper::konversiTgl($start_date, 'ttd')) }} - {{ strtoupper(GeneralHelper::konversiTgl($end_date, 'ttd')) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Data Transaksi</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered table-hover" style="overflow-x: scroll">
                            <thead class="bg-info">
                                <th width="15%">Tgl Transaksi</th>
                                <th width="25%">Diberikan kepada</th>
                                <th>Keterangan</th>
                                <th width="15%" class="text-right">Nominal</th>
                            </thead>
                            <tbody>
                                @php $total = 0 @endphp
                                @foreach ($data as $item)
                                    <tr>
                                        <td>{{ GeneralHelper::konversiTgl($item->finance_date, 'ttd') }}</td>
                                        <td>{{ $item->sdm_name }}</td>
                                        <td>{{ $item->finance_desc }}</td>
                                        <td class="text-right">
                                            {{ GeneralHelper::rupiah($item->finance_item_amount) }}
                                        </td>
                                    </tr>
                                @php $total += $item->finance_item_amount @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="3" class="text-right">Total</th>
                                <th class="text-right">{{ GeneralHelper::rupiah($total) }}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
