@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('reports/laporan_akuntansi/laba_rugi') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Laba Rugi
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Laba Rugi</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="bg-info">
                    <h5 class="py-3 px-2">
                        Laporan {{ $jenis_laporan }} periode {{ strtoupper(GeneralHelper::konversiTgl($start_date, 'ttd')) }} - {{ strtoupper(GeneralHelper::konversiTgl($end_date, 'ttd')) }}
                    </h5>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" style="overflow-x: scroll">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail</h5>

                        <div class="card-tools">
                            <form action="{{ route('reports.laporan_akuntansi.laba_rugi.print') }}" method="POST" target="_blank">
                                @csrf
                                <input type="hidden" name="jenis_laporan" value="{{ $jenis_laporan }}">
                                <input type="hidden" name="date" value="{{ $start_date . ' - ' . $end_date }}">

                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="fas fa-print"></i> CETAK
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <th width="3%">I</th>
                                            <th colspan="4">PEREDARAN USAHA</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <a href="{{ url('reports/laporan_akuntansi/laba_rugi/detail_transaction?type=PENJUALAN_RUMAH_SUBSIDI&jenis_laporan='.$jenis_laporan.'&start_date='.$start_date.'&end_date='.$end_date) }}" target="_blank">
                                                    Penjualan Rumah Subsidi
                                                </a>
                                            </td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">
                                                {{ GeneralHelper::rupiah($penjualan['jml_penjualan_rumah_subsidi']) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <a href="{{ url('reports/laporan_akuntansi/laba_rugi/detail_transaction?type=PENJUALAN_RUMAH_NONSUBSIDI&jenis_laporan='.$jenis_laporan.'&start_date='.$start_date.'&end_date='.$end_date) }}"
                                                    target="_blank">
                                                    Penjualan Rumah Non Subsidi
                                                </a>
                                            </td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">
                                                {{ GeneralHelper::rupiah($penjualan['jml_penjualan_rumah_nonsubsidi']) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2" class="text-right">Jumlah</td>
                                            <td width="5%" class="text-right border-top">Rp</td>
                                            <td width="20%" class="text-right border-top">
                                                {{ GeneralHelper::rupiah($penjualan['jml_penjualan_rumah_subsidi'] + $penjualan['jml_penjualan_rumah_nonsubsidi']) }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-12">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <th width="3%">II</th>
                                            <th colspan="4">HARGA POKOK PENJUALAN</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <a href="{{ url('reports/laporan_akuntansi/laba_rugi/detail_transaction?type=PERSEDIAAN_AWAL&jenis_laporan='.$jenis_laporan.'&start_date='.$start_date.'&end_date='.$end_date) }}"
                                                    target="_blank">
                                                    Persediaan Awal
                                                </a>
                                            </td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">{{ GeneralHelper::rupiah($hpp['persediaan_awal']) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <a href="{{ url('reports/laporan_akuntansi/laba_rugi/detail_transaction?type=PENGADAAN_MATERIAL&jenis_laporan='.$jenis_laporan.'&start_date='.$start_date.'&end_date='.$end_date) }}"
                                                    target="_blank">
                                                    Pengadaan Material
                                                </a>
                                            </td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">{{ GeneralHelper::rupiah($hpp['pengadaan_material']) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2" class="text-right">Jumlah</td>
                                            <td width="5%" class="text-right border-top">Rp</td>
                                            <td width="20%" class="text-right border-top">
                                                {{ GeneralHelper::rupiah($hpp['persediaan_awal'] + $hpp['pengadaan_material']) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <a href="{{ url('reports/laporan_akuntansi/laba_rugi/detail_transaction?type=PERSEDIAAN_AKHIR_PENJUALAN_KPR&jenis_laporan='.$jenis_laporan.'&start_date='.$start_date.'&end_date='.$end_date) }}"
                                                    target="_blank">
                                                    Persediaan Akhir
                                                </a>
                                            </td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">{{ GeneralHelper::rupiah($persediaan_akhir) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2" class="text-right">Jumlah</td>
                                            <td width="5%" class="text-right border-top">Rp</td>
                                            <td width="20%" class="text-right border-top">
                                                {{ GeneralHelper::rupiah(($hpp['persediaan_awal'] + $hpp['pengadaan_material']) - $persediaan_akhir) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2"><b>Penghasilan Bruto</b></td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">
                                                @php
                                                    $penghasilan_bruto = ($penjualan['jml_penjualan_rumah_subsidi'] + $penjualan['jml_penjualan_rumah_nonsubsidi']) - (($hpp['persediaan_awal'] + $hpp['pengadaan_material']) - $persediaan_akhir);
                                                @endphp
                                                {{ GeneralHelper::rupiah($penghasilan_bruto) }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-12">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <th width="3%">III</th>
                                            <th colspan="4">BIAYA UMUM DAN ADMINISTRASI</th>
                                        </tr>
                                        @php $total_biaya = 0; @endphp
                                        @foreach ($biaya as $item => $value)
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <a href="{{ url('reports/laporan_akuntansi/laba_rugi/detail_transaction?type=BIAYA&account_id='.$item.'&start_date='.$start_date.'&end_date='.$end_date) }}"
                                                    target="_blank">
                                                    {{ $value['account_name'] }}
                                                </a>
                                            </td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">{{ GeneralHelper::rupiah($value['account_amount']) }}</td>
                                        </tr>
                                        @php $total_biaya += $value['account_amount']; @endphp
                                        @endforeach

                                        <tr>
                                            <td></td>
                                            <td colspan="2" class="text-right">Jumlah</td>
                                            <td width="5%" class="text-right border-top">Rp</td>
                                            <td width="20%" class="text-right border-top">{{ GeneralHelper::rupiah($total_biaya) }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="col-12">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td width="3%"></td>
                                            <td><b>Penghasilan Neto</b></td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">
                                                <b>{{ GeneralHelper::rupiah($penghasilan_bruto - $total_biaya) }}</b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@include('finance.ref_asset.asset.js')
@endsection
