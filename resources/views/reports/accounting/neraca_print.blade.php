<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Laporan Akuntansi - Neraca</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
        <style>
            @page {
                size: A4
            }
        </style>
    </head>


    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">

                {{-- heading --}}
                <div class="row">
                    <div class="col-12">
                        <p class="font-weight-bold my-0">NERACA</p>
                        <p class="font-weight-bold my-0">PT. LAN SENA JAYA</p>
                        <p class="font-weight-bold my-0">NPWP : 76.273.251.9-409.000</p>
                        <p class="font-weight-bold my-0">PERIODE
                            {{ strtoupper(GeneralHelper::konversiTgl($start_date, 'ttd')) }} -
                            {{ strtoupper(GeneralHelper::konversiTgl($end_date, 'ttd')) }}</p>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-12">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <th colspan="5">AKTIVA</th>
                                </tr>
                                <tr>
                                    <th colspan="5">Aktiva Lancar :</th>
                                </tr>
                                <tr>
                                    <td width="2%"></td>
                                    <td colspan="2">Kas & Setara Kas</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($total_kas) }}</td>
                                </tr>
                                @if ($jenis_laporan == 'INHOUSE/BANK')
                                    @foreach ($kas as $item)
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>{{ $item['cash_name'] }}</td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="20%" class="text-right">{{ GeneralHelper::rupiah($item['cash_masuk'] - $item['cash_keluar']) }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td></td>
                                    <td colspan="2">Persediaan Bangunan</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($persediaan_bangunan) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2">Persediaan Tanah</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($persediaan_tanah) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2">Piutang Usaha</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($piutang_usaha) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2">Piutang Lain-lain</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($piutang_lain_lain) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2">Uang Muka</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($uang_muka) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2">Lain-lain/Pajak dibayar dimuka</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($lain_lain) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2" class="text-right">Jumlah</td>
                                    <td width="5%" class="text-right border-top">Rp</td>
                                    <td width="20%" class="text-right border-top">
                                        {{ GeneralHelper::rupiah($total_aktiva_lancar) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-12">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <th colspan="5">Aktiva Tetap :</th>
                                </tr>
                                @php $total_aset = 0; @endphp
                                @foreach ($aktiva_tetap['assets'] as $asset)
                                    <tr>
                                        <td></td>
                                        <td colspan="2">{{ $asset['asset_name'] }}</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="20%" class="text-right">{{ GeneralHelper::rupiah($asset['asset_price']) }}</td>
                                    </tr>
                                @php $total_aset += $asset['asset_price']; @endphp
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td colspan="2">Jumlah</td>
                                    <td width="5%" class="text-right border-top">Rp</td>
                                    <td width="20%" class="text-right border-top">{{ GeneralHelper::rupiah($total_aset) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2">Akumulasi Penyusutan</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($aktiva_tetap['depreciation']) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2" class="text-right">Jumlah</td>
                                    <td width="5%" class="text-right border-top">Rp</td>
                                    <td width="20%" class="text-right border-top">
                                        {{ GeneralHelper::rupiah($total_aset - $aktiva_tetap['depreciation']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                    <th colspan="4">Total Aktiva</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah(($total_aset - $aktiva_tetap['depreciation']) + $total_aktiva_lancar) }}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-12">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <th colspan="5">PASIVA</th>
                                </tr>
                                <tr>
                                    <th colspan="5">Kewajiban dan Modal :</th>
                                </tr>
                                <tr>
                                    <td width="2%"></td>
                                    <td colspan="2">Hutang Bank</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($hutang_bank) }}</td>
                                </tr>
                                <tr>
                                    <td width="2%"></td>
                                    <td colspan="2">Hutang Usaha</td>
                                    <td width="5%" class="text-right">Rp</td>
                                    <td width="20%" class="text-right">{{ GeneralHelper::rupiah($hutang_usaha) }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2" class="text-right">Jumlah</td>
                                    <td width="5%" class="text-right border-top">Rp</td>
                                    <td width="20%" class="text-right border-top">
                                        {{ GeneralHelper::rupiah($hutang_bank + $hutang_usaha) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
