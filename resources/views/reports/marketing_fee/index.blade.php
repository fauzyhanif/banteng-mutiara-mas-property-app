@extends('index')

@section('content')
<section class="content-header">
    <h1>Marketing Fee</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Laporan
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th width="12%" width="10%">No Marketer</th>
                                <th>Nama Marketer</th>
                                <th width="15%">Total Penjualan</th>
                                <th class="text-right" width="12%">Total Komisi</th>
                                <th class="text-right" width="12%">Komisi Cair</th>
                                <th class="text-right" width="12%">Sisa</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_marketer as $marketer)
                                    <tr>
                                        <td>
                                            <a href="{{ url('/reports/marketing_fee/detail?affiliate_num=' . $marketer->affiliate_num) }}">
                                                {{ $marketer->affiliate_num }}
                                            </a>
                                        </td>
                                        <td>{{ $marketer->sdm->name }}</td>
                                        <td>
                                            @if (array_key_exists($marketer->affiliate_num, $res_trnsct))
                                                {{ $res_trnsct[$marketer->affiliate_num]['ttl_sales'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if (array_key_exists($marketer->affiliate_num, $res_trnsct))
                                                {{ GeneralHelper::rupiah($res_trnsct[$marketer->affiliate_num]['affiliate_fee']) }}
                                            @else
                                                0,00
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if (array_key_exists($marketer->affiliate_num, $res_trnsct))
                                                {{ GeneralHelper::rupiah($res_trnsct[$marketer->affiliate_num]['affiliate_fee_paid']) }}
                                            @else
                                                0,00
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if (array_key_exists($marketer->affiliate_num, $res_trnsct))
                                                {{ GeneralHelper::rupiah($res_trnsct[$marketer->affiliate_num]['sisa']) }}
                                            @else
                                                0,00
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
