@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('reports.marketing_fee') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Laporan Marketing Fee
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Marketing Fee</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Nama / No Marketer</td>
                                    <td>: {{ $marketer->sdm->name }} / {{ $marketer->affiliate_num }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Rekening</td>
                                    <td>: Bank {{ $marketer->sdm->rek_bank }} {{ $marketer->sdm->rek_number }} A/N {{ $marketer->sdm->rek_name }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">No Handphone</td>
                                    <td>: {{ $marketer->sdm->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" width="25%">Alamat</td>
                                    <td>: {{ $marketer->sdm->address }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Penjualan</span>
                        <span class="info-box-number">
                            {{ $res_infografis['ttl_sales'] }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-comments-dollar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Komisi</span>
                        <span class="info-box-number">
                            Rp {{ GeneralHelper::rupiah($res_infografis['affiliate_fee']) }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-sign-in-alt"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Komisi Dibayar</span>
                        <span class="info-box-number">
                            Rp {{ GeneralHelper::rupiah($res_infografis['affiliate_fee_paid']) }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-wallet"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Sisa Komisi</span>
                        <span class="info-box-number">
                            Rp {{ GeneralHelper::rupiah($res_infografis['sisa']) }}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Daftar Penjualan</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered">
                            <thead class="bg-info">
                                <th>Type & Nomor Unit</th>
                                <th>Konsumen</th>
                                <th>Komisi</th>
                                <th>Komisi Cair</th>
                                <th>Sisa Komisi</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_trnsct as $trnsct)
                                    <tr>
                                        <td>{{ $trnsct->unit->unitType->name }} &bullet; {{ $trnsct->unit_id }}</td>
                                        <td>{{ $trnsct->customer->name }} &bullet; {{ $trnsct->customer->institute }}</td>
                                        <td>{{ GeneralHelper::rupiah($trnsct->affiliate_fee) }}</td>
                                        <td>{{ GeneralHelper::rupiah($trnsct->affiliate_fee_paid) }}</td>
                                        <td>{{ GeneralHelper::rupiah($trnsct->affiliate_fee - $trnsct->affiliate_fee_paid) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@include('prop.ref_bank.asset.js')
@endsection
