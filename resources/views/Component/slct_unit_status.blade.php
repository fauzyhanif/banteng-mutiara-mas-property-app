<select name="unittype_id" class="form-control" @if ($required == 'Y') selected @endif>
    <option value="">** Pilih Status</option>
    @foreach ($datas as $data)
        <option value="{{ $data->unitstatus_id }}" {{ ($id == $data->unitstatus_id) ? 'selected' : '' }}>
            {{ $data->name }}
        </option>
    @endforeach
</select>
