<option value="">-- Pilih Unit --</option>
@foreach ($datas as $data)
    <option value="{{ $data->number }}" {{ ($id == $data->unit_id) ? 'selected' : '' }}>
        Type {{ $data->unitType->name }} Nomor {{ $data->number }} Harga Cash {{ GeneralHelper::rupiah($data->harga_jual_cash) }} Harga KPR {{ GeneralHelper::rupiah($data->harga_jual_kpr) }}
    </option>
@endforeach
