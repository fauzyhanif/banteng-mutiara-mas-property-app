<option value="">** Pilih Mandor</option>
@foreach ($datas as $data)
    <option value="{{ $data->sdm_id }}" {{ ($id == $data->sdm_id) ? 'selected' : '' }}>
        {{ $data->sdm->name }} ({{ $data->specialist }})
    </option>
@endforeach
