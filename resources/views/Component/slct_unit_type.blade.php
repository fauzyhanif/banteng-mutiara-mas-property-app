<select name="unittype_id" class="form-control" @if ($required == 'Y') selected @endif>
    <option value="">** Pilih Type Unit</option>
    @foreach ($datas as $data)
        <option value="{{ $data->unittype_id }}" {{ ($id == $data->unittype_id) ? 'selected' : '' }}>
            {{ $data->name }}
        </option>
    @endforeach
</select>
