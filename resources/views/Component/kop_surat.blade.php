
<div class="row kop-surat">
    <div class="col-md-2 col-print-2">
        <img src="{{ url('public/img/logo_bmm.jpeg') }}" style="max-width: 80px; margin-top: -10px; margin-left: 70px" alt="" class="logo-kop">
    </div>
    <div class="col-md-8 col-print-8 text-center">
        <h4><b>PT LAN SENA JAYA</b></h4>
        <p style="margin-top: -10px; font-size: 11px">DEVELOPER & CONTRACTOR</p>
        <p style="margin-top: -17px; font-size: 11px">
            Perum Benteng Mutiara Mas Ruko No. 16  Babakan Situ 004/002
            <br>
            Desa Benteng Kec. Cempaka Kab. Purwakarta  (0264) - 8308450 Jawa Barat 41181
        </p>
    </div>
    <div class="col-md-2 col-print-8"></div>
</div>

<hr style="margin-top: -5px; border-bottom: 3px solid rgb(53, 53, 200)">
