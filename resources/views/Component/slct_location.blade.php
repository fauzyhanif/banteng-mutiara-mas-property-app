<option value="">** Pilih Lokasi</option>
@foreach ($datas as $data)
    <option value="{{ $data->location_id }}" {{ ($id == $data->location_id) ? 'selected' : '' }}>
        {{ $data->name }}
    </option>
@endforeach
