@extends('marketer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/marketer_dashboard') }}">Dashboard Marketer</a></li>
                    <li class="breadcrumb-item active">Affiliasi</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title">Data Pribadi Affiliasi</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td width="30%" class="text-secondary">Nama</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $affiliate->sdm->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">No Affilasi</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $affiliate->affiliate_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">No Handphone</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $affiliate->sdm->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Email</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $affiliate->sdm->email }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Alamat</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $affiliate->sdm->address }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>{{ $jml_penjualan }}</h3>

                        <p>Jumlah Penjualan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ $jml_penjualan_booking }}</h3>

                        <p>Penjualan Tahap Booking</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{ $jml_penjualan_akad }}</h3>

                        <p>Penjualan Sudah Akad</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Data Penjualan</h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="rounded border" style="background-color: #B9FFF8; font-size: 20px">
                                    <div class="px-2 py-2">
                                        <b>Penting!</b> Hak marketer akan dicairkan apabila data booking sudah diterima dan diproses oleh admin
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered table-hover table-sm">
                            <thead class="bg-info">
                                <th>Tgl Booking</th>
                                <th>Konsumen</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Hak Marketer</th>
                                <th>Hak Terbayarkan</th>
                                <th width="10%">Detail</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_penjualan as $item)
                                <tr>
                                    <td>{{ $item->sales_date }}</td>
                                    <td>{{ $item->customer->name }}</td>
                                    <td>Blok {{ $item->block->name }} No {{ $item->unit_id }}</td>
                                    <td>BOOKING</td>
                                    <td>{{ GeneralHelper::rupiah($item->affiliate_fee) }}</td>
                                    <td>{{ GeneralHelper::rupiah($item->affiliate_fee_paid) }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{ url('marketer_dashboard/booking_online/detail', $item->id) }}">
                                            <i class="fas fa-eye"></i> Detail
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="mt-3">
                            {{ $dt_penjualan->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
