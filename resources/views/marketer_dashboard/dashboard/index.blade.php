@extends('marketer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/marketer_dashboard') }}">Dashboard Marketer</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12">
                <div class="rounded border" style="background-color: #FFF9CA; font-size: 20px">
                    <div class="px-2 py-2">
                        Halo <b>{{ Session::get('auth_nama') }}</b>, Selamat datang kembali di sistem kami.
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>{{ $jml_penjualan }}</h3>

                        <p>Jumlah Penjualan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ $jml_penjualan_booking }}</h3>

                        <p>Penjualan Tahap Booking</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{ $jml_penjualan_akad }}</h3>

                        <p>Penjualan Sudah Akad</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">5 Penjualan Terakhir</h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="rounded border" style="background-color: #B9FFF8; font-size: 20px">
                                    <div class="px-2 py-2">
                                        <b>Penting!</b> Hak marketer akan dicairkan apabila data booking sudah diterima dan diproses oleh admin
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered table-hover table-sm">
                            <thead class="bg-info">
                                <th width="5%">No</th>
                                <th>Tgl Booking</th>
                                <th>Konsumen</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Hak Marketer</th>
                                <th>Hak Terbayarkan</th>
                                <th width="10%">Detail</th>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @foreach ($dt_penjualan_terakhir as $item)
                                <tr>
                                    <td>{{ $no }}.</td>
                                    <td>{{ $item->sales_date }}</td>
                                    <td>{{ $item->customer->name }}</td>
                                    <td>Blok {{ $item->block->name }} No {{ $item->unit_id }}</td>
                                    <td>BOOKING</td>
                                    <td>{{ GeneralHelper::rupiah($item->affiliate_fee) }}</td>
                                    <td>{{ GeneralHelper::rupiah($item->affiliate_fee_paid) }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{ url('marketer_dashboard/booking_online/detail', $item->id) }}">
                                            <i class="fas fa-eye"></i> Detail
                                        </a>
                                    </td>
                                </tr>
                                @php $no += 1; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
