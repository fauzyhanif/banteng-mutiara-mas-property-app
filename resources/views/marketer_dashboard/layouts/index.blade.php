@include('marketer_dashboard.layouts.head')
@include('marketer_dashboard.layouts.navbar')
@include('marketer_dashboard.layouts.sidebar')
<div class="content-wrapper">
    @yield('content')
</div>
@include('marketer_dashboard.layouts.footer')
