<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a href="{{ url('/marketer_dashboard') }}" class="navbar-brand">
            <span class="brand-text font-weight-bold">Dashboard</span>
        </a>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('marketer_dashboard.booking_online') }}" class="nav-link">Booking Online</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('marketer_dashboard.affiliasi') }}" class="nav-link">Affiliasi</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('marketer_dashboard.reset_password') }}" class="nav-link">Reset Password</a>
                </li>
            </ul>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav-link">Logout</a>
            </li>
            <li class="nav-item">
                <button class="navbar-toggler order-1" type="button" data-toggle="collapse"
                    data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </li>
        </ul>
    </div>
</nav>
