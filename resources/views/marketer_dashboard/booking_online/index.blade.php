@extends('marketer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Booking Online</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/marketer_dashboard') }}">Dashboard Marketer</a></li>
                    <li class="breadcrumb-item active">Booking Online</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">

                    <form action="{{ url('/marketer_dashboard/booking_online/store') }}" method="POST">
                        @csrf
                        <div class="card-body">

                            <div class="bg-info text-center">
                                <h5 class="py-2">Unit</h5>
                            </div>

                            <div class="form-group">
                                <label>Lokasi <span class="text-red">*</span></label>
                                <select name="location_id" id="" class="form-control" required onchange="showBlocks(this.value)">
                                    <option value="">-- Pilih Lokasi --</option>
                                    @foreach ($locations as $location)
                                    <option value="{{ $location->location_id }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Blok <span class="text-red">*</span></label>
                                <select name="block_id" id="" class="form-control" required onchange="showUnits(this.value)">
                                    <option value="">-- Pilih Blok --</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Unit <span class="text-red">*</span></label>
                                <select name="unit_id" id="" class="form-control" required>
                                    <option value="">-- Pilih Unit --</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Jenis pembelian <span class="text-red">*</span></label>
                                <select name="sales_type" id="" class="form-control" required>
                                    <option value="">-- Pilih Jenis Pembelian --</option>
                                    <option>CASH</option>
                                    <option>KPR</option>
                                </select>
                            </div>

                            <div class="mt-5 bg-info text-center">
                                <h5 class="py-2">Data Konsumen</h5>
                            </div>

                            <div class="form-group">
                                <label>Nama Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="name" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Tempat Lahir Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="place_of_birth" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Tanggal Lahir Konsumen <span class="text-red">*</span></label>
                                <input type="date" name="date_ofbirth" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Pasangan Konsumen (Suami/Istri) <span class="text-red">*</span></label>
                                <input type="text" name="couple" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>NIK Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="nik" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>No. Handphone Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="phone_num" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Institusi Konsumen </label>
                                <input type="text" name="institute" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Alamat KTP Konsumen <span class="text-red">*</span></label>
                                <textarea name="address" class="form-control form-required-register" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Alamat Domisili Konsumen <span class="text-red">*</span></label>
                                <textarea name="address_domicile" class="form-control form-required-register" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Pekerjaan Konsumen</label>
                                <input type="text" name="job" class="form-control">
                            </div>

                            <br>

                            <button type="submit" class="btn btn-success btn-save">
                                Simpan Booking
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
        var url = '{{ url('prop/component/slct_block?location_id=') }}' + location_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(response){
                $('select[name="block_id"]').html(response);
            }
        });
    }

    function showUnits(block_id) {
        var url = '{{ url('prop/component/slct_unit_ready?block_id=') }}' + block_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(response){
                $('select[name="unit_id"]').html(response);
            }
        });
    }

    function save(params) {
        window.scrollTo(0, 0);
        $('#alert-information').css('display', 'none');
        $('#alert-success-registration').css('display', 'block');
    }

    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('form[data-remote]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("PROSES...");
                },
                success: function (res) {
                    window.scrollTo(0, 0);
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Booking");
                    $('#alert-information').css('display', 'none');
                    if (res.status == 'success') {
                        $('#alert-success-registration').css('display', 'block');
                    } else {
                        $('#alert-danger-registration').css('display', 'block');
                    }

                }
            });
        });
    })();
</script>
@endsection
