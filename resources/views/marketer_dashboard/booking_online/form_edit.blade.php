@extends('marketer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Booking Online</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/marketer_dashboard') }}">Dashboard Marketer</a></li>
                    <li class="breadcrumb-item active">Booking Online</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row mb-2">
            <div class="col-md-12">
                <a href="{{ url('marketer_dashboard/booking_online/detail', $sales->id) }}" class="btn btn-default btn-sm">
                    <i class="fas fa-arrow-left"></i> Kembali ke detail
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title">Form Ubah Data</h5>
                    </div>

                    <div class="card-body">
                        <form action="{{ url('/marketer_dashboard/booking_online/edit', $sales->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="sdm_id" value="{{ $sales->customer_id }}">

                            <div class="bg-info text-center">
                                <h5 class="py-2">Unit</h5>
                            </div>

                            @if ($sales->booking_online_status == 'MENUNGGU')
                                <div class="form-group">
                                    <label>Lokasi <span class="text-red">*</span></label>
                                    <select name="location_id" id="" class="form-control" required onchange="showBlocks(this.value)">
                                        <option value="">-- Pilih Lokasi --</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->location_id }}" @if ($sales->location_id == $location->location_id) selected @endif>{{ $location->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Blok <span class="text-red">*</span></label>
                                    <select name="block_id" id="" class="form-control" required onchange="showUnits(this.value)">
                                        @foreach ($blocks as $block)
                                            <option value="{{ $block->block_id }}" @if ($sales->block_id == $block->block_id) selected @endif>{{ $block->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Unit <span class="text-red">*</span></label>
                                    <select name="unit_id" id="" class="form-control" required>
                                        @foreach ($units as $unit)
                                            <option value="{{ $unit->number }}" @if ($sales->unit_id == $unit->number) selected @endif>
                                                Type {{ $unit->unitType->name }} Nomor {{ $unit->number }} Harga Cash {{ GeneralHelper::rupiah($unit->harga_jual_cash) }} Harga KPR {{ GeneralHelper::rupiah($unit->harga_jual_kpr) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="rounded border bg-warning">
                                            <div class="px-2 py-2">
                                                Booking sudah diperiksa dan diterima oleh Admin, anda sudah tidak bisa mengubah unit booking.
                                                Silahkan hubungi admin untuk merubah data unit.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td width="30%" class="text-secondary">Unit Yang Dibooking</td>
                                            <td width="3%" class="text-right">:</td>
                                            <td width="67%">{{ $sales->location->name }} Blok {{ $sales->block->name }} No {{ $sales->unit_id }}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%" class="text-secondary">Type Unit</td>
                                            <td width="3%" class="text-right">:</td>
                                            <td width="67%">{{ $sales->unit->unitType->name }}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%" class="text-secondary">Harga</td>
                                            <td width="3%" class="text-right">:</td>
                                            <td width="67%">Rp {{ GeneralHelper::rupiah($sales->ttl_trnsct) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif


                            <div class="form-group">
                                <label>Jenis pembelian <span class="text-red">*</span></label>
                                <select name="trnsct_type" id="" class="form-control" required>
                                    <option value="">-- Pilih Jenis Pembelian --</option>
                                    <option @if($sales->sales_type == 'CASH') selected @endif>CASH</option>
                                    <option @if($sales->sales_type == 'KPR') selected @endif>KPR</option>
                                </select>
                            </div>

                            <div class="mt-5 bg-info text-center">
                                <h5 class="py-2">Data Konsumen</h5>
                            </div>

                            <div class="form-group">
                                <label>Nama Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="name" value="{{ $sales->customer->name }}" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Tempat Lahir Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="place_of_birth" value="{{ $sales->customer->place_of_birth }}" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Tanggal Lahir Konsumen <span class="text-red">*</span></label>
                                <input type="date" name="date_ofbirth" value="{{ $sales->customer->date_ofbirth }}" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Pasangan Konsumen (Suami/Istri) <span class="text-red">*</span></label>
                                <input type="text" name="couple" value="{{ $sales->customer->couple }}" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>NIK Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="nik" value="{{ $sales->customer->nik }}" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>No. Handphone Konsumen <span class="text-red">*</span></label>
                                <input type="text" name="phone_num" value="{{ $sales->customer->phone_num }}" class="form-control form-required-register" required>
                            </div>

                            <div class="form-group">
                                <label>Institusi Konsumen </label>
                                <input type="text" name="institute" value="{{ $sales->customer->institute }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Alamat KTP Konsumen <span class="text-red">*</span></label>
                                <textarea name="address"  class="form-control form-required-register" required>{{ $sales->customer->address }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Alamat Domisili Konsumen <span class="text-red">*</span></label>
                                <textarea name="address_domicile" class="form-control form-required-register" required>{{$sales->customer->address_domicile}}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Pekerjaan Konsumen</label>
                                <input type="text" name="job" value="{{ $sales->customer->job }}" class="form-control">
                            </div>

                            <br>

                            <button type="submit" class="btn btn-success btn-save">
                                Simpan Perubahan
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
        var url = '{{ url('prop/component/slct_block?location_id=') }}' + location_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(response){
                $('select[name="block_id"]').html(response);
            }
        });
    }

    function showUnits(block_id) {
        var url = '{{ url('prop/component/slct_unit_ready?block_id=') }}' + block_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(response){
                $('select[name="unit_id"]').html(response);
            }
        });
    }

    function save(params) {
        window.scrollTo(0, 0);
        $('#alert-information').css('display', 'none');
        $('#alert-success-registration').css('display', 'block');
    }

    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('form[data-remote]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("PROSES...");
                },
                success: function (res) {
                    window.scrollTo(0, 0);
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Booking");
                    $('#alert-information').css('display', 'none');
                    if (res.status == 'success') {
                        $('#alert-success-registration').css('display', 'block');
                    } else {
                        $('#alert-danger-registration').css('display', 'block');
                    }

                }
            });
        });
    })();
</script>
@endsection
