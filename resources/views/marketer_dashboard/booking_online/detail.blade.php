@extends('marketer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Booking Online</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/marketer_dashboard') }}">Dashboard Marketer</a></li>
                    <li class="breadcrumb-item active">Booking Online</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row mb-4">
            @if ($sales->booking_online_status == 'MENUNGGU')
                <div class="col-md-12">
                    <div class="rounded border bg-info">
                        <div class="px-2 py-2">
                            Booking masih dalam status pemeriksaan oleh Admin, anda masih bisa mengubah data booking.
                        </div>
                    </div>
                </div>
            @endif

            @if ($sales->booking_online_status == 'DITERIMA')
                <div class="col-md-12">
                    <div class="rounded border bg-success">
                        <div class="px-2 py-2">
                            Booking sudah diperiksa dan diterima oleh Admin, anda sudah tidak bisa mengubah data booking.
                        </div>
                    </div>
                </div>
            @endif

            @if ($sales->booking_online_status == 'DITOLAK')
                <div class="col-md-12">
                    <div class="rounded border bg-red">
                        <div class="px-2 py-2">
                            Booking sudah diperiksa dan ditolak oleh Admin
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail Booking</h5>

                        <div class="card-tools">
                            <a href="{{ url('marketer_dashboard/booking_online/form_edit', $sales->id) }}" class="btn btn-sm btn-primary">
                                <i class="fas fa-edit"></i> Ubah Data
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td width="30%" class="text-secondary">Tanggal Booking</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ GeneralHelper::konversiTgl($sales->sales_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">No Booking</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->sales_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Jenis Pembelian</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->sales_type }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Status Booking</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->booking_online_status }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Unit Yang Dibooking</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->location->name }} Blok {{ $sales->block->name }} No {{ $sales->unit_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Type Unit</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->unit->unitType->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Harga</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">Rp {{ GeneralHelper::rupiah($sales->ttl_trnsct) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Hak Marketer</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">Rp {{ GeneralHelper::rupiah($sales->affiliate_fee) }} (<span class="text-blue">Hak marketer akan dicairkan apabila data booking sudah diterima dan diproses oleh admin</span>)</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Hak Terbayarkan</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">Rp {{ GeneralHelper::rupiah($sales->affiliate_fee_paid) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Nama Konsumen</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Tempat, Tanggal Lahir</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->place_of_birth }}, {{ GeneralHelper::konversiTgl($sales->customer->date_ofbirth, 'ttd') }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">NIK</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->nik }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Nama Pasangan</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->couple }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Nomor Telepon</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Alamat Domisili</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->address_domicile }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Alamat KTP</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->address }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Pekerjaan</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->job }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Institusi</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $sales->customer->institute }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
