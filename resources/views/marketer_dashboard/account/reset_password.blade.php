@extends('marketer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Ganti Password</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/marketer_dashboard') }}"></a></li>
                    <li class="breadcrumb-item active">Ganti Password</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Ganti Password</h5>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form action="{{ url('/marketer_dashboard/do_reset_password') }}" method="POST">

                            @csrf
                            <input type="hidden" name="id" value="{{ $user->id }}">

                            <div class="form-group">
                                <label>Password Lama <span class="text-red">*</span></label>
                                <input type="password" name="password_old" class="form-control form-password" required>
                            </div>

                            <div class="form-group">
                                <label>Password Baru <span class="text-red">*</span></label>
                                <input type="password" name="password_new" class="form-control form-password" required minlength="4">
                            </div>

                            <div class="form-group">
                                <label>Konfirmasi Password Baru <span class="text-red">*</span></label>
                                <input type="password" name="password_new_confirm" class="form-control form-password" required minlength="4">
                                <span class="text-red" id="alert_password_new_confirm" style="display: none">Konfirmasi password baru salah!</span>
                            </div>

                            <div class="form-check">
                                <input type="checkbox" name="show" id="exampleCheck1" class="form-check-input">
                                <label for="exampleCheck1" class="form-check-label">Tampilkan password</label>
                            </div>

                            <br>

                            <button class="btn btn-sm btn-success btn-block">
                                Simpan Password
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function(){
        $('#exampleCheck1').click(function(){
            var value = $(this).is(':checked');

            $(this).is(':checked') ? $('.form-password').attr('type', 'text') : $('.form-password').attr('type', 'password');
        });

        $('input[name="password_new_confirm"]').keyup(function(){
            var password_new_confirm = this.value;
            var password_new = $('input[name="password_new"]').val();

            if (password_new_confirm == password_new) {
                $('#alert_password_new_confirm').css('display', 'none')
            } else {
                $('#alert_password_new_confirm').css('display', 'block')
            }
        });
    });
</script>
@endsection
