@include('layouts.head')
@include('layouts.navbar')
@include('layouts.sidebar')
<div class="content-wrapper">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>
@include('layouts.footer')
