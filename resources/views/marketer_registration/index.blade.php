<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pendaftaran Marketer</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- jQuery -->
    <script src="{{ url('public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ url('public/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
</head>
<body>
    <section class="container" style="margin-top: 50px; padding-bottom: 150px;">
        <div class="row justify-content-center">
            <div class="col-lg-6 text-center">
                <h3 class="font-weight-bold">Pendaftaran Marketer (Downline)</h3>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 mt-3">
                <div class="card">
                    <div class="card-header">
                        <div class="alert alert-info" id="alert-information">
                            <h4 class="font-weight-bold">Perhatian</h4>
                            <p>
                                Isilah data diri anda dengan benar di form berikut ini.
                            </p>
                        </div>

                        <div class="alert alert-success" id="alert-success-registration" style="display: none">
                            <h4 class="font-weight-bold">Selamat!</h4>
                            <p>
                                Sekarang anda sudah menjadi bagian dari kami sebagai Marketer Downline. <br>
                                Nomor Marketer kamu  adalah <b id="marketer-num"></b> <br>
                                Nomor Markter mohon dicatat atau diingat!
                            </p>
                        </div>

                        <div class="alert alert-danger" id="alert-danger-registration" style="display: none">
                            <h4 class="font-weight-bold">Mohon Maaf!</h4>
                            <p>
                                Pendaftaran gagal, silahkan ulangi pendaftaran.
                            </p>
                        </div>
                    </div>
                    <form action="{{ url('/pendaftaran_marketer/store') }}" method="POST" data-remote>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama <span class="text-red">*</span></label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>No. Handphone <span class="text-red">*</span></label>
                                <input type="text" name="phone_num" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nama Bank Rekening yang anda punya <span class="text-red">*</span></label>
                                <input type="text" name="rek_bank" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nomor Rekening <span class="text-red">*</span></label>
                                <input type="text" name="rek_number" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Rekening Atas Nama <span class="text-red">*</span></label>
                                <input type="text" name="rek_name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="address" class="form-control"></textarea>
                            </div>

                            <br>

                            <button type="submit" class="btn btn-success btn-save">
                                Daftar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('form[data-remote]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("PROSES...");
                },
                success: function (res) {
                    window.scrollTo(0, 0);
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Daftar");
                    $('#alert-information').css('display', 'none');
                    if (res.status == 'success') {
                        $('#alert-success-registration').css('display', 'block');
                        $('#marketer-num').text(res.affiliate_num)
                    } else {
                        $('#alert-danger-registration').css('display', 'block');
                    }

                }
            });
        });
    })();
    </script>
</body>
</html>
