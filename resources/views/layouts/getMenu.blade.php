<li class="nav-item">
    <a href="{{ url('/') }}" class="nav-link {{ (request()->is('/')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>Dashboard</p>
    </a>
</li>

<li class="nav-header">DATA SYSTEM</li>
<li class="nav-item">
    <a href="{{ url('/system/module') }}" class="nav-link {{ (request()->is(url('/system/module'))) ? 'active' : '' }}">
        <i class="nav-icon fas fa-th-large"></i>
        <p>Module</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ url('/system/role') }}" class="nav-link {{ (request()->is('system/role')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-users"></i>
        <p>Role</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ url('/system/user') }}" class="nav-link {{ (request()->is('system/user')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-user"></i>
        <p>User</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ url('/system/group-menu') }}" class="nav-link {{ (request()->is('system/group-menu')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-stream"></i>
        <p>Group Menu</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ url('/system/acl') }}" class="nav-link {{ (request()->is('system/acl')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-check-circle"></i>
        <p>ACL</p>
    </a>
</li>
