<script>
$(function () {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
});

function successAlert(data) {
    $('#response-alert').html('');
    var res = '<div class="alert alert-success">';
        res += '<h5><i class="icon fas fa-check"></i> Berhasil!</h5>';
        res += data.text;
        res += '</div>';

    return res;
}

function errorAlert(data) {
    $('#response-alert').html('');
    var res = '<div class="alert alert-danger">';
        res += '<h5><i class="icon fas fa-ban"></i> Gagal!</h5>';
        $.each(data.responseJSON.errors, function(key,value) {
            res += value + "<br>";
        })
        res += '</div>';

    return res;
}
</script>
