<nav class="main-header navbar navbar-expand border-bottom-0 navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        {{-- <li class="nav-item d-none d-sm-inline-block" style="margin-right: 7px;">
            <a href="{{ url('/travel/booking/search-cars-page') }}" class="nav-link btn btn-outline-secondary">
                <i class="fas fa-handshake"></i> &nbsp;
                Booking
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link btn btn-outline-secondary" data-toggle="dropdown" href="#">
                <i class="fas fa-money-check-alt"></i> &nbsp;
                Transaksi
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                <a href="" class="dropdown-item">
                    Pemasukan
                </a>
                <div class="dropdown-divider"></div>
                <a href="" class="dropdown-item">
                    Pengeluaran
                </a>
                <div class="dropdown-divider"></div>
                <a href="" class="dropdown-item">
                    Terima Uang
                </a>
                <div class="dropdown-divider"></div>
                <a href="" class="dropdown-item">
                    Kirim Uang
                </a>
            </div>
        </li> --}}
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ url('/gate') }}" class="nav-link">
                <i class="fas fa-th-large"></i> &nbsp;
                Gate
            </a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ url('/logout') }}" class="nav-link">
                <i class="fas fa-sign-out-alt"></i> &nbsp;
                Logout
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-user"></i> &nbsp;
                {{ Session::get('auth_nama') }}
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                {{-- <a href="#" class="dropdown-item">
                    <i class="fas fa-user mr-2"></i> Profile
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('auth.change_password') }}" class="dropdown-item">
                    <i class="fas fa-eye mr-2"></i> Ubah Password
                </a> --}}
                <a href="{{ route('logout') }}" class="dropdown-item">
                    <i class="fas fa-sign-out-alt mr-2"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
