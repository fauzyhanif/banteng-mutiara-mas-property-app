<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BMM Property | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Daterangepicker -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    {{-- datatable --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Daterangepicker -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- fullCalendar -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar/main.min.css') }}">
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar-daygrid/main.min.css') }}">
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar-timegrid/main.min.css') }}">
    <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar-bootstrap/main.min.css') }}">
    {{-- Toastr --}}
    <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/toastr/toastr.css') }}">
    {{-- Select2 --}}
    <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">

    <!-- jQuery -->
    <script src="{{ url('public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ url('public/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('public/admin-lte/dist/js/adminlte.min.js') }}"></script>
    <!-- Jquery Redirect -->
    <script src="{{ url('public/admin-lte/plugins/jquery-redirect/jquery-redirect.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('public/admin-lte/dist/js/demo.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/fullcalendar/main.min.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/fullcalendar-daygrid/main.min.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/fullcalendar-timegrid/main.min.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/fullcalendar-interaction/main.min.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/fullcalendar-bootstrap/main.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ url('public/admin-lte/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ url('public/admin-lte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ url('public/admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    {{-- Toastr --}}
    <script src="{{ url('public/admin-lte/plugins/toastr/toastr.min.js') }}"></script>
    {{-- Datatable --}}
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="{{ url('public/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ url('public/admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <!-- select2 -->
    <script src="{{ url('public/admin-lte/plugins/select2/js/select2.min.js') }}"></script>
    <!-- jquery redirect -->
    <script src="{{ url('public/js/jquery-redirect.js') }}"></script>
    <!-- jquery mask -->
    <script src="{{ url('public/admin-lte/plugins/jquery-mask/dist/jquery.mask.min.js') }}"></script>

    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    @include('cssCustom.custom')
    @include('jsCustom.main')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
