<aside class="main-sidebar elevation-4 sidebar-dark-orange">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ url('/public/system/module', Session::get('auth_module_image')) }}" alt="" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">{{ Session::get('auth_module_nama') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-legacy nav-flat" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/') }}" class="nav-link {{ (request()->is('/')) ? 'active' : '' }}" style="margin-left: -15px !important">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('/dashboard-progress') }}" class="nav-link {{ (request()->is('/dashboard-progress')) ? 'active' : '' }}"
                        style="margin-left: -15px !important">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard Progress</p>
                    </a>
                </li>

                @foreach (json_decode(Session::get('auth_group_menu')) as $group)
                    <li class="nav-header">{{ $group->nama }}</li>

                    @foreach (json_decode(Session::get('auth_menu')) as $menu)
                        @php $arrGroup = json_decode($menu->id_group_menu) @endphp
                        @if (in_array($group->id_group_menu, $arrGroup))
                            @if ($menu->is_parent == 'Y')
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link" style="margin-left: -15px !important">
                                        <i class="nav-icon fas {{ $menu->icon }}"></i>
                                        <p>
                                            {{ $menu->label }}
                                            <i class="right fas fa-angle-right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @foreach (json_decode(Session::get('auth_submenu')) as $submenu)
                                            @php $arrGroupSubmenu = json_decode($menu->id_group_menu) @endphp
                                            @if (in_array($group->id_group_menu, $arrGroupSubmenu) && $submenu->parent == $menu->id)
                                                <li class="nav-item">
                                                    <a href="{{ url($submenu->url) }}" class="nav-link">
                                                        <i class="far fa-circle nav-icon"></i>
                                                        <p>{{ $submenu->label }}</p>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a href="{{ url($menu->url) }}" class="nav-link {{ (request()->is($menu->url)) ? 'active' : '' }}" style="margin-left: -15px !important">
                                        <i class="nav-icon fas {{ $menu->icon }}"></i>
                                        <p>{{ $menu->label }}</p>
                                    </a>
                                </li>
                            @endif
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </nav>
    </div>
</aside>
