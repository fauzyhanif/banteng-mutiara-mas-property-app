<style>
body {
    font-family: 'Open Sans', sans-serif;
    font-family: 'Poppins', sans-serif;
}

.card {
    box-shadow: 0px !important;
}

.table td, .table th {
    padding: 0.45rem 0.75rem !important;
}

.scroll-x {
    overflow-x: scroll;
}

.card-body {
    overflow-x: scroll;
}
</style>
