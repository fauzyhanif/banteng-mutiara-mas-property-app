@include('customer_dashboard.layouts.head')
@include('customer_dashboard.layouts.navbar')
@include('customer_dashboard.layouts.sidebar')
<div class="content-wrapper">
    @yield('content')
</div>
@include('customer_dashboard.layouts.footer')
