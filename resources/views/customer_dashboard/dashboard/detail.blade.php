@extends('customer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/customer_dashboard/dashboard') }}">Dashboard Customer</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/customer_dashboard/dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if ($purchase->booking_online_file_bayar == '')
                    <div class="alert alert-warning">
                        <h5>
                            <i class="icon fas fa-info"></i> Himbauan
                        </h5>

                        Silahkan upload bukti pembayaran (Booking Fee) pada form dibawah ini!
                    </div>
                @else
                    @if ($purchase->booking_online_status == 'MENUNGGU')
                        <div class="alert alert-info">
                            <h5>
                                <i class="icon fas fa-info"></i> Info
                            </h5>

                            Pesanan kamu masih menunggu konfirmasi perusahaan.
                        </div>
                    @elseif ($purchase->booking_online_status == 'DITERIMA')
                        <div class="alert alert-success">
                            <h5>
                                <i class="icon fas fa-info"></i> Info
                            </h5>

                            Pesanan kamu sudah diterima perusahaan.
                        </div>
                    @elseif ($purchase->booking_online_status == 'DITOLAK')
                        <div class="alert alert-danger">
                            <h5>
                                <i class="icon fas fa-info"></i> Info
                            </h5>

                            Pesanan kamu ditolak perusahaan.
                        </div>
                    @endif
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Upload Bukti Pembayaran</h5>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif

                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        
                        <form action="{{ url('/customer_dashboard/dashboard/upload_bukti_bayar') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="sales_id" value="{{ $purchase->sales_id }}">
                            <div class="form-group">
                                <label>Sisipkan file bukti transfer disini</label>
                                <input type="file" name="file" class="form-control form-file">
                            </div>

                            <span class="text-blue">
                                <ul>
                                    <li>Format file berupa .jpg|.jpeg|.png|.pdf</li>
                                    <li>Size file maksimal 2mb</li>
                                </ul>
                            </span>

                            <button type="submit" class="btn btn-sm btn-success btn-block">Upload</button>

                            <hr>

                            <span class="font-weight-light">File yang pernah kamu upload:</span>
                            <br>
                            <a href="{{ url('/booking_online_file_bayar', $purchase->booking_online_file_bayar) }}" target="_blank">{{ $purchase->booking_online_file_bayar }}</a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail Pesanan Unit</h5>

                        @if ($purchase->booking_online_status == 'DITERIMA')
                            <div class="card-tools">
                                <a href="{{ url('/prop/sales/print_sppr', $purchase->id) }}" class="btn btn-sm btn-primary" target="_blank" rel="noopener noreferrer">
                                    <i class="fas fa-print"></i> Cetak SPPR
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <div class="form-group">
                            <Label>Lokasi</Label>
                            <p style="margin-top: -10px">{{ $purchase->location->name }} {{ $purchase->block->name }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Type Unit</Label>
                            <p style="margin-top: -10px">
                                @if (array_key_exists($purchase->unit->unittype_id, $res_unit_types))
                                {{ $res_unit_types[$purchase->unit->unittype_id] }}
                                @endif
                            </p>
                        </div>

                        <div class="form-group">
                            <Label>Nomor Unit</Label>
                            <p style="margin-top: -10px">{{ $purchase->unit_id }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Jenis Pembelian</Label>
                            <p style="margin-top: -10px">{{ $purchase->sales_type }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Harga</Label>
                            <p style="margin-top: -10px">{{ GeneralHelper::rupiah($purchase->ttl_trnsct) }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Terbayar</Label>
                            <p style="margin-top: -10px">{{ GeneralHelper::rupiah($purchase->ttl_trnsct_paid) }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Sisa</Label>
                            <p style="margin-top: -10px">{{ GeneralHelper::rupiah($purchase->ttl_trnsct - $purchase->ttl_trnsct_paid) }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Marketer</Label>
                            <p style="margin-top: -10px">
                                @if ($purchase->affiliate_num != '')
                                    {{ $purchase->marketer->sdm->name }}
                                @else
                                    -
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('.form-file').change(function() {
    var filePath = this.value;

    // Allowing file type
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;

    if (!allowedExtensions.exec(filePath)) {
        alert('Jenis file tidak bisa diupload');
        this.value = '';
        return false;
    } else {
        var FileSize = this.files[0].size / 2048 / 2048; // in MB
        if (FileSize > 1) {
            this.value = '';
            alert("Mohon maaf file terlalu besar, maximal file 2 mb.")
            return false;
        }
    }
});
</script>
@endsection
