@extends('customer_dashboard.layouts.index')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/customer_dashboard/dashboard') }}">Dashboard Customer</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        @if ($customer->user->password_changed == 'N')
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning">
                        <h5>
                            <i class="icon fas fa-info"></i> Info
                        </h5>

                        Password kamu untuk masuk sistem saat ini adalah 54321, silahkan ganti password untuk akses yang lebih aman.

                        <br>

                        <a href="{{ url('/customer_dashboard/account/change_password') }}" class="btn btn-sm btn-primary">
                            Ganti Password
                        </a>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Data Konsumen</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <Label>Nama Lengkap</Label>
                            <p style="margin-top: -10px">{{ $customer->name }}</p>
                        </div>

                        <div class="form-group">
                            <Label>No Handphone</Label>
                            <p style="margin-top: -10px">{{ $customer->phone_num }}</p>
                        </div>

                        <div class="form-group">
                            <Label>NIK</Label>
                            <p style="margin-top: -10px">{{ $customer->nik }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Pasangan</Label>
                            <p style="margin-top: -10px">{{ $customer->couple }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Pekerjaan</Label>
                            <p style="margin-top: -10px">{{ $customer->job }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Institusi</Label>
                            <p style="margin-top: -10px">{{ $customer->institute }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Alamat KTP</Label>
                            <p style="margin-top: -10px">{{ $customer->address }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Alamat Domisili</Label>
                            <p style="margin-top: -10px">{{ $customer->address_domicile }}</p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Data Pembelian</h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-sm table-bordered">
                            <thead class="bg-primary">
                                <th>Lokasi</th>
                                <th>Type & Nomor Unit</th>
                                <th>Harga</th>
                                <th>Pembelian</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($purchases as $purchase)
                                    <tr>
                                        <td>{{ $purchase->location->name }} {{ $purchase->block->name }}</td>
                                        <td>
                                            @if (array_key_exists($purchase->unit->unittype_id, $res_unit_types))
                                                {{ $res_unit_types[$purchase->unit->unittype_id] }}
                                            @endif
                                            &#8226;
                                            <a href="{{ url('/customer_dashboard/dashboard/detail', $purchase->id) }}">
                                                {{ $purchase->unit_id }}
                                            </a>
                                        </td>
                                        <td>{{ GeneralHelper::rupiah($purchase->ttl_trnsct) }}</td>
                                        <td>{{ $purchase->sales_type }}</td>
                                        <td>{{ $purchase->booking_online_status }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
