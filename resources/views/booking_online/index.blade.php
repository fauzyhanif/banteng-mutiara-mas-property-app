<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking Online</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- jQuery -->
    <script src="{{ url('public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ url('public/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
</head>
<body>
    <section class="container" style="margin-top: 50px; padding-bottom: 150px">
        <div class="row justify-content-center">
            <div class="col-lg-6 text-center">
                <h3 class="font-weight-bold">Booking Online</h3>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-10 mt-3">
                <div class="card">
                    <div class="card-header">
                        <div class="alert alert-info" id="alert-information">
                            <h4 class="font-weight-bold">Perhatian</h4>
                            <p>
                                Isilah data diri anda dengan benar di form berikut ini.
                            </p>
                        </div>

                        <div class="alert alert-success" id="alert-success-registration" style="display: none">
                            <h4 class="font-weight-bold">Selamat!</h4>
                            <p>
                                Anda sudah memesan 1 buah unit, Admin perusahaan akan menghubungi anda dalam waktu dekat. <br>
                                Terima kasih.
                            </p>
                        </div>

                        <div class="alert alert-danger" id="alert-danger-registration" style="display: none">
                            <h4 class="font-weight-bold">Mohon Maaf!</h4>
                            <p>
                                Aktifitas booking gagal, silahkan ulangi.
                            </p>
                        </div>

                    </div>
                    <form action="{{ url('/booking_online_checkout') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Lokasi <span class="text-red">*</span></label>
                                <select name="location_id" id="" class="form-control" required onchange="showBlocks(this.value)">
                                    <option value="">-- Pilih Lokasi --</option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->location_id }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Blok <span class="text-red">*</span></label>
                                <select name="block_id" id="" class="form-control" required onchange="showUnits(this.value)">
                                    <option value="">-- Pilih Blok --</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Unit <span class="text-red">*</span></label>
                                <select name="unit_id" id="" class="form-control" required>
                                    <option value="">-- Pilih Unit --</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Jenis pembelian <span class="text-red">*</span></label>
                                <select name="trnsct_type" id="" class="form-control" required>
                                    <option value="">-- Pilih Jenis Pembelian --</option>
                                    <option>CASH</option>
                                    <option>KPR</option>
                                </select>
                            </div>

                            <div class="form-group form-register">
                                <label>Nomor Marketer <span class="text-info">(Optional)</span></label>
                                <input type="text" name="aaffiliate_num" class="form-control">
                            </div>


                            <br>

                            <button type="submit" class="btn btn-success btn-save">
                                Booking
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <script>
    function showBlocks(location_id) {
        var url = '{{ url('prop/component/slct_block?location_id=') }}' + location_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(response){
                $('select[name="block_id"]').html(response);
            }
        });
    }

    function showUnits(block_id) {
        var url = '{{ url('prop/component/slct_unit_ready?block_id=') }}' + block_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(response){
                $('select[name="unit_id"]').html(response);
            }
        });
    }

    function save(params) {
        window.scrollTo(0, 0);
        $('#alert-information').css('display', 'none');
        $('#alert-success-registration').css('display', 'block');
    }

    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('form[data-remote]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("PROSES...");
                },
                success: function (res) {
                    window.scrollTo(0, 0);
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Booking");
                    $('#alert-information').css('display', 'none');
                    if (res.status == 'success') {
                        $('#alert-success-registration').css('display', 'block');
                    } else {
                        $('#alert-danger-registration').css('display', 'block');
                    }

                }
            });
        });
    })();
    </script>
</body>
</html>
