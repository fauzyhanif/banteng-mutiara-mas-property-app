<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Checkout</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- jQuery -->
        <script src="{{ url('public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <!-- Bootstrap 4 -->
        <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ url('public/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    </head>

    <body>
        <section class="container" style="margin-top: 50px; padding-bottom: 150px">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <h3 class="font-weight-bold">Checkout</h3>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-10 mt-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="alert alert-info" id="alert-information">
                                <h4 class="font-weight-bold">Perhatian</h4>
                                <p>
                                    <ul>
                                        <li>Jika anda sudah pernah membeli unit melalui booking online silahkan klik "Sudah Punya Akun"</li>
                                        <li>Jika anda belum pernah membeli unit melalui booking online silahkan isi formulir data diri dengan lengkap</li>
                                    </ul>
                                </p>
                            </div>

                            <div class="alert alert-danger" id="alert-danger-registration" style="display: none">
                                <h4 class="font-weight-bold">Mohon Maaf!</h4>
                                <p id="alert-danger-text">

                                </p>
                            </div>

                        </div>
                        <form action="{{ url('/booking_online/store') }}" method="POST" data-remote>
                            @csrf
                            <input type="hidden" name="location_id" value="{{ $unit->location_id }}">
                            <input type="hidden" name="block_id" value="{{ $unit->block_id }}">
                            <input type="hidden" name="unit_id" value="{{ $unit->number }}">
                            <input type="hidden" name="sales_type" value="{{ $trnsct_type }}">
                            <input type="hidden" name="affiliate_num" value="{{ $affiliate_num }}">

                            <div class="card-body">
                                <table class="table table-bordered table-sm">
                                    <tbody>
                                        <tr>
                                            <th width="35%">Lokasi</th>
                                            <td>{{ $unit->location->name }} {{ $unit->block->name }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%">Type Unit</th>
                                            <td>{{ $unit->unitType->name }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%">Nomor Unit</th>
                                            <td>{{ $unit->number }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%">Jenis Pembelian</th>
                                            <td>{{ $trnsct_type }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%">Harga</th>
                                            <td>
                                                {{ ($trnsct_type == 'CASH') ? GeneralHelper::rupiah($unit->harga_jual_cash) : GeneralHelper::rupiah($unit->harga_jual_kpr) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="2">
                                                <a href="{{ url('booking_online') }}">Ganti Unit</a>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="form-group" id="btn-show-form-login">
                                    <a href="javascript::void(0)" onclick="showFormLogin()">Sudah Punya Akun</a>
                                </div>

                                <div class="form-group" id="btn-show-form-register" style="display: none">
                                    <a href="javascript::void(0)" onclick="showFormRegister()">Pengguna Baru</a>
                                </div>

                                <input type="hidden" name="login_register" value="REGISTER">

                                <div class="form-group form-login" style="display: none">
                                    <label>Email <span class="text-red">*</span></label>
                                    <input type="email" name="login_email" class="form-control form-required-login">
                                </div>

                                <div class="form-group form-login" style="display: none">
                                    <label>Password <span class="text-red">*</span></label>
                                    <input type="text" name="login_password" class="form-control form-required-login">
                                </div>

                                <div class="form-group form-register">
                                    <label>Nama Konsumen <span class="text-red">*</span></label>
                                    <input type="text" name="name" class="form-control form-required-register" required>
                                </div>

                                <div class="form-group form-register">
                                    <label>Tanggal Lahir Konsumen <span class="text-red">*</span></label>
                                    <input type="date" name="date_ofbirth" class="form-control form-required-register" required>
                                </div>

                                <div class="form-group form-register">
                                    <label>Pasangan Konsumen (Suami/Istri) <span class="text-red">*</span></label>
                                    <input type="text" name="couple" class="form-control form-required-register" required>
                                </div>

                                <div class="form-group form-register">
                                    <label>NIK Konsumen <span class="text-red">*</span></label>
                                    <input type="text" name="nik" class="form-control form-required-register" required>
                                </div>

                                <div class="form-group form-register">
                                    <label>No. Handphone Konsumen <span class="text-red">*</span></label>
                                    <input type="text" name="phone_num" class="form-control form-required-register" required>
                                </div>

                                <div class="form-group form-register">
                                    <label>Email <span class="text-red">*</span></label>
                                    <input type="email" name="email" class="form-control form-required-register" required>
                                </div>

                                <div class="form-group form-register">
                                    <label>Institusi Konsumen </label>
                                    <input type="text" name="institute" class="form-control">
                                </div>

                                <div class="form-group form-register">
                                    <label>Alamat KTP Konsumen <span class="text-red">*</span></label>
                                    <textarea name="address" class="form-control form-required-register" required></textarea>
                                </div>

                                <div class="form-group form-register">
                                    <label>Alamat Domisili Konsumen <span class="text-red">*</span></label>
                                    <textarea name="address_domicile" class="form-control form-required-register" required></textarea>
                                </div>

                                <div class="form-group form-register">
                                    <label>Pekerjaan Konsumen</label>
                                    <input type="text" name="job" class="form-control">
                                </div>



                                <br>

                                <button type="submit" class="btn btn-success btn-save">
                                    Booking
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <script>
        function showFormLogin() {
            $('input[name="login_register"]').val('LOGIN');
            $('.form-login').css('display', 'block');
            $('.form-register').css('display', 'none');
            $('.form-required-login').attr('required', true);
            $('.form-required-register').attr('required', false);
            $('#btn-show-form-register').css('display', 'block');
            $('#btn-show-form-login').css('display', 'none');
        }

        function showFormRegister() {
            $('input[name="login_register"]').val('REGISTER');
            $('.form-login').css('display', 'none');
            $('.form-register').css('display', 'block');
            $('.form-required-login').attr('required', false);
            $('.form-required-register').attr('required', true);
            $('#btn-show-form-login').css('display', 'block');
            $('#btn-show-form-register').css('display', 'none');
        }

        (function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('form[data-remote]').on('submit', function(e) {
                e.preventDefault();
                var form    = $(this);
                var url     = form.prop('action');
                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType:'json',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".btn-save").addClass("disabled");
                        $(".btn-save").text("PROSES...");
                    },
                    success: function (res) {
                        window.scrollTo(0, 0);
                        $(".btn-save").removeClass("disabled");
                        $(".btn-save").text("Booking");
                        $('#alert-information').css('display', 'none');
                        if (res.status == 'success') {
                            // redirect to worklist by unit
                            var baseUrl = {!! json_encode(url('/customer_dashboard/dashboard')) !!};
                            window.location.href = baseUrl;
                        } else {
                            $('#alert-danger-registration').css('display', 'block');
                            $('#alert-danger-text').html(res.text);
                        }
                    }
                });
            });
        })();
        </script>
    </body>
</html>
