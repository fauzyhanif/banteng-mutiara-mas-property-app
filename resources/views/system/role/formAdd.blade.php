@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.role') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Role
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Role Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Role Baru
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.role.add.new') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>ID Role <span class="text-red">*</span></label>
                                <input type="number" name="id_role" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nama Role <span class="text-red">*</span></label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @foreach ($modules as $module)
                                <input type="checkbox" name="id_module[]" value="{{ $module->id_module }}"> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.role.asset.js')
@endsection
