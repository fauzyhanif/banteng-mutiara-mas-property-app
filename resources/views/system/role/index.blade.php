@extends('index')

@section('content')
<section class="content-header">
    <h1>Role</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Role
                        <div class="card-tools">
                            <a href="{{ route('system.role.form.add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Role Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th class="text-center">ID Role</th>
                                <th>Nama</th>
                                <th>Module</th>
                                <th class="text-center">Aktif</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)
                                <tr>
                                    <td class="text-center">{{ $role->id_role }}</td>
                                    <td>{{ $role->nama }}</td>
                                    <td>
                                        @php
                                            $arrModule = json_decode($role->id_module)
                                        @endphp
                                        @foreach ($modules as $module)
                                            @if (in_array($module->id_module, $arrModule))
                                                <label class="badge badge-warning">{{ $module->nama }}</label>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-center">{{ $role->aktif }}</td>
                                    <td>
                                        <a href="{{ route('system.role.form.edit', $role->id_role) }}" class="btn btn-xs btn-secondary">
                                            <i class="fas fa-pen-square"></i> &nbsp;
                                            Edit
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
