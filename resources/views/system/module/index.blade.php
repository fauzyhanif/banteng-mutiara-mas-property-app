@extends('index')

@section('content')
<section class="content-header">
    <h1>Module</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Module
                        <div class="card-tools">
                            <a href="{{ route('system.module.form.add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Module Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th class="text-center">Image</th>
                                <th class="text-center">ID Module</th>
                                <th>Nama</th>
                                <th class="text-center">Aktif</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($modules as $module)
                                <tr>
                                    <td class="text-center">
                                        <img src="{{ url('/public/system/module', $module->image) }}" class="logo" style="width: 50px">
                                    </td>
                                    <td class="text-center">{{ $module->id_module }}</td>
                                    <td>
                                        <strong>{{ $module->nama }}</strong> <br>
                                        <small class="text-muted">{{ $module->deskripsi }}</small>
                                    </td>
                                    <td class="text-center">{{ $module->aktif }}</td>
                                    <td>
                                        <a href="{{ route('system.module.form.edit', $module->id_module) }}" class="btn btn-xs btn-secondary">
                                            <i class="fas fa-pen-square"></i> &nbsp;
                                            Edit
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
