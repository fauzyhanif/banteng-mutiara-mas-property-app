@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.module') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Module
            </a>
        </li>
        <li class="breadcrumb-item active">Edit Module</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Edit Module
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.module.edit', $module->id_module) }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Kode Module <span class="text-red">*</span></label>
                                <input type="number" name="id_module" value="{{ $module->id_module }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nama Module <span class="text-red">*</span></label>
                                <input type="text" name="nama" value="{{ $module->nama }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi <span class="text-red">*</span></label>
                                <textarea name="deskripsi" class="form-control" required>{{ $module->deskripsi }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Icon Module <span class="text-secondary font-weight-light font-italic">(Kosongkan jika tidak ingin diganti)</span></label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Aktif</label>
                                <p>
                                    <input type="radio" name="aktif[]" value="Y" @if ($module->aktif == "Y") checked @endif> Ya <br>
                                    <input type="radio" name="aktif[]" value="N" @if ($module->aktif == "N") checked @endif> Tidak
                                </p>
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.module.asset.js')
@endsection
