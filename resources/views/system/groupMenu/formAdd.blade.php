@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.groupmenu') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Group Menu
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Group Menu Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Group Menu Baru
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.groupmenu.add.new') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>ID Group Menu <span class="text-red">*</span></label>
                                <input type="number" name="id_group_menu" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nama Group Menu <span class="text-red">*</span></label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @foreach ($modules as $module)
                                <input type="checkbox" name="id_module[]" value="{{ $module->id_module }}"> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Role <span class="text-red">*</span></label> <br>
                                @foreach ($roles as $role)
                                <input type="checkbox" name="id_role[]" value="{{ $role->id_role }}"> {{ $role->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Urut <span class="text-red">*</span></label>
                                <input type="number" name="urut" class="form-control" required>
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.groupMenu.asset.js')
@endsection
