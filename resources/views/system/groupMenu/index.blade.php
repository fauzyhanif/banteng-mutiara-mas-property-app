@extends('index')

@section('content')
<section class="content-header">
    <h1>Group Menu</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Group Menu
                        <div class="card-tools">
                            <a href="{{ route('system.groupmenu.form.add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Group Menu Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th width="5%">ID</th>
                                <th>Nama</th>
                                <th>Module</th>
                                <th>Role</th>
                                <th>Aktif</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($groups as $group)
                                <tr>
                                    <td>
                                        <label class="badge badge-secondary">{{ $group->id_group_menu }}</label>
                                    </td>
                                    <td>{{ $group->nama }}</td>
                                    <td>
                                        @php
                                            $arrModule = json_decode($group->id_module)
                                        @endphp
                                        @foreach ($modules as $module)
                                            @if (in_array($module->id_module, $arrModule))
                                                <label class="badge badge-warning">{{ $module->nama }}</label>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @php
                                            $arrRole = json_decode($group->id_role)
                                        @endphp
                                        @foreach ($roles as $role)
                                            @if (in_array($role->id_role, $arrRole))
                                                <label class="badge badge-warning">{{ $role->nama }}</label>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ $group->aktif }}</td>
                                    <td>
                                        <a href="{{ route('system.groupmenu.form.edit', $group->id_group_menu) }}" class="btn btn-xs btn-warning">
                                            <i class="fas fa-pen-square"></i>
                                            Edit
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
