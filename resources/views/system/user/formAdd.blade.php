@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.user') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar User
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah User Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form User Baru
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.user.add.new') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Nama User <span class="text-red">*</span></label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Email <span class="text-red">*</span></label>
                                <input type="email" name="email" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Password <span class="text-red">*</span></label>
                                <input type="password" name="password" class="form-control" required>

                                <span class="text-muted">*Minimal 6 karakter</span>
                            </div>

                            <div class="form-group">
                                <label>No. Telepon <span class="text-red">*</span></label>
                                <input type="text" name="no_telp" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Alamat <span class="text-red">*</span></label>
                                <textarea name="alamat" class="form-control" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @foreach ($modules as $module)
                                <input type="checkbox" name="id_module[]" value="{{ $module->id_module }}"> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Role <span class="text-red">*</span></label> <br>
                                @foreach ($roles as $role)
                                <input type="checkbox" name="id_role[]" value="{{ $role->id_role }}"> {{ $role->nama }} <br>
                                @endforeach
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.user.asset.js')
@endsection
