@extends('index')

@section('content')
<section class="content-header">
    <h1>User</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar User
                        <div class="card-tools">
                            <a href="{{ route('system.user.form.add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah User Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telp</th>
                                <th class="text-center">Aktif</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->nama }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->no_telp }}</td>
                                    <td class="text-center">{{ $user->aktif }}</td>
                                    <td>
                                        <a href="{{ route('system.user.form.edit', $user->id) }}" class="btn btn-xs btn-secondary">
                                            <i class="fas fa-pen-square"></i> &nbsp;
                                            Edit
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
