@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.user') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar User
            </a>
        </li>
        <li class="breadcrumb-item active">Edit User</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Edit User
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.user.edit', $user->id) }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Nama User <span class="text-red">*</span></label>
                                <input type="text" name="nama" value="{{ $user->nama }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Email <span class="text-red">*</span></label>
                                <input type="email" name="email" value="{{ $user->email }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Password <span class="text-red">*</span></label>
                                <input type="password" name="password" class="form-control">

                                <span class="text-muted">*Kosongkan jika tidak ingin mengubah password</span> <br>
                                <span class="text-muted">*Minimal 6 karakter</span>
                            </div>

                            <div class="form-group">
                                <label>No. Telepon <span class="text-red">*</span></label>
                                <input type="text" name="no_telp" value="{{ $user->no_telp }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Alamat <span class="text-red">*</span></label>
                                <textarea name="alamat" class="form-control" required>{{ $user->alamat }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @php
                                    $id_module = json_decode($user->id_module);
                                @endphp
                                @foreach ($modules as $module)
                                <input
                                    type="checkbox"
                                    name="id_module[]"
                                    value="{{ $module->id_module }}"
                                    @if (isset($id_module) && in_array($module->id_module, $id_module))
                                        checked
                                    @endif> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Role <span class="text-red">*</span></label> <br>
                                @php
                                    $id_role = json_decode($user->id_role);
                                @endphp
                                @foreach ($roles as $role)
                                <input
                                    type="checkbox"
                                    name="id_role[]"
                                    value="{{ $role->id_role }}"
                                    @if (isset($id_role) && in_array($role->id_role, $id_role))
                                        checked
                                    @endif> {{ $role->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Aktif</label>
                                <p>
                                    <input type="radio" name="aktif[]" value="Y" @if ($user->aktif == "Y") checked @endif> Ya <br>
                                    <input type="radio" name="aktif[]" value="N" @if ($user->aktif == "N") checked @endif> Tidak
                                </p>
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.user.asset.js')
@endsection
