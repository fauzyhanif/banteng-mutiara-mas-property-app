<div class="form-group">
    <label>Posisi <span class="text-red">*</span></label>
    <select name="position_id" class="form-control">
        @foreach ($positions as $position)
            <option
                value="{{ $position->position_id }}"
                @isset($sdm)
                    @if ($sdm->position_id == $position->position_id)
                    selected
                    @endif
                @endisset>
                {{ $position->name }}
            </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>NIP <span class="text-red">*</span></label>
    <input
        type="text"
        name="nip"
        class="form-control"
        required
        @isset($sdm)
            value="{{ $sdm->nip }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($sdm)
            value="{{ $sdm->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($sdm)
            value="{{ $sdm->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat <span class="text-red">*</span></label>
    <input
        type="text"
        name="address"
        class="form-control"
        required
        @isset($sdm)
            value="{{ $sdm->address }}"
        @endisset
    >
</div>

@isset($sdm)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($sdm)
                @if ($sdm->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($sdm)
                @if ($sdm->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
