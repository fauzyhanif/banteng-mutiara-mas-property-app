@extends('index')

@section('content')
<section class="content-header">
    <h1>Daftar SDM</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar SDM
                        <div class="card-tools">
                            <a href="{{ route('system.sdm.form-add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah SDM Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th width="15%">Posisi</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.RefSdm.asset.js')
@endsection

