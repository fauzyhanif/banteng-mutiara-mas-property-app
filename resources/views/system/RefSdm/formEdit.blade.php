@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.sdm') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar SDM
            </a>
        </li>
        <li class="breadcrumb-item active">Edit SDM</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#general">General</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <form id="form-add" action="{{ route('system.sdm.edit', $sdm->sdm_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="edit">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div id="response-alert"></div>

                            <div class="tab-content">
                                <div class="tab-pane container active" id="general">
                                    @include('system.RefSdm.generalForm')
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.RefSdm.asset.js')
@endsection
