<style>
    .main-card {
        margin-top: 85px;
    }

    .card-header {
        background-color: #61B15A;
        color: #fff
    }

    .logo {
        float: left;
        width: 100%;
    }

    .logo-module {
        width: 56px
    }

    .card-header > .company-name {
        /* display: inline-block; */
        margin-bottom: -7px;
        /* margin-bottom: 5px; */
    }

    .card-header > .company-name > h1 {
        font-size: 20px;
    }

    .card-header > .company-name > h2 {
        font-size: 24px;
        margin-top: -8px;
    }

    .card-header > .wrapp-button {
        float: right;
    }

    .navigation > .module > a {
        text-decoration: none;
        color: #212529;
    }

    .navigation > .module > a:hover > .card {
        text-decoration: none;
        color: #212529;
        background-color: #e6e6e6 !important;
    }

    .navigation > .role > a {
        text-decoration: none;
        color: #212529;
    }

    .navigation > .role > a:hover > .card {
        text-decoration: none;
        color: #212529;
        background-color: #e6e6e6 !important;
    }

    .module-name {
        display: inline-block;
        margin-left: 13px;
    }

    .module > a > .card > .card-body > img {
        width: 55px;
        margin-top: -40px;
    }

</style>
