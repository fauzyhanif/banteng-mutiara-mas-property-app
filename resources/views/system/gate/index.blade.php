<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

    @include('system.gate.css')
    @include('system.gate.cssResponsive')
</head>
<body>
    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="card main-card">
                            <div class="card-header">
                                {{-- <h3 class="card-title">Benteng Mutiara Mas</h3> --}}
                                {{-- <img src="{{ url('/public/img/logo_km_3.png') }}" style="width: 50%" alt="logo"> --}}
                                <div class="mt-3 wrapp-button">
                                    <button class="btn btn-sm btn-primary">
                                        <i class="fas fa-user"></i> Halaman Profil
                                    </button>
                                    <button class="btn btn-sm btn-light" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </button>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row ">
                                    <div class="col-lg-12 wrapp-module">
                                        <div class="main-title mb-3">
                                            <h4>Daftar Modul</h4>
                                            <p class="text-muted">Silahkan pilih modul dibawah ini</p>
                                        </div>

                                        <div class="navigation row">
                                            @foreach ($modules as $module)
                                                <div class="col-md-12 mb-2 module">
                                                    <a href="#" onclick="showRole('{{ $module->id_module }}')">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row module-list">
                                                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 text-center">
                                                                        <img src="{{ url('/public/system/module', $module->image) }}" class="logo-module" alt="Logo">
                                                                    </div>
                                                                    <div class="col-lg-11 col-md-10 col-sm-12 col-xs-12">
                                                                        <div class="module-name">
                                                                            <h5 class="card-title font-weight-bold">{{ $module->nama }}</h5>
                                                                            <p class="card-text text-muted">{{ $module->deskripsi }}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>

<script>
    function showRole(idModule) {
        var url = 'gate/show-role/' + idModule;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('.wrapp-module').html(res);
                if ($(window).width() <= 768) {
                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                }
            },
        });
    }
</script>
</html>
