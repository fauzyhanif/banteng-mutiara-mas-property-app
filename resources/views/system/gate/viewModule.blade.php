<div class="main-title mb-3">
    <h4>
        Daftar Module
    </h4>
    <p class="text-muted">Silahkan pilih module dibawah ini</p>
</div>
<div class="navigation row">
    @foreach ($modules as $module)
    <div class="col-md-12 mb-2 module">
        <a href="#" onclick="showRole('{{ $module->id_module }}')">
            <div class="card">
                <div class="card-body">
                    <div class="row module-list">
                        <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 text-center">
                            <img src="{{ url('/public/system/module', $module->image) }}" class="logo-module"
                                alt="Logo">
                        </div>
                        <div class="col-lg-11 col-md-10 col-sm-12 col-xs-12">
                            <div class="module-name">
                                <h5 class="card-title font-weight-bold">{{ $module->nama }}</h5>
                                <p class="card-text text-muted">{{ $module->deskripsi }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endforeach
</div>
