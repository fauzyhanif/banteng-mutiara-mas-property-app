<div class="main-title mb-3">
    <h4>
        <a href="javascript:void(0)" onclick="showModule()">
            Daftar Module
        </a>
        /
        Daftar Role
    </h4>
    <p class="text-muted">Silahkan pilih role dibawah ini</p>
</div>

<div class="navigation row">
    @php
        $arrRole = json_decode(Session::get('auth_role'))
    @endphp
    @foreach ($roles as $role)
        @if (in_array($role->id_role, $arrRole))
            <div class="col-md-12 mb-2 role">
                <a href="{{ route('gate.set.auth.role.and.module', [$role->id_role, $idModule]) }}">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $role->nama }}</h5>
                            <p class="card-text">Sistem Manajemen Property</p>
                        </div>
                    </div>
                </a>
            </div>
        @endif
    @endforeach

</div>

<script>
function showModule() {
    var url = 'gate/show-module';
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function (res) {
            $('.wrapp-module').html(res);
            if ($(window).width() <= 768) {
                $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            }
        },
    });
}
</script>
