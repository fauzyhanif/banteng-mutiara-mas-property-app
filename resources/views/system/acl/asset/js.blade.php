<script>
(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                form.trigger("reset");
                var res = successAlert(data);
                $('#response-alert').append(res);
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });

        // scroll to top
        $(window).scrollTop(0);
    });
})();

function successAlert(data) {
    $('#response-alert').html('');
    var res = '<div class="alert alert-success">';
        res += '<h5><i class="icon fas fa-check"></i> Berhasil!</h5>';
        res += data.text;
        res += '</div>';

    return res;
}

function errorAlert(data) {
    $('#response-alert').html('');
    var res = '<div class="alert alert-danger">';
        res += '<h5><i class="icon fas fa-ban"></i> Gagal!</h5>';
        $.each(data.responseJSON.errors, function(key,value) {
            res += value + "<br>";
        })
        res += '</div>';

    return res;
}
</script>
