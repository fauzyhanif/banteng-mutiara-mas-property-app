@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.acl') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Acl
            </a>
        </li>
        <li class="breadcrumb-item active">Edit Acl</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Edit Acl
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.acl.edit', $acl->id) }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @php
                                    $arrModule = json_decode($acl->id_module);
                                @endphp
                                @foreach ($modules as $module)
                                <input type="checkbox" name="id_module[]" value="{{ $module->id_module }}" @if (in_array($module->id_module, $arrModule)) checked @endif> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Role <span class="text-red">*</span></label> <br>
                                @php
                                    $arrRole = json_decode($acl->id_role);
                                @endphp
                                @foreach ($roles as $role)
                                <input type="checkbox" name="id_role[]" value="{{ $role->id_role }}" @if (in_array($role->id_role, $arrRole)) checked @endif> {{ $role->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Group Menu <span class="text-red">*</span></label> <br>
                                @php
                                    $arrGroup = json_decode($acl->id_group_menu);
                                @endphp
                                @foreach ($groups as $group)
                                <input type="checkbox" name="id_group_menu[]" value="{{ $group->id_group_menu }}" @if (in_array($group->id_group_menu, $arrGroup)) checked @endif> {{ $group->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Ditampilkan di menu sidebar?</label>
                                <p>
                                    <input type="radio" name="is_menu[]" value="N" @if($acl->is_menu == 'N') checked @endif> Tidak <br>
                                    <input type="radio" name="is_menu[]" value="Y" @if($acl->is_menu == 'Y') checked @endif> Ya <br>
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Submenu?</label>
                                <p>
                                    <input type="radio" name="is_child[]" value="N" @if($acl->is_child == 'N') checked @endif> Tidak <br>
                                    <input type="radio" name="is_child[]" value="Y" @if($acl->is_child == 'Y') checked @endif> Ya <br>
                                </p>
                            </div>

                            <div class="form-group">
                                <label>ID Parent</label>
                                <input type="number" name="parent" value="{{ $acl->parent }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Icon</label>
                                <input type="text" name="icon" value="{{ $acl->icon }}" class="form-control" placeholder="fa-loading">
                            </div>

                            <div class="form-group">
                                <label>Label Menu</label>
                                <input type="text" name="label" value="{{ $acl->label }}" class="form-control" placeholder="Acl">
                            </div>

                            <div class="form-group">
                                <label>Method</label>
                                <p>
                                    <input type="checkbox" name="method[]" value="GET" checked> GET <br>
                                    <input type="checkbox" name="method[]" value="POST"> POST <br>
                                    <input type="checkbox" name="method[]" value="PUT"> PUT <br>
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Url</label>
                                <input type="text" name="url" value="{{ $acl->url }}" class="form-control" placeholder="/system/acl">
                            </div>

                            <div class="form-group">
                                <label>Nama Route</label>
                                <input type="text" name="route" value="{{ $acl->route }}" class="form-control" placeholder="system.acl">
                            </div>

                            <div class="form-group">
                                <label>Controller</label>
                                <input type="text" name="controller" value="{{ $acl->controller }}" class="form-control" placeholder="AclController">
                            </div>

                            <div class="form-group">
                                <label>Function</label>
                                <input type="text" name="function" value="{{ $acl->function }}" class="form-control" placeholder="index">
                            </div>

                            <div class="form-group">
                                <label>Middleware</label>
                                <p>
                                    <input type="checkbox" name="middleware[]" value="auth" @if($acl->middleware == 'auth') checked @endif> auth <br>
                                    <input type="checkbox" name="middleware[]" value="gate" @if($acl->middleware == 'gate') checked @endif> gate
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Urut</label>
                                <input type="number" name="urut" value="{{ $acl->urut }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Aktif</label>
                                <p>
                                    <input type="radio" name="aktif[]" value="Y" @if ($acl->aktif == "Y") checked @endif> Ya <br>
                                    <input type="radio" name="aktif[]" value="N" @if ($acl->aktif == "N") checked @endif> Tidak
                                </p>
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.acl.asset.js')
@endsection
