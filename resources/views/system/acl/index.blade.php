@extends('index')

@section('content')
<section class="content-header">
    <h1>Acl</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Acl
                        <div class="card-tools">
                            <a href="{{ route('system.acl.form.add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Acl Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">Aksi</th>
                                    <th rowspan="2">ID</th>
                                    <th rowspan="2">URL</th>
                                    <th colspan="{{ count($modules) }}" class="text-center">Module</th>
                                    <th colspan="{{ count($roles) }}" class="text-center">Role</th>
                                </tr>
                                <tr>
                                    @foreach ($modules as $module)
                                    <th class="text-center">{{ $module->nama }}</th>
                                    @endforeach
                                    @foreach ($roles as $role)
                                    <th class="text-center">{{ $role->nama }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($acls as $acl)
                                <tr>
                                    <td>
                                        <a href="{{ route('system.acl.form.edit', $acl->id) }}" class="btn btn-secondary btn-xs">
                                            <i class="fas fa-pen-square"></i>
                                        </a>
                                        <a href="{{ route('system.acl.delete', $acl->id) }}" class="btn btn-danger btn-xs">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                    <td>{{ $acl->id }}</td>
                                    <td>{{ $acl->url }}</td>

                                    @foreach ($modules as $module)
                                    @php
                                        $checkModule = ""
                                    @endphp
                                    @if (in_array($module->id_module, json_decode($acl->id_module)))
                                    @php
                                        $checkModule = "checked"
                                    @endphp
                                    @endif
                                    <td class="text-center">
                                        <input type="checkbox" name="id_module[]" {{ $checkModule }} value="{{ $module->id }}">
                                    </td>
                                    @endforeach

                                    @foreach ($roles as $role)
                                    @php
                                        $checkRole = ""
                                    @endphp
                                    @if (in_array($role->id_role, json_decode($acl->id_role)))
                                    @php
                                        $checkRole = "checked"
                                    @endphp
                                    @endif
                                    <td class="text-center">
                                        <input type="checkbox" name="id_role[]" {{ $checkRole }} value="{{ $role->id }}">
                                    </td>
                                    @endforeach

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
