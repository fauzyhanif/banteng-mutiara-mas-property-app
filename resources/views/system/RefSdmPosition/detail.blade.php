@extends('index')

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('travel.customer') }}">
				<i class="fas fa-long-arrow-alt-left"></i> &nbsp;
				Daftar Customer
			</a>
		</li>
		<li class="breadcrumb-item active">Detail Customer</li>
	</ol>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">
							Detail customer
						</h3>
					</div>
					<div class="card-body">
						<div class="row mb-2">
							<div class="col-md-4">Nama</div>
							<div class="col-md-8">: <b>{{ $customer->name }}</b></div>
						</div>
						<div class="row mb-2">
							<div class="col-md-4">No Telp / Hp </div>
							<div class="col-md-8">: <b>{{ $customer->phone_num }}</b></div>
						</div>
						<div class="row mb-2">
							<div class="col-md-4">Alamat</div>
							<div class="col-md-8">: <b>{{ $customer->address }}</b></div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-7">
				<div class="row">
					<div class="col-lg-6 col-12">
						<!-- small box -->
						<div class="small-box bg-info">
							<div class="inner">
								<h3>{{ $customer->ttl_trnsct }}x</h3>

								<p>Total Transaksi</p>
							</div>
							<div class="icon">
								<i class="fas fa-car"></i>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-12">
						<!-- small box -->
						<div class="small-box bg-warning">
							<div class="inner">
								<h3>Rp. {{ GeneralHelper::rupiah($customer->ttl_payment) }}</h3>

								<p>Total Pembayaran</p>
							</div>
							<div class="icon">
								<i class="fas fa-wallet"></i>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Sejarah Transaksi</h3>
							</div>
							<div class="card-body scroll-x">
								<table class="table">
									<thead>
										<th>No</th>
										<th>Kode Booking</th>
										<th>Tujuan</th>
										<th>Tgl Berangkat</th>
									</thead>
									<tbody>
										<tr>
                                            <td colspan="4" class="text-center">Loading...</td>
                                        </tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@include('travel.RefCustomer.asset.js')
@endsection
