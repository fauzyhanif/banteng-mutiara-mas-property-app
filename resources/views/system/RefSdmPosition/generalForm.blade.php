<div class="form-group">
    <label>ID Jabatan <span class="text-red">*</span></label>
    <input
        type="text"
        name="position_id"
        class="form-control"
        required
        @isset($position)
            readonly
            value="{{ $position->position_id }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Nama Jabatan <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($position)
            value="{{ $position->name }}"
        @endisset
    >
</div>

@isset($position)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($position)
                @if ($position->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($position)
                @if ($position->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
