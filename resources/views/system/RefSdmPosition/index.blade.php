@extends('index')

@section('content')
<section class="content-header">
    <h1>Daftar Jabatan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Jabatan
                        <div class="card-tools">
                            <a href="{{ route('system.jabatan.form-add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Jabatan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th width="15%">ID Jabatan</th>
                                <th>Nama Jabatan</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.RefSdmPosition.asset.js')
@endsection

