<table class="table table-sm table-bordered table-hover">
    <thead class="bg-info">
        <th>Unit</th>
        <th width="25%">Jml Pengerjaan</th>
    </thead>
    <tbody>
        @foreach ($units as $unit)
            <tr>
                <td>
                    <a href="{{ url('prop/unit/worklist_form', $unit->unit_id) }}">
                        {{ $unit->location_name . ' ' . $unit->block_name . ' No ' . $unit->number }}
                    </a>
                </td>
                <td>{{ $unit->jml_job }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@if ($units->lastPage() > 1)
<ul class="pagination mt-2">
    <li class="page-item {{ ($units->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="progresPembangunanSelesai(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $units->lastPage(); $i++)
        <li class="page-item {{ ($units->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="progresPembangunanSelesai({{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($units->currentPage() == $units->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="progresPembangunanSelesai({{ $units->url($units->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif

