<table class="table table-sm table-bordered table-striped">
    <thead class="bg-info">
        <th>Lokasi</th>
        <th>Blok</th>
        <th class="text-center" width="25%">Unit Ready</th>
    </thead>
    <tbody>
        @foreach ($datas as $data)
        <tr>
            <td>{{ $data->location_name }}</td>
            <td>
                <a href="{{ url('/prop/sales/list_unit_second/' . $data->location_id . '/' . $data->block_id) }}" target="_blank">
                    {{ $data->block_name }}
                </a>
            </td>
            <td class="text-center">{{ $data->jml }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
<ul class="pagination mt-2">
    <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="resumeUnitReady(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $datas->lastPage(); $i++)
        <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="resumeUnitReady({{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="resumeUnitReady({{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif
