<div class="modal" id="modal-list-pembangunan-belum-selesai">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Progress Pembangunan (Belum Selesai)</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="py-2">
                    <h4>
                        Blok : <span class="block-name"></span>
                    </h4>
                </div>
                <table class="table table-bordered table-sm table-hover">
                    <thead class="bg-info">
                        <th width="5%">No</th>
                        <th>Unit</th>
                        <th width="25%">Jumlah Pengerjaan</th>
                    </thead>
                    <tbody id="list-data"></tbody>
                </table>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
