<div class="bg-purple mb-2">
    <div class="p-3">
        <h5>Total operasional belum terbayar : </h5>
        <h3 class="font-weight-bold">{{ GeneralHelper::rupiah($total) }}</h3>
    </div>
</div>

<table class="table table-sm table-bordered table-hover">
    <thead class="bg-info">
        <th>Penerima</th>
        <th class="text-right" width="20%">Jumlah</th>
    </thead>
    <tbody>
        @foreach ($datas as $data)
            <tr>
                <td>
                    <a class="font-weight-bold" href="{{ url('finance/cost/detail', $data->finance_id) }}" target="_blank">
                        {{ $data->sdm->name }}
                    </a>
                    <br>
                    <span class="text-muted">{{ $data->finance_desc }}</span>
                </td>
                <td class="text-right">
                    {{ GeneralHelper::rupiah($data->finance_amount) }}
                    @if ($data->finance_amount - $data->finance_amount_paid > 0)
                        <br>
                        <span style="font-size: 12px">
                            <span class="text-muted">Sisa</span>
                            <span class="text-danger"> {{ GeneralHelper::rupiah($data->finance_amount - $data->finance_amount_paid) }}</span>
                        </span>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
    <ul class="pagination mt-2">
        <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="operasional(1)">Previous</a>
        </li>
        @for ($i = 1; $i <= $datas->lastPage(); $i++)
            <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
                <a class="page-link" href="javascript:void(0)" onclick="operasional({{ $i }})">{{ $i }}</a>
            </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="operasional({{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
    </ul>
@endif

