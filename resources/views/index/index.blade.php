@extends('index')

@section('content')
<section class="content-header">
    <h1>Dashboard</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Progress Pembangunan (Belum Selesai)
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm datatable-progres-pembangunan-belum-selesai">
                                <thead class="bg-info">
                                    <th>Blok</th>
                                    <th class="text-center">Jml Pengerjaan</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Progress Pembangunan (Selesai)
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm datatable-progres-pembangunan-selesai">
                                <thead class="bg-info">
                                    <th>Blok</th>
                                    <th class="text-center">Jml Pengerjaan</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Unit Ready
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm datatable-resume-unit-ready">
                                <thead class="bg-info">
                                    <th>Lokasi</th>
                                    <th>Blok</th>
                                    <th class="text-center">Unit Ready</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Booking Terbaru
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-8">
                                <input type="text" name="list_booking_range_date" class="form-control form-control-sm" value="{{ date('Y-m-01') . ' - ' . date('Y-m-d') }}">
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary btn-sm" onclick="listBooking(1)"><i class="fas fa-filter"></i> FILTER</button>
                            </div>
                        </div>
                        <div class="table-responsive" id="list-booking">
                            <table class="table table-bordered table-hover table-sm">
                                <thead class="bg-info">
                                    <th width="50%">Unit</th>
                                    <th width="50%">Konsumen</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Progres KPR
                        </h5>
                    </div>
                    <div class="card-body" id="progres-kpr">
                        <table class="table table-sm table-bordered table-striped">
                            <thead class="bg-info">
                                <th width="70%">Keterangan</th>
                                <th class="text-center">Jml Unit</th>
                            </thead>
                            <tbody>
                                @foreach ($steps as $step)
                                    <tr>
                                        <td>{{ $step->name }}</td>
                                        <td class="text-center">0</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Pengajuan Pencairan Dana
                        </h5>
                        <div class="card-tools">
                            <a href="{{ url('finance/pengajuan_pencairan') }}">
                                Lihat Semua Pengajuan
                            </a>
                        </div>
                    </div>
                    <div class="card-body" id="pengajuan-pencairan-selesai">

                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Operasional
                        </h5>
                        <div class="card-tools">
                            <a href="{{ url('finance/cost') }}">
                                Lihat Semua Operasional
                            </a>
                        </div>
                    </div>
                    <div class="card-body" id="operasional">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('index.modal_list_pembangunan_belum_selesai')
@include('index.modal_list_pembangunan_selesai')

<script>
    $(function() {
        progresPembangunanBelumSelesai(1);
        progresPembangunanSelesai(1);
        pengajuanPencairan();
        progresKpr();
        operasional();
        resumeUnitReady(1);
        listBooking(1);

        $('input[name="list_booking_range_date"]').daterangepicker({
            locale : {
                format : 'YYYY-MM-DD'
            }
        });
    });

    function progresPembangunanBelumSelesai(current_page) {
        var base = {!! json_encode(route('dashboard.progres_pembangunan_belum_selesai')) !!};
        $('.datatable-progres-pembangunan-belum-selesai').DataTable({
            ordering: false,
            info: false,
            info: false,
            processing: true,
            serverSide: true,
            ajax: base,

            columns: [
                { data: 'block', name: 'block', searchable: true },
                { data: 'jml_job', name: 'jml_job', searchable: false, className: 'text-center' },
            ]
        });
    }

    function showModalListPembangunanBelumSelesai(block_id, block_name) {
        var modal = $('#modal-list-pembangunan-belum-selesai').modal('show');
        modal.find('.block-name').text(block_name)

        var url = {!! json_encode(url('dashboard/progres_pembangunan_belum_selesai/list_unit')) !!} + '/' + block_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                modal.find('#list-data').html(res);
            }
        })
    }

    function progresPembangunanSelesai(current_page) {
        var base = {!! json_encode(url('dashboard/progres_pembangunan_selesai')) !!};
        $('.datatable-progres-pembangunan-selesai').DataTable({
            ordering: false,
            info: false,
            info: false,
            processing: true,
            serverSide: true,
            ajax: base,

            columns: [
                { data: 'block', name: 'block', searchable: true },
                { data: 'jml_job', name: 'jml_job', searchable: false, className: 'text-center' },
            ]
        });
    }

    function showModalListPembangunanSelesai(block_id, block_name) {
        var modal = $('#modal-list-pembangunan-selesai').modal('show');
        modal.find('.block-name').text(block_name)

        var url = {!! json_encode(url('dashboard/progres_pembangunan_selesai/list_unit')) !!} + '/' + block_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                modal.find('#list-data').html(res);
            }
        })
    }

    function pengajuanPencairan() {
        var url = {!! json_encode(route('dashboard.pengajuan_pencairan')) !!};
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#pengajuan-pencairan').html(res);
            }
        })
    }

    function progresKpr() {
        var url = {!! json_encode(route('dashboard.progres_kpr')) !!};
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#progres-kpr').html(res);
            }
        })
    }

    function operasional(current_page) {
        var url = {!! json_encode(url('dashboard/operasional?page=')) !!}+current_page;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#operasional').html(res);
            }
        })
    }

    function resumeUnitReady(current_page) {
        var base = {!! json_encode(url('dashboard/resume_unit_ready')) !!};
        $('.datatable-resume-unit-ready').DataTable({
            ordering: false,
            info: false,
            info: false,
            processing: true,
            serverSide: true,
            ajax: base,

            columns: [
                { data: 'location_name', name: 'location_name', searchable: true },
                { data: 'html_block', name: 'html_block', searchable: true },
                { data: 'jml', name: 'jml', searchable: false, className: 'text-center' },
            ]
        });
    }

    function listBooking(current_page) {
        var list_booking_range_date = $('input[name="list_booking_range_date"]').val().split(' - ');
        var start_date = list_booking_range_date[0];
        var end_date = list_booking_range_date[1];
        var url = {!! json_encode(url('dashboard/list_booking?page=')) !!}+current_page+'&start_date='+start_date+'&end_date='+end_date;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#list-booking').html(res);
            }
        })
    }
</script>
@endsection
