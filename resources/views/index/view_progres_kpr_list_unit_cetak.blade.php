<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dokumen</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    <style>
        body {
            font-size: 11px
        }
    </style>
</head>
<body>
    <body onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                <div class="row kop-surat">
                    <div class="col-md-1 col-print-1">
                        <img src="{{ url('public/img/logo_bmm.jpeg') }}" style="max-width: 50px; margin-top: -10px;"
                            alt="" class="logo-kop">
                    </div>
                    <div class="col-md-11 col-print-11">
                        <h4><b>PT LAN SENA JAYA</b></h4>
                        <p style="margin-top: -10px; font-size: 11px">DEVELOPER & CONTRACTOR</p>
                    </div>
                </div>

                <hr style="margin-top: -10px">

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="font-weight-bold mb-4">DAFTAR UNIT PENJUALAN KPR ({{ $step_name }})</p>

                        <table class="table table-sm table-bordered">
                            <thead>
                                <th width="3%" class="text-center">No</th>
                                <th width="8%">Tgl Booking</th>
                                <th width="22%">Nomor Unit</th>
                                <th width="25%">Konsumen</th>
                                <th width="10%">No Telp</th>
                                <th width="15%">Keterangan</th>
                                <th width="12%">Uang Masuk</th>
                                <th width="12%">Sisa</th>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @foreach ($datas as $data)
                                    <tr>
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ date("d-m-Y", strtotime($data->sales_date)) }}</td>
                                        <td>{{ $data->location->name . ' ' . $data->block->name . ' NO ' . $data->unit_id }} </td>
                                        <td>{{ $data->customer->name . ' (' . $data->customer->institute . ')' }} </td>
                                        <td>{{ $data->customer->phone_num }} </td>
                                        <td>{{ ($data->step_sales != "0" && $data->step_sales != "") ? $data->sales_step->step_desc : '-' }} </td>
                                        <td>{{ GeneralHelper::rupiah($data->ttl_trnsct_paid) }} </td>
                                        <td>{{ GeneralHelper::rupiah($data->ttl_trnsct - $data->ttl_trnsct_paid) }} </td>
                                    </tr>
                                @php $no += 1; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </body>
</body>
</html>
