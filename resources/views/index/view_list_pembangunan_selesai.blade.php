@php $no = 1; @endphp
@foreach ($datas as $item)
    <tr>
        <td>{{ $no }}.</td>
        <td>
            <a href="{{ url('/prop/unit/worklist_form', $item->unit_id) }}" target="_blank" rel="noopener noreferrer">
                Unit No {{ $item->number }}
            </a>
        </td>
        <td>{{ $item->jml_job }}</td>
    </tr>
@php $no += 1; @endphp
@endforeach
