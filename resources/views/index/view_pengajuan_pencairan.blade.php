<table class="table table-sm table-bordered table-striped">
    <thead class="bg-info">
        <th width="70%">Keterangan</th>
        <th>Jumlah</th>
    </thead>
    <tbody>
        @foreach ($datas as $data)
            <tr>
                <td>
                    {{ $data->submission_department }} <br>
                    <span class="text-secondary">{{ GeneralHelper::konversiTgl($data->submission_deadline) }}</span>
                </td>
                <td>{{ GeneralHelper::rupiah($data->submission_amount) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
