@extends('index')

@section('content')
<section class="content-header">
    <h2>Laporan Step Progress Penjualan KPR</h2>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <b>{{ $step_name }}</b>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Unit</h3>

                        <div class="card-tools">
                            <a href="{{ url('/dashboard/progres_kpr/list_unit_cetak?step_id='.$step_id) }}" target="_blank" class="btn btn-sm btn-primary">
                                <i class="fas fa-print"></i> Cetak
                            </a>
                        </div>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-sm table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th width="15%">Nomor Unit</th>
                                <th>Konsumen</th>
                                <th width="15%">Tgl Booking</th>
                                <th width="15%">No Telp</th>
                                <th width="15%">Tgl Step</th>
                                <th width="12%">Keterangan</th>
                                <th width="12%">Uang Masuk</th>
                                <th width="12%">Sisa</th>
                                <th width="12%">Detail</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function() {
    var base = {!! json_encode(url('/dashboard/progres_kpr/list_unit_json?step_id='.$step_id)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'type_number', name: 'type_number', searchable: true },
            { data: 'customer', name: 'customer', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: true },
            { data: 'customer_phone', name: 'customer_phone', searchable: true },
            { data: 'step_sales_date_html', name: 'step_sales_date_html', searchable: true },
            { data: 'sales_step_desc', name: 'sales_step_desc', searchable: true },
            { data: 'ttl_trnsct_paid', name: 'ttl_trnsct_paid', searchable: true },
            { data: 'sisa', name: 'sisa', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: true },
        ]
    });
});
</script>
@endsection
