<section class="lists-container">
    <div class="list">
        <h3 class="list-title">Unit ({{ $units->count() }})</h3>

        <ul class="list-items">
            @foreach ($units as $unit)
            <a href="{{ url('prop/unit/detail', $unit->unit_id) }}" target="_blank" style="color: black">
                <li>
                    <b>{{ $unit->block->name }} No {{ $unit->number }}</b> <br>
                    <span class="text-secondary">Tipe {{ $unit->unitType->name }}</span>

                    <br><br>
                    @if ($unit->is_publish == '0')
                        <span class="badge badge-danger">Belum Siap Jual</span>
                    @else
                        <span class="badge badge-success">Siap Jual</span>
                    @endif
                </li>
            </a>
            @endforeach
        </ul>
    </div>

    @foreach ($steps as $step)
        <div class="list">
            <h3 class="list-title">{{ $step->name }} ({{ count($step->sales) }})</h3>

            <ul class="list-items">
                @if (count($step->sales) > 0)
                    @foreach ($step->sales as $sales)
                        <a href="{{ url('prop/sales/detail', $sales->id) }}" target="_blank" style="color: black">
                            <li>
                                <b>{{ $sales->block->name }} No {{ $sales->unit_id }}</b> <br>
                                <span class="text-secondary">{{ $sales->customer->name }} ({{ $sales->customer->phone_num }})</span> <br><br>
                                Booking : {{ date("d-m-Y", strtotime($sales->sales_date)) }} <br>
                                Step ini : {{ ($sales->step_sales_date != null) ? date("d-m-Y", strtotime($sales->step_sales_date)) : '-' }} <br>
                                Ket : {{ $sales->sales_step_desc }}
                            </li>
                        </a>
                    @endforeach
                @endif
            </ul>
        </div>
    @endforeach

</section>
