@extends('index')

@section('content')

<div id="top-page"></div>

<section class="content-header">
    <h1>Dashboard Progress</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 d-inline">
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-filter">
                    <i class="fas fa-filter"></i> Filter
                </button>

                <button class="btn btn-sm btn-default" data-slide="prev" onclick="prev()">
                    <i class="fas fa-angle-left"></i> Sebelumnya
                </button>

                <button class="btn btn-sm btn-default btn-pause" onclick="pause()">
                    <i class="fas fa-stop"></i> Pause
                </button>

                <button class="btn btn-sm btn-default btn-cycle" onclick="cycle()">
                    <i class="fas fa-play"></i> Jalankan
                </button>

                <button class="btn btn-sm btn-default" data-slide="next" onclick="next()">
                    Selanjutnya <i class="fas fa-angle-right"></i>
                </button>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-body">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <h4><b>Unit Ready</b></h4>
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-sm datatable-resume-unit-ready">
                                            <thead class="bg-info">
                                                <th width="20%">Lokasi</th>
                                                <th width="10%">Blok</th>
                                                <th width="10%">Jml Unit</th>
                                                <th>Nomor Unit</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($blocks as $block)
                                                <tr>
                                                    <td>{{ $block->location_name }}</td>
                                                    <td>{{ $block->block_name }}</td>
                                                    <td>{{ $block->jml }}</td>
                                                    <td>
                                                        @foreach ($res_units[$block->block_id] as $unit)
                                                            {{ $unit }},
                                                        @endforeach
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                @foreach ($steps as $step)
                                <div class="carousel-item">
                                    <h4><b>{{ $step->name }} ({{ count($step->sales) }})</b></h4>
                                    <div class="table-responsive mt-3">
                                        <table class="table table-bordered table-hover table-sm">
                                            <thead class="bg-info">
                                                <th width="20%">Unit</th>
                                                <th width="20%">Konsumen</th>
                                                <th width="15%">Tgl Booking</th>
                                                <th width="15%">Tgl Step</th>
                                                <th width="20%">Keterangan</th>
                                                <th width="10%">Aksi</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($step->sales->sortBy('block_name') as $sales)
                                                <tr>
                                                    <td><b>{{ $sales->block->name }} No {{ $sales->unit_id }}</b></td>
                                                    <td>{{ $sales->customer->name }} ({{ $sales->customer->phone_num }})
                                                    </td>
                                                    <td>{{ date("d-m-Y", strtotime($sales->sales_date)) }}</td>
                                                    <td>{{ ($sales->step_sales_date != null) ? date("d-m-Y", strtotime($sales->step_sales_date)) : '-' }}
                                                    </td>
                                                    <td>
                                                        @php

                                                        @endphp
                                                        @if ($sales->sales_step)
                                                            {{ $sales->sales_step->step_desc }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('prop/sales/detail', $sales->id) }}" class="btn btn-xs btn-info" target="_blank">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                        <a href="{{ url('prop/sales/print_sppr', $sales->id) }}" class="btn btn-xs btn-secondary" target="_blank">
                                                            <i class="fas fa-print"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal" id="modal-filter">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Filter</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="" method="POST">
                @csrf
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Blok yang ditamplikan :</label>
                            <div class="form-group" id="checkbox1">
                                @foreach ($ref_block as $block)
                                    @if (count($block->units) > 0)
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" name="filter_blocks[]"
                                                id="customCheckbox1{{ $block->block_id }}" value="{{ $block->block_id }}"
                                                @if (array_key_exists($block->block_id,$key_blocks))
                                                    checked
                                                @endif
                                            >
                                            <label for="customCheckbox1{{ $block->block_id }}" class="custom-control-label">{{ $block->location->name }} - {{ $block->name }}</label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">Progres yang ditampilkan :</label>
                            <div class="form-group" id="checkbox2">
                                @foreach ($ref_step as $step)
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="filter_steps[]"
                                        id="customCheckbox2{{ $step->step_id }}" value="{{ $step->step_id }}" @if (array_key_exists($step->step_id,
                                    $key_steps))
                                    checked
                                    @endif
                                    >
                                    <label for="customCheckbox2{{ $step->step_id }}" class="custom-control-label">{{ $step->name }}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Terapkan</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="bottom-page"></div>
<script>



$('.carousel').carousel({
    interval: 20000
})

function prev() {
    $('.carousel').carousel('prev')
}

function next() {
    $('.carousel').carousel('next')
}

function pause() {
    $('.carousel').carousel('pause')
}

function cycle() {
    $('.carousel').carousel('cycle')
}

// var second = 0;
// setInterval(timers, 1000);

// function scrollDown() {
//     var hash = '#bottom-page';
//     $('html, body').animate({
//         scrollTop: $(hash).offset().top
//     }, 24000, function(){

//         // Add hash (#) to URL when done scrolling (default click behavior)
//         window.location.hash = hash;
//     });
// }

// function scrollTop() {
//     var hash = '#top-page';
//     $('html, body').animate({
//         scrollTop: $(hash).offset().top
//     }, 500, function(){

//         // Add hash (#) to URL when done scrolling (default click behavior)
//         window.location.hash = hash;
//     });
// }

// function timers() {
//     second++;
//     if (second == 1) {
//         scrollDown()
//     }

//     if(second == 24) {
//         scrollTop()
//     }

//     if (second == 25) {
//         second = 0;
//     }

//     console.log(second)
// }
</script>
@endsection
