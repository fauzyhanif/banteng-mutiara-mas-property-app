<table class="table table-sm table-bordered table-striped">
    <thead class="bg-info">
        <th width="70%">Keterangan</th>
        <th class="text-center">Jml Unit</th>
    </thead>
    <tbody>
        @foreach ($steps as $step)
        <tr>
            <td>
                <a href="{{ url('/dashboard/progres_kpr/list_unit?step_id=' . $step->step_id) }}">
                    {{ $step->name }}
                </a>
            </td>
            <td class="text-center">
                @if (array_key_exists($step->step_id, $data))
                    {{ $data[$step->step_id] }}
                @else
                    0
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
