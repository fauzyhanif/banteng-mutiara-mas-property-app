@if ($datas->total() > 0)
    <p><span class="text-muted">Total Data</span> : <span class="font-weight-bold">{{ $datas->total() }}</span></p>
@endif


<table class="table table-sm table-bordered table-hover">
    <thead class="bg-info">
        <th width="50%">Unit</th>
        <th width="50%">Konsumen</th>
    </thead>
    <tbody>
        @forelse ($datas as $data)
            <tr>
                <td>
                    <a href="{{ url('prop/sales/detail', $data->id) }}" target="_blank">
                        {{ $data->block->name }} NO {{ $data->unit_id }}
                    </a> <br>
                    <span class="text-muted">Booking : {{ GeneralHelper::konversiTgl($data->sales_date, 'ttd') }}</span>

                    @if ($data->affiliate_num != '')
                        <br>
                        <span class="text-muted">Marketing : {{ $data->marketer->sdm->name }}</span>
                    @endif
                </td>
                <td>
                    {{ $data->customer->name }} <br>
                    <span class="text-muted">{{ $data->customer->phone_num }}</span>
                </td>
            </tr>
        @empty
            <tr>
                <td class="text-center" colspan="2">Data tidak ditemukan</td>
            </tr>
        @endforelse
    </tbody>
</table>

@if ($datas->lastPage() > 1)
    <ul class="pagination mt-2">
        <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="listBooking(1)">Previous</a>
        </li>
        @for ($i = 1; $i <= $datas->lastPage(); $i++)
            <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
                <a class="page-link" href="javascript:void(0)" onclick="listBooking({{ $i }})">{{ $i }}</a>
            </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="listBooking({{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
    </ul>
@endif

