<form name="marketing_fee" action="{{ route('prop.sales.detail_marketing_fee_store') }}" method="POST">

    @csrf

    <input type="hidden" name="id" value="{{ $sales->id }}">
    <input type="hidden" name="sales_id" value="{{ $sales->sales_id }}">
    <input type="hidden" name="location_id" value="{{ $sales->location_id }}">
    <input type="hidden" name="block_id" value="{{ $sales->block_id }}">
    <input type="hidden" name="unit_id" value="{{ $sales->unit_id }}">
    <input type="hidden" name="affiliate_num" value="{{ $sales->affiliate_num }}">
    <input type="hidden" name="affiliate_fee" value="{{ $sales->affiliate_fee }}">
    <input type="hidden" name="affiliate_fee_paid" value="{{ $sales->affiliate_fee_paid }}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Form Input Cicilan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <table class="table table-sm table-borderless">
                    <tbody>
                        <tr>
                            <td width="25%">Marketer</td>
                            <td width="3%" class="text-right">:</td>
                            <td width="72%">{{ $sales->marketer->sdm->name }}</td>
                        </tr>
                        <tr>
                            <td width="25%">No HP</td>
                            <td width="3%" class="text-right">:</td>
                            <td width="72%">{{ $sales->marketer->sdm->phone_num }}</td>
                        </tr>
                        <tr>
                            <td width="25%">Alamat</td>
                            <td width="3%" class="text-right">:</td>
                            <td width="72%">{{ $sales->marketer->sdm->address }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12">
                <table class="table table-sm table-borderless">
                    <tbody>
                        <tr>
                            <td width="25%">Unit</td>
                            <td width="3%" class="text-right">:</td>
                            <td width="72%">Blok {{ $sales->block->name }} No {{ $sales->unit->number }}  </td>
                        </tr>
                        <tr>
                            <td width="25%">Status</td>
                            <td width="3%" class="text-right">:</td>
                            <td width="72%">{{ $sales->last_sales_step->name }}</td>
                        </tr>
                        <tr>
                            <td width="25%">Konsumen</td>
                            <td width="3%" class="text-right">:</td>
                            <td width="72%">{{ $sales->customer->name }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        @if ($sales->affiliate_fee_paid > 0)
            <div class="row mb-4">
                <div class="col-md-12">
                    <table class="table-bordered table-hover table-sm" style="width: 100%">
                        <thead class="bg-info">
                            <th width="5%" class="text-center">No</th>
                            <th width="15%">Tanggal</th>
                            <th width="25%">No Kwitansi</th>
                            <th>Akun Kas / Bank</th>
                            <th width="20%" class="text-right">Nominal</th>
                            <th width="10%">Cetak</th>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($histories as $history)
                                <tr>
                                    <td class="text-center">{{ $no }}.</td>
                                    <td>{{ date("d-m-Y", strtotime($history->trnsct->finance_date)) }}</td>
                                    <td>{{ $history->finance_paid_num }}</td>
                                    <td>{{ $history->cash_bank->cash_name }}</td>
                                    <td class="text-right">{{ number_format($history->finance_paid_amount,0,',','.') }}</td>
                                    <td>
                                        <a href="{{ route('finance.cost.print_payment', $history->finance_paid_id)}}" target='_blank' class="btn btn-sm btn-warning">Cetak</a>
                                    </td>
                                </tr>
                            @php $no += 1; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

        @if (($sales->affiliate_fee - $sales->affiliate_fee_paid) > 0)
            <div class="form-group">
                <label>Tanggal <span class="text-red">*</span></label>
                <input type="date" name="date" class="form-control" value="{{ date('Y-m-d') }}" required>
            </div>

            <div class="form-group">
                <label>Uang diambil dari <span class="text-red">*</span></label>
                <select name="cash_id" class="form-control" required>
                    <option value="">** Pilih Rekening</option>
                    @foreach ($cashes as $cash)
                    <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Sebesar <span class="text-red">*</span></label>
                <input type="text" name="amount" class="form-control money" value="{{ $sales->affiliate_fee - $sales->affiliate_fee_paid }}">
            </div>

            <div class="form-group">
                <label>Keterangan</label>
                <textarea name="desc" class="form-control">Pencairan marketing fee</textarea>
            </div>
        @endif
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        @if (($sales->affiliate_fee - $sales->affiliate_fee_paid) > 0)
            <button type="submit" class="btn btn-primary">Simpan</button>
        @endif
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
</form>

<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

(function() {
    $('form[name="marketing_fee"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }

                $('#modal-form-pencairan-hak').modal('hide')
                $('.datatable-hak').DataTable().ajax.reload(null, false);

            },
            error: function (data) {
                toastr.error(data)
            }
        });
    });
})();
</script>
