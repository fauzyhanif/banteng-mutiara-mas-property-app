@extends('index')

@section('content')

<input type="hidden" name="status" value="{{ $status }}">

<section class="content-header">
    <h1>Hak Marketer</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-md-12">
                <a href="{{ url('prop/marketer/hak?status=ALL') }}" class="btn {{ ($status == 'ALL') ? 'btn-primary' : 'btn-outline-primary' }}  mr-2">
                    <i class="fas fa-list"></i>&nbsp; Semua Data
                </a>
                <a href="{{ url('prop/marketer/hak?status=NOT_YET') }}" class="btn {{ ($status == 'NOT_YET') ? 'btn-primary' : 'btn-outline-primary' }}  mr-2">
                    <i class="fas fa-spinner"></i>&nbsp; Belum Terpenuhi
                </a>
                <a href="{{ url('prop/marketer/hak?status=DONE') }}" class="btn {{ ($status == 'DONE') ? 'btn-primary' : 'btn-outline-primary' }} ">
                    <i class="fas fa-check"></i>&nbsp; Sudah Terpenuhi
                </a>
                <a href="{{ url('prop/marketer/hak?status=KPR_SUDAH_AKAD') }}" class="btn {{ ($status == 'KPR_SUDAH_AKAD') ? 'btn-primary' : 'btn-outline-primary' }} ">
                    <i class="fas fa-check"></i>&nbsp; KPR Sudah Akad
                </a>
                <a href="{{ url('prop/marketer/hak?status=KPR_DP_LUNAS') }}" class="btn {{ ($status == 'KPR_DP_LUNAS') ? 'btn-primary' : 'btn-outline-primary' }} ">
                    <i class="fas fa-check"></i>&nbsp; KPR DP Lunas
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Hak Marketer
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-sm table-bordered table-striped datatable-hak">
                            <thead class="bg-info">
                                <th>Marketer</th>
                                <th>Konsumen</th>
                                <th>Unit</th>
                                <th width="12%">Hak Marketer</th>
                                <th width="12%">Hak terbayar</th>
                                <th width="12%">Sisa</th>
                                <th width="12%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_marketer.form_pencairan_hak')

<script>
$(function() {
    loadData();
});

function loadData() {
    var status = $('input[name="status"]').val();
    var base = {!! json_encode(url('prop/marketer/hak_list?status=')) !!} + status;
    $('.datatable-hak').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'marketer_html', name: 'marketer_html', searchable: true },
            { data: 'customer_html', name: 'customer_html', searchable: true },
            { data: 'unit_html', name: 'unit_html', searchable: true },
            { data: 'hak_marketer_html', name: 'hak_marketer_html', searchable: true },
            { data: 'hak_marketer_paid_html', name: 'hak_marketer_paid_html', searchable: false },
            { data: 'hak_marketer_sisa_html', name: 'hak_marketer_sisa_html', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function show_modal_pencairan_hak(id) {
    $('#modal-form-pencairan-hak').modal('show');

    var url = {!! json_encode(url('prop/marketer/hak_detail?id=')) !!} + id;

    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function (res) {
            $('#form-pencairan-hak-view').html(res);
        }
    });
}
</script>
@endsection
