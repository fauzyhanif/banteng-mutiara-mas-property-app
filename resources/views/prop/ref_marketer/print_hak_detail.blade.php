<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FORMULIR REFERENSI KONSUMEN</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')
                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold">
                            FORMULIR REFERENSI KONSUMEN <br>
                            PERUMAHAN BENTENG MUTIARA MAS
                        </p>

                        <table class="table table-bordered table-sm mt-3">
                            <tbody>
                                <tr>
                                    <td width="30%">Nama</td>
                                    <td>{{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Blok/No</td>
                                    <td>{{ $sales->block->name }} No {{ $sales->unit_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Referensi/Marketing</td>
                                    <td>{{ $sales->marketer->sdm->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Tgl Booking</td>
                                    <td>{{ GeneralHelper::konversiTgl($sales->sales_date) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, ........................................</div>

                    <div class="col-md-6 col-print-6 text-center mb-4">Konsumen,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Referensi,</div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->customer->name }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->marketer->sdm->name }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
