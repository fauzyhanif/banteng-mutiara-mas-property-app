<script>
$(function() {
    loadData();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update') {
                    form.trigger("reset");
                }

                if (res.status == 'success') {
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="marketer_create_user"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                form.trigger("reset");
                toastr.success(res.text);
                $('.datatable').DataTable().ajax.reload(null, false);
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="marketer_reset_password"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                form.trigger("reset");
                toastr.success(res.text);
                $('.datatable').DataTable().ajax.reload(null, false);
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="marketer_delete"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.type == 'success') {
                    form.trigger("reset");
                    toastr.success(res.text);
                    $('.datatable').DataTable().ajax.reload(null, false);
                } else {
                    toastr.warning(res.text);
                }

                $('#form-delete').modal('hide');
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('prop.marketer.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'name_category', name: 'name_category', searchable: true },
            { data: 'affiliate_num', name: 'affiliate_num', searchable: true },
            { data: 'rekening', name: 'rekening', searchable: true },
            { data: 'phone_address', name: 'phone_address', searchable: true },
            { data: 'html_have_login', name: 'html_have_login', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function show_modal_create_user(sdm_id, email, phone_num) {
    $('#form-create-user').modal('show');
    $('input[name="sdm_id"]').val(sdm_id);
    $('input[name="email"]').val(email);
    $('input[name="no_telp"]').val(phone_num);
}

function show_modal_reset_password(sdm_id, email, phone_num) {
    $('#form-reset-password').modal('show');
    $('input[name="sdm_id"]').val(sdm_id);
    $('#email-string').text(email);
    $('#phone-num-string').text(phone_num);
}

function show_modal_delete(sdm_id, name, affiliate_num) {
    $('#form-delete').modal('show');
    $('input[name="sdm_id"]').val(sdm_id);
    $('input[name="affiliate_num"]').val(affiliate_num);
    $('#marketer-name').text(name);
}
</script>
