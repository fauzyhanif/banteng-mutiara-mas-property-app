@if(!isset($data))
<div class="form-group">
    <label for="">Sudah pernah terdaftar sebagai Mandor/Suplier/Customer?</label>
    <select name="is_exist" class="form-control" onchange="is_exists(this.value)">
        <option value="0">Belum pernah</option>
        <option value="1">Sudah pernah</option>
    </select>
</div>

<div class="form-group sdm-exists" style="display: none">
    <label for="">Nama User</label>
    <select name="sdm_id" class="form-control" onchange="choose_sdm(this.value)">

    </select>
</div>
@endif

<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Kategori <span class="text-red">*</span></label>
    <select name="marketer_type" class="form-control" required>
        @foreach ($marketer_types as $type)
            <option value="{{ $type->type_id }}" @isset($data) @if ($data->marketer->marketer_type == $type->type_id) selected @endif @endisset>{{ $type->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Rekening Bank <span class="text-red">*</span></label>
    <input type="text" name="rek_bank" class="form-control" required @isset($data) value="{{ $data->rek_bank }}" @endisset>
</div>

<div class="form-group">
    <label>Rekening Nomor <span class="text-red">*</span></label>
    <input type="text" name="rek_number" class="form-control" required @isset($data) value="{{ $data->rek_number }}" @endisset>
</div>

<div class="form-group">
    <label>Rekening Atas Nama <span class="text-red">*</span></label>
    <input type="text" name="rek_name" class="form-control" required @isset($data) value="{{ $data->rek_name }}" @endisset>
</div>

<div class="form-group">
    <label>Alamat <span class="text-red">*</span></label>
    <textarea name="address" class="form-control" required>@isset($data){{ $data->address }}@endisset</textarea>
</div>

<script>
function is_exists(params) {
    if (params == '1') {
        $('.sdm-exists').css('display', 'block');
    } else {
        $("select[name='sdm_id']").select2("val", "");
        $('.sdm-exists').css('display', 'none');
        $('input[name="name"]').val('');
        $('select[name="category"]').val('DOWNLINE');
        $('input[name="phone_num"]').val('');
        $('input[name="rek_bank"]').val('');
        $('input[name="rek_number"]').val('');
        $('input[name="rek_name"]').val('');
        $('textarea[name="address"]').val('');
    }
}

function choose_sdm(params) {
    var url = "{{ url('/prop/component/get_sdm_by_id') }}" + "/" + params;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('input[name="name"]').val(res.name);
            $('input[name="phone_num"]').val(res.phone_num);
            $('input[name="rek_bank"]').val(res.rek_bank);
            $('input[name="rek_number"]').val(res.rek_number);
            $('input[name="rek_name"]').val(res.rek_name);
            $('textarea[name="address"]').val(res.address);
        }
    })
}

$(document).ready(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("select[name='sdm_id']").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_sdm_not_marketer') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
})
</script>
