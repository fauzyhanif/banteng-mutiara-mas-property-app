<!-- The Modal -->
<div class="modal" id="form-delete">
    <div class="modal-dialog">
        <div class="modal-content">

            <form name="marketer_delete" action="{{ route('prop.marketer.delete') }}" method="POST">
                @csrf
                <input type="hidden" name="sdm_id" value="">
                <input type="hidden" name="affiliate_num" value="">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    yakin ingin menghapus marketer <b id="marketer-name"></b>?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-danger">Ya, Hapus Sekarang</button>
                </div>
            </form>

        </div>
    </div>
</div>
