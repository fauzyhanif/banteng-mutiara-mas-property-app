<!-- The Modal -->
<div class="modal" id="form-reset-password">
    <div class="modal-dialog">
        <div class="modal-content">

            <form name="marketer_reset_password" action="{{ route('prop.marketer.reset_password') }}" method="POST">
                @csrf
                <input type="hidden" name="sdm_id" value="0">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Email <span class="text-red">*</span></label>
                        <p id="email-string"></p>
                    </div>
                    <div class="form-group">
                        <label>No Handphone <span class="text-red">*</span></label>
                        <p id="phone-num-string"></p>
                    </div>

                    <div class="form-group">
                        <label>Reset Password <span class="text-red">*</span></label>
                        <p>12345678</p>
                        <p class="text-info">Marketer bisa mengubah password sendiri di halaman login marketer</p>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>
