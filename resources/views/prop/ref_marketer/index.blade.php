@extends('index')

@section('content')
<section class="content-header">
    <h1>Marketer</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Marketer
                        <div class="card-tools">
                            <a href="{{ route('prop.marketer.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Marketer Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-sm table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th>Nama / Kategori</th>
                                <th>No Afiliasi</th>
                                <th>Rekening</th>
                                <th width="30%">No HP / Alamat</th>
                                <th width="15%">Akses Login</th>
                                <th width="20%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_marketer.form_delete')
@include('prop.ref_marketer.form_create_user')
@include('prop.ref_marketer.form_reset_password')
@include('prop.ref_marketer.asset.js')
@endsection

