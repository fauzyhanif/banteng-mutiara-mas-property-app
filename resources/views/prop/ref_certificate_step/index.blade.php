@extends('index')

@section('content')
<section class="content-header">
    <h1>Step Sertifikat</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Step Sertifikat
                        <div class="card-tools">
                            <a href="{{ route('prop.certificate_step.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Step Sertifikat Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Nama</th>
                                <th width="15%" class="text-center">No Urut</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(route('prop.certificate_step.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'name', name: 'name', searchable: true },
            { data: 'orders', name: 'orders', searchable: true, class: 'text-center' },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@endsection

