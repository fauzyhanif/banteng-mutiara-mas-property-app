
<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input type="text" name="name" class="form-control" required @isset($data) value="{{ $data->name }}" @endisset>
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input type="text" name="phone_num" class="form-control" required @isset($data) value="{{ $data->phone_num }}" @endisset >
</div>

<div class="form-group">
    <label>Alamat KTP <span class="text-red">*</span></label>
    <textarea name="address" class="form-control" required>@isset($data){{ $data->address }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Alamat Domisili / Kantor</label>
    <textarea name="address_domicile" class="form-control">@isset($data){{ $data->address_domicile }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Rekening Bank <span class="text-red">*</span></label>
    <input type="text" name="rek_bank" class="form-control" required @isset($data) value="{{ $data->rek_bank }}" @endisset>
</div>

<div class="form-group">
    <label>Rekening Nomor <span class="text-red">*</span></label>
    <input type="text" name="rek_number" class="form-control" required @isset($data) value="{{ $data->rek_number }}" @endisset>
</div>

<div class="form-group">
    <label>Rekening Atas Nama <span class="text-red">*</span></label>
    <input type="text" name="rek_name" class="form-control" required @isset($data) value="{{ $data->rek_name }}" @endisset>
</div>
