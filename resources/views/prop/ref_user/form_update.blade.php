@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.user') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar user lain-lain
            </a>
        </li>
        <li class="breadcrumb-item active">Update user lain-lain</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.user.update', $data->sdm_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            Form

                            <div class="card-tools">
                                <a href="{{ route('prop.print_kpr_requirements', $data->sdm_id) }}" class="btn bg-purple btn-sm">
                                    <i class="fas fa-print"></i> &nbsp;
                                    Print Persyaratan KPR
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('prop.ref_user.form_general')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-sm btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-sm btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_user.asset.js')
@endsection
