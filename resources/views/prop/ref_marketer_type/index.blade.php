@extends('index')

@section('content')
<section class="content-header">
    <h1>Jenis Marketer</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Jenis Marketer
                        <div class="card-tools">
                            <a href="{{ route('prop.marketer_type.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Jenis Marketer
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <tr>
                                    <th rowspan="2">Jenis</th>
                                    @foreach ($subsidiTypes as $subsidiType)
                                        <th width="16%" colspan="2" class="text-center">{{ $subsidiType->name }}</th>
                                    @endforeach
                                    <th rowspan="2" width="10%">Aksi</th>
                                </tr>
                                <tr>
                                    @foreach ($subsidiTypes as $subsidiType)
                                        <th width="8%">Booking Fee</th>
                                        <th width="8%">Akad Fee</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ $data->name }}</td>
                                        @foreach ($subsidiTypes as $subsidiType)
                                            @if (array_key_exists($data->type_id, $marketingFeeSettingsArray))
                                                @if (array_key_exists($subsidiType->id, $marketingFeeSettingsArray[$data->type_id]))
                                                   <td>{{ GeneralHelper::rupiah($marketingFeeSettingsArray[$data->type_id][$subsidiType->id]['booking_fee']) }}</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            @else
                                                <td></td>
                                            @endif

                                            @if (array_key_exists($data->type_id, $marketingFeeSettingsArray))
                                                @if (array_key_exists($subsidiType->id, $marketingFeeSettingsArray[$data->type_id]))
                                                   <td>{{ GeneralHelper::rupiah($marketingFeeSettingsArray[$data->type_id][$subsidiType->id]['akad_fee']) }}</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            @else
                                                <td></td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <a href="{{ url('prop/marketer_type/form_update', $data->type_id) }}" class="btn btn-sm btn-primary">
                                                <i class="fas fa-edit"></i> Update
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_marketer_type.asset.js')
@endsection

