@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.marketer_type') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Jenis Marketer
            </a>
        </li>
        <li class="breadcrumb-item active">Update Jenis Marketer</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        {{-- <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-default-warning">
                    <h4 class="alert-header">Perhatian!</h4>
                    
                    <ul>
                        <li>Nama jenis marketer silahkan diisi nama jenis marketer yang anda inginkan</li>
                        <li>Dibawah form nama jenis marketer ada beberapa form setting marketing fee untuk <b>masing-masing type subsidi</b>, untuk data master type subsidi ada di menu Unit Rumah > Type Subsidi.</li>
                        <li>
                            Metode Perumusan : untuk menentukan rumus nominal marketing fee ketika ada penjualan yang dibawa oleh marketing
                            <ol>
                                <li>NOMINAL : Jika kamu pilih NOMINAL maka di form booking fee dan akad fee <b>isi dengan nominal (contoh : 1.000.000)</b></li>
                                <li>PERCENTAGE (%) : Jika kamu pilih PERCENTAGE maka di form booking fee dan akad fee <b>isi dengan angka persen (contoh : 2.5 - tidak perlu ketik '%' setelahnya)</b></li>
                                <li>CUSTOM : Jika kamu pilih CUSTOM maka form booking fee dan akad fee <b>biarkan kosong</b>, admin akan diminta untuk mengisi nominal marketing fee di fitur penjualan unit.</li>
                            </ol>
                        </li>
                        <li>Jika anda memilih NOMINAL / PERCENTAGE dan membiarkan form booking fee dan akad fee kosong maka Metode Perumusan dianggap CUSTOM</li>
                        
                    </ul>
                </div>
            </div>
        </div> --}}
        
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.marketer_type.update', $data->type_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            
                            <div class="form-group">
                                <label>Nama Jenis Marketer<span class="text-red">*</span></label>
                                <input type="text" name="name" value="{{ $data->name }}" class="form-control" required>
                            </div>

                            @foreach ($subsidiTypes as $subsidiType)
                                <p class="">Setting Marketing Fee untuk {{ $subsidiType->name }}</p>
                                <div class="row mb-3">
                                    <input type="hidden" name="method[{{ $subsidiType->id }}]" value="NOMINAL">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Booking Fee</label>
                                            <input type="text" name="booking_fee[{{ $subsidiType->id }}]" value="{{ (array_key_exists($subsidiType->id, $marketingFeeSettingsArray)) ? $marketingFeeSettingsArray[$subsidiType->id]['booking_fee'] : '' }}" class="form-control money">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Akad Fee</label>
                                            <input type="text" name="akad_fee[{{ $subsidiType->id }}]" value="{{ (array_key_exists($subsidiType->id, $marketingFeeSettingsArray)) ? $marketingFeeSettingsArray[$subsidiType->id]['akad_fee'] : '' }}" class="form-control money">
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <hr>
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>

                            <br>
                            <br>
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_marketer_type.asset.js')
@endsection
