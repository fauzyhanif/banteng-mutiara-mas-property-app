@extends('index')

@section('content')
<section class="content-header">
    <h1>Step Penjualan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Step Penjualan
                        <div class="card-tools">
                            <a href="{{ route('prop.sales_step.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Step Penjualan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Nama</th>
                                <th width="20%">Type Penjualan</th>
                                <th width="15%" class="text-center">No Urut</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(route('prop.sales_step.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'name', name: 'name', searchable: true },
            { data: 'type', name: 'type', searchable: true },
            { data: 'orders', name: 'orders', searchable: true, class: 'text-center' },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@endsection

