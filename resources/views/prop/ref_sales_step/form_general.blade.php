<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Type Penjualan</label>
    <select name="type" class="form-control">
        <option {{ (isset($data) && $data->type == 'CASH') ? 'selected' : '' }}>CASH</option>
        <option {{ (isset($data) && $data->type == 'KPR') ? 'selected' : '' }}>KPR</option>
    </select>
</div>

<div class="form-group">
    <label>No Urut</label>
    <input
        type="text"
        name="orders"
        class="form-control"
        @isset($data)
            value="{{ $data->orders }}"
        @endisset
    >
</div>

@isset($data)
    <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control">
            <option value="Y" {{ ($data->is_active == 'Y') ? 'selected' : '' }}>Aktif</option>
            <option value="N" {{ ($data->is_active == 'N') ? 'selected' : '' }}>Non Aktif</option>
        </select>
    </div>
@endisset
