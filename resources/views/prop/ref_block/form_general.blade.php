<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>
        Lokasi Perumahan <span class="text-danger">*</span>
    </label>
    <select name="location_id" class="form-control" required>
        <option value="">** Pilih Lokasi</option>
        @foreach ($locations as $location)
            <option
                value="{{ $location->location_id }}"
                @isset($data)
                    @if ($location->location_id == $data->location->location_id)
                        selected
                    @endif
                @endisset>
                {{ $location->name }}
            </option>
        @endforeach
    </select>
</div>
