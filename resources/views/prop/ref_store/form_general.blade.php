@if(!isset($data))
<div class="form-group">
    <label for="">Sudah pernah terdaftar sebagai Mandor/Suplier/Customer?</label>
    <select name="is_exist" class="form-control" onchange="is_exists(this.value)">
        <option value="0">Belum pernah</option>
        <option value="1">Sudah pernah</option>
    </select>
</div>

<div class="form-group sdm-exists" style="display: none">
    <label for="">Nama Toko</label>
    <select name="sdm_id" class="form-control" onchange="choose_sdm(this.value)">

    </select>
</div>
@endif

<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat</label>
    <textarea name="address" class="form-control">@isset($data){{ $data->address }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Keterangan</label>
    <textarea name="description" class="form-control">@isset($data){{ $data->store->description }}@endisset</textarea>
</div>

<hr>

<h4 class="font-weight-bold">Barang yang ada di toko ini</h4>

<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <th class="text-center" width="10%"></th>
        <th>Nama Barang</th>
    </thead>
    <tbody>
        @foreach ($goods as $good)
            <tr>
                <td class="text-center">
                    <label>
                        <input type="checkbox" name="goods_id[]" value="{{ $good->goods_id }}" @isset($arr_store_goods) {{ (array_key_exists($good->goods_id, $arr_store_goods)) ? 'checked' : '' }} @endisset>
                    </label>
                </td>
                <td>{{ $good->name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    function is_exists(params) {
    if (params == '1') {
        $('.sdm-exists').css('display', 'block');
    } else {
        $("select[name='sdm_id']").select2("val", "");
        $('.sdm-exists').css('display', 'none');
        $('input[name="name"]').val('');
        $('input[name="phone_num"]').val('');
        $('textarea[name="address"]').val('');
        $('textarea[name="description"]').val('');
    }
}

function choose_sdm(params) {
    var url = "{{ url('/prop/component/get_sdm_by_id') }}" + "/" + params;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('input[name="name"]').val(res.name);
            $('input[name="phone_num"]').val(res.phone_num);
            $('textarea[name="address"]').val(res.address);
            $('textarea[name="description"]').val(res.description);
        }
    })
}

$(document).ready(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("select[name='sdm_id']").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_sdm_not_store') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
})
</script>
