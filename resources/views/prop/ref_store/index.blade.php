@extends('index')

@section('content')
<section class="content-header">
    <h1>Toko Bangunan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Toko Bangunan
                        <div class="card-tools">
                            <a href="{{ route('prop.toko_bangunan.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Toko Bangunan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-sm table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Nama</th>
                                <th>Keterangan</th>
                                <th width="30%">No HP / Alamat</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_store.asset.js')
@endsection

