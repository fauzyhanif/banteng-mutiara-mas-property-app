<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Satuan <span class="text-red">*</span></label>
    <input
        type="text"
        name="unit"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->unit }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Harga <span class="text-red">*</span></label>
    <input
        type="text"
        name="price"
        class="form-control money"
        required
        @isset($data)
            value="{{ $data->price }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Stok Saat Ini</label>
    <input
        type="text"
        name="stock"
        class="form-control"
        required
        @if(isset($data))
            value="{{ $data->stock }}"
        @else
            value="0"
        @endif
    >
</div>

