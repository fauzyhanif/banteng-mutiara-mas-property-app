@extends('index')

@section('content')
<section class="content-header">
    <h1>Stok Barang</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Stok Barang
                        <div class="card-tools">
                            <a href="{{ route('prop.goods.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Stok Barang Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Nama Barang</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_goods.asset.js')
@endsection

