@extends('index')

@section('content')
<section class="content-header">
    <h1>Jenis Marketer</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Jenis Marketer
                        <div class="card-tools">
                            <a href="{{ route('prop.marketer_type.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Jenis Marketer
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <tr>
                                    <th rowspan="2">Jenis</th>
                                    <th width="25%" colspan="2" class="text-center">Subsidi</th>
                                    <th width="25%" colspan="2" class="text-center">Non Subsidi</th>
                                    <th rowspan="2" width="10%">Status Aktif</th>
                                    <th rowspan="2" width="15%">Aksi</th>
                                </tr>
                                <tr>
                                    <th width="12%">Subsidi Booking Fee</th>
                                    <th width="12%">Subsidi Akad Fee</th>
                                    <th width="12%">Non Subsidi Booking Fee</th>
                                    <th width="12%">Non Subsidi Akad Fee</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(route('prop.marketer_type.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'name', name: 'name', searchable: true },
            { data: 'html_subsidi_booking_fee', name: 'html_subsidi_booking_fee', searchable: true },
            { data: 'html_subsidi_akad_fee', name: 'html_subsidi_akad_fee', searchable: true },
            { data: 'html_nonsubsidi_booking_fee', name: 'html_nonsubsidi_booking_fee', searchable: true },
            { data: 'html_nonsubsidi_akad_fee', name: 'html_nonsubsidi_akad_fee', searchable: true },
            { data: 'is_active', name: 'is_active', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@include('prop.ref_marketer_type.asset.js')
@endsection

