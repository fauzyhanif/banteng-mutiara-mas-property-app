@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.marketer_type') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Jenis Marketer
            </a>
        </li>
        <li class="breadcrumb-item active">Update Jenis Marketer</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.marketer_type.update', $data->type_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('prop.ref_marketer_type.form_general')
                            <hr>
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>

                            <br>
                            <br>
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_marketer_type.asset.js')
@endsection
