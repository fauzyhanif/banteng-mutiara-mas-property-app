<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Subsidi Booking Fee</label>
    <input
        type="text"
        name="subsidi_booking_fee"
        class="form-control money"
        @isset($data)
            value="{{ $data->subsidi_booking_fee }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Subsidi Akad Fee</label>
    <input
        type="text"
        name="subsidi_akad_fee"
        class="form-control money"
        @isset($data)
            value="{{ $data->subsidi_akad_fee }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Non Subsidi Booking Fee</label>
    <input
        type="text"
        name="nonsubsidi_booking_fee"
        class="form-control money"
        @isset($data)
            value="{{ $data->nonsubsidi_booking_fee }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Non Subsidi Akad Fee</label>
    <input
        type="text"
        name="nonsubsidi_akad_fee"
        class="form-control money"
        @isset($data)
            value="{{ $data->nonsubsidi_akad_fee }}"
        @endisset
    >
</div>

@isset($data)
    <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control">
            <option value="Y" {{ ($data->is_active == 'Y') ? 'selected' : '' }}>Aktif</option>
            <option value="N" {{ ($data->is_active == 'N') ? 'selected' : '' }}>Non Aktif</option>
        </select>
    </div>
@endisset
