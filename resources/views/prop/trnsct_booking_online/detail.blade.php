@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.booking_online') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Booking Online
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Booking Online</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Profil Konsumen</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <Label>Nama Lengkap</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->name }}</p>
                        </div>

                        <div class="form-group">
                            <Label>No Handphone</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->phone_num }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Tanggal Lahir</Label>
                            <p style="margin-top: -10px">{{ GeneralHelper::konversiTgl($purchase->customer->date_ofbirth) }}</p>
                        </div>

                        <div class="form-group">
                            <Label>NIK</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->nik }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Suami/Istri</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->couple }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Pekerjaan</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->job }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Institusi</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->institute }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Alamat KTP</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->address }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Alamat Domisili</Label>
                            <p style="margin-top: -10px">{{ $purchase->customer->address_domicile }}</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Detail Booking</h5>

                        @if ($purchase->booking_online_status != 'DITERIMA')
                            <div class="card-tools">
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                    <i class="fas fa-check-circle"></i> Konfirmasi Booking
                                </button>
                            </div>
                        @endif
                    </div>
                    <div class="card-body scroll-x">

                        <div class="form-group">
                            <Label>Status Booking</Label>
                            <p style="margin-top: -10px">{{ $purchase->booking_online_status }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Lokasi</Label>
                            <p style="margin-top: -10px">{{ $purchase->location->name }} {{ $purchase->block->name }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Type Unit</Label>
                            <p style="margin-top: -10px">
                                {{ $purchase->unit->unitType->name }}

                            </p>
                        </div>

                        <div class="form-group">
                            <Label>Nomor Unit</Label>
                            <p style="margin-top: -10px">{{ $purchase->unit_id }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Jenis Pembelian</Label>
                            <p style="margin-top: -10px">{{ $purchase->sales_type }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Harga</Label>
                            <p style="margin-top: -10px">{{ GeneralHelper::rupiah($purchase->ttl_trnsct) }}</p>
                        </div>

                        <div class="form-group">
                            <Label>Marketer</Label>
                            <p style="margin-top: -10px">
                                @if ($purchase->affiliate_num != '')
                                    {{ $purchase->marketer->sdm->name }}
                                @else
                                -
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.trnsct_booking_online.konfirmasi')
@include('prop.trnsct_booking_online.asset.js')
@endsection
