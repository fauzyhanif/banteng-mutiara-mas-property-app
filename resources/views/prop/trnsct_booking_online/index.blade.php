@extends('index')

@section('content')
<section class="content-header">
    <h1>Booking Online</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Booking Online
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-sm table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Tgl & No Booking</th>
                                <th>Lokasi & Blok</th>
                                <th>Tipe</th>
                                <th>No Unit</th>
                                <th>Harga</th>
                                <th>Konsumen</th>
                                <th>Marketer</th>
                                <th width="10%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    var base = {!! json_encode(route('prop.booking_online.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'date_and_id_sales', name: 'date_and_id_sales', searchable: true },
            { data: 'location_and_block', name: 'location_and_block', searchable: true },
            { data: 'unit_type', name: 'unit_type', searchable: true },
            { data: 'unit_id', name: 'unit_id', searchable: true },
            { data: 'ttl_trnsct', name: 'ttl_trnsct', searchable: true },
            { data: 'customer.name', name: 'customer', searchable: true },
            { data: 'marketer', name: 'marketer', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
});
</script>
@endsection
