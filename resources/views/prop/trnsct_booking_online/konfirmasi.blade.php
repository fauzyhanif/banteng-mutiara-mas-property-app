
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Konfirmasi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form name="sales_store" action="{{ route('prop.booking_online.konfirmasi') }}" method="POST">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="id" value="{{ $purchase->id }}">

                    <div class="form-group">
                        <label for="">File Bukti Pembayaran</label>
                        @if ($purchase->booking_online_file_bayar != '')
                            <br>
                            <a href="{{ url('/booking_online_file_bayar', $purchase->booking_online_file_bayar) }}" target="_blank">{{ $purchase->booking_online_file_bayar }}</a>
                        @else
                            <p>File bukti pembayaran belum diupload konsumen</p>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Status Konfirmasi</label>
                        <select name="booking_online_status" class="form-control">
                            <option {{ ($purchase->booking_online_status == 'MENUNGGU') ? 'selected' : '' }}>MENUNGGU</option>
                            <option {{ ($purchase->booking_online_status == 'DITERIMA') ? 'selected' : '' }}>DITERIMA</option>
                            <option {{ ($purchase->booking_online_status == 'DITOLAK') ? 'selected' : '' }}>DITOLAK</option>
                        </select>
                    </div>

                    @if ($purchase->sales_type == 'KPR')
                        <div class="form-group">
                            <label>Bank Tujuan KPR</label>
                            <select name="kpr_bank" class="form-control form-kpr">
                                @foreach ($banks as $bank)
                                <option value="{{ $bank->sdm_id }}">{{ $bank->sdm->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Max Kredit Pengajuan</label>
                            <input type="text" name="kpr_credit_pengajuan" class="form-control money form-kpr" value="0">
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="">Potongan</label>
                        <input type="text" name="discount" class="form-control money" value="{{ $purchase->discount }}">
                    </div>

                    <div class="form-group">
                        <label for="">Marketer</label>
                        <div class="input-group mb-3">
                            <select name="affiliate_num" class="form-control" id="select-marketer" onchange="showAffiliateFee(this.value)">
                                @if ($purchase->affiliate_num != '')
                                    <option value="{{ $purchase->affiliate_num }}">{{ $purchase->marketer->sdm->name }}</option>
                                @else
                                    <option value="">** Pilih Marketer</option>
                                @endif
                            </select>
                            <div class="input-group-append">
                                <a href="{{ route('prop.marketer.form_store') }}" target="_blank" class="btn btn-primary">
                                    <i class="fas fa-plus"></i> Marketer Baru
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Fee Marketer</label>
                        <input type="text" name="affiliate_fee" class="form-control money" value="{{ $purchase->affiliate_fee }}">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Konfirmasi</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
