<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function() {
    $("#select-marketer").select2({
        val: '0',
        theme: 'bootstrap4',
        ajax: {
        url: "{{ url('/prop/component/slct_marketer') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="sales_store"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    if (res.booking_online_status == 'DITERIMA') {
                        // redirect to worklist by unit
                        var baseUrl = {!! json_encode(url('/prop/sales/form_pay/')) !!};
                        var baseUrl = baseUrl + "/" + res.sales_id;
                        window.location.href = baseUrl;
                    }
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
