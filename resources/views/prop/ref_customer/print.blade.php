<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dokumen Data Konsumen</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Papper css -->
    <link rel="stylesheet"
        href="{{ url('public/admin-lte/plugins/paper-css-master/paper.min.css') }}">
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</head>
<body>
    <!-- Set "A5", "A4" or "A3" for class name -->
    <!-- Set also "landscape" if you need -->

    <body class="A4">

        <!-- Each sheet element should have the class "sheet" -->
        <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
        <section class="sheet padding-10mm">
            @include('Component.kop_surat')

            {{-- titiel --}}
            <div class="row justify-content-center mb-4">
                <h4 class="font-weight-bold">Data Konsumen</h4>
            </div>

            {{-- data --}}
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-borderless table-sm">
                        <tbody>
                            <tr>
                                <td scope="row" width="30%">Nama</td>
                                <td>: {{ $data->name }}</td>
                            </tr>
                            <tr>
                                <td scope="row" width="30%">NIK</td>
                                <td>: {{ $data->nik }}</td>
                            </tr>
                            <tr>
                                <td scope="row" width="30%">Pasangan</td>
                                <td>: {{ $data->couple }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Pekerjaan</td>
                                <td>: {{ $data->job }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Institusi</td>
                                <td>: {{ $data->institute }}</td>
                            </tr>
                            <tr>
                                <td width="30%">NPWP</td>
                                <td>: {{ $data->npwp }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Alamat KTP</td>
                                <td>: {{ $data->address }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Alamat Domisili</td>
                                <td>: {{ $data->address_domicile }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>

    </body>
</body>
</html>
