@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.customer') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Customer
            </a>
        </li>
        <li class="breadcrumb-item active">Update Customer</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.customer.update', $data->sdm_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            Form

                            <div class="card-tools">
                                <a href="{{ route('prop.print_kpr_requirements', $data->sdm_id) }}" class="btn bg-purple btn-sm">
                                    <i class="fas fa-print"></i> &nbsp;
                                    Print Persyaratan KPR
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('prop.ref_customer.form_general')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-sm btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-sm btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_customer.asset.js')
@endsection
