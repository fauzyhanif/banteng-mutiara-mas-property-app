@if(!isset($data))
<div class="form-group">
    <label for="">Sudah pernah terdaftar sebagai Mandor/Suplier/Marketer?</label>
    <select name="is_exist" class="form-control" onchange="is_exists(this.value)">
        <option value="0">Belum pernah</option>
        <option value="1">Sudah pernah</option>
    </select>
</div>

<div class="form-group sdm-exists" style="display: none">
    <label for="">Nama User</label>
    <select name="sdm_id" class="form-control" onchange="choose_sdm(this.value)">

    </select>
</div>
@endif

<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input type="text" name="name" class="form-control" required @isset($data) value="{{ $data->name }}" @endisset >
</div>

<div class="form-group">
    <label>Tempat Lahir</label>
    <input type="text" name="place_of_birth" class="form-control" @isset($data) value="{{ $data->place_of_birth }}" @endisset >
</div>

<div class="form-group">
    <label>Tanggal Lahir <span class="text-red">*</span></label>
    <input type="date" name="date_ofbirth" class="form-control" required @isset($data) value="{{ $data->date_ofbirth }}" @endisset >
</div>

<div class="form-group">
    <label>NIK <span class="text-red">*</span></label>
    <input type="text" name="nik" class="form-control" @isset($data) value="{{ $data->nik }}" @endisset required >
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input type="text" name="phone_num" class="form-control" required @isset($data) value="{{ $data->phone_num }}" @endisset >
</div>

<div class="form-group">
    <label>Email</label>
    <input type="text" name="email" class="form-control" @isset($data) value="{{ $data->email }}" @endisset>
</div>

<div class="form-group">
    <label>Alamat KTP <span class="text-red">*</span></label>
    <textarea name="address" class="form-control" required>@isset($data){{ $data->address }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Alamat Domisili / Kantor</label>
    <textarea name="address_domicile" class="form-control">@isset($data){{ $data->address_domicile }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Pekerjaan</label>
    <input type="text" name="job" class="form-control" @isset($data) value="{{ $data->job }}" @endisset>
</div>

<div class="form-group">
    <label>Institusi</label>
    <input type="text" name="institute" class="form-control" @isset($data) value="{{ $data->institute }}" @endisset>
</div>

<div class="form-group">
    <label>Pendapatan per Bulan</label>
    <input type="text" name="income" class="form-control money" @isset($data) value="{{ $data->income }}" @endisset>
</div>

<div class="form-group">
    <label>NPWP</label>
    <input type="text" name="npwp" class="form-control" @isset($data) value="{{ $data->npwp }}" @endisset>
</div>

<div class="form-group">
    <label>Sudah Menikah?</label>
    <select name="is_married" class="form-control" onchange="filter_is_married(this.value)">
        <option value="N" @isset($data) {{ ($data->is_married == 'N') ? 'selected' : '' }} @endisset>Belum Menikah</option>
        <option value="Y" @isset($data) {{ ($data->is_married == 'Y') ? 'selected' : '' }} @endisset>Sudah Menikah</option>
    </select>
</div>

<div class="form-group form-is-married" @isset($data) @if($data->is_married == 'N') style="display: none" @endif @else style="display: none" @endisset>
    <label>Nama Pasangan (Suami/Istri)</label>
    <input type="text" name="couple" class="form-control" @isset($data) value="{{ $data->couple }}" @endisset>
</div>

<div class="form-group form-is-married" @isset($data) @if($data->is_married == 'N') style="display: none" @endif @else style="display: none" @endisset>
    <label>Tempat Lahir Pasangan</label>
    <input type="text" name="couple_place_of_birth" class="form-control" @isset($data) value="{{ $data->couple_place_of_birth }}" @endisset>
</div>

<div class="form-group form-is-married" @isset($data) @if($data->is_married == 'N') style="display: none" @endif @else style="display: none" @endisset>
    <label>Tanggal Lahir Pasangan</label>
    <input type="date" name="couple_date_of_birth" class="form-control" @isset($data) value="{{ $data->couple_date_of_birth }}" @endisset>
</div>

<div class="form-group form-is-married" @isset($data) @if($data->is_married == 'N') style="display: none" @endif @else style="display: none" @endisset>
    <label>Pekerjaan Pasangan</label>
    <input type="text" name="couple_job" class="form-control" @isset($data) value="{{ $data->couple_job }}" @endisset>
</div>

<div class="form-group form-is-married" @isset($data) @if($data->is_married == 'N') style="display: none" @endif @else style="display: none" @endisset>
    <label>NIK Pasangan</label>
    <input type="text" name="couple_nik" class="form-control" @isset($data) value="{{ $data->couple_nik }}" @endisset>
</div>

<div class="form-group form-is-married" @isset($data) @if($data->is_married == 'N') style="display: none" @endif @else style="display: none" @endisset>
    <label>Alamat Domisili Pasangan</label>
    <textarea name="couple_address_domicile" class="form-control">@isset($data){{ $data->couple_address_domicile }}@endisset</textarea>
</div>

@isset($data)
<div class="form-group">
    <label>Bank Rekening (Proses KPR)</label>
    <input type="text" name="rek_bank_new" class="form-control" @isset($data) value="{{ $data->rek_bank_new }}" @endisset>
</div>

<div class="form-group">
    <label>Nomor Rekening (Proses KPR)</label>
    <input type="text" name="rek_number_new" class="form-control" @isset($data) value="{{ $data->rek_number_new }}" @endisset>
</div>

<div class="form-group">
    <label>Status Aktif</label>
    <select name="is_active" class="form-control">
        <option value="Y" @if ($data->is_active == 'Y') selected @endif>Aktif</option>
        <option value="N" @if ($data->is_active == 'N') selected @endif>Nonaktif</option>
    </select>
</div>
@endisset

<script>
function is_exists(params) {
    if (params == '1') {
        $('.sdm-exists').css('display', 'block');
    } else {
        $("select[name='sdm_id']").select2("val", "");
        $('.sdm-exists').css('display', 'none');
        $('input[name="name"]').val('');
        $('input[name="date_ofbirth"]').val('');
        $('input[name="couple"]').val('');
        $('input[name="nik"]').val('');
        $('input[name="phone_num"]').val('');
        $('input[name="institute"]').val('');
        $('input[name="address"]').val('');
        $('input[name="address_domicile"]').val('');
        $('input[name="job"]').val('');
    }
}

function filter_is_married(params) {
    if (params == 'Y') {
        $('.form-is-married').css("display", "block");
    } else {
        $('.form-is-married').css("display", "none");
    }
}

function choose_sdm(params) {
    var url = "{{ url('/prop/component/get_sdm_by_id') }}" + "/" + params;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('input[name="name"]').val(res.name);
            $('input[name="date_ofbirth"]').val(res.date_ofbirth);
            $('input[name="couple"]').val(res.couple);
            $('input[name="nik"]').val(res.nik);
            $('input[name="phone_num"]').val(res.phone_num);
            $('input[name="institute"]').val(res.institute);
            $('input[name="address"]').val(res.address);
            $('input[name="address_domicile"]').val(res.address_domicile);
            $('input[name="job"]').val(res.job);
        }
    })
}

$(document).ready(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("select[name='sdm_id']").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_sdm_not_customer') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
})
</script>
