@extends('index')

@section('content')
<section class="content-header">
    <h1>Customer</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Customer
                        <div class="card-tools">
                            <a href="{{ route('prop.customer.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Customer Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-sm table-bordered table-striped datatable" style="width: 100%">
                            <thead class="bg-info">
                                <th>Nama / NIK</th>
                                <th>Pasangan</th>
                                <th>Institusi</th>
                                <th width="30%">No HP / Alamat Domisili</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_customer.asset.js')
@endsection

