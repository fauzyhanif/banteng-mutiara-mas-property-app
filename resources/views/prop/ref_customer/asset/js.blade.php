<script>
$(function() {
    loadData();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        if ($('input[name="type"]').val() == 'store' && $('select[name="is_exists"]').val() == '1' && $('select[name="sdm_id"]').val() == '') {
            alert('Silahkan isi form dengan benar.')
        } else {
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if ($('input[name="type"]').val() != 'update') {
                        form.trigger("reset");
                    }

                    if (res.status == 'success') {
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                },
                error: function (data) {
                    var res = errorAlert(data);
                    $('#response-alert').append(res);
                }
            });
        }
    });
})();

function loadData() {
    var base = {!! json_encode(route('prop.customer.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'name_nik', name: 'name_nik', searchable: true },
            { data: 'couple', name: 'couple', searchable: true },
            { data: 'institute', name: 'institute', searchable: true },
            { data: 'phone_address', name: 'phone_address', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

</script>
