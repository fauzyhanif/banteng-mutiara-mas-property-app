@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.unit_type') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Unit Type
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Unit Type</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.unit_type.store') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="store">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nama <span class="text-red">*</span></label>
                                        <input type="text" name="name" class="form-control" required @isset($data) value="{{ $data->name }}"
                                            @endisset>
                                    </div>

                                    <div class="form-group">
                                        <label>Panjang Tanah</label>
                                        <input type="text" name="ground_length" class="form-control" @isset($data)
                                            value="{{ $data->ground_length }}" @endisset>
                                    </div>

                                    <div class="form-group">
                                        <label>Lebar Tanah</label>
                                        <input type="text" name="ground_wide" class="form-control" @isset($data) value="{{ $data->ground_wide }}"
                                            @endisset>
                                    </div>

                                    <div class="form-group">
                                        <label>Luas Tanah</label>
                                        <input type="text" name="ground_area" class="form-control" @isset($data) value="{{ $data->ground_area }}"
                                            @endisset>
                                    </div>

                                    <div class="form-group">
                                        <label>Panjang Bangunan</label>
                                        <input type="text" name="building_length" class="form-control" @isset($data)
                                            value="{{ $data->building_length }}" @endisset>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Lebar Bangunan</label>
                                        <input type="text" name="building_wide" class="form-control" @isset($data)
                                            value="{{ $data->building_wide }}" @endisset>
                                    </div>

                                    <div class="form-group">
                                        <label>Luas Bangunan</label>
                                        <input type="text" name="building_area" class="form-control" @isset($data)
                                            value="{{ $data->building_area }}" @endisset>
                                    </div>

                                    <div class="form-group">
                                        <label>Marketing Fee untuk Agent</label>
                                        <input type="text" name="marketing_fee_agent" class="form-control money" @if (isset($data))
                                            value="{{ $data->marketing_fee_agent }}" @else value="0" @endif>
                                    </div>

                                    <div class="form-group">
                                        <label>Marketing Fee untuk Downline</label>
                                        <input type="text" name="marketing_fee_downline" class="form-control money" @if (isset($data))
                                            value="{{ $data->marketing_fee_downline }}" @else value="0" @endif>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#cash">Harga Cash</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#kpr">Harga KPR</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane container active" id="cash">
                                            <div class="row mt-4">
                                                @foreach ($price_item as $item)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{ $item->name }}</label>
                                                        <input type="text" name="price_id_cash[{{ $item->price_id }}]" class="form-control money" value="0">
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane container fade" id="kpr">
                                            <div class="row mt-4">
                                                @foreach ($price_item as $item)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{ $item->name }}</label>
                                                        <input type="text" name="price_id_kpr[{{ $item->price_id }}]" class="form-control money" value="0">
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_unit_type.asset.js')
@endsection
