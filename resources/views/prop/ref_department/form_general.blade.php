<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Sub dari</label>
    <select name="parent" class="form-control">
        <option value="">** Pilih departemt</option>
        @foreach ($depts as $dept)
            <option
                value="{{ $dept->dept_id }}"
                @isset($data)
                    @if ($dept->dept_id == $data->parent)
                        selected
                    @endif
                @endisset>
                {{ $dept->name }}
            </option>
        @endforeach
    </select>
</div>
