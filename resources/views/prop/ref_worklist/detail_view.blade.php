<table class="table table-bordered">
    <tbody>
        <tr>
            <td width="30%">Pekerjaan</td>
            <td width="70%">{{ $detail->worklist_name }}</td>
        </tr>
        <tr>
            <td width="30%">Brief</td>
            <td width="70%">{{ $detail->brief }}</td>
        </tr>
        <tr>
            <td width="30%">Mandor</td>
            <td width="70%">{{ $detail->mandor_name }}</td>
        </tr>
        <tr>
            <td width="30%">Progress</td>
            <td width="70%">
                <div class="progress-group">
                    {{ $detail->progress }}%
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-primary" style="width: {{ $detail->progress }}%"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td width="30%">Lokasi</td>
            <td width="70%">{{ ($detail->location_id != '') ? $detail->location_name : '' }}</td>
        </tr>
        <tr>
            <td width="30%">Blok</td>
            <td width="70%">{{ ($detail->block_id != '') ? $detail->block_name : '' }}</td>
        </tr>
        <tr>
            <td width="30%">Unit</td>
            <td width="70%">{{ ($detail->unit_id != '') ? $detail->number : '' }}</td>
        </tr>
        <tr>
            <td width="30%">Budget</td>
            <td width="70%">
                {{ GeneralHelper::rupiah($detail->budget) }} X {{ $detail->qty }} = {{ GeneralHelper::rupiah($detail->total) }}
                @if ($detail->st_transfer == 1)
                    (<span class="text-green">Sudah dicairkan ke kas mandor</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td width="30%">Tanggal Mulai</td>
            <td width="70%">{{ GeneralHelper::konversiTgl($detail->date_start, 'ttd') }}</td>
        </tr>
        <tr>
            <td width="30%">Deadline</td>
            <td width="70%">{{ GeneralHelper::konversiTgl($detail->date_end, 'ttd') }}</td>
        </tr>
    </tbody>
</table>

{{-- @if ($detail->progress >= 100 && $detail->st_transfer == 0)
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h5><i class="icon fas fa-info"></i> Peralihan budget mandor menjadi hutang perusahaan</h5>

        Total budget yang akan dialihkan sebesar <b>{{ GeneralHelper::rupiah($detail->total) }}</b> <br><br>

        <button type="button" class="btn btn-warning btn-sm" onclick="save_salary_mandor('{{ $detail->list_id }}', '{{ $detail->total }}', '{{ $detail->mandor_id }}')">
            ALIHKAN
        </button>
    </div>
@endif

@if ($detail->st_transfer == '1' && $finance_amount_paid <= 0)
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h5><i class="icon fas fa-info"></i> Pembatalan peralihan budget mandor</h5>

        Total budget yang akan dibatalkan sebesar <b>{{ GeneralHelper::rupiah($detail->total) }}</b> <br><br>

        <button type="button" class="btn btn-primary btn-sm"
            onclick="cancel_salary_mandor('{{ $detail->list_id }}', '{{ $detail->total }}', '{{ $detail->mandor_id }}')">
            BATALKAN
        </button>
    </div>
@endif --}}

{{-- <span class="pull-left"><span class="text-red">Catatan:</span> Jika progress mencapai 100% maka budget mandor bisa dicairkan ke kas mandor.</span> --}}
