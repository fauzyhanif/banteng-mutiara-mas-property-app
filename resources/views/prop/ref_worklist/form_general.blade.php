<div class="form-group">
    <label>Nama Pekerjaan<span class="text-red">*</span></label>
    <select name="worklist_id" class="form-control" onchange="showMandor(this.value)">
        <option value="">** Pilih Pekerjaan</option>
        @foreach ($worklists as $worklist)
            <option value="{{ $worklist->id ."|". $worklist->sdm_id }}" @isset($data) {{ ($data->worklist_id == $worklist->id) ? 'selected' : '' }}  @endisset>{{ $worklist->name }}</option>
        @endforeach
    </select>

</div>

<div class="form-group">
    <label>Brief</label>
   <textarea name="brief" class="form-control">@isset($data){{ $data->brief }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Lokasi Perumahan</label>
    <select name="location_id" class="form-control" onchange="showBlocks(this.value)">
        <option value="">** Pilih Lokasi</option>
    </select>
</div>

<div class="form-group">
    <label>Blok Perumahan</label>
    <select name="block_id" class="form-control" onchange="showUnits(this.value)">
        <option value="">** Pilih Blok</option>
    </select>
</div>

<div class="form-group">
    <label>
        Unit <span class="text-danger">*</span>
    </label>
    <select name="unit_id" class="select2"  data-placeholder="** Pilih unit" style="width: 100%;">
        @isset($data)
        @php $selected_unit = ""; @endphp
        @foreach ($units as $unit)
            @if ($unit->unit_id == $data->unit_id) @php $selected_unit = "selected"  @endphp  @endif
            <option value="{{ $unit->unit_id }}" {{ $selected_unit }}>{{ $unit->number }}</option>
        @endforeach
        @endisset
    </select>
</div>

<div class="form-group">
    <label>
        Mandor <span class="text-danger">*</span>
    </label>
    <select name="mandor_id" class="form-control" required>
        <option value="">** Pilih Mandor</option>
    </select>
</div>

<div class="form-group">
    <label>Budget<span class="text-red">*</span></label>
    <input type="text" name="budget" class="form-control money" required @if(isset($data)) value="{{ $data->total }}" @else value="0"  @endif>
</div>

<div class="form-group">
    <label>Qty<span class="text-red">*</span></label>
    <input type="number" name="qty" class="form-control" required @if(isset($data)) value="{{ $data->qty }}" @else value="1" @endif>
</div>

<div class="form-group">
    <label>Total Budget<span class="text-red">*</span></label>
    <input type="text" name="total" class="form-control money" required @if(isset($data)) value="{{ $data->total }}" @else value="0" @endif>
</div>

<div class="form-group">
    <label>Progress (hitungan persen)<span class="text-red">*</span></label>
    <input type="number" name="progress" class="form-control" required @if(isset($data)) value="{{ $data->progress }}" @else value="0" @endif>
</div>

<div class="form-group">
    <label for="">Tgl Mulai</label>
    <input type="date" name="date_start" class="form-control" @if(isset($data)) value="{{ $data->date_start }}" @else value="{{ date('Y-m-d') }}" @endif>
</div>

<div class="form-group">
    <label for="">Tgl Deadline</label>
    <input type="date" name="date_end" class="form-control" @if(isset($data)) value="{{ $data->date_end }}" @else value="{{ date('Y-m-d') }}" @endif>
</div>
