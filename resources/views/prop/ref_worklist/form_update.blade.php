@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.work_list') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar List Pengerjaan
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah List Pengerjaan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.work_list.update', $data->list_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @if ($data->st_transfer == 1)
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5><i class="icon fas fa-info"></i> Oopss!</h5>
                                    Pengerjaan ini tidak bisa diubah lagi karna budget pengerjaan sudah dicairkan ke kas mandor
                                </div>
                            @endif

                            <table class="table table-bordered table-sm mb-4">
                                <tbody>
                                    <tr>
                                        <td width="25%">Lokasi</td>
                                        <td width="75%">{{ $data->unit->location->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Blok</td>
                                        <td width="75%">{{ $data->unit->block->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">No Unit</td>
                                        <td width="75%">{{ $data->unit_id }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="form-group">
                                <label>Nama Pekerjaan<span class="text-red">*</span></label>
                                <select name="worklist_id" class="form-control" onchange="showMandor(this.value)">
                                    <option value="">** Pilih Pekerjaan</option>
                                    @foreach ($worklists as $worklist)
                                    <option value="{{ $worklist->id ." |". $worklist->sdm_id }}" @isset($data)
                                        {{ ($data->worklist_id == $worklist->id) ? 'selected' : '' }} @endisset>{{ $worklist->name }}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Brief</label>
                                <textarea name="brief" class="form-control">@isset($data){{ $data->brief }}@endisset</textarea>
                            </div>

                            <div class="form-group">
                                <label>
                                    Mandor <span class="text-danger">*</span>
                                </label>
                                <select name="mandor_id" class="form-control" required>
                                    <option value="">** Pilih Mandor</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Budget<span class="text-red">*</span></label>
                                <input type="text" name="budget" class="form-control money" required @if(isset($data)) value="{{ $data->total }}"
                                    @else value="0" @endif>
                            </div>

                            <div class="form-group">
                                <label>Qty<span class="text-red">*</span></label>
                                <input type="number" name="qty" class="form-control" required @if(isset($data)) value="{{ $data->qty }}" @else
                                    value="1" @endif>
                            </div>

                            <div class="form-group">
                                <label>Total Budget<span class="text-red">*</span></label>
                                <input type="text" name="total" class="form-control money" required @if(isset($data)) value="{{ $data->total }}"
                                    @else value="0" @endif>
                            </div>

                            <div class="form-group">
                                <label>Progress (hitungan persen)<span class="text-red">*</span></label>
                                <input type="number" name="progress" class="form-control" required @if(isset($data)) value="{{ $data->progress }}"
                                    @else value="0" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Tgl Mulai</label>
                                <input type="date" name="date_start" class="form-control" @if(isset($data)) value="{{ $data->date_start }}" @else
                                    value="{{ date('Y-m-d') }}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Tgl Deadline</label>
                                <input type="date" name="date_end" class="form-control" @if(isset($data)) value="{{ $data->date_end }}" @else
                                    value="{{ date('Y-m-d') }}" @endif>
                            </div>

                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            @if ($data->st_transfer == 0)
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                            @endif
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    loadComponent('{{ url('prop/component/slct_mandor?id='.$data->mandor_id) }}', 'select[name="mandor_id"]');
});
</script>
@include('prop.ref_worklist.asset.js')
@endsection
