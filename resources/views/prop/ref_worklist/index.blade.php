@extends('index')

@section('content')
<section class="content-header">
    <h1>List Pengerjaan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-12">
                <a href="{{ url('/prop/work_list?status=1') }}"
                    class="btn {{ ($status == 1) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Pekerjaan Baru
                </a>

                <a href="{{ url('/prop/work_list?status=2') }}"
                    class="btn {{ ($status == 2) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Sedang Berlangsung
                </a>

                <a href="{{ url('/prop/work_list?status=3') }}"
                    class="btn {{ ($status == 3) ? 'btn-primary' : 'btn-outline-primary' }} mr-1">
                    Selesai
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar List Pengerjaan
                        <div class="card-tools">
                            <a href="{{ route('prop.work_list.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah List Pengerjaan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Pekerjaan</th>
                                <th>Mandor</th>
                                <th>Lokasi</th>
                                <th>Progress</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_worklist.detail')
@include('prop.ref_worklist.asset.js')

<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(url('/prop/work_list/list_data?status='.$status)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'worklist', name: 'worklist', searchable: true },
            { data: 'mandor', name: 'mandor', searchable: true },
            { data: 'location', name: 'location', searchable: true },
            { data: 'progress_bar', name: 'progress_bar', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@endsection

