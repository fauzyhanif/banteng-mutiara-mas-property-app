<script>
function showMandor(params) {
    var my_array = params.split("|");
    var worklist_id = my_array[0];
    var sdm_id = my_array[1];
    loadComponent('{{ url('prop/component/slct_mandor?id=') }}'+sdm_id, 'select[name="mandor_id"]');

    var url = '{{ url('prop/worklist_master/get_salary?worklist_id=') }}'+worklist_id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('input[name="budget"]').val(res);
        }
    })
}

$(function() {
    $('.select2').select2();

    $('input[name="budget"]').keyup(function() {
        var budget = this.value.replace(/\./g, '');
        var qty = $('input[name="qty"]').val();
        var total = budget * qty;
        console.log(total)
        $('input[name="total"]').val(formatRupiah(total));
    });

    $('input[name="qty"]').keyup(function() {
        var qty = this.value;
        var budget = $('input[name="budget"]').val().replace(/\./g, '');
        var total = budget * qty;
        console.log(total)
        $('input[name="total"]').val(formatRupiah(total));
    });
});

function showBlocks(id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+id, 'select[name="block_id"]');
}

function showUnits(id) {
    loadComponent('{{ url('prop/component/slct_unit?block_id=') }}'+id, 'select[name="unit_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update') {
                    form.trigger("reset");
                }

                if (res.status == 'success') {
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function detail(list_id) {
    var url = {!! json_encode(route('prop.work_list.detail')) !!};
    $('#detail').modal('show');
    $.ajax({
        type: 'POST',
        url: url,
        dataType:'html',
        data: "list_id="+list_id,
        success: function (res) {
            $('#detail').modal('show');
            $('#detail-view').html(res);
        }
    })
}

function save_salary_mandor(list_id, ttl_salary, mandor_id) {
    var url = {!! json_encode(route('prop.work_list.save_salary_mandor')) !!};
    $('#detail').modal('show');
    $.ajax({
        type: 'POST',
        url: url,
        dataType:'json',
        data: "list_id="+list_id+"&ttl_salary="+ttl_salary+"&mandor_id="+mandor_id,
        success: function (res) {
            if (res.status == 'success') {
                toastr.success(res.text)
            } else {
                toastr.error(res.text)
            }

            detail(list_id);
        }
    })
}

function cancel_salary_mandor(list_id, ttl_salary, mandor_id) {
    var url = {!! json_encode(route('prop.work_list.cancel_salary_mandor')) !!};
    $('#detail').modal('show');

    $.ajax({
        type: 'POST',
        url: url,
        dataType:'json',
        data: "list_id="+list_id+"&ttl_salary="+ttl_salary+"&mandor_id="+mandor_id,
        success: function (res) {
            if (res.status == 'success') {
                toastr.success(res.text)
            } else {
                toastr.error(res.text)
            }

            detail(list_id);
        }
    })
}

</script>
