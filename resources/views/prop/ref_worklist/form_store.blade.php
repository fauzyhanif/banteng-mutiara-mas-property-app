@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.work_list') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar List Pengerjaan
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah List Pengerjaan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.work_list.store') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="store">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('prop.ref_worklist.form_general')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    loadComponent('{{ url('prop/component/slct_location') }}', 'select[name="location_id"]');
});
</script>
@include('prop.ref_worklist.asset.js')
@endsection
