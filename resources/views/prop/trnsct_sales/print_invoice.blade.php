<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice {{ $payment->invoice_id }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print_second')
    <style>
        @page {
            size: A5 landscape
        }

        body {
            letter-spacing: 1px;
            font-family: "Times New Roman", Times, serif;
            letter-spacing: 1.5px;
        }
    </style>
</head>
<body>
    <body class="A5 landscape" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold mb-2">KWITANSI PEMBAYARAN UNIT</p>

                        <table class="table table-borderless table-sm" style="margin-bottom: 0px">
                            <tbody>
                                <tr>
                                    <td width="35%">No Kwitansi</td>
                                    <td class="font-weight-bold">: {{ $payment->sales_paid_num }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Tgl Pembayaran</td>
                                    <td class="font-weight-bold">: {{ GeneralHelper::konversiTgl($payment->sales_paid_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Telah Diterima Dari</td>
                                    <td class="font-weight-bold">: {{ $payment->sales_paid_person }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Sebesar</td>
                                    <td class="font-weight-bold">: Rp {{ GeneralHelper::rupiah($payment->sales_paid_amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Terbilang</td>
                                    <td class="font-weight-bold">: {{ GeneralHelper::terbilang($payment->sales_paid_amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Keterangan</td>
                                    <td class="font-weight-bold">: {{ $payment->sales_paid_desc }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-weight-bold">Untuk</td>
                                </tr>
                                <tr>
                                    <td width="35%">Unit</td>
                                    <td class="font-weight-bold">: {{ $payment->sales->unit_id }} ({{ $payment->sales->unit->unitType->name }})</td>
                                </tr>
                                <tr>
                                    <td width="35%">Lokasi</td>
                                    <td class="font-weight-bold">: {{ $payment->sales->location->name }} {{ $payment->sales->block->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Konsumen</td>
                                    <td class="font-weight-bold">: {{ $payment->sales->customer->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, {{ GeneralHelper::konversiTgl($payment->sales_paid_date, 'ttd') }}</div>

                    <div class="col-md-6 col-print-6 text-center mb-4">Penyetor,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Petugas,</div>

                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $payment->sales_paid_person }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $payment->sales_paid_admin }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
