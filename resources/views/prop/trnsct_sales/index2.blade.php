@extends('index')
@section('content')
<input type="hidden" name="pages" value="1">
<input type="hidden" name="hidden_blocks" value="@foreach($key_blocks as $key => $value){{$key}}-@endforeach">


<section class="content-header">
    <h1>Daftar Penjualan (Filter)</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Penjualan
                        <div class="card-tools">
                            <a href="{{ route('prop.sales.list_unit') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Input Penjualan
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <div class="row mb-3">
                            <div class="col-lg-3">
                                <input type="text" name="keyword" class="form-control form-control-sm" id="form-search" placeholder="Cari nama konsumen">
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-sm btn-default" onclick="search()">
                                    <i class="fas fa-search"></i> Cari
                                </button>
                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-filter">
                                    <i class="fas fa-filter"></i> Filter
                                </button>
                            </div>

                            <div class="col-md-3 text-right">
                                <span>Show</span>
                                <select name="per_page" onchange="getData(1)">
                                    <option>25</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                                <span>entries</span>
                            </div>
                        </div>

                        <div id='view'>
                            <div class="table-responsive">
                                @include('prop.trnsct_sales.index2_list_data')
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-filter">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Filter</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="" method="POST">
                @csrf
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Blok yang ditamplikan :</label>
                            <div class="form-group" id="checkbox1">
                                @foreach ($ref_block as $block)
                                @if (count($block->units) > 0)
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="selected_blocks[]"
                                        id="customCheckbox1{{ $block->block_id }}" value="{{ $block->block_id }}"
                                        @if(array_key_exists($block->block_id,$key_blocks))
                                            checked
                                        @endif
                                    >
                                    <label for="customCheckbox1{{ $block->block_id }}" class="custom-control-label">{{ $block->location->name }} -
                                        {{ $block->name }}
                                    </label>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">Status yang ditampilkan :</label>
                            <div class="form-group" id="checkbox2">
                                @foreach ($ref_sales_type as $key => $value)
                                <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="selected_sales_types[]"
                                            id="customCheckbox2{{ $key }}" value="{{ $key }}"
                                            @if(array_key_exists($key,$key_sales_types))
                                                checked
                                            @endif
                                        >
                                        <label for="customCheckbox2{{ $key }}" class="custom-control-label">{{ $value }}</label>
                                    </div>
                                @endforeach

                                @foreach ($ref_kpr_status as $key => $value)
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="selected_kpr_status[]"
                                        id="customCheckbox2{{ $key }}" value="{{ $key }}"
                                        @if(array_key_exists($key,$key_kpr_status))
                                            checked
                                        @endif
                                    >
                                    <label for="customCheckbox2{{ $key }}" class="custom-control-label">{{ $value }}</label>
                                </div>
                                @endforeach
                            </div>

                            <br><br>

                            <label for="">Step Terakhir</label>
                            <div class="form-group" id="checkbox3">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="selected_sales_steps[]"
                                        id="customCheckbox30" value="0"
                                        @if(array_key_exists(0,$key_sales_steps))
                                            checked
                                        @endif
                                    >
                                    <label for="customCheckbox30" class="custom-control-label">BOOKING</label>
                                </div>

                                @foreach ($ref_sales_step as $key => $value)
                                    @if ($value->step_id != 0)
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" name="selected_sales_steps[]"
                                                id="customCheckbox3{{ $value->step_id }}" value="{{ $value->step_id }}"
                                                @if(array_key_exists($value->step_id,$key_sales_steps))
                                                    checked
                                                @endif
                                            >
                                            <label for="customCheckbox3{{ $value->step_id }}" class="custom-control-label">{{ $value->type }} - {{ $value->name }}</label>
                                        </div>  
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Terapkan</button>
                </div>
            </form>

        </div>
    </div>
</div>

@include('prop.trnsct_sales.form_delete_sales')
<script type="text/javascript">
    $(window).on('hashchange', function() {

    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');

        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            getData(page);
        }
    }
});

$(document).ready(function() {
    $(document).on('click', '.pagination a' ,function(event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl=$(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        $('input[name="pages"]').val(page)

        getData(page);
    });

    $('#form-search').on('keypress', function(e){
        if(e.which == 13) { // Check if the key pressed is Enter (key code 13)
            e.preventDefault(); // Prevent the default form submission
            getData(1);
        }
    });
});


function getData(page){
    var per_page = $('select[name="per_page"]').val();
    var keyword = $('input[name="keyword"]').val();

    $.ajax({
        url: {!! json_encode(route('prop.sales2')) !!} + '?page=' + page + '&per_page=' + per_page + '&keyword=' + keyword,
        type: "get",
        datatype: "html",
    }).done(function(data){
        $("#view").empty().html(data);
        location.hash = page;
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        alert('No response from server');
    });
}

function search() {
    getData(1)
}

function show_modal_delete_sales(id) {
    var form = $('#modal-delete-sales').modal('show');
    form.find('input[name="delete_sales_id"]').val(id)
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="delete_sales"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                    getData(1);
                } else {
                    toastr.error(res.text)
                }
            }
        });

        $('#modal-delete-sales').modal('hide');
    });
})();
</script>
@endsection
