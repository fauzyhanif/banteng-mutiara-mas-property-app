<!-- The Modal -->
<div class="modal" id="form-switch-unit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Pindah Unit</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <form name="show_units" action="{{ route('prop.sales.switch_unit_list_unit') }}" method="POST">
                            @csrf
                            <input type="hidden" name="sales_id" value="{{ $sales->id }}">
                            <div class="form-group">
                                <label>Lokasi <span class="text-red">*</span></label>
                                <select name="location_id" class="form-control" required onchange="showBlocks(this.value)">
                                    <option value="">** Pilih Lokasi</option>
                                    @foreach ($locations as $location)
                                    <option value="{{ $location->location_id }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Blok <span class="text-red">*</span></label>
                                <select name="block_id" class="form-control" required>
                                    <option value="">** Pilih Blok Dahulu</option>
                                </select>
                            </div>

                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    TAMPILKAN UNIT
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-12" id="view-list-units">
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>

        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+location_id, 'select[name="block_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="show_units"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#view-list-units').html(res)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
