<!-- The Modal -->
<div class="modal" id="detail-customer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Detail Konsumen</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-borderless table-sm">
                            <tbody>
                                <tr>
                                    <td class="text-muted" width="25%">Nama Konsumen</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">NIK</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->nik }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Tempat, Tgl Lahir</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->place_of_birth }}, {{ GeneralHelper::konversiTgl($sales->customer->date_ofbirth, 'T') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">No Handphone</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Email</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->email }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Alamat KTP</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->address }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Alamat Domisili</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->address_domicile }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Pekerjaan</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->job }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Instansi</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->institute }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Penghasilan</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->income }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">NPWP</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $sales->customer->npwp }}</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Scan KTP</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>@if ($sales->customer->file_ktp != '')<a href="{{ url('public/customer_document', $sales->customer->file_ktp) }}" target="_blank" rel="noopener noreferrer">Lihat Dokumen</a>@else - @endif</td>
                                </tr>
                                <tr>
                                    <td class="text-muted" width="25%">Scan KK</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>@if ($sales->customer->file_kk != '')<a href="{{ url('public/customer_document', $sales->customer->file_kk) }}" target="_blank" rel="noopener noreferrer">Lihat Dokumen</a>@else - @endif</td>
                                </tr>

                                @if ($sales->customer->is_married == 'Y')
                                    <tr>
                                        <td colspan="3" class="font-weight-bold">Pasangan</td>
                                    </tr>                     
                                    <tr>
                                        <td class="text-muted" width="25%">Nama Pasangan</td>
                                        <td class="text-right" width="3%">:</td>
                                        <td>{{ $sales->customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="25%">NIK</td>
                                        <td class="text-right" width="3%">:</td>
                                        <td>{{ $sales->customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="25%">Tempat, Tgl Lahir</td>
                                        <td class="text-right" width="3%">:</td>
                                        <td>{{ $sales->customer->couple_place_of_birth }}, {{ GeneralHelper::konversiTgl($sales->customer->couple_date_of_birth, 'T') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="25%">Alamat</td>
                                        <td class="text-right" width="3%">:</td>
                                        <td>{{ $sales->customer->couple_address_domicile }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="25%">No Handphone</td>
                                        <td class="text-right" width="3%">:</td>
                                        <td>{{ $sales->customer->couple_phone_num }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted" width="25%">Pekerjaan</td>
                                        <td class="text-right" width="3%">:</td>
                                        <td>{{ $sales->customer->couple_job }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

        </div>
    </div>
</div>
