<!-- The Modal -->
<div class="modal" id="detail-installment-form-potongan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="installment_potongan" action="{{ route('prop.sales.detail_installment_potongan') }}" method="POST">

                @csrf

                <input type="hidden" name="id" value="{{ $sale->id }}">
                <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">


                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Input Potongan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Jumlah Potongan <span class="text-red">*</span></label>
                        <input type="text" name="discount" class="form-control money" value="{{ $sale->discount }}">
                    </div>

                    <div class="form-group">
                        <label>Keterangan Potongan</label>
                        <textarea name="discount_desc" class="form-control">{{ $sale->discount_desc }}</textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});
</script>
