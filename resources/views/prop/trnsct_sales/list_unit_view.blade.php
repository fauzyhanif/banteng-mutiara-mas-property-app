<div class="card shadow-none">
    <div class="card-header">
        <h3 class="card-title">
            Daftar Unit
        </h3>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <th>Nomor Unit</th>
                <th class="text-right">Harga Cash</th>
                <th class="text-right">Harga KPR</th>
                <th>Status</th>
                <th width="15%">Pilih</th>
            </thead>
            <tbody>
                @foreach ($units as $unit)
                <tr>
                    <td>
                        <b>{{ $unit->number }}</b> <br>
                        <span class="font-weight-light">Type {{ $unit->unitType->name }}</span>
                    </td>
                    <td class="text-right">
                        {{ GeneralHelper::rupiah($unit->harga_jual_cash) }}
                    </td>
                    <td class="text-right">
                        {{ GeneralHelper::rupiah($unit->harga_jual_kpr) }}
                    </td>
                    <td>
                        @if ($unit->status == 'READY')
                        <span class="text-green">{{ $unit->status }}</span>
                        @elseif ($unit->status == 'BOOKED')
                        <span class="text-orange">{{ $unit->status }}</span>
                        @else
                        <span class="text-red">{{ $unit->status }}</span>
                        @endif
                    </td>
                    <td>
                        @if ($unit->status == 'READY')
                        <form action="{{ route('prop.sales.form_store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="location_id" value="{{ $unit->location_id }}">
                            <input type="hidden" name="block_id" value="{{ $unit->block_id }}">
                            <input type="hidden" name="number" value="{{ $unit->number }}">
                            <button type="submit" class="btn btn-sm btn-primary">
                                PILIH
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </button>
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
