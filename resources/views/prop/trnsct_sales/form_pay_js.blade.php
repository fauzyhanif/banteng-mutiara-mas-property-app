<script>
$(function() {
    $('input[name="ttl_trnsct_paid"]').keyup(function() {
        var ttl_trnsct_paid = this.value.replace(/\./g, '');
        var ttl_trnsct = $('input[name="ttl_trnsct"]').val();
        var uang_muka = $('input[name="uang_muka"]').val();
        var discount = $('input[name="discount"]').val();

        var result = (ttl_trnsct - discount) - ttl_trnsct_paid;
        if (result > 0) {
            $('.rest-of-bill').css('display', 'block');
            $('input[name="rest_of_bill"]').val(formatRupiah(result));
        } else {
            $('.rest-of-bill').css('display', 'none');
            $('input[name="rest_of_bill"]').val(0);
        }

        if (ttl_trnsct_paid < uang_muka) {
            $('#btn-save').css('display', 'none');
        } else {
            $('#btn-save').css('display', 'block');
        }
    });
});
(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="sales_pay"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    // redirect to worklist by unit
                    var baseUrl = {!! json_encode(url('/prop/sales/detail/')) !!};
                    var baseUrl = baseUrl + "/" + res.sales_id;
                    window.location.href = baseUrl;
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

</script>
