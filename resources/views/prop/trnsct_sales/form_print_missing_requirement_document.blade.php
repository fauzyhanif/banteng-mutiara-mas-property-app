<!-- The Modal -->
<div class="modal" id="form-print-missing-requirement-document">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Cetak Kekurangan Dokumen Persyaratan KPR</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="{{ route('prop.sales.print_missing_requirement_document') }}" method="POST" target="_blank">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            @csrf
                            <input type="hidden" name="sales_id" value="{{ $sales->id }}">
                            <div class="form-group">
                                <label for="">Dokumen Yang Kurang</label>
                                <textarea name="kpr_missing_requirement_document" class="form-control" required>{{ $sales->kpr_missing_requirement_document }}</textarea>
                                <span class="text-info">*Jika kekurangan dokumen lebih dari 1, pisahkan dengan tanda koma (,)</span>
                            </div>

                            <div class="form-group">
                                <label for="">Tgl Batas Pengumpulan Berkas</label>
                                <input type="date" name="kpr_document_collection_limit" value="{{ ($sales->kpr_document_collection_limit != '') ? $sales->kpr_document_collection_limit : '' }}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+location_id, 'select[name="block_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="show_units"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#view-list-units').html(res)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
