<!-- The Modal -->
<div class="modal" id="form-update-marketer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Update Marketer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="update_marketers" action="{{ route('prop.sales.update_marketer') }}" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            @csrf
                            <input type="hidden" name="sales_id" value="{{ $sales->id }}">
                            <div class="form-group">
                                <label>Marketer sebelumnya : </label> <br>
                                @if ($sales->affiliate_num != '' && $sales->affiliate_num != '0')
                                    {{ $sales->marketer->sdm->name }}
                                @else
                                    <span class="text-red font-weight-bold">Tidak ada</span>
                                @endif

                                <br>
                                Rp {{ GeneralHelper::rupiah($sales->affiliate_fee) }}


                            </div>

                            <div class="form-group">
                                <label>Marketer</label>
                                <div class="input-group mb-3">
                                    <select name="affiliate_num" class="form-control" id="select-marketer">
                                        <option value="">** Pilih Marketer</option>
                                    </select>
                                    <div class="input-group-append">
                                        <a href="{{ route('prop.marketer.form_store') }}" target="_blank" class="btn btn-primary">
                                            <i class="fas fa-plus"></i> Marketer Baru
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+location_id, 'select[name="block_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="show_units"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#view-list-units').html(res)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
