<!-- The Modal -->
<div class="modal" id="form-update-customer-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Update Data Konsumen</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="update_customer_data" action="{{ url('prop/customer/update_from_sales_page', $sales->customer) }}" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            @csrf
                            <div class="form-group">
                                <label>Nama <span class="text-red">*</span></label>
                                <input type="text" name="name" class="form-control" required value="{{ $sales->customer->name }}">
                            </div>

                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" name="place_of_birth" class="form-control" value="{{ $sales->customer->place_of_birth }}">
                            </div>

                            <div class="form-group">
                                <label>Tanggal Lahir <span class="text-red">*</span></label>
                                <input type="date" name="date_ofbirth" class="form-control" required value="{{ $sales->customer->date_ofbirth }}">
                            </div>

                            <div class="form-group">
                                <label>NIK <span class="text-red">*</span></label>
                                <input type="text" name="nik" class="form-control" value="{{ $sales->customer->nik }}" required>
                            </div>

                            <div class="form-group">
                                <label>No. Handphone <span class="text-red">*</span></label>
                                <input type="text" name="phone_num" class="form-control" required  value="{{ $sales->customer->phone_num }}">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control"  value="{{ $sales->customer->email }}">
                            </div>

                            <div class="form-group">
                                <label>Alamat KTP <span class="text-red">*</span></label>
                                <textarea name="address" class="form-control" required>{{ $sales->customer->address }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Alamat Domisili / Kantor</label>
                                <textarea name="address_domicile" class="form-control">{{ $sales->customer->address_domicile }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Pekerjaan</label>
                                <input type="text" name="job" class="form-control" value="{{ $sales->customer->job }}">
                            </div>

                            <div class="form-group">
                                <label>Institusi</label>
                                <input type="text" name="institute" class="form-control" value="{{ $sales->customer->institute }}">
                            </div>

                            <div class="form-group">
                                <label>Pendapatan per Bulan</label>
                                <input type="text" name="income" class="form-control money" value="{{ $sales->customer->income }}">
                            </div>

                            <div class="form-group">
                                <label>NPWP</label>
                                <input type="text" name="npwp" class="form-control" value="{{ $sales->customer->npwp }}">
                            </div>

                            <div class="form-group">
                                <label>Sudah Menikah?</label>
                                <select name="is_married" class="form-control">
                                    <option value="N" {{ ($sales->customer->is_married == 'N') ? 'selected' : '' }}>Belum Menikah
                                    </option>
                                    <option value="Y" {{ ($sales->customer->is_married == 'Y') ? 'selected' : '' }}>Sudah Menikah
                                    </option>
                                </select>
                            </div>

                            <div class="form-group form-is-married">
                                <label>Nama Pasangan (Suami/Istri)</label>
                                <input type="text" name="couple" class="form-control"  value="{{ $sales->customer->couple }}">
                                <span class="text-info">*Diisi jika sudah menikah</span>
                            </div>

                            <div class="form-group form-is-married" >
                                <label>Tempat Lahir Pasangan</label>
                                <input type="text" name="couple_place_of_birth" class="form-control" value="{{ $sales->customer->couple_place_of_birth }}">
                                <span class="text-info">*Diisi jika sudah menikah</span>
                            </div>

                            <div class="form-group form-is-married">
                                <label>Tanggal Lahir Pasangan</label>
                                <input type="date" name="couple_date_of_birth" class="form-control" value="{{ $sales->customer->couple_date_of_birth }}">
                                <span class="text-info">*Diisi jika sudah menikah</span>
                            </div>

                            <div class="form-group form-is-married">
                                <label>Pekerjaan Pasangan</label>
                                <input type="text" name="couple_job" class="form-control"  value="{{ $sales->customer->couple_job }}">
                                <span class="text-info">*Diisi jika sudah menikah</span>
                            </div>

                            <div class="form-group form-is-married">
                                <label>NIK Pasangan</label>
                                <input type="text" name="couple_nik" class="form-control"  value="{{ $sales->customer->couple_nik }}">
                                <span class="text-info">*Diisi jika sudah menikah</span>
                            </div>

                            <div class="form-group form-is-married">
                                <label>Alamat Domisili Pasangan</label>
                                <textarea name="couple_address_domicile" class="form-control">{{ $sales->customer->couple_address_domicile }}</textarea>
                                <span class="text-info">*Diisi jika sudah menikah</span>
                            </div>

                            <div class="form-group">
                                <label>Bank Rekening (Proses KPR)</label>
                                <input type="text" name="rek_bank_new" class="form-control"  value="{{ $sales->customer->rek_bank_new }}">
                            </div>

                            <div class="form-group">
                                <label>Nomor Rekening (Proses KPR)</label>
                                <input type="text" name="rek_number_new" class="form-control"  value="{{ $sales->customer->rek_number_new }}">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+location_id, 'select[name="block_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="show_units"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#view-list-units').html(res)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
