<script>
    var isSubmitting = false;
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(function() {
        $("#select-marketer").select2({
            val: '0',
            theme: 'bootstrap4',
            ajax: {
            url: "{{ route('prop.component.slct_marketer') }}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });
    });

    function detailInstallment(sales_id) {
        var url = {!! json_encode(url('/prop/sales/detail_installment')) !!};
        var data = "sales_id="+sales_id;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                $('#view-detail-component').html(res)
            }
        });
    }

    function detailKpr(sales_id) {
        var url = {!! json_encode(url('/prop/sales/detail_kpr')) !!};
        var data = "sales_id="+sales_id;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                $('#view-detail-component').html(res)
            }
        });
    }

    function detailSalesStep(sales_id, location_id, block_id, unit_id, sales_type) {
        var url = {!! json_encode(url('/prop/sales/detail_sales_step')) !!};
        var data = "sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id+"&sales_type="+sales_type;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                $('#view-detail-component').html(res)
            }
        });
    }

    function deleteSalesStep(id, sales_id, location_id, block_id, unit_id, sales_type) {
        var url = {!! json_encode(url('/prop/sales/detail_sales_step_delete')) !!};
        var data = "id="+id+"&sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id+"&sales_type="+sales_type;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                detailSalesStep(res.sales_id, res.location_id, res.block_id, res.unit_id, res.sales_type);
                toastr.success(res.text);
            }
        });
    }

    function detailCertificateStep(sales_id, location_id, block_id, unit_id, sales_type) {
        var url = {!! json_encode(url('/prop/sales/detail_certificate_step')) !!};
        var data = "sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id+"&sales_type="+sales_type;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                $('#view-detail-component').html(res)
            }
        });
    }

    function deleteCertificateStep(id, sales_id, location_id, block_id, unit_id, sales_type) {
        var url = {!! json_encode(url('/prop/sales/detail_certificate_step_delete')) !!};
        var data = "id="+id+"&sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id+"&sales_type="+sales_type;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                detailCertificateStep(res.sales_id, res.location_id, res.block_id, res.unit_id, res.sales_type);
                toastr.success(res.text);
            }
        });
    }

    function detailCertificatePosition(sales_id, location_id, block_id, unit_id) {
        var url = {!! json_encode(url('/prop/sales/detail_certificate_position')) !!};
        var data = "sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                $('#view-detail-component').html(res)
            }
        });
    }

    function deleteCertificatePosition(id, sales_id, location_id, block_id, unit_id, sales_type) {
        var url = {!! json_encode(url('/prop/sales/detail_certificate_position_delete')) !!};
        var data = "id="+id+"&sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id+"&sales_type="+sales_type;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                detailCertificatePosition(res.sales_id, res.location_id, res.block_id, res.unit_id, res.sales_type);
                toastr.success(res.text);
            }
        });
    }

    function detailMarketingFee(id, sales_id, location_id, block_id, unit_id, affiliate_num, affiliate_fee, affiliate_fee_paid) {
        var url = {!! json_encode(url('/prop/sales/detail_marketing_fee')) !!};
        var data = "id="+id+"&sales_id="+sales_id+"&location_id="+location_id+"&block_id="+block_id+"&unit_id="+unit_id+"&affiliate_num="+affiliate_num+"&affiliate_fee="+affiliate_fee+"&affiliate_fee_paid="+affiliate_fee_paid;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success:function(res){
                $('#view-detail-component').html(res)
            }
        });
    }

    (function() {
        $('form[name="sales_step"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailSalesStep(res.sales_id, res.location_id, res.block_id, res.unit_id, res.sales_type)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-sales-step-form-store').modal('hide')
                    $('#detail-sales-step-form-update-'+res.id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="certificate_step"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailCertificateStep(res.sales_id, res.location_id, res.block_id, res.unit_id, res.sales_type)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-certificate-step-form-store').modal('hide')
                    $('#detail-certificate-step-form-update-'+res.id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="certificate_position"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailCertificatePosition(res.sales_id, res.location_id, res.block_id, res.unit_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-certificate-position-form-store').modal('hide')
                    $('#detail-certificate-position-form-update-'+res.id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="installment"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailInstallment(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-installment-form-store').modal('hide')
                    $('#detail-installment-form-update-'+res.id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="installment_bill"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailInstallment(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-installment-form-store').modal('hide')
                    $('#detail-installment-form-update-'+res.id).modal('hide')
                    $('#detail-installment-form-bill').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="marketing_fee"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailMarketingFee(res.id, res.sales_id, res.location_id, res.block_id, res.unit_id, res.affiliate_num, res.affiliate_fee, res.affiliate_fee_paid)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-marketing-fee-form-store').modal('hide')
                    $('#detail-marketing-fee-form-update-'+res.id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="kpr"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailKpr(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-kpr-form-store').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="kpr_credit"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailKpr(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-kpr-credit-form-store').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="kpr_credit_cancel"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailKpr(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#kpr-credit-modal-cancel-'+res.sales_paid_id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="kpr_return"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailKpr(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-kpr-return').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="kpr_change_bank"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailKpr(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-kpr-form-change-bank').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="kpr_return_cancel"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailKpr(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#kpr-return-modal-cancel-'+res.sales_return_id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="installment_delete"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailInstallment(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#installment-modal-delete-'+res.sales_paid_id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="installment_update"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailInstallment(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#installment-modal-update-'+res.sales_paid_id).modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="installment_biaya_tambahan"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailInstallment(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-installment-form-biaya-tambahan').modal('hide')

                    // redirect to worklist by unit
                    var baseUrl = {!! json_encode(url('/prop/sales/detail/')) !!};
                    var baseUrl = baseUrl + "/" + res.id;
                    window.location.href = baseUrl;
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="installment_potongan"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        detailInstallment(res.sales_id)
                        toastr.success(res.text)
                    } else {
                        toastr.error(res.text)
                    }

                    $('#detail-installment-form-potongan').modal('hide')

                    // redirect to worklist by unit
                    var baseUrl = {!! json_encode(url('/prop/sales/detail/')) !!};
                    var baseUrl = baseUrl + "/" + res.id;
                    window.location.href = baseUrl;
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="update_marketers"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    toastr.success(res.text)
                    detailInstallment(res.sales_id)

                    $('#form-update-marketer').modal('hide')

                    var baseUrl = {!! json_encode(url('/prop/sales/detail/')) !!};
                    var baseUrl = baseUrl + "/" + res.id;
                    window.location.href = baseUrl;
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        $('form[name="upload-document-ktp-kk"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            if (isSubmitting) {
                return; // Jika sedang dalam proses submit, keluar dari fungsi
            }

            isSubmitting = true;
            // Lakukan kueri Ajax di sini

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    isSubmitting = false;
                    if (res.status == 'success') {
                        toastr.success(res.text)
                        var baseUrl = {!! json_encode(url('/prop/sales/detail/')) !!};
                        var baseUrl = baseUrl + "/" + res.id;
                        window.location.href = baseUrl;
                    } else {
                        toastr.error(res.text)
                    }

                    $('.modal').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });

        
        $('form[name="update-affiliate-fee"]').on('submit', function(e) {
            
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            if (isSubmitting) {
                return; // Jika sedang dalam proses submit, keluar dari fungsi
            }

            isSubmitting = true;
            // Lakukan kueri Ajax di sini

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        isSubmitting = false;
                        toastr.success(res.text)
                        var baseUrl = {!! json_encode(url('/prop/sales/detail/')) !!};
                        var baseUrl = baseUrl + "/" + res.id;
                        window.location.href = baseUrl;
                    } else {
                        toastr.error(res.text)
                    }

                    $('.modal').modal('hide')
                },
                error: function (data) {
                    toastr.error(data)
                }
            });
        });
    })();
</script>
