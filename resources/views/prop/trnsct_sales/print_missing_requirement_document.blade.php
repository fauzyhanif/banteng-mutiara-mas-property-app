<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Kekurangan Dokumen Persyaratan KPR</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-right">Purwakarta, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}</p>

                        <p>
                            Yth. <br>
                            {{ $sales->customer->name }}
                        </p>         

                        <p>
                            Perihal : Penyampaian Kekurangan Dokumen Persyaratan Formalitas Permohonan KPR Perum Benteng Mutiara Mas,
                        </p>

                        <p>
                            Sehubungan dengan Permohonan KPR yang sedang berjalan proses {{ $sales->bank->name }}, dengan data sebagai berikut :
                            <table class="table table-sm table-borderless ml-4 mt-0" style="margin-top: -10px !important">
                                <tbody>
                                    <tr>
                                        <td width="25%">Nomor Permohonan</td>
                                        <td class="text-right" width="4%">:</td>
                                        <td>{{ sprintf('%04d', $sales->sppr_number) }}/BMM/SPPR/{{ substr($sales->sales_date, 5,2) }}/{{ substr($sales->sales_date, 0,4) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Judul Permohonan</td>
                                        <td class="text-right" width="4%">:</td>
                                        <td>Pengajuan KPR perum benteng mutiara mas</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Tanggal Pengajuan</td>
                                        <td class="text-right" width="4%">:</td>
                                        <td>{{ GeneralHelper::konversiTgl($sales->sales_date, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Tanggal Penerimaan Berkas</td>
                                        <td class="text-right" width="4%">:</td>
                                        <td>{{ GeneralHelper::konversiTgl(date('Y-m-d', strtotime('+14 days', strtotime($sales->sales_date))), 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Pemohon</td>
                                        <td class="text-right" width="4%">:</td>
                                        <td>{{ $sales->customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Marketing</td>
                                        <td class="text-right" width="4%">:</td>
                                        <td>{{ $sales->marketer->sdm->name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </p> 

                        <p>
                            Bahwa sesuai dengan surat Pemberitahuan Kekurangan Persyaratan Formalitas yang telah kami terima
                            melalui surat Nomor : {{ sprintf('%04d', $sales->sppr_number) }}/BMM/SPPR/{{ substr($sales->sales_date, 5,2) }}/{{ substr($sales->sales_date, 0,4) }} 
                            tanggal {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}, bersama ini kami menyampaikan
                            kekurangan kelengkapan persyaratan formalitas sebagai berikut:

                            <table class="table table-sm table-borderless ml-4 mt-0" style="margin-top: -10px !important">
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach (explode(',', $sales->kpr_missing_requirement_document) as $item)
                                        <tr>
                                            <td width="5%" class="text-right">{{ $no }}.</td>
                                            <td>{{ $item }}</td>
                                        </tr>
                                        @php
                                            $no += 1;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </p>       

                        <p>Dengan berkas kekurangan diatas agar segera di lengkapi paling lambat tanggal {{ GeneralHelper::konversiTgl($sales->kpr_document_collection_limit, 'ttd') }}</p>  

                        <p>Atas perhatian dan kerjasamanya, kami ucapkan terima kasih</p>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-md-6 col-print-6 text-center mb-4"></div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Mengetahui,</div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2"></div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ Session::get('auth_nama') }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
