@extends('index')

@section('content')
<section class="content-header">
    <h1>List Unit</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Input Pembayaran
                        </h3>
                    </div>
                    <div class="card-body">
                        <form name="sales_pay" action="{{ route('prop.sales.pay') }}" method="POST">

                            @csrf
                            <input type="hidden" name="id" value="{{ $sale->id }}">
                            <input type="hidden" name="sales_type" value="{{ $sale->sales_type }}">
                            <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">
                            <input type="hidden" name="location_id" value="{{ $sale->location_id }}">
                            <input type="hidden" name="block_id" value="{{ $sale->block_id }}">
                            <input type="hidden" name="unit_id" value="{{ $sale->unit_id }}">
                            <input type="hidden" name="customer_id" value="{{ $sale->customer_id }}">
                            <input type="hidden" name="customer_name" value="{{ $sale->customer->name }}">
                            <input type="hidden" name="affiliate_num" value="{{ $sale->affiliate_num }}">
                            <input type="hidden" name="affiliate_fee" value="{{ $sale->affiliate_fee }}">
                            @if ($sale->sales_type == 'CASH')
                                <input type="hidden" name="ttl_trnsct" value="{{ $sale->ttl_trnsct }}">
                            @else
                                <input type="hidden" name="ttl_trnsct" value="{{ ($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->discount }}">
                            @endif
                            <input type="hidden" name="discount" value="{{ $sale->discount }}">
                            <input type="hidden" name="uang_muka" value="{{ $unit[0]->uang_muka }}">

                            <div class="row">
                                @if ($sale->sales_type == 'CASH')
                                    <div class="col-lg-6 mb-3 border">
                                        <div class="col-lg-12 m-2">
                                            <p>Total Tagihan <span class="text-red">*</span></p>
                                            <h2 class="font-weight-bold" style="margin-top: -15px">
                                                Rp {{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->discount) }}
                                            </h2>
                                            @if ($sale->discount > 0)
                                                (<span class="text-green">Sudah dikurangi potongan sebesar <b>{{ GeneralHelper::rupiah($sale->discount) }}</b></span>)
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-6 mb-3 border">
                                        <div class="col-lg-12 m-2">
                                            <p>Uang Muka yang Harus Dibayar <span class="text-red">*</span></p>
                                            <h2 class="font-weight-bold" style="margin-top: -15px">
                                                Rp {{ GeneralHelper::rupiah($unit[0]->uang_muka) }}
                                            </h2>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-lg-4 mb-3 border">
                                        <div class="col-lg-12 m-2">
                                            <p>Pengajuan KPR <span class="text-red">*</span></p>
                                            <h3 class="font-weight-bold" style="margin-top: -15px">
                                                Rp {{ GeneralHelper::rupiah($sale->kpr_credit_pengajuan) }}
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-3 border">
                                        <div class="col-lg-12 m-2">
                                            <p>Total Tagihan <span class="text-red">*</span></p>
                                            <h3 class="font-weight-bold" style="margin-top: -15px">
                                                Rp {{ GeneralHelper::rupiah(($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->discount) }}
                                            </h3>
                                            @if ($sale->discount > 0)
                                            (<span class="text-green">Sudah dikurangi potongan sebesar
                                                <b>{{ GeneralHelper::rupiah($sale->discount) }}</b></span>)
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-3 border">
                                        <div class="col-lg-12 m-2">
                                            <p>Uang Muka yang Harus Dibayar <span class="text-red">*</span></p>
                                            <h3 class="font-weight-bold" style="margin-top: -15px">
                                                @if ($unit && $unit[0]->uang_muka > (($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->discount))
                                                    Rp {{ GeneralHelper::rupiah(($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->discount) }}
                                                @else
                                                    Rp {{ GeneralHelper::rupiah($unit[0]->uang_muka) }}
                                                @endif
                                            </h3>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="">Tanggal Pembayaran <span class="text-red">*</span></label>
                                        <input type="date" name="trnsct_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="">Pembayaran Masuk Ke <span class="text-red">*</span></label>
                                        <select name="account_id" class="form-control" required>
                                            <option value="">** Pilih Kas / Bank</option>
                                            @foreach ($cashes as $cash)
                                                <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">Nominal Bayar <span class="text-red">*</span></label>
                                        @if ($unit && $unit[0]->uang_muka > (($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->discount))
                                        <input type="text" name="ttl_trnsct_paid" class="form-control money" value="{{ ($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->discount }}" required>
                                        @else
                                        <input type="text" name="ttl_trnsct_paid" class="form-control money" value="{{ $unit[0]->uang_muka }}" required>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">Keterangan</label>
                                        <textarea name="description" class="form-control">Uang Muka</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12 mt-4" id="btn-save">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        SIMPAN PEMBAYARAN
                                    </button>
                                </div>

                                @if ($sale->sales_type == 'KPR')

                                    <div class="col-lg-12 mt-4">
                                        <div class="alert alert-danger" style="background-color:#FE8F8F !important">
                                            <h5>Perhatian!</h5>
                                            Angsuran pertama harus sesuai dengan uang muka yang harus dibayar yaitu <b>Rp {{ GeneralHelper::rupiah($unit[0]->uang_muka) }}</b>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.trnsct_sales.form_pay_js')
@endsection
