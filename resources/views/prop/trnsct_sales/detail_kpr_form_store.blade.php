<!-- The Modal -->
<div class="modal" id="detail-kpr-form-store">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="kpr" action="{{ route('prop.sales.detail_kpr_store') }}" method="POST">

                @csrf

                <input type="hidden" name="id" value="{{ $sale->id }}">
                <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">
                <input type="hidden" name="location_id" value="{{ $sale->location_id }}">
                <input type="hidden" name="block_id" value="{{ $sale->block_id }}">
                <input type="hidden" name="unit_id" value="{{ $sale->unit_id }}">
                <input type="hidden" name="customer_id" value="{{ $sale->customer_id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Approval Pengajuan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="kpr_result_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>

                    <div class="form-group">
                        <label>Status <span class="text-red">*</span></label>
                        <select name="kpr_status" class="form-control" required onchange="kprStatus(this.value)">
                            <option>ACCEPTED</option>
                            <option>REJECTED</option>
                        </select>
                    </div>

                    <div class="form-group" id="kpr-credit-acc">
                        <label>Kredit Acc <span class="text-red">*</span></label>
                        <input type="text" name="kpr_credit_acc" class="form-control money" value="{{ $sale->kpr_credit_pengajuan }}">
                    </div>

                    <div class="form-group">
                        <label>Biaya Tambahan <span class="text-red">*</span></label>
                        <input type="text" name="additional_cost" class="form-control money" value="{{ $sale->additional_cost }}">
                    </div>

                    <div class="form-group kpr-credit-rejected" style="display: none">
                        <label>Uang konsumen dikembalikan sebesar</label>
                        <input type="text" name="return_plan" class="form-control money" value="0">
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="kpr_desc" class="form-control"></textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

function kprStatus(params) {
    if (params === 'ACCEPTED') {
        $('#kpr-account').css('display', 'block');
        $('#kpr-credit-acc').css('display', 'block');
        $('.kpr-credit-rejected').css('display', 'none');
    } else {
        $('.kpr-credit-rejected').css('display', 'block');
        $('#kpr-account').css('display', 'none');
        $('#kpr-credit-acc').css('display', 'none');
    }
}
</script>
