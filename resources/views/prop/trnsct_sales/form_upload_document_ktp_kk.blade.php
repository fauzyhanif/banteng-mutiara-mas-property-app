<!-- The Modal -->
<div class="modal" id="form-upload-document-ktp-kk">
    <div class="modal-dialog">
        <div class="modal-content">

            <form name="upload-document-ktp-kk" action="{{ route('prop.sales.upload-document-ktp-kk') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ $sales->id }}">
                <input type="hidden" name="customer_id" value="{{ $sales->customer_id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Upload Dokumen Ktp & Kk</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ktp</label>
                        <input type="file" name="file_ktp" class="form-control form-file">
                    </div>

                    <div class="form-group">
                        <label>Kk</label>
                        <input type="file" name="file_kk" class="form-control form-file">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>

$('.form-file').change(function() {

  var filePath = this.value;

  // Allowing file type
  var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;

  if (!allowedExtensions.exec(filePath)) {
    alert('Jenis file yang diperbolehkan : jpg, jpeg, png, pdf !');
    this.value = '';
    return false;
  }
});
</script>