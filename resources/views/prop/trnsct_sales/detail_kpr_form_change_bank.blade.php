<!-- The Modal -->
<div class="modal" id="detail-kpr-form-change-bank">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="kpr_change_bank" action="{{ route('prop.sales.detail_kpr_change_bank') }}" method="POST">

                @csrf

                <input type="hidden" name="id" value="{{ $sale->id }}">
                <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Ganti Bank Tujuan KPR</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Bank</label>
                        <select name="kpr_bank" class="form-control">
                            <option value="">-- Pilih Bank --</option>
                            @foreach ($banks as $bank)
                            <option value="{{ $bank->sdm_id }}" {{ ($sale->kpr_bank == $bank->sdm_id) ? 'selected' : '' }}>{{ $bank->sdm->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

function kprStatus(params) {
    if (params === 'ACCEPTED') {
        $('#kpr-account').css('display', 'block');
        $('#kpr-credit-acc').css('display', 'block');
    } else {
        $('.kpr-credit-rejected').css('display', 'block');
        $('#kpr-account').css('display', 'none');
        $('#kpr-credit-acc').css('display', 'none');
    }
}
</script>
