<!-- The Modal -->
<div class="modal" id="modal-price">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Rincian Harga</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-control bg-info mt-4">
                    <label>CASH</label>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <th>Item</th>
                        <th width="35%" class="text-right">Harga</th>
                    </thead>
                    <tbody>
                        @php $ttl_price = 0 @endphp
                        @foreach ($unit_prices as $price)
                            @if ($price->sales_type == 'CASH')
                                <tr>
                                    <td>{{ $price->priceItem->name }}</td>
                                    <td class="text-right">{{ GeneralHelper::rupiah($price->price) }}</td>
                                </tr>
                                @php $ttl_price += $price->price @endphp
                            @endif
                        @endforeach
                    </tbody>
                    <tfoot>
                        <th class="text-center">Total</th>
                        <th class="text-right">{{ GeneralHelper::rupiah($ttl_price) }}</th>
                    </tfoot>
                </table>

                <div class="form-control bg-info mt-4">
                    <label>KPR</label>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <th>Item</th>
                        <th width="35%" class="text-right">Harga</th>
                    </thead>
                    <tbody>
                        @php $ttl_price = 0 @endphp
                        @foreach ($unit_prices as $price)
                        @if ($price->sales_type == 'KPR')
                        <tr>
                            <td>{{ $price->priceItem->name }}</td>
                            <td class="text-right">{{ GeneralHelper::rupiah($price->price) }}</td>
                        </tr>
                        @php $ttl_price += $price->price @endphp
                        @endif
                        @endforeach
                    </tbody>
                    <tfoot>
                        <th class="text-center">Total</th>
                        <th class="text-right">{{ GeneralHelper::rupiah($ttl_price) }}</th>
                    </tfoot>
                </table>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
