@extends('index')
@section('content')
<input type="hidden" name="pages" value="1">

<section class="content-header">
    <h1>Daftar Penjualan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Penjualan
                        <div class="card-tools">
                            <a href="{{ route('prop.sales.list_unit') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Input Penjualan
                            </a>

                            <a href="{{ url('prop/sales-export') }}" class="btn btn-sm btn-warning">
                                <span class="badge badge-danger" id="export-sales-count">0</span> Export Excel
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        
                        <div class="row mb-3">
                            <div class="col-md-3">
                                <select name="filter" id="" class="form-control form-control-sm" onchange="handleFilter(this.value)">
                                    <option value="all" {{ $filter == 'all' ? 'selected' : '' }}>Semua Penjualan</option>
                                    <option value="slow_progress" {{ $filter == 'slow_progress' ? 'selected' : '' }}>Penjualan Progres Lambat</option>
                                    <option value="kpr_rejected" {{ $filter == 'kpr_rejected' ? 'selected' : '' }}>Penjualan KPR Reject</option>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <select name="keyword_column" id="" class="form-control form-control-sm">
                                            <option value="customer_name">Nama Konsumen</option>
                                            <option value="block_name">Nama Blok</option>
                                            <option value="sales_type">Type Penjualan</option>
                                        </select>
                                    </div>

                                    <input type="text" name="keyword" class="form-control form-control-sm" id="form-search">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-sm btn-primary" onclick="search()"><i class="fas fa-search"></i> Cari</button>
                            </div>
                            <div class="col-md-2 text-right">
                                <span>Show</span>
                                <select name="per_page" onchange="getData(1)">
                                    <option>25</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                                <span>entries</span>
                            </div>
                        </div>

                        <div id='view'>
                            <div class="table-responsive">
                                @include('prop.trnsct_sales.index2_list_data')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.trnsct_sales.form_delete_sales')
<script type="text/javascript">
    $(window).on('hashchange', function() {

    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');

        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            getData(page);
        }
    }
});

$(document).ready(function() {
    get_count_sales_export();
    $(document).on('click', '.pagination a' ,function(event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl=$(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        $('input[name="pages"]').val(page)

        getData(page);
    });

    $('#form-search').on('keypress', function(e){
        if(e.which == 13) { // Check if the key pressed is Enter (key code 13)
            e.preventDefault(); // Prevent the default form submission
            getData(1);
        }
    });
});

function handleFilter(params) {
    var url = {!! json_encode(route('prop.sales')) !!} + '?filter=' + params;
    window.location.href = url;
}


function getData(page){
    var keyword = $('input[name="keyword"]').val();
    var keyword_column = $('select[name="keyword_column"]').val();
    var per_page = $('select[name="per_page"]').val();
    var filter = $('select[name="filter"]').val();

    keyword = keyword.replace(" ", "-");
    $.ajax({
        url: {!! json_encode(route('prop.sales')) !!} + '?page=' + page + '&per_page=' + per_page + '&keyword_column=' + keyword_column + '&keyword=' + keyword + '&filter=' + filter,
        type: "get",
        datatype: "html",
    }).done(function(data){
        $("#view").empty().html(data);
        location.hash = page;
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        alert('No response from server');
    });
}

function search() {
    var page = $('input[name="pages"]').val();
    getData(page);
}

function show_modal_delete_sales(id) {
    var form = $('#modal-delete-sales').modal('show');
    form.find('input[name="delete_sales_id"]').val(id)
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="delete_sales"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                    getData(1);
                } else {
                    toastr.error(res.text)
                }
            }
        });

        $('#modal-delete-sales').modal('hide');
    });
})();

function send_to_sales_export(id) {
     $.ajax({
        type: 'POST',
        url: {!! json_encode(route('prop.sales-export.create')) !!},
        dataType:'json',
        data: "id="+id,
        success: function (res) {
            if (res.status == 'success') {
                toastr.success(res.text)
                get_count_sales_export()
            } else {
                toastr.error(res.text)
            }
        }
    });
}

function get_count_sales_export() {
     $.ajax({
        type: 'GET',
        url: {!! json_encode(route('prop.sales-export.count')) !!},
        dataType:'json',
        success: function (res) {
            $('#export-sales-count').html(res);
        }
    });
}

</script>
@endsection
