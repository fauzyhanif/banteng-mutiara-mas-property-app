@extends('index')

@section('content')
<section class="content-header">
    <h1>List Unit</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Form Tampilkan Unit
                        </h3>
                    </div>
                    <div class="card-body">
                        <form name="show_units" action="{{ route('prop.sales.show_units') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Lokasi <span class="text-red">*</span></label>
                                <select name="location_id" class="form-control" required onchange="showBlocks(this.value)">
                                    <option value="">** Pilih Lokasi</option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->location_id }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Blok <span class="text-red">*</span></label>
                                <select name="block_id" class="form-control" required>
                                    <option value="">** Pilih Blok Dahulu</option>
                                </select>
                            </div>

                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    TAMPILKAN UNIT
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8" id="view-list-units">
            </div>
        </div>
    </div>
</section>

<script>
function showBlocks(location_id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+location_id, 'select[name="block_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="show_units"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#view-list-units').html(res)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
@endsection
