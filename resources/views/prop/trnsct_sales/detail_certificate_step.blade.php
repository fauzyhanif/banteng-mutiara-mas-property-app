<div class="card-header">
    <h3 class="card-title">Progress Step Sertifikat</h3>
    <div class="card-tools">
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#detail-certificate-step-form-store">
            <i class="fas fa-plus-circle"></i> &nbsp; Input Progress Baru
        </button>
    </div>
</div>

<div class="card-body" style="max-height: 500px; overflow-y:scroll">

    @if (count($unit_steps) > 0)
        <!-- The time line -->
        <div class="timeline">
            @foreach ($unit_steps as $step)
                <div>
                    <i class="fas fa-envelope {{ ($loop->first) ? 'bg-green' : 'bg-gray' }}"></i>
                    <div class="timeline-item">
                        <span class="time font-weight-bold"><i class="fas fa-callendar"></i> {{ GeneralHelper::konversiTgl($step->step_date) }}</span>
                        <h3 class="timeline-header font-weight-bold {{ ($loop->first) ? 'text-green' : 'text-gray' }}">{{ $step->step->name }}</h3>

                        <div class="timeline-body">
                            {{ $step->step_desc }}

                            @if ($step->step_image != '')
                                <br>
                                <br>
                                Dokumen : <br>
                                <a href="{{ url('public/sales_step_certificate', $step->step_image) }}" target="_blank">Buka Dokumen</a>

                            @endif
                        </div>

                        <div class="timeline-footer">
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail-certificate-step-form-update-{{ $step->id }}">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-xs" onclick="deleteCertificateStep({{ $step->id }}, '{{ $sales_id }}', '{{ $location_id }}', '{{ $block_id }}', '{{ $unit_id }}', '{{ $sales_type }}')">
                                <i class="fas fa-trash"></i>
                            </button>

                            @include('prop.trnsct_sales.detail_certificate_step_form_update')

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <h3 class="text-center">Belum ada progress</h3>
    @endif
</div>

@include('prop.trnsct_sales.detail_certificate_step_form_store')
@include('prop.trnsct_sales.detail_js')
