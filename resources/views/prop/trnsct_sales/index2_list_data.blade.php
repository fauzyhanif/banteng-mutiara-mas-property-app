<table class="table table-bordered table-striped datatable">
    <thead class="bg-info">
        <th width="12%">Tgl & Tipe Penjualan</th>
        <th>Unit</th>
        <th>Tipe</th>
        <th>Step Terakhir</th>
        <th>Harga</th>
        <th>Konsumen</th>
        <th>Marketer</th>
        <th width="10%">Aksi</th>
    </thead>
    <tbody>
        @foreach ($datas as $item)
            <tr>
                <td>
                    {{ GeneralHelper::konversiTgl($item->sales_date, 'slash') }} <br>
                    @if ($item->sales_type == 'CASH')
                        <span class="badge badge-success">CASH</span>
                    @elseif($item->sales_type == 'KPR')
                        @if ($item->kpr_status == 'WAITING')
                            <span class="badge badge-warning">KPR ({{ $item->kpr_status }})</span>
                        @elseif ($item->kpr_status == 'ACCEPTED')
                            <span class="badge badge-info">KPR ({{ $item->kpr_status }})</span>
                        @else
                            <span class="badge badge-danger">KPR ({{ $item->kpr_status }})</span> <br>
                            <span class="text-muted">{{ $item->kpr_desc }}</span>
                        @endif
                    @endif
                </td>
                <td>
                    <b>{{ $item->block->name }} No {{ $item->unit_id }}</b><br> 
                    <span class="text-secondary">{{ $item->location->name }}</span> 
                </td>
                <td>{{ $item->unit->unitType->name }}</td>
                <td>
                    <b>{{ $item->last_sales_step->name }}</b> 
                    @if ($item->sales_type == 'KPR' && $item->step_sales != '0')
                        <br> <span class="text-secondary">{{ GeneralHelper::konversiTgl($item->step_sales_date, 'slash') }}</span>                                                
                    @endif

                    @php
                        $tanggal_pertama = $item->sales_date;
                        $tanggal_kedua = date('Y-m-d');

                        // Konversi tanggal menjadi objek DateTime
                        $datetime_pertama = new DateTime($tanggal_pertama);
                        $datetime_kedua = new DateTime($tanggal_kedua);

                        // Hitung selisih antara kedua tanggal
                        $selisih = $datetime_pertama->diff($datetime_kedua);

                        $step_sales_slow_progress = [0, 1, 28];
                    @endphp

                    @if ($item->sales_type == 'KPR' && $item->kpr_status == 'WAITING' && $selisih->days > 14 && in_array($item->step_sales, $step_sales_slow_progress))
                        <br> <span class="badge badge-danger"><i class="fas fa-exclamation-triangle"></i> Progres Lambat</span>
                    @endif
                    
                </td>
                <td>{{ number_format($item->ttl_trnsct,0,',','.') }}</td>
                <td>
                    {{ $item->customer->name }} <br> 
                    <span class="text-muted">{{ $item->customer->phone_num }}</span> <br>
                    <span class="text-muted">{{ $item->customer->institute }}</span>
                </td>
                <td>
                    @if ($item->affiliate_num != '')
                        {{ $item->marketer->sdm->name }} <br>
                        Fee : {{ number_format($item->affiliate_fee,0,',','.') }}
                    @endif
                </td>
                <td>
                    <a href="{{ url('prop/sales/detail', $item->id) }}" class="btn btn-xs btn-info">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ url('prop/sales/print_sppr', $item->id) }}" target='_blank' class='btn btn-secondary btn-xs'>
                        <i class="fas fa-print"></i>
                    </a>
                    <button type="button" class="btn btn-xs btn-danger" onclick="show_modal_delete_sales({{ $item->id }})">
                        <i class="fas fa-trash"></i>
                    </button>

                    <button type="button" class="btn btn-xs btn-warning" onclick="send_to_sales_export('{{ $item->sales_id }}')">
                        <i class="fas fa-file-excel"></i>
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="row mt-4">
    <div class="col-md-8">
        {!! $datas->render() !!}
    </div>
    <div class="col-md-4">
        <p class="text-right">
            showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }}
        </p>
    </div>
</div>



