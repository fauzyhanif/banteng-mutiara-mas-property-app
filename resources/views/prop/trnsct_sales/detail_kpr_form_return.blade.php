<!-- The Modal -->
<div class="modal" id="detail-kpr-return">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="kpr_return" action="{{ route('prop.sales.detail_kpr_return') }}" method="POST">

                @csrf

                <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">
                <input type="hidden" name="customer_name" value="{{ $sale->customer->name }}">
                <input type="hidden" name="return_plan_paid" value="{{ $sale->return_plan_paid }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Return Uang Konsumen</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="trnsct_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>


                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Uang diambil dari <span class="text-red">*</span></label>
                            <select name="account_id" class="form-control" required>
                                <option value="">** Pilih Kas / Bank</option>
                                @foreach ($cashes as $cash)
                                <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Jumlah <span class="text-red">*</span></label>
                            <input type="text" name="amount" class="form-control money" value="0" required>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

function kprStatus(params) {
    if (params === 'ACCEPTED') {
        $('#kpr-account').css('display', 'block');
        $('#kpr-credit-acc').css('display', 'block');
    } else {
        $('.kpr-credit-rejected').css('display', 'block');
        $('#kpr-account').css('display', 'none');
        $('#kpr-credit-acc').css('display', 'none');
    }
}
</script>
