<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function() {
    $("#select-customer").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_customer') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });

    $("#select-marketer").select2({
        val: '0',
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_marketer') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="sales_store"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    // redirect to worklist by unit
                    var baseUrl = {!! json_encode(url('/prop/sales/form_pay/')) !!};
                    var baseUrl = baseUrl + "/" + res.sales_id;
                    window.location.href = baseUrl;
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function salesType(params) {
    var harga_cash = '{{ $unit->harga_jual_cash }}';
    var harga_kpr = '{{ $unit->harga_jual_kpr }}';
    if (params == 'CASH') {
        $('.view-kpr').css('display', 'none');
        $('input[name="basic_price"]').val(harga_cash);
        $('.form-kpr').attr('required', false);
    } else {
        $('.view-kpr').css('display', 'block');
        $('input[name="basic_price"]').val(harga_kpr)
        $('.form-kpr').attr('required', true);
    }
}

function showMarketingFee(affiliateNum) {
    var url = {!! json_encode(url('/prop/sales/get-marketing-fee')) !!};
    var locationId = $('input[name="location_id"]').val();
    var blockId = $('input[name="block_id"]').val();
    var unitId = $('input[name="unit_id"]').val();
    var data = 'affiliate_num='+affiliateNum+'&location_id='+locationId+'&block_id='+blockId+'&unit_id='+unitId;

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType:'html',
        success: function (res) {
            $('#marketing-fee-setting').html(res)
        },
        error: function (data) {
            var res = errorAlert(data);
            toastr.error(res)
        }
    });
}

</script>
