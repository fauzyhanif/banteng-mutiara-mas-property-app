<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SURAT KOMPLEN</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold"><i>SURAT KOMPLEN</i></p>

                        <table class="table table-borderless table-sm mt-3">
                            <tbody>
                                <tr>
                                    <td width="35%">Nama</td>
                                    <td>: {{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Blok/No</td>
                                    <td>: {{ $sales->block->name }} No {{ $sales->unit_id }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <p class="mt-2">
                            Mengajukan Komplen Mengenai :
                        </p>

                        <table class="table table-borderless table-sm">
                            <tbody>
                                @php
                                    $complains = explode(",", $sales->complain_list);
                                    $no = 1;
                                @endphp
                                @foreach ($complains as $complain)
                                    <tr>
                                        <td width="5%">{{ $no }}.</td>
                                        <td>{{ $complain }}</td>
                                    </tr>
                                @php $no += 1; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, {{ GeneralHelper::konversiTgl($sales->complain_date, 'ttd') }}</div>

                    <div class="col-md-6 col-print-6 text-center mb-4">Yang Menerima,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Yang Mengajukan,</div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->complain_person }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->customer->name }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
