<!-- The Modal -->
<div class="modal" id="detail-certificate-position-form-store">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="certificate_position" action="{{ route('prop.sales.detail_certificate_position_store') }}" enctype="multipart/form-data" method="POST">

                @csrf
                <input type="hidden" name="sales_id" value="{{ $sales_id }}">
                <input type="hidden" name="location_id" value="{{ $location_id }}">
                <input type="hidden" name="block_id" value="{{ $block_id }}">
                <input type="hidden" name="unit_id" value="{{ $unit_id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Input Progress Posisi Sertifikat</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Posisi <span class="text-red">*</span></label>
                        <select name="step_position" class="form-control" required>
                            <option>KANTOR</option>
                            <option>BANK</option>
                            <option>KONSUMEN</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="step_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>

                    <div class="form-group">
                        <label>Bukti Penyerahan</label>
                        <input type="file" name="step_image" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="step_desc" class="form-control"></textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
