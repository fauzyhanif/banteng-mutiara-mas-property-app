<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TANDA TERIMA KUNCI</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold">TANDA TERIMA KUNCI</p>
                        <p class="text-center font-weight-bold mb-2" style="margin-top: -15px">
                            {{ sprintf('%04d', $sales->key_handover_number) }}/BAST-KC/LSJ/{{ substr($sales->key_handover_date, 5,2) }}/{{substr($sales->key_handover_date, 0,4) }}
                        </p>

                        <p class="font-weight-bold mt-3">Telah diterima kunci rumah Perumahan Benteng Mutiara Mas :</p>

                        <table class="table table-borderless table-sm" style="margin-top: -10px">
                            <tbody>
                                <tr>
                                    <td width="35%">Nama</td>
                                    <td>: {{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Blok / No</td>
                                    <td>: {{ $sales->block->name }} No {{ $sales->unit_id }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Type</td>
                                    <td>: {{ $sales->unit->unitType->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Kunci</td>
                                    <td>: Satu Set</td>
                                </tr>
                            </tbody>
                        </table>

                        <p class="mt-2">
                            Masa pemeliharaan <b>100 hari sejak kunci rumah</b> tersebut dengan catatan tidak merenovasi dan memperbaiki sendiri.
                        </p>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, {{ GeneralHelper::konversiTgl($sales->key_handover_date, 'ttd') }}</div>

                    <div class="col-md-6 col-print-6 text-center mb-4">Yang Menyerahkan,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Yang Menerima,</div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->key_handover_person }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->customer->name }})</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
