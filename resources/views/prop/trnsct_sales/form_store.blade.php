@extends('index')

@section('content')
<section class="content-header">
    <h1>List Unit</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Informasi Unit
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="35%">Lokasi</td>
                                    <td>{{ $unit->location->name }} Blok {{ $unit->block->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Nomor Unit</td>
                                    <td>{{ $unit->number }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Type Unit</td>
                                    <td>{{ $unit->unitType->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Minimal Uang Muka</td>
                                    <td>Rp. {{ GeneralHelper::rupiah($unit->uang_muka) }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Harga Jual CASH</td>
                                    <td>
                                        Rp. {{ GeneralHelper::rupiah($unit->harga_jual_cash) }}
                                        &nbsp;&nbsp;
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-price">
                                            Lihat Rincian Harga
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%">Harga Jual KPR</td>
                                    <td>
                                        Rp. {{ GeneralHelper::rupiah($unit->harga_jual_kpr) }}
                                        &nbsp;&nbsp;
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-price">
                                            Lihat Rincian Harga
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Form Penjualan
                        </h3>
                    </div>

                    <form name="sales_store" action="{{ route('prop.sales.store') }}" method="POST">

                        @csrf
                        <input type="hidden" name="location_id" value="{{ $unit->location_id }}">
                        <input type="hidden" name="block_id" value="{{ $unit->block_id }}">
                        <input type="hidden" name="unit_id" value="{{ $unit->number }}">
                        <input type="hidden" name="basic_price" value="{{ $unit->harga_jual_cash }}">

                        <div class="card-body">
                            <div class="form-group">
                                <label>Tipe Penjualan <span class="text-red">*</span></label>
                                <select name="sales_type" class="form-control" required onchange="salesType(this.value)">
                                    <option>CASH</option>
                                    <option>KPR</option>
                                </select>
                            </div>

                            <div class="form-group view-kpr" style="display: none">
                                <label>Bank Tujuan KPR</label>
                                <select name="kpr_bank" class="form-control form-kpr">
                                    @foreach ($banks as $bank)
                                        <option value="{{ $bank->sdm_id }}">{{ $bank->sdm->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group view-kpr" style="display: none">
                                <label>Max Kredit Pengajuan</label>
                                <input type="text" name="kpr_credit_pengajuan" class="form-control money form-kpr" value="{{ $unit->max_credit_pengajuan }}">
                            </div>

                            <div class="form-group">
                                <label>Tanggal Pembelian <span class="text-red">*</span></label>
                                <input type="date" name="sales_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                            </div>

                            <div class="form-group">
                                <label>Konsumen <span class="text-red">*</span></label>
                                <div class="input-group mb-3">
                                    <select name="customer_id"  class="form-control" id="select-customer" required>
                                        <option value="0">** Pilih Konsumen</option>
                                    </select>
                                    <div class="input-group-append">
                                        <a href="{{ route('prop.customer.form_store') }}" target="_blank" class="btn btn-primary">
                                            <i class="fas fa-plus"></i> Konsumen Baru
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Komitmen Pembayaran <span class="text-red"></span></label>
                                <textarea name="payment_commitment" class="form-control" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Marketer</label>
                                <div class="input-group mb-3">
                                    <select name="affiliate_num" class="form-control" id="select-marketer" onchange="showMarketingFee(this.value)">
                                        <option value="">** Pilih Marketer</option>
                                    </select>
                                    <div class="input-group-append">
                                        <a href="{{ route('prop.marketer.form_store') }}" target="_blank" class="btn btn-primary">
                                            <i class="fas fa-plus"></i> Marketer Baru
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="marketing-fee-setting"></div>

                            <div class="form-group">
                                <label>Potongan</label>
                                <input type="text" name="discount" class="form-control money" value="0">
                            </div>

                            <div class="form-group">
                                <label>Harga Jual Cash (untuk laporan penjualan PAJAK)</label>
                                <input type="text" name="ttl_trnsct_for_pajak" class="form-control money" value="0">
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success btn-block">
                                SIMPAN & SELANJUTNYA INPUT PEMBAYARAN DLL
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.trnsct_sales.list_price')
@include('prop.trnsct_sales.form_store_js')
@endsection
