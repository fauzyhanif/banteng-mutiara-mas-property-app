<!-- The Modal -->
<div class="modal" id="detail-certificate-step-form-store">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="certificate_step" action="{{ route('prop.sales.detail_certificate_step_store') }}" method="POST" enctype="multipart/form-data">

                @csrf
                <input type="hidden" name="sales_id" value="{{ $sales_id }}">
                <input type="hidden" name="location_id" value="{{ $location_id }}">
                <input type="hidden" name="block_id" value="{{ $block_id }}">
                <input type="hidden" name="unit_id" value="{{ $unit_id }}">
                <input type="hidden" name="sales_type" value="{{ $sales_type }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Input Progress Step Sertifikat</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Step <span class="text-red">*</span></label>
                        <select name="step_id" class="form-control" required>
                            <option value="">** Pilih Step</option>
                            @foreach ($ref_steps as $rstep)
                                <option value="{{ $rstep->step_id }}">{{ $rstep->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="step_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>

                    <div class="form-group">
                        <label>Dokumen</label>
                        <input type="file" name="step_image" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="step_desc" class="form-control"></textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
