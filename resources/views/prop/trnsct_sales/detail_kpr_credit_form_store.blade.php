<!-- The Modal -->
<div class="modal" id="detail-kpr-credit-form-store">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="kpr_credit" action="{{ route('prop.sales.detail_kpr_credit_store') }}" method="POST">

                @csrf

                <input type="hidden" name="id" value="{{ $sale->id }}">
                <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">
                <input type="hidden" name="location_id" value="{{ $sale->location_id }}">
                <input type="hidden" name="block_id" value="{{ $sale->block_id }}">
                <input type="hidden" name="unit_id" value="{{ $sale->unit_id }}">
                <input type="hidden" name="ttl_trnsct" value="{{ $sale->ttl_trnsct }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Input Pencairan Uang KPR</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="trnsct_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>

                    <div class="form-group">
                        <label>Uang Masuk ke <span class="text-red">*</span></label>
                        <select name="account_id" class="form-control" required>
                            <option value="">** Pilih Rekening</option>
                            @foreach ($cashes as $cash)
                            <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Sebesar <span class="text-red">*</span></label>
                        <input type="text" name="ttl_trnsct_paid" class="form-control money" value="{{ $sale->kpr_credit_acc - $sale->kpr_credit_acc_paid }}">
                    </div>

                    <div class="form-group">
                        <label>Diterima Dari <span class="text-red">*</span></label>
                        <input type="text" name="customer_name" class="form-control" value="{{ $sale->bank->name }}">
                    </div>

                    <div class="form-group">
                        <label>Catatan</label>
                        <textarea name="description" class="form-control">Pencairan Uang KPR</textarea>
                    </div>

                    <div class="form-group">
                        <label>Jml Potongan</label>
                        <input type="text" name="installment_amount" class="form-control money" value="0">
                    </div>

                    <div class="form-group">
                        <label>Keterangan Potongan</label>
                        <textarea name="installment_desc" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Pilih Hutang ke Bank</label>
                        <select name="debt_id" class="form-control">
                            <option value="">** Pilih Hutang</option>
                            @foreach ($debts as $debt)
                            <option value="{{ $debt->debt_id }}">{{ $debt->sdm->name }} (Sisa {{ GeneralHelper::rupiah($debt->debt_total) }})</option>
                            @endforeach
                        </select>
                    </div>


                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});
</script>
