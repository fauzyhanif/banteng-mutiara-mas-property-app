<!-- The Modal -->
<div class="modal" id="detail-certificate-position-form-update-{{ $step->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="certificate_position" action="{{ route('prop.sales.detail_certificate_position_update') }}" enctype="multipart/form-data" method="POST">

                @csrf
                <input type="hidden" name="id" value="{{ $step->id }}">
                <input type="hidden" name="sales_id" value="{{ $step->sales_id }}">
                <input type="hidden" name="location_id" value="{{ $step->location_id }}">
                <input type="hidden" name="block_id" value="{{ $step->block_id }}">
                <input type="hidden" name="unit_id" value="{{ $step->unit_id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Update Progress Posisi Sertifikat</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Posisi <span class="text-red">*</span></label>
                        <select name="step_position" class="form-control" required>
                            <option {{ $step->step_position == 'KANTOR' ? 'selected' : '' }}>KANTOR</option>
                            <option {{ $step->step_position == 'BANK' ? 'selected' : '' }}>BANK</option>
                            <option {{ $step->step_position == 'KONSUMEN' ? 'selected' : '' }}>KONSUMEN</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="step_date" class="form-control" value="{{ $step->step_date }}" required>
                    </div>

                    <div class="form-group">
                        <label>Bukti Penyerahan</label>
                        <input type="file" name="step_image" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="step_desc" class="form-control">{{ $step->step_desc }}</textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
