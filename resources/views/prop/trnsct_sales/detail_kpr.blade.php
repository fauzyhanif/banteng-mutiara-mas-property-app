<div class="card-header">
    <h3 class="card-title">Informasi KPR</h3>

    <div class="card-tools">
        @if ($sale->kpr_status != 'ACCEPTED')
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#detail-kpr-form-change-bank">
                <i class="fas fa-edit"></i> &nbsp; Ganti Bank Tujuan
            </button>
        @endif

        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#detail-kpr-form-store">
            <i class="fas fa-plus-circle"></i> &nbsp; Approval Pengajuan
        </button>
    </div>

</div>

<div class="card-body">
    <table class="table table-sm table-borderless">
        <tbody>
            <tr>
                <td width="25%" class="font-weight-bold">Bank</td>
                <td>: {{ $sale->bank->name }}</td>
            </tr>
            <tr>
                <td width="25%" class="font-weight-bold">Kredit Pengajuan</td>
                <td>: {{ GeneralHelper::rupiah($sale->kpr_credit_pengajuan) }}</td>
            </tr>
            <tr>
                <td width="25%" class="font-weight-bold">Status</td>
                <td>: {{ $sale->kpr_status }}</td>
            </tr>
            @if ($sale->kpr_status != 'WAITING')
                <tr>
                    <td width="25%" class="font-weight-bold">Tgl Keluar Hasil</td>
                    <td>: {{ GeneralHelper::konversiTgl($sale->kpr_result_date) }}</td>
                </tr>
                <tr>
                    <td width="25%" class="font-weight-bold">Keterangan</td>
                    <td>: {{ $sale->kpr_desc }}</td>
                </tr>
            @endif

            @if ($sale->kpr_status == 'REJECTED')
                <tr>
                    <td width="25%" class="font-weight-bold">Uang akan dikembalikan</td>
                    <td>: {{ GeneralHelper::rupiah($sale->return_plan) }}</td>
                </tr>
                <tr>
                    <td width="25%" class="font-weight-bold">Uang sudah dikembalikan</td>
                    <td>: {{ GeneralHelper::rupiah($sale->return_plan_paid) }}</td>
                </tr>
            @endif
        </tbody>
    </table>

    @if ($sale->kpr_status == 'ACCEPTED')
        <br>

        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td width="25%" class="font-weight-bold">Biaya Tambahan</td>
                    <td>Rp {{ GeneralHelper::rupiah($sale->additional_cost) }}</td>
                </tr>
                <tr>
                    <td width="25%" class="font-weight-bold">Kredit Pengajuan</td>
                    <td>Rp {{ GeneralHelper::rupiah($sale->kpr_credit_pengajuan) }}</td>
                </tr>
                <tr>
                    <td width="25%" class="font-weight-bold">Kredit ACC</td>
                    <td>Rp {{ GeneralHelper::rupiah($sale->kpr_credit_acc) }}</td>
                </tr>
                <tr>
                    <td width="25%" class="font-weight-bold">Kredit ACC Terbayar</td>
                    <td>Rp {{ GeneralHelper::rupiah($sale->kpr_credit_acc_paid) }}</td>
                </tr>
                <tr>
                    <td width="25%" class="font-weight-bold">Sisa ACC</td>
                    <td>Rp {{ GeneralHelper::rupiah($sale->kpr_credit_acc - $sale->kpr_credit_acc_paid) }}</td>
                </tr>
            </tbody>
        </table>
    @endif

    <br>
    <hr>
    <br>

    @if ($sale->kpr_status == 'ACCEPTED')
        <h4 class="font-weight-bold">
            Daftar Pencairan
        </h4>

        @if ($sale->kpr_credit_acc > $sale->kpr_credit_acc_paid)
            <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#detail-kpr-credit-form-store">
                <i class="fas fa-plus-circle"></i> &nbsp; Input Pencairan Uang KPR
            </button>
        @endif

        <table class="table table-bordered table-sm">
            <thead class="bg-info">
                <th width="5%">No</th>
                <th width="30%">No Kwitansi & Tgl</th>
                <th>Keterangan</th>
                <th width="15%" class="text-right">Nominal</th>
                <th width="15%" class="text-center">Batalkan</th>
            </thead>
            <tbody>
                @php $total = 0; @endphp
                @php $no = 1; @endphp
                @foreach ($payments as $payment)
                    <tr>
                        <td width="5%" class="text-center">{{ $no }}.</td>
                        <td>
                            <b>{{ $payment->sales_paid_num }}</b> <br>
                            {{ GeneralHelper::konversiTgl($payment->sales_paid_date) }}
                        </td>
                        <td>
                            {{ $payment->sales_paid_desc }}
                            @if ((int) $payment->debt_id > 0)
                                <br><br>

                                Jumlah Potongan : {{ GeneralHelper::rupiah($payment->installment_amount) }} <br>
                                Ket Potongan : {{ $payment->installment_desc }} <br>
                                Bank : {{ $payment->installment_debt->sdm->name }}
                            @endif
                        </td>
                        <td class="text-right">{{ GeneralHelper::rupiah($payment->sales_paid_amount) }}</td>
                        <td class="text-center">
                            @if ($payment->sales_paid_cancel == '1')
                                <span class="text-red">Batal</span>
                            @else
                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                data-target="#kpr-credit-modal-cancel-{{ $payment->sales_paid_id }}">
                                <i class="fas fa-trash"></i>
                            </button>

                            <div class="modal" id="kpr-credit-modal-cancel-{{ $payment->sales_paid_id }}" >
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <form name="kpr_credit_cancel" action="{{ route('prop.sales.detail_kpr_credit_store_cancel') }}"
                                            method="POST">
                                            <input type="hidden" name="sales_paid_id" value="{{ $payment->sales_paid_id }}">
                                            <input type="hidden" name="sales_id" value="{{ $payment->sales_id }}">
                                            <input type="hidden" name="sales_paid_amount" value="{{ $payment->sales_paid_amount }}">
                                            <input type="hidden" name="ttl_trnsct_paid" value="{{ $sale->ttl_trnsct_paid }}">
                                            <input type="hidden" name="kpr_credit_acc_paid" value="{{ $sale->kpr_credit_acc_paid }}">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">Konfirmasi Pembatalan</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                Yakin ingin membatalkan transaksi Pencairan KPR ini?
                                            </div>

                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Batalkan Pencairan</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            @endif
                        </td>
                    </tr>
                    @php $no += 1; @endphp
                    @php $total += $payment->sales_paid_amount; @endphp
                @endforeach
            </tbody>
            <tfoot>
                <th class="text-center" colspan="3">Total</th>
                <th class="text-right">{{ GeneralHelper::rupiah($total) }}</th>
            </tfoot>
        </table>
    @else
        <h4 class="font-weight-bold">
            Daftar Return
        </h4>

        @if ($sale->return_plan > $sale->return_plan_paid)
        <button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal"
            data-target="#detail-kpr-return">
            <i class="fas fa-plus-circle"></i> &nbsp; Input Return
        </button>
        @endif

        <table class="table table-bordered table-sm">
            <thead class="bg-info">
                <th width="30%">No Kwitansi & Tgl</th>
                <th>Keterangan</th>
                <th width="15%" class="text-right">Nominal</th>
                <th width="15%" class="text-right">Batalkan</th>
            </thead>
            <tbody>
                @php $total_return = 0; @endphp
                @foreach ($returns as $return)
                <tr>
                    <td>
                        <b>{{ $return->sales_return_num }}</b> <br>
                        {{ GeneralHelper::konversiTgl($return->sales_return_date) }}
                    </td>
                    <td>{{ $return->sales_return_desc }}</td>
                    <td class="text-right">{{ GeneralHelper::rupiah($return->sales_return_amount) }}</td>
                    <td class="text-center">
                        @if ($return->sales_return_cancel == '1')
                            <span class="text-red">Batal</span>
                        @else
                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                data-target="#kpr-return-modal-cancel-{{ $return->sales_return_id }}">
                                <i class="fas fa-trash"></i>
                            </button>

                            <div class="modal" id="kpr-return-modal-cancel-{{ $return->sales_return_id }}" class="installment-modal-delete">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <form name="kpr_return_cancel" action="{{ route('prop.sales.detail_kpr_return_cancel') }}" method="POST">
                                            <input type="hidden" name="sales_return_id" value="{{ $return->sales_return_id }}">
                                            <input type="hidden" name="sales_id" value="{{ $return->sales_id }}">
                                            <input type="hidden" name="sales_return_amount" value="{{ $return->sales_return_amount }}">
                                            <input type="hidden" name="return_plan_paid" value="{{ $sale->return_plan_paid }}">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">Konfirmasi Pembatalan Return</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                Yakin ingin membatalkan transaksi return ini?
                                            </div>

                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Batalkan Return</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        @endif
                    </td>
                </tr>
                @php $total_return += $return->sales_return_amount; @endphp
                @endforeach
            </tbody>
            <tfoot>
                <th class="text-center" colspan="2">Total</th>
                <th class="text-right">{{ GeneralHelper::rupiah($total_return) }}</th>
            </tfoot>
        </table>
    @endif
</div>

@include('prop.trnsct_sales.detail_kpr_credit_form_store')
@include('prop.trnsct_sales.detail_kpr_form_store')
@include('prop.trnsct_sales.detail_kpr_form_return')
@include('prop.trnsct_sales.detail_kpr_form_change_bank')
@include('prop.trnsct_sales.detail_js')
