<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Pembatalan</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold">SURAT PEMBATALAN</p>

                        <p>
                            Nomor : {{ sprintf('%04d', $sales->sppr_number) }}/BMM/SPPR/{{ substr($sales->sales_date, 5,2) }}/{{ substr($sales->sales_date, 0,4) }} <br>
                            Perihal : Surat Pembatalan KPR <br>
                            Kepada Yth. <br>
                            {{ $sales->customer->name }}
                        </p>         

                        <p>
                            Bahwa berhubung sesuatu hal, maka kami selaku Developer Perum Benteng Mutiara Mas bermufakat
                            untuk melakukan pembatalan atas transaksi KPR di Perum Benteng Mutiara Mas Campaka dengan
                            syarat-syarat serta ketentuan sebagaimana disebutkan dalam pasal-pasal sebagai berikut :
                        </p>

                        <p>
                            <b>PASAL 1</b> <br>
                            Developer Benteng Mutiara Mas membatalkan proses KPR tersebut, sehingga pihak developer tidak
                            akan melanjutkan proses KPR kepada pihak Bank. Developer setuju akan mengembalikan dokumen KPR 
                            dan juga kwitansi booking yang sudah diterima dari calon konsumen.
                        </p>

                        <p>
                            <b>PASAL 2</b> <br>
                            Developer setuju akan mengembalikan sepenuhnya uang muka masuk yang diterima dari konsumen.
                            Untuk nominal booking akan hangus dengan adanya surat pembatalan ini, dengan sendirinya menjadi 
                            milik developer sebagai sanksi dibatalkannya proses KPR. Sementara besaran uang muka akan 
                            dikembalikan oleh developer ke calon konsumen selambat-lambatnya 14 hari kerja setelah surat 
                            pembatalan ini diterbitkan, dan rumah yang dibatalkan terjual kembali oleh pembeli lain. Atau selambat-
                            lambatnya 1 minggu dari surat pembatalan ini.
                        </p>

                        <p>
                            <b>PASAL 3</b> <br>
                            Dengan dilaksanakan pasal 1 dan pasal 2 tersebut diatas, maka developer setuju mengenai
                            pembatalan KPR ini dianggap telah selesai tuntas dan tidak ada tuntutan atau tagihan yang berupa 
                            apapun juga dari pihak calon konsumen dan lainnya, dengan ini saling memberikan pembebasan dan 
                            pelunasan sepenuhnya.
                        </p>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-12">
                        Mengetahui, <br>

                        <br><br><br><br><br>
                        <b>Alan Suherlan</b> <br>
                        (Direktur Utama)
                    </div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
