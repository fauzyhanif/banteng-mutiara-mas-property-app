<!-- The Modal -->
<div class="modal" id="detail-installment-form-bill">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="installment_bill" action="{{ route('prop.sales.detail_installment_bill') }}" method="POST">

                @csrf

                <input type="hidden" name="sales_id" value="{{ $sale->sales_id }}">
                <input type="hidden" name="paid_off" value="{{ $tagihan_dibayar }}">
                <input type="hidden" name="reminder" value="{{ $tagihan_sisa }}">


                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Surat Tagihan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <table class="table table-bordered striped">
                        <thead class="bg-info">
                            <th width="30%">Total Tagihan</th>
                            <th width="30%">Sudah Dibayar</th>
                            <th width="30%">Sisa</th>
                        </thead>
                        <tbody>
                            @if ($sale->sales_type == 'CASH')
                                <tr>
                                    <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct) }}</td>
                                    <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct_paid) }}</td>
                                    <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->ttl_trnsct_paid) }}</td>
                                </tr>
                            @else
                                @if ($sale->kpr_credit_acc == '0')
                                    <tr>
                                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) }}</td>
                                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct_paid) }}</td>
                                        <td>{{ GeneralHelper::rupiah(($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->ttl_trnsct_paid) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->kpr_credit_acc) }}</td>
                                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid) }}</td>
                                        <td>{{ GeneralHelper::rupiah(($sale->ttl_trnsct - $sale->kpr_credit_acc) - ($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid)) }}</td>
                                    </tr>
                                @endif
                            @endif
                        </tbody>
                    </table>

                    <br>
                    
                    <div class="form-group">
                        <label>Tanggal Jatuh Tempo<span class="text-red">*</span></label>
                        <input type="date" name="due_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>

