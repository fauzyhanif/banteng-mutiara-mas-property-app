<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SPPR</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')

                {{-- konten --}}
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold">SURAT PERNYATAAN PEMBELIAN RUMAH</p>
                        <p class="text-center font-weight-bold mb-1" style="margin-top: -15px">{{ sprintf('%04d', $sales->sppr_number) }}/BMM/SPPR/{{ substr($sales->sales_date, 5,2) }}/{{ substr($sales->sales_date, 0,4) }}</p>

                        <p class="font-weight-bold">Yang bertanda tangan dibawah ini,</p>

                        <table class="table table-borderless table-sm" style="margin-top: -10px">
                            <tbody>
                                <tr>
                                    <td width="35%">Nama</td>
                                    <td>: {{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Alamat</td>
                                    <td>: {{ $sales->customer->address }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">No. Telp/HP</td>
                                    <td>: {{ $sales->customer->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Pekerjaan</td>
                                    <td>: {{ $sales->customer->job }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Instansi</td>
                                    <td>: {{ $sales->customer->institute }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Alamat Kantor</td>
                                    <td>: {{ $sales->customer->address_domicile }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <p class="font-weight-bold" style="margin-top: -10px">
                            Dengan ini menyatakan bahwa atas kemauan sendiri telah memesan 1 (satu) unit rumah berikut
                            tanahnya dengan ketentuan sebagai berikut:
                        </p>

                        <table class="table table-borderless table-sm" style="margin-top: -10px">
                            <tbody>
                                <tr>
                                    <td width="35%">Lokasi</td>
                                    <td class="font-weight-bold">: {{ $sales->location->name }} </td>
                                </tr>
                                <tr>
                                    <td width="35%">Blok/Kavling</td>
                                    <td class="font-weight-bold">: {{ $sales->block->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Type</td>
                                    <td class="font-weight-bold">: {{ $sales->unit->unitType->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Nomor Unit</td>
                                    <td class="font-weight-bold">: {{ $sales->unit_id }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Luas Tanah</td>
                                    <td class="font-weight-bold">: {{ $sales->unit->unitType->ground_area }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Tanah Lebih / Harga</td>
                                    <td class="font-weight-bold">: {{ $sales->unit->ground_hook }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Jumlah Kewajiban / Uang Muka</td>
                                    <td class="font-weight-bold">: Rp {{ GeneralHelper::rupiah($sales->ttl_trnsct) }} / Rp {{ GeneralHelper::rupiah($sales->ttl_trnsct - $sales->kpr_credit_pengajuan) }}</td>
                                </tr>

                            </tbody>
                        </table>

                        <p style="margin-top: -10px">Dengan ini menyatakan menyetujui syarat-syarat dengan ketentuan sebagai berikut:</p>

                        <ol style="margin-top: -10px">
                            <li>Semua pembayaran sesuai waktu yang telah ditentukan, tanpa adanya pemberitahuan terlebih dahulu oleh <b>PIHAK PENJUAL</b>.</li>
                            <li>
                                Apabila terjadi keterlambatan ataupun kelalaian atas kewajiban tersebut, kami bersedia mengikuti sanksi-sanksi sebagai berikut:
                                <ul>
                                    <li>Apabila ada penurunan kpr dari bank yang ditetapkan awal {{ GeneralHelper::rupiah($sales->kpr_credit_pengajuan) }}, kekurangannya akad dibebankan ke penambahan uang muka</li>
                                    <li>Dikenakan biaya administrasi keterlambatan sebesar 1.5% setiap hari dihitung dari seluruh kewajiban.</li>
                                    <li>Apabila keterlambatan / kelalaian pembayaran angsuran kepada Developer lebih dari seluruh 30 hari kerja, maka surat bukti pemesanan {{ $sales->block->name }} No {{ $sales->unit_id }} menjadi batal.</li>
                                    <li>Apabila terjadi perubahan nama atau balik nama / pindah kavling yang telah dipesan maka pembeli dikenakan biaya administrasi sebesar Rp 500.000,- (sebelum akad kredit)</li>
                                    <li>
                                        Apabila terjadi pembatalan sepihak ataupun pengunduran diri dari saya sebagai konsumen, maka saya bersedia dikenakan denda administrasi 10% dari uang yang sudah masuk dan Booking Fee hangus.
                                        Tetapi apabila terjadi penolakan dari pihak Bank maka uang kembali 100% dan Booking Fee hangus.
                                    </li>
                               </ul>
                            </li>
                        </ol>

                        <p style="margin-top: -10px">Demikian surat pernyataan ini dibuat, untuk melengkapi persyaratan KPR yang akan kami tandatangani.</p>

                    </div>
                </div>

                <div class="row" style="margin-top: -10px">
                    <div class="col-md-6 col-print-6 text-center"></div>
                    <div class="col-md-6 col-print-6 text-center">Purwakarta, {{ GeneralHelper::konversiTgl($sales->sales_date, 'ttd') }}</div>

                    <div class="col-md-6 col-print-6 text-center mb-4">Yang Menyatakan / Pembeli,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Penjual,</div>

                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ $sales->customer->name }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">(PT. LAN SENA JAYA)</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
