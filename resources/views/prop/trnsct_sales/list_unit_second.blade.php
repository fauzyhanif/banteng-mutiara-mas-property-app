@extends('index')

@section('content')
<section class="content-header">
    <h1>List Unit</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Daftar Unit
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th>Lokasi</th>
                                <th>Blok</th>
                                <th>Nomor Unit</th>
                                <th>Tipe</th>
                                <th class="text-right">Harga Cash</th>
                                <th class="text-right">Harga KPR</th>
                                <th>Status</th>
                                <th width="15%">Pilih</th>
                            </thead>
                            <tbody>
                                @foreach ($units as $unit)
                                <tr>
                                    <td>{{ $unit->location->name }}</td>
                                    <td>{{ $unit->block->name }}</td>
                                    <td><b>{{ $unit->number }}</b></td>
                                    <td>{{ $unit->unitType->name }}</td>
                                    <td class="text-right">
                                        {{ GeneralHelper::rupiah($unit->harga_jual_cash) }}
                                    </td>
                                    <td class="text-right">
                                        {{ GeneralHelper::rupiah($unit->harga_jual_kpr) }}
                                    </td>
                                    <td>
                                        @if ($unit->status == 'READY')
                                        <span class="text-green">{{ $unit->status }}</span>
                                        @elseif ($unit->status == 'BOOKED')
                                        <span class="text-orange">{{ $unit->status }}</span>
                                        @else
                                        <span class="text-red">{{ $unit->status }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($unit->status == 'READY')
                                        <form action="{{ route('prop.sales.form_store') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="location_id" value="{{ $unit->location_id }}">
                                            <input type="hidden" name="block_id" value="{{ $unit->block_id }}">
                                            <input type="hidden" name="number" value="{{ $unit->number }}">
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                PILIH
                                                <i class="fas fa-long-arrow-alt-right"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
