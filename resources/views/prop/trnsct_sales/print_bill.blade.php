@php
    $tagihan_total = 0;
    if ($result->sales->sales_type == 'CASH') {
        $tagihan_total = $result->sales->ttl_trnsct;
    } else {
        if ($result->sales->kpr_credit_acc == '0') {
            $tagihan_total = $result->sales->ttl_trnsct - $result->sales->kpr_credit_pengajuan;
        } else {
            $tagihan_total = $result->sales->ttl_trnsct - $result->sales->kpr_credit_acc;
        }
    }
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Kekurangan Bayar Uang Muka</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold">SURAT KEKURANGAN BAYAR UANG MUKA</p>

                        <p>
                            Saya yang bertanda tangan dibawah ini:

                            <table class="table table-sm table-borderless ml-2 mt-0" style="margin-top: -10px !important">
                                <tbody>
                                    <tr>
                                        <td width="25%">Nama</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>{{ $result->sales->customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Tempat/Tanggal Lahir</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>{{ $result->sales->customer->place_of_birth }}, {{ $result->sales->customer->date_ofbirth }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Pekerjaan</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>{{ $result->sales->customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Alamat</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>{{ $result->sales->customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </p>

                        <p>
                            Dengan ini menyatakan bahwa saya telah melakukan pembayaran uang muka sebesar Rp {{ GeneralHelper::rupiah($result->paid_off) }} ({{ GeneralHelper::terbilang($result->paid_off) }}) <br>
                            dan masih memiliki kekurangan bayar uang muka sebesar Rp. {{ GeneralHelper::rupiah($result->reminder) }} ({{ GeneralHelper::terbilang($result->reminder) }}) 
                            untuk pembelian 1 (unit) rumah subsidi <b>Di Benteng Mutiara mas</b>.

                            <table class="table table-sm table-borderless ml-2 mt-0" style="margin-top: -10px !important">
                                <tbody>
                                    <tr>
                                        <td width="25%">Unit</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>Tipe {{ $result->sales->unit->unitType->name }} No {{ $result->sales->unit_id }} {{ $result->sales->block->name }} {{ $result->sales->location->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Harga jual rumah</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>Rp {{ GeneralHelper::rupiah($result->sales->ttl_trnsct) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Besaran uang muka</td>
                                        <td class="text-right" width="5%">:</td>
                                        <td>Rp {{ GeneralHelper::rupiah($tagihan_total) }}</td>
                                    </tr>
                                    @if ($result->sales->sales_type == 'KPR')                                    
                                        <tr>
                                            <td width="25%">Bank Pelaksana</td>
                                            <td class="text-right" width="5%">:</td>
                                            <td>{{ $result->sales->bank->name }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </p>

                        <p>
                            @if ($result->sales->sales_type == 'KPR') 
                                Jika permohonan KPR rumah subsidi saya di setujui,  maka
                            @endif
                            saya bersedia untuk membayar kekurangan uang muka selambat-lambat nya pada tanggal : {{ GeneralHelper::konversiTgl($result->due_date, 'ttd') }} <br>
                            Demikian kami sampaikan, atas perhatian nya kami ucapkan terima kasih.
                        </p>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-md-6 col-print-6 text-center mb-4"></div>
                    <div class="col-md-6 col-print-6 text-center mb-4">........................, {{ GeneralHelper::konversiTgl($result->created_at, 'ttd') }}</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Menyetujui,</div>
                    <div class="col-md-6 col-print-6 text-center mb-4">Pemohon,</div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-md-6 col-print-6 text-center mt-2">({{ Session::get('auth_nama') }})</div>
                    <div class="col-md-6 col-print-6 text-center mt-2">{{ $result->sales->customer->name }}</div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
