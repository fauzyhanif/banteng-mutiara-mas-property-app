<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Kekurangan Dokumen Persyaratan KPR</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

    @include('Component.css_print')
</head>
<body>
    <body class="A4" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                @include('Component.kop_surat')
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center font-weight-bold">SURAT PERINGATAN KE-2</p>

                        <p>
                            Nomor : {{ sprintf('%04d', $sales->sppr_number) }}/BMM/SPPR/{{ substr($sales->sales_date, 5,2) }}/{{ substr($sales->sales_date, 0,4) }} <br>
                            Perihal : Surat Peringatan 2 (dua) <br>
                            Kepada Yth. <br>
                            {{ $sales->customer->name }}
                        </p>         

                        <p>
                            Sehubungan dengan Surat Peringatan Pertama (SP 1) yang sebelumnya disampaikan kepada {{ $sales->customer->name }}, Developer kemudian
                            memutuskan untuk menindaklanjuti melalui Surat Peringatan Kedua (SP 2). Hal ini disadari saudara yang tidak menunjukkan  sikap positif
                            dan inisiatif sebagai respon atas SP 1 sebelumnya,
                        </p>

                        <p>
                            Oleh karena itu, Surat Peringatan Kedua ini diberikan dengan tujuan sebagai teguran kepada {{ $sales->customer->name }}
                            agar dapat memenuhi kekurangan berkas selambat-lambatnya <b>Tanggal {{ GeneralHelper::konversiTgl($sales->kpr_document_collection_limit_2, 'T') }}</b>.
                        </p>

                        <p>
                            Bila berkas kekurangan belum kami terima dengan tanggal yang ditetapkan diatas, maka kami selaku developer akan mengirim surat 
                            pembatalan KPR di Perum Benteng Mutiara Mas.
                        </p>

                        <p>
                            Demikian Surat Peringatan Kedua ini kami sampaikan dengan sebenar-benarnya,
                        </p>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-12">
                        Mengetahui, <br>
                        PT LAN SENA JAYA

                        <br><br><br><br><br>
                        <b>Alan Suherlan</b> <br>
                        (Direktur Utama)
                    </div>

                </div>
            </div>

        </section>
    </body>
</body>
</html>
