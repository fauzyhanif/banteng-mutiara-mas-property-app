<div class="card shadow-none">
    <div class="card-header">
        <h3 class="card-title">
            Daftar Unit
        </h3>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <th>Nomor Unit</th>
                <th class="text-right">Harga Cash</th>
                <th class="text-right">Harga KPR</th>
                <th>Status</th>
                <th width="35%">Pilih</th>
            </thead>
            <tbody>
                @foreach ($units as $unit)
                    <tr>
                        <td>
                            <b>{{ $unit->number }}</b> <br>
                            <span class="font-weight-light">Type {{ $unit->unitType->name }}</span>
                        </td>
                        <td class="text-right">
                            {{ GeneralHelper::rupiah($unit->harga_jual_cash) }}
                        </td>
                        <td class="text-right">
                            {{ GeneralHelper::rupiah($unit->harga_jual_kpr) }}
                        </td>
                        <td>
                            @if ($unit->status == 'READY')
                            <span class="text-green">{{ $unit->status }}</span>
                            @elseif ($unit->status == 'BOOKED')
                            <span class="text-orange">{{ $unit->status }}</span>
                            @else
                            <span class="text-red">{{ $unit->status }}</span>
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('prop.sales.switch_unit') }}" method="POST">
                                @csrf
                                @if ($unit->status == 'READY')
                                    <input type="hidden" name="sales_id" value="{{ $sales_id }}">
                                    <input type="hidden" name="location_id" value="{{ $unit->location_id }}">
                                    <input type="hidden" name="block_id" value="{{ $unit->block_id }}">
                                    <input type="hidden" name="unit_id" value="{{ $unit->number }}">

                                    <div class="input-group input-group-sm">
                                        <input type="text" name="reason_to_move" class="form-control form-control-sm" placeholder="Sebutkan alasan pindah">
                                        <span class="input-group-append">
                                            <button type="submit" class="btn btn-info btn-flat"> <i class="fas fa-long-arrow-alt-right"></i>&nbsp; PILIH</button>
                                        </span>
                                    </div>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
