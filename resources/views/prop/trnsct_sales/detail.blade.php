@extends('index')

@section('content')

{{-- hidden data untuk get sejarah cicilan dll --}}
<input type="hidden" name="sales_id" value="{{ $sales->sales_id }}">

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.sales') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Penjualan
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Penjualan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Informasi Penjualan
                        </h3>

                        <div class="card-tools">
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-wrench"></i> Kumpulan Aksi
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                    <a href="{{ route('prop.print_kpr_requirements', $sales->customer_id) }}" target="_blank" class="dropdown-item">
                                        <i class="fas fa-print"></i> &nbsp;Cetak Persyaratan
                                    </a>
                                    <a href="{{ route('prop.sales.print_sppr', $sales->id) }}" target="_blank" class="dropdown-item">
                                        <i class="fas fa-print"></i> &nbsp;Cetak SPPR
                                    </a>

                                    @if ($sales->sales_type == 'CASH' && $sales->ttl_trnsct_paid > 0)
                                        <button class="dropdown-item" data-toggle="modal" data-target="#form-print-key-handover">
                                            <i class="fas fa-print"></i> &nbsp;Cetak Serah Terima Kunci
                                        </button>
                                        <button class="dropdown-item" data-toggle="modal" data-target="#form-print-complain">
                                            <i class="fas fa-print"></i> &nbsp;Cetak Surat Komplen
                                        </button>
                                    @endif

                                    @if ($sales->sales_type == 'KPR')
                                        @php
                                            $uang_muka_kpr = ($sales->ttl_trnsct - $sales->kpr_credit_acc) - ($sales->ttl_trnsct_paid - $sales->kpr_credit_acc_paid);
                                        @endphp
                                        @if ($uang_muka_kpr == 0 && $sales->kpr_status == 'ACCEPTED')
                                            <button class="dropdown-item" data-toggle="modal" data-target="#form-print-key-handover">
                                                <i class="fas fa-print"></i> &nbsp;Cetak Serah Terima Kunci
                                            </button>
                                            <button class="dropdown-item" data-toggle="modal" data-target="#form-print-complain">
                                                <i class="fas fa-print"></i> &nbsp;Cetak Surat Komplen
                                            </button>
                                        @endif
                                        <button class="dropdown-item" data-toggle="modal" data-target="#form-print-missing-requirement-document">
                                            <i class="fas fa-print"></i> &nbsp;Cetak Kekurangan Dokumen Persyaratan KPR
                                        </button>

                                        <button class="dropdown-item" data-toggle="modal" data-target="#form-print-warning-letter-2">
                                            <i class="fas fa-print"></i> &nbsp;Cetak Surat Peringatan 2
                                        </button>

                                        <a href="{{ url('prop/sales/print_cancellation_letter', $sales->id) }}" target="_blank" class="dropdown-item">
                                            <i class="fas fa-print"></i> &nbsp;Cetak Surat Pembatalan
                                        </a>
                                    @endif

                                    <a class="dropdown-divider"></a>
                                    <button class="dropdown-item" data-toggle="modal" data-target="#form-switch-unit">
                                        <i class="fas fa-edit"></i> &nbsp;Pindah Unit
                                    </button>
                                    <button class="dropdown-item" data-toggle="modal" data-target="#form-update-marketer">
                                        <i class="fas fa-edit"></i> &nbsp;Update Marketer
                                    </button>
                                    <button class="dropdown-item" data-toggle="modal" data-target="#form-update-additional-cost">
                                        <i class="fas fa-edit"></i> &nbsp;Update Biaya Tambahan
                                    </button>
                                    <button class="dropdown-item" data-toggle="modal" data-target="#form-update-customer-data">
                                        <i class="fas fa-edit"></i> &nbsp;Update Data Konsumen
                                    </button>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="bg-info font-weight-bold">Konsumen</td>
                                </tr>
                                <tr>
                                    <td width="25%">Nama</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">No Handphone</td>
                                    <td width="75%" class="font-weight-bold">
                                        @php $customer_phone_num = $sales->customer->phone_num; @endphp
                                        @if (substr($sales->customer->phone_num,0,1) == '0')
                                            @php
                                                $customer_phone_num = substr_replace($sales->customer->phone_num, '+62', 0, 1);
                                            @endphp
                                        @endif
                                        : {{ $sales->customer->phone_num }} &nbsp; <a class="btn btn-light btn-sm text-success" href="https://wa.me/{{ $customer_phone_num }}" target="_blank" class="text-success"><i class="fab fa-whatsapp"></i> Hubungi</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">Email</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->customer->email }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Domisili</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->customer->address_domicile }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">NPWP</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->customer->npwp }}</td>
                                </tr>
                                @if ($sales->sales_type == 'KPR')
                                    <tr>
                                        <td width="25%">Bank (Proses KPR)</td>
                                        <td width="75%" class="font-weight-bold">: {{ $sales->customer->rek_bank_new }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">No Rekening (Proses KPR)</td>
                                        <td width="75%" class="font-weight-bold">: {{ $sales->customer->rek_number_new }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td width="25%">Scan KTP</td>
                                    <td width="75%"> : @if ($sales->customer->file_ktp != '')<a href="{{ url('public/customer_document', $sales->customer->file_ktp) }}" target="_blank" rel="noopener noreferrer">Lihat Dokumen</a>@else - @endif</td>
                                </tr>
                                <tr>
                                    <td width="25%">Scan KK</td>
                                    <td width="75%"> : @if ($sales->customer->file_kk != '')<a href="{{ url('public/customer_document', $sales->customer->file_kk) }}" target="_blank" rel="noopener noreferrer">Lihat Dokumen</a>@else - @endif</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="button" class="btn btn-light btn-sm text-primary" data-toggle="modal" data-target="#form-upload-document-ktp-kk"><i class="fas fa-upload"></i> Upload Dokumen Ktp & Kk</button>
                                        <button type="button" class="btn btn-light btn-sm text-primary" data-toggle="modal" data-target="#detail-customer"><i class="fas fa-eye"></i> Detail Konsumen</button>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" class="bg-info font-weight-bold">Unit</td>
                                </tr>
                                <tr>
                                    <td width="25%">Jenis Rumah</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->unit->jenis_rumah }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Lokasi</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->location->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Blok</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->block->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">No Unit</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->unit_id }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Tipe</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->unit->unitType->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">NOP</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->unit->nop }}</td>
                                </tr>
                                @if ($sales->reason_to_move != null)
                                    <tr>
                                        <td width="25%">Alasan Pindah</td>
                                        <td width="75%" class="font-weight-bold">: {{ $sales->reason_to_move }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td colspan="2" class="bg-info font-weight-bold">Penjualan</td>
                                </tr>
                                <tr>
                                    <td width="25%">No Penjualan</td>
                                    <td width="75%" class="font-weight-bold">: {{ $sales->sales_id }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Tgl Penjualan</td>
                                    <td class="font-weight-bold" width="75%">: {{ GeneralHelper::konversiTgl($sales->sales_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Jenis Penjualan</td>
                                    <td width="75%" class="font-weight-bold">
                                        :
                                        {{ $sales->sales_type }}
                                        @if ($sales->sales_type == 'KPR')
                                            @if ($sales->kpr_status == 'WAITING')
                                                ({{ $sales->kpr_status }})
                                            @elseif ($sales->kpr_status == 'ACCEPTED')
                                                (<span class="text-green">{{ $sales->kpr_status }}</span>)
                                            @elseif ($sales->kpr_status == 'REJECTED')
                                                (<span class="text-red">{{ $sales->kpr_status }}</span>)
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @if ($sales->sales_type == 'KPR')
                                <tr>
                                    <td width="25%">Bank KPR</td>
                                    <td class="font-weight-bold" width="75%">: {{ $sales->bank->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Kredit Pengajuan</td>
                                    <td class="font-weight-bold" width="75%">: Rp {{ GeneralHelper::rupiah($sales->kpr_credit_pengajuan) }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <td width="25%">Marketer</td>
                                    <td width="75%" class="font-weight-bold">: @if ($sales->affiliate_num != '' && $sales->affiliate_num != '0') {{ $sales->marketer->sdm->name }} ({{ $sales->marketer->affiliate_num }}) @endif</td>
                                </tr>
                                <tr>
                                    <td width="25%">Fee Marketer</td>
                                    <td width="75%" class="font-weight-bold">
                                        : Rp {{ GeneralHelper::rupiah($sales->affiliate_fee) }}
                                        @if (Session::get('auth_role_aktif') == '2')
                                            &nbsp; <a href="javascript:void(0)" class="btn btn-sm btn-light text-primary" data-toggle="modal" data-target="#form-update-affiliate-fee"><i class="fas fa-edit"></i> Ubah</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">Harga Jual Awal</td>
                                    <td class="font-weight-bold" width="75%">: Rp {{ GeneralHelper::rupiah($sales->basic_price) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Potongan</td>
                                    <td class="font-weight-bold" width="75%">: Rp {{ GeneralHelper::rupiah($sales->discount) }} @if ($sales->discount_desc != '') ({{ $sales->discount_desc }}) @endif()</td>
                                </tr>
                                <tr>
                                    <td width="25%">Biaya Tambahan</td>
                                    <td class="font-weight-bold" width="75%">: Rp {{ GeneralHelper::rupiah($sales->additional_cost) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Biaya Tambahan Ket.</td>
                                    <td class="font-weight-bold" width="75%">: {{ $sales->additional_cost_note }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Harga Jual Final</td>
                                    <td class="font-weight-bold" width="75%">: Rp {{ GeneralHelper::rupiah($sales->ttl_trnsct) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Uang Masuk</td>
                                    <td class="font-weight-bold" width="75%">: Rp {{ GeneralHelper::rupiah($sales->ttl_trnsct_paid) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Sisa Tagihan</td>
                                    <td class="font-weight-bold" width="75%">
                                        : Rp
                                        @if ($sales->sales_type == 'CASH')
                                            {{ GeneralHelper::rupiah($sales->ttl_trnsct - $sales->ttl_trnsct_paid) }}
                                        @else
                                            {{ GeneralHelper::rupiah(($sales->ttl_trnsct - $sales->kpr_credit_acc) - ($sales->ttl_trnsct_paid - $sales->kpr_credit_acc_paid)) }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">Komitmen Pembayaran</td>
                                    <td class="font-weight-bold" width="75%">: {{ $sales->payment_commitment }}</td>
                                </tr>
                                <tr>
                                    <td width="25%">Harga Jual <br><span class="text-info">(untuk laporan penjualan - PAJAK)</span></td>
                                    <td class="font-weight-bold" width="75%">
                                        : {{ GeneralHelper::rupiah($sales->ttl_trnsct_for_pajak) }}
                                        &nbsp; <a href="javascript:void(0)" class="btn btn-sm btn-light text-primary" data-toggle="modal" data-target="#form-change-price-for-pajak"><i class="fas fa-edit"></i> Ubah Harga</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <!-- Nav pills -->
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link active"
                                    data-toggle="pill"
                                    href="javascript:void(0)"
                                    onclick="detailInstallment('{{ $sales->sales_id }}')">
                                    Angsuran Konsumen
                                </a>
                            </li>
                            @if ($sales->sales_type == 'KPR')
                                <li class="nav-item">
                                    <a
                                        class="nav-link"
                                        data-toggle="pill"
                                        href="javascript:void(0)"
                                        onclick="detailKpr('{{ $sales->sales_id }}')">
                                        Info KPR
                                    </a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a
                                    class="nav-link"
                                    data-toggle="pill"
                                    href="javascript:void(0)"
                                    onclick="detailSalesStep('{{ $sales->sales_id }}', '{{ $sales->location_id }}', '{{ $sales->block_id }}', '{{ $sales->unit_id }}', '{{ $sales->sales_type }}')">
                                    Step Penjualan
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    class="nav-link"
                                    data-toggle="pill"
                                    href="javascript:void(0)"
                                    onclick="detailCertificateStep('{{ $sales->sales_id }}', '{{ $sales->location_id }}', '{{ $sales->block_id }}', '{{ $sales->unit_id }}', '{{ $sales->sales_type }}')">
                                    Step Sertifikat
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    class="nav-link"
                                    data-toggle="pill"
                                    href="javascript:void(0)"
                                    onclick="detailCertificatePosition('{{ $sales->sales_id }}', '{{ $sales->location_id }}', '{{ $sales->block_id }}', '{{ $sales->unit_id }}', '{{ $sales->sales_type }}')">
                                    Posisi Sertifikat
                                </a>
                            </li>
                            <li class="nav-item">
                                <a
                                    class="nav-link"
                                    data-toggle="pill"
                                    href="javascript:void(0)"
                                    onclick="detailMarketingFee('{{ $sales->id }}', '{{ $sales->sales_id }}', '{{ $sales->location_id }}', '{{ $sales->block_id }}', '{{ $sales->unit_id }}', '{{ $sales->affiliate_num }}', '{{ $sales->affiliate_fee }}', '{{ $sales->affiliate_fee_paid }}')">
                                    Pencairan Marketing Fee
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="card" id="view-detail-component">

                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.trnsct_sales.form_switch_unit');
@include('prop.trnsct_sales.form_update_marketer');
@include('prop.trnsct_sales.form_change_price_for_pajak');
@include('prop.trnsct_sales.form_update_additional_cost');
@include('prop.trnsct_sales.form_update_customer_data');
@include('prop.trnsct_sales.form_update_affiliate_fee');
@include('prop.trnsct_sales.form_print_key_handover');
@include('prop.trnsct_sales.form_print_complain');
@include('prop.trnsct_sales.form_print_missing_requirement_document');
@include('prop.trnsct_sales.form_print_warning_letter_2');
@include('prop.trnsct_sales.form_upload_document_ktp_kk');
@include('prop.trnsct_sales.detail_customer');

<script>
    $(document).ready(function(){
        var sales_id = $('input[name="sales_id"]').val();

        // ketika akses halaman detail
        detailInstallment(sales_id);
    });
</script>

@include('prop.trnsct_sales.detail_js')

@endsection
