<!-- The Modal -->
<div class="modal" id="modal-delete-sales">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="delete_sales" action="{{ url('prop/sales/delete_sales') }}" method="POST">
            <!-- Modal Header -->

                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus Transaksi Penjualan Unit</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                {{-- id sales --}}
                <input type="hidden" name="delete_sales_id" value="0">

                <!-- Modal body -->
                <div class="modal-body">
                    Jika anda menghapus transaksi ini maka semua data hutang piutang &
                    jurnal keuangan yg bersangkutan dengan transaksi ini akan terhapus
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus Transaksi Ini</button>
            </div>
            </form>

        </div>
    </div>
</div>
