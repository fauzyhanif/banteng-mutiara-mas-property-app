<!-- The Modal -->
<div class="modal" id="form-update-affiliate-fee">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Update Fee Marketer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="update-affiliate-fee" action="{{ route('prop.sales.update-affiliate-fee') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $sales->id }}">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Fee Marketer</label>
                                <input type="text" name="affiliate_fee" value="{{ $sales->affiliate_fee }}" class="form-control money">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>