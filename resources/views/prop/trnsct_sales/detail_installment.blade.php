@php
    $tagihan_total = 0;
    $tagihan_dibayar = 0;
    $tagihan_sisa = 0;
    if ($sale->sales_type == 'CASH') {
        $tagihan_total = $sale->ttl_trnsct;
        $tagihan_dibayar = $sale->ttl_trnsct_paid;
        $tagihan_sisa = $sale->ttl_trnsct - $sale->ttl_trnsct_paid;
    } else {
        if ($sale->kpr_credit_acc == '0') {
            $tagihan_total = $sale->ttl_trnsct - $sale->kpr_credit_pengajuan;
            $tagihan_dibayar = $sale->ttl_trnsct_paid;
            $tagihan_sisa = ($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->ttl_trnsct_paid;
        } else {
            $tagihan_total = $sale->ttl_trnsct - $sale->kpr_credit_acc;
            $tagihan_dibayar = $sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid;
            $tagihan_sisa = ($sale->ttl_trnsct - $sale->kpr_credit_acc) - ($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid);
        }
    }
@endphp

<div class="card-header">
    <h3 class="card-title">Angsuran Konsumen</h3>
    <div class="card-tools">
        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#detail-installment-form-potongan">
            <i class="fas fa-plus-circle"></i> &nbsp; Input Potongan
        </button>

        @if ($sale->sales_type == 'CASH')
            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#detail-installment-form-biaya-tambahan">
                <i class="fas fa-plus-circle"></i> &nbsp; Input Biaya Tambahan
            </button>

            @if ($sale->ttl_trnsct - $sale->ttl_trnsct_paid > 0)
                <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#detail-installment-form-store">
                    <i class="fas fa-plus-circle"></i> &nbsp; Input Angsuran Baru
                </button>
            @endif
        @else
            @if (($sale->ttl_trnsct - $sale->kpr_credit_acc) - ($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid) > 0)
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                    data-target="#detail-installment-form-store">
                    <i class="fas fa-plus-circle"></i> &nbsp; Input Angsuran Baru
                </button>
            @endif
        @endif
    </div>
</div>

<div class="card-body">
    <table class="table table-bordered striped">
        <thead class="bg-info">
            <th width="30%">Total Tagihan</th>
            <th width="30%">Sudah Dibayar</th>
            <th width="30%">Sisa</th>
        </thead>
        <tbody>
            @if ($sale->sales_type == 'CASH')
                <tr>
                    <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct) }}</td>
                    <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct_paid) }}</td>
                    <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->ttl_trnsct_paid) }}</td>
                </tr>
            @else
                @if ($sale->kpr_credit_acc == '0')
                    <tr>
                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) }}</td>
                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct_paid) }}</td>
                        <td>{{ GeneralHelper::rupiah(($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->ttl_trnsct_paid) }}</td>
                    </tr>
                @else
                    <tr>
                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct - $sale->kpr_credit_acc) }}</td>
                        <td>{{ GeneralHelper::rupiah($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid) }}</td>
                        <td>{{ GeneralHelper::rupiah(($sale->ttl_trnsct - $sale->kpr_credit_acc) - ($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid)) }}</td>
                    </tr>
                @endif
            @endif
        </tbody>
    </table>

    <br>
    <br>

    <h5>Daftar Pembayaran</h5>
    <table class="table table-bordered striped">
        <thead class="bg-info">
            <th width='5%'>No</th>
            <th>Tanggal</th>
            <th>No Kwitansi & Deskripsi</th>
            <th>Nominal</th>
            <th width='15%'>Cetak</th>
        </thead>
        <tbody>
            @php $no = 1 @endphp
            @php $total = 0 @endphp
            @foreach ($payments as $payment)
            <tr>
                <td>{{ $no }}.</td>
                <td>{{ GeneralHelper::konversiTgl($payment->sales_paid_date, 'slash') }}</td>
                <td> <span class="text-secondary">{{ $payment->sales_paid_num }}</span> <br> {{ $payment->sales_paid_desc }} </td>
                <td>{{ GeneralHelper::rupiah($payment->sales_paid_amount) }}</td>
                <td>
                    <a href="{{ route('prop.sales.print_invoice', $payment->sales_paid_id) }}" target="_blank"
                        class="btn btn-primary btn-xs">
                        <i class="fas fa-print"></i>
                    </a>
                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#installment-modal-delete-{{ $payment->sales_paid_id }}">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#installment-modal-update-{{ $payment->sales_paid_id }}">
                        <i class="fas fa-edit"></i>
                    </button>

                    <div class="modal" id="installment-modal-delete-{{ $payment->sales_paid_id }}" class="installment-modal-delete">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <form name="installment_delete" action="{{ route('prop.sales.detail_installment_delete') }}" method="POST">
                                    <input type="hidden" name="sales_paid_id" value="{{ $payment->sales_paid_id }}">
                                    <input type="hidden" name="sales_id" value="{{ $sales_id }}">
                                    <input type="hidden" name="amount_delete" value="{{ $payment->sales_paid_amount }}">
                                    <input type="hidden" name="ttl_trnsct_paid" value="{{ $sale->ttl_trnsct_paid }}">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Konfirmasi Pembatalan Pembayaran</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Yakin ingin membatalkan transaksi pembayaran ini?
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Batalkan Pembayaran</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="modal" id="installment-modal-update-{{ $payment->sales_paid_id }}" class="installment-modal-update">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <form name="installment_update" action="{{ route('prop.sales.detail_installment_update') }}" method="POST">
                                    <input type="hidden" name="sales_paid_id" value="{{ $payment->sales_paid_id }}">
                                    <input type="hidden" name="sales_id" value="{{ $sales_id }}">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Ubah Deskripsi Pembayaran</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="">Deskripsi Pembayaram</label>
                                            <textarea name="sales_paid_desc" class="form-control">{{ $payment->sales_paid_desc }}</textarea>
                                        </div>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Ubah Deskripsi</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            @php $no += 1 @endphp
            @php $total += $payment->sales_paid_amount @endphp
            @endforeach
        </tbody>
        <tfoot>
            <th colspan="3" class="text-center">Total</th>
            <th>Rp {{ GeneralHelper::rupiah($total) }}</th>
            <th></th>
        </tfoot>
    </table>

    <br>
    <br>

    <div class="row">
        <div class="col-lg-6">
            <h5>Daftar Surat Tagihan</h5>
        </div>
        <div class="col-lg-6 text-right">
            @if ($tagihan_sisa > 0)
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#detail-installment-form-bill">
                    <i class="fas fa-edit"></i> Buat Surat Tagihan
                </button>
            @endif
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover">
                <thead class="bg-info">
                    <th width="25%">Tgl Tagihan</th>
                    <th width="25%">Jatuh Tempo</th>
                    <th width="30%">Kekurangan</th>
                    <th>Cetak</th>
                </thead>
                <tbody>
                    @foreach ($bills as $bill)
                        <tr>
                            <td>{{ GeneralHelper::konversiTgl($bill->created_at, 'ttd') }}</td>
                            <td>{{ GeneralHelper::konversiTgl($bill->due_date, 'ttd') }}</td>
                            <td>{{ GeneralHelper::rupiah($bill->reminder) }}</td>
                            <td>
                                <a href="{{ url('prop/sales/print_bill', $bill->id) }}" target="_blank" class="btn btn-sm btn-warning">
                                    <i class="fas fa-print"></i> Cetak
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

@include('prop.trnsct_sales.detail_installment_form_store')
@include('prop.trnsct_sales.detail_installment_form_biaya_tambahan')
@include('prop.trnsct_sales.detail_installment_form_potongan')
@include('prop.trnsct_sales.detail_installment_form_bill')
@include('prop.trnsct_sales.detail_js')
