<!-- The Modal -->
<div class="modal" id="detail-marketing-fee-form-store">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="marketing_fee" action="{{ route('prop.sales.detail_marketing_fee_store') }}" method="POST">

                @csrf

                <input type="hidden" name="id" value="{{ $id }}">
                <input type="hidden" name="sales_id" value="{{ $sales_id }}">
                <input type="hidden" name="location_id" value="{{ $location_id }}">
                <input type="hidden" name="block_id" value="{{ $block_id }}">
                <input type="hidden" name="unit_id" value="{{ $unit_id }}">
                <input type="hidden" name="affiliate_num" value="{{ $affiliate_num }}">
                <input type="hidden" name="affiliate_fee" value="{{ $affiliate_fee }}">
                <input type="hidden" name="affiliate_fee_paid" value="{{ $affiliate_fee_paid }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Input Cicilan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="date" class="form-control" value="{{ date('Y-m-d') }}" required>
                    </div>

                    <div class="form-group">
                        <label>Uang diambil dari <span class="text-red">*</span></label>
                        <select name="cash_id" class="form-control" required>
                            <option value="">** Pilih Rekening</option>
                            @foreach ($cashes as $cash)
                            <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Sebesar <span class="text-red">*</span></label>
                        <input type="text" name="amount" class="form-control money" value="0">
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="desc" class="form-control">Pencairan marketing fee</textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});
</script>
