<!-- The Modal -->
<div class="modal" id="form-change-price-for-pajak">
    <div class="modal-dialog">
        <div class="modal-content">

            <form name="" action="{{ route('prop.sales.change_price_for_pajak') }}" method="POST">
                @csrf
                <input type="hidden" name="sales_id" value="{{ $sales->id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Ubah Data</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Harga Jual (untuk laporan penjualan - PAJAK) <span class="text-red">*</span></label>
                        <input type="text" name="ttl_trnsct_for_pajak" class="form-control money" value="{{ $sales->ttl_trnsct_for_pajak }}">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    function showBlocks(location_id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+location_id, 'select[name="block_id"]');
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="show_units"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#view-list-units').html(res)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();
</script>
