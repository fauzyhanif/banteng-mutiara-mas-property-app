<div class="card-header">
    <h3 class="card-title">Pencairan Marketing Fee</h3>
    @if ($affiliate_fee - $affiliate_fee_paid > 0)
        <div class="card-tools">
            <a href="{{ url('prop/marketer/print_hak_detail', $id) }}" class="btn btn-warning btn-sm" target="_blank">
                <i class="fas fa-print"></i> Cetak Form Referensi
            </a>
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#detail-marketing-fee-form-store">
                <i class="fas fa-plus-circle"></i> &nbsp; Input Pencairan Baru
            </button>
        </div>
    @endif
</div>

<div class="card-body">
    <table class="table table-bordered table-stripped">
        <thead class="bg-info">
            <th width="30%">Marketing Fee</th>
            <th width="30%">Sudah Cair</th>
            <th width="30%">Sisa</th>
        </thead>
        <tbody>
            <tr>
                <td>{{ GeneralHelper::rupiah($affiliate_fee) }}</td>
                <td>{{ GeneralHelper::rupiah($affiliate_fee_paid) }}</td>
                <td>{{ GeneralHelper::rupiah($affiliate_fee - $affiliate_fee_paid) }}</td>
            </tr>
        </tbody>
    </table>

    <br>
    <br>

    <table class="table table-bordered table-stripped table-sm">
        <thead class="bg-info">
            <th width='5%'>No</th>
            <th>Tanggal</th>
            <th>Nominal</th>
            <th width='15%'>Cetak</th>
        </thead>
        <tbody>
            @php $no = 1 @endphp
            @foreach ($payments as $payment)
            <tr>
                <td>{{ $no }}.</td>
                <td>{{ GeneralHelper::konversiTgl($payment->finance_date, 'slash') }}</td>
                <td>{{ GeneralHelper::rupiah($payment->finance_amount) }}</td>
                <td>
                    <a href="{{ url('finance/cost/print_payment', $payment->finance_paid_id) }}" target="_blank"
                        class="btn btn-primary btn-sm">
                        <i class="fas fa-print"></i> &nbsp; Cetak
                    </a>
                </td>
            </tr>
            @php $no += 1 @endphp
            @endforeach
        </tbody>
    </table>
</div>

@include('prop.trnsct_sales.detail_marketing_fee_form_store')
@include('prop.trnsct_sales.detail_js')
