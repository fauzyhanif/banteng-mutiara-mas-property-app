<table class="table table-bordered table-striped datatable">
    <thead class="bg-info">
        <th>Tgl & No Penjualan</th>
        <th>Lokasi & Blok</th>
        <th>Nomor Unit</th>
        <th>Tipe</th>
        <th>Harga</th>
        <th>Konsumen</th>
        <th>Marketer</th>
        <th width="15%">Aksi</th>
    </thead>
    <tbody>
        @foreach ($datas as $item)
            <tr>
                <td>
                    {{ GeneralHelper::konversiTgl($item->sales_date, 'slash') }} <br>
                    @if ($item->sales_type == 'CASH')
                        <span class="badge badge-success">CASH</span>
                    @elseif($item->sales_type == 'KPR')
                        @if ($item->kpr_status == 'WAITING')
                            <span class="badge badge-warning">KPR ({{ $item->kpr_status }})</span>
                        @elseif ($item->kpr_status == 'ACCEPTED')
                            <span class="badge badge-info">KPR ({{ $item->kpr_status }})</span>
                        @else
                            <span class="badge badge-danger">KPR ({{ $item->kpr_status }})</span>
                        @endif
                    @endif
                </td>
                <td>{{ $item->location->name }} Blok {{ $item->block->name }}</td>
                <td>{{ $item->unit_id }}</td>
                <td>{{ $item->unit->unitType->name }}</td>
                <td>{{ number_format($item->ttl_trnsct,0,',','.') }}</td>
                <td>{{ $item->customer->name }} <br> <span class="text-muted">{{ $item->customer->phone_num }}</span> </td>
                <td>
                    @if ($item->affiliate_num != '')
                        {{ $item->marketer->sdm->name }} <br>
                        Fee : {{ number_format($item->affiliate_fee,0,',','.') }}
                    @endif
                </td>
                <td>
                    <a href="{{ url('prop/sales/detail', $item->id) }}" class="btn btn-xs btn-info">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ url('prop/sales/print_sppr', $item->id) }}" target='_blank' class='btn btn-secondary btn-xs'>
                        <i class="fas fa-print"></i>
                    </a>
                    <button type="button" class="btn btn-xs btn-danger" onclick="show_modal_delete_sales({{ $item->id }})">
                        <i class="fas fa-trash"></i>
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="row mt-4">
    <div class="col-md-8">
        {!! $datas->render() !!}
    </div>
    <div class="col-md-4">
        <p class="text-right">
            showing {{ $datas->firstItem() }} to {{ $datas->lastItem() }} of {{ $datas->total() }}
        </p>
    </div>
</div>



