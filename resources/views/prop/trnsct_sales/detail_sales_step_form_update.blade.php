<!-- The Modal -->
<div class="modal" id="detail-sales-step-form-update-{{ $step->id }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form name="sales_step" action="{{ route('prop.sales.detail_sales_step_update') }}" method="POST">

                @csrf
                <input type="hidden" name="id" value="{{ $step->id }}">
                <input type="hidden" name="sales_id" value="{{ $step->sales_id }}">
                <input type="hidden" name="location_id" value="{{ $step->location_id }}">
                <input type="hidden" name="block_id" value="{{ $step->block_id }}">
                <input type="hidden" name="unit_id" value="{{ $step->unit_id }}">
                <input type="hidden" name="sales_type" value="{{ $sales_type }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Update Progress Step Penjualan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Step <span class="text-red">*</span></label>
                        <select name="step_id" class="form-control" required>
                            <option value="">** Pilih Step</option>
                            @foreach ($ref_steps as $rstep)
                                <option value="{{ $rstep->step_id }}" {{ ($step->step_id == $rstep->step_id) ? 'selected' : '' }}>{{ $rstep->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tanggal <span class="text-red">*</span></label>
                        <input type="date" name="step_date" class="form-control" value="{{ $step->step_date }}" required>
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="step_desc" class="form-control">{{ $step->step_desc }}</textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
