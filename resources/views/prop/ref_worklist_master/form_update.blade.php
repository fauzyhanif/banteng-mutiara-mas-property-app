@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.worklist_master') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengerjaan
            </a>
        </li>
        <li class="breadcrumb-item active">Update Pengerjaan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.worklist_master.update', $data->id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('prop.ref_worklist_master.form_general')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    loadComponent('{{ url('prop/component/slct_mandor?id='.$data->sdm_id) }}', 'select[name="sdm_id"]');
});
</script>
@include('prop.ref_worklist_master.asset.js')
@endsection
