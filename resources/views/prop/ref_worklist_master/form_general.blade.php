<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Harga <span class="text-red">*</span></label>
    <input
        type="text"
        name="price"
        class="form-control money"
        required
        @isset($data)
            value="{{ $data->price }}"
        @endisset
    >
</div>

<div class="form-group">
    <label for="">Mandor <span class="text-red">*</span></label>
    <select name="sdm_id" class="form-control" required>
        <option value="">** Pilih Mandor</option>
    </select>
</div>

<div class="form-group">
    <label>Urutan <span class="text-red">*</span></label>
    <input
        type="number"
        name="orders"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->orders }}"
        @endisset
    >
</div>

