@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.subsidi_type') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Type Subsidi
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Data Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.subsidi_type.store') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="store">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Type <span class="text-red">*</span></label>
                                <select name="type" required class="form-control">
                                    <option>SUBSIDI</option>
                                    <option>NONSUBSIDI</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Nama <span class="text-red">*</span></label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div>

                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_unit_type.asset.js')
@endsection
