@extends('index')

@section('content')
<section class="content-header">
    <h1>Type Subsidi</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Type Subsidi
                        <div class="card-tools">
                            <a href="{{ route('prop.subsidi_type.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Data Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th width="20%">Type</th>
                                <th>Nama</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_subsidi_type.asset.js')
@endsection

