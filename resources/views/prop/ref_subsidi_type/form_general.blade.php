<div class="form-group">
    <label>Type <span class="text-red">*</span></label>
    <select name="type" required class="form-control">
        <option @isset($data) {{ ($data->type == 'SUBSIDI') ? 'selected' : '' }} @endisset>SUBSIDI</option>
        <option @isset($data) {{ ($data->type == 'NONSUBSIDI') ? 'selected' : '' }} @endisset>NONSUBSIDI</option>
    </select>
</div>

<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input type="text" name="name" class="form-control" required @isset($data) value="{{ $data->name }}" @endisset>
</div>


