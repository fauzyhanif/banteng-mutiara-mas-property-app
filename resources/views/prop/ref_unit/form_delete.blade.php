<!-- The Modal -->
<div class="modal" id="modal-delete-unit">
    <div class="modal-dialog">
        <div class="modal-content">

            <form name="delete_unit" action="{{ url('prop/unit/delete') }}" method="POST">
                @csrf
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                {{-- id unit --}}
                <input type="hidden" name="delete_unit_id" value="">

                <!-- Modal body -->
                <div class="modal-body">
                    Yakin ingin hapus unit <span id="unit-name" class="font-weight-bold"></span>?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-danger">Hapus Unit Ini</button>
                </div>
            </form>

        </div>
    </div>
</div>
