@extends('index')

@section('content')
<section class="content-header">
    <h1>Unit</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-lg-3">
                <select name="location_id" id="" class="form-control" onchange="showBlocks(this.value)">
                    <option value="">-- Pilih Lokasi --</option>

                </select>
            </div>

            <div class="col-lg-3">
                <select name="block_id" id="" class="form-control" onchange="showUnits(this.value)">
                    <option value="">-- Pilih Blok --</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Unit
                        <div class="card-tools">
                            <a href="{{ route('prop.unit.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Unit Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th width="15%">Nomor Unit</th>
                                <th width="10%">Status</th>
                                <th width="10%">Status Jual</th>
                                <th>Konsumen</th>
                                <th width="20%">Harga Jual</th>
                                <th width="12%">Uang Masuk</th>
                                <th width="12%">Sisa</th>
                                <th width="12%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_unit.form_delete')

<script>
$(function() {
    loadData();
    loadComponent('{{ url('prop/component/slct_location') }}', 'select[name="location_id"]');
});

function showBlocks(id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+id, 'select[name="block_id"]');
}

function showUnits(params) {
    var location_id = $('select[name="location_id"]').val();
    var base = {!! json_encode(url("/prop/unit?location_id=")) !!} + location_id + "&block_id=" + params;
    console.log(base)
    location.replace(base)
}

function loadData() {
    var base = {!! json_encode(url('/prop/unit/list_data_json?location_id='.$location_id.'&block_id='.$block_id)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'type_number', name: 'type_number', searchable: true },
            { data: 'status', name: 'status', searchable: true },
            { data: 'is_publish_html', name: 'is_publish_html', searchable: true },
            { data: 'customer', name: 'customer', searchable: true },
            { data: 'selling_price', name: 'selling_price', searchable: true },
            { data: 'ttl_trnsct_paid', name: 'ttl_trnsct_paid', searchable: true },
            { data: 'sisa', name: 'sisa', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: true },
        ]
    });
}

function show_modal_delete(unit_id, unit_number, block_name, location_name) {
    $('#modal-delete-unit').modal('show');
    $('input[name="delete_unit_id"]').val(unit_id);
    $('#unit-name').html(`${location_name} ${block_name} ${unit_number}`);
}

(function() {
    $('form[name="delete_unit"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                    $('.datatable').DataTable().ajax.reload(null, false);
                    $('#modal-delete-unit').modal('hide');
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

</script>
@endsection

