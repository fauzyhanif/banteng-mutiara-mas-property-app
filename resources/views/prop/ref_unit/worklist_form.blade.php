@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('prop/unit/detail', $unit->unit_id) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Detail Unit
            </a>
        </li>
        <li class="breadcrumb-item active">Pengerjaan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Daftar Pengerjaan
                        </h5>
                    </div>

                    <div class="card-body">
                        <form action="{{ url('prop/unit/worklist_update') }}" method="POST">
                            @csrf
                            <table class="table table-sm table-bordered table-hover">
                                <thead class="bg-info">
                                    <th class="text-center">
                                        <input type="checkbox" id="check-all-2">
                                    </th>
                                    <th>Pekerjaan & Mandor</th>
                                    <th>Budget & Progress</th>
                                    <th class="text-center">Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ($unit_worklist as $item)
                                    <tr>
                                        <td class="text-center">
                                            <input type="checkbox" name="worklist_id[{{ $item->list_id }}]" class="checkbox-update">
                                        </td>
                                        <td>
                                            {{ $item->worklist->name }}
                                            <br>
                                            (<span class="text-info">{{ $item->mandor->sdm->name }}</span>)
                                        </td>
                                        <td>{{ $item->progress }}%</td>
                                        <td class="text-center">
                                            <a href="{{ url('prop/unit/worklist_delete?list_id=' . $item->list_id) }}" class="btn btn-danger btn-xs tooltips"
                                                title="Hapus pengerjaan">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <button type="submit" class="btn btn-sm btn-success mt-3">
                                <i class="fas fa-edit"></i>
                                Jadikan Progress 100%
                            </button>
                        </form>

                        <br>

                        <p class="text-secondary mt-3">
                            Untuk update progress menjadi 100% silahkan pilih & centang item pengerjaan lalu klik tombol diatas.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Pekerjaan Mandor
                    </div>
                    <form action="{{ url('prop/unit/worklist_store', $unit_id) }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <table class="table table-bordered table-sm">
                                <thead class="bg-info">
                                    <th class="text-center" width="10%">
                                        <input type="checkbox" id="check-all-3">
                                    </th>
                                    <th>Nama Pengerjaan</th>
                                    <th>Mandor</th>
                                    <th width="15%">Harga</th>
                                    <th width="15%">Progress (%)</th>
                                </thead>
                                <tbody>
                                    @foreach ($worklists as $worklist)
                                    <tr>
                                        <td class="text-center">
                                            <input type="checkbox" name="worklist_id[{{ $worklist->id }}]"
                                                class="checkbox-peritem checkbox-store" value="{{ $worklist->id }}">
                                        </td>
                                        <td>{{ $worklist->name }}</td>
                                        <td>
                                            <select name="mandor_id[{{ $worklist->id }}]"
                                                class="form-control form-control-sm form-select-mandor">
                                                <option value="">** Pilih Mandor</option>
                                                @foreach ($mandors as $mandor)
                                                <option value="{{ $mandor->sdm_id }}" @if ($mandor->sdm_id == $worklist->sdm_id)
                                                    selected @endif>
                                                    {{ $mandor->sdm->name }} ({{ $mandor->specialist }})
                                                </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="budget[{{ $worklist->id }}]"
                                                class="form-control form-control-sm money" value="{{ $worklist->price }}">
                                        </td>
                                        <td>
                                            <input type="text" name="progress[{{ $worklist->id }}]"
                                                class="form-control form-control-sm" value="0">
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-body">
                            <p class="font-italic">
                                <span class="text-red">
                                    * Hanya pengerjaan yang diceklis yang akan tersimpan di database <br>
                                    * Pengerjaan yang diceklis pastikan mandor sudah dipilih
                                </span>
                            </p>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
</section>

@include('prop.ref_unit.asset.js')
@endsection
