@extends('index')

@section('content')
<section class="content-header">
    <h1>List Pengerjaan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Pekerjaan Mandor
                    </div>
                    <form name="form_worklist" action="{{ route('prop.unit.store_worklist', $unit_id) }}" method="POST">
                        <div class="card-body">
                            <table class="table table-bordered table-sm">
                                <thead class="bg-info">
                                    <th class="text-center" width="10%">
                                        <input type="checkbox" id="check-all">
                                    </th>
                                    <th>Nama Pengerjaan</th>
                                    <th>Mandor</th>
                                    <th width="15%">Harga</th>
                                    <th width="15%">Progress (%)</th>
                                </thead>
                                <tbody>
                                    @foreach ($worklists as $worklist)
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="worklist_id[{{ $worklist->id }}]" class="checkbox-peritem" value="{{ $worklist->id }}">
                                            </td>
                                            <td>{{ $worklist->name }}</td>
                                            <td>
                                                <select name="mandor_id[{{ $worklist->id }}]" class="form-control form-control-sm form-select-mandor">
                                                    <option value="">** Pilih Mandor</option>
                                                    @foreach ($mandors as $mandor)
                                                        <option value="{{ $mandor->sdm_id }}" @if ($mandor->sdm_id == $worklist->sdm_id) selected  @endif>
                                                            {{ $mandor->sdm->name }} ({{ $mandor->specialist }})
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="budget[{{ $worklist->id }}]" class="form-control form-control-sm money" value="{{ $worklist->price }}">
                                            </td>
                                            <td>
                                                <input type="text" name="progress[{{ $worklist->id }}]" class="form-control form-control-sm" value="0">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-body">
                            <p class="font-italic">
                                <span class="text-red">
                                    * Hanya pengerjaan yang diceklis yang akan tersimpan di database <br>
                                    * Pengerjaan yang diceklis pastikan mandor sudah dipilih
                                </span>
                            </p>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_unit.asset.js')
@endsection
