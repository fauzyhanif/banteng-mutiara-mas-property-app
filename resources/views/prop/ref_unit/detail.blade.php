@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.unit') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Unit
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Unit</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                Informasi Unit
                            </h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered striped">
                                <tbody>
                                    <tr>
                                        <td width="30%" class="bg-info">Jenis Rumah</td>
                                        <td><b>{{ $dt_info->jenis_rumah }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-warning">Nomor Unit</td>
                                        <td><b>{{ $dt_info->number }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-warning">Type Unit</td>
                                        <td><b>{{ $dt_info->unitType->name }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-warning">Lokasi</td>
                                        <td><b>{{ $dt_info->location->name }} Blok {{ $dt_info->block->name }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-info">Status</td>
                                        <td><b>{{ $dt_info->status }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-info">Tanah Hook</td>
                                        <td><b>{{ $dt_info->ground_hook }}</b></td>
                                    </tr>

                                    <tr>
                                        <td width="30%" class="bg-info">NOP</td>
                                        <td><b>{{ $dt_info->nop }}</b></td>
                                    </tr>

                                    <tr>
                                        <td width="30%" class="bg-info">Min. Uang Muka</td>
                                        <td><b>{{ GeneralHelper::rupiah($dt_info->uang_muka) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-info">Harga Jual Cash</td>
                                        <td><b>{{ GeneralHelper::rupiah($dt_info->harga_jual_cash) }}</b></td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="bg-info">Harga Jual KPR</td>
                                        <td><b>{{ GeneralHelper::rupiah($dt_info->harga_jual_kpr) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Rincian Harga</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-control bg-info">
                                <label>Cash</label>
                            </div>

                            <table class="table table-bordered table-striped">
                                <thead>
                                    <th>Item</th>
                                    <th class="text-right">Harga</th>
                                </thead>
                                <tbody>
                                    @php $harga_jual_cash = 0 @endphp
                                    @foreach ($price_item as $item)
                                        @if ($item->sales_type == 'CASH')
                                            <tr>
                                                <td>{{ $item->priceItem->name }}</td>
                                                <td class="text-right">{{ GeneralHelper::rupiah($item->price) }}</td>
                                            </tr>
                                            @php $harga_jual_cash += $item->price @endphp
                                        @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th>Total</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($harga_jual_cash) }}</th>
                                </tfoot>
                            </table>

                            <div class="form-control bg-info mt-4">
                                <label>KPR</label>
                            </div>

                            <table class="table table-bordered table-striped">
                                <thead>
                                    <th>Item</th>
                                    <th class="text-right">Harga</th>
                                </thead>
                                <tbody>
                                    @php $harga_jual_kpr = 0 @endphp
                                    @foreach ($price_item as $item)
                                        @if ($item->sales_type == 'KPR')
                                            <tr>
                                                <td>{{ $item->priceItem->name }}</td>
                                                <td class="text-right">{{ GeneralHelper::rupiah($item->price) }}</td>
                                            </tr>

                                            @php $harga_jual_kpr += $item->price @endphp
                                        @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th>Total</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($harga_jual_kpr) }}</th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-21 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                List Pengerjaan
                            </h3>

                            <div class="card-tools">
                                <a href="{{ url('prop/unit/worklist_form', $dt_info->unit_id) }}" class="btn btn-sm btn-primary">
                                    <div class="fas fa-edit"></div>
                                    Input & Update Pengerjaan
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-striped table-pengerjaan">
                                <thead class="bg-info">
                                    <th>Pekerjaan</th>
                                    <th>Mandor</th>
                                    <th>Progress</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(url('/prop/work_list/list_data_by_unit?unit_id='.$dt_info->number.'&block_id='.$dt_info->block_id.'&location_id='.$dt_info->location_id)) !!};
    $('.table-pengerjaan').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'worklists', name: 'worklists', searchable: true },
            { data: 'mandor', name: 'mandor', searchable: true },
            { data: 'progress', name: 'progress', searchable: true },
        ]
    });
}
</script>

@endsection
