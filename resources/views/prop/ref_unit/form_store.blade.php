@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.unit') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Unit
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Unit</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.unit.store') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="store">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>
                                    Lokasi Perumahan <span class="text-danger">*</span>
                                </label>
                                <select name="location_id" class="form-control" required onchange="showBlocks(this.value)">
                                    <option value="">** Pilih Lokasi</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>
                                    Blok <span class="text-danger">*</span>
                                </label>
                                <select name="block_id" class="form-control" required>
                                    <option value="">** Pilih Blok</option>
                                    @isset($blok)
                                        @foreach ($blocks as $block)
                                            <option value="{{ $block->block_id }}" @isset($data) @if ($block->block_id == $data->block->block_id)
                                                selected
                                                @endif
                                                @endisset>
                                                {{ $block->name }}
                                            </option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>

                            <div class="form-group">
                                <label>
                                    Type Unit <span class="text-danger">*</span>
                                </label>
                                <select name="unittype_id" class="form-control" required onchange="showPriceByUnitType(this.value)">
                                    <option value="">** Pilih Type Unit</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label>Nomor Rumah <span class="text-red">*</span></label>
                                <input type="text" name="number" class="form-control" required @isset($data) disabled value="{{ $data->number }}"
                                    @endisset>
                            </div>

                            <div class="form-group">
                                <label>Tanah Hook</label>
                                <input type="text" name="ground_hook" class="form-control" @isset($data) value="{{ $data->ground_hook }}" @endisset>
                            </div>

                            <div class="form-group">
                                <label>Minimal Uang Muka</label>
                                <input type="text" name="uang_muka" class="form-control money" @isset($data) value="{{ $data->uang_muka }}"
                                    @endisset>
                            </div>

                            <div class="form-group">
                                <label>Jenis Rumah <span class="text-red">*</span></label>
                                <select name="jenis_rumah"  class="form-control" required onchange="showSubsidiType()">
                                    <option>SUBSIDI</option>
                                    <option>NONSUBSIDI</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Jenis Subsidi <span class="text-red">*</span></label>
                                <select name="subsidi_type_id"  class="form-control" required>
                                    <option value="">-- Pilih Subsidi --</option>
                                </select>

                                <span class="text-info">* Untuk setting marketing fee</span>
                            </div>

                            <div class="form-group">
                                <label>NOP</label>
                                <input type="text" name="nop" class="form-control" @isset($data) value="{{ $data->nop }}" @endisset>
                            </div>

                            <div class="form-group">
                                <label>Status Siap Jual</label>
                                <select name="is_publish" class="form-control">
                                    <option value="1">Siap Jual</option>
                                    <option value="0">Belum Siap</option>
                                </select>
                            </div>

                            <div class="form-control bg-info text-center mt-4 mb-4">
                                <label>BIAYA & LAIN-LAIN</label>
                            </div>

                            <div class="row mt-4">
                                <div class="col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#cash">Harga Cash</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#kpr">Harga KPR</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content" id="content-price">
                                        <div class="tab-pane container active" id="cash">
                                            <div class="row mt-4">
                                                @foreach ($price_item as $item)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{ $item->name }}</label>
                                                        <input type="text" name="price_id_cash[{{ $item->price_id }}]" class="form-control money"
                                                            value="0">
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane container fade" id="kpr">
                                            <div class="row mt-4">
                                                @foreach ($price_item as $item)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{ $item->name }}</label>
                                                        <input type="text" name="price_id_kpr[{{ $item->price_id }}]" class="form-control"
                                                            value="0">
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
    loadComponent('{{ url('prop/component/slct_location') }}', 'select[name="location_id"]');
    loadComponent('{{ url('prop/component/slct_unit_type') }}', 'select[name="unittype_id"]');
    loadComponent('{{ url('prop/component/slct_unit_status') }}', 'select[name="unitstatus_id"]');
    showSubsidiType();

    $('.price-item').keyup(function() {
        var sum = 0;
        $('.price-item').each(function(){
            sum += parseFloat(this.value.replace(/\./g, ''));
        });

        $('input[name="ttl_harga_jual"]').val(formatRupiah(sum))
    });
});
</script>
@include('prop.ref_unit.asset.js')
@endsection
