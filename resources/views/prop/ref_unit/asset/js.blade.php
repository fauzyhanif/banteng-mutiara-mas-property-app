<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

function showBlocks(id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+id, 'select[name="block_id"]');
}

function showSubsidiType() {
    var type = $('select[name="jenis_rumah"]').val();
    loadComponent('{{ url('prop/component/slct_subsidi_type?type=') }}'+type, 'select[name="subsidi_type_id"]');
}

function showPriceByUnitType(id) {
    var url = '{{ url('prop/unit/show_price_by_unit_type?id=') }}'+ id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function (res) {
            $('#content-price').html(res);
        }
    })
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update') {
                    form.trigger("reset");
                }

                if (res.status == 'success') {
                    if ($('input[name="type"]').val() == 'store') {
                        // redirect to worklist by unit
                        var baseUrl = {!! json_encode(url('/prop/unit/form_worklist/')) !!};
                        var baseUrl = baseUrl + "/" + res.id;
                        window.location.href = baseUrl;
                    }
                    toastr.success(res.text)
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="form_worklist"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)

                    // redirect to worklist by unit
                    var baseUrl = {!! json_encode(url('/prop/unit/detail/')) !!};
                    var baseUrl = baseUrl + "/" + res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    // fitur baru
    $('form[name="worklist_form"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)

                    // redirect to worklist by unit
                    var baseUrl = {!! json_encode(url('/prop/unit/worklist_form/')) !!};
                    var baseUrl = baseUrl + "/" + res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    // check all
    $("#check-all").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
        if ($(this).prop('checked')==true){
            $('.form-select-mandor').attr('required', true);
        } else {
            $('.form-select-mandor').attr('required', false);
        }
    });

    // check all
    $("#check-all-2").click(function(){
        $('.checkbox-update').not(this).prop('checked', this.checked);
    });

    // check all
    $("#check-all-3").click(function(){
        $('.checkbox-store').not(this).prop('checked', this.checked);
    });

    // check all
    $(".checkbox-peritem").click(function(){
        if ($(this).prop('checked')==true){
            $('select[name="mandor_id['+this.value+']"]').attr('required', true);
        } else {
            $('select[name="mandor_id['+this.value+']"]').attr('required', false);
        }
    });
})();

function showMandor(params) {
    var my_array = params.split("|");
    var worklist_id = my_array[0];
    var sdm_id = my_array[1];
    loadComponent('{{ url('prop/component/slct_mandor?id=') }}'+sdm_id, 'select[name="mandor_id"]');

    var url = '{{ url('prop/worklist_master/get_salary?worklist_id=') }}'+worklist_id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function (res) {
            $('input[name="budget"]').val(res);
            $('input[name="total"]').val(res);
        }
    })
}

function name(params) {
    
}

</script>
