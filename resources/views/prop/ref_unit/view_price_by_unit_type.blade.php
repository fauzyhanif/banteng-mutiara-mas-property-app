<div class="tab-pane container active" id="cash">
    <div class="row mt-4">
        @foreach ($price_item as $item)
            @if ($item->sales_type == 'CASH')
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{ $item->priceItem->name }}</label>
                        <input type="text" name="price_id_cash[{{ $item->price_id }}]" class="form-control money" value="{{ $item->price }}">
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
<div class="tab-pane container fade" id="kpr">
    <div class="row mt-4">
        @foreach ($price_item as $item)
            @if ($item->sales_type == 'KPR')
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{ $item->priceItem->name }}</label>
                        <input type="text" name="price_id_kpr[{{ $item->price_id }}]" class="form-control money" value="{{ $item->price }}">
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>

<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});
</script>
