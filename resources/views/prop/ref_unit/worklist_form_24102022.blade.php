@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('prop/unit/detail', $unit->unit_id) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Detail Unit
            </a>
        </li>
        <li class="breadcrumb-item active">Pengerjaan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form name="worklist_form" action="{{ route('prop.unit.worklist_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $unit->unit_id }}">
                        <input type="hidden" name="unit_id" value="{{ $unit->number }}">
                        <input type="hidden" name="block_id" value="{{ $unit->block_id }}">
                        <input type="hidden" name="location_id" value="{{ $unit->location_id }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama Pekerjaan<span class="text-red">*</span></label>
                                <select name="worklist_id" class="form-control" onchange="showMandor(this.value)">
                                    <option value="">** Pilih Pekerjaan</option>
                                    @foreach ($ref_worklist as $worklist)
                                    <option value="{{ $worklist->id ."|". $worklist->sdm_id }}" @isset($data)
                                        {{ ($data->worklist_id == $worklist->id) ? 'selected' : '' }} @endisset>{{ $worklist->name }}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Brief</label>
                                <textarea name="brief" class="form-control">@isset($data){{ $data->brief }}@endisset</textarea>
                            </div>

                            <div class="form-group">
                                <label>
                                    Mandor <span class="text-danger">*</span>
                                </label>
                                <select name="mandor_id" class="form-control" required>
                                    <option value="">** Pilih Mandor</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Budget<span class="text-red">*</span></label>
                                <input type="text" name="budget" class="form-control money" required @if(isset($data)) value="{{ $data->total }}"
                                    @else value="0" @endif>
                            </div>

                            <div class="form-group">
                                <label>Qty<span class="text-red">*</span></label>
                                <input type="number" name="qty" class="form-control" required @if(isset($data)) value="{{ $data->qty }}" @else
                                    value="1" @endif>
                            </div>

                            <div class="form-group">
                                <label>Total Budget<span class="text-red">*</span></label>
                                <input type="text" name="total" class="form-control money" required @if(isset($data)) value="{{ $data->total }}"
                                    @else value="0" @endif>
                            </div>

                            <div class="form-group">
                                <label>Progress (hitungan persen)<span class="text-red">*</span></label>
                                <input type="number" name="progress" class="form-control" required @if(isset($data)) value="{{ $data->progress }}"
                                    @else value="0" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Tgl Mulai</label>
                                <input type="date" name="date_start" class="form-control" @if(isset($data)) value="{{ $data->date_start }}" @else
                                    value="{{ date('Y-m-d') }}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Tgl Deadline</label>
                                <input type="date" name="date_end" class="form-control" @if(isset($data)) value="{{ $data->date_end }}" @else
                                    value="{{ date('Y-m-d') }}" @endif>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Daftar Pengerjaan
                        </h5>
                    </div>

                    <div class="card-body">
                        <form action="{{ url('prop/unit/worklist_update') }}" method="POST">
                            @csrf
                            <table class="table table-sm table-bordered table-hover">
                                <thead class="bg-info">
                                    <th class="text-center">
                                        <input type="checkbox" id="check-all-2">
                                    </th>
                                    <th>Pekerjaan & Mandor</th>
                                    <th>Budget & Progress</th>
                                    <th class="text-center">Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ($unit_worklist as $item)
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="worklist_id[{{ $item->list_id }}]">
                                            </td>
                                            <td>
                                                {{ $item->worklist->name }}
                                                <br>
                                                (<span class="text-info">{{ $item->mandor->sdm->name }}</span>)
                                            </td>
                                            <td>
                                                {{ GeneralHelper::rupiah($item->total) }}
                                                @if ($item->st_transfer == '1')
                                                    <span class="text-secondary">(Budget sudah dialihkan)</span>
                                                @endif
                                                <br>
                                                (<span class="text-info">{{ $item->progress }}%</span>)
                                            </td>
                                            <td class="text-center">
                                                @if ($item->progress < 100)
                                                    <a href="{{ url('prop/unit/worklist_delete?list_id=' . $item->list_id) }}" class="btn btn-danger btn-xs tooltips" title="Hapus pengerjaan">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                @endif

                                                @if ($item->progress >= 100 && $item->st_transfer == '0')
                                                    <a href="{{ url('prop/unit/worklist_salary_mandor_store?list_id=' . $item->list_id) }}" class="btn btn-primary btn-xs tooltips" title="Alihkan budget ke hutang perusahaan">
                                                        <i class="fas fa-exchange-alt"></i>
                                                    </a>
                                                @endif

                                                @if ($item->progress >= 100 && $item->st_transfer == '1')
                                                    <a href="{{ url('prop/unit/worklist_salary_mandor_cancel?list_id=' . $item->list_id) }}" class="btn btn-warning btn-xs tooltips" title="Batalkan perlihan budget ke hutang perusahaan!">
                                                        <i class="fas fa-exchange-alt"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <button type="submit" class="btn btn-sm btn-success mt-3">
                                <i class="fas fa-edit"></i>
                                Jadikan Progress 100%
                            </button>
                        </form>

                        <br>

                        <p class="text-secondary mt-3">
                            Untuk update progress menjadi 100% silahkan pilih & centang item pengerjaan lalu klik tombol diatas.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('prop.ref_unit.asset.js')
@endsection
