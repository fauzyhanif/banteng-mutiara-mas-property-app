@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.unit') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Unit
            </a>
        </li>
        <li class="breadcrumb-item active">Update Unit</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.unit.update', $data->unit_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>
                                    Lokasi Perumahan <span class="text-danger">*</span>
                                </label>
                                <select name="location_id" class="form-control" required onchange="showBlocks(this.value)">
                                    <option value="">** Pilih Lokasi</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>
                                    Blok <span class="text-danger">*</span>
                                </label>
                                <select name="block_id" class="form-control" required>
                                    <option value="">** Pilih Blok</option>
                                    @isset($blok)
                                    @foreach ($blocks as $block)
                                    <option value="{{ $block->block_id }}" @isset($data) @if ($block->block_id == $data->block->block_id)
                                        selected
                                        @endif
                                        @endisset>
                                        {{ $block->name }}
                                    </option>
                                    @endforeach
                                    @endisset
                                </select>
                            </div>

                            <div class="form-group">
                                <label>
                                    Type Unit <span class="text-danger">*</span>
                                </label>
                                <select name="unittype_id" class="form-control" required>
                                    <option value="">** Pilih Type Unit</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label>Nomor Rumah <span class="text-red">*</span></label>
                                <input type="text" name="number" class="form-control" required value="{{ $data->number }}" style="pointer-events: none;">
                            </div>

                            <div class="form-group">
                                <label>Tanah Hook</label>
                                <input type="text" name="ground_hook" class="form-control" @isset($data) value="{{ $data->ground_hook }}" @endisset>
                            </div>

                            <div class="form-group">
                                <label>Minimal Uang Muka</label>
                                <input type="text" name="uang_muka" class="form-control money" @isset($data) value="{{ $data->uang_muka }}"
                                    @endisset>
                            </div>

                            <div class="form-group">
                                <label>Jenis Rumah <span class="text-red">*</span></label>
                                <select name="jenis_rumah" class="form-control" required onchange="showSubsidiType()">
                                    <option {{ ($data->jenis_rumah == 'SUBSIDI') ? 'selected' : '' }}>SUBSIDI</option>
                                    <option {{ ($data->jenis_rumah == 'NONSUBSIDI') ? 'selected' : '' }}>NONSUBSIDI</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Jenis Subsidi <span class="text-red">*</span></label>
                                <select name="subsidi_type_id"  class="form-control" required>
                                    <option value="">-- Pilih Subsidi --</option>
                                </select>

                                <span class="text-info">* Untuk setting marketing fee</span>
                            </div>

                            <div class="form-group">
                                <label>NOP</label>
                                <input type="text" name="nop" class="form-control" @isset($data) value="{{ $data->nop }}" @endisset>
                            </div>

                            <div class="form-group">
                                <label>Status Siap Jual</label>
                                <select name="is_publish" class="form-control">
                                    <option value="1" {{ $data->is_publish == '1' ? 'selected' : '' }}>Siap Jual</option>
                                    <option value="0" {{ $data->is_publish == '0' ? 'selected' : '' }}>Belum Siap</option>
                                </select>
                            </div>

                            <div class="form-control bg-info text-center mt-4 mb-4">
                                <label>BIAYA & LAIN-LAIN</label>
                            </div>

                            <div class="row mt-4">
                                <div class="col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#cash">Harga Cash</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#kpr">Harga KPR</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content" id="content-price">
                                        <div class="tab-pane container active" id="cash">
                                            <div class="row mt-4">
                                                @foreach ($price_item as $item)
                                                    @if ($item->sales_type == 'CASH')
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>{{ $item->priceItem->name }}</label>
                                                                <input type="text" name="price_id_cash[{{ $item->unitprice_id }}]" class="form-control money"
                                                                    value="{{ $item->price }}">
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane container fade" id="kpr">
                                            <div class="row mt-4">
                                                @foreach ($price_item as $item)
                                                    @if ($item->sales_type == 'KPR')
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>{{ $item->priceItem->name }}</label>
                                                                <input type="text" name="price_id_kpr[{{ $item->unitprice_id }}]" class="form-control money"
                                                                    value="{{ $item->price }}">
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    var param_get_block = {{ $data->location_id }} + "&id=" + {{ $data->block_id }};
    loadComponent('{{ url('prop/component/slct_location?id='.$data->location_id) }}', 'select[name="location_id"]');
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+param_get_block, 'select[name="block_id"]');
    loadComponent('{{ url('prop/component/slct_unit_type?id='.$data->unittype_id) }}', 'select[name="unittype_id"]');
    loadComponent('{{ url('prop/component/slct_unit_status?id='.$data->unitstatus_id) }}', 'select[name="unitstatus_id"]');
    loadComponent('{{ url('prop/component/slct_subsidi_type?id='.$data->subsidi_type_id) }}', 'select[name="subsidi_type_id"]');

    $('.price-item').keyup(function() {
        var sum = 0;
        $('.price-item').each(function(){
            sum += parseFloat(this.value.replace(/\./g, ''));
        });

        $('input[name="ttl_harga_jual"]').val(formatRupiah(sum))
    });
});
</script>
@include('prop.ref_unit.asset.js')
@endsection
