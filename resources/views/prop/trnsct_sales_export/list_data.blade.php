<div class="table-responsive">
  <table class="table table-sm table-bordered table-hover">
    <thead class="bg-info">
      <th width="20%">Konsumen</th>
      <th width="15%">TTL</th>
      <th>Alamat</th>
      <th width="20%">Pasangan</th>
      <th width="15%">Unit</th>
      <th width="7%" class="text-center">Hapus</th>
    </thead>
    <tbody>
      @foreach ($results as $result)
        <tr>
          <td>{{ $result->customer_name }} <br> <span class="text-muted">NIK {{ $result->customer_nik }}</span></td>
          <td>{{ $result->customer_place_of_birth }}, {{ GeneralHelper::konversiTgl($result->customer_date_of_birth, 'T') }}</td>
          <td>{{ $result->customer_address }}</td>
          <td>
            @if ($result->customer_id_married == 'Y')
              {{ $result->couple_name }} <br> <span class="text-muted">NIK {{ $result->couple_nik }}</span>
            @else
              -
            @endif
          </td>
          <td>{{ $result->block_name }} No {{ $result->unit_id }}</td>
          <td class="text-center">
            <button type="button" class="btn btn-xs btn-danger" onclick="deleteOne('{{ $result->id }}')">
              <i class="fas fa-trash"></i>
            </button>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row mt-4">
  <div class="col-md-8">
    {!! $results->render() !!}
  </div>
  <div class="col-md-4">
    <p class="text-right">
      showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }}
    </p>
  </div>
</div>

