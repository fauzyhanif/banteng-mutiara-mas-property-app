<table>
  <tbody>
    <tr>
      <td>Nama Konsumen</td>
      <td>Nik Konsumen</td>
      <td>TTL Konsumen</td>
      <td>Alamat Konsumen</td>
      <td>Sudah Menikah</td>
      <td>Nama Pasangan</td>
      <td>Nik Pasangan</td>
      <td>TTL Pasangan</td>
      <td>Unit yg Dipesan</td>
    </tr>
    @foreach ($items as $result)
      <tr>
        <td>{{ $result->customer_name }}</td>
        <td>{{ $result->customer_nik }}</td>
        <td>{{ $result->customer_place_of_birth }}, {{ GeneralHelper::konversiTgl($result->customer_date_of_birth, 'T') }}</td>
        <td>{{ $result->customer_address }}</td>
        <td>{{ ($result->customer_is_married == 'Y') ? 'Sudah' : 'Belum' }}</td>
        <td>{{ ($result->couple_name != '') ? $result->couple_name : '-' }}</td>
        <td>{{ ($result->couple_nik != '') ? $result->couple_nik : '-' }}</td>
        <td>
          @if ($result->customer_is_married == 'Y')
            {{ $result->couple_place_of_birth }}, {{ GeneralHelper::konversiTgl($result->couple_date_of_birth, 'T') }}
          @endif
        </td>
        <td>{{ $result->block_name }} No {{ $result->unit_id }}</td>
      </tr>        
    @endforeach
  </tbody>
</table>