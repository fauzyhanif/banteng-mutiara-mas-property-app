@extends('index')
@section('content')
<input type="hidden" name="pages" value="1">

<section class="content-header">
    <h1>Daftar Export Penjualan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Penjualan
                        <div class="card-tools">
                            <button type="button" class="btn btn-sm btn-danger" onclick="deleteAll()">
                                <i class="fas fa-trash"></i> Hapus Semua
                            </button>
                            <a href="{{ url('prop/sales-export/export') }}" class="btn btn-sm btn-warning">
                                <i class="fas fa-file-export"></i> Export Excel
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <div class="row mb-3">
                            <div class="col-md-5">
                                <input type="text" name="keyword" class="form-control form-control-sm">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-sm btn-primary" onclick="search()">Cari</button>
                            </div>
                            <div class="col-md-5 text-right">
                                <span>Show</span>
                                <select name="per_page" onchange="getData(1)">
                                    <option>25</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                                <span>entries</span>
                            </div>
                        </div>

                        <div id='view'>
                            <div class="table-responsive">
                                @include('prop.trnsct_sales_export.list_data')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(window).on('hashchange', function() {

    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');

        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            getData(page);
        }
    }
});

$(document).ready(function() {
    $(document).on('click', '.pagination a' ,function(event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl=$(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        $('input[name="pages"]').val(page)

        getData(page);
    });
});


function getData(page){
    var keyword = $('input[name="keyword"]').val();
    var per_page = $('select[name="per_page"]').val();

    keyword = keyword.replace(" ", "-");
    $.ajax({
        url: {!! json_encode(route('prop.sales-export.list-data')) !!} + '?page=' + page + '&per_page=' + per_page + '&keyword=' + keyword,
        type: "get",
        datatype: "html",
    }).done(function(data){
        $("#view").empty().html(data);
        location.hash = page;
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        alert('No response from server');
    });
}

function search() {
    var page = $('input[name="pages"]').val();
    getData(page)
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})();

function deleteOne(id) {
     $.ajax({
        type: 'POST',
        url: {!! json_encode(route('prop.sales-export.delete')) !!},
        dataType:'json',
        data: "id="+id,
        success: function (res) {
            if (res.status == 'success') {
                toastr.success(res.text)
                getData(1)
            } else {
                toastr.error(res.text)
            }
        }
    });
}

function deleteAll() {
     $.ajax({
        type: 'GET',
        url: {!! json_encode(route('prop.sales-export.delete-all')) !!},
        dataType:'json',
        success: function (res) {
            if (res.status == 'success') {
                toastr.success(res.text)
                getData(1)
            } else {
                toastr.error(res.text)
            }
        }
    });
}

</script>
@endsection
