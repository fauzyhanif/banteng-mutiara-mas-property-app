<script>
$(function() {
    loadData();
});

function showBlocks(id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+id, 'select[name="block_id"]');
}

function showPurchaseItem(id) {
    var url = '{{ url('prop/purchase/view_list_item') }}' + '/' + id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function (res) {
            $('.view-list-item').html(res);
        },
    });
}

function reloadPurchaseItemResponPage(id) {
    var url = '{{ url('prop/purchase/view_list_item_respon_page') }}' + '/' + id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function (res) {
            $('.view-list-item-respon-page').html(res);
        },
    });
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var purchase_id = $('input[name="purchase_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update') {
                    form.trigger("reset");
                }

                if (res.status == 'success') {
                    toastr.success(res.text)
                    showPurchaseItem(purchase_id);
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="respon_store"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var purchase_id = $('input[name="purchase_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                    reloadPurchaseItemResponPage(purchase_id);
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="delete_item"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var purchase_id = $('input[name="purchase_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                    showPurchaseItem(purchase_id);
                    $('#confirm-delete-item').modal('hide');
                } else {
                    toastr.error(res.text)
                    $('#confirm-delete-item').modal('hide');
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="change_status"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var status  = $('#status-purchase').val();

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $('#change-status').modal('hide');
                if (res.status == 'success') {
                    toastr.success(res.text)
                    if (status == '0') {
                        $('#text-status-purchase').html("<span class='text-secondary'>Belum ditanggapi</span>")
                    } else {
                        $('#text-status-purchase').html("<span class='text-green'>Sudah ditanggapi</span>")
                    }
                } else {
                    toastr.error(res.text)
                }
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('prop.purchase.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'purchase_date', name: 'purchase_date', searchable: true },
            { data: 'admin', name: 'admin', searchable: true },
            { data: 'store.name', name: 'store.name', searchable: true },
            { data: 'location', name: 'location', searchable: true },
            { data: 'note', name: 'note', searchable: true },
            { data: 'status_acc', name: 'status_acc', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function confirmDeleteItem(id) {
    var modal = $('#confirm-delete-item').modal('show');
    modal.find('input[name="purcitem_id"]').val(id)
}

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document).ready(function(){
    $("#select-sdm").select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ route('prop.component.slct_sdm') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });

    $('.money').mask('000.000.000.000.000', {reverse: true});
    /* For add action */
    // clone table row
    $('.btn-clone').on('click', function () {
        var form = $(".form-content:last").clone(true).appendTo(".form-wrapper");
        form.find('select[name="finance_item_account_id[]"]').val("");
        form.find('input[name="finance_item_amount[]"]').val("0");


        // add attr button remove row
        form.find('.btn-danger').addClass("btn-remove");
    });

    // remove table row
    $('table').on('click', '.btn-remove',function () {
        $(this).closest(".form-content").remove();
        sumTotal();
    });

    $('input[name="finance_item_amount[]"]').on('keyup', function() {
        sumTotal();
    });

});

function sumTotal() {
    var ttl = 0;
    $('input[name="finance_item_amount[]"]').each(function(){
        ttl += parseFloat(this.value.replace(/\./g, ''));
    });

    $('input[name="finance_amount"]').val(formatRupiah(ttl));
    $('input[name="ttl_amount_text"]').val(formatRupiah(ttl));
    $('input[name="finance_amount_paid"]').val(formatRupiah(ttl));
    checkBalance();
}

function show_list_payment(finance_id, finance_date, finance_user, finance_desc, finance_amount, finance_amount_paid) {
    if(isNaN(finance_amount - finance_amount_paid)) {var finance_sisa = 0;}
    $('#modal-list-payments').modal('show');
    $('#modal-list-payments-finance-date').text(finance_date);
    $('#modal-list-payments-finance-user').text(finance_user);
    $('#modal-list-payments-finance-desc').text(finance_desc);
    $('#modal-list-payments-finance-amount').text(finance_amount);
    $('#modal-list-payments-finance-amount-paid').text(finance_amount_paid);
    $('#modal-list-payments-finance-amount-sisa').text(finance_sisa);


    var url = "{{ url('finance/cost/list_payments') }}"+"/"+finance_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function (res) {
                $('#view-list-payments').html(res);
            }
        });
}

</script>
