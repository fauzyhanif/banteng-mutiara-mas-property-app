@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.purchase') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Purchase
            </a>
        </li>
        <li class="breadcrumb-item active">
            <a href="{{ url('prop/purchase/form_update', $purchase_id) }}">
                Kembali ke form utama
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah purchase item</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.purchase.store_item') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="store">
                        <input type="hidden" name="purchase_id" value="{{ $purchase_id }}">
                        <input type="hidden" name="store_id" value="{{ $store_id }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form input barang
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama Barang <span class="text-red">*</span></label>
                                <select name="goods_id" class="form-control" required onchange="show_unit(this.value)" style="width: 100%">
                                    <option value="">** Pilih Barang</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Qty <span class="text-red">*</span></label>
                                <div class="input-group">
                                    <input type="text" name="qty" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="satuan">satuan</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary btn-block">
                                Simpan
                            </button>
                            <a href="{{ route('prop.purchase') }}" class="btn btn-success btn-block">
                                Selesai & Kembali
                            </a>
                            <a href="{{ url('prop/purchase/print', $purchase_id) }}" class="btn bg-purple btn-block" target="_blank">
                                Selesai & Cetak
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Daftar Barang
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <th width="5%">No.</th>
                                <th>Nama barang</th>
                                <th width="15%">Qty</th>
                                <th width="5%">Hapus</th>
                            </thead>
                            <tbody class="view-list-item">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var purchase_id = $('input[name="purchase_id"]').val();
    var store_id = $('input[name="store_id"]').val();
    showPurchaseItem(purchase_id);

    $('select[name="goods_id"]').select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ url('prop/component/slct_goods?store_id=') }}"+store_id,
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
});

function show_unit(params) {
    params = params.split("|");
    console.log(params)
    $('#satuan').text(params[1])

}
</script>
@include('prop.trnsct_purchase.form_delete_item')
@include('prop.trnsct_purchase.asset.js')
@endsection
