@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.purchase') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Purchase
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Purchase</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Informasi Purchase</h3>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="30%">Tanggal</td>
                                    <td>{{ GeneralHelper::konversiTgl($purchase->purchase_date, 'slash') }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Dibuat Oleh</td>
                                    <td>{{ $purchase->admin }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Untuk Lokasi</td>
                                    <td>
                                        @if ($purchase->location_id != "")
                                            {{ $purchase->location->name }} Blok {{ $purchase->block->name }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">Catatan</td>
                                    <td>{{ $purchase->note }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Status</td>
                                    <td>
                                        @if ($purchase->status == '0')
                                            <span class="text-secondary">Belum ditanggapi</span>
                                        @else
                                            <span class="text-green">Sudah ditanggapi</span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Item Purchase</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <th width="5%">No</th>
                                <th>Nama Barang</th>
                                <th width="15%">Qty</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @foreach ($purchase_item as $item)
                                    <tr>
                                        <td>{{ $no }}.</td>
                                        <td>{{ $item->goods->name }}</td>
                                        <td>{{ $item->qty }}</td>
                                    </tr>
                                @php $no += 1 @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
