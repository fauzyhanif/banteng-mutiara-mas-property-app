@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.purchase') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Purchase
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Purchase</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="form-add" action="{{ route('prop.purchase.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="store">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('prop.trnsct_purchase.form_general')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan - selanjutnya input barang
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
    loadComponent('{{ url('prop/component/slct_location') }}', 'select[name="location_id"]');
});
</script>
@include('prop.trnsct_purchase.asset.js')
@endsection
