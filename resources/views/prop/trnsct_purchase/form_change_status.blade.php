<div class="modal" id="change-status" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Ubah Status Purchasing</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="change_status" action="{{ url('/prop/purchase/change_status', $purchase->purchase_id) }}" method="POST">
                @csrf

                <div class="modal-body">
                    <div class="form-group">
                        <label>Status Purchase</label>
                        <select name="status" class="form-control" id="status-purchase">
                            <option value="0" {{ ($purchase->status == '0') ? 'selected' : '' }}>Belum Ditanggapi</option>
                            <option value="1" {{ ($purchase->status == '1') ? 'selected' : '' }}>Sudah Ditanggapi</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan Status</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>
