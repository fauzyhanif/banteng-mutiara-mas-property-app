<div class="form-group">
    <label>Tanggal <span class="text-red">*</span></label>
    <input
        type="date"
        name="purchase_date"
        class="form-control"
        required
        @if (isset($data))
            value="{{ $data->purchase_date }}"
        @else
            value="{{ date('Y-m-d') }}"
        @endif
    >
</div>

<div class="form-group">
    <label>Lokasi</label>
    <select name="location_id" class="form-control" onchange="showBlocks(this.value)" required>
        <option value="">** Pilih Lokasi</option>
    </select>
</div>

<div class="form-group">
    <label>Blok</label>
    <select name="block_id" class="form-control">
        <option value="">** Pilih Blok</option>
    </select>
</div>

<div class="form-group">
    <label>Toko Tujuan</label>
    <select name="store_id" class="form-control" required>
        <option value="">** Pilih Toko</option>
        @foreach ($stores as $store)
        <option value="{{ $store->sdm_id }}" @isset($data) {{ ($store->sdm_id == $data->store_id) ? 'selected' : '' }} @endisset>{{ $store->sdm->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Catatan</label>
    <textarea name="note" class="form-control">@isset($data){{$data->note}}@endisset</textarea>
</div>

<div class="form-group">
    <label>Termin</label>
    <input type="text" name="termin" @if (isset($datas)) value="{{ $datas->termin }}"  @else value="30 HARI"  @endif class="form-control">
</div>

<div class="form-group">
    <label>Biaya Pengiriman</label>
    <input type="text" name="shipping_cost" @if (isset($datas)) value="{{ $datas->shipping_cost }}"  @else value="0"  @endif class="form-control">
</div>

<div class="form-group">
    <label>Metode Pembayaran</label>
    <textarea name="payment_method" class="form-control">@isset($data){{$data->payment_method}}@endisset</textarea>
    <span class="text-secondary"><i>Gunakan tanda koma (,) jika metode pembayaran lebih dari satu</i></span>
</div>

<div class="form-group">
    <label>Admin</label>
    <input type="text" name="admin" @if (isset($datas)) value="{{ $datas->admin }}"  @else value="{{ Session::get('auth_nama') }}"  @endif class="form-control" disabled>
</div>

@isset($data)
    <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
            <option value="0" {{ ($data->status == '0') ? 'selected' : '' }}>Belum ditanggapi</option>
            <option value="1" {{ ($data->status == '1') ? 'selected' : '' }}>Sudah ditanggapi</option>
        </select>
    </div>
@endisset
