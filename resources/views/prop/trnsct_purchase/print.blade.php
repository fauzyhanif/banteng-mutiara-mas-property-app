<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Purchase order</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">
                    @include('Component.kop_surat')

                    {{-- konten --}}
                    <div class="row">
                        <div class="col-md-12 col-print-12">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <h4 class="font-weight-bold">PURCHASE ORDER</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td widht="50%">
                                            <div class="row">
                                                <div class="col-md-3 col-print-3">Kepada</div>
                                                <div class="col-md-9 col-print-9">: {{ $purchase->store->name }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-print-3">Alamat</div>
                                                <div class="col-md-9 col-print-9">: {{ $purchase->store->address }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-print-3">Telepon</div>
                                                <div class="col-md-9 col-print-9">: {{ $purchase->store->phone_number }}</div>
                                            </div>
                                        </td>
                                        <td widht="50%">
                                            <div class="row">
                                                <div class="col-md-4 col-print-4">PO Number</div>
                                                <div class="col-md-8 col-print-8">: {{ $purchase->purchase_id.'/LSJ/PO/'.GeneralHelper::bulan_romawi(substr($purchase->purchase_date,5,2)).'/'.substr($purchase->purchase_date,0,4) }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-print-4">Tanggal</div>
                                                <div class="col-md-8 col-print-8">: {{ GeneralHelper::konversiTgl($purchase->purchase_date) }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-print-4">Proyek</div>
                                                <div class="col-md-8 col-print-8">: {{ $purchase->location->name }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-print-4">Termin</div>
                                                <div class="col-md-8 col-print-8">: {{ $purchase->termin }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-print-4">Penerima</div>
                                                <div class="col-md-8 col-print-8">: Tasam (081282749555)</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-print-4">Alamat Kirim</div>
                                                <div class="col-md-8 col-print-8">: DESA BENTENG - PURWAKARTA</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <h5 class="font-weight-bold text-center"></h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-print-12">
                            <h5>A. Bahan Material</h5>
                        </div>
                        <div class="col-md-12 col-print-12">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <th width="5%">No</th>
                                    <th>Description</th>
                                    <th width="5%">Qty</th>
                                    <th width="7%">Satuan</th>
                                    <th width="15%">Harga</th>
                                    <th width="20%">Jumlah</th>
                                </thead>
                                <tbody>
                                    @php
                                        $subtotal = 0;
                                        $no = 1;
                                    @endphp
                                    @foreach ($purchase_items as $item)
                                        <tr>
                                            <td>{{ $no }}.</td>
                                            <td>{{ $item->goods->name }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <td>{{ $item->goods->unit }}</td>
                                            <td>{{ Generalhelper::rupiah($item->goods->price) }}</td>
                                            <td>
                                                <span class="float-left">Rp</span>
                                                <span class="float-right">{{ Generalhelper::rupiah($item->goods->price * $item->qty) }}</span>
                                            </td>
                                        </tr>
                                        @php
                                            $subtotal += $item->goods->price * $item->qty;
                                            $no = $no + 1;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5" class="font-weight-bold">Subtotal</td>
                                        <td colspan="5" class="font-weight-bold">
                                            <span class="float-left">Rp</span>
                                            <span class="float-right">{{ Generalhelper::rupiah($subtotal) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="font-weight-bold">Pajak</td>
                                        <td colspan="5" class="font-weight-bold">
                                            <span class="float-left">Rp</span>
                                            <span class="float-right">0</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="font-weight-bold">Biaya Pengiriman</td>
                                        <td colspan="5" class="font-weight-bold">
                                            <span class="float-left">Rp</span>
                                            <span class="float-right">{{ Generalhelper::rupiah($purchase->shipping_cost) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="font-weight-bold">Total</td>
                                        <td colspan="5" class="font-weight-bold">
                                            <span class="float-left">Rp</span>
                                            <span class="float-right">{{ Generalhelper::rupiah($subtotal + $purchase->shipping_cost) }}</span>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-print-12">
                            <h5>B. Sistem Pembayaran</h5>
                        </div>
                        <div class="col-md-12 col-print-12">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <th width="5%">No</th>
                                    <th>Metode Pembayaran</th>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                        $payment_method_explode = explode(",", $purchase->payment_method);
                                    @endphp
                                    @foreach ($payment_method_explode as $item)
                                        <tr>
                                            <td>{{ $no++ }}.</td>
                                            <td>{{ $item }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-print-4 text-center">Received by</div>
                        <div class="col-md-4 col-print-4 text-center">Prepared by</div>
                        <div class="col-md-4 col-print-4 text-center">Approved by</div>
                    </div>

                    <div class="row" style="margin-top: 35px">
                        <div class="col-md-4 col-print-4 text-center">Official Stamp & Signature</div>
                        <div class="col-md-4 col-print-4 text-center">
                            <u>Deni Suhendar</u> <br>
                            Purchasing
                        </div>
                        <div class="col-md-4 col-print-4 text-center">
                            <u>Alan Suherlan</u> <br>
                            Director
                        </div>
                    </div>

                </div>
            </section>
        </body>
    </body>

</html>
