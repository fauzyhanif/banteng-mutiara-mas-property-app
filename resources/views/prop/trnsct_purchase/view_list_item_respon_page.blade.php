@php $no = 1 @endphp
@foreach ($datas as $item)
<tr>
    <td>{{ $no }}.</td>
    <td>{{ $item->goods->name }}</td>
    <td>{{ $item->qty }} {{ $item->goods->unit }}</td>
    <td>{{ $item->qty_realization }} {{ $item->goods->unit }}</td>
    <td>
        <input type="text" name="realization[{{ $item->goods_id }}]" class="form-control form-control-sm"
            value="{{ $item->qty - $item->qty_realization }}">
    </td>
</tr>
@php $no += 1 @endphp
@endforeach
