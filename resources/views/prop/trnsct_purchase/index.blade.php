@extends('index')

@section('content')
<section class="content-header">
    <h1>Purchase</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Purchase
                        <div class="card-tools">
                            <a href="{{ route('prop.purchase.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Buat Purchase Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>Dibuat Oleh</th>
                                <th>Toko Tujuan</th>
                                <th>Lokasi</th>
                                <th>Catatan</th>
                                <th>Status</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.trnsct_purchase.asset.js')
@endsection

