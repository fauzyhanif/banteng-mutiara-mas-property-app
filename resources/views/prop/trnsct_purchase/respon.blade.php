@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.purchase') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Purchase
            </a>
        </li>
        <li class="breadcrumb-item active">Respon Purchase</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Informasi Purchase</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <h4 class="font-weight-bold">PURCHASE ORDER</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td widht="50%">
                                        <div class="row">
                                            <div class="col-md-3 col-print-3">Kepada</div>
                                            <div class="col-md-9 col-print-9">: {{ $purchase->store->name }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-print-3">Alamat</div>
                                            <div class="col-md-9 col-print-9">: {{ $purchase->store->address }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-print-3">Telepon</div>
                                            <div class="col-md-9 col-print-9">: {{ $purchase->store->phone_number }}</div>
                                        </div>
                                    </td>
                                    <td widht="50%">
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">PO Number</div>
                                            <div class="col-md-8 col-print-8">:
                                                {{ $purchase->purchase_id.'/LSJ/PO/'.GeneralHelper::bulan_romawi(substr($purchase->purchase_date,5,2)).'/'.substr($purchase->purchase_date,0,4) }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">Tanggal</div>
                                            <div class="col-md-8 col-print-8">: {{ GeneralHelper::konversiTgl($purchase->purchase_date) }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">Proyek</div>
                                            <div class="col-md-8 col-print-8">: {{ $purchase->location->name }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">Termin</div>
                                            <div class="col-md-8 col-print-8">: {{ $purchase->termin }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">Penerima</div>
                                            <div class="col-md-8 col-print-8">: Tasam (081282749555)</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">Alamat Kirim</div>
                                            <div class="col-md-8 col-print-8">: DESA BENTENG - PURWAKARTA</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-print-4">Status</div>
                                            <div class="col-md-8 col-print-8">:
                                                <span id="text-status-purchase">
                                                    @if ($purchase->status == '0')
                                                        <span class="text-secondary">Belum ditanggapi</span>
                                                    @else
                                                        <span class="text-green">Sudah ditanggapi</span>
                                                    @endif
                                                </span>

                                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#change-status">Ubah Status</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Item Purchase</h3>
                    </div>

                    <form  action="{{ route('prop.purchase.respon_store', $purchase->purchase_id) }}" method="POST">

                        @csrf
                        <input type="hidden" name="purchase_id" value="{{ $purchase->purchase_id }}">

                        <div class="card-body">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Tanggal Realisasi <span class="text-red">*</span></label>
                                    <input type="date" name="realization_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Catatan</label>
                                    <textarea name="realization_note" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <thead class="bg-info">
                                        <th width="5%">No</th>
                                        <th>Nama Barang</th>
                                        <th width="10%">Qty</th>
                                        <th width="15%">Harga Satuan</th>
                                        <th width="15%">Jumlah</th>
                                        <th width="10%">Terealisasi</th>
                                        <th width="10%">Realisasi</th>
                                    </thead>
                                    <tbody class="view-list-item-respon-page">
                                        @php $no = 1; $subtotal = 0; @endphp
                                        @foreach ($purchase_item as $item)
                                        <tr>
                                            <td>{{ $no }}.</td>
                                            <td>{{ $item->goods->name }}</td>
                                            <td>{{ $item->qty }} {{ $item->goods->unit }}</td>
                                            <td>{{ GeneralHelper::rupiah($item->goods->price) }}</td>
                                            <td>{{ GeneralHelper::rupiah($item->goods->price * $item->qty) }}</td>
                                            <td>{{ $item->qty_realization }} {{ $item->goods->unit }}</td>
                                            <td>
                                                @if ($item->qty <= $item->qty_realization)
                                                    <span class="text-red">-</span>
                                                @else
                                                    <input type="text" name="realization[{{ $item->goods_id }}]" class="form-control form-control-sm" value="{{ $item->qty - $item->qty_realization }}">
                                                @endif
                                            </td>
                                        </tr>
                                        @php $no += 1; $subtotal += $item->goods->price * $item->qty; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        @if ($item->qty >= $item->qty_realization)
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary float-right">
                                    Simpan Realisasi
                                </button>
                            </div>
                        @endif
                    </form>

                    <div class="card-body mt-4">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead class="bg-info">
                                        <th width="75%">Keterangan</th>
                                        <th class="text-right">Biaya</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($subtotal) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Biaya Pengiriman</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($purchase->shipping_cost) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pajak</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah(0) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">Total</td>
                                            <td class="text-right font-weight-bold">{{ GeneralHelper::rupiah($subtotal + $purchase->shipping_cost) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    @if ($costs->count() == 0)
                        <form  action="{{ route('prop.purchase.store_cost') }}" method="POST">
                            @csrf
                            <input type="hidden" name="purchase_id" value="{{ $purchase->purchase_id }}">
                            <input type="hidden" name="finance_sdm_id" value="{{ $purchase->store_id }}">

                            <div class="card-header">
                                <h3 class="card-title">
                                    Keuangan
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal <span class="text-red">*</span></label>
                                            <input type="date" name="finance_date" class="form-control" value="{{ date('Y-m-d') }}"
                                                required>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Penerima <span class="text-red">*</span></label>
                                            <input type="text" name="finance_sdm_name" class="form-control" value="{{ $purchase->store->name }}" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Deskripsi <span class="text-red">*</span></label>
                                            <textarea name="finance_desc" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <table class="table">
                                        <thead class="bg-info">
                                            <th width="5%">Hapus</th>
                                            <th>Akun Operasional</th>
                                            <th width="20%">Jumlah</th>
                                        </thead>
                                        <tbody class="form-wrapper">
                                            <tr class="form-content">
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-xs btn-danger">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    <select name="finance_item_account_id[]" class="form-control form-control-sm" required>
                                                        <option value="">-- Pilih Akun --</option>
                                                        @foreach ($accounts as $account)
                                                        <option value="{{ $account->account_id }}">{{ $account->account_name }}</option>
                                                        @endforeach %}
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="finance_item_amount[]"
                                                        class="form-control form-control-sm amount money" value="0" required>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="row">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-sm btn-primary btn-clone">
                                                        <i class="fas fa-plus"></i> Tambah Data
                                                    </button>
                                                </td>
                                                <td width="30%" class="text-right">
                                                    <h4 class="font-weight-bold">Total</h4>
                                                </td>
                                                <td width="20%">
                                                    <input type="hidden" name="finance_amount">
                                                    <input type="text" name="ttl_amount_text" class="form-control form-control-sm" disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    <h5 class="font-weight-bold">Dibayar</h5>
                                                </td>
                                                <td width="20%">
                                                    <input type="text" name="finance_amount_paid" class="form-control form-control-sm">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    <h5 class="font-weight-bold">Dibayar Via</h5>
                                                </td>
                                                <td width="20%">
                                                    <select name="finance_paid_cash_id" id="" class="form-control form-control-sm">
                                                        @foreach ($cashes as $cash)
                                                        <option value="{{ $cash->cash_id }}">{{ $cash->cash_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-body text-right" style="border-top: 1px solid rgba(0,0,0,.125)">
                                <button type="submit" class="btn btn-success btn-save">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                            <div class="card-body">
                                <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                            </div>
                        </form>
                    @else
                        <div class="card-header">
                            <h3 class="card-title">
                                Keuangan
                            </h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead class="bg-info">
                                    <th width="15%">Tgl Transaksi</th>
                                    <th>Terkait</th>
                                    <th width="15%" class="text-right">Jumlah</th>
                                    <th width="15%" class="text-right">Dibayar</th>
                                    <th width="15%" class="text-right">Sisa</th>
                                    <th width="10%">Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ($costs as $cost)
                                        <tr>
                                            <td>{{ date("d-m-Y", strtotime($cost->finance_date)) }}</td>
                                            <td>{{ $cost->sdm->name }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($cost->finance_amount) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($cost->finance_amount_paid) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($cost->finance_amount - $cost->finance_amount_paid) }}</td>
                                            <td>
                                                <a href="{{ route('finance.cost.detail', $cost->finance_id) }}" target="_blank" class="btn btn-xs btn-info">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <button class="btn btn-xs btn-primary" onclick='show_list_payment("{{ $cost->finance_id }}", "{{ $cost->finance_date }}", "{{ $cost->sdm->name }}", "{{ $cost->finance_desc }}", "{{ $cost->finance_amount }}", "{{ $cost->finance_amount_paid }}")'>
                                                    <i class="fas fa-print"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@include('finance.trnsct_cost.list_payments')
@include('prop.trnsct_purchase.form_change_status')
@include('prop.trnsct_purchase.asset.js')
@endsection
