<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Deskripsi</label>
    <textarea name="description" class="form-control">@isset($data){{ $data->description }}@endisset</textarea>
</div>

<div class="form-group">
    <label>Alamat</label>
    <textarea name="address" class="form-control">@isset($data){{ $data->address }}@endisset</textarea>
</div>
