<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PEMOHON KPR BERSUBSIDI BTN - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 15</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">(Format Kementrian PUPR)</div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN PEMOHON KPR BERSUBSIDI BTN</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>Yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Lengkap</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/Tgl lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>

                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku pemohon.</p>

                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Lengkap</td>
                                        <td>: {{ $customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/Tgl lahir</td>
                                        <td>: {{ $customer->couple_place_of_birth  }},
                                            {{ ($customer->couple_date_of_birth == '') ? '' : GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->couple_job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->couple_address_domicile }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku suami/istri pemohon.</p>

                            <p>Menyatakan dengan sesungguhnya:</p>
                            <ol style="margin-top: -10px">
                                <li>
                                    Saya selaku pemohon memiliki gaji/upah pokok/penghasil bersih/upah rata-rata*) perbulan sebeser <br>
                                    <b>Rp ....................................... (.............................................................. rupiah)</b>
                                </li>
                                <li>
                                    Saya dan istri/suami*) tidak memiliki hak kepemilikan atas rumah pada saat pengajuan pembiayaan KPR Bersubsidi BTN.
                                </li>
                                <li>
                                    Saya dan istri/suami*) belum pernah menerima subsidi atau bantuan pembiayaan perumahan dari pemerintah terkait
                                    kredit/pembiayaan kepemilikan rumah dan/atau pembangunan rumah swadaya.
                                </li>
                                <li>
                                    Saya membeli Rumah Umum Tapak/Sarusun Umum dengan harga <b>Rp. ....................................... (.............................................................. rupiah)</b>
                                    dari pengembang PT. LAN SENA JAYA
                                </li>
                                <li>
                                    Saya dan istri/suami*) akan menggunakan Rumah Umum Tapak/Sarusun Umum sebagai tempat tinggal saya dan/atau
                                    keluarga dalam kurun waktu paling lambat 1 (satu) tahun setelah terima rumah.
                                </li>
                                <li>
                                    Saya dan istri/suami*) tidak akan menyewakan/mengontrakkan, memperjual-belikan atau memindahtangankan dengan bentuk
                                    perbuatan hukum apapun, kecuali :
                                    <ul>
                                        <li>Penghunian telah melampaui 5 (lima) tahun untuk Rumah Umum Tapak.</li>
                                        <li>Penghunian telah melampaui 20 (dua puluh) tahun untuk Sarusun Umum.</li>
                                        <li>Pindah tempat tinggal sesuai ketentuan peraturan perundang-undangan.</li>
                                        <li>Meninggal dunia (pewarisan), atau</li>
                                        <li>Untuk kepentingan Bank BTN dalam rangka penyelesaian kredit bermasalah.</li>
                                    </ul>
                                </li>
                                <li>
                                    Bahwa semua dokumen persyaratan yang disampaikan kepada Bank BTN untuk memperoleh fasilitas subsidi adalah benar
                                    dan dapat dipertanggungjawabkan keabsahaannya baik secara formil maupun materil.
                                </li>
                                <li>
                                    Apabila di kemudian hari pernyataan saya tidak benar dan/atau tidak saya penuhi, saya bersedia mengembalikan seluruh
                                    subsidi yang telah saya terima dari pemerintah dan bersedia dikenakan sanksi sesuai dengan ketentuan peraturan perundang-undangan.
                                </li>
                            </ol>
                        </div>

                    </div>
                </div>
            </section>

            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 15</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>


                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Demikian surat pernyataan ini saya buat dengan sebenar-benarnya tanpa paksaan dari pihak
                                manapun.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">Menyetujui,</div>
                        <div class="col-md-6 col-print-6 text-center mb-4">Yang membuat pernyataan,</div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12 col-print-12 text-center">Mengetahui,</div>
                        <div class="col-md-12 col-print-12 text-center mb-4">Pimpinan Tempat Bekerja/Kepala Desa/Lurah* ......................................</div>

                        <div class="col-md-12 my-4"></div>

                        <div class="col-md-12 col-print-12 text-center mt-4">(......................................)</div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 col-print-12">
                            <p>*) Coret salah yang tidak perlu</p>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
