<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PENGHUNINAN RUMAH UMUM BERSUBSIDI - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>
    <body>
        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 2</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN PENGHUNINAN RUMAH UMUM BERSUBSIDI</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12">
                            <p>Yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Lengkap</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat, Tanggal Lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Selaku Debitur KPR Bersubsidi BTN menyatakan dengan sesungguhnya bahwa: <br>
                                <ol>
                                    <li>
                                        Saya telah memahami ketentuan penghunian rumah sejahtera sebagaimana dimaksud di dalam Peraturan Menteri Pekerjaan
                                        Umum  dan Perumahan Rakyat.
                                    </li>
                                    <li>
                                        Saya menyatakan bahwa :
                                        <ul>
                                            <li>berpenghasilan tidak melebihi  batas penghasilan kelompok sasaran KPR Bersubsidi;</li>
                                            <li>saya dan istri/suami*) tidak memiliki rumah;</li>
                                            <li>saya dan istri/suami*) tidak pernah menerima subsidi kepemilikan rumah.</li>
                                            <li>menggunakan sendiri dan menghuni rumah umum tapak atau sarusun umum sebagai tempat tinggal dalam jangka waktu paling lambat 1 (satu) tahun setelah serah terima rumah.</li>
                                            <li>tidak akan menyewakan dan/atau mengalihkan kepemilikan rumah umum tapak atau sarusun umum dengan bentuk perbuatan hukum apapun, kecuali sesuai dengan ketentuan Peraturan Menteri Pekerjaan Umum dan Perumahan Rakyat.</li>
                                        </ul>
                                    </li>
                                    <li>
                                        Bahwa semua dokumen persyaratan yang disampaikan kepada Bank BTN untuk memperoleh KPR Bersubsidi BTN adalah benar dan dapat dipertanggungjawabkan keabsahaannya.
                                    </li>
                                    <li>
                                        Apabila di kemudian hari pernyataan ini tidak benar dan/atau tidak saya penuhi, saya bersedia dan memeberikan kuasa kepada Bank BTN untuk menghentikan fasilitas KPR Bersubsidi BTN
                                        dan/atau mengubah menjadi KPR BTN Non-Subsidi, setelah Bank BTN menerima surat permintaan penghentian KPR Bersubsidi dari pihak yang berwenang.
                                    </li>
                                    <li>
                                        Saya bersedia untuk menanggung segala biaya yang meliputi biaya asuransi, biaya pengikatan angunan, dan biaya lainnya yang timbul atas penghentian KPR Bersubsidi BTN
                                    </li>
                                </ol>

                                <br>
                                Demikian surat pernyataan ini saya buat dengan sebenar-benarnya tanpa paksaan dari pihak manapun.
                            </p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">Yang Menyetujui,</div>
                        <div class="col-md-6 col-print-6 text-center mb-4">Yang Membuat Pernyataan,</div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12 col-print-23 text-center">
                            Mengetahui, <br>
                            <b>PT. BANK TABUNGAN NEGARA (PERSERO) tbk.</b> <br>
                            <b>KANTOR CABANG KARAWANG</b>
                        </div>
                    </div>

                    <div class="row text-secondary" style="margin-top: 35px">
                        *) coret yang tidak perlu
                    </div>
                </div>
            </section>
        </body>
    </body>
</html>
