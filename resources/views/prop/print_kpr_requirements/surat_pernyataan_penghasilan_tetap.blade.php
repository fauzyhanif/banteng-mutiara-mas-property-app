<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PENGHASILAN - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>
    <body>
        <body class="A4">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 2</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                            (MBR Berpenghasilan tetap)
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN PENGHASILAN</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12">
                            <p>Yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP/Passport</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat Domisili</td>
                                        <td>: {{ $customer->address_domicile }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Menyatakan dengan sesungguhnya bahwa sampai saat surat pernyataan ini ditandatangani, saya
                                menyatakan bahwa jumlah gaji/upah pokok saya adalah sebesar Rp. {{ GeneralHelper::rupiah($customer->income) }} ({{ GeneralHelper::kekata($customer->income) }} rupiah) per
                                bulan. <br><br>
                                Demikian surat pernyataan ini saya buat dengan sebenarnya tanpa paksaan dari pihak manapun dan apabila
                                di kemudian hari pernyataan saya ini tidak benar, saya bersedia mengembalikan seluruh subsidi yang saya
                                terima.
                            </p>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-6 col-print-6 text-center"></div>
                            <div class="col-md-6 col-print-6 text-center">Purwakarta,
                                {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}</div>

                            <div class="col-md-12 col-print-12">Mengetahui:</div>

                            <div class="col-md-6 col-print-6">Kepala Desa/Lurah/Pimpinan Perusahaan/Instansi</div>
                            <div class="col-md-6 col-print-6 text-center">Yang membuat pernyataan,</div>

                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-6 col-print-6 text-center mb-4"></div>
                            <div class="col-md-6 col-print-6 text-center mb-4 text-secondary">Materai secukupnya</div>
                            <br>
                            <br>

                            <div class="col-md-6 col-print-6 mt-2">(..............................)</div>
                            <div class="col-md-6 col-print-6 text-center mt-2">({{ $customer->name }})</div>

                        </div>

                        <div class="row text-secondary" style="margin-top: 35px">
                            <div class="col-lg-12 col-print-12">
                                *diberikan cap perusahaan/instansi
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>
</html>
