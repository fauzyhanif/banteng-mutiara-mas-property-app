<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PENYERAHAN DATA - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>
    <body>
        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 1</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                            (Pemohon FLPP)
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN PENYERAHAN DATA</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12">
                            <p>Saya, yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat, Taggal Lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat Domisili</td>
                                        <td>: {{ $customer->address_domicile }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat Sesuai KTP</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Nomor Telepon/HP</td>
                                        <td>: {{ $customer->phone_num }} </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat email</td>
                                        <td>: </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku Pemohon.</p>

                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->couple_place_of_birth  }},
                                            {{ ($customer->couple_date_of_birth == '') ? '' : GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->couple_job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat Domisili</td>
                                        <td>: {{ $customer->address_domicile }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku suami/istri pemohon.</p>

                            <p>
                                Bersama ini; <br>
                                Menyatakan telah mengetahui, memahami dan menyanggupi untuk memenuhi seluruh ketentuan dan
                                persyaratan Pusat Pengelolaan Dana Pembiayaan Perumahan (PPDPP) untuk mendapatkan fasilitas
                                KPR Sejahtera. <br>
                                Menyampaikan semua data pribadi (KTP, NPWP, Pas Photo) untuk mendapatkan fasilitas KPR
                                Sejahtera dan semua data lainnya yang diperlukan oleh PPDPP melalui Bank BTN, serta menjamin
                                bahwa semua data yang saya sampaikan tersebut adalah benar dan dapat dipertanggungjawabkan
                                keabsahannya. <br>
                                Memberikan kuasa kepada PPDPP untuk mengakses semua data pribadi saya yang terkait data
                                FLPP yang ada di Bank BTN. <br>
                                Apabila dikemudian hari pernyataan saya ini tidak benar dan/atau tidak saya penuhi, saya bersedia
                                mengembalikan seluruh subsidi yang telah saya terima dari Pemerintah dan bersedia dikenakan
                                sanksi sesuai dengan ketentuan peraturan perundang-undangan. <br>
                                Memberikan persetujuan kepada Bank BTN untuk memberikan semua data pribadi saya yang
                                terdapat di Bank BTN kepada PPDPP. <br><br>
                                Demikian surat pernyataan ini saya buat dengan sebenar-benarnya tanpa paksaan dari pihak
                                manapun.
                            </p>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-6 col-print-6 text-center"></div>
                            <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                            <div class="col-md-6 col-print-6 text-center mb-4">Yang Menyetujui,</div>
                            <div class="col-md-6 col-print-6 text-center mb-4">Yang Membuat Pernyataan,</div>


                            <div class="col-md-6 col-print-6 text-center my-4"></div>
                            <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai 6000</div>

                            <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>
                            <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>

                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>
</html>
