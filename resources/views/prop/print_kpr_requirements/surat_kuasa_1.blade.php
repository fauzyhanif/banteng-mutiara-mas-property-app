<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT KUASA - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>
        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 12</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT KUASA</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>Saya, yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>:
                                            {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">yang dalam hal ini bertindak untuk dan atas nama sendiri. Selanjutnya disebut : <b>PEMBERI KUASA</b></p>


                            <p>
                                PT BANK TABUNGAN NEGARA (Persero) Tbk, berkedudukan di Jl. gajah Mada No. 1 jakarta Pusat
                                yang dalam hal ini diwakili oleh ........................................................................ selaku ........................................................................
                                di PT. BANK TABUNGAN NEGARA (Persero) Kantor Cabang ........................................................................ Selanjutnya disebut : <b>PENERIMA KUASA.</b>
                            </p>

                            <p>
                                Dengan ini PEMBERI KUASA memberi kuasa kepada PENERIMA KUASA untuk melakukan pendebetan pada
                                Nomor Rekening Tabungan PEMBERI KUASA: ..........................................atas biaya asuransi, biaya pengikatan
                                agunan, dan biaya lainnya yang timbul atas penghentian KPR Bersubsidi yang disebabkan oleh dokumen
                                pernyataan yang saya buat tidak benar dan/atau tidak saya penuhi dalam proses pengajuan KPR Bersubsidi
                                pada Bank BTN.
                            </p>

                            <p>
                                Kuasa ini diberikan dengan hak Substitusi, tidak dapat dicabut kembali dan tidak akan berakhir karena sebab -
                                sebab yang tercantum dalam pasal 1813 Kitab Undang - Undang Hukum Perdata atau karena sebab apapun
                                juga.
                            </p>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-6 col-print-6 text-center"></div>
                            <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                            <div class="col-md-6 col-print-6 text-center mb-4">
                                Penerima Kuasa <br>
                                PT. BANK TABUNGAN NEGARA (Persero) Tbk <br>
                                Kantor Cabang ........................
                            </div>
                            <div class="col-md-6 col-print-6 text-center mb-4">Pemberi Kuasa,</div>

                            <div class="col-md-6 col-print-6 text-center my-4"></div>
                            <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                            <div class="col-md-6 col-print-6 text-center mt-4">(.....................................................)</div>
                            <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>

                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
