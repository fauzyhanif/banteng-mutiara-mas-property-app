<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>KETETAPAN WAKTU UNTUK VERIFIKASI - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>
    <body>
        <body class="A4 landscape" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-12 col-print-12">
                            <b><u>LAMPIRAN 21</u></b>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>KETETAPAN WAKTU UNTUK VERIFIKASI</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12">
                            <p class="font-weight-bold">
                                PERUMAHAN BENTENG MUTIARA MAS <br>
                                BLOK :
                            </p>
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle" width="5%" rowspan="2">NO</th>
                                        <th class="text-center align-middle" width="30%" rowspan="2">NAMA</th>
                                        <th class="text-center align-middle" width="20%" rowspan="2">NO.TLP</th>
                                        <th class="text-center" width="20%" colspan="2">JAM BISA DIHUBUNGI</th>
                                        <th class="text-center align-middle" width="25%" rowspan="2">KETERANGAN</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">1</th>
                                        <th class="text-center">2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="height: 30px"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 30px"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-lg-12 col-print-12">
                            <p class="font-weight-bold">
                                INSTANSI PEKERJAAN (KATOR TEMPAT KERJA)
                            </p>
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle" width="5%" rowspan="2">NO</th>
                                        <th class="text-center align-middle" width="30%" rowspan="2">NAMA</th>
                                        <th class="text-center align-middle" width="20%" rowspan="2">NO.TLP</th>
                                        <th class="text-center" width="20%" colspan="2">JAM BISA DIHUBUNGI</th>
                                        <th class="text-center align-middle" width="25%" rowspan="2">KETERANGAN</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">1</th>
                                        <th class="text-center">2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="height: 30px"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 30px"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-lg-12 col-print-12">
                            <p class="font-weight-bold">
                                CONTACT EMERGENCY
                            </p>
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle" width="5%" rowspan="2">NO</th>
                                        <th class="text-center align-middle" width="30%" rowspan="2">NAMA</th>
                                        <th class="text-center align-middle" width="20%" rowspan="2">NO.TLP</th>
                                        <th class="text-center" width="20%" colspan="2">JAM BISA DIHUBUNGI</th>
                                        <th class="text-center align-middle" width="25%" rowspan="2">KETERANGAN SAUDARA-ALAMAT LENGKAP</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">1</th>
                                        <th class="text-center">2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="height: 100px"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Keterangan : </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 100px"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Keterangan : </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>
</html>
