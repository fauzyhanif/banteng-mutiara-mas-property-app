<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>BERITA ACARA SERAH TERIMA RUMAH SEJAHTERA TAPAK - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 4</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>BERITA ACARA SERAH TERIMA <br> RUMAH SEJAHTERA TAPAK</u></b></h5>
                        </div>
                        <div class="col-lg-12 col-print-12 text-center">
                            <p>No ........................................</p>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12 col-print-12">
                            <p>
                                Berdasarkan PPJB/AJB*) No ..................... tanggal ........................................ telah dilakukan serah terima
                                pada tanggal ........................................ dari Pengembang <b>PT LAN SENA JAYA</b>, selanjutnya disebut <b>"Pihak Pertama"</b>;
                            </p>

                            <p>Kepada pembeli :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No Telp/HP</td>
                                        <td>: {{ $customer->phone_num }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">selanjutnya disebut <b>"Pihak Kedua"</b></p>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12 col-print-12">
                            <p>Atas 1 (satu) unit Rumah Umum Tapak pada lokasi sebagai berikut:</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>1 Nama Perumahan</td>
                                        <td>: {{ $sale->location->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>2 No Rumah</td>
                                        <td>: {{ $sale->block->name }} No {{ $sale->unit_id }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>3 Luas Tanah dan Lantai Rumah</td>
                                        <td>: {{ $sale->unit->unitType->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>4 Alamat</td>
                                        <td>: {{ $sale->location->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p>Selanjutnya disebut <b>"Obyek Serah Terima"</b>.</p>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12 col-print-12">
                            <p>Obyek Serah Terima dengan kondisi laik fungsi dan dilengkapi dengan:</p>
                            <ol style="margin-top: -10px">
                                <li>Jaringan air bersih sudah berfungsi;</li>
                                <li>Jaringan listrik sudah berfungsi;</li>
                                <li>Jalan lingkungan sudah selesai dan berfungsi;</li>
                                <li>Saluran air limbah/air kotor rumah tangga sudah selesai dan berfungsi; dan</li>
                                <li>Sarana pewadahan sampah individual dan tempat pembuangan sampah sementara.</li>
                            </ol>
                            <p>Demikian berita acara serah terima ini ditandatangani oleh kedua belah pihak dan dapat dipertanggungjawabkan.</p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center">Pihak Pertama/Kuasa*,</div>
                        <div class="col-md-6 col-print-6 text-center">Pihak Kedua,</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">PT LAN SENA JAYA</div>
                        <div class="col-md-6 col-print-6 text-center mb-4"></div>

                        <div class="col-lg-12 my-4"></div>

                        <div class="col-md-6 col-print-6 text-center mt-4">(.......................................)</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>

                    </div>

                    <div class="row text-secondary" style="margin-top: 35px">
                        <div class="col-lg-12 col-print-12">
                            *) coret yang tidak perlu
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
