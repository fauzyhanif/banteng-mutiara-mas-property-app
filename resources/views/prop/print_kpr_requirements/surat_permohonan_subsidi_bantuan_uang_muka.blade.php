<!DOCTYPE html>
<html lang="en">

    @php
        $uang_muka = 0;
        if ($sale->kpr_credit_acc == '0') {
            $uang_muka = $sale->ttl_trnsct - $sale->kpr_credit_pengajuan;
        } else {
            $uang_muka = $sale->ttl_trnsct - $sale->kpr_credit_acc;
        }
    @endphp

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERMOHONAN SUBSIDI BANTUAN UANG MUKA - {{ $customer->name }}
        </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 8</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERMOHONAN SUBSIDI BANTUAN UANG MUKA</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Kepada Yth : <br>
                                Kepala Satuan Kerja Direktorat Jenderal Pembiayaan Perumahan <br>
                                Kementrian Pekerjaan Umum dan Perumahan Rakyat <br>
                                Jalan Raden Patah 1 No 1 Lantai 2 Wing 3. <br>
                                Kebayoran Baru, Jakarta Selatan 12110
                            </p>

                            <p class="font-weight-bold">
                                Perihal : Permohonan Subsidi Bantuan Uang Muka (SBUM)
                            </p>

                            <p>
                                Saya yang bertanda tangan dibawah ini :
                            </p>

                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>Mengajukan permohonan Subsidi Bantuan Uang Muka untuk pembelian rumah sejahtera tapak dengan keterangan sebagai berikut: </p>

                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Pengembang</td>
                                        <td>: PT. LAN SENA JAYA</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat Rumah Yang Dibeli</td>
                                        <td>: {{ $sale->location->address }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Harga Jual Rumah</td>
                                        <td>: Rp </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Besaran Uang Muka</td>
                                        <td>: Rp </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Bank Pelaksana</td>
                                        <td>: {{ $sale->bank->name }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Sebagai pertimbangan, bersama ini kami lampirkan dokumen fotokopi surat pengakuan kekurangan bayar uang
                                muka pembelian rumah sejahtera tapak yang disetujui oleh ...............................................*)
                            </p>

                            <p>
                                Dengan surat permohonan ini saya menyatakan telah memahami dan tunduk pada ketentuan Pemerintah yang mengatur
                                Subsidi Bantuan Uang Muka (SBUM). Apabila dikemudian hari saya tidak dapat menjalankan ketentuan Pemerintah
                                tersebut diatas yang mengakibatkan Pemerintah mencabut semua kemudahan dan subsidi terkait
                                kemudahan dalam perolehan rumah, saya bersedia mengembalikan semua kemudahan dan subsidi yang telah saya
                                terima tersebut.
                            </p>

                            <p>
                                Demikian kami sampaikan atas perhatiannya kami ucapkan terima kasih.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center mb-4"></div>
                        <div class="col-md-6 col-print-6 text-center mb-4">Purwakarta, .........................................</div>

                        <div class="col-lg-12 my-4"></div>

                        <div class="col-md-6 col-print-6 text-center mt-4"></div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 col-print-12">
                            *) diisi dengan nama direktur atau yang mewakili pengembang dan nama perusahaan pengembang
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
