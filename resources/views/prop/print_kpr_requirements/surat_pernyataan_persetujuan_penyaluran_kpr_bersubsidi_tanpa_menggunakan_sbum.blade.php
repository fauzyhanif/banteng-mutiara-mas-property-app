<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PERSETUJUAN PENYALURAN KPR BERSUBSIDI TANPA MENGGUNAKAN SBUM - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>
        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 6</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN PERSETUJUAN PENYALURAN KPR BERSUBSIDI TANPA MENGGUNAKAN SBUM</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12">
                            <p>Saya, yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku Pemohon.</p>

                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->couple_place_of_birth  }},
                                            {{ ($customer->couple_date_of_birth == '') ? '' : GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->couple_job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->couple_address_domicile }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku suami/istri pemohon.</p>

                            <p>
                                Menyatakan dengan sesungguhnya bahwa sehubungan dengan belum dilakukannya kerja sama penyaluran Subsidi Bantuan
                                Uang Muka Perumahan (SBUM) tahun 2021 atas fasilitas Kredit Pemilikan Rumah (KPR) Bersubsidi yang Saya dan istri/suami
                                ajukan maka Saya dan istri/suami mengetahui dan menyetujui bahwa penyaluran KPR Bersubsidi dimaksud atas pembelian rumah umum
                                tapak pada proyek perumahan Benteng Mutiara Mas Cluster <b>{{ $sale->unit->unitType->name }}</b>
                                Blok/No <b>{{ $sale->block->name }}/{{ $sale->unit_id }}</b> yang dikembangkan oleh <b>PT LAN SENA JAYA</b> tidak difasilitasi oleh SBUM.
                            </p>

                            <p>
                                Demikian surat pernyataan ini saya buat dengan sebenarnya tanpa paksaan dari pihak manapun dan apabila dikemudian hari
                                pernyataan saya ini tidak benar, saya bersedia mengembalikan seluruh subsidi yang saya terima.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">Menyetujui,</div>
                        <div class="col-md-6 col-print-6 text-center mb-4">Membuat Pernyataan,</div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai 6000</div>

                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-print-12 text-center">Mengetahui,</div>
                        <div class="col-md-12 col-print-12 text-center">Pengembang PT LAN SENA JAYA</div>


                        <div class="col-md-12 col-print-12 text-center" style="margin-top: 80px">(.................................................)</div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
