<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERSYARATAN KELOMPOK SASARAN - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 7</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>PERSYARATAN KELOMPOK SASARAN</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 col-print-12">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="5%" rowspan="3">NO</th>
                                        <th class="text-center" width="55%" rowspan="3">PERSYARATAN</th>
                                        <th class="text-center" width="40%" colspan="3">KELOMPOK SASARAN</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" colspan="2">KAWIN</th>
                                        <th class="text-center" rowspan="2">LAJANG</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">PEMOHON</th>
                                        <th class="text-center">PASANGAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Kartu Tanda Penduduk (KTP)</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Kartu Keluarga (KK)</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>Akta Nikah</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td>Tidak memeiliki rumah *</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5</td>
                                        <td>Belum pernah menerima subsidi perolehan rumah berupa pemilikan rumah dari Pemeritah *</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6</td>
                                        <td>Nomor Pokok Wajib Pajak (NPWP) **</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7</td>
                                        <td>SPT tahunan PPh Orang Pribadi sesuai peraturan perundang-undangan ***</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">8</td>
                                        <td>Penghasilan tidak melebihi batas penghasilan yang ditentukan **</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">9</td>
                                        <td>Surat Pemesanan Rumah dari Pengembang yang paling sedikit memuat harga jual rumah dan alamat rumah</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">10</td>
                                        <td>Surat pernyataan Pemohon</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                            <br>

                            <p>
                                Catatan : <br>
                                * dikecualikan untuk PNS/TNI/POLRI yang pindah domisili karena kepentingan dinas dan berlaku hanya sekali. <br>
                                ** berstatus kawin hanya dipersyaratkan suami/istri. <br>
                                *** dikecualikan untuk penghasilan dibawah PTKP.

                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
