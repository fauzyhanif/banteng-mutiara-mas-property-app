<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN TIDAK BEKERJA - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>
    <body>
        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-12 col-print-12">
                            <b><u>LAMPIRAN 20</u></b>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN TIDAK BEKERJA / TIDAK MEMPUNYAI PENGHASILAN</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-12 col-print-12">
                            <p>Yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat Lahir</td>
                                        <td>: {{ $customer->couple_place_of_birth  }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tanggal Lahir</td>
                                        <td>: {{ ($customer->couple_date_of_birth == '') ? '' : GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->couple_job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->couple_address_domicile }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Nomor Telepon/HP</td>
                                        <td>: {{ $customer->couple_phone_num }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Dengan ini menyatakan bahwa selama ini <b>tidak mempunyai pekerjaan / tidak bekerja</b>. <br>
                                Demikian Surat Pernyataan ini kami buat dengan sebenar-benarnya tanpa paksaan dari pihak manapun dan
                                apabila dikemudian hari pernyataan saya tidak benar, saya bersedia mengembalikan seluruh subsidi yang saya terima.

                            </p>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-6 col-print-6 text-center"></div>
                            <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                            <div class="col-md-6 col-print-6 mb-4 text-center">Mengetahui,  <br> Kepala Kelurahan ...........................</div>
                            <div class="col-md-6 col-print-6 mb-4 text-center">Yang membuat pernyataan,</div>

                            <div class="col-md-6 col-print-6 text-center my-4"></div>
                            <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai 10.000</div>

                            <div class="col-md-6 col-print-6 text-center mt-4">(....................................................)</div>
                            <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>

                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>
</html>
