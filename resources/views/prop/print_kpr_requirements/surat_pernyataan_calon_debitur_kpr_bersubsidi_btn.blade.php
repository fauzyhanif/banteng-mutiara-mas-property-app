<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN CALON DEBITUR KPR BERSUBSIDI BTN - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 16</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light"></div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN CALON DEBITUR KPR BERSUBSIDI BTN</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Berkenaan dengan persetujuan Kredit Kepemilikan Rumah Bersubsidi di BTN (KPR, Bersubsidi BTN) yang
                                disampaikan PT. Bank Tabungan Negara (Persero) Tbk. (Bank BTN) melalui Surat Penegasan Persetujuan
                                Pemeberian Krdit (SP3K) No .................................... Tanggal .................................... Kami yang
                                bertanda tangan dibawah ini :

                            </p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Lengkap</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/Tgl lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>

                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku calon debitur.</p>

                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Lengkap</td>
                                        <td>: {{ $customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/Tgl lahir</td>
                                        <td>: {{ $customer->couple_place_of_birth  }},
                                            {{ ($customer->couple_date_of_birth == '') ? '' : GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->couple_job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->couple_address_domicile }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku suami/istri calon debitur.</p>

                            <p>Menyatakan dengan sesungguhnya:</p>
                            <ol style="margin-top: -10px">
                                <li>
                                    Telah melaksanan setiap proses permohonan KPR Bersubsidi BTN sesuai dengan ketentuan Bank BTN
                                    dan menyetujui SP3K dimaksud berdasarkan itikad baik, dalam keadaan bebas, mandiri dan tidak
                                    dibawah tekanan maupun pengaruh dari pihak lain (<i>independencv</i>).
                                </li>
                                <li>
                                    Telah menerima dan memahami dengan baik setiap penjelasan yang disampaikan Bank BTN mengenai
                                    fasilitas KPR Bersubsidi BTN, hak dan kewajiban kami sebagai Debitur serta kewajiban lainnya sesuai
                                    ketentuan peraturan perundang-undangan.
                                </li>
                                <li>
                                    Bersedia melakukan aktivasi ualng QR Code setiap tahun hinggan ke 5 (lima) sejak akad KPR Bersubsidi
                                    BTN sesuai dengan ketentuan pemerintah.
                                </li>
                                <li>
                                    Bersedia memindahkan domisili kependudukan dalam KTP ke alamat Agunan paling lambat 1 (satu)
                                    tahun sejak akad KPR Bersubsidi BTN.
                                </li>
                                <li>
                                    Seluruh dokumen persyaratan yang disampaikan kepada Bank BTN  untuk memperoleh fasilitas subsidi
                                    adalah benar dan dapat dipertanggungjawabkan keabasahannya baik secara formil maupun materil.
                                </li>
                                <li>
                                    Bersedia untuk menanggung segala biaya yang meliputi biaya asuransi , biaya pengikatan agunan, dan
                                    biaya lainnya yang timbul karena terjadinya penghentian KPR Bersubsidi BTN dan/atau
                                    perubahan/konversi menjadi KPR Non-subsidi.
                                </li>
                                <li>
                                    Tidak akan menjanjikan atau memberikan sesuatu baik secara langsung dan tidak langsung, baik atas
                                    inisiatif sendiri maupun orang lain, baik dengan menggunakan sarana elektronik atau tanpa sarana
                                    elektronik, baik dalam bentuk uang atau bukan seperti hadiah, cinderamata, komisi, pinjaman tanpa bunga,
                                    tiket perjalanan, fasilitas penginapan, perjalanan wisata, pengobatan cuma-cuma, hiburan dari
                                    fasilitas lainnya atau bentuk lainnya kepada setiap pejabat dan/atau pegawai Bank BTN termasuk
                                    anggota keluarga intinya.
                                </li>
                                <li>
                                    Apabila di kemudian hari diketahui bahwa pernyataan kami ini dan pernyataan lainnya yang kami
                                    sampaikan kepada Bank BTN tidak benar dan/atau tidak saya penuhi, maka saya bersedia
                                    mengembalikan seluruh subsidi yang telah saya terima dari pemerintah, bersedia dikenakan sanksi
                                    sesuai dengan ketentuan peraturan perundang-undangan dan bersedia mengubah/mengkonversi
                                    menjadi KPR Non-subsidi.
                                </li>
                            </ol>
                        </div>

                    </div>
                </div>
            </section>

            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 16</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>


                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Demikian surat pernyataan ini saya buat dengan sebenar-benarnya tanpa paksaan dari pihak
                                manapun.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">Menyetujui,</div>
                        <div class="col-md-6 col-print-6 text-center mb-4">Yang membuat pernyataan,</div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-12 col-print-12 text-center">Mengetahui,</div>
                        <div class="col-md-12 col-print-12 text-center mb-4">Pimpinan Tempat Bekerja/Kepala Desa/Lurah* ......................................</div>

                        <div class="col-md-12 my-4"></div>

                        <div class="col-md-12 col-print-12 text-center mt-4">(......................................)</div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 col-print-12">
                            <p>*) Coret salah yang tidak perlu</p>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
