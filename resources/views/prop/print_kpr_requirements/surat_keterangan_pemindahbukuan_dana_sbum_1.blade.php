<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT KETERANGAN PEMINDAHBUKUAN DANA SBUM - {{ $customer->name }}
        </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 10</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT KETERANGAN PEMINDAHBUKUAN DANA SBUM <br> <i>(STANDING INSTRUCTION)</i></u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Sehubungan dengan permohonan dana Subsidi Bantuan Uang Muka (SBUM) kepada Kepala Satuan Kerja Derktorat
                                Jendral Pembiayaan Perumahan Kementrian Pekerjaan Umum dan Perumahan Rakyat, maka saya yang bertanda
                                yangan dibawah ini :
                            </p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat, Tanggal Lahir</td>
                                        <td>:
                                            {{ $customer->place_of_birth  }}, {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Dengan ini memberikan kuasa kepada PT. Bank Tabungan Negara (Persero) Tbk. Kantor Cabang .............................................
                                Untuk melakukan pemindahbukuan pencairan dana Subsidi Bantuan Uang Muka (SBUM) senilai
                                Rp ............................................., - .......................................................................................... untuk digunakan sebagai pengurangan pokok
                                kredit/pembayaran kekurangan uang muka pembelian Rumah Umum Tapak *), kepada :
                            </p>

                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama Pengembang</td>
                                        <td>: PT. LAN SENA JAYA</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Nomor Rekening</td>
                                        <td>: 00181-01-30-666-666-1</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Rekening Atas Nama</td>
                                        <td>: PT. LAN SENA JAYA</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pada Bank</td>
                                        <td>: Bank BTN Kantor Cabang/Kantor Cabang/Kantor Kas ..................................................................</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Demikian <i>Standing Instruction</i> ini dibuat tanpa adanya paksaan dari pihak manapun. Akibat apapun yang mungkin
                                timbul dari paksaan penyaluran dana oleh PT. Bank Tabungan Negara (Persero) Tbk. Berdasarkan <i>Standing Instruction</i>
                                ini adalah sepenuhnya menjadi tanggung jawab saya pribadi.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">
                            Menyetujui <br>
                            PT. BANK TABUNGAN NEGARA (Persero) Tbk <br>
                            Kantor Cabang ........................
                        </div>
                        <div class="col-md-6 col-print-6 text-center mb-4">Kota/Kabupaten, tanggal bulan tahun,</div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                        <div class="col-md-6 col-print-6 text-center mt-4">(............................................................)</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                        <div class="col-md-6 col-print-6 text-center text-secondary"><i>Nama Lengkap, jabatan, Stempel</i></div>
                        <div class="col-md-6 col-print-6 text-center text-secondary"><i>Nama Lengkap Pembuat SI</i></div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
