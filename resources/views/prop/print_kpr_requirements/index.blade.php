@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('prop.customer') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Customer
            </a>
        </li>
        <li class="breadcrumb-item active">Print Persyaratan KPR</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Data Customer

                        <div class="card-tools">
                            <a href="{{ route('prop.customer.form_update', $customer->sdm_id) }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-edit"></i> &nbsp;
                                Ubah Data Customer
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="font-weight-bold">Data Konsumen</p>
                                <table class="table table-sm table-bordered">
                                    <tbody>
                                        <tr>
                                            <td class="text-secondary" width="25%">Nama</td>
                                            <td>{{ $customer->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Tempat/Tgl Lahir</td>
                                            <td>{{ $customer->place_of_birth . ', ' . GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">No Telpon</td>
                                            <td>{{ $customer->phone_num }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Alamat KTP</td>
                                            <td>{{ $customer->address }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Alamat Domisili/Kantor</td>
                                            <td>{{ $customer->address_domicile }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Pekerjaan</td>
                                            <td>{{ $customer->job }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Institusi</td>
                                            <td>{{ $customer->institute }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Penghasilan per Bulan</td>
                                            <td>{{ GeneralHelper::rupiah($customer->income) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="font-weight-bold">Data Pasangan Konsumen</p>
                                <table class="table table-sm table-bordered">
                                    <tbody>
                                        <tr>
                                            <td class="text-secondary" width="25%">Nama</td>
                                            <td>{{ $customer->couple }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">NIK</td>
                                            <td>{{ $customer->couple_nik }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Tempat/Tgl Lahir</td>
                                            <td>{{ $customer->couple_place_of_birth . ', ' . GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Alamat Domisili/Kantor</td>
                                            <td>{{ $customer->couple_address_domicile }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-secondary" width="25%">Pekerjaan</td>
                                            <td>{{ $customer->couple_job }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Print Lampiran
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-sm table-bordered table-hover">
                            <thead class="bg-info">
                                <th width="15%">No Lampiran</th>
                                <th>Keterangan</th>
                                <th width="10%">Cetak</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>SURAT PERNYATAAN PENYERAHAN DATA</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/1') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>SURAT PERNYATAAN PENGHUNINAN RUMAH UMUM BERSUBSIDI</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/2') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>SURAT KUASA PENDEBATAN DANA</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/3') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4.</td>
                                    <td>BERITA ACARA SERAH TERIMA RUMAH SEJAHTERA TAPAK</td>
                                    <td>
                                        <button class="btn btn-sm bg-purple" onclick="show_list_unit('4', 'BERITA ACARA SERAH TERIMA RUMAH SEJAHTERA TAPAK', '{{ $customer->sdm_id }}')">
                                            <i class="fas fa-print"></i> Print
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5.</td>
                                    <td>SURAT PERNYATAAN PENYERAHAN SPT PPH</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/5') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6.</td>
                                    <td>SURAT PERNYATAAN PERSETUJUAN PENYALURAN KPR BERSUBSIDI TANPA MENGGUNAKAN SBUM</td>
                                    <td>
                                        <button class="btn btn-sm bg-purple"
                                            onclick="show_list_unit('6', 'SURAT PERNYATAAN PERSETUJUAN PENYALURAN KPR BERSUBSIDI TANPA MENGGUNAKAN SBUM', '{{ $customer->sdm_id }}')">
                                            <i class="fas fa-print"></i> Print
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7.</td>
                                    <td>SURAT PERSYARATAN KELOMPOK SASARAN</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/7') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8.</td>
                                    <td>SURAT PERMOHONAN SUBSIDI BANTUAN UANG MUKA (SBUM)</td>
                                    <td>
                                        <button class="btn btn-sm bg-purple"
                                            onclick="show_list_unit('8', 'SURAT PERMOHONAN SUBSIDI BANTUAN UANG MUKA (SBUM)', '{{ $customer->sdm_id }}')">
                                            <i class="fas fa-print"></i> Print
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9.</td>
                                    <td>SURAT PENGAKUAN KEKURANGAN BAYAR UANG MUKA</td>
                                    <td>
                                        <button class="btn btn-sm bg-purple"
                                            onclick="show_list_unit('9', 'SURAT PENGAKUAN KEKURANGAN BAYAR UANG MUKA', '{{ $customer->sdm_id }}')">
                                            <i class="fas fa-print"></i> Print
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10.</td>
                                    <td>SURAT KETERANGAN PEMINDAHBUKUAN DANA SBUM</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/10') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11.</td>
                                    <td>SURAT PERNYATAAN PENYELESAIAN PRASARANA, SARANA & UTILITAS PERUMAHAN </td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/11') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>12.</td>
                                    <td>SURAT KUASA 1</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/12') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>13.</td>
                                    <td>SURAT PERNYATAAN PRASARANA, SARANA & UTILITAS PERUMAHAN </td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/13') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>14.</td>
                                    <td>SURAT PERNYATAAN PEMOHON KPR BERSUBSIDI BTN (Format Internal Bank)</td>
                                    <td>
                                        <button class="btn btn-sm bg-purple"
                                            onclick="show_list_unit('14', 'SURAT PERNYATAAN PEMOHON KPR BERSUBSIDI BTN (Format Internal Bank)', '{{ $customer->sdm_id }}')">
                                            <i class="fas fa-print"></i> Print
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>15.</td>
                                    <td>SURAT PERNYATAAN PEMOHON KPR BERSUBSIDI BTN (Format Kementrian PUPR)</td>
                                    <td>
                                        <button class="btn btn-sm bg-purple"
                                            onclick="show_list_unit('15', 'SURAT PERNYATAAN PEMOHON KPR BERSUBSIDI BTN (Format Kementrian PUPR)', '{{ $customer->sdm_id }}')">
                                            <i class="fas fa-print"></i> Print
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16.</td>
                                    <td>SURAT PERNYATAAN CALON DEBITUR KPR BERSUBSIDI BTN</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/16') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>17.</td>
                                    <td>SURAT KUASA 2</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/17') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>18.</td>
                                    <td>SURAT KETERANGAN PEMINDAHBUKUAN DANA SBUM</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/18') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>19.</td>
                                    <td>SURAT PERNYATAAN TIDAK MEMILIKI RUMAH</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/19') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>20.</td>
                                    <td>SURAT PERNYATAAN TIDAK BEKERJA</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/20') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>21.</td>
                                    <td>KETETAPAN WAKTU UNTUK VERIFIKASI</td>
                                    <td>
                                        <a href="{{ url('prop/print_kpr_requirements/'.$customer->sdm_id.'/print/21') }}" class="btn btn-sm bg-purple" target="_blank">
                                            <i class="fas fa-print"></i> Print
                                        </a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-list-unit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cetak Persyaratan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <h5 class="mb-3">
                    LAMPIRAN : <span id="judul-lampiran" class="text-info"></span>
                </h5>

                <input type="hidden" name="no-lampiran">

                <table class="table table-sm table-bordered table-hover">
                    <thead class="bg-info">
                        <th width="5%">No</th>
                        <th>Pembelian</th>
                        <th>Perumahan</th>
                        <th>Type Unit</th>
                        <th>Blok & No</th>
                        <th width="10%">Cetak</th>
                    </thead>
                    <tbody>
                        @php $no = 1; @endphp
                        @foreach ($sales as $sale)
                            <tr>
                                <td>{{ $no }}.</td>
                                <td>
                                    @if ($sale->sales_type == 'CASH')
                                        <span for="" class="badge badge-success">CASH</span>
                                    @else
                                        <span for="" class="badge badge-info">KPR</span>
                                    @endif
                                </td>
                                <td>{{ $sale->location->name }}</td>
                                <td>{{ $sale->unit->unitType->name }}</td>
                                <td>{{ $sale->block->name }} No {{ $sale->unit_id }}</td>
                                <td>
                                    <button type="button" class="btn btn-sm bg-purple" onclick="print('{{ $sale->id }}')">
                                        <i class="fas fa-print"></i> Print
                                    </button>
                                </td>
                            </tr>
                        @php $no += 1; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>


<script>
    function show_list_unit(no_lampiran, nama_lampiran, customer_id) {
        $('#modal-list-unit').modal('show');
        $('#judul-lampiran').text(nama_lampiran);
        $('input[name="no-lampiran"]').val(no_lampiran);
    }

    function print(sales_id) {
        var no_lampiran = $('input[name="no-lampiran"]').val();
        var customer_id = '{{ $customer->sdm_id }}';
        var url = {!! json_encode(url('/prop/print_kpr_requirements')) !!} + '/' + customer_id + '/print/' + no_lampiran + '/' + sales_id;
        window.open(url, '_blank');
    }
</script>
@endsection
