<!DOCTYPE html>
<html lang="en">

    @php
        $uang_muka = 0;
        $uang_muka_terbayar = 0;
        $uang_muka_belum_terbayar = 0;
        if ($sale->kpr_credit_acc == '0') {
            $uang_muka = $sale->ttl_trnsct - $sale->kpr_credit_pengajuan;
            $uang_muka_terbayar = $sale->ttl_trnsct_paid;
            $uang_muka_belum_terbayar = ($sale->ttl_trnsct - $sale->kpr_credit_pengajuan) - $sale->ttl_trnsct_paid;
        } else {
            $uang_muka = $sale->ttl_trnsct - $sale->kpr_credit_acc;
            $uang_muka_terbayar = $sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid;
            $uang_muka_belum_terbayar = ($sale->ttl_trnsct - $sale->kpr_credit_acc) - ($sale->ttl_trnsct_paid - $sale->kpr_credit_acc_paid);
        }
    @endphp

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PENGAKUAN KEKURANGAN BAYAR UANG MUKA - {{ $customer->name }}
        </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 9</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PENGAKUAN KEKURANGAN BAYAR UANG MUKA</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>Saya, yang bertanda tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat, Tanggal Lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Dengan ini menyatakan bahwa saya telah melakukan pembayaran uang muka sebesar Rp ....................................... (.............................................................. rupiah)
                                dan masih meliliki kekurangan bayar uang muka sebesar Rp ....................................... (.............................................................. rupiah)
                                untuk pembelian rumah sejahtera tapak kepada :
                            </p>

                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: PT. LAN SENA JAYA</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat Rumah Yang Dibeli</td>
                                        <td>: {{ $sale->location->address }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Harga Jual Rumah</td>
                                        <td>: Rp </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Besaran Uang Muka</td>
                                        <td>: Rp </td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Bank Pelaksana</td>
                                        <td>: {{ $sale->bank->name }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                                Demikian kami sampaikan, atas perhatiannya kami ucapkan terima kasih.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta,.........................................</div>

                        <div class="col-md-6 col-print-6 text-center">Menyetujui,</div>
                        <div class="col-md-6 col-print-6 text-center">Pemohon,</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">(Jabatan yang mewakili pengembang)</div>
                        <div class="col-md-6 col-print-6 text-center mb-4"></div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                        <div class="col-md-6 col-print-6 text-center mt-4">(..................................)</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 col-print-12">
                            <p>*) diisi dengan nama direktur atau yang mewakili pengemban dan nama perusahaan/pengembang</p>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
