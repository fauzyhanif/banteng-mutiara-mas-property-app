<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PRASARANA, SARANA & UTILITAS PERUMAHAN - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>
        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 13</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN <br> PRASARANA, SARANA & UTILITAS PERUMAHAN</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>Saya, yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku calon debitur.</p>

                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width='35%'>Nama</td>
                                        <td>: {{ $customer->couple }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Tempat/tgl lahir</td>
                                        <td>: {{ $customer->couple_place_of_birth  }},
                                            {{ ($customer->couple_date_of_birth == '') ? '' : GeneralHelper::konversiTgl($customer->couple_date_of_birth, 'ttd') }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->couple_job }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>NIK</td>
                                        <td>: {{ $customer->couple_nik }}</td>
                                    </tr>
                                    <tr>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->couple_address_domicile }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px">Selaku suami/istri pemohon.</p>

                            <p>Menyatakan hal-hal sebagai berikut:</p>
                            <ol style="margin-top: -10px">
                                <li>
                                    Saya telah mempertimbangkan dengan baik dan tanpa paksaan dari pihak manapun sebelum
                                    memutuskan untuk membeli 1 (satu) unit Rumah Sejahtera Tapak/Satuan Rumah Sejahtera
                                    Susun*) dari pengembang/developer <b>PT. LAN SENA JAYA</b>
                                </li>
                                <li>
                                    Saya telah mengetahui dan bersedia menerima kondisi Rumah Sejahtera Tapak/Satuan Rumah Sejahtera Susun*) beserta dengan kondisi Prasarana, Sarana & Utilitas (PSU)
                                    dengan rincian sebagai berikut:
                                    <ul>
                                        <li>Telah ada bukti pembayaran biaya penyambunga listrik dari PLN.</li>
                                        <li>Telah tersedia sumber air yang berfungsi.</li>
                                        <li>Badan jalan telah dilakukan pengerasan.</li>
                                        <li>Saluran/drainase lingkungan telah tergali.</li>
                                    </ul>
                                </li>
                                <li>
                                    Saya tidak akan mengkaitkan kondisi Prasarana, Sarana & Utilitas (PSU) dengan
                                    kewajiban pembayaran angsuran KPR BTN Bersubsidi.*)
                                </li>
                            </ol>

                            <p>
                                Demikian surat pernyataan ini saya buat dengan sebenar-benarnya tanpa paksaan dari pihak manapun.
                            </p>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-6 col-print-6 text-center"></div>
                            <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                            <div class="col-md-6 col-print-6 text-center mb-4">Menyetujui,</div>
                            <div class="col-md-6 col-print-6 text-center mb-4">Yang membuat pernyataan,</div>

                            <br>
                            <br>
                            <div class="col-md-6 col-print-6 text-center my-4"></div>
                            <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>
                            <br>
                            <br>

                            <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->couple }})</div>
                            <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>

                        </div>

                        <div class="row mt-4">
                            <div class="col-md-12 col-print-12">
                                <p>*) Pilih salah satu</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
