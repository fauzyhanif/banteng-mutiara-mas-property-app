<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT PERNYATAAN PENYELESAIAN PRASARANA, SARANA & UTILITAS PERUMAHAN - {{ $customer->name }}
        </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 11</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT PERNYATAAN PENYELESAIAN <br> PRASARANA, SARANA & UTILITAS PERUMAHAN</i></u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Yang bertanda tangan dibawah ini:
                            </p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width='25%'>Nama</td>
                                        <td widt="3%" class="text-right">:</td>
                                        <td>ALAN SUHERLAN</td>
                                    </tr>
                                    <tr>
                                        <td width='25%'>No. KTP</td>
                                        <td widt="3%" class="text-right">:</td>
                                        <td>3214120810690001</td>
                                    </tr>
                                    <tr>
                                        <td width='25%'>Alamat Kantor/Telp</td>
                                        <td widt="3%" class="text-right">:</td>
                                        <td>Perumahan Benteng Mutiara Mas Ruko No. 16 Kp babakan Situ 04/02 / 0264-8308460</td>
                                    </tr>
                                    <tr>
                                        <td width='25%'>Jabatan</td>
                                        <td widt="3%" class="text-right">:</td>
                                        <td>
                                            Direktur Utama yang mewakili PT LAN SENA JAYA <br>
                                            selaku pengembang pada proyek perumahan Benteng Mutiara Mas
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                            <p>
                                Menyatakan hal-hal sebagai berikut:
                            </p>

                            <ol style="margin-top: -10px">
                                <li>
                                    Bahwa rumah sejahtera yang dijual oleh PT. LAN SENA JAYA dan diserah terimakan kepada debitur Bank
                                    BTN pada saat akad kredit adalah dalam kondisi siap huni dan telah memenuhi persyaratan teknis
                                    keselamatan, keamanan dan kehandalan bangunan sesuai dengan ketentuan Pemerintah yang berlaku.
                                </li>
                                <li>
                                    Bahwa pada saat surat pernyataan ini ditandatangani, PT LAN SENA JATA telah menyerahkan bukti
                                    pembayaran biaya penyambungan listrik dari PLN dan jalan lingkungan telah dilakukan perkerasan badan
                                    jalan dan berfungsi.
                                </li>
                                <li>
                                    Bahwa PT LAN SENA JAYA bersedia menyelesaikan jalan lingkungan paling lambat 3 (tiga) bulan sejak
                                    perjanjian kredit/akad pembayaran KPR Bersubsidi.
                                </li>
                                <li>
                                    Bahwa PT LAN SENA JAYA bersedia menyediakan dana jaminan kepada Bank BTN berupa dana yang
                                    ditahan (dana retensi) dengan rincian sebagai berikut :
                                    <ol>
                                        <li>
                                            Dana yang ditahan untuk setiap debit/unit rumah, berjumlah paling sedikit 2 (dua) kali nilai jalan
                                            lingkungan yang belum terselesaikan.
                                        </li>
                                        <li>
                                            Nilai jalan lingkungan adalah berdasarkan penilaian <i>(appraisal) Bank BTN.</i>
                                        </li>
                                        <li>
                                            Dana yang ditahan diambil dari hasil setiap pencairan KPR Bersubsidi untuk setiap debit/unit rumah
                                            yang jalan lingkungan yang belum terselesaikan.
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    Dalam hal PT. LAN SENA JAYA tidak dapat menyelesaikan kewajiban sebagaimana dimaksud butir 3 di
                                    atas maka bersedia dan menyetujui dana jaminan sebagaimana dimaksud butir 4 di atas digunakan oleh
                                    Bank BTN untuk memastikan kewajiban penyelesaian jalan lingkungan dengan sesuai dengan ketentuan
                                    Pemerintah yang berlaku.
                                </li>
                            </ol>

                            <p>
                                Surat pernyataan ini adalah bagian yang tidak terpisahkan dari Perjanjian Kerjasama (PKS) dengan Bank BTN
                                Kantor Cabang ................................ tentang Penyediaan Dukungan KPR BTN Bersubsidi Nomor
                                ........................................................... tanggal ................................
                            </p>

                            <p>
                                Demikian surat pernyataan ini dibuat dengan sebenarnya tanpa paksaan dari pihak manapun dan apabila di
                                kemudian hari pernyataan ini tidak benar, maka bersedia menerima konsekuensi sesuai dengan ketentuan
                                Pemerintah dan Perundang-undangan yang berlaku.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-3"></div>
                        <div class="col-md-6 col-print-6 text-center mb-3">Yang membuat pernyataan,</div>

                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>

                        <div class="col-md-6 col-print-6 text-center mt-4"></div>
                        <div class="col-md-6 col-print-6 text-center mt-4"><u>ALAN SUHERLAN</u></div>
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Direktur
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
