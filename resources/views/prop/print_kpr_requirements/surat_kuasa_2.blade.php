<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>SURAT KUASA - {{ $customer->name }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('Component.css_print')
    </head>

    <body>

        <body class="A4" onload="window.print()">
            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 17</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light"></div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-lg-12 col-print-12 text-center">
                            <h5><b><u>SURAT KUASA</u></b></h5>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>Yang bertanda-tangan di bawah ini :</p>
                            <table class="table table-borderless table-sm" style="margin-top: -10px">
                                <tbody>
                                    <tr>
                                        <td width="5%">I.</td>
                                        <td width='35%'>Nama Lengkap</td>
                                        <td>: {{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>No KTP</td>
                                        <td>: {{ $customer->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>Tempat/Tgl lahir</td>
                                        <td>:
                                            {{ $customer->place_of_birth  }},
                                            {{ ($customer->date_ofbirth == '') ? '' : GeneralHelper::konversiTgl($customer->date_ofbirth, 'ttd') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>Pekerjaan</td>
                                        <td>: {{ $customer->job }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>Alamat</td>
                                        <td>: {{ $customer->address }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>Nomor Rekening Simpanan</td>
                                        <td>: {{ $customer->rek_number }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>No. SP3K</td>
                                        <td>: </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>Tanggal Akad KPR Bersubsidi</td>
                                        <td>: </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width='35%'>No Rekening KPR Bersubsidi</td>
                                        <td>: </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: -10px; margin-left: 35px;">Dalam hal ini bertindak untuk atas nama sendiri, selanjutnya disebut <b>"Pemberi Kuasa"</b>.</p>

                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width='5%'>II.</td>
                                        <td>
                                            PT. Bank Tabungan Negara (Persero) tbk. (Bank BTN), berkedudukan di Jalan Gajah Mada No. 01 Jakarta
                                            Pusat yang dalam hal ini diwakili oleh ................................ selaku ................................ pada Bank BTN
                                            Kantor Cabang ................................. Selajutnya disebuh <b>"Penerima Kuasa".</b>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                            <p>
                                Dengan ini <b>Pemberi Kuasa</b> memberikan <b>Kuasa khusus</b> kepada <b>Penerima Kuasa</b> untuk melakukan hal-hal
                                sebagai berikut:
                            </p>
                            <ol>
                                <li>
                                    Membayarkan sejumlah dana kepada Penjual/Pengembang dari hasil pencairan kredit yang diterima
                                    oleh <b>Pemberi Kuasa</b> dari Bank BTN untuk pembayaran lunas harga jual rumah beserta lahan sesuai
                                    dengan tujuan pemberian kredit.
                                </li>
                                <li>
                                    Melakukan pemindahbukuan pencairan dana Subsidi Bantuan Uang Muka (SBUM)/Dana Bantuan
                                    Pembiayaan Perumahan Berbasis Tabungan (BP2BT)* dari rekening simpanan milik <b>Pemberi Kuasa</b> di
                                    Bank BTN sebagaimana tersebut diatas senilai Rp ................................. - (.................................)
                                    untuk digunakan sebagai pengurang pokok kredit/pembayaran kekurangan uang muka pembelian
                                    Rumah Umum Tapak dalam hal Pemeberi Kuasa mendapatkan fasilitas SBUM/Dana BP2BT*.
                                </li>
                                <li>
                                    Pembayaran dan pemindahbukuan dana sebagaimana dimaksud bada butir 1 dan butir 2 ditujukan kepada :
                                    <br>
                                    <table class="table table-borderless table-sm">
                                        <tbody>
                                            <tr>
                                                <td width="25%">Nama Pengembang</td>
                                                <td>: PT. LAN SENA JAYA</td>
                                            </tr>
                                            <tr>
                                                <td width="25%">Nomor Rekening</td>
                                                <td>: 00181-01-30-666-666-1</td>
                                            </tr>
                                            <tr>
                                                <td width="25%">Rekening Atas Nama</td>
                                                <td>: PT. LAN SENA JAYA</td>
                                            </tr>
                                            <tr>
                                                <td width="25%">Pada Bank</td>
                                                <td>: Bank BTN Kantor Cabang/Kantor Cabang Pembantu/Kantor Kas* ...................................</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    Memblokir, mendebat dan/atau memindahbukuan dana rekening dari rekening simpanan milik <b>Pemberi Kuasa</b>
                                    di Bank BTN sebagaimana tersebut di atas untuk keperluan pembayaran:
                                    <br>
                                    a. Biaya proses dan/atau realisasi kredit; <br>
                                    b. Angsuran kredit yang meliputi pokok, bunga, denda, dan biaya lainnya; dan <br>
                                    c. Biaya asuransi, pengikatan angunan, dan biaya lainya yang timbul karna terjadinya penghentian KP4
                                    Bersubsidi BTN dan/atau perubahan/konversi menjadi KPR Non-subsidi.
                                </li>
                                <li>
                                    Pembayaran, pemindahbukuan, pemblokiran, dan/atau pendebatan dana sebagaimana dimaksud pada
                                    butir 1 s.d butir 4 diatas dapat dilakukan oleh Bank BTN secara manual, otomatis dan/atau mekanisme transaksi lainnya yang berlaku di Bank BTN.
                                </li>
                            </ol>
                        </div>

                    </div>
                </div>
            </section>

            <section class="sheet padding-10mm content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-6 col-print-6">
                            <b><u>LAMPIRAN 17</u></b>
                        </div>
                        <div class="col-lg-6 col-print-6 text-right font-weight-light">
                        </div>
                    </div>


                    <div class="row mt-4">
                        <div class="col-lg-12 col-print-12">
                            <p>
                                Demikian Surat Kuasa ini dibuat dengan Hak Substitusi, dan tidak dapat dicabut kembali seta tidak akan
                                berakhir karena sebab-sebab yang tercantum dalam Pasal 1813 Kitab Undang-Undang Hukum Perdata atau karena sebab apaupun juga.
                            </p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6 col-print-6 text-center"></div>
                        <div class="col-md-6 col-print-6 text-center">Purwakarta, .........................................</div>

                        <div class="col-md-6 col-print-6 text-center mb-4">
                            PENERIMA KUASA, <br>
                            PT BANK TABUNGAN NEGARA (PERSERO) Tbk. <br>
                            KANTOR CABANG ................................
                        </div>
                        <div class="col-md-6 col-print-6 text-center mb-4">PEMBERI KUASA,</div>

                        <br>
                        <br>
                        <div class="col-md-6 col-print-6 text-center my-4"></div>
                        <div class="col-md-6 col-print-6 text-center my-4 text-secondary">Materai secukupnya</div>
                        <br>
                        <br>

                        <div class="col-md-6 col-print-6 text-center mt-4">(..............................................)</div>
                        <div class="col-md-6 col-print-6 text-center mt-4">({{ $customer->name }})</div>
                    </div>


                    <div class="row mt-4">
                        <div class="col-md-12 col-print-12">
                            <p>*) Coret yang tidak perlu</p>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </body>

</html>
