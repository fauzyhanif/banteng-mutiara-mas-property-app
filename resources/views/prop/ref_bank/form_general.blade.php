<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input type="text" name="name" class="form-control" required @isset($data) value="{{ $data->name }}"
        @endisset>
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input type="text" name="phone_num" class="form-control" required @isset($data) value="{{ $data->phone_num }}"
        @endisset>
</div>

<div class="form-group">
    <label>Alamat Bank <span class="text-red">*</span></label>
    <textarea name="address" class="form-control" required>@isset($data){{ $data->address }}@endisset</textarea>
</div>
