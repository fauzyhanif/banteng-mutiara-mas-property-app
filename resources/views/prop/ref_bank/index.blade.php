@extends('index')

@section('content')
<section class="content-header">
    <h1>Bank</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Bank
                        <div class="card-tools">
                            <a href="{{ route('prop.bank.form_store') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Bank Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Nama Bank</th>
                                <th width="25%">No Telpon</th>
                                <th width="25%">Alamat</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('prop.ref_bank.asset.js')
@endsection

