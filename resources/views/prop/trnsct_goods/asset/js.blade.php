<script>
function showBlocks(id) {
    loadComponent('{{ url('prop/component/slct_block?location_id=') }}'+id, 'select[name="block_id"]');
}

function showItem(id) {
    var url = '{{ url('prop/trnsct_goods/view_list_item') }}' + '/' + id;
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function (res) {
            $('.view-list-item').html(res);
        },
    });
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var purchase_id = $('input[name="trnsct_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'update') {
                    form.trigger("reset");
                }

                if (res.status == 'success') {
                    toastr.success(res.text)
                    showItem(purchase_id);
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="delete_item"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var purchase_id = $('input[name="trnsct_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text)
                    showItem(purchase_id);
                    $('#confirm-delete-item').modal('hide');
                } else {
                    toastr.error(res.text)
                    $('#confirm-delete-item').modal('hide');
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function confirmDeleteItem(id) {
    var modal = $('#confirm-delete-item').modal('show');
    modal.find('input[name="trnitem_id"]').val(id)
}

</script>
