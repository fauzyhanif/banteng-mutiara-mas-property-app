@extends('index')
@section('content')

@php
    $type_name = ($type == 'IN') ? 'Masuk' : 'Keluar';
@endphp

<section class="content-header">
    <h1>Barang {{ $type_name }}</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Barang {{ $type_name }}
                        <div class="card-tools">
                            <a href="{{ url('prop/trnsct_goods/form_store?type=' . $type) }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Buat Data Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>
                                    @if ($type == 'IN')
                                        Dibeli Dari
                                    @else
                                        Diambil Oleh
                                    @endif
                                </th>
                                <th>Catatan</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(url('/prop/trnsct_goods/list_data?type='.$type)) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'trnsct_date', name: 'trnsct_date', searchable: true },
            { data: 'related_person', name: 'related_person', searchable: true },
            { data: 'note', name: 'note', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@include('prop.trnsct_goods.asset.js')
@endsection

