@php $no = 1 @endphp
@foreach ($datas as $data)
    <tr>
        <td>{{ $no }}</td>
        <td>{{ $data->goods->name }}</td>
        <td>{{ $data->qty }} {{ $data->goods->unit }}</td>
        <td>
            <button type="button" class="btn btn-xs btn-danger" onclick="confirmDeleteItem('{{ $data->trnitem_id }}')">
                <i class="fas fa-trash"></i>
            </button>
        </td>
    </tr>
@php $no += 1 @endphp
@endforeach
