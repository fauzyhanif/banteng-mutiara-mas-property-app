@extends('index')
@section('content')

@php
    $type_name = ($type == 'IN') ? 'Masuk' : 'Keluar';
@endphp

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
           <a href="{{ url('/prop/trnsct_goods?type='.$type) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Barang {{ $type_name }}
            </a>
        </li>
        <li class="breadcrumb-item active">Update Barang {{ $type_name }}</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="form-add" action="{{ route('prop.trnsct_goods.update', $data->trnsct_id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="update">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('prop.trnsct_goods.form_general')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary">
                                Simpan - selanjutnya input barang
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    loadComponent('{{ url('prop/component/slct_store?id='.$data->buy_from) }}', 'select[name="buy_from"]');
    loadComponent('{{ url('prop/component/slct_mandor?id='.$data->mandor_id) }}', 'select[name="mandor_id"]');
});
</script>
@include('prop.trnsct_goods.asset.js')
@endsection
