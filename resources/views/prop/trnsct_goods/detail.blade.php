@extends('index')
@section('content')

@php
$type_name = ($type == 'IN') ? 'Masuk' : 'Keluar';
@endphp

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/prop/trnsct_goods?type='.$type) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Barang {{ $type_name }}
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Barang {{ $type_name }}</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Informasi Barang {{ $type_name }}</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="30%">Tanggal</td>
                                    <td>{{ GeneralHelper::konversiTgl($goods->trnsct_date, 'slash') }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Catatan</td>
                                    <td>{{ $goods->note }}</td>
                                </tr>
                                @if ($type == 'IN')
                                    <tr>
                                        <td width="30%">Dibeli dari</td>
                                        <td>{{ $goods->store->name }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td width="30%">Diambil Oleh</td>
                                        <td>{{ $goods->mandor->name }}</td>
                                    </tr>
                                @endif

                                <tr>
                                    <td width="30%">Catatan</td>
                                    <td>{{ $goods->note }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Dibuat Oleh</td>
                                    <td>{{ $goods->admin }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Item</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <th width="5%">No</th>
                                <th>Nama Barang</th>
                                <th width="15%">Qty</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @foreach ($goods_item as $item)
                                    <tr>
                                        <td>{{ $no }}.</td>
                                        <td>{{ $item->goods->name }}</td>
                                        <td>{{ $item->qty }} {{ $item->goods->unit }}</td>
                                    </tr>
                                @php $no += 1 @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
