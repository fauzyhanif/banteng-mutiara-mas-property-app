@extends('index')
@section('content')

@php
    $type_name = ($type == 'IN') ? 'Masuk' : 'Keluar';
@endphp

<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/prop/trnsct_goods?type='.$type) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Barang {{ $type_name }}
            </a>
        </li>
        <li class="breadcrumb-item active">
            <a href="{{ url('/prop/trnsct_goods/form_store?type='.$type) }}">
                Kembali ke form utama
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah item</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('prop.trnsct_goods.store_item') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="{{ $type }}">
                        <input type="hidden" name="trnsct_id" value="{{ $id }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form input barang
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama Barang <span class="text-red">*</span></label>
                                <select name="goods_id" class="form-control" required onchange="show_unit(this.value)">
                                    <option value="">** Pilih Barang</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Qty <span class="text-red">*</span></label>
                                <div class="input-group">
                                    <input type="text" name="qty" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="satuan">satuan</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-primary btn-block">
                                Simpan
                            </button>
                            <a href="{{ url('/prop/trnsct_goods?type='.$type) }}" class="btn btn-success btn-block">
                                Selesai
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Daftar Barang
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <th width="5%">No.</th>
                                <th>Nama barang</th>
                                <th width="15%">Qty</th>
                                <th width="5%">Hapus</th>
                            </thead>
                            <tbody class="view-list-item">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var trnsct_id = $('input[name="trnsct_id"]').val();
    showItem(trnsct_id);

    $('select[name="goods_id"]').select2({
        theme: 'bootstrap4',
        ajax: {
        url: "{{ url('prop/component/slct_goods?store_id=') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
            _token: CSRF_TOKEN,
            search: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
        }
    });
});

function show_unit(params) {
    params = params.split("|");
    console.log(params)
    $('#satuan').text(params[1])

}
</script>
@include('prop.trnsct_goods.form_delete_item')
@include('prop.trnsct_goods.asset.js')
@endsection
