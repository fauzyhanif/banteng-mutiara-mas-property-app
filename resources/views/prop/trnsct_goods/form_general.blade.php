<input type="hidden" name="type" value="{{ $type }}">

<div class="form-group">
    <label>Tanggal <span class="text-red">*</span></label>
    <input
        type="date"
        name="trnsct_date"
        class="form-control"
        required
        @if (isset($data))
            value="{{ $data->trnsct_date }}"
        @else
            value="{{ date('Y-m-d') }}"
        @endif
    >
</div>

@if ($type == 'IN')
    <div class="form-group">
        <label>Dibeli Dari</label>
        <select name="buy_from" class="form-control" required>
            <option value="">** Pilih Toko Bangunan</option>
        </select>
    </div>
@else
    <div class="form-group">
        <label>Diambil Oleh</label>
        <select name="mandor_id" class="form-control" required>
            <option value="">** Pilih Mandor</option>
        </select>
    </div>
@endif

<div class="form-group">
    <label>Catatan</label>
    <textarea name="note" class="form-control">@isset($data){{$data->note}}@endisset</textarea>
</div>

<div class="form-group">
    <label>Admin</label>
    <input type="text" name="admin" @if (isset($datas)) value="{{ $datas->admin }}"  @else value="{{ Session::get('auth_nama') }}"  @endif class="form-control" disabled>
</div>
