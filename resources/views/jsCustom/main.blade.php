<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

function formatRupiah(angka){
    var number_string = angka.toString(),
    sisa = number_string.length % 3,
    rupiah = number_string.substr(0, sisa),
    ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    return rupiah
}

function loadComponent(url, viewSelector) {
    $.ajax({
        type: 'GET',
        url: url,
        dataType:'html',
        success: function(response){
            $(viewSelector).html(response);
        }
    });
}

function GetFormattedDate(date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month='0' + month;
    if (day.length < 2) day='0' + day;
    return [day, month, year].join('/');
}


</script>
