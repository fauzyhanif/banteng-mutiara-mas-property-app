-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 01 Mei 2021 pada 21.22
-- Versi Server: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.2.34-18+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyamasempat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_acl`
--

CREATE TABLE `sys_ref_acl` (
  `id` int(11) NOT NULL,
  `id_module` json NOT NULL,
  `id_role` json NOT NULL,
  `id_group_menu` json DEFAULT NULL,
  `is_menu` varchar(1) NOT NULL DEFAULT 'Y',
  `icon` varchar(45) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `is_child` varchar(1) NOT NULL DEFAULT 'N',
  `parent` int(5) DEFAULT NULL,
  `method` json NOT NULL,
  `url` varchar(45) NOT NULL,
  `route` varchar(45) NOT NULL,
  `controller` varchar(45) NOT NULL,
  `function` varchar(45) NOT NULL,
  `middleware` varchar(45) DEFAULT NULL,
  `urut` int(5) DEFAULT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_ref_acl`
--

INSERT INTO `sys_ref_acl` (`id`, `id_module`, `id_role`, `id_group_menu`, `is_menu`, `icon`, `label`, `is_child`, `parent`, `method`, `url`, `route`, `controller`, `function`, `middleware`, `urut`, `aktif`, `created_at`, `updated_at`) VALUES
(1, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-th-large', 'Module', 'N', NULL, '[\"get\"]', '/system/module', 'system.module', 'ModuleController', 'index', '[\"auth\"]', 1, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(6, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-users', 'Role', 'N', NULL, '[\"GET\"]', '/system/role', 'system.role', 'RoleController', 'index', '[\"auth\"]', 2, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(9, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'N', NULL, NULL, 'N', NULL, '[\"GET\"]', '/system/role/form-add', 'system.role.form.add', 'RoleController', 'formAdd', '[\"auth\"]', NULL, 'Y', '2020-12-28 02:41:58', '2020-12-28 02:41:58'),
(11, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-user', 'User', 'N', NULL, '[\"GET\"]', '/system/user', 'system.user', 'UserController', 'index', '[\"auth\"]', 3, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(16, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-stream', 'Group Menu', 'N', NULL, '[\"GET\"]', '/system/group-menu', 'system.groupmenu', 'GroupMenuController', 'index', '[\"auth\"]', 4, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(20, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'N', NULL, NULL, 'N', NULL, '[\"POST\"]', '/system/group-menu/add-new', 'system.groupmenu.add.new', 'GroupMenuController', 'addNew', '[\"auth\"]', NULL, 'Y', '2020-12-28 02:41:58', '2020-12-28 02:41:58'),
(21, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-check-circle', 'ACL', 'N', NULL, '[\"GET\"]', '/system/acl', 'system.groupmenu', 'AclController', 'index', '[\"auth\"]', 4, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(27, '[\"2\"]', '[\"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-car', 'Kategori Kendaraan', 'N', NULL, '[\"GET\"]', '/travel/kategori-kendaraan', 'travel.kategorikendaraan', 'RefKategoriKendaraanController', 'index', 'null', 1, 'Y', '2021-01-03 19:37:44', '2021-01-03 19:39:33'),
(30, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-car-side', 'Kendaraan', 'N', NULL, '[\"GET\"]', '/travel/kendaraan', 'travel.kendaraan', 'Travel\\RefKendaraanController', 'index', '[\"auth\"]', 2, 'Y', '2021-01-04 01:03:52', '2021-01-04 01:03:52'),
(31, '[\"2\"]', '[\"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-id-card', 'Driver', 'N', NULL, '[\"GET\"]', '/travel/driver', 'travel.driver', 'Travel\\RefDriverController', 'index', 'null', 3, 'Y', '2021-01-03 19:37:44', '2021-01-03 19:39:33'),
(32, '[\"2\"]', '[\"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-id-card-alt', 'Helper', 'N', NULL, '[\"GET\"]', '/travel/helper', 'travel.driver', 'Travel\\RefHelperController', 'index', 'null', 4, 'Y', '2021-01-03 19:37:44', '2021-01-03 19:39:33'),
(33, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-users', 'Customer', 'N', NULL, '[\"GET\"]', '/travel/customer', 'travel.customer', 'Travel\\RefPelangganController', 'index', '[\"auth\"]', 5, 'Y', '2021-01-18 19:03:37', '2021-01-18 19:03:37'),
(34, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-handshake', 'Input Booking', 'N', NULL, '[\"GET\"]', '/travel/booking/search-cars-page', 'travel.booking.search.cars.page', 'Travel\\TrxBookingController', 'searchCarsPage', 'null', 1, 'Y', '2021-01-18 19:17:08', '2021-01-18 19:19:09'),
(35, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-book', 'Daftar Booking', 'N', NULL, '[\"GET\"]', '/travel/booking', 'travel.booking', 'Travel/TrxBookingController', 'index', '[\"auth\"]', 2, 'Y', '2021-01-18 20:57:44', '2021-01-18 20:57:44'),
(36, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-file-alt', 'Cetak SPJ', 'N', NULL, '[\"GET\"]', '/travel/spj', 'travel.spj.index', 'SpjController', 'index', 'null', 3, 'Y', '2021-01-28 18:46:11', '2021-01-28 18:46:41'),
(37, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-file-invoice', 'Pembayaran', 'N', NULL, '[\"GET\"]', '/travel/payment', 'travel.payment', 'Travel\\TrnsctPaymentController', 'index', 'null', 4, 'Y', '2021-01-29 05:07:26', '2021-01-29 05:08:06'),
(38, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-user-tag', 'Agen', 'N', NULL, '[\"GET\"]', '/travel/agen', 'travel.agen', 'Travel/RefAgenController', 'index', 'null', 6, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(40, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-calendar-check', 'Kalender Booking', 'N', NULL, '[\"GET\"]', '/travel/kalender-booking', 'travel.kalender.booking', 'Travel/TrxCalendarBookingController', 'index', 'null', 5, 'Y', '2021-02-02 05:58:57', '2021-02-02 06:07:15'),
(41, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-stream', 'Group Akun', 'N', NULL, '[\"GET\"]', '/travel/finance/account-group', 'travel.finance.account-group', 'PaymentAccountController', 'index', '[\"auth\"]', 1, 'Y', '2021-03-30 23:08:16', '2021-03-30 23:08:16'),
(42, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Pemasukan', 'N', NULL, '[\"GET\"]', '/travel/income', 'travel.income', 'Travel/TrnsctIncomeController', 'index', '[\"auth\"]', 3, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(43, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Pengeluaran', 'N', NULL, '[\"GET\"]', '/travel/spending', 'travel.income', 'Travel/TrnsctSpendingController', 'index', '[\"auth\"]', 4, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(44, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"6\"]', 'Y', 'fa-clipboard-list', 'Agenda Armada', 'N', NULL, '[\"GET\"]', '/travel/laporan/agenda-armada', 'travel.laporan.agenda-armada', 'ReportAgendaUnit', 'index', '[\"auth\"]', 1, 'Y', '2021-04-02 18:30:53', '2021-04-02 18:30:53'),
(45, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"6\"]', 'Y', 'fa-clipboard-list', 'Pendapatan Sewa Bus', 'N', NULL, '[\"GET\"]', '/travel/laporan/pendapatan-sewa-bus', 'travel.laporan.pendapatan-sewa-bus', 'ReportIncome', 'index', '[\"auth\"]', 1, 'Y', '2021-04-02 18:30:53', '2021-04-02 18:30:53'),
(46, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-user-tag', 'Jabatan', 'N', NULL, '[\"GET\"]', '/system/jabatan', 'system.jabatan', 'System/RefSdmPositionController', 'index', 'null', 7, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(47, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-user-tag', 'SDM', 'N', NULL, '[\"GET\"]', '/system/sdm', 'system.sdm', 'System/RefSdmPositionController', 'index', 'null', 8, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(48, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-stream', 'Akun', 'N', NULL, '[\"GET\"]', '/travel/finance/account', 'travel.finance.account', 'PaymentAccountController', 'index', '[\"auth\"]', 2, 'Y', '2021-03-30 23:08:16', '2021-03-30 23:08:16'),
(49, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-clipboard-list', 'Laba Rugi', 'N', NULL, '[\"GET\"]', '/travel/laba-rugi', 'travel.laba-rugi', 'Travel/ReportLabaRugiController', 'index', '[\"auth\"]', 5, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(50, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Terima Uang', 'N', NULL, '[\"GET\"]', '/travel/receive_money', 'travel.receive_money', 'Travel/TrnsctSpendingController', 'index', '[\"auth\"]', 4, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(51, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Kirim Uang', 'N', NULL, '[\"GET\"]', '/travel/deposit_money', 'travel.deposit_money', 'Travel/TrnsctSpendingController', 'index', '[\"auth\"]', 4, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(52, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-clipboard-list', 'Neraca', 'N', NULL, '[\"GET\"]', '/travel/neraca', 'travel.neraca', 'Travel/ReportNeracaController', 'index', '[\"auth\"]', 6, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_group_menu`
--

CREATE TABLE `sys_ref_group_menu` (
  `id_group_menu` int(11) NOT NULL,
  `id_module` json NOT NULL,
  `id_role` json NOT NULL,
  `nama` varchar(35) NOT NULL,
  `urut` int(2) DEFAULT NULL,
  `aktif` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_ref_group_menu`
--

INSERT INTO `sys_ref_group_menu` (`id_group_menu`, `id_module`, `id_role`, `nama`, `urut`, `aktif`, `created_at`, `updated_at`) VALUES
(1, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'DATA MASTER TRAVEL', 1, 'Y', '2020-12-27 02:37:10', '2020-12-28 02:37:13'),
(2, '[\"3\"]', '[\"1\", \"2\", \"4\"]', 'DATA MASTER BERAS', 1, 'Y', '2020-12-27 02:37:33', '2020-12-28 02:37:26'),
(3, '[\"1\"]', '[\"1\", \"2\"]', 'DATA SYSTEM', 1, 'Y', '2020-12-28 02:36:54', '2020-12-28 02:36:54'),
(4, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'MENU UTAMA', 2, 'Y', '2021-01-18 19:12:07', '2021-01-18 19:12:07'),
(5, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'KEUANGAN', 3, 'Y', '2021-03-30 23:00:55', '2021-03-30 23:01:26'),
(6, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'LAPORAN', 4, 'Y', '2021-04-02 18:24:55', '2021-04-02 18:24:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_module`
--

CREATE TABLE `sys_ref_module` (
  `id` int(11) NOT NULL,
  `id_module` int(5) NOT NULL,
  `image` varchar(45) DEFAULT NULL,
  `nama` varchar(45) NOT NULL,
  `deskripsi` text NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_ref_module`
--

INSERT INTO `sys_ref_module` (`id`, `id_module`, `image`, `nama`, `deskripsi`, `aktif`, `created_at`, `updated_at`) VALUES
(14, 1, '20201229013241.png', 'System Developer', 'System Developer', 'Y', '2020-12-21 21:39:23', '2020-12-28 18:32:41'),
(15, 2, '20201228091521.png', 'SIM Travel', 'Sistem Informasi Manajemen Travel', 'Y', '2020-12-21 21:39:49', '2020-12-28 02:15:21'),
(16, 3, '20201228091601.png', 'SIM Beras', 'Sistem Informasi Manajemen Beras', 'Y', '2020-12-28 02:16:01', '2020-12-28 02:16:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_role`
--

CREATE TABLE `sys_ref_role` (
  `id` int(11) NOT NULL,
  `id_role` int(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `id_module` json DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_ref_role`
--

INSERT INTO `sys_ref_role` (`id`, `id_role`, `nama`, `id_module`, `aktif`, `created_at`, `updated_at`) VALUES
(5, 1, 'Admin Developer', '[\"1\", \"2\", \"3\"]', 'Y', '2020-12-26 20:56:37', '2020-12-28 02:32:28'),
(6, 2, 'Superadmin', '[\"2\", \"3\"]', 'Y', '2020-12-26 20:56:45', '2020-12-28 02:32:39'),
(7, 3, 'Admin Travel', '[\"2\"]', 'Y', '2020-12-26 20:56:52', '2020-12-28 02:32:55'),
(8, 4, 'Admin Beras', '[\"3\"]', 'Y', '2020-12-26 20:56:59', '2020-12-28 02:33:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_salary`
--

CREATE TABLE `sys_ref_salary` (
  `salary_id` int(11) NOT NULL,
  `module_id` varchar(5) NOT NULL,
  `month` varchar(10) NOT NULL,
  `position_id` varchar(5) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `salary` int(20) NOT NULL,
  `salary_paid` int(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_sdm`
--

CREATE TABLE `sys_ref_sdm` (
  `sdm_id` int(11) NOT NULL,
  `module_id` varchar(5) NOT NULL,
  `position_id` varchar(5) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_ref_sdm`
--

INSERT INTO `sys_ref_sdm` (`sdm_id`, `module_id`, `position_id`, `nip`, `name`, `phone_num`, `address`, `active`, `created_at`, `updated_at`) VALUES
(1, '2', '02', '001', 'Hanif', '082325494207', 'legonkulon', 'N', '2021-04-03 20:46:54', '2021-04-03 20:57:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_sdm_position`
--

CREATE TABLE `sys_ref_sdm_position` (
  `module_id` varchar(10) NOT NULL,
  `position_id` varchar(5) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_ref_sdm_position`
--

INSERT INTO `sys_ref_sdm_position` (`module_id`, `position_id`, `name`, `active`, `created_at`, `updated_at`) VALUES
('2', '01', 'Direktur', 'N', '2021-04-03 20:00:59', '2021-04-03 20:17:58'),
('2', '02', 'Manajer Oprasional', 'Y', '2021-04-03 20:18:42', '2021-04-03 20:18:42'),
('2', '03', 'Bendahara', 'Y', '2021-04-03 20:18:51', '2021-04-03 20:18:51'),
('2', '04', 'Karyawan', 'Y', '2021-04-03 20:19:10', '2021-04-03 20:19:10'),
('2', '05', 'Security', 'Y', '2021-04-03 20:19:17', '2021-04-03 20:19:17'),
('2', '06', 'Montir', 'Y', '2021-04-03 20:19:26', '2021-04-03 20:19:26'),
('2', '07', 'Office Boy', 'Y', '2021-04-03 20:19:43', '2021-04-03 20:19:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_user`
--

CREATE TABLE `sys_ref_user` (
  `id` int(10) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_module` json NOT NULL,
  `id_role` json NOT NULL,
  `aktif` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT 'Y',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sys_ref_user`
--

INSERT INTO `sys_ref_user` (`id`, `nama`, `email`, `email_verified_at`, `password`, `no_telp`, `alamat`, `id_module`, `id_role`, `aktif`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Fauzy Hanif', 'admin@admin.com', NULL, '$2y$10$WhCklAwsUVDpUVcksVmLIu3NZxmNy0zcioyliZz63LY.tDGBa.Epu', '082325494207', 'Subang', '[\"1\", \"2\", \"3\"]', '[\"1\", \"2\", \"3\", \"4\"]', 'Y', 'WD7BGSZz4cStSNbeRFQoOZ1wdX6a3dhijPQldDtPlzLehi9tHGrwPOmS0jp3', '2020-12-16 21:06:19', '2020-12-28 02:35:41'),
(2, 'Admin Beras', 'beras@admin.com', NULL, '$2y$10$gj1rXzJ1/sU7Q2Yy5PAlr.Y45GMhxggPI9DFDnG6iEJAP44vVHQ4q', '08231241412', 'Subang', '[\"3\"]', '[\"4\"]', 'Y', NULL, '2020-12-26 21:15:20', '2020-12-28 02:34:46'),
(3, 'Admin Travel', 'travel@addmin.com', NULL, '$2y$10$jqkQN53vpn01kDSPxjAyzOdFFoEpCV/sH0p8CtyucCQhVb1dRZhhW', '082325421424', 'Subang', '[\"2\"]', '[\"3\"]', 'N', NULL, '2020-12-26 21:50:04', '2020-12-28 02:35:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_agent`
--

CREATE TABLE `trvl_ref_agent` (
  `agent_id` int(11) NOT NULL,
  `agent_type` enum('AGENT','BIRO') NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(25) NOT NULL,
  `address` text,
  `ttl_trnsct` int(5) DEFAULT NULL,
  `ttl_commission` int(25) DEFAULT NULL,
  `active` enum('Y','N') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_agent`
--

INSERT INTO `trvl_ref_agent` (`agent_id`, `agent_type`, `name`, `phone_num`, `address`, `ttl_trnsct`, `ttl_commission`, `active`, `created_at`, `updated_at`) VALUES
(1, 'AGENT', 'AGEN JOMBANG', '08414221442', 'JOMBANG', NULL, NULL, 'Y', '2021-03-07 20:05:35', '2021-04-03 20:22:54'),
(2, 'AGENT', 'asdsad', 'asd', 'asdsad', NULL, NULL, 'Y', '2021-04-18 21:17:49', '2021-04-18 21:17:49'),
(3, 'AGENT', 'tess', '111', '111', NULL, NULL, 'Y', '2021-04-18 21:24:58', '2021-04-18 21:24:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_booking_id`
--

CREATE TABLE `trvl_ref_booking_id` (
  `id` int(11) NOT NULL,
  `month` varchar(7) NOT NULL,
  `serial_num` int(4) UNSIGNED ZEROFILL NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car`
--

CREATE TABLE `trvl_ref_car` (
  `id` int(11) NOT NULL,
  `category_id` varchar(4) NOT NULL,
  `condition_id` int(5) DEFAULT '1',
  `police_num` varchar(10) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `owners_address` text,
  `brand` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `raft_year` varchar(4) DEFAULT NULL,
  `purchase_year` varchar(4) DEFAULT NULL,
  `machine_cc` varchar(10) DEFAULT NULL,
  `framework_num` varchar(45) DEFAULT NULL,
  `machine_num` varchar(45) DEFAULT NULL,
  `bpkb_num` varchar(45) DEFAULT NULL,
  `nominal_tax` int(22) DEFAULT NULL,
  `kir_date` varchar(12) DEFAULT NULL,
  `kps_permit_date` varchar(12) DEFAULT NULL,
  `insurance_date` varchar(12) DEFAULT NULL,
  `stnk_tax_date` varchar(12) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_car`
--

INSERT INTO `trvl_ref_car` (`id`, `category_id`, `condition_id`, `police_num`, `owner`, `owners_address`, `brand`, `type`, `color`, `raft_year`, `purchase_year`, `machine_cc`, `framework_num`, `machine_num`, `bpkb_num`, `nominal_tax`, `kir_date`, `kps_permit_date`, `insurance_date`, `stnk_tax_date`, `active`, `created_at`, `updated_at`) VALUES
(4, '01', 1, 'T 7563 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19126', 'J08EUFJ77150', 'M13674651', 1234900, NULL, NULL, NULL, '2021-04-19', 'Y', NULL, '2021-04-18 20:18:33'),
(5, '1', 1, 'T 7564 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19121', 'J08EUFJ77145', 'M13674652', 1234900, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(6, '1', 1, 'T 7566 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19124', 'J08EUFJ77148', 'M13674654', 1234900, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(7, '1', 1, 'T 7573 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2017', '2017', '7684', 'MJERK8JSKHJN20120', 'J08EUFJ85512', 'N06047673', 1238100, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(8, '1', 1, 'T 7574 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2017', '2017', '7684', 'MJERK8JSKHJN20112', 'J08EUFJ85390', 'N06047674', 1238100, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(9, '1', 1, 'T 7576 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2017', '2017', '7684', 'MJERK8JSKHJN20113', 'J08EUFJ85391', 'N06047675', 1238100, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(10, '2', 1, 'T 7545 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ / R260', 'ORANGE KOMBINASI', '2015', '2015', '7684', 'MJERK8JSKFJN18110', 'J08EUFJ73302', 'M01174561', 1800100, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(11, '2', 1, 'T 7546 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ / R260', 'ORANGE KOMBINASI', '2015', '2015', '7684', 'MJERK8JSKFJN18106', 'J08EUFJ73298', 'M01174562', 1800100, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(12, '2', 1, 'T 7547 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ / R260', 'ORANGE KOMBINASI', '2015', '2015', '7684', 'MJERK8JSKFJN18107', 'J08EUFJ73299', 'MO1174563', 1800100, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(13, '2', 1, 'T 7562 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19130', 'EUFJ77154', 'M13674650', 1234900, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(14, '2', 1, 'T 7565 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19127', 'J08EUFJ77151', 'M13674653', 1234900, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(15, '3', 1, 'T 7532 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'MITSUBISHI', 'COLT DIESEL FE 73 HD (4X2) M/T', 'ORANGE KOMBINASI', '2014', '2014', '3908', 'MHMFE84PBEJ006769', '4D34TKX6961', 'L10058381', 992200, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(16, '3', 1, 'T 7533 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'MITSUBISHI', 'COLT DIESEL FE 73 HD (4X2) M/T', 'ORANGE KOMBINASI', '2014', '2014', '3908', 'MHMFE84PBEJ006770', '4D34TKX6962', 'L10058380', 992200, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(17, '3', 1, 'T 7590 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'MITSUBISHI', 'COLT DIESEL FE 84G BC (4X2) M/T', 'ORANGE KOMBINASI', '2018', '2018', '3908', 'MHMFE84PBJJ009852', '4D34TS81796', '008011753', 906700, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(18, '3', 1, 'T 7591 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'MITSUBISHI', 'COLT DIESEL FE 84G BC (4X2) M/T', 'ORANGE KOMBINASI', '2018', '2018', '3908', 'MHMFE84PBJJ009837', '4D34TSB1804', '', 906700, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(19, '4', 1, 'T 7527 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'ISUZU', 'NHR 55 CO E2-1', 'ORANGE KOMBINASI METALIK', '2014', '2014', '2771', 'MHCNHR55EEJ056733', 'M056733', 'L00853557', 498400, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(20, '4', 1, 'T 7528 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'ISUZU', 'NHR 55 CO E2-1', 'ORANGE KOMBINASI METALIK', '2014', '2014', '2771', 'MHCNHR55EEJ056735', 'M056735', 'L00853558', 498400, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(21, '4', 1, 'T 7529 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'ISUZU', 'NHR 55 CO E2-1', 'ORANGE KOMBINASI METALIK', '2014', '2014', '2771', 'MHCNHR55EEJ056734', 'M056734', 'L00853559', 498400, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(22, '4', 1, 'T 7571 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'ISUZU', 'NKR 55 CO E2-1 LWB', 'ORANGE KOMBINASI', '2017', '2017', '2771', 'MHCNKR55HHJ074759', 'M074759', 'N05812677', 603600, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(23, '4', 1, 'T 7572 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'ISUZU', 'NKR 55 CO E2-1 LWB', 'ORANGE KOMBINASI', '2017', '2017', '2771', 'MHCNKR55HHJ074757', 'M074757', 'N05812678', 603600, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(24, '5', 1, 'T 7584 TC', 'PT KARYA MAS EMPAT', 'Jln Raya Rancaudik KM 05 Ds. Rancaudik Kec. Tambakdahan Subang', 'TOYOTA', 'HIACE COMMUTER MT', 'PUTIH', '2018', '2018', '2494', 'JTFSS22P4J0178236', '2KDA977542', '005070896', 1238400, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(25, '5', 1, 'T 7585 TC ', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Dsn. Rancaudik-Tambakdahan Subang', 'TOYOTA', 'HIACE COMMUTER MT', 'PUTIH', '2018', '2018', '2494', 'JTFSS22P3J0178390', '2KDA977980', '005070897', 1238400, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(26, '5', 1, 'T 7586 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 05 Dsn. Rancaudik - Kec. Tambakdahan Subang', 'TOYOTA', 'HIACE COMMUTER MT', 'PUTIH', '2018', '2018', '2494', 'JTFSS22P8J0178269', '2KDA977643', '005070898', 1238400, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(27, '2', 1, '080929', 'asd', NULL, 'asds', NULL, 'asdsa', NULL, NULL, '123', '123', '123', '08989', 123321, NULL, NULL, '2021-04-02', '2021-04-17', 'Y', '2021-04-04 17:49:57', '2021-04-04 17:49:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car_category`
--

CREATE TABLE `trvl_ref_car_category` (
  `id` int(11) NOT NULL,
  `category_id` varchar(4) NOT NULL,
  `name` varchar(45) NOT NULL,
  `seat` varchar(20) NOT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_car_category`
--

INSERT INTO `trvl_ref_car_category` (`id`, `category_id`, `name`, `seat`, `active`, `created_at`, `updated_at`) VALUES
(1, '1', 'Big Bus SHD', '47/59', 'Y', '2021-03-07 20:13:49', '2021-03-07 20:13:49'),
(2, '2', 'Big Bus Setra', '47/59', 'Y', '2021-03-07 20:14:03', '2021-03-07 20:14:03'),
(3, '3', 'Medium Bus', '29/31', 'Y', '2021-03-07 20:14:17', '2021-03-07 20:14:17'),
(4, '4', 'Elf Short', '15', 'Y', '2021-03-07 20:14:29', '2021-03-07 20:14:29'),
(5, '5', 'Elf Long', '19', 'Y', '2021-03-07 20:14:47', '2021-03-07 20:14:47'),
(8, '6', 'Hiace', '15', 'Y', '2021-03-07 20:14:47', '2021-03-07 20:14:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car_condition`
--

CREATE TABLE `trvl_ref_car_condition` (
  `id` int(5) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_car_condition`
--

INSERT INTO `trvl_ref_car_condition` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Baik', NULL, '2021-02-22 02:54:03'),
(2, 'Sedang Service', NULL, '2021-02-22 02:54:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car_service`
--

CREATE TABLE `trvl_ref_car_service` (
  `id` int(11) NOT NULL,
  `police_num` varchar(15) NOT NULL,
  `repair_date` varchar(10) DEFAULT NULL,
  `repair_place` varchar(45) DEFAULT NULL,
  `information` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_customer`
--

CREATE TABLE `trvl_ref_customer` (
  `cust_id` bigint(20) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(25) NOT NULL,
  `address` text,
  `ttl_trnsct` int(5) DEFAULT NULL,
  `ttl_payment` int(25) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_customer`
--

INSERT INTO `trvl_ref_customer` (`cust_id`, `name`, `phone_num`, `address`, `ttl_trnsct`, `ttl_payment`, `created_at`, `updated_at`) VALUES
(1, 'AQEELA', '082325494207', 'LEGONKULON', 1, 3800000, '2021-04-21 20:02:13', '2021-04-21 20:02:13'),
(2, 'AZIZAH', '089660500095', 'ASSUNNAH, CIREBON', 1, 10000000, '2021-04-23 18:41:42', '2021-04-23 18:41:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_driver`
--

CREATE TABLE `trvl_ref_driver` (
  `driver_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `sim_num` varchar(40) DEFAULT NULL,
  `phone_num` varchar(45) NOT NULL,
  `address` text,
  `driver_type` varchar(15) NOT NULL,
  `police_num` varchar(45) DEFAULT NULL,
  `car_permit` text,
  `ttl_trnsct` int(5) DEFAULT '0',
  `ttl_saving` int(25) DEFAULT '0',
  `active` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_driver`
--

INSERT INTO `trvl_ref_driver` (`driver_id`, `name`, `sim_num`, `phone_num`, `address`, `driver_type`, `police_num`, `car_permit`, `ttl_trnsct`, `ttl_saving`, `active`, `created_at`, `updated_at`) VALUES
(1, 'WIRTA', NULL, '082216445003', 'Dsn. Pangadangan RT. 18/05 Desa Rancasari Kec. Pamanukan Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, '2021-03-29 17:18:12'),
(2, 'ALIMIN', NULL, '085294465892', 'Dsn. Kertajaya RT. 02/13 Desa Mulyasari kec, Pamanukan Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, '2021-04-15 00:40:33'),
(3, 'DARWIN', NULL, '081324504132', 'Dsn Gardu II RT. 005/002 Ds. Gardu Mukti Kec. Tambakdahan Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, '2021-03-29 17:18:12'),
(4, 'KARYIM MULYADI', NULL, '085318965047', 'Dsn. Pangadangan RT 18/05 Desa Rancasari Kec. Pamanukan Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\",\"2\", \"3\"]', 0, 0, 'Y', NULL, '2021-04-20 23:59:38'),
(5, 'UDIN FAHRUDIN', NULL, '081224642887', 'Dsn. Jatibaru RT.003/001 Ds. Mulyasari Kec. Binong Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\",\"2\", \"3\"]', 0, 0, 'Y', NULL, '2021-04-23 18:48:18'),
(6, 'H. AMIN MUBAROK', NULL, '081310253230', 'Dsn. Sukawera RT. 019/005 Ds. Mekarjaya Kec. Compreng Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\",\"2\", \"3\"]', 0, 0, 'Y', NULL, '2021-03-28 21:17:14'),
(7, 'CEPI KURNIAWAN', NULL, '081290667082', 'Dsn. Sukatani RT. 02/01 Ds. Sukatani Kec. Compreng Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\",\"2\", \"3\"]', 0, 0, 'Y', NULL, '2021-05-01 06:28:43'),
(8, 'SOLIHIN', NULL, '081395492506', 'Kp. Pojok Tengah RT. 03/05 Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, '2021-03-27 01:13:55'),
(9, 'ASEP KUSMANA', NULL, '081395443443', 'Kp. Sukasari RT. 02/05 Desa Cikole Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(10, 'RODI', NULL, '082128598694', 'Dsn. Rawasari RT. 17/08 Desa Sukamandi Kec. Ciasem Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(11, 'EMAN', NULL, '081210910193', 'Kp. Karang Tengah RT. 002/015 Ds. Cikole Kec. Lembang Kab. Bandung', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(12, 'MOCH MUKTI D', NULL, '082110717919', 'Jl. Angkasa GG Motor No. 17 RT. 13/06 Gunung Sahari Jakarta Pusat', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(13, 'JAENUDIN', NULL, '085328864444', 'Kp. Sukamukti RT. 003/003 Ds. Situsaeur Kec. Karangpawitan Kab. Garut', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(14, 'ROHIM KUSTIAWAN', NULL, '081214934767', 'Kp. Pamecelan RT. 02/06 Desa Sukajaya Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, '2021-04-04 23:41:20'),
(15, 'AGUS SUROSO', NULL, '081295609511', 'Kp. Cinangka RT. 001/009 Ds. Margalakasana Kec. Cipeundeuy Kab. Bandung Barat', 'CADANGAN', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(16, 'MAHFUD', NULL, '082218946238', 'Dsn. Priangan RT. 008/003 Ds. Binagun Kec. Pataruman Banjar', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(17, 'AGUS SANTOSA', NULL, '081315801322', 'Kp. Haurpanggung RT. 002/004 Ds. Haurpanggung Kec. Tarogong Kidul Kab. Garut', 'CADANGAN', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, '2021-04-10 21:08:41'),
(18, 'ENGKUS KUSNADI', NULL, '081394317401', 'Bunisari Kulon RT. 001/006 Ds. Gadobangkong Kec. Ngamprah Kab. Bandung Barat', 'CADANGAN', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(19, 'EDI SUPRIYATIN', NULL, '085221447255', 'Dsn. Karangmalang RT. 009/003 Ds. Bobos Kec. Legonkulon Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(20, 'NANA', NULL, '082216825855', 'Dsn. Gardu II RT. 005/002 Ds. Gardu Mukti Kec. Tambakdahan Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(21, 'AGUS SUUD', NULL, '081324993605', 'Kp. Batureok RT.01/08 Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(22, 'NANANG BIN OCEP', NULL, '081291449589', 'Jl. Mayjen Sutiyoso No. 8 Ke. Kota Baru Bandar Lampung', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(23, 'DARYA SURYANA', NULL, '085321679939', 'Dsn. Tanjungkerta RT. 013 RW 004 Desa Gandasari Kec. Cikaum Kab. Subang', 'UTAMA', 'T 7563 TC', '[\"1\"]', 0, 0, 'Y', NULL, NULL),
(24, 'hanif', '123', '123123', 'asd', 'UTAMA', 'T 7563 TC', '[\"1\",\"2\"]', 0, 0, 'Y', '2021-04-04 17:52:09', '2021-04-04 17:52:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_driver_saving`
--

CREATE TABLE `trvl_ref_driver_saving` (
  `id` bigint(20) NOT NULL,
  `type` enum('DRIVER','HELPER') NOT NULL,
  `user_id` int(5) NOT NULL,
  `booking_id` varchar(20) DEFAULT NULL,
  `trnsct_date` varchar(10) NOT NULL,
  `information` text,
  `in` int(20) DEFAULT '0',
  `out` int(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_finance_account`
--

CREATE TABLE `trvl_ref_finance_account` (
  `id` int(11) NOT NULL,
  `group_id` varchar(10) NOT NULL,
  `is_parent` varchar(1) DEFAULT '',
  `parent` varchar(10) DEFAULT NULL,
  `account_id` varchar(25) NOT NULL,
  `name` varchar(45) NOT NULL,
  `post_report` varchar(15) DEFAULT NULL,
  `post_sub_report` varchar(35) DEFAULT NULL,
  `cash_flow_group` varchar(15) DEFAULT NULL,
  `debit` int(20) DEFAULT NULL,
  `credit` int(20) DEFAULT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_finance_account`
--

INSERT INTO `trvl_ref_finance_account` (`id`, `group_id`, `is_parent`, `parent`, `account_id`, `name`, `post_report`, `post_sub_report`, `cash_flow_group`, `debit`, `credit`, `active`, `created_at`, `updated_at`) VALUES
(1, '1-000', 'Y', NULL, '1-100', 'ASET LANCAR', 'NERACA', NULL, NULL, 0, 0, 'Y', '2021-04-06 19:54:05', '2021-04-06 19:54:05'),
(2, '1-000', 'Y', NULL, '1-200', 'ASET TETAP', 'NERACA', NULL, NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(3, '1-000', '', '1-100', '1-110', 'Kas Kantor', 'NERACA', 'AKTIVA_LANCAR', NULL, 0, 0, 'Y', '2021-04-06 20:06:32', '2021-04-06 20:06:32'),
(4, '1-000', '', '1-100', '1-120', 'Bank BRI', 'NERACA', 'AKTIVA_LANCAR', NULL, 0, 0, 'Y', '2021-04-06 20:31:15', '2021-04-06 20:31:15'),
(5, '1-000', '', '1-100', '1-130', 'Uang Muka Sewa', 'NERACA', 'AKTIVA_LANCAR', NULL, 0, 0, 'Y', '2021-04-06 20:31:34', '2021-04-06 20:31:34'),
(6, '1-000', '', '1-100', '1-140', 'Piutang Bon', 'NERACA', 'AKTIVA_LANCAR', NULL, 0, 0, 'Y', '2021-04-06 20:32:06', '2021-04-06 20:32:06'),
(7, '2-000', 'Y', NULL, '2-100', 'Kewajiban Lancar', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-06 20:38:19', '2021-04-06 20:38:19'),
(8, '3-000', '', NULL, '3-100', 'Modal', 'NERACA', 'MODAL', NULL, 0, 0, 'Y', '2021-04-06 20:38:41', '2021-04-06 20:38:41'),
(9, '2-000', '', '2-100', '2-110', 'Hutang ke H. Ade', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-06 20:39:11', '2021-04-06 20:39:11'),
(10, '2-000', '', '2-100', '2-120', 'Hutang ke Hj. Im Inayah', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-06 20:40:09', '2021-04-06 20:40:09'),
(13, '3-000', '', NULL, '3-200', 'Laba Bersih', 'NERACA', 'MODAL', NULL, 0, 0, 'Y', '2021-04-06 20:41:50', '2021-04-06 20:41:50'),
(14, '4-000', '', NULL, '4-100', 'Pendapatan Sewa', 'LABARUGI', 'PENDAPATAN', NULL, 0, 0, 'Y', '2021-04-06 20:42:05', '2021-04-06 20:42:05'),
(15, '5-000', '', NULL, '5-100', 'HPP Perjalanan', 'LABARUGI', 'HPP', NULL, 0, 0, 'Y', '2021-04-06 20:42:32', '2021-04-06 20:42:32'),
(16, '6-000', '', NULL, '6-011', 'Gaji', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:43:55', '2021-04-06 20:44:06'),
(17, '6-000', '', NULL, '6-012', 'Listrik, Wifi & Kuota', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:44:49', '2021-04-06 20:44:49'),
(18, '6-000', '', NULL, '6-013', 'Makanan & Minuman', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(19, '6-000', '', NULL, '6-014', 'ATK & Printing', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(20, '6-000', '', NULL, '6-015', 'Perlengkapan Mobil', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(21, '6-000', '', NULL, '6-016', 'Pengurusan Surat Mobil', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(22, '6-000', '', NULL, '6-017', 'Service Mobil', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(23, '6-000', '', NULL, '6-018', 'Perizinan & Legal', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(24, '6-000', '', NULL, '6-019', 'Pajak', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:47:26'),
(25, '7-000', '', NULL, '7-100', 'Pendapatan Lainnya', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:20', '2021-04-07 19:42:20'),
(26, '7-000', '', NULL, '7-200', 'Fee Paket Biro', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(37, '8-000', '', NULL, '8-001', 'Admin Bank', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(38, '8-000', '', NULL, '8-002', 'Langganan Software', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(39, '8-000', '', NULL, '8-003', 'Tunjangan Hari raya', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(40, '8-000', '', NULL, '8-004', 'Upah Kerja', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(41, '1-000', '', '1-200', '1-210', 'Inventaris Kantor', 'NERACA', 'AKTIVA_TETAP', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(42, '1-000', '', '1-200', '1-220', 'Kendaraan', 'NERACA', 'AKTIVA_TETAP', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(43, '1-000', '', '1-200', '1-230', 'Tanah & Bangunan', 'NERACA', 'AKTIVA_TETAP', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(44, '1-000', '', '', '1-800', 'Depresiasi', 'NERACA', 'DEPRESIASI', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(45, '7-000', '', '7-100', '7-300', 'BUNGA BANK', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-17 00:20:54', '2021-04-17 00:20:54'),
(46, '2-000', '', '2-100', '2-130', 'Hutang Usaha', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-21 20:15:44', '2021-04-21 20:15:44'),
(47, '6-000', '', NULL, '6-020', 'Komisi Agen/Biro', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:47:26'),
(48, '7-000', '', NULL, '7-400', 'Fee Refund', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-30 18:41:47', '2021-04-30 18:41:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_finance_account_group`
--

CREATE TABLE `trvl_ref_finance_account_group` (
  `group_id` varchar(15) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_finance_account_group`
--

INSERT INTO `trvl_ref_finance_account_group` (`group_id`, `name`, `active`, `created_at`, `updated_at`) VALUES
('1-000', 'ASET', 'Y', '2021-04-06 19:51:05', '2021-04-06 19:51:05'),
('2-000', 'KEWAJIBAN', 'Y', '2021-04-06 19:51:16', '2021-04-06 19:51:16'),
('3-000', 'EKUITAS', 'Y', '2021-04-06 19:51:25', '2021-04-06 19:51:25'),
('4-000', 'PENDAPATAN', 'Y', '2021-04-06 19:51:37', '2021-04-06 19:51:37'),
('5-000', 'HARGA POKOK PENJUALAN', 'Y', '2021-04-06 19:51:50', '2021-04-06 19:51:50'),
('6-000', 'BIAYA USAHA', 'Y', '2021-04-06 19:52:10', '2021-04-06 19:52:10'),
('7-000', 'PENDAPATAN LAINNYA', 'Y', '2021-04-06 19:52:23', '2021-04-08 20:08:06'),
('8-000', 'BIAYA LAINNYA', 'Y', '2021-04-08 20:07:46', '2021-04-08 20:07:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_helper`
--

CREATE TABLE `trvl_ref_helper` (
  `helper_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` text,
  `phone_num` varchar(25) DEFAULT NULL,
  `ttl_trnsct` int(5) DEFAULT NULL,
  `ttl_saving` int(25) DEFAULT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_helper`
--

INSERT INTO `trvl_ref_helper` (`helper_id`, `name`, `address`, `phone_num`, `ttl_trnsct`, `ttl_saving`, `active`, `created_at`, `updated_at`) VALUES
(1, 'ADIT', 'BOBOS', '089321456987', NULL, NULL, 'Y', '2021-04-21 20:00:07', '2021-04-21 20:00:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_payment_id`
--

CREATE TABLE `trvl_ref_payment_id` (
  `id` int(11) NOT NULL,
  `month` varchar(7) NOT NULL,
  `serial_num` int(4) UNSIGNED ZEROFILL NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_payment_method`
--

CREATE TABLE `trvl_ref_payment_method` (
  `payment_method_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_payment_method`
--

INSERT INTO `trvl_ref_payment_method` (`payment_method_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cash', '2021-03-22 07:24:33', '2021-03-22 07:24:33'),
(2, 'Transfer BRI', '2021-03-22 07:24:33', '2021-03-22 07:24:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_spj_budget_account`
--

CREATE TABLE `trvl_ref_spj_budget_account` (
  `account_id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trvl_ref_spj_budget_account`
--

INSERT INTO `trvl_ref_spj_budget_account` (`account_id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Driver', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(2, 'Co. Driver', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(3, 'Jarak', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(4, 'BBM', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(5, 'Tol', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(6, 'Parkir', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(7, 'Lain-lain', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_spj_id`
--

CREATE TABLE `trvl_ref_spj_id` (
  `id` int(11) NOT NULL,
  `month` varchar(7) NOT NULL,
  `serial_num` int(4) UNSIGNED ZEROFILL NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_booking`
--

CREATE TABLE `trvl_trnsct_booking` (
  `booking_id` varchar(15) NOT NULL,
  `trnsct_date` varchar(30) NOT NULL,
  `cust_id` int(5) DEFAULT NULL,
  `cust_name` varchar(45) DEFAULT NULL,
  `cust_phone_num` varchar(25) DEFAULT NULL,
  `cust_address` text,
  `booking_start_date` varchar(10) NOT NULL,
  `booking_end_date` varchar(10) NOT NULL,
  `destination` text NOT NULL,
  `pick_up_location` text NOT NULL,
  `standby_time` text NOT NULL,
  `information` text,
  `booking_from` varchar(15) NOT NULL,
  `agent_id` int(3) DEFAULT NULL,
  `agent_commission` int(20) DEFAULT NULL,
  `st_commission` varchar(20) DEFAULT 'BELUM DICAIRKAN',
  `ttl_unit` int(3) NOT NULL,
  `ttl_trnsct` int(20) NOT NULL,
  `ttl_trnsct_paid` int(20) DEFAULT '0',
  `charge` int(20) DEFAULT NULL,
  `discount` int(20) DEFAULT NULL,
  `status` varchar(25) DEFAULT 'MENUNGGU',
  `cancel_date` varchar(10) DEFAULT NULL,
  `cancel_by` varchar(45) DEFAULT NULL,
  `money_return` int(25) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_booking_dtl`
--

CREATE TABLE `trvl_trnsct_booking_dtl` (
  `trnsctdtl_id` bigint(20) NOT NULL,
  `booking_id` varchar(25) NOT NULL,
  `booking_start_date` varchar(10) NOT NULL,
  `booking_end_date` varchar(10) NOT NULL,
  `category_id` varchar(10) NOT NULL,
  `unit_qty` int(3) NOT NULL DEFAULT '0',
  `unit_qty_spj` int(3) NOT NULL DEFAULT '0',
  `price` int(20) NOT NULL DEFAULT '0',
  `total` int(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_jurnal`
--

CREATE TABLE `trvl_trnsct_jurnal` (
  `jurnal_id` bigint(20) NOT NULL,
  `proof_id` varchar(15) NOT NULL,
  `trnsct_date` varchar(25) NOT NULL,
  `related_person` varchar(45) NOT NULL,
  `account_id` varchar(15) NOT NULL,
  `description` text NOT NULL,
  `debit` int(25) DEFAULT '0',
  `credit` int(25) DEFAULT '0',
  `type` varchar(15) NOT NULL,
  `post_net_profit` varchar(35) DEFAULT NULL,
  `is_main` enum('Y','N') NOT NULL,
  `user` varchar(45) NOT NULL,
  `status` varchar(10) DEFAULT 'OK',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `booking_id` varchar(25) DEFAULT NULL,
  `spj_id` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `trvl_trnsct_jurnal`
--
DELIMITER $$
CREATE TRIGGER `cancel_jurnal` BEFORE UPDATE ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
IF NEW.status = 'BATAL' THEN
UPDATE trvl_ref_finance_account
SET credit = credit - old.credit, 
	debit = debit - old.debit
WHERE account_id = old.account_id;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_akun_laba_bersih` BEFORE INSERT ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
IF NEW.post_net_profit = 'LABARUGI' THEN
UPDATE trvl_ref_finance_account
SET credit = credit + NEW.credit, 
	debit = debit + NEW.debit
WHERE account_id = '3-200';
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_saldo_neraca_insert` BEFORE INSERT ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
UPDATE trvl_ref_finance_account
SET debit = debit + NEW.debit, credit = credit + NEW.credit
WHERE account_id = NEW.account_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_saldo_neraca_update` BEFORE UPDATE ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
UPDATE trvl_ref_finance_account
SET debit = (debit - OLD.debit) + NEW.debit, 
credit = (credit - OLD.credit) + NEW.credit
WHERE account_id = NEW.account_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_payment`
--

CREATE TABLE `trvl_trnsct_payment` (
  `payment_id` varchar(15) NOT NULL,
  `booking_id` varchar(20) DEFAULT NULL,
  `payment_date` varchar(35) NOT NULL,
  `received_from` varchar(45) NOT NULL,
  `amount` int(25) NOT NULL,
  `payment_method` varchar(15) NOT NULL,
  `information` text NOT NULL,
  `is_canceled` int(1) NOT NULL DEFAULT '0',
  `admin` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_spj`
--

CREATE TABLE `trvl_trnsct_spj` (
  `id` bigint(20) NOT NULL,
  `spj_id` varchar(25) NOT NULL,
  `booking_id` varchar(25) NOT NULL,
  `police_num` varchar(10) NOT NULL,
  `category_id` varchar(10) NOT NULL,
  `main_driver` varchar(10) NOT NULL,
  `second_driver` varchar(10) DEFAULT NULL,
  `helper_id` varchar(10) DEFAULT NULL,
  `premi_main_driver` int(15) DEFAULT NULL,
  `premi_second_driver` int(15) DEFAULT NULL,
  `premi_helper` int(15) DEFAULT NULL,
  `budget` int(20) NOT NULL DEFAULT '0',
  `km_start` varchar(15) DEFAULT NULL,
  `km_end` varchar(15) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'MENUNGGU',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `trvl_trnsct_spj`
--
DELIMITER $$
CREATE TRIGGER `update_qty_unit_spj` AFTER INSERT ON `trvl_trnsct_spj` FOR EACH ROW BEGIN
UPDATE trvl_trnsct_booking_dtl set unit_qty_spj = unit_qty_spj + 1
where category_id = NEW.category_id and booking_id = NEW.booking_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_spj_budget`
--

CREATE TABLE `trvl_trnsct_spj_budget` (
  `spjbudget_id` bigint(20) NOT NULL,
  `spj_id` varchar(25) NOT NULL,
  `account_id` int(10) NOT NULL,
  `qty` int(10) DEFAULT NULL,
  `duration` varchar(20) DEFAULT NULL,
  `amount` int(20) DEFAULT NULL,
  `ttl_amount` int(20) DEFAULT NULL,
  `information` text,
  `is_saving` enum('Y','N') DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sys_ref_acl`
--
ALTER TABLE `sys_ref_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aktif` (`aktif`);

--
-- Indexes for table `sys_ref_group_menu`
--
ALTER TABLE `sys_ref_group_menu`
  ADD PRIMARY KEY (`id_group_menu`);

--
-- Indexes for table `sys_ref_module`
--
ALTER TABLE `sys_ref_module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_module` (`id_module`);

--
-- Indexes for table `sys_ref_role`
--
ALTER TABLE `sys_ref_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_role` (`id_role`);

--
-- Indexes for table `sys_ref_salary`
--
ALTER TABLE `sys_ref_salary`
  ADD PRIMARY KEY (`salary_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `month` (`month`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `sys_ref_sdm`
--
ALTER TABLE `sys_ref_sdm`
  ADD PRIMARY KEY (`sdm_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sys_ref_sdm_position`
--
ALTER TABLE `sys_ref_sdm_position`
  ADD PRIMARY KEY (`position_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `sys_ref_user`
--
ALTER TABLE `sys_ref_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `trvl_ref_agent`
--
ALTER TABLE `trvl_ref_agent`
  ADD PRIMARY KEY (`agent_id`);

--
-- Indexes for table `trvl_ref_booking_id`
--
ALTER TABLE `trvl_ref_booking_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trvl_ref_car`
--
ALTER TABLE `trvl_ref_car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trvl_ref_car_category`
--
ALTER TABLE `trvl_ref_car_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trvl_ref_car_service`
--
ALTER TABLE `trvl_ref_car_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `police_num` (`police_num`);

--
-- Indexes for table `trvl_ref_customer`
--
ALTER TABLE `trvl_ref_customer`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `trvl_ref_driver`
--
ALTER TABLE `trvl_ref_driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `trvl_ref_driver_saving`
--
ALTER TABLE `trvl_ref_driver_saving`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trvl_ref_finance_account`
--
ALTER TABLE `trvl_ref_finance_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_id` (`account_id`);

--
-- Indexes for table `trvl_ref_finance_account_group`
--
ALTER TABLE `trvl_ref_finance_account_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `trvl_ref_helper`
--
ALTER TABLE `trvl_ref_helper`
  ADD PRIMARY KEY (`helper_id`);

--
-- Indexes for table `trvl_ref_payment_id`
--
ALTER TABLE `trvl_ref_payment_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trvl_ref_payment_method`
--
ALTER TABLE `trvl_ref_payment_method`
  ADD PRIMARY KEY (`payment_method_id`);

--
-- Indexes for table `trvl_ref_spj_budget_account`
--
ALTER TABLE `trvl_ref_spj_budget_account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `trvl_ref_spj_id`
--
ALTER TABLE `trvl_ref_spj_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trvl_trnsct_booking`
--
ALTER TABLE `trvl_trnsct_booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD UNIQUE KEY `trnsct_date` (`trnsct_date`),
  ADD KEY `booking_start_date` (`booking_start_date`),
  ADD KEY `booking_end_date` (`booking_end_date`);

--
-- Indexes for table `trvl_trnsct_booking_dtl`
--
ALTER TABLE `trvl_trnsct_booking_dtl`
  ADD PRIMARY KEY (`trnsctdtl_id`),
  ADD KEY `booking_end_date` (`booking_end_date`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `booking_start_date` (`booking_start_date`),
  ADD KEY `booking_id` (`booking_id`);

--
-- Indexes for table `trvl_trnsct_jurnal`
--
ALTER TABLE `trvl_trnsct_jurnal`
  ADD PRIMARY KEY (`jurnal_id`);

--
-- Indexes for table `trvl_trnsct_payment`
--
ALTER TABLE `trvl_trnsct_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `booking_id` (`booking_id`);

--
-- Indexes for table `trvl_trnsct_spj`
--
ALTER TABLE `trvl_trnsct_spj`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `police_num` (`police_num`),
  ADD KEY `main_driver` (`main_driver`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `spj_id` (`spj_id`);

--
-- Indexes for table `trvl_trnsct_spj_budget`
--
ALTER TABLE `trvl_trnsct_spj_budget`
  ADD PRIMARY KEY (`spjbudget_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sys_ref_acl`
--
ALTER TABLE `sys_ref_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `sys_ref_group_menu`
--
ALTER TABLE `sys_ref_group_menu`
  MODIFY `id_group_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sys_ref_module`
--
ALTER TABLE `sys_ref_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sys_ref_role`
--
ALTER TABLE `sys_ref_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sys_ref_salary`
--
ALTER TABLE `sys_ref_salary`
  MODIFY `salary_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_ref_sdm`
--
ALTER TABLE `sys_ref_sdm`
  MODIFY `sdm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sys_ref_user`
--
ALTER TABLE `sys_ref_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `trvl_ref_agent`
--
ALTER TABLE `trvl_ref_agent`
  MODIFY `agent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `trvl_ref_booking_id`
--
ALTER TABLE `trvl_ref_booking_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_ref_car`
--
ALTER TABLE `trvl_ref_car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `trvl_ref_car_category`
--
ALTER TABLE `trvl_ref_car_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `trvl_ref_car_service`
--
ALTER TABLE `trvl_ref_car_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_ref_customer`
--
ALTER TABLE `trvl_ref_customer`
  MODIFY `cust_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `trvl_ref_driver`
--
ALTER TABLE `trvl_ref_driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `trvl_ref_driver_saving`
--
ALTER TABLE `trvl_ref_driver_saving`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_ref_finance_account`
--
ALTER TABLE `trvl_ref_finance_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `trvl_ref_helper`
--
ALTER TABLE `trvl_ref_helper`
  MODIFY `helper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trvl_ref_payment_id`
--
ALTER TABLE `trvl_ref_payment_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_ref_payment_method`
--
ALTER TABLE `trvl_ref_payment_method`
  MODIFY `payment_method_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `trvl_ref_spj_budget_account`
--
ALTER TABLE `trvl_ref_spj_budget_account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `trvl_ref_spj_id`
--
ALTER TABLE `trvl_ref_spj_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_trnsct_booking_dtl`
--
ALTER TABLE `trvl_trnsct_booking_dtl`
  MODIFY `trnsctdtl_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_trnsct_jurnal`
--
ALTER TABLE `trvl_trnsct_jurnal`
  MODIFY `jurnal_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_trnsct_spj`
--
ALTER TABLE `trvl_trnsct_spj`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trvl_trnsct_spj_budget`
--
ALTER TABLE `trvl_trnsct_spj_budget`
  MODIFY `spjbudget_id` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
