<?php

namespace App\SystemModel;

use Illuminate\Database\Eloquent\Model;

class SysRefModule extends Model
{
    protected $table = "sys_ref_module";
    // protected $primaryKey = "id_module";
    protected $fillable = [
        "id",
        "id_module",
        "nama",
        "deskripsi",
        "image",
        "aktif",
    ];
}
