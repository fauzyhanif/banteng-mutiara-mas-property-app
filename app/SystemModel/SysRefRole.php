<?php

namespace App\SystemModel;

use Illuminate\Database\Eloquent\Model;

class SysRefRole extends Model
{
    protected $table = "sys_ref_role";
    protected $fillable = [
        "id",
        "id_role",
        "nama",
        "aktif",
    ];
}
