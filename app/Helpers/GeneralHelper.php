<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class GeneralHelper {

    public static function konversiTgl($date, $date_format='')
    {
        $dayList = array(
            'Sunday'    => 'Minggu',
            'Monday'    => 'Senin',
            'Tuesday'   => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday'  => 'Kamis',
            'Friday'    => 'Jumat',
            'Saturday'  => 'Sabtu'
        );

        $monthList = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );

        $format_hari = date('l', strtotime($date));
        $format_tgl  = date('d', strtotime($date));
        $format_bln  = date('m', strtotime($date));
        $format_thn  = date('Y', strtotime($date));

        switch ($date_format) {
            case 'l':
                # Hari ex: Kamis
                $output = $dayList[$format_hari];
                break;
            case 'd':
                # Tanggal ex: 21
                $output = $format_tgl;
                break;
            case 'm':
                # Bulan ex: Januari
                $output = $monthList[$format_bln];
                break;
            case 'y':
                # Tahun ex: 2016
                $output = $format_thn;
                break;
            case 'T':
                # Tgl Lahir
                $output = $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
            case 'slash':
                # indo bgt
                $output = $format_tgl . '/' . $format_bln . '/' . $format_thn;
                break;
            case 'ttd':
                # indo bgt
                $output = $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
            default:
                # Hari, Tanggal-Bulan-Tahun ex: Rabu, 26-Juli-2016
                $output = $dayList[$format_hari] . ', ' . $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
        }

        return $output;
    }

    public static function month($month)
    {
        $monthList = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );

        return $monthList[$month];
    }

    public static function rupiah($angka){
        $hasil_rupiah = number_format($angka,0,',','.');
        return $hasil_rupiah;
    }

    public static function terbilang($x, $style = 1) {
        if ($x < 0) {
            $hasil="minus " . trim(GeneralHelper::kekata($x));
        } else {
            $hasil = trim(GeneralHelper::kekata($x));
        }

        switch ($style) {
            case 1:
                $hasil = ucwords($hasil);
            break;
            case 2:
                $hasil = strtolower($hasil);
            break;
            case 3:
                $hasil = ucwords($hasil);
            break;
            default:
                $hasil = ucfirst($hasil);
            break;
        }

        return $hasil . " Rupiah";
    }

    public static function kekata($x) {
		$x = abs($x);
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";

		if ($x < 12) {
			$temp = " " . $angka[$x];
		} elseif ($x < 20) {
			$temp = GeneralHelper::kekata($x - 10) . " belas";
		} elseif ($x < 100) {
			$temp = GeneralHelper::kekata($x / 10) . " puluh" . GeneralHelper::kekata($x % 10);
		} elseif ($x < 200) {
			$temp = " seratus" . GeneralHelper::kekata($x - 100);
		} elseif ($x < 1000) {
			$temp = GeneralHelper::kekata($x / 100) . " ratus" . GeneralHelper::kekata($x % 100);
		} elseif ($x < 2000) {
			$temp = " seribu" . GeneralHelper::kekata($x - 1000);
		} elseif ($x < 1000000) {
			$temp = GeneralHelper::kekata($x / 1000) . " ribu" . GeneralHelper::kekata($x % 1000);
		} elseif ($x < 1000000000) {
			$temp = GeneralHelper::kekata($x / 1000000) . " juta" . GeneralHelper::kekata($x % 1000000);
		} elseif ($x < 1000000000000) {
			$temp = GeneralHelper::kekata($x / 1000000000) . " milyar" . GeneralHelper::kekata(fmod($x, 1000000000));
		} elseif ($x < 1000000000000000) {
			$temp = GeneralHelper::kekata($x / 1000000000000) . " trilyun" . GeneralHelper::kekata(fmod($x, 1000000000000));
		}

		return $temp;
	}

    public static function bulan_romawi($bulan)
    {
        $romawi['01'] = 'I';
        $romawi['02'] = 'II';
        $romawi['03'] = 'III';
        $romawi['04'] = 'IV';
        $romawi['05'] = 'V';
        $romawi['06'] = 'VI';
        $romawi['07'] = 'VII';
        $romawi['08'] = 'VIII';
        $romawi['09'] = 'IX';
        $romawi['10'] = 'X';
        $romawi['11'] = 'XI';
        $romawi['12'] = 'XII';

        return $romawi[$bulan];
    }



}
