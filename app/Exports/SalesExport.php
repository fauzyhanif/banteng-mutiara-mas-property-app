<?php
namespace App\Exports;

use App\ModelProp\TrnsctSalesExport;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class SalesExport implements FromView
{

  public function view(): View
  {
    $items = TrnsctSalesExport::all();

    return view('prop.trnsct_sales_export.export', [
      'items' => $items,
    ]);
  }
}
?>