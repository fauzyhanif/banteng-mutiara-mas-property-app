<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefCertificateStep extends Model
{
    protected $table = "prop_ref_certificate_step";
    protected $primaryKey = "step_id";
    protected $fillable = [
        "step_id",
        "name",
        "orders",
        "is_active",
    ];
}
