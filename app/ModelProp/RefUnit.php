<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class RefUnit extends Model
{
    use Compoships;

    protected $table = "prop_ref_unit";
    protected $primaryKey = "unit_id";
    protected $fillable = [
        "unit_id",
        "location_id",
        "block_id",
        "unittype_id",
        "number",
        "status",
        "uang_muka",
        "max_credit_pengajuan",
        "max_credit_acc",
        "ttl_harga_jual",
        "marketer_id",
        "marketer_fee",
        "marketer_fee_paid",
        "jenis_rumah",
        "subsidi_type_id",
        'is_publish'
    ];

    public function unitType()
    {
        return $this->belongsTo(RefUnitType::class, 'unittype_id', 'unittype_id');
    }

    public function location()
    {
        return $this->belongsTo(RefLocation::class, 'location_id', 'location_id');
    }

    public function block()
    {
        return $this->belongsTo(RefBlock::class, ['location_id', 'block_id'], ['location_id', 'block_id']);
    }

    public function customer()
    {
        return $this->belongsTo(RefSdm::class, 'customer_id', 'sdm_id');
    }

    public function unitStatus()
    {
        return $this->belongsTo(RefUnitStatus::class, 'unitstatus_id', 'unitstatus_id');
    }

    public function worklist()
    {
        return $this->belongsTo(RefWorklist::class, 'unit_id', 'unit_id');
    }


}
