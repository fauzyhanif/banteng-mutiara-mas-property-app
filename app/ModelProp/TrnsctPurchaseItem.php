<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctPurchaseItem extends Model
{
    protected $table = "prop_trnsct_purchase_item";
    protected $primaryKey = "purcitem_id";
    protected $fillable = [
        "purcitem_id",
        "purchase_id",
        "goods_id",
        "qty",
    ];

    public function goods()
    {
        return $this->belongsTo(RefGoods::class, 'goods_id', 'goods_id');
    }
}
