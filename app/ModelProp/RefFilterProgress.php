<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefFilterProgress extends Model
{
    protected $table = "prop_ref_filter_progress";

    protected $fillable = [
        "type",
        "id"
    ];

}
