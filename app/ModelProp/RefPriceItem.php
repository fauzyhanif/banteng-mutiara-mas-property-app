<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefPriceItem extends Model
{
    protected $table = "prop_ref_price_item";
    protected $primaryKey = "price_id";
    protected $fillable = [
        "price_id",
        "name",
        "orders",
        "is_active",
    ];
}
