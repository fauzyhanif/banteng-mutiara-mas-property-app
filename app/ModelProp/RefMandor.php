<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefMandor extends Model
{
    protected $table = "prop_ref_mandor";
    protected $primaryKey = "sdm_id";
    protected $fillable = [
        "sdm_id",
        "specialist",
        "ttl_salary",
        "ttl_salary_taken",
        "price",
    ];

    public function sdm()
    {
        return $this->belongsTo(RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
