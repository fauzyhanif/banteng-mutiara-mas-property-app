<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefUnitStatus extends Model
{
    protected $table = "prop_ref_unit_status";
    protected $primaryKey = "unitstatus_id";
    protected $fillable = [
        "unitstatus_id",
        "name",
    ];
}
