<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefSalesStep extends Model
{
    protected $table = "prop_ref_sales_step";
    protected $primaryKey = "step_id";
    protected $fillable = [
        "step_id",
        "name",
        "type",
        "orders",
        "is_active",
    ];

    public function sales()
    {
        return $this->hasMany(TrnsctSales::class, 'step_sales', 'step_id');
    }
}
