<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctGoodsItem extends Model
{
    protected $table = "prop_trnsct_goods_item";
    protected $primaryKey = "trnitem_id";
    protected $fillable = [
        "trnitem_id",
        "trnsct_id",
        "goods_id",
        "qty",
    ];

    public function goods()
    {
        return $this->belongsTo(RefGoods::class, 'goods_id', 'goods_id');
    }
}
