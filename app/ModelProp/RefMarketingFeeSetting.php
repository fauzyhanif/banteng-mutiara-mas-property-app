<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefMarketingFeeSetting extends Model
{
  protected $table = "prop_ref_marketing_fee_setting";
  protected $primaryKey = "id";
  protected $fillable = [
    "id",
    "marketer_type_id",
    "subsidi_type_id",
    "method",
    "booking_fee",
    "akad_fee"
  ];

  public function marketerType()
  {
    return $this->belongsTo(RefMarketerType::class, 'marketer_type_id', 'type_id');
  }

  public function subdisiType()
  {
    return $this->belongsTo(RefSubsidiType::class, 'subsidi_type_id', 'id');
  }
}
