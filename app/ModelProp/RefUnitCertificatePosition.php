<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefUnitCertificatePosition extends Model
{
    protected $table = "prop_ref_unit_certificate_position";
    protected $fillable = [
        "id",
        "sales_id",
        "location_id",
        "block_id",
        "unit_id",
        "step_name",
        "step_date",
        "step_desc",
    ];
}
