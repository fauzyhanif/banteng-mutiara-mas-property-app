<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class RefBlock extends Model
{
    use Compoships;

    protected $table = "prop_ref_block";
    protected $primaryKey = "block_id";
    protected $fillable = [
        "block_id",
        "location_id",
        "name",
        "is_active",
    ];

    protected $appends = ["jml_unit_ready"];

    public function location()
    {
        return $this->belongsTo(RefLocation::class, 'location_id', 'location_id');
    }

    public function units()
    {
        return $this->hasMany(RefUnit::class, ['location_id', 'block_id'], ['location_id', 'block_id']);
    }

    public function getJmlUnitReadyAttribute()
    {
        return $this->units->where('status', 'READY')->count();
    }
}
