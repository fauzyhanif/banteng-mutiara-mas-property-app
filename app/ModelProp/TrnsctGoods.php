<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctGoods extends Model
{
    protected $table = "prop_trnsct_goods";
    protected $primaryKey = "trnsct_id";
    protected $fillable = [
        "trnsct_id",
        "invoice_id",
        "type",
        "trnsct_date",
        "due_date",
        "ttl_trnsct",
        "ttl_trnsct_paid",
        "buy_from",
        "mandor_id",
        "goods",
        "is_active",
    ];

    public function mandor()
    {
        return $this->belongsTo(RefSdm::class, 'mandor_id', 'sdm_id');
    }

    public function store()
    {
        return $this->belongsTo(RefSdm::class, 'buy_from', 'sdm_id');
    }
}
