<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefUnitCertificateStep extends Model
{
    protected $table = "prop_ref_unit_certificate_step";
    protected $fillable = [
        "id",
        "sales_id",
        "location_id",
        "block_id",
        "unit_id",
        "step_id",
        "step_date",
        "step_desc",
    ];

    public function step()
    {
        return $this->belongsTo(RefCertificateStep::class, 'step_id', 'step_id');
    }
}
