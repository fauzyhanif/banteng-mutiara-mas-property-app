<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctSalesReturn extends Model
{
    protected $table = "prop_trnsct_sales_return";
    protected $primaryKey = "sales_return_id";
    protected $fillable = [
        "sales_return_id",
        "sales_id",
        "sales_return_num",
        "sales_return_date",
        "sales_return_person",
        "sales_return_cash_id",
        "sales_return_desc",
        "sales_return_amount",
        "sales_return_kpr",
        "sales_return_cancel",
    ];

    public function cash_bank()
    {
        return $this->belongsTo(\App\ModelFinance\RefCashBank::class, 'sales_return_cash_id', 'cash_id');
    }

    public function sales()
    {
        return $this->belongsTo(TrnsctSales::class, 'sales_id', 'sales_id');
    }
}
