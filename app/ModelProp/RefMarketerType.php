<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefMarketerType extends Model
{
    protected $table = "prop_ref_marketer_type";
    protected $primaryKey = 'type_id';
    protected $fillable = [
        "type_id",
        "name",
        "subsidi_booking_fee",
        "subsidi_akad_fee",
        "nonsubsidi_booking_fee",
        "nonsubsidi_akad_fee",
        "is_active",
    ];

    public function marketingFeeSettings()
    {
        return $this->hasMany(RefMarketingFeeSetting::class, 'marketer_type_id', 'type_id');    
    }
}
