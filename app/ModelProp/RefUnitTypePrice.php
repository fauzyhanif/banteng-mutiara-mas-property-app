<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefUnitTypePrice extends Model
{
    protected $table = "prop_ref_unit_type_price";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "unittype_id",
        "sales_type",
        "price_id",
        "price"
    ];

    public function priceItem()
    {
        return $this->belongsTo(RefPriceItem::class, 'price_id', 'price_id');
    }
}
