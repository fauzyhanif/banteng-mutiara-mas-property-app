<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefStoreGoods extends Model
{
    protected $table = "prop_ref_store_goods";
    protected $fillable = [
        "sdm_id",
        "goods_id",
    ];

    public function goods()
    {
        return $this->belongsTo(RefGoods::class, 'goods_id', 'goods_id');
    }
}
