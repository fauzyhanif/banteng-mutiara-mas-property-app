<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;
class RefUnitSalesStep extends Model
{
    use Compoships;

    protected $table = "prop_ref_unit_sales_step";
    protected $fillable = [
        "id",
        "invoice_id",
        "location_id",
        "block_id",
        "unit_id",
        "step_id",
        "step_date",
        "step_desc",
    ];

    public function step()
    {
        return $this->belongsTo(RefSalesStep::class, 'step_id', 'step_id');
    }
}
