<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefGoods extends Model
{
    protected $table = "prop_ref_goods";
    protected $primaryKey = "goods_id";
    protected $fillable = [
        "goods_id",
        "name",
        "stock",
        "price",
        "unit",
        "is_active",
    ];
}
