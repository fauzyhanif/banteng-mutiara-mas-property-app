<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefDepartment extends Model
{
    protected $table = "prop_ref_department";
    protected $primaryKey = "dept_id";
    protected $fillable = [
        "dept_id",
        "parent",
        "name",
        "is_active",
    ];
}
