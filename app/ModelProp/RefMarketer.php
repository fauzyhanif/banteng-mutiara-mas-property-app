<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefMarketer extends Model
{
    protected $table = "prop_ref_marketer";
    protected $primaryKey = 'sdm_id';
    protected $fillable = [
        "sdm_id",
        "marketer_type",
        "affiliate_num",
        "orders",
    ];

    public function marketerType()
    {
        return $this->belongsTo(RefMarketerType::class, 'marketer_type', 'type_id');
    }

    public function sdm()
    {
        return $this->belongsTo(RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
