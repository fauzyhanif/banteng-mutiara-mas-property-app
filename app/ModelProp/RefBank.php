<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefBank extends Model
{
    protected $table = "prop_ref_bank";
    protected $fillable = [
        "sdm_id"
    ];

    public function sdm()
    {
        return $this->belongsTo(RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
