<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefUnitType extends Model
{
    protected $table = "prop_ref_unit_type";
    protected $primaryKey = "unittype_id";
    protected $fillable = [
        "unittype_id",
        "name",
        "ground_area",
        "ground_length",
        "ground_wide",
        "building_area",
        "building_length",
        "building_wide",
        "marketing_fee_agent",
        "marketing_fee_downline",
        "is_active",
    ];
}
