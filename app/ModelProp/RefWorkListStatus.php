<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefWorklistStatus extends Model
{
    protected $table = "prop_ref_worklist_status";
    protected $primaryKey = "status_id";
    protected $fillable = [
        "status_id",
        "name",
    ];
}
