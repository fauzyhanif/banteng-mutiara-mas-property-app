<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefLocation extends Model
{
    protected $table = "prop_ref_location";
    protected $primaryKey = "location_id";
    protected $fillable = [
        "location_id",
        "name",
        "description",
        "address",
        "is_active",
    ];
    
}
