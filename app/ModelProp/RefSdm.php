<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefSdm extends Model
{
    protected $table = "prop_ref_sdm";
    protected $primaryKey = "sdm_id";
    protected $fillable = [
        "sdm_id",
        "name",
        "date_ofbirth",
        "place_of_birth",
        "nik",
        "email",
        "phone_num",
        "address",
        "address_domicile",
        "job",
        "institute",
        "income",
        "rek_bank",
        "rek_number",
        "rek_name",
        "rek_bank_new",
        "rek_number_new",
        "is_married",
        "couple",
        "couple_nik",
        "couple_phone_num",
        "couple_date_of_birth",
        "couple_place_of_birth",
        "couple_job",
        "couple_address_domicile",
        "note",
        "file_ktp",
        "file_kk",
        "is_active",
    ];

    public function mandor()
    {
        return $this->belongsTo(RefMandor::class, 'sdm_id', 'sdm_id');
    }

    public function marketer()
    {
        return $this->belongsTo(RefMarketer::class, 'sdm_id', 'sdm_id');
    }

    public function store()
    {
        return $this->belongsTo(RefStore::class, 'sdm_id', 'sdm_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'sdm_id', 'sdm_id');
    }
}
