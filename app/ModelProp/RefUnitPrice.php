<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefUnitPrice extends Model
{
    protected $table = "prop_ref_unit_price";
    protected $primaryKey = "unitprice_id";
    protected $fillable = [
        "unitprice_id",
        "location_id",
        "block_id",
        "unit_id",
        "price_id",
        "price",
    ];

    public function priceItem()
    {
        return $this->belongsTo(RefPriceItem::class, 'price_id', 'price_id');
    }
}
