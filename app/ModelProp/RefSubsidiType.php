<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefSubsidiType extends Model
{
  protected $table = "prop_ref_subsidi_type";
  protected $primaryKey = "id";
  protected $fillable = [
    "id",
    "type",
    "name"
  ];

  public function units()
  {
    return $this->hasMany(RefUnit::class, 'subsidi_type_id', 'id');
  }
}
