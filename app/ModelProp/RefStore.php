<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefStore extends Model
{
    protected $table = "prop_ref_store";
    protected $primaryKey = "sdm_id";
    protected $fillable = [
        "sdm_id",
        "description",
    ];

    public function sdm()
    {
        return $this->belongsTo(RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
