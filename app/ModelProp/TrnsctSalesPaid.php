<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctSalesPaid extends Model
{
    protected $table = "prop_trnsct_sales_paid";
    protected $primaryKey = "sales_paid_id";
    protected $fillable = [
        "sales_paid_id",
        "sales_id",
        "sales_paid_num",
        "sales_paid_date",
        "sales_paid_person",
        "sales_paid_cash_id",
        "sales_paid_desc",
        "sales_paid_amount",
        "sales_paid_kpr",
        "installment_amount",
        "installment_desc",
        "debt_id",
        "sales_paid_cancel",
    ];

    public function cash_bank()
    {
        return $this->belongsTo(\App\ModelFinance\RefCashBank::class, 'sales_paid_cash_id', 'cash_id');
    }

    public function sales()
    {
        return $this->belongsTo(TrnsctSales::class, 'sales_id', 'sales_id');
    }

    public function installment_debt()
    {
        return $this->belongsTo(\App\ModelFinance\RefDebtToBank::class, 'debt_id', 'debt_id');
    }
}
