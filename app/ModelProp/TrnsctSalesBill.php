<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctSalesBill extends Model
{
    protected $table = "prop_trnsct_sales_bill";
    protected $primaryKey = "id";
    protected $fillable = [
        "sales_id",
        "paid_off",
        "reminder",
        "due_date",
        "created_at"
    ];

    public function sales()
    {
        return $this->belongsTo(\App\ModelProp\TrnsctSales::class, 'sales_id', 'sales_id');
    }

}
