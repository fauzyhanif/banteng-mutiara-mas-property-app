<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class TrnsctSalesExport extends Model
{
    protected $table = "prop_trnsct_sales_export";
    protected $primaryKey = "id";
    protected $fillable = [
        'id',
        'sales_id',
        'customer_name',
        'customer_nik',
        'customer_date_of_birth',
        'customer_place_of_birth',
        'customer_address',
        'customer_is_married',
        'couple_name',
        'couple_nik',
        'couple_date_of_birth',
        'couple_place_of_birth',
        'location_id',
        'location_name',
        'block_id',
        'block_name',
        'unit_id',
    ];

    public function sales()
    {
        return $this->belongsTo(TrnsctSales::class, 'sales_id', 'sales_id');
    }

}
