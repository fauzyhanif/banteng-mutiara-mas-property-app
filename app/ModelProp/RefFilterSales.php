<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefFilterSales extends Model
{
    protected $table = "prop_ref_filter_sales";

    protected $fillable = [
        "type",
        "id"
    ];

    public $incrementing = false;

    protected $keyType = 'string';

}
