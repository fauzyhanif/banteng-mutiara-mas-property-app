<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class RefWorklistMaster extends Model
{
    protected $table = "prop_ref_worklist_master";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "name",
        "sdm_id",
        "price",
        "orders",
    ];

    public function sdm()
    {
        return $this->belongsTo(RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
