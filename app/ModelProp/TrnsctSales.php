<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class TrnsctSales extends Model
{
    use Compoships;

    protected $table = "prop_trnsct_sales";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "sales_type",
        "sales_date",
        "sales_id",
        "sppr_number",
        "location_id",
        "block_id",
        "unit_id",
        "discount",
        "basic_price",
        "additional_cost",
        "ttl_trnsct",
        "ttl_trnsct_paid",
        "customer_id",
        "affiliate_num",
        "affiliate_fee",
        "affiliate_fee_paid",
        "step_sales",
        "step_sales_date",
        "step_certificate",
        "step_certificate_date",
        "certificate_position",
        "payment_commitment",
        "payment_paid",
        "status",
        "return_plan",
        "return_plan_paid",
        "reason_to_move",
        "key_handover_date",
        "kpr_missing_requirement_document",
        "kpr_document_collection_limit",
        "kpr_document_collection_limit_2"
    ];

    public function location()
    {
        return $this->belongsTo(RefLocation::class, 'location_id', 'location_id');
    }

    public function block()
    {
        return $this->belongsTo(RefBlock::class, 'block_id', 'block_id');
    }

    public function unit()
    {
        return $this->belongsTo(RefUnit::class, ['location_id','block_id','unit_id'], ['location_id','block_id','number']);
    }

    public function customer()
    {
        return $this->belongsTo(RefSdm::class, 'customer_id', 'sdm_id');
    }

    public function marketer()
    {
        return $this->belongsTo(RefMarketer::class, 'affiliate_num', 'affiliate_num');
    }

    public function bank()
    {
        return $this->belongsTo(RefSdm::class, 'kpr_bank', 'sdm_id');
    }

    public function sales_step()
    {
        return $this->belongsTo(RefUnitSalesStep::class, ['location_id','block_id','unit_id', 'step_sales'], ['location_id','block_id','unit_id', 'step_id']);
    }

    public function last_sales_step()
    {
        return $this->belongsTo(RefSalesStep::class, 'step_sales', 'step_id');
    }

    public function payments()
    {
        return $this->hasMany(TrnsctSalesPaid::class, 'sales_id', 'sales_id');
    }
}
