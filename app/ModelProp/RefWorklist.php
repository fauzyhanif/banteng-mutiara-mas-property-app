<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class RefWorklist extends Model
{
    use Compoships;

    protected $table = "prop_ref_worklist";
    protected $primaryKey = "list_id";
    protected $fillable = [
        "list_id",
        "location_id",
        "block_id",
        "unit_id",
        "worklist_id",
        "mandor_id",
        "todo",
        "brief",
        "budget",
        "qty",
        "total",
        "progress",
        "st_transfer",
    ];

    public function location()
    {
        return $this->belongsTo(RefLocation::class, 'location_id', 'location_id');
    }

    public function block()
    {
        return $this->belongsTo(RefBlock::class, 'block_id', 'block_id');
    }

    public function unit()
    {
        return $this->belongsTo(RefUnit::class, ['location_id','block_id','unit_id'], ['location_id','block_id','number']);
    }

    public function mandor()
    {
        return $this->belongsTo(RefMandor::class, 'mandor_id', 'sdm_id');
    }

    public function status()
    {
        return $this->belongsTo(RefWorklistStatus::class, 'status_id', 'status_id');
    }

    public function worklist()
    {
        return $this->belongsTo(RefWorklistMaster::class, 'worklist_id', 'id');
    }

    public function sdm()
    {
        return $this->belongsTo(RefSdm::class, 'mandor_id', 'sdm_id');
    }
}
