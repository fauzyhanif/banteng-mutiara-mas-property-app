<?php

namespace App\ModelProp;

use Illuminate\Database\Eloquent\Model;

class TrnsctPurchase extends Model
{
    protected $table = "prop_trnsct_purchase";
    protected $primaryKey = "purchase_id";
    protected $fillable = [
        "purchase_id",
        "purchase_date",
        "location_id",
        "block_id",
        "store_id",
        "admin",
        "note",
        "termin",
        "shipping_cost",
        "payment_method",
        "status",
        "soft_delete",
    ];

    public function location()
    {
        return $this->belongsTo(RefLocation::class, 'location_id', 'location_id');
    }

    public function block()
    {
        return $this->belongsTo(RefBlock::class, 'block_id', 'block_id');
    }

    public function store()
    {
        return $this->belongsTo(RefSdm::class, 'store_id', 'sdm_id');
    }
}
