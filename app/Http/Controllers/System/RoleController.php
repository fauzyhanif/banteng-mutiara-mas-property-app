<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response,DB;
use Illuminate\Support\Facades\File;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $roles = \App\SystemModel\SysRefRole::all();
        $modules = \App\SystemModel\SysRefModule::all();
        return view('system.role.index', \compact('roles', 'modules'));
    }

    public function formAdd()
    {
        $modules = \App\SystemModel\SysRefModule::all();
        return view('system.role.formAdd',\compact('modules'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'id_role'=>'required|max:2|unique:sys_ref_role',
            'nama'=>'required',
            'id_module'=>'required',
        ]);

        $new = new \App\SystemModel\SysRefRole();
        $new->id_role = $request->get('id_role');
        $new->nama = $request->get('nama');
        $new->id_module = \json_encode($request->get('id_module'));
        $new->save();
        $response = [
            "type" => "success",
            "text" => "Role berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($idRole)
    {
        $role = \App\SystemModel\SysRefRole::where("id_role", $idRole)->first();
        $modules = \App\SystemModel\SysRefModule::all();
        return view('system.role.formEdit', \compact("role", "modules"));
    }

    public function edit(Request $request, $idRole)
    {
        $request->validate([
            'id_role'=>'required|max:2|unique:sys_ref_role,id_role,' . $idRole . ',id_role',
            'nama'=>'required',
            'id_module'=>'required',
            'aktif'=>'required',
        ]);

        $old = \App\SystemModel\SysRefRole::where("id_role", $idRole)->first();
        $old->id_role = $request->get('id_role');
        $old->nama = $request->get('nama');
        $old->id_module = \json_encode($request->get('id_module'));
        $old->aktif = $request->get('aktif')[0];
        $old->update();
        $response = [
            "type" => "success",
            "text" => "Role berhasil diubah."
        ];

        return response()->json($response);
    }

}
