<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response,DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = \App\User::all();
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        return view('system.user.index', \compact('users', 'modules', 'roles'));
    }

    public function formAdd()
    {
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        return view('system.user.formAdd',\compact('modules', 'roles'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'email'=>'required|unique:sys_ref_user',
            'password'=>'required|min:6',
            'no_telp'=>'required',
            'alamat'=>'required',
        ]);

        $new = new \App\User();
        $new->nama = $request->get('nama');
        $new->email = $request->get('email');
        $new->password = Hash::make($request->get('password'));
        $new->alamat = $request->get('alamat');
        $new->no_telp = $request->get('no_telp');
        $new->id_module = \json_encode($request->get('id_module'));
        $new->id_role = \json_encode($request->get('id_role'));
        $new->save();

        $response = [
            "type" => "success",
            "text" => "User berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($idUser)
    {
        $user = \App\User::find($idUser);
        $roles = \App\SystemModel\SysRefRole::all();
        $modules = \App\SystemModel\SysRefModule::all();
        return view('system.user.formEdit', \compact("user", "roles", "modules"));
    }

    public function edit(Request $request, $idUser)
    {
        $request->validate([
            'nama'=>'required',
            'email'=>'required|unique:sys_ref_user,email,' . $request->get('email') . ',email',
            'no_telp'=>'required',
            'alamat'=>'required',
            'aktif'=>'required',
        ]);

        $id_module = ($request->get('id_module') != null) ? $request->get('id_module') : [""];
        $id_role = ($request->get('id_role') != null) ? $request->get('id_role') : [""];

        $old = \App\User::find($idUser);
        $old->nama = $request->get('nama');
        $old->email = $request->get('email');
        $old->no_telp = $request->get('no_telp');
        $old->alamat = $request->get('alamat');
        $old->id_module = \json_encode($id_module);
        $old->id_role = \json_encode($id_role);
        $old->aktif = $request->get('aktif')[0];

        if ($request->get('password') != '') {
            $old->password = Hash::make($request->get('password'));
        }

        $old->update();
        $response = [
            "type" => "success",
            "text" => "User berhasil diubah."
        ];

        return response()->json($response);
    }

}
