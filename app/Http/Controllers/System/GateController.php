<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class GateController extends Controller
{
    public function __construct()
    {
        if (!Session::has('auth_name')) {
            return redirect('/login');
        }
    }

    public function index()
    {
        $arrModule = Session::get('auth_module');
        $arrModule = json_decode($arrModule);
        $modules = \App\SystemModel\SysRefModule::where('aktif', 'Y')
                    ->whereIn('id_module', $arrModule)
                    ->get();
        return view('system.gate.index', \compact('modules'));
    }

    public function showModule()
    {
        $arrModule = Session::get('auth_module');
        $arrModule = json_decode($arrModule);
        $modules = \App\SystemModel\SysRefModule::where('aktif', 'Y')
                    ->whereIn('id_module', $arrModule)
                    ->get();
        return view('system.gate.viewModule', \compact('modules'));
    }

    public function showRole($idModule)
    {
        $roles = \App\SystemModel\SysRefRole::where('aktif', 'Y')
                    ->where("id_module", "like", "%$idModule%")
                    ->get();

        return view('system.gate.viewRole', \compact('roles', 'idModule'));
    }

    public function setAuth($idRole, $idModule)
    {
        $module = \App\SystemModel\SysRefModule::where("id_module", $idModule)->first();
        $groups = \App\SystemModel\SysRefGroupMenu::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->get();

        $menus = \App\SystemModel\SysRefAcl::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->where("is_menu", "Y")
                ->where("parent", "0")
                ->where("aktif", "Y")
                ->orderBy("urut", "ASC")
                ->get();


        $submenus = \App\SystemModel\SysRefAcl::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->where("is_menu", "Y")
                ->where("parent", "!=", "0")
                ->where("aktif", "Y")
                ->orderBy("urut", "ASC")
                ->get();

        $acls = \App\SystemModel\SysRefAcl::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->get();

        $arr_acls = [];
        foreach ($acls as $key => $value) {
            $arr_acls[] = "$value->route";
        }

        Session::put('auth_role_aktif', $idRole);
        Session::put('auth_module_aktif', $idModule);
        Session::put('auth_module_image', $module->image);
        Session::put('auth_module_nama', $module->nama);
        Session::put('auth_group_menu', $groups);
        Session::put('auth_menu', $menus);
        Session::put('auth_submenu', $submenus);
        Session::put('auth_acl', $arr_acls);

        return redirect('/');
    }

    public function getMenu()
    {
        $idRole = Session::get('auth_role_aktif');
        $idModule = Session::get('auth_module_aktif');

        return view('layouts.getMenu');
    }

    public function error404()
    {
        return view('system.gate.error404');
    }
}
