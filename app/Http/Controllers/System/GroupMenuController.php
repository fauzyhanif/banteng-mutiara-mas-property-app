<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response,DB;

class GroupMenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $groups = \App\SystemModel\SysRefGroupMenu::all();
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        return view('system.groupMenu.index', \compact('groups', 'modules', 'roles'));
    }

    public function formAdd()
    {
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        return view('system.groupMenu.formAdd',\compact('modules', 'roles'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'id_group_menu'=>'required|max:2|unique:sys_ref_group_menu',
            'nama'=>'required',
            'id_module'=>'required',
            'id_role'=>'required',
            'urut'=>'required',
        ]);

        $new = new \App\SystemModel\SysRefGroupMenu();
        $new->id_group_menu = $request->get('id_group_menu');
        $new->nama = $request->get('nama');
        $new->id_module = \json_encode($request->get('id_module'));
        $new->id_role = \json_encode($request->get('id_role'));
        $new->urut = $request->get('urut');
        $new->save();
        $response = [
            "type" => "success",
            "text" => "Group Menu berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($idGroup)
    {
        $group = \App\SystemModel\SysRefGroupMenu::where("id_group_menu", $idGroup)->first();
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        return view('system.groupMenu.formEdit', \compact("group", "roles", "modules"));
    }

    public function edit(Request $request, $idGroup)
    {
        $request->validate([
            'id_group_menu'=>'required|max:2|unique:sys_ref_group_menu,id_group_menu,' . $idGroup . ',id_group_menu',
            'nama'=>'required',
            'id_module'=>'required',
            'id_role'=>'required',
            'urut'=>'required',
            'aktif'=>'required',
        ]);

        // print_r($request->all()); die();

        $old = \App\SystemModel\SysRefGroupMenu::where("id_group_menu", $idGroup)->first();
        $old->id_group_menu = $request->get('id_group_menu');
        $old->nama = $request->get('nama');
        $old->id_module = \json_encode($request->get('id_module'));
        $old->id_role = \json_encode($request->get('id_role'));
        $old->urut = $request->get('urut');
        $old->aktif = $request->get('aktif')[0];
        $old->update();
        $response = [
            "type" => "success",
            "text" => "Group menu berhasil diubah."
        ];

        return response()->json($response);
    }

}
