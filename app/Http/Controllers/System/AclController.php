<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response,DB;

class AclController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $acls = \App\SystemModel\SysRefAcl::all();
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        return view('system.acl.index', \compact('acls', 'modules', 'roles'));
    }

    public function formAdd()
    {
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        $groups = \App\SystemModel\SysRefGroupMenu::all();
        return view('system.acl.formAdd',\compact('modules', 'roles', 'groups'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'id_module'=>'required',
            'id_role'=>'required',
            'method'=>'required',
            'url'=>'required',
            'route'=>'required',
            'controller'=>'required',
            'function'=>'required',
            'middleware'=>'required',
        ]);

        $new = new \App\SystemModel\SysRefAcl();
        $new->id_module = \json_encode($request->get('id_module'));
        $new->id_role = \json_encode($request->get('id_role'));
        $new->id_group_menu = \json_encode($request->get('id_group_menu'));
        $new->is_menu = $request->get('is_menu')[0];
        $new->label = $request->get('label');
        $new->icon = $request->get('icon');
        $new->is_child = $request->get('is_child')[0];
        $new->parent = $request->get('parent');
        $new->method = \json_encode($request->get('method'));
        $new->url = $request->get('url');
        $new->route = $request->get('route');
        $new->controller = $request->get('controller');
        $new->function = $request->get('function');
        $new->middleware = \json_encode($request->get('middleware'));
        $new->urut = $request->get('urut');
        $new->save();
        $response = [
            "type" => "success",
            "text" => "Acl berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($idAcl)
    {
        $acl = \App\SystemModel\SysRefAcl::find($idAcl);
        $modules = \App\SystemModel\SysRefModule::all();
        $roles = \App\SystemModel\SysRefRole::all();
        $groups = \App\SystemModel\SysRefGroupMenu::all();
        return view('system.acl.formEdit', \compact("acl", "roles", "modules", "groups"));
    }

    public function edit(Request $request, $idAcl)
    {
        $request->validate([
            'id_module'=>'required',
            'id_role'=>'required',
            'method'=>'required',
            'url'=>'required',
            'route'=>'required',
            'controller'=>'required',
            'function'=>'required',
        ]);

        $old = \App\SystemModel\SysRefAcl::find($idAcl);
        $old->id_module = \json_encode($request->get('id_module'));
        $old->id_role = \json_encode($request->get('id_role'));
        $old->id_group_menu = \json_encode($request->get('id_group_menu'));
        $old->is_menu = $request->get('is_menu')[0];
        $old->label = $request->get('label');
        $old->icon = $request->get('icon');
        $old->is_child = $request->get('is_child')[0];
        $old->parent = $request->get('parent');
        $old->method = \json_encode($request->get('method'));
        $old->url = $request->get('url');
        $old->route = $request->get('route');
        $old->controller = $request->get('controller');
        $old->function = $request->get('function');
        $old->middleware = \json_encode($request->get('middleware'));
        $old->urut = $request->get('urut');
        $old->aktif = $request->get('aktif')[0];
        $old->update();
        $response = [
            "type" => "success",
            "text" => "Acl berhasil diubah."
        ];

        return response()->json($response);
    }

    public function delete($id)
    {
        $data = \App\SystemModel\SysRefAcl::find($id);
        $data->delete();

        return redirect('/system/acl')->with('success', 'Acl berhasil dihapus');
    }

}
