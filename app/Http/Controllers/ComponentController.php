<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use DataTables;

class ComponentController extends Controller
{
    public function slctLocation(Request $request)
    {
        $id = ($request->has('id')) ? $request->input('id') : null;
        $required = ($request->has('required')) ? $request->input('required') : 'Y';

        $datas = \App\ModelProp\RefLocation::all();
        return view('Component.slct_location', compact('datas', 'required', 'id'));
    }

    public function slctBlock(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $location_id = $request->input('location_id');
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefBlock::where('location_id', $location_id)->orderBy('name', 'ASC')->get();
        return view('Component.slct_block', compact('datas', 'required', 'id'));
    }

    public function slctUnit(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $block_id = $request->input('block_id');
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefUnit::with(['unitType'])->where('block_id', $block_id)->orderBy('number', 'ASC')->get();
        return view('Component.slct_unit', compact('datas', 'required', 'id'));
    }

    public function slctUnitReady(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $block_id = $request->input('block_id');
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefUnit::with(['unitType'])
            ->where('block_id', $block_id)
            ->where('status', 'READY')
            ->orderBy('number', 'ASC')
            ->get();

        return view('Component.slct_unit', compact('datas', 'required', 'id'));
    }

    public function slctUnitType(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefUnitType::all();
        return view('Component.slct_unit_type', compact('datas', 'required', 'id'));
    }

    public function slctUnitStatus(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefUnitStatus::all();
        return view('Component.slct_unit_status', compact('datas', 'required', 'id'));
    }

    public function slctMandor(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefMandor::with(['sdm'])->get();
        return view('Component.slct_mandor', compact('datas', 'required', 'id'));
    }

    public function slctStore(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $id = ($request->has('id')) ? $request->input('id') : null;

        $datas = \App\ModelProp\RefStore::with(['sdm'])->get();
        return view('Component.slct_store', compact('datas', 'required', 'id'));
    }

    public function slctSdm(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = \App\ModelProp\RefSdm::orderBy('name','asc')
                ->select('sdm_id','name', 'address')
                ->where('is_active', 'Y')
                ->limit(10)
                ->get();
        }else{
             $datas = \App\ModelProp\RefSdm::orderBy('name','asc')
                ->select('sdm_id','name', 'address')
                ->where('name', 'like', '%' .$search . '%')
                ->where('is_active', 'Y')
                ->limit(10)
                ->get();
        }

        $response = array();
        foreach($datas as $data){
            $address = ($data->address != "") ? " ($data->address)" : "";
            $response[] = array(
                "id" => "$data->sdm_id",
                "text" => $data->name . $address
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctSdmNotCustomer(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_customer', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_customer.sdm_id')
                ->whereNull('prop_ref_customer.sdm_id')
                ->get();
        }else{
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_customer', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_customer.sdm_id')
                ->where('prop_ref_sdm.name', 'like', '%' .$search . '%')
                ->whereNull('prop_ref_customer.sdm_id')
                ->get();
        }

        $response = array();
        foreach($datas as $data){
            $address = ($data->address != "") ? " ($data->address)" : "";
            $response[] = array(
                "id" => "$data->sdm_id",
                "text" => $data->name . $address
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctSdmNotMarketer(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_marketer', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_marketer.sdm_id')
                ->whereNull('prop_ref_marketer.sdm_id')
                ->get();
        }else{
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_marketer', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_marketer.sdm_id')
                ->where('prop_ref_sdm.name', 'like', '%' .$search . '%')
                ->whereNull('prop_ref_marketer.sdm_id')
                ->get();
        }

        $response = array();
        foreach($datas as $data){
            $address = ($data->address != "") ? " ($data->address)" : "";
            $response[] = array(
                "id" => "$data->sdm_id",
                "text" => $data->name . $address
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctSdmNotStore(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_store', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_store.sdm_id')
                ->whereNull('prop_ref_store.sdm_id')
                ->get();
        }else{
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_store', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_store.sdm_id')
                ->where('prop_ref_sdm.name', 'like', '%' .$search . '%')
                ->whereNull('prop_ref_store.sdm_id')
                ->get();
        }

        $response = array();
        foreach($datas as $data){
            $address = ($data->address != "") ? " ($data->address)" : "";
            $response[] = array(
                "id" => "$data->sdm_id",
                "text" => $data->name . $address
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctSdmNotMandor(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_mandor', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_mandor.sdm_id')
                ->whereNull('prop_ref_mandor.sdm_id')
                ->get();
        }else{
            $datas = DB::table('prop_ref_sdm')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_mandor', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_mandor.sdm_id')
                ->where('prop_ref_sdm.name', 'like', '%' .$search . '%')
                ->whereNull('prop_ref_mandor.sdm_id')
                ->get();
        }

        $response = array();
        foreach($datas as $data){
            $address = ($data->address != "") ? " ($data->address)" : "";
            $response[] = array(
                "id" => "$data->sdm_id",
                "text" => $data->name . $address
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctCustomer(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = DB::table('prop_ref_customer')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_sdm', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_customer.sdm_id')
                ->where('prop_ref_sdm.is_active', 'Y')
                ->limit(10)
                ->get();
        }else{
            $datas = DB::table('prop_ref_customer')
                ->select('prop_ref_sdm.sdm_id','prop_ref_sdm.name', 'prop_ref_sdm.address')
                ->leftJoin('prop_ref_sdm', 'prop_ref_sdm.sdm_id', '=', 'prop_ref_customer.sdm_id')
                ->where('prop_ref_sdm.name', 'like', '%' .$search . '%')
                ->where('prop_ref_sdm.is_active', 'Y')
                ->limit(10)
                ->get();
        }

        $response = array();
        foreach($datas as $data){
            $address = ($data->address != "") ? " (".$data->address.")" : "";
            $response[] = array(
                "id" => "$data->sdm_id",
                "text" => $data->name.$address
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctMarketer(Request $request){

        $search = $request->search;

        if($search == ''){
            $datas = \App\ModelProp\RefMarketer::with(['sdm'])
            ->get();
        }else{
            $datas = \App\ModelProp\RefMarketer::with(['sdm'])
            ->whereHas('sdm', function ($query) use($search) {
                return $query->where('name', 'like', "%$search%");
            })->get();
        }

        $response[] = [
            "id" => "",
            "text" => "** Pilih Marketer"
        ];

        foreach($datas as $data){
            $response[] = array(
                "id" => "$data->affiliate_num",
                "text" => $data->sdm->name . " (" . $data->affiliate_num . ")"
            );
        }

        echo json_encode($response);
        exit;
    }

    public function slctGoods(Request $request){

        $search = $request->search;
        $store_id = $request->get('store_id');
        $response = array();

        if($search == ''){
            if ($store_id != '') {
                $datas = \App\ModelProp\RefStoreGoods::with(['goods' => function ($query) {
                            $query->select('goods_id','name');
                            $query->where('is_active', 'Y');
                            $query->orderBy('name','asc');
                        }])
                        ->where('sdm_id', $store_id)
                        ->limit(10)
                        ->get();


                foreach($datas as $data){
                    $response[] = array(
                        "id" => $data->goods->goods_id,
                        "text" => $data->goods->name
                    );
                }
            } else {
                $datas = \App\ModelProp\RefGoods::select('goods_id', 'name as goods_name')
                        ->where("is_active", "Y")
                        ->orderBy("name", "ASC")
                        ->get();

                foreach($datas as $data){
                    $response[] = array(
                        "id" => "$data->goods_id",
                        "text" => $data->goods_name
                    );
                }
            }
        }else{
            if ($store_id != '') {
                $datas = \App\ModelProp\RefStoreGoods::with(['goods' => function ($query) use($search) {
                            $query->select('goods_id','name');
                            $query->where('name', 'like', '%' .$search . '%');
                            $query->where('is_active', 'Y');
                            $query->orderBy('name','asc');
                        }])
                        ->where('sdm_id', $store_id)
                        ->limit(10)
                        ->get();

                foreach($datas as $data){
                    $response[] = array(
                        "id" => $data->goods->goods_id,
                        "text" => $data->goods->name
                    );
                }
            } else {
                $datas = \App\ModelProp\RefGoods::select('goods_id', 'name as goods_name')
                        ->where('name', 'like', '%' .$search . '%')
                        ->where("is_active", "Y")
                        ->orderBy("name", "ASC")
                        ->get();

                foreach($datas as $data){
                    $response[] = array(
                        "id" => "$data->goods_id",
                        "text" => $data->goods_name
                    );
                }
            }

        }




        echo json_encode($response);
        exit;
    }

    public function getSdmById($sdm_id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);
        echo json_encode($sdm);
    }

    public function slctSubsidiType(Request $request)
    {
        $required = ($request->has('required')) ? $request->input('required') : 'Y';
        $id = ($request->has('id')) ? $request->input('id') : null;
        $type = ($request->has('type')) ? $request->input('type') : 'SUBSIDI';

        $datas = \App\ModelProp\RefSubsidiType::where('type', $type)->get();
        return view('Component.slct_subsidi_type', compact('datas', 'required', 'id'));
    }

}
