<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class HutangPiutangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('reports.hutang_piutang.index');
    }

    public function detail($sdm_id)
    {
        $user = \App\ModelProp\RefSdm::findOrFail($sdm_id);
        $dt_penjualan = [];
        $perusahaan_hutang = 0;
        $mereka_hutang = 0;


        // get kasbon
        $dt_kasbon = \App\ModelFinance\Kasbon::selectRaw("(kasbon_amount - kasbon_amount_paid) as jumlah, kasbon_date")
            ->where("kasbon_cancel", "0")
            ->where("kasbon_sdm_id", "$sdm_id")
            ->whereRaw("(kasbon_amount - kasbon_amount_paid) > 0")
            ->get();

        $mereka_hutang = $dt_kasbon->sum('jumlah');

        // get operasional
        $dt_operasional = \App\ModelFinance\Trnsct::selectRaw("(finance_amount - finance_amount_paid) as jumlah, finance_date, finance_desc")
            ->where("finance_cancel", "0")
            ->where("finance_sdm_id", "$sdm_id")
            ->where(DB::raw('finance_amount - finance_amount_paid'), '>', 0)
            ->get();

        $perusahaan_hutang = $dt_operasional->sum('jumlah');

        // get data penjualan cash
        $dt_penjualan_cash = \App\ModelProp\TrnsctSales::with('location', 'block')
            ->where("sales_type", "CASH")
            ->where("customer_id", "$sdm_id")
            ->get();

        $dt_penjualan_kpr_cust = \App\ModelProp\TrnsctSales::with('location', 'block')
            ->where("sales_type", "KPR")
            ->where("customer_id", "$sdm_id")
            ->get();

        $dt_penjualan_kpr_bank = \App\ModelProp\TrnsctSales::with('location', 'block')
            ->where("sales_type", "KPR")
            ->where("kpr_status", "ACCEPTED")
            ->where("kpr_bank", "$sdm_id")
            ->get();

        foreach ($dt_penjualan_cash as $key => $value) {
            $dt_penjualan[] = [
                "id" => $value->id,
                "payment_commitment" => $value->payment_commitment,
                "tgl_transaksi" => $value->sales_date,
                "unit" => $value->location->name . " " . $value->block->name . " No " . $value->unit_id,
                "sisa" => $value->ttl_trnsct - $value->ttl_trnsct_paid
            ];

            $mereka_hutang += $value->ttl_trnsct - $value->ttl_trnsct_paid;
        }

        foreach ($dt_penjualan_kpr_cust as $key => $value) {
            $dt_penjualan[] = [
                "id" => $value->id,
                "payment_commitment" => $value->payment_commitment,
                "tgl_transaksi" => $value->sales_date,
                "unit" => $value->location->name . " " . $value->block->name . " No " . $value->unit_id,
                "sisa" => ($value->ttl_trnsct - $value->kpr_credit_acc) - ($value->ttl_trnsct_paid - $value->kpr_credit_acc_paid)
            ];

            $mereka_hutang += ($value->ttl_trnsct - $value->kpr_credit_acc) - ($value->ttl_trnsct_paid - $value->kpr_credit_acc_paid);
        }

        foreach ($dt_penjualan_kpr_bank as $key => $value) {
            $dt_penjualan[] = [
                "id" => $value->id,
                "payment_commitment" => $value->payment_commitment,
                "tgl_transaksi" => $value->sales_date,
                "unit" => $value->location->name . " " . $value->block->name . " No " . $value->unit_id,
                "sisa" => $value->kpr_credit_acc - $value->kpr_credit_acc_paid
            ];

            $mereka_hutang += $value->kpr_credit_acc - $value->kpr_credit_acc_paid;
        }

        return view('reports.hutang_piutang.detail', compact('user', 'dt_kasbon', 'dt_operasional', 'dt_penjualan', 'mereka_hutang', 'perusahaan_hutang'));
    }

    public function total()
    {
        $mereka_hutang = 0;
        $perusahaan_hutang = 0;

        // mereka hutang
        $kasbon = $this->getKasbon();
        $penjualan_cash = $this->getPenjualanCash()['hasil'];
        $penjualan_kpr_cust = $this->getPenjualanKprCust()['hasil'];
        $penjualan_kpr_bank = $this->getPenjualanKprBank();

        $mereka_hutang = array_sum($kasbon)+array_sum($penjualan_cash)+array_sum($penjualan_kpr_cust)+array_sum($penjualan_kpr_bank);

        // perusahaan hutang
        $operasional = $this->getOperasional();
        $perusahaan_hutang = array_sum($operasional);

        $arr = [
            "mereka_hutang" => number_format($mereka_hutang,0,',','.'),
            "perusahaan_hutang" => number_format($perusahaan_hutang,0,',','.'),
        ];

        return response()->json($arr);
    }

    public function kasbon()
    {
        $dt_sdm = $this->getSdm();
        $kasbon = $this->getKasbon();

        foreach ($kasbon as $key => $value) {
            $dt_sdm[$key]["mereka_hutang"] = number_format($value,0,',','.');

        }

        foreach ($dt_sdm as $key => $value) {
            if ($dt_sdm[$value['sdm_id']]["mereka_hutang"] == '0') {
                unset($dt_sdm[$value['sdm_id']]);
            }
        }

        return DataTables::of($dt_sdm)
            ->addColumn('name_link', function($dt_sdm)
            {
                $sdm_id = $dt_sdm['sdm_id'];
                $name = $dt_sdm['name'];
                // edits link
                $btn = "<a href='" . url('reports/hutang_piutang/detail/'.$sdm_id) . "' >";
                $btn .= $name;
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['name_link'])
            ->toJson();

    }

    public function kasbonTotal()
    {
        $kasbon = $this->getKasbon();

        $arr = [
            "mereka_hutang" => number_format(array_sum($kasbon),0,',','.'),
            "perusahaan_hutang" => number_format(0,0,',','.'),
        ];

        return response()->json($arr);
    }

    public function penjualanCash()
    {
        $dt_sdm = $this->getSdm();
        $penjualan_cash = $this->getPenjualanCash()['hasil'];
        $tgl_bayar_terakhir = $this->getPenjualanCash()['tgl_bayar_terakhir'];

        foreach ($penjualan_cash as $key => $value) {
            $dt_sdm[$key]["mereka_hutang"] = $value;
        }

        foreach ($tgl_bayar_terakhir as $key => $value) {
            $dt_sdm[$key]["tgl_bayar_terakhir"] = $value;
        }

        unset($dt_sdm[0]);

        foreach ($dt_sdm as $value) {
            if ($dt_sdm[$value['sdm_id']]["mereka_hutang"] == '0') {
                unset($dt_sdm[$value['sdm_id']]);
            } else {
                $dt_sdm[$value['sdm_id']]["mereka_hutang"] = number_format($value['mereka_hutang'],0,',','.');
            }
        }

        return DataTables::of($dt_sdm)
            ->addColumn('name_link', function($dt_sdm)
            {
                $sdm_id = $dt_sdm['sdm_id'];
                $name = $dt_sdm['name'];
                // edits link
                $btn = "<a href='" . url('reports/hutang_piutang/detail/'.$sdm_id) . "' >";
                $btn .= $name;
                $btn .= "</a> ";

                return $btn;
            })
            ->addColumn('html_tgl_bayar_terakhir', function($dt_sdm){
                return date("d-m-Y", strtotime($dt_sdm['tgl_bayar_terakhir']));
            })
            ->rawColumns(['name_link', 'html_tgl_bayar_terakhir'])
            ->toJson();

    }

    public function penjualanTotalCash()
    {
        $penjualan_cash = $this->getPenjualanCash()['hasil'];

        $arr = [
            "mereka_hutang" => number_format(array_sum($penjualan_cash),0,',','.'),
            "perusahaan_hutang" => number_format(0,0,',','.'),
        ];

        return response()->json($arr);
    }

    public function penjualanKpr($type)
    {
        // type 1 = customer
        // type 2 = bank

        $dt_sdm = $this->getSdm();

        if ($type == '1') {
            $penjualan_kpr_cust = $this->getPenjualanKprCust()['hasil'];
            foreach ($penjualan_kpr_cust as $key => $value) {
                $dt_sdm[$key]["mereka_hutang"] = $dt_sdm[$key]["mereka_hutang"] + $value;
            }
        } else {
            $penjualan_kpr_bank = $this->getPenjualanKprBank();
            foreach ($penjualan_kpr_bank as $key => $value) {
                $jumlah = ($value == "" || $value == null) ? 0 : $value;
                $dt_sdm[$key]["mereka_hutang"] += $dt_sdm[$key]["mereka_hutang"] + $jumlah;
            }
        }

        $tgl_bayar_terakhir = $this->getPenjualanKprCust()['tgl_bayar_terakhir'];

        foreach ($dt_sdm as $key => $value) {
            if ($dt_sdm[$value['sdm_id']]["mereka_hutang"] == '0') {
                unset($dt_sdm[$value['sdm_id']]);
            } else {
                $dt_sdm[$value['sdm_id']]["mereka_hutang"] = number_format($value['mereka_hutang'],0,',','.');
                $dt_sdm[$value['sdm_id']]["tgl_bayar_terakhir"] = (array_key_exists($value['sdm_id'], $tgl_bayar_terakhir)) ? $tgl_bayar_terakhir[$value['sdm_id']] : '';
            }

        }


        return DataTables::of($dt_sdm)
            ->addColumn('name_link', function($dt_sdm)
            {
                $sdm_id = $dt_sdm['sdm_id'];
                $name = $dt_sdm['name'];
                // edits link
                $btn = "<a href='" . url('reports/hutang_piutang/detail/'.$sdm_id) . "' >";
                $btn .= $name;
                $btn .= "</a> ";

                return $btn;
            })
            ->addColumn('html_tgl_bayar_terakhir', function($dt_sdm){
                return date("d-m-Y", strtotime($dt_sdm['tgl_bayar_terakhir']));
            })
            ->rawColumns(['name_link', 'html_tgl_bayar_terakhir'])
            ->toJson();

    }

    public function penjualanTotalKpr($type)
    {
        // type 1 = customer
        // type 2 = bank

        $jumlah = 0;
        if ($type == '1') {
            $jumlah = array_sum($this->getPenjualanKprCust()['hasil']);
        } else {
            $jumlah = array_sum($this->getPenjualanKprBank());
        }

        $arr = [
            "mereka_hutang" => number_format($jumlah,0,',','.'),
            "perusahaan_hutang" => number_format(0,0,',','.'),
        ];

        return response()->json($arr);
    }

    public function operasional()
    {
        $dt_sdm = $this->getSdm();
        $operasional = $this->getOperasional();

        foreach ($operasional as $key => $value) {
            $dt_sdm[$key]["perusahaan_hutang"] = number_format($value,0,',','.');

        }

        foreach ($dt_sdm as $key => $value) {
            if ($dt_sdm[$value['sdm_id']]["perusahaan_hutang"] == '0') {
                unset($dt_sdm[$value['sdm_id']]);
            }
        }

        return DataTables::of($dt_sdm)
            ->addColumn('name_link', function($dt_sdm)
            {
                $sdm_id = $dt_sdm['sdm_id'];
                $name = $dt_sdm['name'];
                // edits link
                $btn = "<a href='" . url('reports/hutang_piutang/detail/'.$sdm_id) . "' >";
                $btn .= $name;
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['name_link'])
            ->toJson();

    }

    public function operasionalTotal()
    {
        $operasional = $this->getOperasional();

        $arr = [
            "perusahaan_hutang" => number_format(array_sum($operasional),0,',','.'),
            "mereka_hutang" => number_format(0,0,',','.'),
        ];

        return response()->json($arr);
    }

    public function getSdm()
    {
        $dt_sdm = \App\ModelProp\RefSdm::orderBy("name", "ASC")->get();

        $result = [];
        foreach ($dt_sdm as $key => $value) {
            $result[$value->sdm_id] = [
                "sdm_id" => $value->sdm_id,
                "name" => $value->name,
                "phone_num" => $value->phone_num,
                "institute" => $value->institute,
                "mereka_hutang" => 0,
                "perusahaan_hutang" => 0,
            ];
        }

        return $result;
    }

    public function getKasbon()
    {
        $result = \App\ModelFinance\Kasbon::with('sdm')
            ->where("kasbon_cancel", "0")
            ->get();

        $arr = [];
        foreach ($result as $key => $value) {
            $hasil = $value->kasbon_amount - $value->kasbon_amount_paid;
            if (array_key_exists($value->kasbon_sdm_id, $arr)) {
                $arr[$value->kasbon_sdm_id] += $hasil;
            } else {
                $arr[$value->kasbon_sdm_id] = $hasil;
            }
        }

        return $arr;
    }

    public function getPenjualanCash()
    {
        $result = \App\ModelProp\TrnsctSales::with(['payments' => function($q) {
                $q->orderBy('sales_paid_date', 'DESC');
            }])
            ->where("sales_type", "CASH")
            ->get();

        $arr = [];
        $arr_tanggal = [];

        foreach ($result as $key => $value) {
            $hasil = $value->ttl_trnsct - $value->ttl_trnsct_paid;
            if (array_key_exists($value->customer_id, $arr)) {
                $arr[$value->customer_id] += $hasil;
            } else {
                $arr[$value->customer_id] = $hasil;
            }
        }

        foreach ($result as $key => $value) {
            $arr_tanggal[$value->customer_id] = (count($value->payments) > 0) ? $value->payments[0]->sales_paid_date : '';
        }

        return [
            'hasil' => $arr,
            'tgl_bayar_terakhir' => $arr_tanggal,
        ];
    }

    public function getPenjualanKprCust()
    {
        $result = \App\ModelProp\TrnsctSales::with(['payments' => function($q) {
                $q->orderBy('sales_paid_date', 'DESC');
            }])
            ->where("sales_type", "KPR")
            ->where("kpr_status", "ACCEPTED")
            ->get();

        $arr = [];
        $arr_tanggal = [];

        foreach ($result as $key => $value) {
            $hasil = ($value->ttl_trnsct - $value->kpr_credit_acc) - ($value->ttl_trnsct_paid - $value->kpr_credit_acc_paid);
            if (array_key_exists($value->customer_id, $arr)) {
                $arr[$value->customer_id] += $hasil;
            } else {
                $arr[$value->customer_id] = $hasil;
            }
        }

        foreach ($result as $key => $value) {
            $arr_tanggal[$value->customer_id] = (count($value->payments) > 0) ? $value->payments[0]->sales_paid_date : '';
        }

        return [
            'hasil' => $arr,
            'tgl_bayar_terakhir' => $arr_tanggal,
        ];
    }

    public function getPenjualanKprBank()
    {
        $result = \App\ModelProp\TrnsctSales::where("sales_type", "KPR")
            ->where("kpr_status", "ACCEPTED")
            ->get();

        $arr = [];
        foreach ($result as $key => $value) {
            $hasil = $value->kpr_credit_acc - $value->kpr_credit_acc_paid;
            if (array_key_exists($value->kpr_bank, $arr)) {
                $arr[$value->kpr_bank] += $hasil;
            } else {
                $arr[$value->kpr_bank] = $hasil;
            }
        }

        return $arr;
    }

    public function getOperasional()
    {
        $result = \App\ModelFinance\Trnsct::where("finance_cancel", "0")->get();

        $arr = [];
        foreach ($result as $key => $value) {
            $hasil = $value->finance_amount - $value->finance_amount_paid;
            if (array_key_exists($value->finance_sdm_id, $arr)) {
                $arr[$value->finance_sdm_id] += $hasil;
            } else {
                $arr[$value->finance_sdm_id] = $hasil;
            }
        }

        return $arr;
    }
}
