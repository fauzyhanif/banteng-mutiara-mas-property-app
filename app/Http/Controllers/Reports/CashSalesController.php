<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class CashSalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $blocks = \App\ModelProp\RefBlock::with(['location'])->orderBy('name', 'ASC')->get();
        $steps = \App\ModelProp\RefSalesStep::where('type', 'CASH')->orderBy('orders')->get();

        $dt_sold = DB::table('prop_trnsct_sales')
            ->select(DB::raw("location_id, block_id, COUNT(*) AS total,
                    SUM(IF(ttl_trnsct_paid >= ttl_trnsct AND sales_type = 'CASH', 1, 0)) AS lunas,
                    SUM(IF(ttl_trnsct_paid < ttl_trnsct AND sales_type = 'CASH', 1, 0)) AS blm_lunas"))
            ->where('status', '!=', 'CANCEL')
            ->where('sales_type', 'CASH')
            ->groupBy('location_id', 'block_id')
            ->get();

        $res_sold = [];
        foreach ($dt_sold as $key => $value) {
            $res_sold[$value->location_id.'|'.$value->block_id] = [
                "sold" => $value->total,
                "lunas" => $value->lunas,
                "blm_lunas" => $value->blm_lunas,
            ];
        }

        $dt_step = DB::table('prop_trnsct_sales')
            ->select(DB::raw("location_id, block_id, step_sales, count(*) as total"))
            ->where('status', '!=', 'CANCEL')
            ->where('sales_type', 'CASH')
            ->groupBy('location_id', 'block_id', 'step_sales')
            ->get();


        $res_step = [];
        foreach ($dt_step as $key => $value) {
            $res_step[$value->location_id.'|'.$value->block_id."|".$value->step_sales] = $value->total;
        }

        return view('reports.cash_sales.index', compact('blocks', 'res_sold', 'steps', 'res_step'));
    }

    public function ListUnit(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $type = $request->get('type');
        $step_id = $request->get('step_id');

        return view('reports.cash_sales.list_unit', compact('location_id', 'block_id', 'type', 'step_id'));
    }

    public function listUnitJson(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $type = $request->get('type');
        $step_id = $request->get('step_id');

        if ($type == 'sold') {
            $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer'])
                ->where('status', '!=', 'CANCEL')
                ->where('location_id' , $location_id)
                ->where('block_id' , $block_id)
                ->where('sales_type' , 'CASH')
                ->orderBy('sales_id', 'DESC')
                ->get();
        } elseif ($type == 'lunas') {
            $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer'])
                ->where('status', '!=', 'CANCEL')
                ->where('location_id' , $location_id)
                ->where('block_id' , $block_id)
                ->where('sales_type' , 'CASH')
                ->where('ttl_trnsct_paid', '>=', DB::raw('ttl_trnsct'))
                ->orderBy('sales_id', 'DESC')
                ->get();
        } elseif ($type == 'blm_lunas') {
            $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer'])
                ->where('status', '!=', 'CANCEL')
                ->where('location_id' , $location_id)
                ->where('block_id' , $block_id)
                ->where('sales_type' , 'CASH')
                ->where('ttl_trnsct_paid', '<', DB::raw('ttl_trnsct'))
                ->orderBy('sales_id', 'DESC')
                ->get();
        } elseif ($type == 'step') {
            $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer'])
                ->where('status', '!=', 'CANCEL')
                ->where('location_id' , $location_id)
                ->where('block_id' , $block_id)
                ->where('sales_type' , 'CASH')
                ->where('step_sales', "$step_id")
                ->orderBy('sales_id', 'DESC')
                ->get();
        }

        return DataTables::of($datas)
            ->addColumn('type_number', function($datas){
                $text = "<span class='font-weight-bold'>".$datas->location->name . " " . $datas->block->name . " - " . $datas->unit_id . "</span><br>";

                return $text;
            })
            ->addColumn('customer', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text = "<span class='font-weight-bold'>".$datas->customer->name."</span><br>";
                    $text .= $datas->customer->institute;
                }

                return $text;
            })
            ->addColumn('customer_phone', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text .= $datas->customer->phone_num;
                }

                return $text;
            })
            ->addColumn('selling_price', function($datas){
                return number_format($datas->ttl_trnsct,2,',','.');
            })
            ->addColumn('sisa', function($datas){
                return number_format($datas->ttl_trnsct - $datas->ttl_trnsct_paid,2,',','.');
            })
            ->addColumn('ttl_trnsct_paid', function($datas){
                return number_format($datas->ttl_trnsct_paid,2,',','.');
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.sales.detail', $datas->id) . "' class='btn btn-primary btn-xs' target='_blank'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['type_number', 'customer', 'customer_phone', 'selling_price', 'sisa', 'ttl_trnsct_pai', 'actions_link'])
            ->toJson();

    }

}
