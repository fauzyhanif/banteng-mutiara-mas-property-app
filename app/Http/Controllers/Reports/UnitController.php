<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $blocks = \App\ModelProp\RefBlock::with(['location'])->orderBy('name', 'ASC')->get();

        $sales_status = DB::table('prop_ref_unit')
            ->select(DB::raw("location_id, block_id, count(*) as total, sum(if(status = 'READY',1,0)) as ready, sum(if(status = 'SOLD',1,0)) as sold, sum(if(status = 'BOOKED',1,0)) as booking"))
            ->groupBy('location_id', 'block_id')
            ->get();

        // olah data untuk jml brp unit yg terjual dan ready
        $res_sales_status = [];
        foreach ($sales_status as $key => $value) {
            $res_sales_status[$value->location_id.'|'.$value->block_id] = [
                "total" => $value->total,
                "ready" => $value->ready,
                "sold" => $value->sold,
                "booking" => $value->booking,
            ];
        }

        // persiapan olah data untuk jml brp unit yg masih dibangun dan selesai
        $res_development_status = [];
        foreach ($blocks as $key => $value) {
            $res_development_status[$value->location_id.'|'.$value->block_id] = [
                "selesai" => 0,
                "blm_selesai" => 0
            ];
        }

        $development_status = DB::table('prop_ref_worklist')
            ->select(DB::raw("location_id, block_id, count(*) as jml_job, sum(progress) as jml_progress"))
            ->groupBy('unit_id')
            ->get();

        // olah data untuk jml brp unit yg masih dibangun dan selesai
        foreach ($development_status as $key => $value) {
            if ($value->jml_progress / $value->jml_job >= 100) {
                $res_development_status[$value->location_id.'|'.$value->block_id]['selesai'] += 1;
            } else {
                $res_development_status[$value->location_id.'|'.$value->block_id]['blm_selesai'] += 1;
            }
        }

        return view('reports.unit.index', compact('blocks', 'res_sales_status', 'res_development_status'));
    }

    public function ListUnit(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $type = $request->get('type');

        return view('reports.unit.list_unit', compact('location_id', 'block_id', 'type'));
    }

    public function listUnitJson(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $type = $request->get('type');

        $sales = \App\ModelProp\TrnsctSales::where('location_id', $location_id)
            ->where('block_id', "$block_id")
            ->where('status', '!=', 'CANCEL')
            ->get();

        $res_sales = [];
        foreach ($sales as $key => $value) {
            $res_sales[$value->location_id."|".$value->block_id."|".$value->unit_id] = [
                "ttl_trnsct_paid" => $value->ttl_trnsct_paid,
                "sisa" => $value->ttl_trnsct - $value->ttl_trnsct_paid,
            ];
        }

        if ($type == 'all') {
            $datas = \App\ModelProp\RefUnit::with('UnitType', 'location', 'block', 'customer')
                ->where('location_id', $location_id)
                ->where('block_id', "$block_id")
                ->orderBy('number', 'ASC')
                ->get();

            return $this->getDataJsonByNonProgress($block_id, $type, $datas, $res_sales);
        } elseif ($type == 'READY') {
            $datas = \App\ModelProp\RefUnit::with('UnitType', 'location', 'block', 'customer')
                ->where('location_id', $location_id)
                ->where('block_id', "$block_id")
                ->where("status", "$type")
                ->orderBy('number', 'ASC')
                ->get();

            return $this->getDataJsonByNonProgress($block_id, $type, $datas, $res_sales);
        } elseif ($type == 'SOLD') {
            $datas = \App\ModelProp\TrnsctSales::with(['unit.unitType', 'customer'])
                ->where('location_id', $location_id)
                ->where('block_id', "$block_id")
                ->where('status', '!=', 'CANCEL')
                ->orderBy('unit_id', 'ASC')
                ->get();

            return DataTables::of($datas)
                ->addColumn('type_number', function($datas){
                    $text = "<span class='font-weight-bold'>$datas->unit_id</span><br>";
                    $text .= $datas->unit->unitType->name;

                    return $text;
                })
                ->addColumn('customer', function($datas){
                    $text = "";
                    if ($datas->customer_id != "") {
                        $text = "<span class='font-weight-bold'>".$datas->customer->name."</span><br>";
                        $text .= $datas->customer->institute;
                    }

                    return $text;
                })
                ->addColumn('customer_phone', function($datas){
                    $text = "";
                    if ($datas->customer_id != "") {
                        $text .= $datas->customer->phone_num;
                    }

                    return $text;
                })
                ->addColumn('selling_price', function($datas){
                    return number_format($datas->ttl_trnsct,2,',','.');
                })
                ->addColumn('sisa', function($datas) use($res_sales){
                    return number_format($datas->ttl_trnsct - $datas->ttl_trnsct_paid,2,',','.');
                })
                ->addColumn('ttl_trnsct_paid', function($datas) use($res_sales){
                    return number_format($datas->ttl_trnsct_paid,2,',','.');
                })
                ->rawColumns(['type_number', 'customer', 'customer_phone', 'selling_price', 'sisa', 'ttl_trnsct_paid'])
                ->toJson();
        } elseif ($type == 'BOOKING') {
            $datas = \App\ModelProp\TrnsctSales::with(['unit.unitType', 'customer'])
                ->where('block_id', "$block_id")
                ->where('status', 'BOOKING')
                ->orderBy('unit_id', 'ASC')
                ->get();

            return DataTables::of($datas)
                ->addColumn('type_number', function($datas){
                    $text = "<span class='font-weight-bold'>$datas->unit_id</span><br>";
                    $text .= $datas->unit->unitType->name;

                    return $text;
                })
                ->addColumn('customer', function($datas){
                    $text = "";
                    if ($datas->customer_id != "") {
                        $text = "<span class='font-weight-bold'>".$datas->customer->name."</span><br>";
                        $text .= $datas->customer->institute;
                    }

                    return $text;
                })
                ->addColumn('customer_phone', function($datas){
                    $text = "";
                    if ($datas->customer_id != "") {
                        $text .= $datas->customer->phone_num;
                    }

                    return $text;
                })
                ->addColumn('selling_price', function($datas){
                    return number_format($datas->ttl_trnsct,2,',','.');
                })
                ->addColumn('sisa', function($datas) use($res_sales){
                    return number_format($datas->ttl_trnsct - $datas->ttl_trnsct_paid,2,',','.');
                })
                ->addColumn('ttl_trnsct_paid', function($datas) use($res_sales){
                    return number_format($datas->ttl_trnsct_paid,2,',','.');
                })
                ->rawColumns(['type_number', 'customer', 'customer_phone', 'selling_price', 'sisa', 'ttl_trnsct_paid'])
                ->toJson();

        } elseif ($type == 'ON_PROGRESS') {
            $datas = DB::table('prop_ref_worklist')
                ->select('prop_ref_unit.number', 'prop_ref_unit_type.name as type_name', 'prop_ref_sdm.name as cust_name', 'prop_ref_sdm.institute', 'prop_ref_sdm.phone_num',
                        'prop_ref_unit.harga_jual_cash', 'prop_ref_unit.harga_jual_kpr', 'prop_ref_unit.customer_id', 'prop_ref_unit.block_id', 'prop_ref_unit.status')
                ->leftJoin('prop_ref_unit', 'prop_ref_worklist.unit_id', '=', 'prop_ref_unit.number')
                ->leftJoin('prop_ref_unit_type', 'prop_ref_unit.unittype_id', '=', 'prop_ref_unit_type.unittype_id')
                ->leftJoin('prop_ref_sdm', 'prop_ref_unit.customer_id', '=', 'prop_ref_sdm.sdm_id')
                ->where('prop_ref_worklist.location_id', $location_id)
                ->where('prop_ref_worklist.block_id', $block_id)
                ->where('prop_ref_worklist.progress', '<', '100')
                ->groupBy('prop_ref_worklist.location_id','prop_ref_worklist.block_id','prop_ref_worklist.unit_id')
                ->orderBy('prop_ref_unit.number')
                ->get();

            return $this->getDataJsonByProgress($block_id, $type, $datas, $res_sales);
        }

    }

    public function getDataJsonByNonProgress($block_id, $type, $datas, $res_sales)
    {
        return DataTables::of($datas)
            ->addColumn('type_number', function($datas){
                $text = "<span class='font-weight-bold'>$datas->number</span><br>";
                $text .= $datas->unitType->name;

                return $text;
            })
            ->addColumn('customer', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text = "<span class='font-weight-bold'>".$datas->customer->name."</span><br>";
                    $text .= $datas->customer->institute;
                }

                return $text;
            })
            ->addColumn('customer_phone', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text .= $datas->customer->phone_num;
                }

                return $text;
            })
            ->addColumn('selling_price', function($datas){
                return "Cash: " . number_format($datas->harga_jual_cash,2,',','.') . " <br>KPR: " . number_format($datas->harga_jual_kpr,2,',','.');
            })
            ->addColumn('sisa', function($datas) use($res_sales){
                $res = "0.00";
                if (array_key_exists($datas->block_id."|".$datas->number, $res_sales)) {
                    $res = number_format($res_sales[$datas->block_id."|".$datas->number]['sisa'],2,',','.');
                }
                return $res;
            })
            ->addColumn('ttl_trnsct_paid', function($datas) use($res_sales){
                $res = "0.00";
                if (array_key_exists($datas->block_id."|".$datas->number, $res_sales)) {
                    $res = number_format($res_sales[$datas->block_id."|".$datas->number]['ttl_trnsct_paid'],2,',','.');
                }
                return $res;
            })
            ->rawColumns(['type_number', 'customer', 'customer_phone', 'selling_price', 'sisa', 'ttl_trnsct_paid'])
            ->toJson();
    }

    public function getDataJsonByProgress($block_id, $type, $datas, $res_sales)
    {
        return DataTables::of($datas)
            ->addColumn('type_number', function($datas){
                $text = "<span class='font-weight-bold'>".$datas->number."</span><br>";
                $text .= $datas->type_name;

                return $text;
            })
            ->addColumn('customer', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text = "<span class='font-weight-bold'>".$datas->cust_name."</span><br>";
                    $text .= $datas->institute;
                }

                return $text;
            })
            ->addColumn('customer_phone', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text .= $datas->phone_num;
                }

                return $text;
            })
            ->addColumn('selling_price', function($datas){
                return "Cash: " . number_format($datas->harga_jual_cash,2,',','.') . " <br>KPR: " . number_format($datas->harga_jual_kpr,2,',','.');
            })
            ->addColumn('sisa', function($datas) use($res_sales){
                $res = "0.00";
                if (array_key_exists($datas->block_id."|".$datas->number, $res_sales)) {
                    $res = number_format($res_sales[$datas->block_id."|".$datas->number]['sisa'],2,',','.');
                }
                return $res;
            })
            ->addColumn('ttl_trnsct_paid', function($datas) use($res_sales){
                $res = "0.00";
                if (array_key_exists($datas->block_id."|".$datas->number, $res_sales)) {
                    $res = number_format($res_sales[$datas->block_id."|".$datas->number]['ttl_trnsct_paid'],2,',','.');
                }
                return $res;
            })
            ->rawColumns(['type_number', 'customer', 'customer_phone', 'selling_price', 'sisa', 'ttl_trnsct_paid'])
            ->toJson();
    }

}
