<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class AccountingController extends Controller
{
    private $hpp_pajak;

    public function __construct()
    {
        $this->middleware('auth');
        $this->hpp_pajak = 150500000;
    }

    public function index()
    {
        return view('reports.accounting.index');
    }

    public function labaRugi()
    {
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-d');

        return view('reports.accounting.laba_rugi', compact('start_date', 'end_date'));
    }

    public function labaRugiDetail(Request $request)
    {
        $jenis_laporan = $request->get('jenis_laporan');
        $start_date = explode(" - ", $request->get('date'))[0];
        $end_date = explode(" - ", $request->get('date'))[1];

        $hpp_pajak = $this->hpp_pajak;

        $penjualan = $this->getPenjualan($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $hpp = $this->getHpp($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $persediaan_akhir = $this->getPersediaanBangunan($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $biaya = $this->getBiaya($start_date, $end_date);

        return view('reports.accounting.laba_rugi.detail',
            compact(
                'jenis_laporan',
                'start_date',
                'end_date',
                'penjualan',
                'hpp',
                'persediaan_akhir',
                'biaya'
            )
        );
    }

    public function labaRugiDetailTransaction(Request $request)
    {
        $type = $request->get('type');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $jenis_laporan = $request->get('jenis_laporan');
        $hpp_pajak = $this->hpp_pajak;

        if ($type == 'PENJUALAN_RUMAH_SUBSIDI') {
            $data_kpr = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->whereBetween('prop_trnsct_sales.step_sales_date', ["$start_date", "$end_date"])
                ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
                ->where('prop_ref_unit.jenis_rumah', '=', 'SUBSIDI')
                ->where('prop_trnsct_sales.sales_type', '=', 'KPR')
                ->where('prop_trnsct_sales.kpr_status', 'ACCEPTED')
                ->where(function($query){
                    $query->where('prop_trnsct_sales.step_sales', '=', '6');
                    $query->orWhere('prop_trnsct_sales.step_sales', '=', '16');
                    $query->orWhere('prop_trnsct_sales.step_sales', '=', '20');
                    $query->orWhere('prop_trnsct_sales.step_sales', '=', '21');
                })
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();


            // peredaran usaha subsidi CASH
            $data_cash = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->whereBetween('prop_trnsct_sales.sales_date', ["$start_date", "$end_date"])
                ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
                ->where('prop_ref_unit.jenis_rumah', '=', 'SUBSIDI')
                ->where('prop_trnsct_sales.sales_type', '=', 'CASH')
                ->where('prop_trnsct_sales.ttl_trnsct', '<=', DB::raw('prop_trnsct_sales.ttl_trnsct_paid'))
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();

            return view('reports.accounting.laba_rugi.detail_penjualan',
                compact('type', 'hpp_pajak', 'jenis_laporan', 'start_date', 'end_date', 'data_kpr', 'data_cash')
            );
        }

        if ($type == 'PENJUALAN_RUMAH_NONSUBSIDI') {
            $data_kpr = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->whereBetween('prop_trnsct_sales.step_sales_date', ["$start_date", "$end_date"])
                ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
                ->where('prop_ref_unit.jenis_rumah', '=', 'NONSUBSIDI')
                ->where('prop_trnsct_sales.sales_type', '=', 'KPR')
                ->where('prop_trnsct_sales.kpr_status', 'ACCEPTED')
                ->where(function($query){
                    $query->where('prop_trnsct_sales.step_sales', '=', '6');
                    $query->orWhere('prop_trnsct_sales.step_sales', '=', '16');
                    $query->orWhere('prop_trnsct_sales.step_sales', '=', '20');
                    $query->orWhere('prop_trnsct_sales.step_sales', '=', '21');
                })
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();


            // peredaran usaha subsidi CASH
            $data_cash = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                    ,"prop_trnsct_sales.ttl_trnsct_for_pajak"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->whereBetween('prop_trnsct_sales.sales_date', ["$start_date", "$end_date"])
                ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
                ->where('prop_ref_unit.jenis_rumah', '=', 'NONSUBSIDI')
                ->where('prop_trnsct_sales.sales_type', '=', 'CASH')
                ->where('prop_trnsct_sales.ttl_trnsct', '<=', DB::raw('prop_trnsct_sales.ttl_trnsct_paid'))
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();

            return view('reports.accounting.laba_rugi.detail_penjualan',
                compact('type', 'hpp_pajak', 'jenis_laporan', 'start_date', 'end_date', 'data_kpr', 'data_cash')
            );
        }

        if ($type == 'PERSEDIAAN_AWAL') {
            $data = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->where('prop_trnsct_sales.sales_type', 'KPR')
                ->where('prop_trnsct_sales.status', '!=','CANCEL')
                ->where("prop_trnsct_sales.sales_date", "<=", "$start_date")
                ->where(function($query){
                    $query->where('prop_trnsct_sales.step_sales', '!=', '6');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '16');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '20');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '21');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '24');
                })
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();

            return view('reports.accounting.laba_rugi.detail_persediaan_awal',
                compact('type', 'hpp_pajak', 'jenis_laporan', 'start_date', 'end_date', 'data')
            );
        }

        if ($type == 'PENGADAAN_MATERIAL') {
            $data = DB::table('fin_trnsct_item')
                ->select('fin_trnsct.finance_desc', 'fin_trnsct.finance_date', 'prop_ref_sdm.name as sdm_name', 'fin_trnsct_item.finance_item_amount')
                ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
                ->leftJoin('prop_ref_sdm', 'fin_trnsct.finance_sdm_id', '=', 'prop_ref_sdm.sdm_id')
                ->where(function($query){
                    $query->where('fin_trnsct_item.finance_item_account_id', '3');
                    $query->orWhere('fin_trnsct_item.finance_item_account_id', '4');
                    $query->orWhere('fin_trnsct_item.finance_item_account_id', '6');
                })
                ->where('fin_trnsct.finance_cancel', '0')
                ->whereBetween('fin_trnsct.finance_date', ["$start_date", "$end_date"])
                ->orderBy('fin_trnsct.finance_date')
                ->get();

            return view('reports.accounting.laba_rugi.detail_pengadaan_material',
                compact('type', 'jenis_laporan', 'start_date', 'end_date', 'data')
            );
        }

        if ($type == 'PERSEDIAAN_AKHIR_STOK') {
            $dt_persediaan_awal = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->where('prop_trnsct_sales.sales_type', 'KPR')
                ->where('prop_trnsct_sales.status', '!=','CANCEL')
                ->where("prop_trnsct_sales.sales_date", "<=", "$start_date")
                ->where(function($query){
                    $query->where('prop_trnsct_sales.step_sales', '!=', '6');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '16');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '20');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '21');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '24');
                })
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();

            return view('reports.accounting.laba_rugi.detail_persediaan_akhir_stok',
                compact('type', 'hpp_pajak', 'jenis_laporan', 'start_date', 'end_date', 'dt_persediaan_awal')
            );
        }

        if ($type == 'PERSEDIAAN_AKHIR_PENJUALAN_CASH') {
            $data = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->whereBetween('prop_trnsct_sales.sales_date', ["$start_date", "$end_date"])
                ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
                ->where('prop_trnsct_sales.sales_type', '=', 'CASH')
                ->where('prop_trnsct_sales.ttl_trnsct', '<=', DB::raw('prop_trnsct_sales.ttl_trnsct_paid'))
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();

            return view('reports.accounting.laba_rugi.detail_persediaan_akhir_penjualan_cash',
                compact('type', 'hpp_pajak', 'jenis_laporan', 'start_date', 'end_date', 'data')
            );
        }

        if ($type == 'PERSEDIAAN_AKHIR_PENJUALAN_KPR') {
            $data = DB::table('prop_trnsct_sales')
                ->select(
                    "prop_trnsct_sales.sales_date", "prop_ref_block.name as block_name", "prop_trnsct_sales.unit_id",
                    "prop_ref_sdm.name as sdm_name", "prop_ref_unit_type.name as type_name", "prop_trnsct_sales.ttl_trnsct"
                )
                ->leftJoin("prop_ref_block", "prop_trnsct_sales.block_id", "=", "prop_ref_block.block_id")
                ->leftJoin("prop_ref_sdm", "prop_trnsct_sales.customer_id", "=", "prop_ref_sdm.sdm_id")
                ->leftJoin('prop_ref_unit', function($join){
                    $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                    $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                    $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
                })
                ->leftJoin("prop_ref_unit_type", "prop_ref_unit.unittype_id", "=", "prop_ref_unit_type.unittype_id")
                ->whereBetween('prop_trnsct_sales.step_sales_date', ["$start_date", "$end_date"])
                ->where('prop_trnsct_sales.sales_type', 'KPR')
                ->where('prop_trnsct_sales.status', '!=','CANCEL')
                ->where('prop_trnsct_sales.status', '!=','REJECTED')
                ->where(function($query){
                    $query->where('prop_trnsct_sales.step_sales', '!=', '6');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '16');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '20');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '21');
                    $query->where('prop_trnsct_sales.step_sales', '!=', '24');
                })
                ->orderBy('prop_trnsct_sales.sales_date')
                ->orderBy('prop_ref_block.name')
                ->get();

            return view('reports.accounting.laba_rugi.detail_persediaan_akhir_penjualan_kpr',
                compact('type', 'hpp_pajak', 'jenis_laporan', 'start_date', 'end_date', 'data')
            );
        }

        if ($type == 'BIAYA') {
            $account_id = $request->get('account_id');

            $account = \App\ModelFinance\RefAccountAccounting::findOrFail($account_id);

            $data = DB::table('fin_ref_account_accounting')
                ->select('fin_trnsct.finance_desc', 'fin_trnsct.finance_date', 'prop_ref_sdm.name as sdm_name', 'fin_trnsct_item.finance_item_amount')
                ->leftJoin('fin_ref_account', 'fin_ref_account_accounting.account_id', '=', 'fin_ref_account.account_accounting_id')
                ->leftJoin('fin_trnsct_item', 'fin_ref_account.account_id', '=', 'fin_trnsct_item.finance_item_account_id')
                ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
                ->leftJoin('prop_ref_sdm', 'fin_trnsct.finance_sdm_id', '=', 'prop_ref_sdm.sdm_id')
                ->whereBetween('fin_trnsct.finance_date', ["$start_date", "$end_date"])
                ->where('fin_trnsct.finance_cancel', '=', '0')
                ->where('fin_ref_account.account_accounting_id', '=', "$account_id")
                ->get();

            return view('reports.accounting.laba_rugi.detail_biaya',
                compact('type', 'account', 'start_date', 'end_date', 'data')
            );
        }

    }


    public function labaRugiPrint(Request $request)
    {
        $jenis_laporan = $request->get('jenis_laporan');
        $start_date = explode(" - ", $request->get('date'))[0];
        $end_date = explode(" - ", $request->get('date'))[1];

        $hpp_pajak = $this->hpp_pajak;

        $penjualan = $this->getPenjualan($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $hpp = $this->getHpp($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $persediaan_akhir = $this->getPersediaanBangunan($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $biaya = $this->getBiaya($start_date, $end_date);

        return view('reports.accounting.laba_rugi_print',
            compact(
                'jenis_laporan',
                'start_date',
                'end_date',
                'penjualan',
                'hpp',
                'persediaan_akhir',
                'biaya'
            )
        );
    }

    public function getPenjualan($start_date, $end_date, $hpp_pajak, $jenis_laporan)
    {
        // set variable
        $penjualan['jml_penjualan_rumah_subsidi'] = 0;
        $penjualan['jml_penjualan_rumah_nonsubsidi'] = 0;

        // peredaran usaha subsidi KPR
        $peredaran_usaha_subsidi_kpr = DB::table('prop_trnsct_sales')
            ->select(
                DB::raw("
                    prop_ref_unit.jenis_rumah,
                    count(prop_trnsct_sales.unit_id) jml_unit,
                    SUM($hpp_pajak) as jml_penjualan_pajak,
                    SUM(prop_trnsct_sales.ttl_trnsct) jml_penjualan_inhouse"
                )
            )
            ->leftJoin('prop_ref_unit', function($join){
                $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
            })
            ->whereBetween('prop_trnsct_sales.step_sales_date', ["$start_date", "$end_date"])
            ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
            ->where('prop_ref_unit.jenis_rumah', '=', 'SUBSIDI')
            ->where('prop_trnsct_sales.sales_type', '=', 'KPR')
            ->where('prop_trnsct_sales.kpr_status', 'ACCEPTED')
            ->where(function($query){
                $query->where('prop_trnsct_sales.step_sales', '=', '6');
                $query->orWhere('prop_trnsct_sales.step_sales', '=', '16');
                $query->orWhere('prop_trnsct_sales.step_sales', '=', '20');
                $query->orWhere('prop_trnsct_sales.step_sales', '=', '21');
            })
            ->groupBy('prop_ref_unit.jenis_rumah')
            ->get();


        // peredaran usaha subsidi CASH
        $peredaran_usaha_subsidi_cash = DB::table('prop_trnsct_sales')
            ->select(
                DB::raw("
                    prop_ref_unit.jenis_rumah,
                    count(prop_trnsct_sales.unit_id) jml_unit,
                    SUM($hpp_pajak) as jml_penjualan_pajak,
                    SUM(prop_trnsct_sales.ttl_trnsct) jml_penjualan_inhouse"
                )
            )
            ->leftJoin('prop_ref_unit', function($join){
                $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
            })
            ->whereBetween('prop_trnsct_sales.sales_date', ["$start_date", "$end_date"])
            ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
            ->where('prop_ref_unit.jenis_rumah', '=', 'SUBSIDI')
            ->where('prop_trnsct_sales.sales_type', '=', 'CASH')
            ->where('prop_trnsct_sales.ttl_trnsct', '<=', DB::raw('prop_trnsct_sales.ttl_trnsct_paid'))
            ->groupBy('prop_ref_unit.jenis_rumah')
            ->get();


        // peredaran usaha nonsubsidi KPR
        $peredaran_usaha_nonsubsidi_kpr = DB::table('prop_trnsct_sales')
            ->select(
                DB::raw("
                    prop_ref_unit.jenis_rumah,
                    prop_trnsct_sales.unit_id jml_unit,
                    SUM(prop_trnsct_sales.ttl_trnsct) as jml_penjualan
                ")
            )
            ->leftJoin('prop_ref_unit', function($join){
                $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
            })
            ->whereBetween('prop_trnsct_sales.step_sales_date', ["$start_date", "$end_date"])
            ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
            ->where('prop_ref_unit.jenis_rumah', '=', 'NONSUBSIDI')
            ->where('prop_trnsct_sales.sales_type', '=', 'KPR')
            ->where('prop_trnsct_sales.kpr_status', 'ACCEPTED')
            ->where(function($query){
                $query->where('prop_trnsct_sales.step_sales', '=', '6');
                $query->orWhere('prop_trnsct_sales.step_sales', '=', '16');
                $query->orWhere('prop_trnsct_sales.step_sales', '=', '20');
                $query->orWhere('prop_trnsct_sales.step_sales', '=', '21');
            })
            ->groupBy('prop_ref_unit.jenis_rumah')
            ->get();

        // peredaran usaha nonsubsidi CASH
        $peredaran_usaha_nonsubsidi_cash = DB::table('prop_trnsct_sales')
            ->select(
                DB::raw("
                    prop_ref_unit.jenis_rumah,
                    prop_trnsct_sales.unit_id jml_unit,
                    SUM(prop_trnsct_sales.ttl_trnsct) as jml_penjualan,
                    SUM(prop_trnsct_sales.ttl_trnsct_for_pajak) as jml_penjualan_pajak
                ")
            )
            ->leftJoin('prop_ref_unit', function($join){
                $join->on('prop_trnsct_sales.location_id', '=', 'prop_ref_unit.location_id');
                $join->on('prop_trnsct_sales.block_id', '=', 'prop_ref_unit.block_id');
                $join->on('prop_trnsct_sales.unit_id', '=', 'prop_ref_unit.number');
            })
            ->whereBetween('prop_trnsct_sales.sales_date', ["$start_date", "$end_date"])
            ->where('prop_trnsct_sales.status', '!=', 'CANCEL')
            ->where('prop_ref_unit.jenis_rumah', '=', 'NONSUBSIDI')
            ->where('prop_trnsct_sales.sales_type', '=', 'CASH')
            ->where('prop_trnsct_sales.ttl_trnsct', '<=',DB::raw('prop_trnsct_sales.ttl_trnsct_paid'))
            ->groupBy('prop_ref_unit.jenis_rumah')
            ->get();


        foreach ($peredaran_usaha_subsidi_kpr as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $penjualan['jml_penjualan_rumah_subsidi'] += $value->jml_penjualan_pajak;
            } else {
                $penjualan['jml_penjualan_rumah_subsidi'] += $value->jml_penjualan_inhouse;
            }
        }

        foreach ($peredaran_usaha_subsidi_cash as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $penjualan['jml_penjualan_rumah_subsidi'] += $value->jml_penjualan_pajak;
            } else {
                $penjualan['jml_penjualan_rumah_subsidi'] += $value->jml_penjualan_inhouse;
            }
        }

        foreach ($peredaran_usaha_nonsubsidi_kpr as $key => $value) {
            $penjualan['jml_penjualan_rumah_nonsubsidi'] += $value->jml_penjualan;
        }

        foreach ($peredaran_usaha_nonsubsidi_cash as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $penjualan['jml_penjualan_rumah_nonsubsidi'] += $value->jml_penjualan_pajak;
            } else {
                $penjualan['jml_penjualan_rumah_nonsubsidi'] += $value->jml_penjualan;
            }
        }

        return $penjualan;
    }

    public function getHpp($start_date, $end_date, $hpp_pajak, $jenis_laporan)
    {
        // set variable
        $hpp = [];
        $hpp['persediaan_awal'] = 0;
        $hpp['pengadaan_material'] = 0;
        $hpp['persediaan_akhir'] = 0;
        $penjualan_bulan_berjalan = 0;

        $persediaan_awal = DB::table('prop_trnsct_sales')
            ->select(
                DB::raw("
                    COUNT(sales_id) as jml_unit,
                    SUM($hpp_pajak) as jml_penjualan_pajak,
                    SUM(prop_trnsct_sales.ttl_trnsct) as jml_penjualan_inhouse
                ")
            )
            ->where('sales_type', 'KPR')
            ->where('status', '!=','CANCEL')
            ->where("sales_date", "<=", "$start_date")
            ->where(function($query){
                $query->where('step_sales', '!=', '6');
                $query->where('step_sales', '!=', '16');
                $query->where('step_sales', '!=', '20');
                $query->where('step_sales', '!=', '21');
                $query->where('step_sales', '!=', '24');
            })
            ->groupBy('sales_type')
            ->get();

        foreach ($persediaan_awal as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $hpp['persediaan_awal'] += $value->jml_penjualan_pajak;
            } else {
                $hpp['persediaan_awal'] += $value->jml_penjualan_inhouse;
            }
        }

        $pengadaan_material = DB::table('fin_trnsct_item')
            ->select(DB::raw("sum(fin_trnsct_item.finance_item_amount) as jml_pengadaan"))
            ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
            ->where(function($query){
                $query->where('fin_trnsct_item.finance_item_account_id', '3');
                $query->orWhere('fin_trnsct_item.finance_item_account_id', '4');
                $query->orWhere('fin_trnsct_item.finance_item_account_id', '6');
            })
            ->where('fin_trnsct.finance_cancel', '0')
            ->whereBetween('fin_trnsct.finance_date', ["$start_date", "$end_date"])
            ->groupBy('fin_trnsct_item.finance_item_account_id')
            ->get();

        foreach ($pengadaan_material as $key => $value) {
            $hpp['pengadaan_material'] += $value->jml_pengadaan;
        }

        $penjualan_kpr = DB::table('prop_trnsct_sales')
            ->whereBetween('step_sales_date', ["$start_date", "$end_date"])
            ->where('sales_type', 'KPR')
            ->where('status', '!=','CANCEL')
            ->where('kpr_status', 'ACCEPTED')
            ->where(function($query){
                $query->where('step_sales', '=', '6');
                $query->orWhere('step_sales', '=', '16');
                $query->orWhere('step_sales', '=', '20');
                $query->orWhere('step_sales', '=', '21');
            })
            ->get();

        foreach ($penjualan_kpr as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $penjualan_bulan_berjalan += $hpp_pajak;
            } else {
                $penjualan_bulan_berjalan += $value->ttl_trnsct;
            }
        }

        $penjualan_cash = DB::table('prop_trnsct_sales')
            ->where('sales_type', 'CASH')
            ->where('status', '!=','CANCEL')
            ->whereBetween('sales_date', ["$start_date", "$end_date"])
            ->where('ttl_trnsct', '<=',DB::raw('ttl_trnsct_paid'))
            ->get();


        foreach ($penjualan_cash as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $penjualan_bulan_berjalan += $hpp_pajak;
            } else {
                $penjualan_bulan_berjalan += $value->ttl_trnsct;
            }
        }

        $hpp['persediaan_akhir'] = $hpp['persediaan_awal'] - $penjualan_bulan_berjalan;

        return $hpp;
    }

    public function getBiaya($start_date, $end_date)
    {
        $akun_biaya = \App\ModelFinance\RefAccountAccounting::where('account_active', 'Y')->get();
        $arr_akun_biaya = [];

        foreach ($akun_biaya as $key => $value) {
            $arr_akun_biaya[$value->account_id] = [
                "account_name" => $value->account_name,
                "account_amount" => 0
            ];
        }

        $nilai_biaya = DB::table('fin_ref_account_accounting')
            ->select(
                DB::raw("
                    fin_ref_account_accounting.account_id,
                    fin_ref_account_accounting.account_name,
                    SUM(fin_trnsct_item.finance_item_amount) as jml_biaya"
                )
            )
            ->leftJoin('fin_ref_account', 'fin_ref_account_accounting.account_id', '=', 'fin_ref_account.account_accounting_id')
            ->leftJoin('fin_trnsct_item', 'fin_ref_account.account_id', '=', 'fin_trnsct_item.finance_item_account_id')
            ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
            ->whereBetween('fin_trnsct.finance_date', ["$start_date", "$end_date"])
            ->where('fin_trnsct.finance_cancel', '=', '0')
            ->groupBy('fin_trnsct_item.finance_item_account_id')
            ->get();

        foreach ($nilai_biaya as $key => $value) {
            $arr_akun_biaya[$value->account_id]['account_amount'] = $value->jml_biaya;
        }

        return $arr_akun_biaya;

    }

    public function neraca()
    {
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-d');

        return view('reports.accounting.neraca', compact('start_date', 'end_date'));
    }

    public function neracaPrint(Request $request)
    {
        $start_date = explode(" - ", $request->get('date'))[0];
        $end_date = explode(" - ", $request->get('date'))[1];
        $jenis_laporan = $request->get('jenis_laporan');
        $hpp_pajak = $this->hpp_pajak;
        $prosentase_pajak_di_muka_subsidi = 0.01;
        $prosentase_pajak_di_muka_nonsubsidi = 0.025;

        // $start_date = "2021-01-01";
        // $end_date = "2022-12-30";

        // aktiva lancar
        $arr_kas = $this->getKas($start_date, $end_date);
        $kas = $arr_kas['arr_akun_kas'];
        $total_kas = $arr_kas['total_kas'];
        $persediaan_bangunan = $this->getPersediaanBangunan($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $persediaan_tanah = $this->getPersediaanTanah();
        $piutang_usaha = $this->getPiutangUsaha($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $uang_muka = $this->getUangMuka($start_date, $end_date);
        $piutang_lain_lain = $this->getPiutangLainLain($start_date, $end_date);
        $arr_lain_lain = $this->getPenjualan($start_date, $end_date, $hpp_pajak, $jenis_laporan);
        $lain_lain = $arr_lain_lain['jml_penjualan_rumah_subsidi'] * $prosentase_pajak_di_muka_subsidi;
        $lain_lain += $arr_lain_lain['jml_penjualan_rumah_nonsubsidi'] * $prosentase_pajak_di_muka_nonsubsidi;

        // total aktiva lancar
        $total_aktiva_lancar = $total_kas + $persediaan_bangunan + $persediaan_tanah + $piutang_usaha + $uang_muka + $lain_lain + $piutang_lain_lain;

        // aktiva tetap = asset
        $aktiva_tetap = $this->getAktivaTetap();

        // pasiva
        $hutang_bank = $this->getHutangBank();
        $hutang_usaha = $this->getHutangUsaha();

        return view('reports.accounting.neraca_print',
            compact(
                'jenis_laporan',
                'start_date',
                'end_date',
                'kas',
                'total_kas',
                'persediaan_bangunan',
                'persediaan_tanah',
                'piutang_usaha',
                'piutang_lain_lain',
                'uang_muka',
                'lain_lain',
                'total_aktiva_lancar',
                'aktiva_tetap',
                'hutang_bank',
                'hutang_usaha'
            )
        );
    }

    public function getKas($start_date, $end_date)
    {
        $akun_kas = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();
        $arr_akun_kas = [];
        $total_kas = 0;

        foreach ($akun_kas as $key => $value) {
            $arr_akun_kas[$value->cash_id] = [
                "cash_name" => $value->cash_name,
                "cash_masuk" => 0,
                "cash_keluar" => 0
            ];
        }

        // ambil uang masuk dari cicilan pembayaran penjualan unit
        $uang_masuk = DB::table('prop_trnsct_sales_paid')
            ->select(DB::raw('sales_paid_cash_id, SUM(sales_paid_amount) AS jml'))
            ->where('sales_paid_cancel', '0')
            ->groupBy('sales_paid_cash_id')
            ->get();

        // ambil uang keluar dari operasional
        $uang_keluar = DB::table('fin_trnsct_paid')
            ->select(DB::raw('finance_paid_cash_id, SUM(finance_paid_amount) AS jml'))
            ->where('finance_paid_cancel', '0')
            ->groupBy('finance_paid_cash_id')
            ->get();

        // set uang masuk dulu
        foreach ($uang_masuk as $key => $value) {
            $arr_akun_kas[$value->sales_paid_cash_id]['cash_masuk'] = $value->jml;
            $total_kas += $value->jml;
        }

        // set uang keluar kemudian
        foreach ($uang_keluar as $key => $value) {
            $arr_akun_kas[$value->finance_paid_cash_id]['cash_keluar'] = $value->jml;
            $total_kas -= $value->jml;
        }

        return ['arr_akun_kas' => $arr_akun_kas, 'total_kas' => $total_kas];
    }

    public function getAktivaTetap()
    {
        $assets = \App\ModelFinance\RefAsset::where(DB::raw('YEAR(asset_purchase_date) + asset_usage_time'), '>=', date('Y'))->get();
        $depreciation = \App\ModelFinance\RefAssetDepreciation::where(DB::raw('YEAR(depreciation_end_date)'), '=', date('Y'))->sum('depreciation_amount');

        $res = [];

        foreach ($assets as $key => $value) {
            $res['assets'][] = [
                'asset_name' => $value->asset_name,
                'asset_price' => $value->asset_price
            ];
        }

        $res['depreciation'] = $depreciation;

        return $res;
    }

    public function getPersediaanTanah()
    {
        $result = 0;
        $unit = \App\ModelProp\RefUnit::where('status', 'READY')->sum('harga_jual_kpr');

        $result = $unit*40/100;
        return $result;
    }

    public function getPiutangUsaha($start_date, $end_date, $hpp_pajak, $jenis_laporan)
    {
        $result = 0;
        $penjualan_cash = DB::table('prop_trnsct_sales')
            ->where('sales_type', 'CASH')
            ->where('status', '!=','CANCEL')
            ->whereBetween('sales_date', ["$start_date", "$end_date"])
            ->where('ttl_trnsct', '>',DB::raw('ttl_trnsct_paid'))
            ->get();

        foreach ($penjualan_cash as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $result += $hpp_pajak;
            } else {
                $result += $value->ttl_trnsct;
            }
        }

        return $result;
    }

    public function getHutangBank()
    {
        $result = 0;
        $debt = \App\ModelFinance\RefDebtToBank::sum('debt_total');
        $debt_paid = \App\ModelFinance\RefDebtToBank::sum('debt_total_paid');

        return $debt - $debt_paid;
    }

    public function getUangMuka($start_date, $end_date)
    {
        $payment = \App\ModelProp\TrnsctSalesPaid::whereBetween('sales_paid_date', ["$start_date", "$end_date"])
            ->where('sales_paid_kpr', '0')
            ->where('sales_paid_cancel', '0')
            ->sum('sales_paid_amount');

        return $payment;
    }

    public function getHutangUsaha()
    {
        $result = 0;
        $hutang = \App\ModelFinance\Trnsct::where('finance_cancel', '0')
            ->where('finance_amount', '>', DB::raw('finance_amount_paid'))
            ->sum('finance_amount');

        $hutang_paid = \App\ModelFinance\Trnsct::where('finance_cancel', '0')
            ->where('finance_amount', '>', DB::raw('finance_amount_paid'))
            ->sum('finance_amount_paid');

        $result = $hutang - $hutang_paid;

        return $result;
    }

    public function getPiutangLainLain($start_date, $end_date)
    {
        $result = 0;
        $data = DB::table('fin_trnsct_item')
            ->select('finance_item_amount')
            ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
            ->where('fin_trnsct_item.finance_item_account_id', '14')
            ->where('fin_trnsct.finance_cancel', '0')
            ->whereBetween('fin_trnsct.finance_date', ["$start_date", "$end_date"])
            ->get();

        foreach ($data as $key => $value) {
            $result += $value->finance_item_amount;
        }

        return $result;
    }

    public function getPersediaanBangunan($start_date, $end_date, $hpp_pajak, $jenis_laporan)
    {
        // set variable
        $penjualan_bulan_berjalan = 0;

        $penjualan_kpr = DB::table('prop_trnsct_sales')
            ->whereBetween('step_sales_date', ["$start_date", "$end_date"])
            ->where('sales_type', 'KPR')
            ->where('status', '!=','CANCEL')
            ->where('status', '!=','REJECTED')
            ->where(function($query){
                $query->where('step_sales', '!=', '6');
                $query->where('step_sales', '!=', '16');
                $query->where('step_sales', '!=', '20');
                $query->where('step_sales', '!=', '21');
                $query->where('step_sales', '!=', '24');
            })
            ->get();

        foreach ($penjualan_kpr as $key => $value) {
            if ($jenis_laporan == 'PAJAK') {
                $penjualan_bulan_berjalan += $hpp_pajak;
            } else {
                $penjualan_bulan_berjalan += $value->ttl_trnsct;
            }
        }

        // $penjualan_bulan_berjalan += \App\ModelProp\RefUnit::where('status', 'READY')->sum('harga_jual_kpr');

        return $penjualan_bulan_berjalan;
    }
}
