<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class ForemanJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $foremans = \App\ModelProp\RefMandor::with(['sdm' => function ($query) {
            $query->orderBy('name', 'ASC');
        }])->get();

        $data = DB::table('prop_ref_worklist')
            ->select(DB::raw('mandor_id, count(*) as total, sum(if(progress < 100,1,0)) as blm_selesai, sum(if(progress >= 100,1,0)) as selesai'))
            ->groupBy('mandor_id')
            ->get();

        $arr_data = [];
        foreach ($data as $key => $value) {
            $arr_data[$value->mandor_id] = [
                "total" => $value->total,
                "blm_selesai" => $value->blm_selesai,
                "selesai" => $value->selesai
            ];
        }

        return view('reports.foreman_job.index', compact('foremans', 'arr_data'));
    }

    public function ListJob(Request $request)
    {
        $mandor_id = $request->get('mandor_id');

        $foreman = \App\ModelProp\RefMandor::with('sdm')->findOrFail($mandor_id);

        $infografis = DB::table('prop_ref_worklist')
            ->select(DB::raw('mandor_id, count(*) as total, sum(if(progress < 100,1,0)) as blm_selesai, sum(if(progress >= 100,1,0)) as selesai'))
            ->where('mandor_id', "$mandor_id")
            ->groupBy('mandor_id')
            ->get();

        $jobs = \App\ModelProp\RefWorklist::with(['location', 'block'])
            ->where('mandor_id', "$mandor_id")
            ->orderBy('date_start')
            ->get();

        return view('reports.foreman_job.list_job', compact('foreman', 'infografis', 'jobs'));
    }

    public function jobsByMandor(Request $request)
    {
        # code...
    }
}
