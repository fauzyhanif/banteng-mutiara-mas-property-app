<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class MarketingFeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $dt_marketer = \App\ModelProp\RefMarketer::with('sdm')->orderBy('affiliate_num', 'ASC')->get();
        $dt_trnsct = DB::table('prop_trnsct_sales')
            ->select(DB::raw("affiliate_num, affiliate_fee,
                COUNT(*) AS ttl_sales,
                SUM(affiliate_fee) AS affiliate_fee,
                SUM(affiliate_fee_paid) AS affiliate_fee_paid,
                SUM(affiliate_fee - affiliate_fee_paid) AS sisa"))
            ->where('status', '!=', 'CANCEL')
            ->groupBy('affiliate_num')
            ->get();

        $res_trnsct = [];
        foreach ($dt_trnsct as $key => $value) {
            $res_trnsct[$value->affiliate_num] = [
                "ttl_sales" => $value->ttl_sales,
                "affiliate_fee" => $value->affiliate_fee,
                "affiliate_fee_paid" => $value->affiliate_fee_paid,
                "sisa" => $value->sisa,
            ];
        }

        return view('reports.marketing_fee.index', compact('dt_marketer', 'res_trnsct'));
    }

    public function detail(Request $request)
    {
        $affiliate_num = $request->get('affiliate_num');
        $marketer = \App\ModelProp\RefMarketer::with('sdm')->where('affiliate_num', "$affiliate_num")->first();

        $infografis = DB::table('prop_trnsct_sales')
            ->select(DB::raw("affiliate_num, affiliate_fee,
                COUNT(*) AS ttl_sales,
                SUM(affiliate_fee) AS affiliate_fee,
                SUM(affiliate_fee_paid) AS affiliate_fee_paid,
                SUM(affiliate_fee - affiliate_fee_paid) AS sisa"))
            ->where('affiliate_num',  "$affiliate_num")
            ->where('status', '!=', 'CANCEL')
            ->groupBy('affiliate_num')
            ->get();

        $res_infografis = [
            "ttl_sales" => 0,
            "affiliate_fee" => 0,
            "affiliate_fee_paid" => 0,
            "sisa" => 0,
        ];

        if ($infografis->count() == 1) {
            $res_infografis = [
                "ttl_sales" => $infografis[0]->ttl_sales,
                "affiliate_fee" => $infografis[0]->affiliate_fee,
                "affiliate_fee_paid" => $infografis[0]->affiliate_fee_paid,
                "sisa" => $infografis[0]->sisa,
            ];
        }

        $dt_trnsct = \App\ModelProp\TrnsctSales::with(['unit.unitType', 'customer'])
            ->where('affiliate_num', "$affiliate_num")
            ->orderBy('sales_id', 'DESC')
            ->get();

        return view('reports.marketing_fee.detail', compact('marketer', 'res_infografis', 'dt_trnsct'));
    }

}
