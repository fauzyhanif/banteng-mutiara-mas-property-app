<?php

namespace App\Http\Controllers\MarketerDashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class AffiliasiController extends Controller
{
    protected $dashboardController;
    public function __construct(DashboardController $dashboardController)
    {
        $this->dashboardController = $dashboardController;
    }

    public function index()
    {
        $sdm_id = Session::get('sdm_id');
        $affiliate = \App\ModelProp\RefMarketer::with(['sdm'])->findOrFail($sdm_id);

        $jml_penjualan = $this->dashboardController->getJmlPenjualan()->count();
        $jml_penjualan_booking = $this->dashboardController->getJmlPenjualanBooking()->count();
        $jml_penjualan_akad = $this->dashboardController->getJmlPenjualanAkad()->count();
        $dt_penjualan = $this->getPenjualan();

        return view('marketer_dashboard.affiliasi.index', compact(
            'affiliate',
            'jml_penjualan',
            'jml_penjualan_booking',
            'jml_penjualan_akad',
            'jml_penjualan_akad',
            'dt_penjualan'
        ));
    }

    public function getPenjualan()
    {
        $affiliate_num = Session::get('affiliate_num');
        $data = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'customer'])
            ->where('affiliate_num', "$affiliate_num")
            ->where('status', '!=','CANCEL')
            ->orderBy('sales_date', 'DESC')
            ->paginate(5);

        return $data;
    }
}
