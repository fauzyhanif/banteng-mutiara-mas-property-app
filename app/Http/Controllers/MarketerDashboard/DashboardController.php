<?php

namespace App\Http\Controllers\MarketerDashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public $affiliate_num;

    public function __construct()
    {
        $this->affiliate_num = Session::get('affiliate_num');
    }

    public function resetPassword()
    {
        $sdm_id = Session::get('sdm_id');
        $user = \App\User::where('sdm_id', "$sdm_id")->first();

        return view('marketer_dashboard.account.reset_password', compact('user'));
    }

    public function doResetPassword(Request $request)
    {
        $id = $request->get('id');
        $user = \App\User::findOrFail($id);
        if (Hash::check($request->get('password_old'), $user->password)) {

            if (strlen($request->get('password_new')) < 8) {
                return back()->with('error', 'Password minimal 8 karakter!');
            } else {
                $user->password = Hash::make(($request->get('password_new')));
                $user->password_changed = 'Y';
                $user->save();
                return back()->with('success', 'Password berhasil diperbaharui!');
            }
        } else {
            return back()->with('error', 'Password lama salah!');
        }
    }

    public function index()
    {
        $jml_penjualan = $this->getJmlPenjualan()->count();
        $jml_penjualan_booking = $this->getJmlPenjualanBooking()->count();
        $jml_penjualan_akad = $this->getJmlPenjualanAkad()->count();
        $dt_penjualan_terakhir = $this->getPenjualanTerakhir();

        return view('marketer_dashboard.dashboard.index', compact(
            'jml_penjualan',
            'jml_penjualan_booking',
            'jml_penjualan_akad',
            'dt_penjualan_terakhir'
        ));
    }

    public function getJmlPenjualan()
    {
        $affiliate_num = Session::get('affiliate_num');
        $data = \App\ModelProp\TrnsctSales::where('affiliate_num', "$affiliate_num")->where('status', '!=','CANCEL');

        return $data;
    }

    public function getJmlPenjualanBooking()
    {
        $data = $this->getJmlPenjualan()->where(function($query){
            $query->where('step_sales', "!=", '6');
            $query->where('step_sales', '!=', '16');
            $query->where('step_sales', '!=', '20');
            $query->where('step_sales', '!=', '21');
        })->get();

        return $data;
    }

    public function getJmlPenjualanAkad()
    {
        $data = $this->getJmlPenjualan()->where(function($query){
            $query->where('step_sales', '6');
            $query->orWhere('step_sales', '16');
            $query->orWhere('step_sales', '20');
            $query->orWhere('step_sales', '21');
        })->get();

        return $data;
    }

    public function getPenjualanTerakhir()
    {
        $affiliate_num = Session::get('affiliate_num');
        $data = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'customer'])
            ->where('affiliate_num', "$affiliate_num")
            ->where('status', '!=','CANCEL')
            ->orderBy('sales_date', 'DESC')
            ->limit(5)
            ->get();

        return $data;
    }

}
