<?php

namespace App\Http\Controllers\MarketerDashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class BookingOnlineController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(\App\Http\Controllers\Finance\TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index()
    {
        $locations = \App\ModelProp\RefLocation::orderBy('name')->get();
        return view('marketer_dashboard.booking_online.index', \compact('locations'));
    }

    public function store(Request $request)
    {
        $new_sdm = new \App\ModelProp\RefSdm();
        $new_sdm->name = $request->get('name');
        $new_sdm->nik = $request->get('nik');
        $new_sdm->place_of_birth = $request->get('place_of_birth');
        $new_sdm->date_ofbirth = $request->get('date_ofbirth');
        $new_sdm->couple = $request->get('couple');
        $new_sdm->phone_num = $request->get('phone_num');
        $new_sdm->address = $request->get('address');
        $new_sdm->address_domicile = $request->get('address_domicile');
        $new_sdm->job = $request->get('job');
        $new_sdm->institute = $request->get('institute');
        $new_sdm->save();

        $new_cust = new \App\ModelProp\RefCustomer();
        $new_cust->sdm_id = $new_sdm->sdm_id;
        $new_cust->save();

        // get invoice number
        $TrnsctJurnalController = $this->TrnsctJurnalController;
        $sales_id = $TrnsctJurnalController->invoiceNumber('SALES');

        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $number = $request->get('unit_id');
        $sales_type = $request->get('sales_type');

        $unit = \App\ModelProp\RefUnit::with(["location", "block", "unitType"])
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("number", "$number")
            ->first();

        $basic_price = ($sales_type == 'CASH') ? $unit->harga_jual_cash : $unit->harga_jual_kpr;

        // cek apakah unit yg dibeli subsidi atau nonsubsidi
        $cek_unit = \App\ModelProp\RefUnit::where('location_id', $request->get('location_id'))
            ->where('block_id', $request->get('block_id'))
            ->where('number', $request->get('unit_id'))
            ->first();

        $affiliate_num = Session::get('affiliate_num');
        $affiliate = \App\ModelProp\RefMarketer::where('affiliate_num', "$affiliate_num")->first();
        $affiliate_type = \App\ModelProp\RefMarketerType::findOrFail($affiliate->marketer_type);

        if($cek_unit->jenis_rumah == 'SUBSIDI') {
            $affiliate_fee = $affiliate_type->subsidi_booking_fee + $affiliate_type->subsidi_akad_fee;
        } else {
            $affiliate_fee = $affiliate_type->nonsubsidi_booking_fee + $affiliate_type->nonsubsidi_akad_fee;
        }



        $new_trnsct = new \App\ModelProp\TrnsctSales();
        $new_trnsct->sales_date = date('Y-m-d');
        $new_trnsct->sales_id = $sales_id;
        $new_trnsct->sales_type = $request->get('sales_type');
        $new_trnsct->location_id = $request->get('location_id');
        $new_trnsct->block_id = $request->get('block_id');
        $new_trnsct->unit_id = $request->get('unit_id');
        $new_trnsct->basic_price = $basic_price;
        $new_trnsct->discount = '0';
        $new_trnsct->ttl_trnsct = $basic_price;
        $new_trnsct->ttl_trnsct_paid = '0';
        $new_trnsct->customer_id = $new_sdm->sdm_id;
        $new_trnsct->affiliate_num = Session::get('affiliate_num');
        $new_trnsct->affiliate_fee = $affiliate_fee;
        $new_trnsct->affiliate_fee_paid = 0;
        $new_trnsct->status = 'BOOKING';
        $new_trnsct->booking_online = 'Y';

        if ($new_trnsct->save()) {

            // ubah status unit menjadi sold
            $data_post = $request->all();
            $data_post['customer_id'] = $new_sdm->sdm_id;
            $this->updateStatusUnitToBooking($data_post);

            return redirect('marketer_dashboard/booking_online/detail/'.$new_trnsct->id)->with(['sales_success', 'Input booking berhasil']);
        } else {
            return redirect()->back();
        }
    }

    public function updateStatusUnitToBooking($request)
    {
        $location_id = $request['location_id'];
        $block_id = $request['block_id'];
        $unit_id = $request['unit_id'];
        $customer_id = $request['customer_id'];
        $affiliate_num = Session::get('affiliate_num');

        \App\ModelProp\RefUnit::where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('number', "$unit_id")
            ->update([
                "customer_id" => $customer_id,
                "marketer_id" => $affiliate_num,
                "status" => 'BOOKED',
            ]);
    }

    public function detail($id)
    {
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'unit.unitType'])->findOrFail($id);
        return view('marketer_dashboard.booking_online.detail', compact('sales'));
    }

    public function formEdit($id)
    {
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'unit.unitType'])->findOrFail($id);
        $locations = \App\ModelProp\RefLocation::orderBy('name')->get();
        $blocks = \App\ModelProp\RefBlock::where('location_id', $sales->location_id)->orderBy('name')->get();
        $units = \App\ModelProp\RefUnit::with(['unitType'])->where('location_id', $sales->location_id)->where('block_id', $sales->block_id)->orderBy('unit_id')->get();

        return view('marketer_dashboard.booking_online.form_edit', compact('sales', 'locations', 'blocks', 'units'));
    }

    public function edit(Request $request, $id)
    {
        $sdm_id = $request->get('sdm_id');

        $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);
        $sdm->name = $request->get('name');
        $sdm->nik = $request->get('nik');
        $sdm->place_of_birth = $request->get('place_of_birth');
        $sdm->date_ofbirth = $request->get('date_ofbirth');
        $sdm->couple = $request->get('couple');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->address_domicile = $request->get('address_domicile');
        $sdm->job = $request->get('job');
        $sdm->institute = $request->get('institute');
        $sdm->save();

        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $number = $request->get('unit_id');
        $sales_type = $request->get('sales_type');

        $unit = \App\ModelProp\RefUnit::with(["location", "block", "unitType"])
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("number", "$number")
            ->first();

        $basic_price = ($sales_type == 'CASH') ? $unit->harga_jual_cash : $unit->harga_jual_kpr;

        $trnsct = \App\ModelProp\TrnsctSales::findOrFail($id);

        // update unit lama jadi ready lagi
        $unit_old = \App\ModelProp\RefUnit::where("location_id", $trnsct->location_id)
            ->where("block_id", $trnsct->block_id)
            ->where("number", "$trnsct->unit_id")
            ->update([
                "status" => "READY",
                "customer_id" => "",
                "marketer_id" => ""
            ]);

        $trnsct->sales_type = $request->get('sales_type');
        $trnsct->location_id = $request->get('location_id');
        $trnsct->block_id = $request->get('block_id');
        $trnsct->unit_id = $request->get('unit_id');
        $trnsct->basic_price = $basic_price;
        $trnsct->ttl_trnsct = $basic_price;
        $trnsct->ttl_trnsct_paid = '0';

        if ($trnsct->save()) {

            // ubah status unit menjadi sold
            $data_post = $request->all();
            $data_post['customer_id'] = $sdm->sdm_id;

            $this->updateStatusUnitToBooking($data_post);

            return redirect('marketer_dashboard/booking_online/detail/'.$trnsct->id)->with(['sales_success', 'Input booking berhasil']);
        } else {
            return redirect()->back();
        }
    }
}
