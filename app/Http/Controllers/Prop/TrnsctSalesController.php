<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use App\ModelProp\RefMarketer;
use App\ModelProp\RefMarketingFeeSetting;
use App\ModelProp\RefSalesStep;
use App\ModelProp\RefSdm;
use App\ModelProp\RefUnit;
use App\ModelProp\TrnsctSalesBill;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Exception;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;

class TrnsctSalesController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(\App\Http\Controllers\Finance\TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->middleware('auth');
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index(Request $request)
    {
        $filter = ($request->get('filter')) ? $request->get('filter') : 'all';

        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'unit.unitType', 'customer', 'marketer.sdm', 'last_sales_step']);

        if ($filter == 'all') {
            $datas = $datas->where('status', '!=', 'CANCEL');
        }

        if ($filter == 'slow_progress') {
            $fourteen_days_ago  = Carbon::now()->subDays(14);
            $step_sales_slow_progress = [0, 1, 28];
            
            $datas = $datas->where('sales_type', 'KPR')
                ->where('kpr_status', 'WAITING')
                ->where('sales_date', '<', $fourteen_days_ago)
                ->whereIn('step_sales', $step_sales_slow_progress);
        }
        
        if ($filter == 'kpr_rejected') {
            $datas = $datas->where('sales_type', 'KPR')
                ->where('kpr_status', 'REJECTED')
                ->where('status', 'CANCEL');
        }

        $datas = $datas->orderBy('block_id', 'DESC')->paginate(25);

        if($request->ajax()){
            $keyword_column = $request->get('keyword_column');
            $keyword = $request->get('keyword');
            $keyword = str_replace("-", " ", $keyword);
            $per_page = $request->get('per_page');
            $slow_progress = ($request->get('slow_progress')) ? true : false;

            $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'unit.unitType', 'customer', 'marketer.sdm', 'last_sales_step']);
        
            if ($keyword_column == 'customer_name') {
                $datas = $datas->whereHas('customer', function($q) use($keyword){
                    $q->whereRaw("LOWER(name) like '%" . $keyword . "%'");
                });
            } elseif ($keyword_column == 'block_name') {
                $datas = $datas->whereHas('block', function($q) use($keyword){
                    $q->whereRaw("LOWER(name) like '%" . $keyword . "%'");
                });
            } elseif ($keyword_column == 'sales_type') {
                $datas = $datas->whereRaw("LOWER(sales_type) like '%" . $keyword . "%'");
            }

            if ($filter == 'all') {
                $datas = $datas->where('status', '!=', 'CANCEL');
            }

            if ($filter == 'slow_progress') {
                $fourteen_days_ago  = Carbon::now()->subDays(14);
                $step_sales_slow_progress = [0, 1, 28];
                
                $datas = $datas->where('sales_type', 'KPR')
                    ->where('kpr_status', 'WAITING')
                    ->where('sales_date', '<', $fourteen_days_ago)
                    ->whereIn('step_sales', $step_sales_slow_progress);
            }
            
            if ($filter == 'kpr_rejected') {
                $datas = $datas->where('sales_type', 'KPR')
                    ->where('kpr_status', 'REJECTED')
                    ->where('status', 'CANCEL');
            }

            $datas = $datas->orderBy('block_id', 'DESC')->paginate($per_page);

            return view('prop.trnsct_sales.index2_list_data', compact('datas'))->render();
        }

        return view('prop.trnsct_sales.index', compact('datas', 'filter'));
    }

    public function index2(Request $request)
    {
        $ref_block = \App\ModelProp\RefBlock::with(['location'])
            ->where('is_Active', 'Y')
            ->orderBy('location_id')
            ->orderBy('name')
            ->get();

        $ref_sales_type = [
            "CASH" => "CASH",
            "KPR" => "KPR"
        ];

        $ref_kpr_status = [
            "WAITING" => "KPR - WAITING",
            "ACCEPTED" => "KPR - ACCEPTED",
            "REJECTED" => "KPR - REJECTED"
        ];

        $ref_sales_step = RefSalesStep::orderBy('type')
            ->orderBy('orders')
            ->get();

        if ($request->isMethod('POST')) {
            \App\ModelProp\RefFilterSales::truncate();

            if (!empty($request->selected_blocks)) {
                foreach ($request->selected_blocks as $key => $value) {
                    $new = new \App\ModelProp\RefFilterSales();
                    $new->type = 'block';
                    $new->id = $value;
                    $new->save();
                }
            }

            if (!empty($request->selected_sales_types)) {
                foreach ($request->selected_sales_types as $key => $value) {
                    $new = new \App\ModelProp\RefFilterSales();
                    $new->type = 'sales_type';
                    $new->id = $value;
                    $new->save();
                }
            }

            if (!empty($request->selected_kpr_status)) {
                foreach ($request->selected_kpr_status as $key => $value) {
                    $new = new \App\ModelProp\RefFilterSales();
                    $new->type = 'kpr_status';
                    $new->id = $value;
                    $new->save();
                }
            }

            if (!empty($request->selected_sales_steps)) {
                foreach ($request->selected_sales_steps as $key => $value) {
                    $new = new \App\ModelProp\RefFilterSales();
                    $new->type = 'sales_step';
                    $new->id = $value;
                    $new->save();
                }
            }
        }

        $selected_blocks_db = \App\ModelProp\RefFilterSales::where('type', 'block')->get();
        $selected_sales_types_db = \App\ModelProp\RefFilterSales::where('type', 'sales_type')->get();
        $selected_kpr_status_db = \App\ModelProp\RefFilterSales::where('type', 'kpr_status')->get();
        $selected_sales_steps_db = \App\ModelProp\RefFilterSales::where('type', 'sales_step')->get();

        $selected_blocks = [];
        $selected_sales_types = [];
        $selected_kpr_status = [];
        $selected_sales_steps = [];
        $key_sales_types = [];
        $key_kpr_status = [];
        $key_blocks = [];
        $key_sales_steps = [];

        foreach ($selected_blocks_db as $key => $value) {
            $selected_blocks[] = $value->id;
            $key_blocks[$value->id] = null;
        }

        foreach ($selected_sales_types_db as $key => $value) {
            $selected_sales_types[] = $value->id;
            $key_sales_types[$value->id] = null;
        }

        foreach ($selected_kpr_status_db as $key => $value) {
            $selected_kpr_status[] = $value->id;
            $key_kpr_status[$value->id] = null;
        }

        foreach ($selected_sales_steps_db as $key => $value) {
            $selected_sales_steps[] = $value->id;
            $key_sales_steps[$value->id] = null;
        }

        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'unit.unitType', 'customer', 'marketer.sdm', 'last_sales_step']);

        if (count($selected_blocks) > 0) {
            $datas = $datas->whereIn('block_id', $selected_blocks);
        }

        if (count($selected_sales_types) > 0) {
            $datas = $datas->whereIn('sales_type', $selected_sales_types);
        }

        if (count($selected_kpr_status)) {
            $datas = $datas->whereIn('kpr_status', $selected_kpr_status);
        }

        if (count($selected_sales_steps) > 0) {
            $datas = $datas->whereIn('step_sales', $selected_sales_steps);
        }

        $datas = $datas->orderBy('block_id', 'DESC')->paginate(25);

        if($request->ajax()){
            $per_page = $request->get('per_page');
            $keyword = $request->get('keyword');

            $selected_blocks_db = \App\ModelProp\RefFilterSales::where('type', 'block')->get();
            $selected_sales_types_db = \App\ModelProp\RefFilterSales::where('type', 'sales_type')->get();
            $selected_kpr_status_db = \App\ModelProp\RefFilterSales::where('type', 'kpr_status')->get();
            $selected_sales_steps_db = \App\ModelProp\RefFilterSales::where('type', 'sales_step')->get();

            $selected_blocks = [];
            $selected_sales_types = [];
            $selected_kpr_status = [];
            $selected_sales_steps = [];

            foreach ($selected_blocks_db as $key => $value) {
                $selected_blocks[] = $value->id;
            }

            foreach ($selected_sales_types_db as $key => $value) {
                $selected_sales_types[] = $value->id;
            }

            foreach ($selected_kpr_status_db as $key => $value) {
                $selected_kpr_status[] = $value->id;
            }

            foreach ($selected_sales_steps_db as $key => $value) {
                $selected_sales_steps[] = $value->id;
            }

            $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'unit.unitType', 'customer', 'marketer.sdm', 'last_sales_step']);

            if ($keyword != '') {
                $datas = $datas->whereHas('customer', function($q) use($keyword){
                    $q->whereRaw("LOWER(name) like '%" . $keyword . "%'");
                });
            }

            if (count($selected_blocks) > 0) {
                $datas = $datas->whereIn('block_id', $selected_blocks);
            }

            if (count($selected_sales_types) > 0) {
                $datas = $datas->whereIn('sales_type', $selected_sales_types);
            }

            if (count($selected_kpr_status)) {
                $datas = $datas->whereIn('kpr_status', $selected_kpr_status);
            }

            if (count($selected_sales_steps) > 0) {
                $datas = $datas->whereIn('step_sales', $selected_sales_types);
            }

            $datas = $datas->orderBy('block_id', 'DESC')->paginate($per_page);

            return view('prop.trnsct_sales.index2_list_data', compact('datas'))->render();
        }

        return view('prop.trnsct_sales.index2', compact('ref_block', 'ref_sales_type', 'ref_kpr_status', 'ref_sales_step', 'key_blocks', 'key_sales_types', 'key_kpr_status', 'key_sales_steps', 'datas'));
    }


    public function listData(Request $request)
    {
        $type = (!empty($request->get('type'))) ? $request->get('type') : 'CASH';
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'unit.unitType', 'customer', 'marketer.sdm'])
            ->where('sales_type', $type)
            ->orderBy('block_id', 'DESC')
            ->get();

        return DataTables::of($datas)
            ->addColumn('ttl_trnsct', function($datas){
                return number_format($datas->ttl_trnsct,2,',','.');
            })
            ->addColumn('date_and_id_sales', function($datas){
                $html = date("d/m/Y", strtotime($datas->sales_date)) . "<br>";

                $sales_type = "<span class='badge badge-success'>CASH</span> ";
                if ($datas->sales_type == 'KPR') {
                    $badge_type = '';

                    if ($datas->kpr_status == 'WAITING') {
                        $badge_type = 'badge-warning';
                    } elseif ($datas->kpr_status == 'ACCEPTED') {
                        $badge_type = 'badge-info';
                    } else {
                        $badge_type = 'badge-danger';
                    }

                    $sales_type = "<span class='badge $badge_type'>KPR ($datas->kpr_status)</span> ";
                }

                $html .= $sales_type;
                $html .= "<span class='badge badge-secondary'>$datas->sales_id</span>";

                return $html;
            })
            ->addColumn('location_and_block', function($datas){
                return $datas->location->name . " Blok " . $datas->block->name;
            })
            ->addColumn('unit_type', function($datas){
                return $datas->unit->unitType->name;
            })
            ->addColumn('marketer', function($datas){
                $html = "-";

                if ($datas->affiliate_num != '') {
                    $html = $datas->marketer->sdm->name . "<br>";
                    $html .= "Fee : " . number_format($datas->affiliate_fee,2,',','.');
                }

                return $html;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.sales.detail', $datas->id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i>";
                $btn .= "</a> ";

                // print sppr link
                $btn .= "<a href='" . route('prop.sales.print_sppr', $datas->id) . "' target='_blank' class='btn btn-secondary btn-xs'>";
                $btn .= "<i class='fas fa-print'></i>";
                $btn .= "</a> ";

                // delete link
                $btn .= "<button type='button' class='btn btn-danger btn-xs' onclick=\"show_modal_delete_sales('$datas->sales_id')\">";
                $btn .= "<i class='fas fa-trash'></i>";
                $btn .= "</button> ";

                return $btn;
            })
            ->rawColumns(['date_and_id_sales', 'location_and_block', 'unit_type', 'marketer', 'actions_link'])
            ->toJson();
    }

    public function listUnit()
    {
        $locations = \App\ModelProp\RefLocation::orderBy('name')->get();

        return view('prop.trnsct_sales.list_unit', compact('locations'));
    }

    public function listUnitSecond($location_id, $block_id)
    {
        $units = \App\ModelProp\RefUnit::with(['location', 'block', 'unitType'])
            ->where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('status', "READY")
            ->orderBy('number')
            ->get();

        return view('prop.trnsct_sales.list_unit_second', compact('units'));
    }

    public function showUnits(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');

        $units = \App\ModelProp\RefUnit::with(['location', 'block', 'unitType'])
            ->where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->orderBy('number')
            ->get();

        return view('prop.trnsct_sales.list_unit_view', compact('units'));
    }

    public function formStore(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $number = $request->get('number');

        $unit = \App\ModelProp\RefUnit::with(['location', 'block', 'unitType'])
            ->where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('number', "$number")
            ->first();

        $unit_prices = \App\ModelProp\RefUnitPrice::with(['priceItem'])
            ->where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('unit_id', "$number")
            ->get();

        $banks = \App\ModelProp\RefBank::with(['sdm' => function ($q) {$q->orderBy('name', 'ASC');}])->get();

        return view('prop.trnsct_sales.form_store', compact('unit', 'unit_prices', 'banks'));
    }

    public function getSpprNumber($sales_date)
    {
        $year = substr($sales_date, 0, 4);
        $result = \App\ModelProp\TrnsctSales::whereYear('sales_date', $year)->orderBy('sppr_number', 'DESC')->limit(1)->get();
        if ($result->count() == 0) {
            $sppr_number = '0001';
        } else {
            $sppr_number = sprintf('%04d', $result[0]->sppr_number) + 1;
        }

        return $sppr_number;
    }

    public function store(Request $request)
    {
        // get invoice number
        $TrnsctJurnalController = $this->TrnsctJurnalController;
        $sales_id = $TrnsctJurnalController->invoiceNumber('SALES');
        $sppr_number = $this->getSpprNumber($request->get('sales_date'));

        $new = new \App\ModelProp\TrnsctSales();
        $new->sales_date = $request->get('sales_date');
        $new->sales_type = $request->get('sales_type');
        $new->sales_date = $request->get('sales_date');
        $new->sales_id = $sales_id;
        $new->sppr_number = $sppr_number;
        $new->location_id = $request->get('location_id');
        $new->block_id = $request->get('block_id');
        $new->unit_id = $request->get('unit_id');
        $new->discount = str_replace('.', '', $request->get('discount'));
        $new->basic_price = str_replace('.', '', $request->get('basic_price'));
        $new->ttl_trnsct = str_replace('.', '', $request->get('basic_price')) - str_replace('.', '', $request->get('discount'));
        $new->ttl_trnsct_for_pajak = str_replace('.', '', $request->get('ttl_trnsct_for_pajak'));
        $new->payment_commitment = $request->get('payment_commitment');
        $new->customer_id = $request->get('customer_id');
        $new->affiliate_num = $request->get('affiliate_num');
        $new->status = 'SOLD';

        $new->affiliate_fee = str_replace(['.', ','], '', $request->affiliate_fee);

        if ($request->get('sales_type') == 'KPR') {
            $new->kpr_bank = $request->get('kpr_bank');
            $new->kpr_credit_pengajuan = str_replace('.', '', $request->get('kpr_credit_pengajuan'));
        }

        if ($new->save()) {

            // ubah status unit menjadi sold
            $data_post = $request->all();
            $this->updateStatusUnitToSold($data_post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $new->id
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formPay($id)
    {
        $sale = \App\ModelProp\TrnsctSales::with(['customer'])
            ->where('id', $id)
            ->first();

        $unit = \App\ModelProp\RefUnit::where('location_id', "$sale->location_id")
            ->where('block_id', "$sale->block_id")
            ->where('number', "$sale->unit_id")
            ->get();

        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();

        return view('prop.trnsct_sales.form_pay', compact('cashes', 'sale', 'unit'));
    }

    public function pay(Request $request)
    {
        $data_post = $request->all();
        $data_post['sales_paid_kpr'] = '0';

        // update table trnsct_sales kolom ttl_trnsct_paid = jml pembayaran
        $this->updateTrnsctPaid($data_post);

        // simpan pembayaran
        $this->storeToSalesPaid($data_post);

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil disimpan',
            'sales_id' => $request->get('id')
        ];

        return response()->json($res);
    }

    public function switchUnitListUnit(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $sales_id = $request->get('sales_id');

        $units = \App\ModelProp\RefUnit::with(['location', 'block', 'unitType'])
            ->where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->orderBy('number')
            ->get();

        return view('prop.trnsct_sales.form_switch_unit_list', compact('units', 'sales_id'));
    }

    public function switchUnit(Request $request)
    {
        $id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $reason_to_move = $request->get('reason_to_move');

        // get sales
        $sales = \App\ModelProp\TrnsctSales::with(['marketer'])->findOrFail($id);

        // update unit lama jadi ready lagi
        $unit_old = \App\ModelProp\RefUnit::where("location_id", $sales->location_id)
            ->where("block_id", $sales->block_id)
            ->where("number", "$sales->unit_id")
            ->update([
                "status" => "READY",
                "customer_id" => "",
                "marketer_id" => ""
            ]);

        // update unit baru jadi sold
        $unit_new = \App\ModelProp\RefUnit::where("location_id", $location_id)
            ->where("block_id", $block_id)
            ->where("number", "$unit_id")
            ->update([
                "status" => "SOLD",
                "customer_id" => $sales->customer_id,
                "marketer_id" => ""
            ]);

        // update sales
        $sales->location_id = $location_id;
        $sales->block_id = $block_id;
        $sales->unit_id = $unit_id;
        $sales->reason_to_move = $reason_to_move;
        $sales->save();

        // update unit_sales_step
        $sales_step = \App\ModelProp\RefUnitSalesStep::where("sales_id", "$sales->sales_id")
            ->update([
                "location_id" => $location_id,
                "block_id" => $block_id,
                "unit_id" => "$unit_id",
            ]);

        // update unit_certificate_position
        $sales_certificate_position = \App\ModelProp\RefUnitCertificatePosition::where("sales_id", "$sales->sales_id")
            ->update([
                "location_id" => $location_id,
                "block_id" => $block_id,
                "unit_id" => "$unit_id",
            ]);

        // update unit_certificate_pstep
        $sales_certificate_step = \App\ModelProp\RefUnitCertificateStep::where("sales_id", "$sales->sales_id")
            ->update([
                "location_id" => $location_id,
                "block_id" => $block_id,
                "unit_id" => "$unit_id",
            ]);

        // pastikan jika marketer_type = 2 maka update data umara
        if ($sales->marketer->marketer_type == 2) {
            $this->callUmaraEndpointUpdateSales($id);
        }

        return redirect()->back();
    }

    public function changePriceForPajak(Request $request)
    {
        $data = \App\ModelProp\TrnsctSales::findOrFail($request['sales_id']);
        $data->ttl_trnsct_for_pajak = str_replace('.', '', $request['ttl_trnsct_for_pajak']);
        $data->save();

        return redirect()->back();
    }

    public function updateTrnsctPaid($request)
    {
        $data = \App\ModelProp\TrnsctSales::findOrFail($request['id']);
        $data->ttl_trnsct_paid += str_replace('.', '', $request['ttl_trnsct_paid']);
        $data->save();

        return $data->ttl_trnsct_paid;
    }

    public function storeToSalesPaid($request)
    {
        // get invoice number
        $TrnsctJurnalController = $this->TrnsctJurnalController;
        $sales_paid_num = $TrnsctJurnalController->invoiceNumber('INCOME');

        $sales_paid = new \App\ModelProp\TrnsctSalesPaid();
        $sales_paid->sales_id = $request['sales_id'];
        $sales_paid->sales_paid_num = $sales_paid_num;
        $sales_paid->sales_paid_person = $request['customer_name'];
        $sales_paid->sales_paid_date = $request['trnsct_date'];
        $sales_paid->sales_paid_cash_id = $request['account_id'];
        $sales_paid->sales_paid_desc = ($request['description']) ? $request['description'] : "Pembayaran " . $request['sales_id'];
        $sales_paid->sales_paid_amount = str_replace(".", "", $request['ttl_trnsct_paid']);
        $sales_paid->sales_paid_kpr = $request['sales_paid_kpr'];
        $sales_paid->installment_amount = (isset($request['installment_amount'])) ? str_replace(".", "", $request['installment_amount']) : '0';
        $sales_paid->installment_desc = (isset($request['installment_desc'])) ? $request['installment_desc'] : '';
        $sales_paid->debt_id = (isset($request['debt_id'])) ? $request['debt_id'] : '';
        $sales_paid->sales_paid_admin = Session::get('auth_nama');
        $sales_paid->save();

        return $sales_paid->sales_paid_id;
    }

    public function updateStatusUnitToSold($request)
    {
        $location_id = $request['location_id'];
        $block_id = $request['block_id'];
        $unit_id = $request['unit_id'];
        $customer_id = $request['customer_id'];
        $affiliate_num = $request['affiliate_num'];

        \App\ModelProp\RefUnit::where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('number', "$unit_id")
            ->update([
                "customer_id" => $customer_id,
                "marketer_id" => $affiliate_num,
                "status" => 'SOLD',
            ]);
    }

    public function detail($id)
    {
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit'])->findOrFail($id);
        $locations = \App\ModelProp\RefLocation::orderBy('name')->get();
        $payments = \App\ModelFinance\TrnsctJurnal::where('sales_id', "$sales->sales_id")
            ->orderBy('sales_id', 'ASC')
            ->get();

        return view('prop.trnsct_sales.detail', compact('sales', 'payments', 'locations'));
    }

    public function printInvoice($sales_paid_id)
    {
        $payment = \App\ModelProp\TrnsctSalesPaid::with(['sales'])->findOrFail($sales_paid_id);
        return view('prop.trnsct_sales.print_invoice', compact('payment'));
    }

    public function printSppr($id)
    {
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer'])->findOrFail($id);

        if ($sales->sppr_number == '') {
            $sppr_number = $this->getSpprNumber($sales->sales_date);
            $sales->sppr_number = $sppr_number;
            $sales->save();
        }

        return view('prop.trnsct_sales.print_sppr', compact('sales'));
    }

    public function detailSalesStep(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $sales_type = $request->get('sales_type');

        $unit_steps = \App\ModelProp\RefUnitSalesStep::with(['step'])
            ->where("sales_id", "$sales_id")
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("unit_id", "$unit_id")
            ->orderBy("step_date", "desc")
            ->orderBy("id", "desc")
            ->get();

        $ref_steps = \App\ModelProp\RefSalesStep::where('type', "$sales_type")
            ->where('is_active' , 'Y')
            ->orderBy('orders')
            ->get();

        return view('prop.trnsct_sales.detail_sales_step',
            compact(
                'unit_steps',
                'ref_steps',
                'sales_id',
                'location_id',
                'block_id',
                'unit_id',
                'sales_type'
            )
        );
    }

    public function detailSalesStepStore(Request $request)
    {
        $new = new \App\ModelProp\RefUnitSalesStep();
        $new->sales_id = $request->get('sales_id');
        $new->location_id = $request->get('location_id');
        $new->block_id = $request->get('block_id');
        $new->unit_id = $request->get('unit_id');
        $new->step_id = $request->get('step_id');
        $new->step_date = $request->get('step_date');
        $new->step_desc = $request->get('step_desc');

        if ($new->save()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailSalesStepUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
                'sales_type' => $request->get('sales_type'),
                'sales_step' => $request->get('sales_step')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detailSalesStepDelete(Request $request)
    {
        $id = $request->get('id');
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $sales_type = $request->get('sales_type');
        $data = \App\ModelProp\RefUnitSalesStep::findOrFail($id);

        if ($data->delete()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailSalesStepUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
                'sales_type' => $request->get('sales_type'),
                'sales_step' => $request->get('sales_step')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detailSalesStepUpdate(Request $request)
    {
        $new = \App\ModelProp\RefUnitSalesStep::findOrFail($request->get('id'));
        $new->step_id = $request->get('step_id');
        $new->step_date = $request->get('step_date');
        $new->step_desc = $request->get('step_desc');

        if ($new->save()) {

            $post['sales_id'] = $new->sales_id;
            $this->DetailSalesStepUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'id' => $request->get('id'),
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
                'sales_type' => $request->get('sales_type'),
                'sales_step' => $request->get('sales_step')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function DetailSalesStepUpdateOnUnit($request)
    {
        $sales_id = $request['sales_id'];
        $last_step ="";
        $last_step_date ="";

        $steps = \App\ModelProp\RefUnitSalesStep::where('sales_id', "$sales_id")->orderBy('step_date', 'DESC')->get();
        if ($steps->count() > 0) {
            $last_step = $steps[0]->step_id;
            $last_step_date = $steps[0]->step_date;
        }

        $unit = \App\ModelProp\TrnsctSales::where('sales_id', "$sales_id")
                ->update(["step_sales" => $last_step, "step_sales_date" => $last_step_date]);
    }

    public function detailCertificateStep(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $sales_type = $request->get('sales_type');

        $unit_steps = \App\ModelProp\RefUnitCertificateStep::with(['step'])
            ->where("sales_id", "$sales_id")
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("unit_id", "$unit_id")
            ->orderBy("step_date", "desc")
            ->orderBy("id", "desc")
            ->get();

        $ref_steps = \App\ModelProp\RefCertificateStep::where('is_active' , 'Y')
            ->orderBy('orders')
            ->get();

        return view('prop.trnsct_sales.detail_certificate_step',
            compact(
                'unit_steps',
                'ref_steps',
                'sales_id',
                'location_id',
                'block_id',
                'unit_id',
                'sales_type'
            )
        );
    }

    public function detailCertificateStepStore(Request $request)
    {
        $step_image_name = '';

        if ($request->file('step_image')) {
            $step_image = $request->file('step_image');

            // rename filename
            $step_image_name = date('YmdHis') . "." . $step_image->getClientOriginalExtension();

            // upload file
            $step_image->move('public/sales_step_certificate', $step_image_name);
        }

        $new = new \App\ModelProp\RefUnitCertificateStep();
        $new->sales_id = $request->get('sales_id');
        $new->location_id = $request->get('location_id');
        $new->block_id = $request->get('block_id');
        $new->unit_id = $request->get('unit_id');
        $new->step_id = $request->get('step_id');
        $new->step_date = $request->get('step_date');
        $new->step_desc = $request->get('step_desc');
        $new->step_image = $step_image_name;

        if ($new->save()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailCertificateStepUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
                'sales_step' => $request->get('sales_step')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detailCertificateStepDelete(Request $request)
    {
        $id = $request->get('id');
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $sales_type = $request->get('sales_type');

        $data = \App\ModelProp\RefUnitCertificateStep::findOrFail($id);

        if ($data->delete()) {

            if ($data->step_image != '') {
                File::delete('public/sales_step_certificate/'.  $data->step_image);
            }

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailCertificateStepUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
                'sales_type' => $request->get('sales_type'),
                'sales_step' => $request->get('sales_step')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detailCertificateStepUpdate(Request $request)
    {
        $new = \App\ModelProp\RefUnitCertificateStep::findOrFail($request->get('id'));
        $new->step_id = $request->get('step_id');
        $new->step_date = $request->get('step_date');
        $new->step_desc = $request->get('step_desc');


        if ($request->file('step_image')) {

            if ($new->step_image != '') {
                File::delete('public/sales_step_certificate'.  $new->step_image);
            }

            $step_image = $request->file('step_image');

            // rename filename
            $step_image_name = date('YmdHis') . "." . $step_image->getClientOriginalExtension();

            // upload file
            $step_image->move('public/sales_step_certificate', $step_image_name);
            $new->step_image = $step_image_name;
        }


        if ($new->save()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailCertificateStepUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'id' => $request->get('id'),
                'sales_id' => $request->get('sales_id'),
                'sales_type' => $request->get('sales_type'),
                'invoice_id' => $request->get('invoice_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
                'sales_step' => $request->get('sales_step')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function DetailCertificateStepUpdateOnUnit($request)
    {
        $sales_id = $request['sales_id'];
        $last_step ="";
        $last_step_date ="";

        $steps = \App\ModelProp\RefUnitCertificateStep::where('sales_id', "$sales_id")->orderBy('step_date', 'DESC')->get();
        if ($steps->count() > 0) {
            $last_step = $steps[0]->step_id;
            $last_step_date = $steps[0]->step_date;
        }

        $unit = \App\ModelProp\TrnsctSales::where('sales_id', "$sales_id")
                ->update(["step_certificate" => $last_step, "step_certificate_date" => $last_step_date]);
    }

    public function detailCertificatePosition(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');

        $unit_steps = \App\ModelProp\RefUnitCertificatePosition::where("sales_id", "$sales_id")
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("unit_id", "$unit_id")
            ->orderBy("step_date", "desc")
            ->get();

        return view('prop.trnsct_sales.detail_certificate_position',
            compact(
                'unit_steps',
                'sales_id',
                'location_id',
                'block_id',
                'unit_id',
                'sales_type'
            )
        );
    }

    public function detailCertificatePositionStore(Request $request)
    {
        $step_image_name = null;

        if ($request->file('step_image')) {
            $step_image = $request->file('step_image');

            // rename filename
            $step_image_name = date('YmdHis') . "." . $step_image->getClientOriginalExtension();

            // upload file
            $step_image->move('bukti_penyerahan_sertifikat', $step_image_name);
        }

        $new = new \App\ModelProp\RefUnitCertificatePosition();
        $new->sales_id = $request->get('sales_id');
        $new->location_id = $request->get('location_id');
        $new->block_id = $request->get('block_id');
        $new->unit_id = $request->get('unit_id');
        $new->step_position = $request->get('step_position');
        $new->step_image = $step_image_name;
        $new->step_date = $request->get('step_date');
        $new->step_desc = $request->get('step_desc');

        if ($new->save()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailCertificatePositionUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detailCertificatePositionDelete(Request $request)
    {
        $id = $request->get('id');
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');

        $data = \App\ModelProp\RefUnitCertificatePosition::findOrFail($id);

        if ($data->step_image != '') {
            File::delete('public/bukti_penyerahan_sertifikat/'.  $data->step_image);
        }

        if ($data->delete()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailCertificatePositionUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id'),
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detailCertificatePositionUpdate(Request $request)
    {
        $new = \App\ModelProp\RefUnitCertificatePosition::findOrFail($request->get('id'));
        $new->step_position = $request->get('step_position');
        $new->step_date = $request->get('step_date');
        $new->step_desc = $request->get('step_desc');

        if ($request->file('step_image')) {

            if ($new->step_image != '') {
                File::delete('bukti_penyerahan_sertifikat/'.  $new->step_image);
            }

            $step_image = $request->file('step_image');

            // rename filename
            $step_image_name = date('YmdHis') . "." . $step_image->getClientOriginalExtension();

            // upload file
            $step_image->move('bukti_penyerahan_sertifikat', $step_image_name);
            $new->step_image = $step_image_name;
        }


        if ($new->save()) {

            $post['sales_id'] = $request->get('sales_id');
            $this->DetailCertificatePositionUpdateOnUnit($post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'id' => $request->get('id'),
                'sales_id' => $request->get('sales_id'),
                'location_id' => $request->get('location_id'),
                'block_id' => $request->get('block_id'),
                'unit_id' => $request->get('unit_id')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function DetailCertificatePositionUpdateOnUnit($request)
    {
        $sales_id = $request['sales_id'];
        $last_step ="";
        $last_step_date ="";

        $steps = \App\ModelProp\RefUnitCertificatePosition::where('sales_id', "$sales_id")->orderBy('step_date', 'DESC')->get();
        if ($steps->count() > 0) {
            $last_step = $steps[0]->step_position;
            $last_step_date = $steps[0]->step_date;
        }

        $unit = \App\ModelProp\TrnsctSales::where('sales_id', "$sales_id")
                ->update(["certificate_position" => $last_step, "certificate_position_date" => $last_step_date]);
    }

    public function detailInstallment(Request $request)
    {
        $sales_id = $request->get('sales_id');

        $sale = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer'])
            ->where('sales_id', "$sales_id")
            ->first();

        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();

        $payments = \App\ModelProp\TrnsctSalesPaid::where('sales_id', "$sales_id")
            ->where('sales_paid_cancel', '0')
            ->where('sales_paid_kpr', '0')
            ->orderBy('sales_paid_num', 'DESC')
            ->get();

        $bills = TrnsctSalesBill::where('sales_id', $sales_id)->orderBy('created_at', 'ASC')->get();

        return view('prop.trnsct_sales.detail_installment',
            compact(
                'sales_id',
                'sale',
                'payments',
                'cashes',
                'bills'
            )
        );
    }

    public function detailInstallmentStore(Request $request)
    {
        $data_post = $request->all();
        $data_post['sales_paid_kpr'] = '0';

        // simpan pembayaran
        $this->storeToSalesPaid($data_post);

        // update table trnsct_sales kolom ttl_trnsct_paid = jml pembayaran
        $new_trnsct_paid = $this->updateTrnsctPaid($data_post);

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil disimpan',
            'sales_id' => $request->get('sales_id'),
        ];

        return response()->json($res);
    }

    public function detailInstallmentDelete(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $sales_paid_id = $request->get('sales_paid_id');
        $amount_delete = $request->get('amount_delete');
        $ttl_trnsct_paid = $request->get('ttl_trnsct_paid');

        // kurangi trnsct_paid di table trnsct_sales
        \App\ModelProp\TrnsctSales::where("sales_id", "$sales_id")->update([
            "ttl_trnsct_paid" => $ttl_trnsct_paid - $amount_delete
        ]);

        // batalkan sales_paid
        $sales_paid = \App\ModelProp\TrnsctSalesPaid::findOrFail($sales_paid_id);
        $sales_paid->sales_paid_cancel = '1';
        $sales_paid->save();

        $res = [
            'status' => 'success',
            'text' => 'Pembayaran berhasil dibatalkan',
            'sales_id' => $request->get('sales_id'),
            'sales_paid_id' => $request->get('sales_paid_id'),
        ];

        return response()->json($res);
    }

    public function detailInstallmentUpdate(Request $request)
    {
        $sales_paid_id = $request->get('sales_paid_id');
        $sales_paid_desc = $request->get('sales_paid_desc');

        // update deskripsi sales_paid
        $sales_paid = \App\ModelProp\TrnsctSalesPaid::findOrFail($sales_paid_id);
        $sales_paid->sales_paid_desc = $sales_paid_desc;
        $sales_paid->save();

        $res = [
            'status' => 'success',
            'text' => 'Pembayaran berhasil dibatalkan',
            'sales_id' => $request->get('sales_id'),
            'sales_paid_id' => $request->get('sales_paid_id'),
        ];

        return response()->json($res);
    }

    public function detailInstallmentBiayaTambahan(Request $request)
    {
        $id = $request->get('id');
        $sales = \App\ModelProp\TrnsctSales::findOrFail($id);

        $selisih = $sales->ttl_trnsct - $sales->additional_cost;
        $selisih += str_replace(".", "", $request->get('additional_cost'));
        $sales->additional_cost = str_replace(".", "", $request->get('additional_cost'));
        $sales->ttl_trnsct = $selisih;
        $sales->save();

        $res = [
            'status' => 'success',
            'text' => 'Berhasil',
            'sales_id' => $request->get('sales_id'),
            'id' => $request->get('id'),
        ];

        return response()->json($res);
    }

    public function detailInstallmentPotongan(Request $request)
    {
        $id = $request->get('id');
        $sales = \App\ModelProp\TrnsctSales::findOrFail($id);
        $sales->discount = str_replace(".", "", $request->get('discount'));
        $sales->discount_desc = $request->get('discount_desc');
        $sales->ttl_trnsct = ($sales->basic_price + $sales->additional_cost) - str_replace('.', '', $request->get('discount'));
        $sales->save();

        $res = [
            'status' => 'success',
            'text' => 'Input potongan berhasil',
            'sales_id' => $request->get('sales_id'),
            'id' => $request->get('id'),
        ];

        return response()->json($res);
    }

    public function detailInstallmentBill(Request $request)
    {
        $new_bill = new TrnsctSalesBill();
        $new_bill->sales_id = $request->sales_id;
        $new_bill->paid_off = $request->paid_off;
        $new_bill->reminder = $request->reminder;
        $new_bill->due_date = $request->due_date;
        $new_bill->save();

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil disimpan',
            'sales_id' => $request->get('sales_id'),
        ];

        return response()->json($res);
    }


    public function detailMarketingFee(Request $request)
    {
        $id = $request->get('id');
        $sales_id = $request->get('sales_id');
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $affiliate_num = $request->get('affiliate_num');
        $affiliate_fee = $request->get('affiliate_fee');
        $affiliate_fee_paid = $request->get('affiliate_fee_paid');

        $payments = DB::table('fin_trnsct_paid')
            ->select('fin_trnsct.finance_date', 'fin_trnsct.finance_amount', 'fin_trnsct_paid.finance_paid_id')
            ->leftJoin('fin_trnsct', 'fin_trnsct_paid.finance_id', '=', 'fin_trnsct.finance_id')
            ->where('fin_trnsct.is_marketing_fee', "1")
            ->where('fin_trnsct.sales_id', $id)
            ->where('fin_trnsct.finance_cancel', '=', '0')
            ->orderBy('fin_trnsct.finance_date', 'ASC')
            ->get();

        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();

        return view('prop.trnsct_sales.detail_marketing_fee',
            compact(
                'id',
                'sales_id',
                'payments',
                'cashes',
                'sales_id',
                'location_id',
                'block_id',
                'unit_id',
                'affiliate_num',
                'affiliate_fee',
                'affiliate_fee_paid'
            )
        );
    }

    public function detailMarketingFeeStore(Request $request)
    {
        $num = $this->getInvoiceNumberForCost('COST');
        $affiliate_num = $request->get('affiliate_num');

        // get sdm_id
        $sdm = \App\ModelProp\RefMarketer::where('affiliate_num', "$affiliate_num")->first();
        $sdm_id = $sdm->sdm_id;


        // store to fin_trnsct
        $finance = new \App\ModelFinance\Trnsct();
        $finance->finance_type = 'PENGELUARAN';
        $finance->finance_date = $request['date'];
        $finance->finance_sdm_id = $sdm_id;
        $finance->finance_desc = $request['desc'];
        $finance->finance_amount = str_replace(".", "", $request['amount']);
        $finance->finance_amount_paid = str_replace(".", "", $request['amount']);
        $finance->is_marketing_fee = '1';
        $finance->sales_id = $request->get('id');

        // store to fin_trnsct_item
        if ($finance->save()) {
            $finance_item = new \App\ModelFinance\TrnsctItem();
            $finance_item->finance_id = $finance->finance_id;
            $finance_item->finance_item_account_id = '8';
            $finance_item->finance_item_amount = str_replace(".", "", $request['amount']);
            $finance_item->save();

            $num = $this->getInvoiceNumberForCost('COST');

            $finance_paid = new \App\ModelFinance\TrnsctPaid();
            $finance_paid->finance_id = $finance->finance_id;
            $finance_paid->finance_paid_num = $num;
            $finance_paid->finance_paid_date = $request->get('date');
            $finance_paid->finance_paid_cash_id = $request->get('cash_id');
            $finance_paid->finance_paid_amount = str_replace(".", "", $request->get('amount'));
            $finance_paid->finance_paid_sdm_id = $sdm_id;
            $finance_paid->save();
        }

        // update table trnsct_sales kolom ttl_trnsct_paid = jml pembayaran
        $data_post = $request->all();
        $res_update_marketing_fee_paid = $this->updateMarketingFeePaid($request);

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil disimpan',
            'id' => $request->get('id'),
            'sales_id' => $request->get('sales_id'),
            'location_id' => $request->get('location_id'),
            'block_id' => $request->get('block_id'),
            'unit_id' => $request->get('unit_id'),
            'affiliate_num' => $request->get('affiliate_num'),
            'affiliate_fee' => $request->get('affiliate_fee'),
            'affiliate_fee_paid' => $res_update_marketing_fee_paid,
        ];

        return response()->json($res);
    }

    public function updateMarketingFeePaid($request)
    {
        $data = \App\ModelProp\TrnsctSales::findOrFail($request['id']);
        $data->affiliate_fee_paid = $data->affiliate_fee_paid + str_replace('.', '', $request['amount']);
        $data->save();

        return $data->affiliate_fee_paid;
    }

    public function printMarketingFeePayment($id)
    {
        $payment = \App\ModelFinance\TrnsctPaid::with("cash_bank", "trnsct", "sdm")->findOrFail($id);
        return view('prop.trnsct_sales.print_marketing_fee_payment', compact('payment'));
    }

    public function detailKpr(Request $request)
    {
        $sales_id = $request->get('sales_id');

        $sale = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'bank'])
            ->where('sales_id', "$sales_id")
            ->first();

        $banks = \App\ModelProp\RefBank::with(['sdm' => function ($q) {$q->orderBy('name', 'ASC');}])->get();

        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_id', 'ASC')->get();

        $payments = \App\ModelProp\TrnsctSalesPaid::with(['installment_debt', 'installment_debt.sdm'])
            ->where('sales_id', "$sales_id")
            ->where('sales_paid_kpr', '1')
            ->orderBy('sales_paid_num', 'ASC')
            ->get();

        $returns = \App\ModelProp\TrnsctSalesReturn::where('sales_id', "$sales_id")
            ->orderBy('sales_return_num', 'ASC')
            ->get();

        $debts = \App\ModelFinance\RefDebtToBank::with(['sdm'])->orderBy('sdm_id')->get();

        return view('prop.trnsct_sales.detail_kpr',
            compact(
                'sale',
                'cashes',
                'payments',
                'returns',
                'banks',
                'debts'
            )
        );
    }

    public function detailKprStore(Request $request)
    {
        $notif = [
            "status" => "danger",
            "title" => "Gagal!",
            "text" => "Mohon maaf telah terjadi kesalahan di sistem."
        ];

        // Jika diterima
        if ($request->get('kpr_status') == 'ACCEPTED') {
            // update data kpr di trnsct
            $sale = \App\ModelProp\TrnsctSales::findOrFail($request->get('id'));
            $sale->kpr_result_date = $request->get('kpr_result_date');
            $sale->kpr_status = $request->get('kpr_status');
            $sale->ttl_trnsct = ($sale->ttl_trnsct - $sale->additional_cost) + str_replace(".", "", $request->get('additional_cost'));
            $sale->additional_cost = str_replace(".", "", $request->get('additional_cost'));
            $sale->kpr_credit_acc = str_replace(".", "", $request->get('kpr_credit_acc'));
            $sale->kpr_desc = $request->get('kpr_desc');
            $sale->save();

            $notif = [
                "status" => "success",
                "title" => "Berhasil!",
                "text" => "Approval pengajuan berhasil disimpan."
            ];

        } else {
            // Jika tidak diterima
            // update status trnsct
            $sale = \App\ModelProp\TrnsctSales::with(['bank', 'customer', 'marketer'])->findOrFail($request->get('id'));
            $sale->kpr_result_date = $request->get('kpr_result_date');
            $sale->kpr_status = $request->get('kpr_status');
            $sale->kpr_desc = $request->get('kpr_desc');
            $sale->return_plan = str_replace(".", "", $request->get('return_plan'));
            $sale->status = 'CANCEL';
            $sale->save();

            $location_id = $request['location_id'];
            $block_id = $request['block_id'];
            $unit_id = $request['unit_id'];

            // update status unit ke READY
            \App\ModelProp\RefUnit::where('location_id', "$location_id")
                ->where('block_id', "$block_id")
                ->where('number', "$unit_id")
                ->update([
                    "customer_id" => '',
                    "marketer_id" => '',
                    "status" => 'READY',
                ]);

            if ($sale->marketer->marketer_type == '2') {
                $this->callUmaraEndpointUpdateSales($request->get('id'));
            }
        }

        $notif = [
            "status" => "success",
            "title" => "Berhasil!",
            "text" => "Approval pengajuan berhasil disimpan.",
            "sales_id" => $request['sales_id']
        ];

        return response()->json($notif);
    }

    public function detailKprCreditStore(Request $request)
    {
        // update data kpr di trnsct
        $sale = \App\ModelProp\TrnsctSales::with(['bank', 'customer'])->findOrFail($request->get('id'));
        $sale->kpr_credit_acc_paid += str_replace(".", "", $request->get('ttl_trnsct_paid'));
        $sale->ttl_trnsct_paid += str_replace(".", "", $request->get('ttl_trnsct_paid'));
        $sale->save();

        $data_post = $request->all();
        $data_post['sales_paid_kpr'] = '1';

        // simpan pembayaran
        $sales_paid_id = $this->storeToSalesPaid($data_post);

        if ($request->get('debt_id') != '') {
            $debt = \App\ModelFinance\RefDebtToBank::findOrFail($request->get('debt_id'));
            $data_post['debt_sdm_id'] = $debt->sdm_id;
            $data_post['sales_paid_id'] = $sales_paid_id;

            // simpan ke operasional
            $this->detailKprDebtInstallment($data_post);

            // kurangi hutang bank
            $this->detailKprReduceBankDebt($data_post['debt_id'], $data_post['installment_amount']);
        }

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil disimpan',
            'sales_id' => $request->get('sales_id')
        ];

        return response()->json($res);
    }

    public function detailKprDebtInstallment($request)
    {
        $num = $this->getInvoiceNumberForCost('COST');

        // store to fin_trnsct
        $finance = new \App\ModelFinance\Trnsct();
        $finance->finance_type = 'PENGELUARAN';
        $finance->finance_date = $request['trnsct_date'];
        $finance->finance_sdm_id = $request['debt_sdm_id'];
        $finance->finance_desc = $request['installment_desc'];
        $finance->finance_amount = str_replace(".", "", $request['installment_amount']);
        $finance->finance_amount_paid = str_replace(".", "", $request['installment_amount']);
        $finance->is_load_installment_bank = '1';
        $finance->sales_paid_id = $request['sales_paid_id'];

        // store to fin_trnsct_item
        if ($finance->save()) {
            $finance_item = new \App\ModelFinance\TrnsctItem();
            $finance_item->finance_id = $finance->finance_id;
            $finance_item->finance_item_account_id = '13';
            $finance_item->finance_item_amount = str_replace(".", "", $request['installment_amount']);
            $finance_item->save();
        }
    }

    public function detailKprReduceBankDebt($debt_id, $amount)
    {
        $debt = \App\ModelFinance\RefDebtToBank::findOrFail($debt_id);
        $debt->debt_total_paid = $debt->debt_total_paid + (int) str_replace(".","",$amount);
        $debt->save();
    }

    public function getInvoiceNumberForCost($type)
    {
        $this_month = date('Y-m');
        $booking_id = 'INV/'.$type.'/';

        $last_invoice = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
            ->where('type', "$type")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_invoice) == 0 ) {
            $order_id = "0001";

            $new = new \App\ModelFinance\RefInvoiceNumber();
            $new->type = $type;
            $new->month = "$this_month";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $order_id = sprintf('%04d', $last_invoice[0]->serial_num);
            $update = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
                ->where("type", "$type")
                ->update(["serial_num" => $order_id + 1]);
        }

        $booking_id .= date('Y/m/') . $order_id;
        return $booking_id;
    }

    public function detailKprChangeBank(Request $request)
    {
        // update data kpr di trnsct
        $sale = \App\ModelProp\TrnsctSales::with(['bank', 'customer'])->findOrFail($request->get('id'));
        $sale->kpr_bank = $request->get('kpr_bank');

        if ($sale->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $request->get('sales_id')
            ];
        } else {
            $res = [
                'status' => 'danger',
                'text' => 'Data gagal disimpan',
                'sales_id' => $request->get('sales_id')
            ];
        }

        return response()->json($res);
    }

    public function detailKprCreditStoreCancel(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $sales_paid_id = $request->get('sales_paid_id');
        $sales_paid_amount = $request->get('sales_paid_amount');
        $ttl_trnsct_paid = $request->get('ttl_trnsct_paid');
        $kpr_credit_acc_paid = $request->get('kpr_credit_acc_paid');

        // kurangi trnsct_sales di table trnsct_sales
        \App\ModelProp\TrnsctSales::where("sales_id", "$sales_id")->update([
            "ttl_trnsct_paid" => $ttl_trnsct_paid - $sales_paid_amount,
            "kpr_credit_acc_paid" => $kpr_credit_acc_paid - $sales_paid_amount,
        ]);

        // batalkan sales_paid
        $sales_paid = \App\ModelProp\TrnsctSalesPaid::findOrFail($sales_paid_id);
        $sales_paid->sales_paid_cancel = '1';
        $sales_paid->save();

        if ($sales_paid->debt_id != '') {
            // batalkan operasional
            $cancel_cost = \App\ModelFinance\Trnsct::where('sales_paid_id', "$sales_paid_id")
                ->update(['finance_cancel' => '1']);

            $cancel_cost = \App\ModelFinance\Trnsct::where('sales_paid_id', "$sales_paid_id")
                ->first();

            if ($cancel_cost) {
                $cancel_cost_paid = \App\ModelFinance\TrnsctPaid::where("finance_id", $cancel_cost->finance_id)
                    ->update(["finance_paid_cancel" => "1"]);
            }

            // batalkan pengurangan bank
            $cancel_reduce_debt = \App\ModelFinance\RefDebtToBank::findOrFail($sales_paid->debt_id);
            $cancel_reduce_debt->debt_total_paid = $cancel_reduce_debt->debt_total_paid - $sales_paid->installment_amount;
            $cancel_reduce_debt->save();
        }

        $res = [
            'status' => 'success',
            'text' => 'Pencairan KPR berhasil dibatalkan',
            'sales_id' => $request->get('sales_id'),
            'sales_paid_id' => $request->get('sales_paid_id'),
        ];

        return response()->json($res);
    }

    public function detailKprReturn(Request $request)
    {
        // get invoice number
        $TrnsctJurnalController = $this->TrnsctJurnalController;
        $sales_return_num = $TrnsctJurnalController->invoiceNumber('COST');

        $sales_return = new \App\ModelProp\TrnsctSalesReturn();
        $sales_return->sales_id = $request['sales_id'];
        $sales_return->sales_return_num = $sales_return_num;
        $sales_return->sales_return_person = $request['customer_name'];
        $sales_return->sales_return_date = $request['trnsct_date'];
        $sales_return->sales_return_cash_id = $request['account_id'];
        $sales_return->sales_return_desc = "Return " . $request['sales_id'];
        $sales_return->sales_return_amount = str_replace(".", "", $request['amount']);
        $sales_return->sales_return_admin = Session::get('auth_nama');;
        $sales_return->save();

        \App\ModelProp\TrnsctSales::where("sales_id", "$request[sales_id]")->update([
            "return_plan_paid" => (int) $request->get("return_plan_paid") + (int) str_replace(".", "", $request['amount'])
        ]);

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil disimpan',
            'sales_id' => $request->get('sales_id')
        ];

        return response()->json($res);
    }

    public function detailKprReturnCancel(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $sales_return_id = $request->get('sales_return_id');
        $sales_return_amount = $request->get('sales_return_amount');
        $return_plan_paid = $request->get('return_plan_paid');

        // kurangi trnsct_sales di table trnsct_sales
        \App\ModelProp\TrnsctSales::where("sales_id", "$sales_id")->update([
            "return_plan_paid" => $return_plan_paid - $sales_return_amount
        ]);

        // batalkan sales_paid
        $sales_return = \App\ModelProp\TrnsctSalesReturn::findOrFail($sales_return_id);
        $sales_return->sales_return_cancel = '1';
        $sales_return->save();

        $res = [
            'status' => 'success',
            'text' => 'Return berhasil dibatalkan',
            'sales_id' => $request->get('sales_id'),
            'sales_return_id' => $request->get('sales_return_id'),
        ];

        return response()->json($res);
    }

    public function deleteSales(Request $request)
    {
        $sales_id = $request->get('delete_sales_id');

        DB::beginTransaction();

        try {            
            // delete transaction sales
            $sales = \App\ModelProp\TrnsctSales::with('marketer')->find($sales_id);

            // update unit
            $unit = \App\ModelProp\RefUnit::where("location_id", "$sales->location_id")
                ->where("block_id", "$sales->block_id")
                ->where("number", "$sales->unit_id")
                ->update([
                    "status" => "READY",
                    "customer_id" => null,
                ]);

            // delete sales payment
            \App\ModelProp\TrnsctSalesPaid::where('sales_id', "$sales->sales_id")->delete();

            // delete sales return
            \App\ModelProp\TrnsctSalesReturn::where('sales_id', "$sales->sales_id")->delete();

            // delete sales step
            \App\ModelProp\RefUnitSalesStep::where('sales_id', "$sales->sales_id")->delete();

            // delete unit certificate step
            \App\ModelProp\RefUnitCertificateStep::where('sales_id', "$sales->sales_id")->delete();

            // delete unit certificate position
            \App\ModelProp\RefUnitCertificatePosition::where('sales_id', "$sales->sales_id")->delete();

            if ($sales->marketer && $sales->marketer->marketer_type == 2) {
                $this->callUmaraEndpointDeleteSales($sales_id);
            }

            $sales->delete();

            $res = [
                'status' => 'success',
                'text' => 'Transaksi berhasil dihapus',
            ];

            DB::commit();
        } catch (Exception $th) {
            DB::rollBack();
            $res = [
                'status' => 'error',
                'text' => 'Transaksi gagal dihapus, silahkan hubungi programmer. <br>' . $th->getMessage(),
            ];
        }

        return response()->json($res);
    }

    public function updateMarketer(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $sales = \App\ModelProp\TrnsctSales::with('marketer')->findOrFail($sales_id);

        if ($request->get('affiliate_num') != '') {
            DB::beginTransaction();
            try {
                $affiliteNum = $request->affiliate_num;
                $affiliateFee = $sales->affiliate_fee;
                $locationId = $sales->location_id;
                $blockId = $sales->block_id;
                $unitId = $sales->unit_id;
                $marketerTypeIdOld = ($sales->marketer) ? $sales->marketer->marketer_type : null;

                // get marketer type from marketer
                $marketer = RefMarketer::where('affiliate_num', $affiliteNum)->first();
                $marketerTypeId = $marketer->marketer_type;

                // get subsidi type from unit
                $unit = RefUnit::where('location_id', $locationId)->where('block_id', $blockId)->where('number', $unitId)->first();
                $subsidiTypeId = $unit->subsidi_type_id;
                $priceCash = $unit->harga_jual_cash;
                $priceKpr = $unit->harga_jual_kpr;

                // get setting
                $setting = RefMarketingFeeSetting::where('marketer_type_id', $marketerTypeId)->where('subsidi_type_id', $subsidiTypeId)->first();
                if ($setting) {
                    $affiliateFee = $setting->booking_fee + $setting->akad_fee;
                }

                $sales->affiliate_num = $affiliteNum;
                $sales->affiliate_fee = $affiliateFee;
                $sales->save();

                // jika marketing sebelumnya umara dan baru buka maka hapus data penjualan umara
                if ($marketerTypeIdOld == 2 && $marketerTypeId != 2) {
                    $this->callUmaraEndpointDeleteSales($sales_id);
                }

                // jika marketing lama kosong dan baru umara maka create penjualan umara
                if ($marketerTypeIdOld == null && $marketerTypeId == 2) {
                    $this->callUmaraEndpointUpdateSales($sales_id);
                }

                DB::commit();
                $res = [
                    'id' => $sales_id,
                    'sales_id' => $sales->sales_id,
                    'status' => 'success',
                    'text' => 'Marketer berhasil diupdate!',
                ];

                return response()->json($res);
            } catch (Exception $th) {
                DB::rollBack();
                throw new Exception($th->getMessage(), 400);
            }
        }

        $res = [
            'id' => $sales_id,
            'sales_id' => $sales->sales_id,
            'status' => 'success',
            'text' => 'Marketer berhasil diupdate!',
        ];

        return response()->json($res);
    }

    public function updateAdditionalCost(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $additional_cost = str_replace(".", "", $request->get('additional_cost'));
        $additional_cost_note = $request->get('additional_cost_note');

        $sales = \App\ModelProp\TrnsctSales::findOrFail($sales_id);
        $sales->ttl_trnsct = ($sales->ttl_trnsct - $sales->additional_cost) + $additional_cost;
        $sales->additional_cost = $additional_cost;
        $sales->additional_cost_note = $additional_cost_note;
        $sales->save();

        return redirect()->back();
    }

    public function printKeyHandover(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $key_handover_date = $request->get('key_handover_date');
        $key_handover_person = $request->get('key_handover_person');
        $key_handover_number = $this->getKeyHandoverNumber($key_handover_date);

        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer'])->findOrFail($sales_id);
        $sales->key_handover_date = $key_handover_date;
        $sales->key_handover_person = $key_handover_person;

        if ($sales->key_handover_number == '') {
            $sales->key_handover_number = $key_handover_number;
        }

        $sales->save();

        return view('prop.trnsct_sales.print_key_handover', compact('sales'));
    }

    public function getKeyHandoverNumber($key_handover_date)
    {
        $year = substr($key_handover_date, 0, 4);
        $result = \App\ModelProp\TrnsctSales::whereYear('key_handover_date', $year)->orderBy('key_handover_number', 'DESC')->limit(1)->get();

        if ($result->count() == 0) {
            $key_handover_number = '0001';
        } else {
            $key_handover_number = sprintf('%04d', $result[0]->key_handover_number) + 1;
        }

        return $key_handover_number;
    }

    public function printComplain(Request $request)
    {
        $sales_id = $request->get('sales_id');
        $complain_date = $request->get('complain_date');
        $complain_list = $request->get('complain_list');
        $complain_person = $request->get('complain_person');

        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer'])->findOrFail($sales_id);
        $sales->complain_date = $complain_date;
        $sales->complain_list = $complain_list;
        $sales->complain_person = $complain_person;
        $sales->save();

        return view('prop.trnsct_sales.print_complain', compact('sales'));
    }

    public function printMissingRequirementDocument(Request $request)
    {
        $sales_id = $request->get('sales_id');

        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'bank', 'marketer'])->findOrFail($sales_id);
        $sales->kpr_missing_requirement_document = $request->kpr_missing_requirement_document;
        $sales->kpr_document_collection_limit = $request->kpr_document_collection_limit;
        $sales->save();

        return view('prop.trnsct_sales.print_missing_requirement_document', compact('sales'));
    }

    public function printWarningLetter2(Request $request)
    {
        $sales_id = $request->get('sales_id');

        $sales = \App\ModelProp\TrnsctSales::with(['customer'])->findOrFail($sales_id);
        $sales->kpr_document_collection_limit_2 = $request->kpr_document_collection_limit_2;
        $sales->save();

        return view('prop.trnsct_sales.print_warning_letter_2', compact('sales'));
    }

    public function printCancellationLetter(Request $request)
    {
        $sales_id = $request->id;
        $sales = \App\ModelProp\TrnsctSales::with(['customer'])->findOrFail($sales_id);

        return view('prop.trnsct_sales.print_cancellation_letter', compact('sales'));
    }
    

    public function printBill(Request $request)
    {
        $result = TrnsctSalesBill::with(['sales', 'sales.location', 'sales.block', 'sales.unit', 'sales.unit.unitType', 'sales.customer', 'sales.bank'])->find($request->id);
        return view('prop.trnsct_sales.print_bill', compact('result'));    
    }

    public function uploadDocumentKtpKk(Request $request)
    {
        $file_ktp_name= null;
        $file_kk_name = null;

        $data = RefSdm::findOrFail($request->customer_id);

        if ($request->file('file_ktp')) {
            if ($data->file_ktp != '') {
                File::delete('public/customer_document/'.  $data->file_ktp);
            }

            $file_ktp = $request->file('file_ktp');

            // rename filename
            $file_ktp_name = "ktp_" . $request->customer_id . "." . $file_ktp->getClientOriginalExtension();

            // upload file
            $file_ktp->move('public/customer_document', $file_ktp_name);
        }

        if ($request->file('file_kk')) {
            if ($data->file_kk != '') {
                File::delete('public/customer_document/'.  $data->file_kk);
            }
            $file_kk = $request->file('file_kk');

            // rename filename
            $file_kk_name = "kk_" . $request->customer_id . "." . $file_ktp->getClientOriginalExtension();

            // upload file
            $file_kk->move('public/customer_document', $file_kk_name);
        }

        
        $data->file_ktp = $file_ktp_name;
        $data->file_kk = $file_kk_name;     

        if ($data->save()) {
            $res = [
                'id' => $request->id,
                'status' => 'success',
                'text' => 'Data berhasil diupload'
            ];
        } else {
            $res = [
                'id' => $request->id,
                'status' => 'failed',
                'text' => 'Data gagal diupload'
            ];
        }

        return response()->json($res);
    }

    public function getMarketingFee(Request $request)
    {
        $affiliteNum = $request->affiliate_num;
        $locationId = $request->location_id;
        $blockId = $request->block_id;
        $unitId = $request->unit_id;

        // get marketer type from marketer
        $marketer = RefMarketer::where('affiliate_num', $affiliteNum)->first();
        if (!$marketer) return $this->showResultMarketingFee(0,0);
        $marketerTypeId = $marketer->marketer_type;

        // get subsidi type from unit
        $unit = RefUnit::where('location_id', $locationId)->where('block_id', $blockId)->where('number', $unitId)->first();
        if (!$unit) return $this->showResultMarketingFee(0,0);

        $subsidiTypeId = $unit->subsidi_type_id;
        $priceCash = $unit->harga_jual_cash;
        $priceKpr = $unit->harga_jual_kpr;

        // get setting
        $setting = RefMarketingFeeSetting::where('marketer_type_id', $marketerTypeId)->where('subsidi_type_id', $subsidiTypeId)->first();
        if (!$setting) return $this->showResultMarketingFee(0,0);

        return $this->showResultMarketingFee($setting->booking_fee, $setting->akad_fee);
    }

    public function showFormCustomMarketingFee()
    {
        $html = "<div class='form-group'>";
        $html .= "<label>Marketing Fee</label>";
        $html .= "<input type='text' name='affiliate_fee' class='form-control money' value='0'>";
        $html .= "</div>";

        return $html;
    }

    public function showResultMarketingFee(int $bookingFee, int $akadFee)
    {
        $html = "<input type='hidden' name='affiliate_fee' value='".($bookingFee+$akadFee)."'>";
        $html .= "<div class='form-group'>";
        $html .= "<label>Marketing Fee</label> <br>";
        $html .= (number_format($bookingFee + $akadFee,0,',','.'));

        if (($akadFee + $bookingFee) == 0) {
            $html .= "<br><span class='text-red'>Marketing fee belum disetting, silahkan cek :</span>";
            $html .= "<br>1. Menu Unit Rumah > Unit > pilih unit yg dituju lalu klik Update, pastikan form JENIS SUBSIDI terisi dengan benar.";
            $html .= "<br>2. Menu Marketing > Marketer > cek apa jenis marketer dari marketer yg dituju.";
            $html .= "<br>3. Menu Marketing > Jenis Marketer > klik jenis marketer yg dituju, pastikan settingan marketing fee setiap jenis rumah  terisi.";
        }

        return $html;
    }

    public function callUmaraEndpointUpdateSales($id) 
    {
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'unit', 'block', 'customer', 'last_sales_step', 'marketer'])->find($id)->toArray();

        if ($sales['marketer']['marketer_type'] == '2') {
            $sales_encoded = json_encode($sales);
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://pro.umara.id/api/update-sales-from-bmm',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $sales_encoded,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: Bearer YmlzbWlsbGFoaXJyYWhtYW5pcnJhaGltLXVtYXJh'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            echo $response;
        }
    }

    public function callUmaraEndpointDeleteSales($id) 
    {
        $sales = \App\ModelProp\TrnsctSales::find($id);
        $sales_id = $sales->sales_id;
        $sales_encoded = json_encode(['sales_id' => $sales_id]);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://pro.umara.id/api/delete-sales-from-bmm',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $sales_encoded,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer YmlzbWlsbGFoaXJyYWhtYW5pcnJhaGltLXVtYXJh'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function updateAffiliateFee(Request $request)
    {
        $id = $request->id;
        $affiliate_fee = str_replace(".", "", $request->affiliate_fee);

        $sales = \App\ModelProp\TrnsctSales::with('marketer')->findOrFail($id);
        $sales->affiliate_fee = $affiliate_fee;

        if ($sales->save()) {
            if ($sales->marketer->marketer_type == 2) {
                $this->callUmaraEndpointUpdateSales($id);
            }
            $res = [
                'id' => $request->id,
                'status' => 'success',
                'text' => 'Data berhasil diupload'
            ];
        } else {
            $res = [
                'id' => $request->id,
                'status' => 'failed',
                'text' => 'Data gagal diupload'
            ];
        }

        return response()->json($res);
    }
}
