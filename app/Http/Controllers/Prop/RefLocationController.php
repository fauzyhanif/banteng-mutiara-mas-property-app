<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefLocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_location.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefLocation::all();

        return DataTables::of($datas)
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.location.form_update', $datas->location_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_location.form_store');
    }

    public function store(Request $request)
    {
        $loc = new \App\ModelProp\RefLocation();
        $loc->name = $request->get('name');
        $loc->description = $request->get('description');
        $loc->address = $request->get('address');

        if ($loc->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefLocation::findOrFail($id);

        return view('prop.ref_location.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $loc = \App\ModelProp\RefLocation::findOrFail($id);
        $loc->name = $request->get('name');
        $loc->description = $request->get('description');
        $loc->address = $request->get('address');

        if ($loc->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
