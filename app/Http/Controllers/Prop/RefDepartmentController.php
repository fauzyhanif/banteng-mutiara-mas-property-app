<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefDepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_department.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_department as a')
            ->leftJoin('prop_ref_department as b', 'a.parent', '=', 'b.dept_id')
            ->select('a.dept_id', 'a.name', 'b.name as parent_name')
            ->orderBy('a.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.department.form_update', $datas->dept_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        $depts = \App\ModelProp\RefDepartment::all();
        return view('prop.ref_department.form_store', \compact('depts'));
    }

    public function store(Request $request)
    {
        $dept = new \App\ModelProp\RefDepartment();
        $dept->name = $request->get('name');
        $dept->parent = $request->get('parent');

        if ($dept->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $depts = \App\ModelProp\RefDepartment::all();
        $data = \App\ModelProp\RefDepartment::findOrFail($id);

        return view('prop.ref_department.form_update', \compact('data', 'depts'));
    }

    public function update(Request $request, $id)
    {
        $dept = \App\ModelProp\RefDepartment::findOrFail($id);
        $dept->name = $request->get('name');
        $dept->parent = $request->get('parent');

        if ($dept->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
