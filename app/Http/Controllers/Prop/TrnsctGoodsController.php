<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class TrnsctGoodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $type = ($request->has('type')) ? $request->input('type') : 1;
        return view('prop.trnsct_goods.index', compact('type'));
    }

    public function listData(Request $request)
    {
        $type = $request->get('type');
        $datas = \App\ModelProp\TrnsctGoods::with(['mandor', 'store'])
            ->where('type', "$type")
            ->orderBy('trnsct_date', 'DESC')
            ->get();

        return DataTables::of($datas)
            ->editColumn('trnsct_date', function($datas)
            {
                $text = date('d/m/Y', strtotime($datas->trnsct_date));
                return $text;
            })
            ->addColumn('related_person', function($datas){
                $text = "";

                if ($datas->type == 'IN') {
                    if ($datas->buy_from != "") {
                        $text = $datas->store->name;
                    }
                } else {
                    if ($datas->mandor_id != "") {
                        $datas->mandor->name;
                    }
                }
                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . url('/prop/trnsct_goods/form_update?id='.$datas->trnsct_id.'&type='.$datas->type) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                // detail link
                $btn .= "<a href='" . url('/prop/trnsct_goods/detail?id='.$datas->trnsct_id.'&type='.$datas->type) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'related_person'])
            ->toJson();
    }

    public function detail(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');

        $goods = \App\ModelProp\TrnsctGoods::where('trnsct_id', $id)
            ->with(['mandor', 'store'])
            ->first();

        $goods_item = \App\ModelProp\TrnsctGoodsItem::with(['goods'])
            ->where('trnsct_id', $id)
            ->orderBy('goods_id', 'ASC')
            ->get();

        return view('prop.trnsct_goods.detail', compact('goods', 'goods_item', 'type'));
    }

    public function formStore(Request $request)
    {
        $type = $request->get('type');
        return view('prop.trnsct_goods.form_store', compact('type'));
    }

    public function formStoreItem(Request $request)
    {
        $id = $request->get('trnsct_id');
        $type = $request->get('type');
        return view('prop.trnsct_goods.form_store_item', compact('id', 'type'));
    }

    public function store(Request $request)
    {
        $goods = new \App\ModelProp\TrnsctGoods();
        $goods->type = $request->get('type');
        $goods->trnsct_date = $request->get('trnsct_date');

        if ($request->get('type') == 'IN') {
            $goods->buy_from = $request->get('buy_from');
        } else {
            $goods->mandor_id = $request->get('mandor_id');
        }

        $goods->note = $request->get('note');
        $goods->admin = Session::get('auth_nama');

        if ($goods->save()) {
            return redirect('/prop/trnsct_goods/form_store_item?trnsct_id='.$goods->trnsct_id.'&type='.$request->get('type'));
        } else {
            return back()->with('status', 'Data gagal disimpan!');
        }
    }

    public function storeItem(Request $request)
    {
        $goods = new \App\ModelProp\TrnsctGoodsItem();
        $goods->trnsct_id = $request->get('trnsct_id');
        $goods->goods_id = $request->get('goods_id');
        $goods->type = $request->get('type');
        $goods->qty = $request->get('qty');

        if ($goods->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function viewListItem($id)
    {
        $datas = \App\ModelProp\TrnsctGoodsItem::with(['goods'])
            ->where('trnsct_id', $id)
            ->orderBy('goods_id', 'ASC')
            ->get();

        return view('prop.trnsct_goods.view_list_item', \compact('datas'));
    }

    public function deleteItem(Request $request)
    {
        $id = $request->get('trnitem_id');
        $data = \App\ModelProp\TrnsctGoodsItem::findOrFail($id);
        if ($data->delete()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil dihapus'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal dihapus'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');
        $data = \App\ModelProp\TrnsctGoods::findOrFail($id);

        return view('prop.trnsct_goods.form_update', \compact('data', 'type'));
    }

    public function update(Request $request, $id)
    {
        $goods = \App\ModelProp\TrnsctGoods::findOrFail($id);
        $goods->type = $request->get('type');
        $goods->trnsct_date = $request->get('trnsct_date');
        $goods->due_date = $request->get('due_date');

        if ($request->get('type') == 'IN') {
            $goods->buy_from = $request->get('buy_from');
        } else {
            $goods->mandor_id = $request->get('mandor_id');
        }

        $goods->note = $request->get('note');
        $goods->admin = Session::get('auth_nama');

        if ($goods->save()) {
            return redirect('/prop/trnsct_goods/form_store_item?trnsct_id='.$goods->trnsct_id.'&type='.$request->get('type'));
        } else {
            return back()->with('status', 'Data gagal disimpan!');
        }
    }
}
