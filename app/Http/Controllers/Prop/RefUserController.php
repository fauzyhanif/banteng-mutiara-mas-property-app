<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_user.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefSdm::orderBy("name", "ASC");

        return DataTables::of($datas)
            ->addColumn('rekening', function($datas) {
                $text = $datas->rek_bank." ".$datas->rek_number."<br>".$datas->rek_name;

                return $text;
            })
            ->addColumn('phone_address', function($datas){
                $text = $datas->phone_num;
                $text .= "<br>$datas->address_domicile";

                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.user.form_update', $datas->sdm_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['phone_address',  'actions_link', 'rekening'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_user.form_store');
    }

    public function store(Request $request)
    {
        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->address_domicile = $request->get('address_domicile');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');

        if ($sdm->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];

        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefSdm::findOrFail($id);

        return view('prop.ref_user.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->address_domicile = $request->get('address_domicile');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');
        $sdm->save();

        if ($sdm->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
