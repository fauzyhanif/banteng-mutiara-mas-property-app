<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class TrnsctBookingOnlineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('prop.trnsct_booking_online.index');
    }

    public function detail($id)
    {
        $purchase = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'unit.unitType','customer', 'marketer.sdm'])->findOrFail($id);
        $banks = \App\ModelProp\RefBank::with(['sdm' => function ($q) {$q->orderBy('name', 'ASC');}])->get();

        return view('prop.trnsct_booking_online.detail', compact('purchase', 'banks'));
    }

    public function konfirmasi(Request $request)
    {
        $id = $request->get('id');

        $purchase = \App\ModelProp\TrnsctSales::findOrFail($id);

        if ($request->get('booking_online_status') == 'DITERIMA') {
            $purchase->discount = str_replace(",", "", $request->get('discount'));
            $purchase->ttl_trnsct = $purchase->basic_price - (int) str_replace(".", "", $request->get('discount'));
            $purchase->affiliate_num = $request->get('affiliate_num');
            $purchase->affiliate_fee = str_replace(".", "", $request->get('affiliate_fee'));
            $purchase->status = "SALE";

            if ($purchase->sales_type == 'KPR') {
                $purchase->kpr_bank = $request->get('kpr_bank');
                $purchase->kpr_credit_pengajuan = str_replace(".", "", $request->get('kpr_credit_pengajuan'));
            }

            $unit = \App\ModelProp\RefUnit::where('location_id', "$purchase->location_id")
                ->where('block_id', "$purchase->block_id")
                ->where('number', "$purchase->unit_id")
                ->update(["status" => "SOLD"]);
        }

        $purchase->booking_online_status = $request->get('booking_online_status');

        if ($purchase->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $id,
                'booking_online_status' => $request->get('booking_online_status'),
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function listData()
    {
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm'])
            ->where('booking_online', 'Y')
            ->orderBy('sales_id', 'DESC')
            ->get();

        return DataTables::of($datas)
            ->addColumn('ttl_trnsct', function($datas){
                return number_format($datas->ttl_trnsct,2,',','.');
            })
            ->addColumn('date_and_id_sales', function($datas){
                $html = date("d/m/Y", strtotime($datas->sales_date)) . "<br>";

                $sales_type = "<span class='badge badge-success'>CASH</span> ";
                if ($datas->sales_type == 'KPR') {
                    $sales_type = "<span class='badge badge-primary'>KPR</span> ";
                }

                $html .= $sales_type;
                $html .= "<span class='badge badge-secondary'>$datas->sales_id</span>";

                return $html;
            })
            ->addColumn('location_and_block', function($datas){
                return $datas->location->name . " Blok " . $datas->block->name;
            })
            ->addColumn('unit_type', function($datas){
                return $datas->unit->unitType->name;
            })
            ->addColumn('marketer', function($datas){
                $html = "-";

                if ($datas->affiliate_num != '') {
                    $html = $datas->marketer->sdm->name . "<br>";
                    $html .= "Fee : " . number_format($datas->affiliate_fee,2,',','.');
                }

                return $html;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.booking_online.detail', $datas->id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i>";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['date_and_id_sales', 'location_and_block', 'unit_type', 'marketer', 'actions_link'])
            ->toJson();
    }

}
