<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefBlockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_block.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_block as a')
            ->leftJoin('prop_ref_location as b', 'a.location_id', '=', 'b.location_id')
            ->select('a.block_id', 'a.name', 'b.name as location_name')
            ->orderBy('a.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.block.form_update', $datas->block_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        $locations = \App\ModelProp\RefLocation::all();
        return view('prop.ref_block.form_store', \compact('locations'));
    }

    public function store(Request $request)
    {
        $block = new \App\ModelProp\RefBlock();
        $block->name = $request->get('name');
        $block->location_id = $request->get('location_id');

        if ($block->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $locations = \App\ModelProp\RefLocation::all();
        $data = \App\ModelProp\RefBlock::where('block_id', $id)->with(['location'])->first();

        return view('prop.ref_block.form_update', \compact('data', 'locations'));
    }

    public function update(Request $request, $id)
    {
        $block = \App\ModelProp\RefBlock::findOrFail($id);
        $block->name = $request->get('name');
        $block->location_id = $request->get('location_id');

        if ($block->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
