<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use App\ModelProp\RefSubsidiType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $status_aktif = ($request->has('status')) ? $request->input('status') : 1;
        $status = \App\ModelProp\RefUnitStatus::all();
        $location_id = ($request->has('location_id')) ? $request->input('location_id') : "";
        $block_id = ($request->has('block_id')) ? $request->input('block_id') : "";

        return view('prop.ref_unit.index', compact('status', 'status_aktif', 'location_id', 'block_id'));
    }

    public function getSummaryUnit()
    {
        $status = \App\ModelProp\RefUnit::groupBy('unitstatus_id')
            ->selectRaw('count(*) as total, unitstatus_id')
            ->get();

        $summary = [];
        foreach ($status as $key => $value) {
            $summary[$value->unitstatus_id] = $value->total;
        }

        return $summary;
    }

    public function detail($id)
    {
        $dt_info = \App\ModelProp\RefUnit::where("unit_id", "$id")
            ->with(['unitType', 'location', 'block', 'unitStatus'])
            ->first();

        $price_item = \App\ModelProp\RefUnitPrice::with(['priceItem'])
            ->where('location_id', "$dt_info->location_id")
            ->where('block_id', "$dt_info->block_id")
            ->where('unit_id', "$dt_info->number")
            ->get();

        return view('prop.ref_unit.detail', compact('dt_info', 'price_item'));
    }

    public function listDataJson(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');

        $sales = \App\ModelProp\TrnsctSales::where('block_id', "$block_id")
            ->where('location_id', '$location_id')
            ->where('status', 'SALE')
            ->get();

        $res_sales = [];
        foreach ($sales as $key => $value) {
            $res_sales[$value->block_id."|".$value->unit_id] = [
                "ttl_trnsct_paid" => $value->ttl_trnsct_paid,
                "sisa" => $value->ttl_trnsct - $value->ttl_trnsct_paid,
            ];
        }

        $datas = \App\ModelProp\RefUnit::with('UnitType', 'location', 'block', 'customer')
                ->where('location_id', "$location_id")
                ->where('block_id', "$block_id")
                ->orderBy('number', 'ASC')
                ->get();

        return DataTables::of($datas)
            ->addColumn('type_number', function($datas){
                $text = "<span class='font-weight-bold'>$datas->number</span><br>";
                $text .= $datas->unitType->name;

                return $text;
            })
            ->addColumn('is_publish_html', function($datas){
                $text = "<span class='badge badge-success'>Siap Jual</span>'";

                if ($datas->is_publish == '0') {
                    $text = "<span class='badge badge-danger'>Belum Siap</span>'";
                }

                if ($datas->status == 'SOLD') {
                    $text = '-';
                }

                return $text;
            })
            ->addColumn('customer', function($datas){
                $text = "";
                if ((int)$datas->customer_id > 0 ) {
                    $text = "<span class='font-weight-bold'>".$datas->customer->name."(".$datas->customer->phone_num.")</span><br>";
                    $text .= $datas->customer->institute;
                }

                return $text;
            })
            ->addColumn('selling_price', function($datas){
                $text = "CASH : " . number_format($datas->harga_jual_cash,2,',','.');
                $text .= "<br>KPR : " . number_format($datas->harga_jual_kpr,2,',','.');

                return $text;
            })
            ->addColumn('sisa', function($datas) use($res_sales){
                $res = "0.00";
                if (array_key_exists($datas->block_id."|".$datas->number, $res_sales)) {
                    $res = number_format($res_sales[$datas->block_id."|".$datas->number]['sisa'],2,',','.');
                }
                return $res;
            })
            ->addColumn('ttl_trnsct_paid', function($datas) use($res_sales){
                $res = "0.00";
                if (array_key_exists($datas->block_id."|".$datas->number, $res_sales)) {
                    $res = number_format($res_sales[$datas->block_id."|".$datas->number]['ttl_trnsct_paid'],2,',','.');
                }
                return $res;
            })
            ->addColumn('actions_link', function($datas)
            {
                $block_name = $datas->block->block_name;
                $location_name = $datas->location->location_name;

                // update link
                $btn = "<a href='" . route('prop.unit.form_update', $datas->unit_id) . "' class='btn btn-primary btn-xs btn-block'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                $btn .= "<a href='" . route('prop.unit.detail', $datas->unit_id) . "' class='btn btn-info btn-xs btn-block'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                if ($datas->status == 'READY') {
                    $btn .= "<button type='button' class='btn btn-danger btn-xs btn-block' onclick=\"show_modal_delete('$datas->unit_id', '$datas->number', '$block_name', '$location_name')\">";
                    $btn .= "<i class='fas fa-trash'></i> Hapus";
                    $btn .= "</button> ";
                }


                return $btn;
            })
            ->rawColumns(['type_number', 'customer', 'selling_price', 'sisa', 'ttl_trnsct_paid', 'actions_link', 'is_publish_html'])
            ->toJson();

    }

    public function formStore()
    {
        $locations = \App\ModelProp\RefLocation::all();
        $types = \App\ModelProp\RefUnitType::all();
        $price_item = \App\ModelProp\RefPriceItem::where('is_active', 'Y')
            ->orderBy('orders')
            ->get();

        $subsidiTypes = RefSubsidiType::where('type', 'SUBSIDI')->get();

        return view('prop.ref_unit.form_store', \compact('locations', 'types', 'price_item', 'subsidiTypes'));
    }

    public function store(Request $request)
    {
        // cek if number unit is exist
        $check_exist_number = $this->checkExistNumber(
            $request->get('location_id'),
            $request->get('block_id'),
            $request->get('number')
        );

        if ($check_exist_number == true) {

            $unit = new \App\ModelProp\RefUnit();
            $unit->location_id = $request->get('location_id');
            $unit->block_id = $request->get('block_id');
            $unit->unittype_id = $request->get('unittype_id');
            $unit->number = $request->get('number');
            $unit->ground_hook = $request->get('ground_hook');
            $unit->uang_muka = \str_replace(".", "", $request->get('uang_muka'));
            $unit->jenis_rumah = $request->get('jenis_rumah');
            $unit->subsidi_type_id = $request->get('subsidi_type_id');
            $unit->nop = $request->get('nop');
            $unit->is_publish = $request->get('is_publish');

            if ($unit->save()) {

                $harga_jual_cash = 0;
                $harga_jual_kpr = 0;

                foreach ($request['price_id_cash'] as $key => $value) {
                    $unit_price = new \App\ModelProp\RefUnitPrice();
                    $unit_price->location_id = $request->get('location_id');
                    $unit_price->block_id = $request->get('block_id');
                    $unit_price->unit_id = $request->get('number');
                    $unit_price->sales_type = 'CASH';
                    $unit_price->price_id = $key;
                    $unit_price->price = str_replace(".", "", $value);
                    $unit_price->save();

                    $harga_jual_cash += str_replace(".", "", $value);
                }

                foreach ($request['price_id_kpr'] as $key => $value) {
                    $unit_price = new \App\ModelProp\RefUnitPrice();
                    $unit_price->location_id = $request->get('location_id');
                    $unit_price->block_id = $request->get('block_id');
                    $unit_price->unit_id = $request->get('number');
                    $unit_price->sales_type = 'KPR';
                    $unit_price->price_id = $key;
                    $unit_price->price = str_replace(".", "", $value);
                    $unit_price->save();

                    $harga_jual_kpr += str_replace(".", "", $value);
                }

                // update harga jual cash dan kpr
                $update_unit = \App\ModelProp\RefUnit::findOrfail($unit->unit_id);
                $update_unit->harga_jual_cash = $harga_jual_cash;
                $update_unit->harga_jual_kpr = $harga_jual_kpr;
                $update_unit->save();

                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan',
                    'id' => $unit->unit_id
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan',
                    'id' => null
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => "Nomor rumah ".$request->get('number')." sudah ada di database"
            ];
        }

        return response()->json($res);
    }

    public function checkExistNumber($location_id, $block_id, $number)
    {
        $data = \App\ModelProp\RefUnit::where("location_id", "$location_id")
                ->where("block_id", "$block_id")
                ->where("number", "$number")
                ->first();

        $res = false;
        if ($data === null) {
            $res = true;
        }

        return $res;
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefUnit::findOrFail($id);
        $price_item = \App\ModelProp\RefUnitPrice::with(['priceItem'])
            ->where('location_id', "$data->location_id")
            ->where('block_id', "$data->block_id")
            ->where('unit_id', "$data->number")
            ->get();

        return view('prop.ref_unit.form_update', \compact('data', 'price_item'));
    }

    public function update(Request $request, $id)
    {
        $unit = \App\ModelProp\RefUnit::findOrFail($id);
        $unit->location_id = $request->get('location_id');
        $unit->block_id = $request->get('block_id');
        $unit->unittype_id = $request->get('unittype_id');
        $unit->ground_hook = $request->get('ground_hook');
        $unit->uang_muka = \str_replace(".", "", $request->get('uang_muka'));
        $unit->jenis_rumah = $request->get('jenis_rumah');
        $unit->subsidi_type_id = $request->get('subsidi_type_id');
        $unit->nop = $request->get('nop');
        $unit->is_publish = $request->get('is_publish');

        if ($unit->save()) {

            $location_id = $request->get('location_id');
            $block_id = $request->get('block_id');
            $number = $unit->number;
            $harga_jual_cash = 0;
            $harga_jual_kpr = 0;

            foreach ($request['price_id_cash'] as $key => $value) {
                $unit_price = \App\ModelProp\RefUnitPrice::findOrFail($key);
                $unit_price->price = str_replace(".", "", $value);
                $unit_price->save();

                $harga_jual_cash += str_replace(".", "", $value);
            }

            foreach ($request['price_id_kpr'] as $key => $value) {
                $unit_price = \App\ModelProp\RefUnitPrice::findOrFail($key);
                $unit_price->price = str_replace(".", "", $value);
                $unit_price->save();

                $harga_jual_kpr += str_replace(".", "", $value);
            }

            // update harga jual cash dan kpr
            $update_unit = \App\ModelProp\RefUnit::findOrfail($unit->unit_id);
            $update_unit->harga_jual_cash = $harga_jual_cash;
            $update_unit->harga_jual_kpr = $harga_jual_kpr;
            $update_unit->save();

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formWorklist($unit_id)
    {
        $worklists = \App\ModelProp\RefWorklistMaster::orderBy('orders', 'ASC')->get();
        $mandors = \App\ModelProp\RefMandor::with(['sdm' => function($query){$query->orderBy('name');}])->get();

        return view('prop.ref_unit.form_worklist', compact('worklists', 'mandors', 'unit_id'));
    }

    public function storeWorklist(Request $request, $unit_id)
    {
        $unit = \App\ModelProp\RefUnit::findOrFail($unit_id);
        $count_success = 0;

        foreach ($request['worklist_id'] as $key => $value) {
            $worklist = new \App\ModelProp\RefWorklist();
            $worklist->worklist_id = $key;
            $worklist->brief = '';
            $worklist->location_id = $unit->location_id;
            $worklist->block_id = $unit->block_id;
            $worklist->unit_id = $unit->number;
            $worklist->mandor_id = $request->get('mandor_id')[$key];
            $worklist->budget = str_replace(".", "", $request->get('budget')[$key]);
            $worklist->total = str_replace(".", "", $request->get('budget')[$key]);
            $worklist->qty = '1';
            $worklist->progress = $request->get('progress')[$key];

            if ($worklist->save()) {
                $count_success += 1;
            }
        }

        if ($count_success > 0) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'id' => $unit_id
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function showpriceByUnitType(Request $request)
    {
        $id = $request->get('id');
        $price_item = \App\ModelProp\RefUnitTypePrice::with(['priceItem'])->where('unittype_id', "$id")->get();

        return view('prop.ref_unit.view_price_by_unit_type', compact('price_item'));
    }

    public function worklistForm($unit_id)
    {
        $unit = \App\ModelProp\RefUnit::findOrFail($unit_id);

        $worklists = \App\ModelProp\RefWorklistMaster::orderBy('orders', 'ASC')->get();
        $mandors = \App\ModelProp\RefMandor::with(['sdm' => function($query){$query->orderBy('name');}])->get();

        $unit_worklist = \App\ModelProp\RefWorklist::with(['mandor.sdm', 'worklist'])
            ->where("location_id", "$unit->location_id")
            ->where("block_id", "$unit->block_id")
            ->where("unit_id", "$unit->number")
            ->get();

        return view('prop.ref_unit.worklist_form', compact('unit', 'worklists', 'unit_worklist', 'mandors', 'unit_id'));
    }

    public function worklistStore(Request $request, $unit_id)
    {
        $unit = \App\ModelProp\RefUnit::findOrFail($unit_id);
        $count_success = 0;

        foreach ($request['worklist_id'] as $key => $value) {
            $worklist = new \App\ModelProp\RefWorklist();
            $worklist->worklist_id = $key;
            $worklist->brief = '';
            $worklist->location_id = $unit->location_id;
            $worklist->block_id = $unit->block_id;
            $worklist->unit_id = $unit->number;
            $worklist->mandor_id = $request->get('mandor_id')[$key];
            $worklist->budget = str_replace(".", "", $request->get('budget')[$key]);
            $worklist->total = str_replace(".", "", $request->get('budget')[$key]);
            $worklist->qty = '1';
            $worklist->progress = $request->get('progress')[$key];

            if ($worklist->save()) {
                $count_success += 1;
            }
        }

        return redirect()->back();

    }

    public function worklistUpdate(Request $request)
    {
        foreach ($request->get('worklist_id') as $key => $value) {
            $worklist = \App\ModelProp\RefWorklist::findOrFail($key);
            $worklist->progress = '100';
            $worklist->save();
        }

        return redirect()->back();
    }

    public function worklistSalaryMandorStore(Request $request)
    {
        $list_id = $request->get('list_id');
        $work = \App\ModelProp\RefWorklist::with(['worklist', 'block'])->findOrFail($list_id);
        $work->st_transfer = 1;
        $work->save();

        $mandor = \App\ModelProp\RefMandor::findOrFail($work->mandor_id);
        $mandor->ttl_salary = $mandor->ttl_salary + $work->total;
        $mandor->save();

        // store to fin_trnsct jadi tabungan mandor
        $finance = new \App\ModelFinance\Trnsct();
        $finance->finance_type = 'PENGELUARAN';
        $finance->finance_date = date('Y-m-d');
        $finance->finance_sdm_id = $work->mandor_id;
        $finance->finance_desc = 'Upah mandor pengerjaan ' . $work->worklist->name . " unit " . $work->unit_id . " blok " . $work->block->name ;
        $finance->finance_amount = $work->total;
        $finance->finance_amount_paid = 0;
        $finance->is_worklist = '1';
        $finance->worklist_id = $list_id;
        $finance->save();

        $finance_item = new \App\ModelFinance\TrnsctItem();
        $finance_item->finance_id = $finance->finance_id;
        $finance_item->finance_item_amount = $work->total;
        $finance_item->finance_item_account_id = '3';
        $finance_item->save();

        return redirect()->back();

    }

    public function worklistSalaryMandorCancel(Request $request)
    {
        $list_id = $request->get('list_id');

        // pembatalan worklist
        $work = \App\ModelProp\RefWorklist::findOrFail($list_id);
        $work->st_transfer = '0';
        $work->save();

        // pembatalan mandor salary
        $mandor = \App\ModelProp\RefMandor::findOrFail($work->mandor_id);
        $mandor->ttl_salary = $mandor->ttl_salary - $work->total;
        $mandor->save();

        // pembatalan finance
        $finance_cost = \App\ModelFinance\Trnsct::where("worklist_id", "$list_id")->where("finance_cancel", "0")->update(["finance_cancel" => "1"]);

        return redirect()->back();
    }

    public function worklistDelete(Request $request)
    {
        $list_id = $request->get('list_id');

        // delete worklist
        $work = \App\ModelProp\RefWorklist::findOrFail($list_id);
        $work->delete();

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $unit_id = $request->get('delete_unit_id');
        $unit = \App\ModelProp\RefUnit::findOrFail($unit_id);
        $unit_number = $unit->number;
        $block_id = $unit->block_id;
        $location_id = $unit->location_id;

        // delete price
        \App\ModelProp\RefUnitPrice::where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('unit_id', "$unit_number")
            ->delete();

        // delete worklist mandor
        \App\ModelProp\RefWorklist::where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('unit_id', "$unit_number")
            ->delete();

        // get sales
        $sales = \App\ModelProp\TrnsctSales::where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('unit_id', "$unit_number")
            ->first();

        if ($sales !== null) {
            // delete sales payment
            \App\ModelProp\TrnsctSalesPaid::where('location_id', "$sales->location_id")
                ->where('block_id', "$sales->block_id")
                ->where('unit_id', "$sales->unit_number")
                ->delete();

            // delete sales return
            \App\ModelProp\TrnsctSalesReturn::where('location_id', "$sales->location_id")
                ->where('block_id', "$sales->block_id")
                ->where('unit_id', "$sales->unit_number")
                ->delete();

            // delete sales
            \App\ModelProp\TrnsctSales::where('location_id', "$location_id")
                ->where('block_id', "$block_id")
                ->where('unit_id', "$unit_number")
                ->delete();

            // delete sales step
            \App\ModelProp\RefUnitSalesStep::where('location_id', "$sales->location_id")
                ->where('block_id', "$sales->block_id")
                ->where('unit_id', "$sales->unit_number")
                ->delete();

            // delete unit certificate step
            \App\ModelProp\RefUnitCertificateStep::where('location_id', "$sales->location_id")
                ->where('block_id', "$sales->block_id")
                ->where('unit_id', "$sales->unit_number")
                ->delete();

            // delete unit certificate position
            \App\ModelProp\RefUnitCertificatePosition::where('location_id', "$sales->location_id")
                ->where('block_id', "$sales->block_id")
                ->where('unit_id', "$sales->unit_number")
                ->delete();
        }

        // delete unit
        $unit->delete();

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil dihapus'
        ];

        return response()->json($res);

    }
}
