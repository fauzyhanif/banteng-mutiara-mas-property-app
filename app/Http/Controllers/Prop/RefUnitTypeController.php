<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefUnitTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_unit_type.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefUnitType::all();

        return DataTables::of($datas)
            ->addColumn('ground', function($datas)
            {
                $text = "$datas->ground_length x $datas->ground_wide = $datas->ground_area";
                return $text;
            })
            ->addColumn('building', function($datas)
            {
                $text = "$datas->building_length x $datas->building_wide = $datas->building_area";
                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.unit_type.form_update', $datas->unittype_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['ground', 'building', 'actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        $price_item = \App\ModelProp\RefPriceItem::where('is_active', 'Y')
            ->orderBy('orders')
            ->get();

        return view('prop.ref_unit_type.form_store', \compact('price_item'));
    }

    public function store(Request $request)
    {
        $unit_type = new \App\ModelProp\RefUnitType();
        $unit_type->name = $request->get('name');
        $unit_type->ground_area = $request->get('ground_area');
        $unit_type->ground_length = $request->get('ground_length');
        $unit_type->ground_wide = $request->get('ground_wide');
        $unit_type->building_area = $request->get('building_area');
        $unit_type->building_length = $request->get('building_length');
        $unit_type->building_wide = $request->get('building_wide');
        $unit_type->marketing_fee_agent = str_replace(".", "", $request->get('marketing_fee_agent'));
        $unit_type->marketing_fee_downline = str_replace(".", "", $request->get('marketing_fee_downline'));

        if ($unit_type->save()) {
            foreach ($request->get('price_id_cash') as $key => $value) {
                $price = new \App\ModelProp\RefUnitTypePrice();
                $price->unittype_id = $unit_type->unittype_id;
                $price->sales_type = 'CASH';
                $price->price_id = $key;
                $price->price = str_replace(".", "", $value);
                $price->save();
            }

            foreach ($request->get('price_id_kpr') as $key => $value) {
                $price = new \App\ModelProp\RefUnitTypePrice();
                $price->unittype_id = $unit_type->unittype_id;
                $price->sales_type = 'KPR';
                $price->price_id = $key;
                $price->price = str_replace(".", "", $value);
                $price->save();
            }

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefUnitType::findOrFail($id);
        $price_item = \App\ModelProp\RefPriceItem::where('is_active', 'Y')
            ->orderBy('orders')
            ->get();

        $unittype_price = \App\ModelProp\RefUnitTypePrice::where("unittype_id", "$id")->get();
        $res_unittype_price = [];
        foreach ($unittype_price as $key => $value) {
            $res_unittype_price[$value->sales_type."|".$value->price_id] = $value->price;
        }

        return view('prop.ref_unit_type.form_update', \compact('data', 'price_item', 'res_unittype_price'));
    }

    public function update(Request $request, $id)
    {
        $unit_type = \App\ModelProp\RefUnitType::findOrFail($id);
        $unit_type->name = $request->get('name');
        $unit_type->ground_area = $request->get('ground_area');
        $unit_type->ground_length = $request->get('ground_length');
        $unit_type->ground_wide = $request->get('ground_wide');
        $unit_type->building_area = $request->get('building_area');
        $unit_type->building_length = $request->get('building_length');
        $unit_type->building_wide = $request->get('building_wide');
        $unit_type->marketing_fee_agent = str_replace(".", "", $request->get('marketing_fee_agent'));
        $unit_type->marketing_fee_downline = str_replace(".", "", $request->get('marketing_fee_downline'));

        if ($unit_type->save()) {
            foreach ($request->get('price_id_cash') as $key => $value) {
                $price = \App\ModelProp\RefUnitTypePrice::where("unittype_id", "$id")
                    ->where("sales_type", "CASH")
                    ->where("price_id", "$key")
                    ->get();

                if ($price->count() == 0) {
                    $price = new \App\ModelProp\RefUnitTypePrice();
                    $price->unittype_id = $unit_type->unittype_id;
                    $price->sales_type = 'CASH';
                    $price->price_id = $key;
                    $price->price = str_replace(".", "", $value);
                    $price->save();
                } else {
                    $price = \App\ModelProp\RefUnitTypePrice::where("unittype_id", "$id")
                        ->where("sales_type", "CASH")
                        ->where("price_id", "$key")
                        ->update(["price" => str_replace(".", "", $value)]);
                }
            }

            foreach ($request->get('price_id_kpr') as $key => $value) {
                $price = \App\ModelProp\RefUnitTypePrice::where("unittype_id", "$id")
                    ->where("sales_type", "KPR")
                    ->where("price_id", "$key")
                    ->get();

                if ($price->count() == 0) {
                    $price = new \App\ModelProp\RefUnitTypePrice();
                    $price->unittype_id = $unit_type->unittype_id;
                    $price->sales_type = 'KPR';
                    $price->price_id = $key;
                    $price->price = str_replace(".", "", $value);
                    $price->save();
                } else {
                    $price = \App\ModelProp\RefUnitTypePrice::where("unittype_id", "$id")
                        ->where("sales_type", "KPR")
                        ->where("price_id", "$key")
                        ->update(["price" => str_replace(".", "", $value)]);
                }
            }

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
