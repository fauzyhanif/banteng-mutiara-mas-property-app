<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefMarketerTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_marketer_type.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefMarketerType::orderBy('name')->get();

        return DataTables::of($datas)
            ->addColumn('html_subsidi_booking_fee', function($datas){
                return number_format($datas->subsidi_booking_fee,0,',','.');
            })
            ->addColumn('html_subsidi_akad_fee', function($datas){
                return number_format($datas->subsidi_akad_fee,0,',','.');
            })
            ->addColumn('html_nonsubsidi_booking_fee', function($datas){
                return number_format($datas->nonsubsidi_booking_fee,0,',','.');
            })
            ->addColumn('html_nonsubsidi_akad_fee', function($datas){
                return number_format($datas->nonsubsidi_akad_fee,0,',','.');
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.marketer_type.form_update', $datas->type_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'html_subsidi_booking_fee', 'html_subsidi_akad_fee', 'html_nonsubsidi_booking_fee', 'html_nonsubsidi_akad_fee'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_marketer_type.form_store');
    }

    public function store(Request $request)
    {
        $new = new \App\ModelProp\RefMarketerType();
        $new->name = $request->get('name');
        $new->subsidi_booking_fee = str_replace(".","",$request->get('subsidi_booking_fee'));
        $new->subsidi_akad_fee = str_replace(".","",$request->get('subsidi_akad_fee'));
        $new->nonsubsidi_booking_fee = str_replace(".","",$request->get('nonsubsidi_booking_fee'));
        $new->nonsubsidi_akad_fee = str_replace(".","",$request->get('nonsubsidi_akad_fee'));

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefMarketerType::findOrFail($id);

        return view('prop.ref_marketer_type.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $new = \App\ModelProp\RefMarketerType::findOrFail($id);
        $new->name = $request->get('name');
        $new->subsidi_booking_fee = str_replace(".","",$request->get('subsidi_booking_fee'));
        $new->subsidi_akad_fee = str_replace(".","",$request->get('subsidi_akad_fee'));
        $new->nonsubsidi_booking_fee = str_replace(".","",$request->get('nonsubsidi_booking_fee'));
        $new->nonsubsidi_akad_fee = str_replace(".","",$request->get('nonsubsidi_akad_fee'));
        $new->is_active = $request->get('is_active');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
