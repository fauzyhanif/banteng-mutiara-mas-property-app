<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefWorklistMasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_worklist_master.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefWorklistMaster::with(['sdm'])->orderBy('orders', 'ASC')->get();

        return DataTables::of($datas)
            ->editColumn('price', function($datas){
                return number_format($datas->price,2,',','.');
            })
            ->addColumn('mandor', function($datas){
                $text = "";
                if ($datas->sdm_id != null) {
                    $text = $datas->sdm->name;
                }
                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.worklist_master.form_update', $datas->id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'mandor'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_worklist_master.form_store');
    }

    public function store(Request $request)
    {
        $work = new \App\ModelProp\RefWorklistMaster();
        $work->name = $request->get('name');
        $work->orders = $request->get('orders');
        $work->sdm_id = $request->get('sdm_id');
        $work->price = str_replace(".", "", $request->get('price'));

        if ($work->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefWorklistMaster::findOrFail($id);

        return view('prop.ref_worklist_master.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $work = \App\ModelProp\RefWorklistMaster::findOrFail($id);
        $work->name = $request->get('name');
        $work->orders = $request->get('orders');
        $work->sdm_id = $request->get('sdm_id');
        $work->price = str_replace(".", "", $request->get('price'));

        if ($work->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function getSalary(Request $request)
    {
        $id = $request->get('worklist_id');
        $worklist = \App\ModelProp\RefWorklistMaster::findOrFail($id);

        return response()->json($worklist->price);
    }
}
