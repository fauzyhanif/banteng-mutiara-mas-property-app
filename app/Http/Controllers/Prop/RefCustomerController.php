<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_customer.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_customer')
            ->leftJoin('prop_ref_sdm', 'prop_ref_customer.sdm_id', '=', 'prop_ref_sdm.sdm_id')
            ->select('prop_ref_sdm.sdm_id', 'prop_ref_sdm.name', 'prop_ref_sdm.nik', 'prop_ref_sdm.phone_num',
                    'prop_ref_sdm.couple', 'prop_ref_sdm.date_ofbirth', 'prop_ref_sdm.address_domicile', 'prop_ref_sdm.institute')
            ->where('prop_ref_sdm.is_active', 'Y')
            ->orderBy('prop_ref_sdm.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('name_nik', function($datas){
                $text = $datas->name;
                $text .= "<br><span class='text-muted'>$datas->nik</span>";

                return $text;
            })
            ->addColumn('phone_address', function($datas){
                $text = $datas->phone_num;
                $text .= "<br>$datas->address_domicile";

                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.customer.form_update', $datas->sdm_id) . "' class='btn btn-primary btn-sm btn-block'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                // cetak link
                $btn .= "<a href='" . route('prop.customer.print', $datas->sdm_id) . "' class='btn btn-secondary btn-sm btn-block' target='_blank'>";
                $btn .= "<i class='fas fa-print'></i> Cetak Data";
                $btn .= "</a> ";

                // cetak link
                $btn .= "<a href='" . route('prop.print_kpr_requirements', $datas->sdm_id) . "' class='btn bg-purple btn-sm btn-block'>";
                $btn .= "<i class='fas fa-print'></i> Syarat KPR";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['phone_address', 'name_nik', 'actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_customer.form_store');
    }

    public function store(Request $request)
    {
        // jika exist user maka store ke customer dan update sdm
        if ($request->get('is_exist') == '1') {
            $sdm = \App\ModelProp\RefSdm::findOrFail($request->get('sdm_id'));
        } else {
            $sdm = new \App\ModelProp\RefSdm();
        }

        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->nik = $request->get('nik');
        $sdm->place_of_birth = $request->get('place_of_birth');
        $sdm->date_ofbirth = $request->get('date_ofbirth');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->email = $request->get('email');
        $sdm->address = $request->get('address');
        $sdm->address_domicile = $request->get('address_domicile');
        $sdm->job = $request->get('job');
        $sdm->institute = $request->get('institute');
        $sdm->income = str_replace(".", "", $request->get('income'));
        $sdm->npwp = $request->get('npwp');
        $sdm->is_married = $request->get('is_married');

        if ($request->get('is_married') == 'Y') {
            $sdm->couple = $request->get('couple');
            $sdm->couple_place_of_birth = $request->get('couple_place_of_birth');
            $sdm->couple_date_of_birth = $request->get('couple_date_of_birth');
            $sdm->couple_job = $request->get('couple_job');
            $sdm->couple_address_domicile = $request->get('couple_address_domicile');
            $sdm->couple_nik = $request->get('couple_nik');
        }

        if ($sdm->save()) {
            $cust = new \App\ModelProp\RefCustomer();
            $cust->sdm_id = $sdm->sdm_id;
            if ($cust->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefSdm::findOrFail($id);

        return view('prop.ref_customer.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->nik = $request->get('nik');
        $sdm->place_of_birth = $request->get('place_of_birth');
        $sdm->date_ofbirth = $request->get('date_ofbirth');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->email = $request->get('email');
        $sdm->address = $request->get('address');
        $sdm->address_domicile = $request->get('address_domicile');
        $sdm->job = $request->get('job');
        $sdm->institute = $request->get('institute');
        $sdm->income = str_replace(".", "", $request->get('income'));
        $sdm->npwp = $request->get('npwp');
        $sdm->rek_bank_new = $request->get('rek_bank_new');
        $sdm->rek_number_new = $request->get('rek_number_new');
        $sdm->is_married = $request->get('is_married');
        $sdm->is_active = $request->get('is_active');

        if ($request->get('is_married') == 'Y') {
            $sdm->couple = $request->get('couple');
            $sdm->couple_place_of_birth = $request->get('couple_place_of_birth');
            $sdm->couple_date_of_birth = $request->get('couple_date_of_birth');
            $sdm->couple_job = $request->get('couple_job');
            $sdm->couple_address_domicile = $request->get('couple_address_domicile');
            $sdm->couple_nik = $request->get('couple_nik');
        }

        $sdm->save();

        if ($sdm->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function updateFromSalesPage(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->nik = $request->get('nik');
        $sdm->place_of_birth = $request->get('place_of_birth');
        $sdm->date_ofbirth = $request->get('date_ofbirth');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->email = $request->get('email');
        $sdm->address = $request->get('address');
        $sdm->address_domicile = $request->get('address_domicile');
        $sdm->job = $request->get('job');
        $sdm->institute = $request->get('institute');
        $sdm->income = str_replace(".", "", $request->get('income'));
        $sdm->npwp = $request->get('npwp');
        $sdm->rek_bank_new = $request->get('rek_bank_new');
        $sdm->rek_number_new = $request->get('rek_number_new');
        $sdm->is_married = $request->get('is_married');
        $sdm->is_active = $request->get('is_active');

        if ($request->get('is_married') == 'Y') {
            $sdm->couple = $request->get('couple');
            $sdm->couple_place_of_birth = $request->get('couple_place_of_birth');
            $sdm->couple_date_of_birth = $request->get('couple_date_of_birth');
            $sdm->couple_job = $request->get('couple_job');
            $sdm->couple_address_domicile = $request->get('couple_address_domicile');
            $sdm->couple_nik = $request->get('couple_nik');
        }

        $sdm->save();

        return redirect()->back();
    }

    public function print($id)
    {
        $data = \App\ModelProp\RefSdm::findOrFail($id);

        return view('prop.ref_customer.print', \compact('data'));
    }
}
