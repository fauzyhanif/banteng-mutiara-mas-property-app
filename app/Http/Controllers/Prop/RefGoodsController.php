<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefGoodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_goods.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefGoods::where('is_active', 'Y')->orderBy('name')->get();

        return DataTables::of($datas)
            ->editColumn('stock', function($datas)
            {
                return $datas->stock . " " . $datas->unit;
            })
            ->editColumn('price', function($datas)
            {
                return number_format($datas->price,2,',','.');
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.goods.form_update', $datas->goods_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_goods.form_store');
    }

    public function store(Request $request)
    {
        $new = new \App\ModelProp\RefGoods();
        $new->name = $request->get('name');
        $new->stock = $request->get('stock');
        $new->price = str_replace('.', '', $request->get('price'));
        $new->unit = $request->get('unit');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefGoods::findOrFail($id);

        return view('prop.ref_goods.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $new = \App\ModelProp\RefGoods::findOrFail($id);
        $new->name = $request->get('name');
        $new->stock = $request->get('stock');
        $new->price = str_replace('.', '', $request->get('price'));
        $new->unit = $request->get('unit');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
