<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefBankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_bank.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_bank')
            ->leftJoin('prop_ref_sdm', 'prop_ref_bank.sdm_id', '=', 'prop_ref_sdm.sdm_id')
            ->select('prop_ref_sdm.sdm_id', 'prop_ref_sdm.name', 'prop_ref_sdm.phone_num', 'prop_ref_sdm.address')
            ->orderBy('prop_ref_sdm.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.bank.form_update', $datas->sdm_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_bank.form_store');
    }

    public function store(Request $request)
    {
        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');

        if ($sdm->save()) {
            $cust = new \App\ModelProp\RefBank();
            $cust->sdm_id = $sdm->sdm_id;
            if ($cust->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefSdm::findOrFail($id);

        return view('prop.ref_bank.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->save();

        if ($sdm->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
