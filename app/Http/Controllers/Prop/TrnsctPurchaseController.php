<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class TrnsctPurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.trnsct_purchase.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\TrnsctPurchase::with(['location', 'block', 'store'])
            ->orderBy('purchase_date', 'DESC')
            ->get();

        return DataTables::of($datas)
            ->editColumn('purchase_date', function($datas)
            {
                $text = date('d/m/Y', strtotime($datas->purchase_date));
                return $text;
            })
            ->addColumn('status_acc', function($datas){
                $text = "<span class='text-secondary'>Belum ditanggapi</span>";

                if ($datas->status == 1) {
                    $text = "<span class='text-green'>Sudah ditanggapi</span>";
                }

                return $text;
            })
            ->addColumn('location', function($datas)
            {
                $text = "";
                if ($datas->location_id != "") {
                    $text = $datas->location->name . " Blok " . $datas->block->name;
                }
                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.purchase.form_update', $datas->purchase_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i>";
                $btn .= "</a> ";

                // detail link
                $btn .= "<a href='" . route('prop.purchase.detail', $datas->purchase_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i>";
                $btn .= "</a> ";

                // respon link
                $btn .= "<a href='" . route('prop.purchase.respon', $datas->purchase_id) . "' class='btn btn-warning btn-xs'>";
                $btn .= "<i class='fas fa-check'></i>";
                $btn .= "</a> ";

                // print link
                $btn .= "<a href='" . route('prop.purchase.print', $datas->purchase_id) . "' target='_blank' class='btn bg-purple btn-xs'>";
                $btn .= "<i class='fas fa-print'></i>";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['location', 'actions_link', 'status_acc'])
            ->toJson();
    }

    public function detail($id)
    {
        $purchase = \App\ModelProp\TrnsctPurchase::where('purchase_id', $id)
            ->with(['location', 'block'])
            ->orderBy('purchase_date', 'DESC')
            ->first();

        $purchase_item = \App\ModelProp\TrnsctPurchaseItem::where('purchase_id', $id)->orderBy('goods_id', 'ASC')->get();

        return view('prop.trnsct_purchase.detail', compact('purchase', 'purchase_item'));
    }

    public function formStore()
    {
        $stores = \App\ModelProp\RefStore::with(['sdm' => function ($query) {
            $query->orderBy('name', 'asc');
        }])->get();
        return view('prop.trnsct_purchase.form_store', compact('stores'));
    }

    public function formStoreItem($purchase_id, $store_id)
    {

        return view('prop.trnsct_purchase.form_store_item', compact('purchase_id', 'store_id'));
    }

    public function store(Request $request)
    {
        $purchase = new \App\ModelProp\TrnsctPurchase();
        $purchase->purchase_date = $request->get('purchase_date');
        $purchase->location_id = $request->get('location_id');
        $purchase->block_id = $request->get('block_id');
        $purchase->store_id = $request->get('store_id');
        $purchase->note = $request->get('note');
        $purchase->termin = $request->get('termin');
        $purchase->shipping_cost = $request->get('shipping_cost');
        $purchase->payment_method = $request->get('payment_method');
        $purchase->admin = Session::get('auth_nama');

        if ($purchase->save()) {
            return redirect('/prop/purchase/form_store_item/'.$purchase->purchase_id.'/'.$purchase->store_id);
        } else {
            return back()->with('status', 'Data gagal disimpan!');
        }
    }

    public function storeItem(Request $request)
    {
        $purchase = new \App\ModelProp\TrnsctPurchaseItem();
        $purchase->purchase_id = $request->get('purchase_id');
        $purchase->goods_id = $request->get('goods_id');
        $purchase->qty = $request->get('qty');

        if ($purchase->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function viewListItem($id)
    {
        $datas = \App\ModelProp\TrnsctPurchaseItem::with(['goods'])
            ->where('purchase_id', $id)
            ->orderBy('goods_id', 'ASC')
            ->get();

        return view('prop.trnsct_purchase.view_list_item', \compact('datas'));
    }

    public function deleteItem(Request $request)
    {
        $id = $request->get('purcitem_id');
        $data = \App\ModelProp\TrnsctPurchaseItem::findOrFail($id);
        if ($data->delete()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil dihapus'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal dihapus'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\TrnsctPurchase::findOrFail($id);
        $stores = \App\ModelProp\RefStore::with(['sdm' => function ($query) {
            $query->orderBy('name', 'asc');
        }])->get();

        return view('prop.trnsct_purchase.form_update', \compact('data', 'stores'));
    }

    public function update(Request $request, $id)
    {
        $purchase = \App\ModelProp\TrnsctPurchase::findOrFail($id);
        $purchase->purchase_date = $request->get('purchase_date');
        $purchase->location_id = $request->get('location_id');
        $purchase->block_id = $request->get('block_id');
        $purchase->store_id = $request->get('store_id');
        $purchase->note = $request->get('note');
        $purchase->termin = $request->get('termin');
        $purchase->shipping_cost = $request->get('shipping_cost');
        $purchase->payment_method = $request->get('payment_method');
        $purchase->status = $request->get('status');

        if ($purchase->save()) {
            return redirect('/prop/purchase/form_store_item/'.$id.'/'.$purchase->store_id);
        } else {
            return back()->with('status', 'Data gagal disimpan!');
        }
    }

    public function respon($id)
    {
        $purchase = \App\ModelProp\TrnsctPurchase::where('purchase_id', $id)
            ->with(['location', 'block'])
            ->orderBy('purchase_date', 'DESC')
            ->first();

        $purchase_item = \App\ModelProp\TrnsctPurchaseItem::where('purchase_id', $id)
            ->orderBy('goods_id', 'ASC')
            ->get();

        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();
        $accounts = \App\ModelFinance\RefAccount::where('account_type', 'PENGELUARAN')->orderBy('account_name')->get();
        $costs = \App\ModelFinance\Trnsct::where('is_purchase', '1')->where('purchase_id', "$id")->where('finance_cancel', '0')->get();

        return view('prop.trnsct_purchase.respon', compact('purchase', 'purchase_item', 'cashes', 'accounts', 'costs'));
    }

    public function responStore(Request $request, $id)
    {
        // store goods info
        $goods = new \App\ModelProp\TrnsctGoods();
        $goods->type = 'IN';
        $goods->trnsct_date = $request->get('realization_date');
        $goods->buy_from = '0';
        $goods->note = $request->get('realization_note');
        $goods->admin = Session::get('auth_nama');

        if ($goods->save()) {

            // store goods item
            foreach ($request->get('realization') as $key => $value) {
                $goods_item = new \App\ModelProp\TrnsctGoodsItem();
                $goods_item->trnsct_id = $goods->trnsct_id;
                $goods_item->goods_id = $key;
                $goods_item->type = 'IN';
                $goods_item->qty = $value;
                $goods_item->save();

                // update purchase item realization_qty
                $purchase_item = \App\ModelProp\TrnsctPurchaseItem::where('purchase_id', '=', "$id")->where("goods_id", "$key")->first();

                $purchase_item->qty_realization = $purchase_item->qty_realization + $value;
                $purchase_item->save();
            }
        }

        return redirect()->back();
    }

    public function viewListItemReponPage($id)
    {
        $datas = \App\ModelProp\TrnsctPurchaseItem::with(['goods'])
            ->where('purchase_id', $id)
            ->orderBy('goods_id', 'ASC')
            ->get();

        return view('prop.trnsct_purchase.view_list_item_respon_page', \compact('datas'));
    }

    public function changeStatus(Request $request, $id)
    {
        $purchase = \App\ModelProp\TrnsctPurchase::findOrFail($id);
        $purchase->status = $request->get('status');

        if ($purchase->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function print($purchase_id)
    {
        $purchase = \App\ModelProp\TrnsctPurchase::with(['store', 'location'])->findOrFail($purchase_id);
        $purchase_items = \App\ModelProp\TrnsctPurchaseItem::with('goods')->where('purchase_id', $purchase_id)->get();

        return view('prop.trnsct_purchase.print', \compact('purchase', 'purchase_items'));

    }

    public function storeCost(Request $request)
    {
        $num = $this->invoiceNumber('COST');

        // store to fin_trnsct
        $finance = new \App\ModelFinance\Trnsct();
        $finance->finance_type = 'PENGELUARAN';
        $finance->finance_date = $request->get('finance_date');
        $finance->finance_sdm_id = $request->get('finance_sdm_id');
        $finance->finance_desc = $request->get('finance_desc');
        $finance->finance_amount = str_replace(".", "", $request->get('finance_amount'));
        $finance->finance_amount_paid = str_replace(".", "", $request->get('finance_amount_paid'));
        $finance->is_purchase = '1';
        $finance->purchase_id = $request->get('purchase_id');

        // store to fin_trnsct_item
        if ($finance->save()) {
            for ($i=0; $i < count($request->get('finance_item_account_id')); $i++) {
                $finance_item = new \App\ModelFinance\TrnsctItem();
                $finance_item->finance_id = $finance->finance_id;
                $finance_item->finance_item_account_id = str_replace(".", "", $request->get('finance_item_account_id')[$i]);
                $finance_item->finance_item_amount = str_replace(".", "", $request->get('finance_item_amount')[$i]);
                $finance_item->save();
            }

            // store to fin_trnsct_paid jika finance_amount_paid > 0
            if (str_replace(".", "", $request->get('finance_amount_paid')) > 0) {
                $finance_paid = new \App\ModelFinance\TrnsctPaid();
                $finance_paid->finance_id = $finance->finance_id;
                $finance_paid->finance_paid_num = $num;
                $finance_paid->finance_paid_date = $request->get('finance_date');
                $finance_paid->finance_paid_cash_id = $request->get('finance_paid_cash_id');
                $finance_paid->finance_paid_amount = str_replace(".", "", $request->get('finance_amount_paid'));
                $finance_paid->finance_paid_sdm_id = $request->get('finance_sdm_id');
                $finance_paid->save();
            }
        }

        return redirect()->back();
    }

    public function invoiceNumber($type)
    {
        $this_month = date('Y-m');
        $booking_id = 'INV/'.$type.'/';

        $last_invoice = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
            ->where('type', "$type")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_invoice) == 0 ) {
            $order_id = "0001";

            $new = new \App\ModelFinance\RefInvoiceNumber();
            $new->type = $type;
            $new->month = "$this_month";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $order_id = sprintf('%04d', $last_invoice[0]->serial_num);
            $update = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
                ->where("type", "$type")
                ->update(["serial_num" => $order_id + 1]);
        }

        $booking_id .= date('Y/m/') . $order_id;
        return $booking_id;
    }
}
