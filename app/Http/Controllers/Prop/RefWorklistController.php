<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefWorklistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $status = ($request->has('status')) ? $request->input('status') : 1;
        return view('prop.ref_worklist.index', compact( 'status'));
    }

    public function listData(Request $request)
    {
        $param = "";
        $value = "";

        $status = $request->get('status');

        if ($status == 1) {
            $param = "=";
            $value = "0";
        } elseif ($status == 3) {
            $param = "=";
            $value = "100";
        }

        $datas = \App\ModelProp\RefWorklist::with(['unit','location', 'block', 'sdm', 'worklist'])->where("progress", "$param", "$value")->get();

        if ($status == '2') {
            $datas = \App\ModelProp\RefWorklist::with(['unit','location', 'block', 'sdm', 'worklist'])->whereBetween("progress", [1,99])->get();
        }

        return DataTables::of($datas)
            ->addColumn('worklist', function($datas){
                $text = $datas->worklist->name;
                return $text;
            })
            ->addColumn('mandor', function($datas){
                $text = $datas->sdm->name;
                return $text;
            })
            ->addColumn('location', function($datas){
                $text = $datas->brief;
                if ($datas->location_id != '' && $datas->block_id != '' && $datas->unit_id != '') {
                    $text = "Nomor Unit <b>$datas->unit_id</b> ";
                    $text .= "Blok " . $datas->block->name;
                    $text .= " " . $datas->location->name;
                }

                return $text;
            })
            ->addColumn('progress_bar', function($datas){
                $progress = $datas->progress;
                $text = "<div class='progress-group'>";
                $text .= "<b>$progress%</b>";
                $text .= "<div class='progress progress-sm'><div class='progress-bar bg-primary' style='width: ".$progress."%'></div>";
                $text .= "</div></div>";

                return $text;
            })
            ->addColumn('status_cair', function($datas){
                $text = "<span class='text-green'>Sudah masuk kas</span>";
                if ($datas->st_transfer == 0) {
                    $text = "<span class='text-red'>Belum masuk kas</span>";
                }

                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.work_list.form_update', $datas->list_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                // detail link
                $btn .= "<a href='javascript:void(0)' class='btn btn-info btn-xs' onclick=\"detail('$datas->list_id')\">";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'location', 'progress_bar', 'status_cair', 'worklist', 'mandor'])
            ->toJson();
    }

    public function listDataByUnit(Request $request)
    {
        $unit_id = $request->get('unit_id');
        $block_id = $request->get('block_id');
        $location_id = $request->get('location_id');

        $datas = \App\ModelProp\RefWorklist::with(['mandor.sdm', 'worklist'])
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("unit_id", "$unit_id")
            ->get();

        return DataTables::of($datas)
             ->addColumn('worklists', function($datas){
                $text = $datas->worklist->name;
                return $text;
            })
            ->addColumn('mandor', function($datas){
                $text = $datas->mandor->sdm->name;
                return $text;
            })
            ->editColumn('progress', function($datas){
                return $datas->progress . "%";
            })
            ->rawColumns(['worklists', 'mandor'])
            ->toJson();
    }

    public function formStore()
    {
        $worklists = \App\ModelProp\RefWorklistMaster::orderBy('orders', 'ASC')->get();
        return view('prop.ref_worklist.form_store', compact('worklists'));
    }

    public function store(Request $request)
    {
        $worklist = new \App\ModelProp\RefWorklist();
        $worklist->worklist_id = str_replace(" ", "", explode("|", $request->get('worklist_id'))[0]);
        $worklist->brief = $request->get('brief');
        $worklist->location_id = $request->get('location_id');
        $worklist->block_id = $request->get('block_id');
        $worklist->mandor_id = $request->get('mandor_id');
        $worklist->unit_id = $request->get('unit_id');
        $worklist->budget = str_replace(".", "", $request->get('budget'));
        $worklist->qty = $request->get('qty');
        $worklist->total = str_replace(".", "", $request->get('total'));
        $worklist->progress = $request->get('progress');
        $worklist->date_start = $request->get('date_start');
        $worklist->date_end = $request->get('date_end');

        if ($request->has('unit_id')) {
        }

        if ($worklist->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefWorklist::where('list_id', $id)->with(['unit', 'unit.location', 'unit.block'])->first();
        $worklists = \App\ModelProp\RefWorklistMaster::orderBy('orders', 'ASC')->get();

        return view('prop.ref_worklist.form_update', \compact('data', 'units', 'worklists'));
    }

    public function update(Request $request, $id)
    {
        $worklist = \App\ModelProp\RefWorklist::findOrFail($id);
        $worklist->worklist_id = str_replace(" ", "", explode("|", $request->get('worklist_id'))[0]);
        $worklist->brief = $request->get('brief');
        $worklist->mandor_id = $request->get('mandor_id');
        $worklist->budget = str_replace(".", "", $request->get('budget'));
        $worklist->qty = $request->get('qty');
        $worklist->total = str_replace(".", "", $request->get('total'));
        $worklist->progress = $request->get('progress');
        $worklist->date_start = $request->get('date_start');
        $worklist->date_end = $request->get('date_end');

        if ($worklist->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function detail(Request $request)
    {
        $list_id = $request->get('list_id');
        $data = DB::table('prop_ref_worklist as a')
            ->leftJoin('prop_ref_location as b', 'a.location_id', '=', 'b.location_id')
            ->leftJoin('prop_ref_block as c', 'a.block_id', '=', 'c.block_id')
            ->leftJoin('prop_ref_sdm as d', 'a.mandor_id', '=', 'd.sdm_id')
            ->leftJoin('prop_ref_unit as e', 'a.unit_id', '=', 'e.number')
            ->leftJoin('prop_ref_worklist_master as f', 'a.worklist_id', '=', 'f.id')
            ->select('a.*', 'b.name as location_name', 'c.name as block_name', 'd.name as mandor_name', 'e.number', 'f.name as worklist_name')
            ->where("a.list_id", "$list_id")
            ->orderBy('a.list_id', 'DESC')
            ->get();

        $detail = $data[0];

        // cek keuangan apakah sudah dicairkan atau belum
        $finance_amount_paid = 0;
        $finance_cost = \App\ModelFinance\Trnsct::where("worklist_id", "$list_id")->where("finance_cancel", "0")->first();

        if ($finance_cost) {
            $finance_amount_paid = $finance_cost->finance_amount_paid;
        }

        return view('prop.ref_worklist.detail_view', \compact('detail', 'finance_amount_paid'));
    }

    public function saveSalaryMandor(Request $request)
    {
        $list_id = $request->get('list_id');
        $ttl_salary = $request->get('ttl_salary');
        $mandor_id = $request->get('mandor_id');

        $mandor = \App\ModelProp\RefMandor::findOrFail($mandor_id);
        $mandor->ttl_salary = $mandor->ttl_salary + $ttl_salary;
        if ($mandor->save()) {
            $work = \App\ModelProp\RefWorklist::with('worklist')->findOrFail($list_id);
            $work->st_transfer = 1;

            // store to fin_trnsct jadi tabungan mandor
            $finance = new \App\ModelFinance\Trnsct();
            $finance->finance_type = 'PENGELUARAN';
            $finance->finance_date = date('Y-m-d');
            $finance->finance_sdm_id = $mandor_id;
            $finance->finance_desc = 'Upah mandor pengerjaan ' . $work->worklist->name . " unit " . $work->unit_id . " blok " . $work->block->name ;
            $finance->finance_amount = $ttl_salary;
            $finance->finance_amount_paid = 0;
            $finance->is_worklist = 1;
            $finance->worklist_id = $list_id;

            if ($finance->save()) {
                $finance_item = new \App\ModelFinance\TrnsctItem();
                $finance_item->finance_id = $finance->finance_id;
                $finance_item->finance_item_amount = $ttl_salary;
                $finance_item->finance_item_account_id = '3';
                $finance_item->save();
            }

            if ($work->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Budget gagal dicairkan'
            ];
        }

        return response()->json($res);
    }

    public function cancelSalaryMandor(Request $request)
    {
        $list_id = $request->get('list_id');
        $ttl_salary = $request->get('ttl_salary');
        $mandor_id = $request->get('mandor_id');

        // pembatalan worklist
        $work = \App\ModelProp\RefWorklist::with('worklist')->findOrFail($list_id);
        $work->st_transfer = 0;
        $work->save();

        // pembatalan mandor salary
        $mandor = \App\ModelProp\RefMandor::findOrFail($mandor_id);
        $mandor->ttl_salary = $mandor->ttl_salary - $ttl_salary;
        $mandor->save();

        // pembatalan finance
        $finance_cost = \App\ModelFinance\Trnsct::where("worklist_id", "$list_id")->where("finance_cancel", "0")->update(["finance_cancel" => "1"]);

        $res = [
            'status' => 'success',
            'text' => 'Data berhasil dibatalkan'
        ];

        return response()->json($res);
    }
}
