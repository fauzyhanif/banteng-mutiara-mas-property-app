<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefMandorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_mandor.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_mandor')
            ->leftJoin('prop_ref_sdm', 'prop_ref_mandor.sdm_id', '=', 'prop_ref_sdm.sdm_id')
            ->select('prop_ref_sdm.sdm_id', 'prop_ref_sdm.name', 'prop_ref_mandor.specialist', 'prop_ref_sdm.phone_num',
                    'prop_ref_sdm.address', 'prop_ref_sdm.rek_bank', 'prop_ref_sdm.rek_number', 'prop_ref_sdm.rek_name',
                    'prop_ref_mandor.price')
            ->orderBy('prop_ref_sdm.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('phone_address', function($datas) {
                $text = $datas->phone_num."<br>".$datas->address;

                return $text;
            })
            ->addColumn('rekening', function($datas) {
                $text = $datas->rek_bank." ".$datas->rek_number."<br>".$datas->rek_name;

                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.mandor.form_update', $datas->sdm_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'phone_address', 'rekening'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_mandor.form_store');
    }

    public function store(Request $request)
    {
        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');
        $sdm->note = $request->get('note');

        if ($sdm->save()) {
            $mandor = new \App\ModelProp\RefMandor();
            $mandor->sdm_id = $sdm->sdm_id;
            $mandor->specialist = $request->get('specialist');

            if ($mandor->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefSdm::where('sdm_id', $id)->with(['mandor'])->first();

        return view('prop.ref_mandor.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');
        $sdm->note = $request->get('note');

        $sdm->save();

        if ($sdm->save()) {
            $mandor = \App\ModelProp\RefMandor::findOrFail($id);
            $mandor->specialist = $request->get('specialist');

             if ($mandor->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
