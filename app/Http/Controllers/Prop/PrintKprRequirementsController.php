<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class PrintKprRequirementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($customer_id)
    {
        $customer = \App\ModelProp\RefSdm::findOrFail($customer_id);
        $sales = \App\ModelProp\TrnsctSales::with(["location", "block", "unit.unitType"])
            ->where("customer_id", $customer_id)
            ->where("status", "!=", "CANCEL")
            ->orderBy("block_id", "ASC")
            ->get();

        return view('prop.print_kpr_requirements.index', compact('customer', 'sales'));
    }

    public function print($customer_id, $lampiran_id, $sales_id = '')
    {
        $customer = \App\ModelProp\RefSdm::findOrFail($customer_id);

        $sale = [];
        if ($sales_id != "") {
            $sale = \App\ModelProp\TrnsctSales::with(["location", "block", "unit.unitType"])->findOrFail($sales_id);
        }

        $lampirans[1]  = 'surat_pernyataan_penyerahan_data';
        $lampirans[2]  = 'surat_pernyataan_penghunian_rumah_umum_bersubsidi';
        $lampirans[3]  = 'surat_kuasa_pendebatan_dana';
        $lampirans[4]  = 'berita_cara_serah_terima_rumah_umum_tapak';
        $lampirans[5]  = 'surat_pernyataan_penyerahan_spt_pph';
        $lampirans[6]  = 'surat_pernyataan_persetujuan_penyaluran_kpr_bersubsidi_tanpa_menggunakan_sbum';
        $lampirans[7]  = 'surat_persyaratan_kelompok_sasaran';
        $lampirans[8]  = 'surat_permohonan_subsidi_bantuan_uang_muka';
        $lampirans[9]  = 'surat_pengakuan_kekurangan_bayar_uang_muka';
        $lampirans[10] = 'surat_keterangan_pemindahbukuan_dana_sbum_1';
        $lampirans[11] = 'surat_pernyataan_penyelesaian_prasarana';
        $lampirans[12] = 'surat_kuasa_1';
        $lampirans[13] = 'surat_pernyataan_prasarana';
        $lampirans[14] = 'surat_pernyataan_pemohon_kpr_bersubsidi_bank';
        $lampirans[15] = 'surat_pernyataan_pemohon_kpr_bersubsidi_pupr';
        $lampirans[16] = 'surat_pernyataan_calon_debitur_kpr_bersubsidi_btn';
        $lampirans[17] = 'surat_kuasa_2';
        $lampirans[18] = 'surat_keterangan_pemindahbukuan_dana_sbum_2';
        $lampirans[19] = 'surat_pernyataan_tidak_memiliki_rumah';
        $lampirans[20] = 'surat_pernyataan_tidak_bekerja';
        $lampirans[21] = 'ketetapan_waktu_untuk_verifikasi';


        return view('prop.print_kpr_requirements.'.$lampirans[$lampiran_id], compact('customer', 'sale'));
    }


}
