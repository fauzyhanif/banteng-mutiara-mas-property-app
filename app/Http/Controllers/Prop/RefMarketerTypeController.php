<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use App\ModelProp\RefMarketerType;
use App\ModelProp\RefMarketingFeeSetting;
use App\ModelProp\RefSubsidiType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefMarketerTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = RefMarketerType::with(['marketingFeeSettings'])->orderBy('name')->get();
        $subsidiTypes = RefSubsidiType::orderBy('name')->get();
        $marketingFeeSettings = RefMarketingFeeSetting::all();
        $marketingFeeSettingsArray = [];

        foreach ($marketingFeeSettings as $key => $value) {
            $marketingFeeSettingsArray[$value->marketer_type_id][$value->subsidi_type_id] = [
                'method' => $value->method,
                'booking_fee' => $value->booking_fee,
                'akad_fee' => $value->akad_fee
            ];
        }

        return view('prop.ref_marketer_type.index', compact('datas', 'subsidiTypes', 'marketingFeeSettingsArray'));
    }

    public function formStore()
    {
        $subsidiTypes = RefSubsidiType::orderBy('name')->get();
        return view('prop.ref_marketer_type.form_store', compact('subsidiTypes'));
    }

    public function store(Request $request)
    {
        $new = new \App\ModelProp\RefMarketerType();
        $new->name = $request->get('name');
        
        if ($new->save()) {

            foreach ($request->method as $key => $value) {
                $subsidiTypeId = $key;
                $method = $value;
                $bookingFee = $request->booking_fee[$key];
                $akadFee = $request->akad_fee[$key];

                $bookingFee = str_replace([',','.'], '', $bookingFee);
                $akadFee = str_replace([',','.'], '', $akadFee);

                $newSetting = new RefMarketingFeeSetting();
                $newSetting->marketer_type_id = $new->type_id;
                $newSetting->subsidi_type_id = $subsidiTypeId;
                $newSetting->method = $method;
                $newSetting->booking_fee = $bookingFee;
                $newSetting->akad_fee = $akadFee;
                $newSetting->save();
            }
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefMarketerType::findOrFail($id);
        $subsidiTypes = RefSubsidiType::orderBy('name')->get();
        $marketingFeeSettings = RefMarketingFeeSetting::where('marketer_type_id', $id)->get();
        $marketingFeeSettingsArray = [];

        foreach ($marketingFeeSettings as $key => $value) {
            $marketingFeeSettingsArray[$value->subsidi_type_id] = [
                'method' => $value->method,
                'booking_fee' => $value->booking_fee,
                'akad_fee' => $value->akad_fee
            ];
        }

        return view('prop.ref_marketer_type.form_update', \compact('data', 'subsidiTypes', 'marketingFeeSettingsArray'));
    }

    public function update(Request $request, $id)
    {
        $new = \App\ModelProp\RefMarketerType::findOrFail($id);
        $new->name = $request->get('name');
        

        if ($new->save()) {
            foreach ($request->method as $key => $value) {
                $subsidiTypeId = $key;
                $method = $value;
                $bookingFee = $request->booking_fee[$key];
                $akadFee = $request->akad_fee[$key];

                $bookingFee = str_replace([',','.'], '', $bookingFee);
                $akadFee = str_replace([',','.'], '', $akadFee);

                // cek ada tidak
                $check = RefMarketingFeeSetting::where('marketer_type_id', $id)->where('subsidi_type_id', $subsidiTypeId)->count();

                if ($check > 0) { // jika ada
                    if ((int) $bookingFee > 0 && (int) $akadFee > 0) {
                        RefMarketingFeeSetting::where('marketer_type_id', $id)
                            ->where('subsidi_type_id', $subsidiTypeId)
                            ->update([
                                'method' => $method,
                                'booking_fee' => $bookingFee,
                                'akad_fee' => $akadFee
                            ]);   
                    } else {
                        RefMarketingFeeSetting::where('marketer_type_id', $id)->where('subsidi_type_id', $subsidiTypeId)->delete(); 
                    }
                } else {
                    $newSetting = new RefMarketingFeeSetting();
                    $newSetting->marketer_type_id = $new->type_id;
                    $newSetting->subsidi_type_id = $subsidiTypeId;
                    $newSetting->method = $method;
                    $newSetting->booking_fee = $bookingFee;
                    $newSetting->akad_fee = $akadFee;
                    $newSetting->save();
                }
            }

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
