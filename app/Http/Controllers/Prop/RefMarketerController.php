<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class RefMarketerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_marketer.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_marketer')
            ->leftJoin('prop_ref_sdm', 'prop_ref_marketer.sdm_id', '=', 'prop_ref_sdm.sdm_id')
            ->leftJoin('prop_ref_marketer_type', 'prop_ref_marketer.marketer_type', '=', 'prop_ref_marketer_type.type_id')
            ->leftJoin('sys_ref_user', function($join) {
                $join->on('prop_ref_marketer.sdm_id', '=', 'sys_ref_user.sdm_id');
                $join->where('sys_ref_user.user_type', '=', 'MARKETER');
            })
            ->select('prop_ref_sdm.sdm_id', 'prop_ref_sdm.name', 'prop_ref_sdm.nik', 'prop_ref_sdm.email', 'prop_ref_sdm.phone_num', 'prop_ref_sdm.address',
                    'prop_ref_marketer.affiliate_num', 'prop_ref_sdm.rek_bank', 'prop_ref_sdm.rek_number', 'prop_ref_sdm.rek_name',
                    'sys_ref_user.email as user_email', 'prop_ref_marketer_type.name as marketer_type_name', 'prop_ref_sdm.phone_num')
            ->orderBy('prop_ref_sdm.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('name_category', function($datas) {
                $text = $datas->name;
                $text .= "</br><span class='text-green'>$datas->marketer_type_name</span>";

                return $text;
            })
            ->addColumn('phone_address', function($datas) {
                $text = $datas->phone_num . "<br>";
                $text .= $datas->address;

                return $text;
            })
            ->addColumn('rekening', function($datas) {
                $text = $datas->rek_bank . "<br>";
                $text .= $datas->rek_number."<br>";
                $text .= $datas->rek_name;

                return $text;
            })
            ->addColumn('html_have_login', function($datas) {
                $text = "<span class='text-red'>Belum Punya</span>";
                if ($datas->user_email != '') {
                    $text = "<span class='text-green'>Sudah Punya</span>";
                }

                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.marketer.form_update', $datas->sdm_id) . "' class='btn btn-primary btn-sm btn-block'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                if ($datas->user_email == '') {
                    $btn .= "<button type='button' class='btn btn-success btn-sm btn-block' onclick=\"show_modal_create_user('$datas->sdm_id', '$datas->email', '$datas->phone_num')\">";
                    $btn .= "<i class='fas fa-check'></i> Beri Akses Login";
                    $btn .= "</button> ";
                } else {
                    $btn .= "<button type='button' class='btn btn-warning btn-sm btn-block' onclick=\"show_modal_reset_password('$datas->sdm_id', '$datas->email', '$datas->phone_num')\">";
                    $btn .= "<i class='fas fa-rotate'></i> Reset Password";
                    $btn .= "</button> ";
                }

                $btn .= "<button type='button' class='btn btn-danger btn-sm btn-block' onclick=\"show_modal_delete('$datas->sdm_id', '$datas->name', '$datas->affiliate_num')\">";
                $btn .= "<i class='fas fa-times'></i> Hapus";
                $btn .= "</button> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'name_category', 'phone_address', 'rekening', 'html_have_login'])
            ->toJson();
    }

    public function formStore()
    {
        $marketer_types = \App\ModelProp\RefMarketerType::where('is_active', 'Y')->orderBy('name')->get();

        return view('prop.ref_marketer.form_store', compact('marketer_types'));
    }

    public function store(Request $request)
    {
        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');

        if ($sdm->save()) {
            // get affiliate num
            $dt_aff = $this->createAffiliateNum();

            $marketer = new \App\ModelProp\RefMarketer();
            $marketer->sdm_id = $sdm->sdm_id;
            $marketer->affiliate_num = $dt_aff['affiliate_num'];
            $marketer->orders = $dt_aff['orders'];
            $marketer->marketer_type = $request->get('marketer_type');

            if ($marketer->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan',
                    'affiliate_num' => $dt_aff['affiliate_num'],
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefSdm::findOrFail($id);
        $marketer_types = \App\ModelProp\RefMarketerType::where('is_active', 'Y')->orderBy('name')->get();

        return view('prop.ref_marketer.form_update', \compact('data', 'marketer_types'));
    }

    public function update(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');
        $sdm->save();

        if ($sdm->save()) {
            $marketer = \App\ModelProp\RefMarketer::findOrFail($id);
            $marketer->marketer_type = $request->get('marketer_type');

            if ($marketer->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function createAffiliateNum()
    {
        $data = DB::table('prop_ref_marketer')->orderBy('orders', 'desc')->first();
        $order_id = '00001';

        if ($data !== null) {
            $order_id = sprintf('%05d', $data->orders + 1);
        }

        $res = [
            'affiliate_num' => 'M'.$order_id,
            'orders' => $order_id
        ];

        return $res;
    }

    public function createUser(Request $request)
    {
        $sdm_id = $request->get('sdm_id');

        $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);
        $sdm->email = $request->get('email');
        $sdm->phone_num = $request->get('no_telp');
        $sdm->save();

        $request->validate([
            'email'=>'required|unique:sys_ref_user',
            'no_telp'=>'required|unique:sys_ref_user',
        ]);

        $new = new \App\User();
        $new->nama = $sdm->name;
        $new->email = $request->get('email');
        $new->password = Hash::make('12345678');
        $new->alamat = $sdm->address;
        $new->no_telp = $sdm->phone_num;
        $new->user_type = 'MARKETER';
        $new->sdm_id = $sdm->sdm_id;
        $new->id_module = \json_encode(['3']);
        $new->id_role = \json_encode(["8"]);
        $new->save();

        $response = [
            "type" => "success",
            "text" => "User berhasil dibuat"
        ];

        return response()->json($response);
    }

    public function resetPassword(Request $request)
    {
        $sdm_id = $request->get('sdm_id');
        \App\User::where('sdm_id', $sdm_id)->where('user_type', 'MARKETER')->update([
            'password' => Hash::make('12345678')
        ]);

        $response = [
            "type" => "success",
            "text" => "Reset Password berhasil"
        ];

        return response()->json($response);
    }

    public function delete(Request $request)
    {
        $sdm_id = $request->get('sdm_id');
        $affiliate_num = $request->get('affiliate_num');

        // check trnsct booking
        $booking = \App\ModelProp\TrnsctSales::where('affiliate_num', "$affiliate_num")->where('status', '!=', 'CANCEL')->get();

        $res = [];
        if ($booking->count() > 0) {
            $res = [
                "type" => "failed",
                "text" => "Marketer ini tidak bisa dihapus, karna ada transaksi penjualan yang melibatkan marketer ini."
            ];
        } else {
            $marketer = \App\ModelProp\RefMarketer::where('sdm_id', $sdm_id)->delete();

            $user = \App\User::where('sdm_id', $sdm_id)->where('user_type', 'MARKETER')->delete();

            $res = [
                "type" => "success",
                "text" => "Hapus marketer berhasil!"
            ];
        }

        return response()->json($res);
    }

    public function hak(Request $request)
    {
        $status = ($request->get('status')) ? $request->get('status') : 'ALL';

        return view('prop.ref_marketer.hak', compact('status'));
    }

    public function hakList(Request $request)
    {
        $status = $request->get('status');
        if ($status == 'ALL') {
            $datas = $this->getHakAll();
        } elseif ($status == 'NOT_YET') {
            $datas = $this->getHakNotYet();
        } elseif ($status == 'DONE') {
            $datas = $this->getHakDone();
        } elseif ($status == 'KPR_SUDAH_AKAD') {
            $datas = $this->getHakKprSudahAkad();
        } elseif ($status == 'KPR_DP_LUNAS') {
            $datas = $this->getHakKprDpLunas();
        }

        return DataTables::of($datas)
            ->addColumn('marketer_html', function($datas) {
                $text = '';
                if ($datas->affiliate_num != '') {
                    $text = '<b>' . $datas->marketer->sdm->name . ' (' . $datas->marketer->sdm->phone_num . ')</b> <br>';
                    $text .= 'Rek. Bank : ' . $datas->marketer->sdm->rek_bank . '<br>';
                    $text .= 'Rek. Nomor : ' . $datas->marketer->sdm->rek_number . '<br>';
                    $text .= 'Rek. Nama : ' . $datas->marketer->sdm->rek_name . '<br>';
                }

                return $text;
            })
            ->addColumn('customer_html', function($datas) {
                $text = '<b>' . $datas->customer->name . '</b><br>';
                $text .= $datas->customer->phone_num;

                return $text;
            })
            ->addColumn('unit_html', function($datas) {
                $text = '<b>' . $datas->block->name . ' No ' . $datas->unit->number . '</b><br>';
                $text .= 'Jenis Penjualan : ' . $datas->sales_type . "<br>";
                $text .= 'Tahap Terbaru : ' . $datas->last_sales_step->name . "<br>";

                if ($datas->sales_type == 'KPR' && ($datas->step_sales == '6' || $datas->step_sales == '16' || $datas->step_sales == '20' || $datas->step_sales == '21')) {
                    $text .= 'Tgl Akad : ' . date("d-m-Y", strtotime($datas->step_sales_date));
                } else {
                    $text .= 'Tgl Penjualan : ' . date("d-m-Y", strtotime($datas->sales_date));
                }

                return $text;
            })
            ->addColumn('hak_marketer_html', function($datas) {
                $text = number_format($datas->affiliate_fee,0,',','.');

                return $text;
            })
            ->addColumn('hak_marketer_paid_html', function($datas) {
                $text = number_format($datas->affiliate_fee_paid,0,',','.');

                return $text;
            })
            ->addColumn('hak_marketer_sisa_html', function($datas) {
                $text = number_format($datas->affiliate_fee - $datas->affiliate_fee_paid,2,',','.');

                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // detail link
                $btn = "<a href='" . route('prop.sales.detail', $datas->id) . "' class='btn btn-info btn-sm btn-block' target='_blank'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                // detail link
                $btn .= "<button class='btn btn-success btn-sm btn-block' onclick=\"show_modal_pencairan_hak('$datas->id')\">";
                $btn .= "<i class='fas fa-edit'></i> Pencairan";
                $btn .= "</button> ";

                // cetak link
                $btn .= "<a href='".url('prop/marketer/print_hak_detail', $datas->id)."' class='btn btn-warning btn-sm btn-block' target='_blank'>";
                $btn .= "<i class='fas fa-print'></i> Cetak";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'marketer_html', 'customer_html', 'unit_html', 'hak_marketer_html', 'hak_marketer_paid_html', 'hak_marketer_sisa_html'])
            ->toJson();
    }

    public function getHakAll()
    {
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])
            ->where('status', '!=', 'CANCEL')
            ->where('kpr_status', '!=', 'REJECTED')
            ->whereNotNull('affiliate_num')
            ->orderBy('sales_date', 'ASC')
            ->get();

        return $datas;
    }

    public function getHakDetail(Request $request)
    {
        $id = $request->id;
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])->findOrFail($id);

        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();

        $histories = \App\ModelFinance\TrnsctPaid::with(['trnsct', 'cash_bank'])
            ->whereHas('trnsct', function ($query) use($id) {
                $query->where('is_marketing_fee', '1');
                $query->where('sales_id', $id);
                $query->where('finance_cancel', '0');
            })
            ->get();

        return view('prop.ref_marketer.form_pencairan_hak_view', compact('sales', 'cashes', 'histories'));
    }

    public function printHakDetail(Request $request)
    {
        $id = $request->id;
        $sales = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])->findOrFail($id);
        
        return view('prop.ref_marketer.print_hak_detail', compact('sales'));
    }

    public function getHakNotYet()
    {
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])
            ->where('status', '!=', 'CANCEL')
            ->where('kpr_status', '!=', 'REJECTED')
            ->where('affiliate_fee', '>', DB::raw('affiliate_fee_paid'))
            ->whereNotNull('affiliate_num')
            ->where(function($query){
                $query->where('step_sales', '=', '6');
                $query->orWhere('step_sales', '=', '16');
                $query->orWhere('step_sales', '=', '20');
                $query->orWhere('step_sales', '=', '21');
            })
            ->orderBy('sales_date', 'ASC')
            ->get();

        return $datas;
    }

    public function getHakDone()
    {
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])
            ->where('status', '!=', 'CANCEL')
            ->where('kpr_status', '!=', 'REJECTED')
            ->where('affiliate_fee', '<=', DB::raw('affiliate_fee_paid'))
            ->whereNotNull('affiliate_num')
            ->orderBy('sales_date', 'ASC')
            ->get();

        return $datas;
    }

    public function getHakKprSudahAkad()
    {
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])
            ->whereNotNull('affiliate_num')
            ->where('status', '!=', 'CANCEL')
            ->where('sales_type', '=', 'KPR')
            ->where('kpr_status', 'ACCEPTED')
            ->where(function($query){
                $query->where('step_sales', '=', '6');
                $query->orWhere('step_sales', '=', '16');
                $query->orWhere('step_sales', '=', '20');
                $query->orWhere('step_sales', '=', '21');
            })
            ->orderBy('sales_date', 'ASC')
            ->get();

        return $datas;
    }

    public function getHakKprDpLunas()
    {
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer.sdm', 'last_sales_step'])
            ->whereNotNull('affiliate_num')
            ->where('status', '!=', 'CANCEL')
            ->where('sales_type', '=', 'KPR')
            ->where('kpr_status', 'ACCEPTED')
            ->where(DB::raw("(ttl_trnsct - kpr_credit_acc) - (ttl_trnsct_paid - kpr_credit_acc_paid)"), ">=", 0)
            ->where(function($query){
                $query->where('step_sales', '=', '6');
                $query->orWhere('step_sales', '=', '16');
                $query->orWhere('step_sales', '=', '20');
                $query->orWhere('step_sales', '=', '21');
            })
            ->orderBy('sales_date', 'ASC')
            ->get();

        return $datas;
    }
}
