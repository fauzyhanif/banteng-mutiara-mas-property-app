<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefCertificateStepController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_certificate_step.index');
    }

    public function listData()
    {
        $datas = \App\ModelProp\RefCertificateStep::orderBy('orders')->get();

        return DataTables::of($datas)
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.certificate_step.form_update', $datas->step_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('prop.ref_certificate_step.form_store');
    }

    public function store(Request $request)
    {
        $new = new \App\ModelProp\RefCertificateStep();
        $new->name = $request->get('name');
        $new->orders = $request->get('orders');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefCertificateStep::findOrFail($id);

        return view('prop.ref_certificate_step.form_update', \compact('data'));
    }

    public function update(Request $request, $id)
    {
        $new = \App\ModelProp\RefCertificateStep::findOrFail($id);
        $new->name = $request->get('name');
        $new->orders = $request->get('orders');
        $new->is_active = $request->get('is_active');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan'
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
