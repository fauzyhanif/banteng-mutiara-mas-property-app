<?php

namespace App\Http\Controllers\Prop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('prop.ref_store.index');
    }

    public function listData()
    {
        $datas = DB::table('prop_ref_store')
            ->leftJoin('prop_ref_sdm', 'prop_ref_store.sdm_id', '=', 'prop_ref_sdm.sdm_id')
            ->select('prop_ref_sdm.sdm_id', 'prop_ref_sdm.name', 'prop_ref_sdm.nik', 'prop_ref_sdm.phone_num',
                    'prop_ref_sdm.address', 'prop_ref_store.description')
            ->orderBy('prop_ref_sdm.name')
            ->get();

        return DataTables::of($datas)
            ->addColumn('phone_address', function($datas){
                $text = $datas->phone_num."<br>".$datas->address;
                return $text;
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.toko_bangunan.form_update', $datas->sdm_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Update";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'phone_address'])
            ->toJson();
    }

    public function formStore()
    {
        $goods = \App\ModelProp\RefGoods::orderBy("name")->get();
        return view('prop.ref_store.form_store', compact("goods"));
    }

    public function store(Request $request)
    {
        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');

        if ($sdm->save()) {
            $store = new \App\ModelProp\RefStore();
            $store->sdm_id = $sdm->sdm_id;
            $store->description = $request->get('description');

            // cek apakah ada ceklis barang
            if (count($request->get('goods_id')) > 0) {
                for ($i=0; $i < count($request->get('goods_id')); $i++) {
                    $store_goods = new \App\ModelProp\RefStoreGoods();
                    $store_goods->sdm_id = $sdm->sdm_id;
                    $store_goods->goods_id = $request->get('goods_id')[$i];
                    $store_goods->save();
                }
            }

            if ($store->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $data = \App\ModelProp\RefSdm::where("sdm_id", $id)->with(['store'])->first();
        $goods = \App\ModelProp\RefGoods::orderBy("name")->get();
        $store_goods = \App\ModelProp\RefStoreGoods::where("sdm_id", $id)->get();

        $arr_store_goods = [];
        foreach ($store_goods as $key => $value) {
            $arr_store_goods[$value->goods_id] = $value->goods_id;
        }

        return view('prop.ref_store.form_update', \compact('data', 'goods', 'arr_store_goods'));
    }

    public function update(Request $request, $id)
    {
        $sdm = \App\ModelProp\RefSdm::findOrFail($id);
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->save();

        if ($sdm->save()) {
            $store = \App\ModelProp\RefStore::findOrFail($id);
            $store->description = $request->get('description');
            if ($store->save()) {

                // cek apakah ada ceklis barang
                if (count($request->get('goods_id')) > 0) {
                    // delete barang yg sudah ada
                    \App\ModelProp\RefStoreGoods::where("sdm_id", "$id")->delete();
                    
                    for ($i=0; $i < count($request->get('goods_id')); $i++) {


                        $store_goods = new \App\ModelProp\RefStoreGoods();
                        $store_goods->sdm_id = $sdm->sdm_id;
                        $store_goods->goods_id = $request->get('goods_id')[$i];
                        $store_goods->save();
                    }
                }

                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan'
                ];
            } else {
               $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }
}
