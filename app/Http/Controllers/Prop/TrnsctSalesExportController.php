<?php

namespace App\Http\Controllers\Prop;

use App\Exports\SalesExport;
use App\Http\Controllers\Controller;
use App\ModelProp\TrnsctSales;
use App\ModelProp\TrnsctSalesExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class TrnsctSalesExportController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $results = TrnsctSalesExport::paginate(25);
    return view('prop.trnsct_sales_export.index', compact('results'));  
  }

  public function listData(Request $request)
  {
    if ($request->ajax()) {
      $limit = $request->get('limit');
      $results = TrnsctSalesExport::paginate($limit);
      return view('prop.trnsct_sales_export.list_data', compact('results')); 
    }
  }

  public function count(Request $request)
  {
    if ($request->ajax()) {
      return TrnsctSalesExport::count();
    }
  }

  public function create(Request $request)
  {
    // check exists
    $check = TrnsctSalesExport::where('sales_id', $request->id)->count();
    if ($check > 0) return response()->json(['status' => 'success', 'text' => 'Data berhasil disimpan']);

    $sales = TrnsctSales::with(['customer', 'location', 'block', 'unit'])->where('sales_id', $request->id)->first();
    $attribute = [
      'sales_id' => $sales->sales_id,
      'customer_name' => $sales->customer->name,
      'customer_nik' => $sales->customer->nik,
      'customer_date_of_birth' => $sales->customer->date_ofbirth,
      'customer_place_of_birth' => $sales->customer->place_of_birth,
      'customer_address' => $sales->customer->address,
      'customer_is_married' => $sales->customer->is_married,
      'couple_name' => $sales->customer->couple,
      'couple_nik' => $sales->customer->couple_nik,
      'couple_date_of_birth' => $sales->customer->couple_date_of_birth,
      'couple_place_of_birth' => $sales->customer->couple_place_of_birth,
      'location_id' => $sales->location_id,
      'location_name' => $sales->location->name,
      'block_id' => $sales->block_id,
      'block_name' => $sales->block->name,
      'unit_id' => $sales->unit_id
    ];

    $new = new TrnsctSalesExport();
    $resCreate = $new->create($attribute);

    $res = [
      'status' => 'failed',
      'text' => 'Data gagal disimpan'
    ];

    if ($resCreate) {
      $res = [
        'status' => 'success',
        'text' => 'Data berhasil disimpan'
      ];
    }

    return response()->json($res);
  }

  public function delete(Request $request)
  {
    $data = TrnsctSalesExport::findOrFail($request->id);
    $resDelete = $data->delete();

    $res = [
      'status' => 'failed',
      'text' => 'Data gagal dihapus'
    ];

    if ($resDelete) {
      $res = [
        'status' => 'success',
        'text' => 'Data berhasil dihapus'
      ];
    }

    return response()->json($res);
  }

  public function deleteAll()
  {
    $data = TrnsctSalesExport::truncate();

    $res = [
      'status' => 'failed',
      'text' => 'Data gagal dihapus'
    ];

    if ($data) {
      $res = [
        'status' => 'success',
        'text' => 'Data berhasil dihapus'
      ];
    }

    return response()->json($res);
  }

  public function export()
  {
    return Excel::download(new SalesExport, 'penjualan.xlsx'); 
  }

}
