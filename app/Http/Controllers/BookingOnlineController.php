<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class BookingOnlineController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(\App\Http\Controllers\Finance\TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index()
    {
        $locations = \App\ModelProp\RefLocation::orderBy('name')->get();
        return view('booking_online.index', compact('locations'));
    }

    public function checkout(Request $request)
    {
        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $unit_id = $request->get('unit_id');
        $trnsct_type = $request->get('trnsct_type');
        $affiliate_num = $request->get('affiliate_num');


        $unit = \App\ModelProp\RefUnit::with(["location", "block", "unitType"])
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("number", "$unit_id")
            ->first();

        return view('booking_online.checkout', compact('unit', 'trnsct_type', 'affiliate_num'));
    }

    public function store(Request $request)
    {
        $sdm_id = "";

        // cek user baru atau lama
        if ($request->get('login_register') == 'REGISTER') {
            // user baru => register
            // cek exist email
            $email = $request->get('email');
            $name = $request->get('name');
            $phone_num = $request->get('name');
            $address = $request->get('address');

            $check_user_by_email = \App\User::where('email', "$email")->where('user_type', 'CUSTOMER')->get();
            if ($check_user_by_email->count() > 0) {
                // jika email sudah terdaftar
                $res = [
                    'status' => 'failed',
                    'text' => 'Email sudah terdaftar di sistem kami.'
                ];

                return response()->json($res);
                die();
            } else {
                // jika email belum terdaftar
                $new_sdm = new \App\ModelProp\RefSdm();
                $new_sdm->name = $request->get('name');
                $new_sdm->nik = $request->get('nik');
                $new_sdm->date_ofbirth = $request->get('date_ofbirth');
                $new_sdm->couple = $request->get('couple');
                $new_sdm->phone_num = $request->get('phone_num');
                $new_sdm->address = $request->get('address');
                $new_sdm->address_domicile = $request->get('address_domicile');
                $new_sdm->job = $request->get('job');

                $new_sdm->institute = $request->get('institute');
                if ($new_sdm->save()) {
                    // replace sdm_id
                    $sdm_id = $new_sdm->sdm_id;

                    // generate random password
                    $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUvWXYZ';
                    $generated_password =  substr(str_shuffle($permitted_chars), 0, 4);

                    $new_cust = new \App\ModelProp\RefCustomer();
                    $new_cust->sdm_id = $new_sdm->sdm_id;
                    $new_cust->save();

                    $new_user = new \App\User();
                    $new_user->nama = $request->get('name');
                    $new_user->email = $request->get('email');
                    $new_user->password = Hash::make("54321");
                    $new_user->no_telp = $request->get('phone_num');
                    $new_user->alamat = $request->get('address');
                    $new_user->id_module = json_encode(["0"]);
                    $new_user->id_role = json_encode(["0"]);
                    $new_user->user_type = "CUSTOMER";
                    $new_user->sdm_id = $new_sdm->sdm_id;
                    $new_user->aktif = "Y";
                    $new_user->save();

                    // set session
                    Session::put('auth_nama', $request->get('name'));
                    Session::put('auth_email', $request->get('email'));
                    Session::put('sdm_id', $sdm_id);
                    Session::put('auth_user_type', 'CUSTOMER');
                } else {
                    $res = [
                        'status' => 'failed',
                        'text' => 'Data konsumen & data booking gagal tersimpan'
                    ];

                    return response()->json($res);
                    die();
                }
            }

        } else {
            // user lama
            $email = $request->get('login_email');
            $exist_user = \App\User::where('email', "$email")->where('user_type', 'CUSTOMER')->first();

            if ($exist_user === null) {
                 $res = [
                    'status' => 'failed',
                    'text' => 'Email salah'
                ];

                return response()->json($res);
                die();
            } else {
                if (!Hash::check($request->get('login_password'), $exist_user->password)) {
                    $res = [
                        'status' => 'failed',
                        'text' => 'Password salah'
                    ];

                    return response()->json($res);
                    die();
                } else {
                    // set session
                    $sdm_id = $exist_user->sdm_id;
                    Session::put('auth_nama', $exist_user->nama);
                    Session::put('auth_email', $exist_user->email);
                    Session::put('sdm_id', $exist_user->sdm_id);
                    Session::put('auth_user_type', 'CUSTOMER');
                }
            }
        }

        // get invoice number
        $TrnsctJurnalController = $this->TrnsctJurnalController;
        $sales_id = $TrnsctJurnalController->invoiceNumber('SALES');

        $location_id = $request->get('location_id');
        $block_id = $request->get('block_id');
        $number = $request->get('unit_id');
        $trnsct_type = $request->get('trnsct_type');

        $unit = \App\ModelProp\RefUnit::with(["location", "block", "unitType"])
            ->where("location_id", "$location_id")
            ->where("block_id", "$block_id")
            ->where("number", "$number")
            ->first();

        $basic_price = ($trnsct_type == 'CASH') ? $unit->harga_jual_cash : $unit->harga_jual_kpr;

        $new_trnsct = new \App\ModelProp\TrnsctSales();
        $new_trnsct->sales_date = date('Y-m-d');
        $new_trnsct->sales_id = $sales_id;
        $new_trnsct->sales_type = $request->get('sales_type');
        $new_trnsct->location_id = $request->get('location_id');
        $new_trnsct->block_id = $request->get('block_id');
        $new_trnsct->unit_id = $request->get('unit_id');
        $new_trnsct->basic_price = $basic_price;
        $new_trnsct->discount = '0';
        $new_trnsct->ttl_trnsct = $basic_price;
        $new_trnsct->ttl_trnsct_paid = '0';
        $new_trnsct->customer_id = $sdm_id;
        $new_trnsct->affiliate_num = ($request->get('affiliate_num') != '') ? $request->get('affiliate_num') : '';
        $new_trnsct->affiliate_fee = '0';
        $new_trnsct->affiliate_fee_paid = '0';
        $new_trnsct->status = 'BOOKING';
        $new_trnsct->booking_online = 'Y';
        if ($new_trnsct->save()) {

            // ubah status unit menjadi sold
            $data_post = $request->all();
            $data_post['customer_id'] = $sdm_id;
            $this->updateStatusUnitToBooking($data_post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sales_id' => $new_trnsct->id
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }

        return response()->json($res);
    }

    public function updateStatusUnitToBooking($request)
    {
        $location_id = $request['location_id'];
        $block_id = $request['block_id'];
        $unit_id = $request['unit_id'];
        $customer_id = $request['customer_id'];
        $affiliate_num = $request['affiliate_num'];

        \App\ModelProp\RefUnit::where('location_id', "$location_id")
            ->where('block_id', "$block_id")
            ->where('number', "$unit_id")
            ->update([
                "customer_id" => $customer_id,
                "marketer_id" => $affiliate_num,
                "status" => 'BOOKED',
            ]);
    }



}
