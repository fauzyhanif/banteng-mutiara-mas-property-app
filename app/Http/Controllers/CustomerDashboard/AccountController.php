<?php

namespace App\Http\Controllers\CustomerDashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{

    public function changePassword()
    {
        if (!Session::has('auth_nama') && Session::get('auth_user_type') != 'CUSTOMER') {
            return redirect('/login');
        }

        $sdm_id = Session::get('sdm_id');
        $user = \App\User::where('sdm_id', "$sdm_id")->first();

        return view('customer_dashboard.account.change_password', compact('user'));
    }

    public function doChangePassword(Request $request)
    {
        $id = $request->get('id');
        $user = \App\User::findOrFail($id);
        if (Hash::check($request->get('password_old'), $user->password)) {
            $user->password = Hash::make(($request->get('password_new')));
            $user->password_changed = 'Y';
            $user->save();
            return back()->with('success', 'Password berhasil diperbaharui!');
        } else {
            return back()->with('error', 'Password lama salah!');
        }
    }


}
