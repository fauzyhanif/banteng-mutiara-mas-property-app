<?php

namespace App\Http\Controllers\CustomerDashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{

    public function index()
    {
        if (!Session::has('auth_nama') && Session::get('auth_user_type') != 'CUSTOMER') {
            return redirect('/login');
        }

        $sdm_id = Session::get('sdm_id');
        $customer = \App\ModelProp\RefSdm::with(['user'])->findOrFail($sdm_id);
        $res_unit_types = $this->unit_types();

        $purchases = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit'])
            ->where('customer_id', "$sdm_id")
            ->orderBy('sales_id', 'DESC')
            ->get();

        return view('customer_dashboard.dashboard.index', compact('customer', 'purchases', 'res_unit_types'));
    }

    public function detail($id)
    {
        if (!Session::has('auth_nama') && Session::get('auth_user_type') != 'CUSTOMER') {
            return redirect('/login');
        }

        $purchase = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'marketer.sdm'])->findOrFail($id);
        $res_unit_types = $this->unit_types();


        return view('customer_dashboard.dashboard.detail', compact('customer', 'purchase', 'res_unit_types'));
    }

    public function unit_types()
    {
        $unit_types = \App\ModelProp\RefUnitType::all();

        $res_unit_types = [];
        foreach ($unit_types as $key => $value) {
            $res_unit_types[$value->unittype_id] = $value->name;
        }

        return $res_unit_types;
    }

    public function uploadBuktiBayar(Request $request)
    {
        if (!Session::has('auth_nama') && Session::get('auth_user_type') != 'CUSTOMER') {
            return redirect('/login');
        }

        $sales_id = $request->get('sales_id');
        $file_name = "";
        $status = "";

        $check_exists_file = \App\ModelProp\TrnsctSales::where('sales_id', "$sales_id")->first();
        if ($check_exists_file->booking_online_file_bayar != '') {
            unlink('booking_online_file_bayar/'.$check_exists_file->booking_online_file_bayar);
        }

        if ($request->file('file')) {
            $file = $request->file('file');
            // rename filename
            $file_name = str_replace("/", "", $sales_id) . '_' . date('YmdHis') . "." . $file->getClientOriginalExtension();

            // upload file
            if ($file->move('booking_online_file_bayar', $file_name)) {
                $status = true;
            } else {
                $status = false;
                $file_name = "";
            }

        }

        \App\ModelProp\TrnsctSales::where('sales_id', "$sales_id")->update(["booking_online_file_bayar" => $file_name]);
        if ($status == true) {
            return back()->with('success', 'File berhasil diupload!');
        } else {
            return back()->with('error', 'File gagal diupload!');
        }

    }

}
