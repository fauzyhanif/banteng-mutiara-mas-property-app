<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function doAuth(Request $request)
    {
        $request->validate([
            "username" => "required",
            "password" => "required"
        ]);

        $data_exist = \App\User::with(['marketer'])
            ->where('email', '=', $request->get('username'))
            ->orWhere('no_telp', '=', $request->get('username'))
            ->first();

        if ($data_exist === null) {
            return redirect('/login')->withErrors(['Email / No Handphone / Password salah!', '']);
        } else {
            if (!Hash::check($request->get('password'), $data_exist->password)) {
                return redirect('/login')->withErrors(['Email / No Handphone / Password salah!', '']);
            } else {
            
                // set session
                Session::put('auth_nama', $data_exist->nama);
                Session::put('auth_email', $data_exist->email);
                Session::put('auth_module', $data_exist->id_module);
                Session::put('auth_role', $data_exist->id_role);

                $decode_module = \json_decode($data_exist->id_module);
                $decode_role = \json_decode($data_exist->id_role);
                if (count($decode_module) == 1 && count($decode_role) == 1) {
                    if ($data_exist->user_type == 'ADMIN') {
                        $this->setAuth($decode_role[0], $decode_module[0]);
                        Session::put('auth_user_type', 'ADMIN');
                        return redirect('/');
                    } elseif ($data_exist->user_type == 'CUSTOMER') {
                        Session::put('sdm_id', $data_exist->sdm_id);
                        Session::put('auth_user_type', 'CUSTOMER');
                        return redirect('/customer_dashboard/dashboard');
                    } elseif ($data_exist->user_type == 'MARKETER') {
                        Session::put('sdm_id', $data_exist->sdm_id);
                        Session::put('sdm_name', $data_exist->name);
                        Session::put('affiliate_num', $data_exist->marketer->affiliate_num);
                        Session::put('auth_user_type', 'MARKETER');
                        return redirect('/marketer_dashboard');
                    }
                } else {
                    // redirect to gate
                    return redirect('/gate');
                }
            }
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/login');
    }

    public function changePassword(Request $request)
    {
        $myEmail = Session::get('auth_email');
        $user = \App\User::where('email', $myEmail)->first();

        return view('auth.changePassword');
    }

    public function setAuth($idRole, $idModule)
    {
        $module = \App\SystemModel\SysRefModule::where("id_module", $idModule)->first();
        $groups = \App\SystemModel\SysRefGroupMenu::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->get();

        $menus = \App\SystemModel\SysRefAcl::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->where("is_menu", "Y")
                ->where("parent", "0")
                ->where("aktif", "Y")
                ->orderBy("urut", "ASC")
                ->get();

        $submenus = \App\SystemModel\SysRefAcl::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->where("is_menu", "Y")
                ->where("parent", "!=", "0")
                ->where("aktif", "Y")
                ->orderBy("urut", "ASC")
                ->get();

        $acls = \App\SystemModel\SysRefAcl::where("id_module", "like", "%$idModule%")
                ->where("id_role", "like", "%$idRole%")
                ->get();

        $arr_acls = [];
        foreach ($acls as $key => $value) {
            $arr_acls[] = "$value->route";
        }

        Session::put('auth_role_aktif', $idRole);
        Session::put('auth_module_aktif', $idModule);
        Session::put('auth_module_image', $module->image);
        Session::put('auth_module_nama', $module->nama);
        Session::put('auth_group_menu', $groups);
        Session::put('auth_menu', $menus);
        Session::put('auth_submenu', $submenus);
        Session::put('auth_acl', $arr_acls);
    }
}
