<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Session;

class MarketerRegistrationController extends Controller
{
    public function index()
    {
        return view('marketer_registration.index');
    }

    public function store(Request $request)
    {
        $sdm = new \App\ModelProp\RefSdm();
        $sdm->name = $request->get('name');
        $sdm->phone_num = $request->get('phone_num');
        $sdm->address = $request->get('address');
        $sdm->rek_bank = $request->get('rek_bank');
        $sdm->rek_number = $request->get('rek_number');
        $sdm->rek_name = $request->get('rek_name');

        if ($sdm->save()) {
            // get affiliate num
            $dt_aff = $this->createAffiliateNum();

            $marketer = new \App\ModelProp\RefMarketer();
            $marketer->sdm_id = $sdm->sdm_id;
            $marketer->affiliate_num = $dt_aff['affiliate_num'];
            $marketer->orders = $dt_aff['orders'];
            $marketer->category = $request->get('category');

            if ($marketer->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan',
                    'affiliate_num' => $dt_aff['affiliate_num'],
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan'
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan'
            ];
        }


        return response()->json($res);
    }

    public function createAffiliateNum()
    {
        $data = DB::table('prop_ref_marketer')->orderBy('orders', 'desc')->first();
        $order_id = '00001';

        if ($data !== null) {
            $order_id = sprintf('%05d', $data->orders + 1);
        }

        $res = [
            'affiliate_num' => 'M'.$order_id,
            'orders' => $order_id
        ];

        return $res;
    }

}
