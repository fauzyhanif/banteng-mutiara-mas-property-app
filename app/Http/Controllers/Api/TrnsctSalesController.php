<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ModelProp\TrnsctSales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class TrnsctSalesController extends Controller
{
    public function res(int $status, string $message, array $data, int $totalData)
    {
        $res = [
            'status' => $status,
            'message' => $message,
            'total_data' => $totalData,
            'data' => $data,
        ];

        return response()->json($res);
    }

    public function getAll(Request $request)
    {
        $marketerTypeId = $request->get('marketer_type_id','');
        $sales = TrnsctSales::with(['location', 'unit', 'block', 'customer', 'last_sales_step', 'marketer'])
            ->whereHas('marketer', function ($query) use ($marketerTypeId) {
                $query->where('marketer_type', $marketerTypeId);
            })
            ->where('kpr_status', '!=', 'REJECTED')
            ->where('status', '!=', 'CANCEL')
            ->get()
            ->toArray();
            
        return $this->res(200, 'ok', $sales, count($sales));
    }

    public function getDetail($id)
    {
        $sales = TrnsctSales::with(['location', 'unit', 'block', 'customer', 'last_sales_step', 'marketer'])->find($id)->toArray();
        return $this->res(200, 'ok', $sales, 1);
    }
}