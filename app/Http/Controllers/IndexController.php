<?php

namespace App\Http\Controllers;

use App\ModelProp\TrnsctSales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use DataTables;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (!Session::has('auth_nama')) {
            return redirect('/login');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $steps = \App\ModelProp\RefSalesStep::where('type', 'KPR')->orderBy('orders', 'ASC')->get();

        return view('index.index', compact('steps'));
    }

    public function pengajuanPencairan()
    {
        $this_day = date('Y-m-d');
        $datas = \App\ModelFinance\TrnsctPengajuanPencairan::where("submission_status", "OPEN")
            ->where("submission_deadline", "<=", "$this_day")
            ->orderBy("submission_deadline", "ASC")
            ->limit(10)
            ->get();

        return view('index.view_pengajuan_pencairan', compact('datas'));
    }

    public function progresPembangunanBelumSelesai()
    {
        // data unit
        $datas = DB::table('prop_ref_worklist as a')
            ->leftJoin('prop_ref_location as b', 'a.location_id', '=', 'b.location_id')
            ->leftJoin('prop_ref_block as c', 'a.block_id', '=', 'c.block_id')
            ->select(DB::raw('count(a.list_id) as jml_job, a.block_id, b.name as location_name, c.name as block_name'))
            ->groupBy('a.location_id', 'a.block_id')
            ->where('a.progress', '<', 100)
            ->orderBy('b.name', 'ASC')
            ->orderBy('c.name', 'ASC')
            ->get();

        return DataTables::of($datas)
            ->addColumn('block', function($datas){
                $btn = "<a href='javascript:void(0)' onclick=\"showModalListPembangunanBelumSelesai('$datas->block_id', '$datas->block_name')\">";
                $btn .= $datas->location_name . ' ' . $datas->block_name;
                $btn .= "</a>";

                return $btn;
            })
            ->rawColumns(['block'])
            ->toJson();
    }

    public function progresPembangunanBelumSelesaiListUnit($block_id)
    {
        $datas = DB::table('prop_ref_worklist as a')
            ->leftJoin('prop_ref_unit as b', function($join)
            {
                $join->on('a.location_id', '=', 'b.location_id')
                    ->on('a.block_id', '=', 'b.block_id')
                    ->on('a.unit_id', '=', 'b.number');
            })
            ->select(DB::raw('count(a.list_id) as jml_job, b.number, b.unit_id'))
            ->where('a.progress', '<', 100)
            ->where('a.block_id', $block_id)
            ->groupBy('a.location_id', 'a.block_id', 'b.number')
            ->orderBy('a.unit_id')
            ->get();

        return view('index.view_list_pembangunan_belum_selesai', compact('datas'));
    }

    public function progresPembangunanSelesai()
    {
        // data unit
        $datas = DB::table('prop_ref_worklist as a')
            ->leftJoin('prop_ref_location as b', 'a.location_id', '=', 'b.location_id')
            ->leftJoin('prop_ref_block as c', 'a.block_id', '=', 'c.block_id')
            ->select(DB::raw('count(a.list_id) as jml_job, a.block_id, b.name as location_name, c.name as block_name'))
            ->groupBy('a.location_id', 'a.block_id')
            ->where('a.progress', '>=', 100)
            ->orderBy('b.name', 'ASC')
            ->orderBy('c.name', 'ASC')
            ->get();

        return DataTables::of($datas)
            ->addColumn('block', function($datas){
                $btn = "<a href='javascript:void(0)' onclick=\"showModalListPembangunanSelesai('$datas->block_id', '$datas->block_name')\">";
                $btn .= $datas->location_name . ' ' . $datas->block_name;
                $btn .= "</a>";

                return $btn;
            })
            ->rawColumns(['block'])
            ->toJson();
    }

    public function progresPembangunanSelesaiListUnit($block_id)
    {
        $datas = DB::table('prop_ref_worklist as a')
            ->leftJoin('prop_ref_unit as b', function($join)
            {
                $join->on('a.location_id', '=', 'b.location_id')
                    ->on('a.block_id', '=', 'b.block_id')
                    ->on('a.unit_id', '=', 'b.number');
            })
            ->select(DB::raw('count(a.list_id) as jml_job, b.number, b.unit_id'))
            ->where('a.progress', '>=', 100)
            ->where('a.block_id', $block_id)
            ->groupBy('a.location_id', 'a.block_id', 'b.number')
            ->orderBy('a.unit_id')
            ->get();

        return view('index.view_list_pembangunan_selesai', compact('datas'));
    }

    public function progresKpr(Type $var = null)
    {
        $steps = \App\ModelProp\RefSalesStep::where('type', 'KPR')->orderBy('orders')->get();
        $dt_step = DB::table('prop_trnsct_sales')
            ->select(DB::raw("block_id, step_sales, count(*) as total"))
            ->where('status', '!=', 'CANCEL')
            ->where('sales_type', 'KPR')
            // ->where('ttl_trnsct_paid', '!=', '0')
            ->groupBy('step_sales')
            ->get();

        $data = [];
        foreach ($dt_step as $key => $value) {
            $data[$value->step_sales] = $value->total;
        }

        return view('index.view_progres_kpr', compact('data', 'steps'));
    }

    public function progresKprListUnit(Request $request)
    {
        $step_id = $request->get('step_id');
        $step = \App\ModelProp\RefSalesStep::findOrFail($step_id);
        $step_name = $step->name;

        return view('index.view_progres_kpr_list_unit', compact('step_id', 'step_name'));
    }

    public function progresKprListUnitCetak(Request $request)
    {
        $step_id = $request->get('step_id');
        $step = \App\ModelProp\RefSalesStep::findOrFail($step_id);
        $step_name = $step->name;

        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer', 'sales_step'])
            ->where('sales_type', 'KPR')
            // ->where('ttl_trnsct_paid', '!=', '0')
            ->where('step_sales', "$step_id")
            ->where('status', "!=", "CANCEL")
            ->orderBy('block_id', 'ASC')
            ->get();

        return view('index.view_progres_kpr_list_unit_cetak', compact('datas', 'step_name'));
    }

    public function progresKprListUnitJson(Request $request)
    {
        $step_id = $request->get('step_id');
        $datas = \App\ModelProp\TrnsctSales::with(['location', 'block', 'unit', 'customer', 'marketer', 'sales_step'])
            ->where('sales_type', 'KPR')
            // ->where('ttl_trnsct_paid', '!=', '0')
            ->where('step_sales', "$step_id")
            ->where('status', "!=", "CANCEL")
            ->orderBy('block_id', 'ASC')
            ->get();

        return DataTables::of($datas)
            ->addColumn('type_number', function($datas){
                $text = "<span class='font-weight-bold'>". $datas->block->name ." No ".  $datas->unit_id."</span> <br>";
                $text .= "<span>". $datas->location->name ."</span>";

                return $text;
            })
            ->addColumn('booking_date', function($datas){
                return date("d-m-Y", strtotime($datas->sales_date));
            })
            ->addColumn('step_sales_date_html', function($datas){
                return date("d-m-Y", strtotime($datas->step_sales_date));
            })
            ->addColumn('sales_step_desc', function($datas){
                $text = "";

                if ($datas->sales_step) {
                    $text = $datas->sales_step->step_desc;
                }

                return $text;
            })
            ->addColumn('customer', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text = "<span class='font-weight-bold'>".$datas->customer->name."</span><br>";
                    $text .= $datas->customer->institute;
                }

                return $text;
            })
            ->addColumn('customer_phone', function($datas){
                $text = "";
                if ($datas->customer_id != "") {
                    $text .= $datas->customer->phone_num;
                }

                return $text;
            })
            ->addColumn('selling_price', function($datas){
                return number_format($datas->ttl_trnsct,2,',','.');
            })
            ->addColumn('sisa', function($datas){
                return number_format($datas->ttl_trnsct - $datas->ttl_trnsct_paid,2,',','.');
            })
            ->addColumn('ttl_trnsct_paid', function($datas){
                return number_format($datas->ttl_trnsct_paid,2,',','.');
            })
            ->addColumn('actions_link', function($datas)
            {
                // update link
                $btn = "<a href='" . route('prop.sales.detail', $datas->id) . "' class='btn btn-primary btn-xs' target='_blank'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                // print sppr link
                $btn .= "<a href='" . route('prop.sales.print_sppr', $datas->id) . "' target='_blank' class='btn btn-info btn-xs' target='_blank'>";
                $btn .= "<i class='fas fa-print'></i> Cetak SPPR";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['type_number', 'customer', 'customer_phone', 'selling_price', 'sisa', 'ttl_trnsct_pai', 'actions_link', 'sales_step_desc', 'booking_date', 'step_sales_date_html'])
            ->toJson();
    }

    public function operasional()
    {
        $total = 0;
        $datas = \App\ModelFinance\Trnsct::with('sdm')
            ->where('finance_amount_paid', "<", DB::raw('finance_amount'))
            ->where('finance_cancel', '0')
            ->orderBy('finance_date', 'ASC');

        $total = $datas->sum('finance_amount') - $datas->sum('finance_amount_paid');
        $datas = $datas->paginate(10);

        return view('index.view_operasional', compact('datas', 'total'));
    }

    public function resumeUnitReady()
    {
        $datas = DB::table('prop_ref_unit')
            ->rightJoin('prop_ref_block', function($join) {
                $join->on('prop_ref_unit.location_id', '=', 'prop_ref_block.location_id');
                $join->on('prop_ref_unit.block_id', '=', 'prop_ref_block.block_id');
            })
            ->leftJoin('prop_ref_location', 'prop_ref_block.location_id', '=', 'prop_ref_location.location_id')
            ->select('prop_ref_location.location_id', 'prop_ref_location.name as location_name',
                    'prop_ref_block.block_id', 'prop_ref_block.name as block_name',
                    DB::raw('COUNT(prop_ref_unit.unit_id) as jml'))
            ->where('prop_ref_unit.status', 'READY')
            ->groupBy('prop_ref_unit.location_id', 'prop_ref_unit.block_id')
            ->orderBy('prop_ref_location.name', 'ASC')
            ->orderBy('prop_ref_block.name', 'ASC')
            ->get();

        return DataTables::of($datas)
            ->addColumn('html_block', function($datas){
                $btn = "<a href='" . url('/prop/sales/list_unit_second/' . $datas->location_id . '/' . $datas->block_id) . "' target='_blank' target='_blank'>";
                $btn .= $datas->block_name;
                $btn .= "</a>";

                return $btn;
            })
            ->rawColumns(['html_block'])
            ->toJson();
    }


    public function dashboardProgress(Request $request)
    {
        $ref_step = \App\ModelProp\RefSalesStep::where('type', 'KPR')
            ->where('is_active', 'Y')
            ->where(function ($query) {
                $query->where('step_id', '!=', 6)
                    ->where('step_id', '!=', 16)
                    ->where('step_id', '!=', 20)
                    ->where('step_id', '!=', 21)
                    ->where('step_id', '!=', 24);
            })
            ->orderBy('orders', 'ASC')
            ->get();

        $ref_block = \App\ModelProp\RefBlock::with(with(['units' => function($query){
                $query->where('status', 'READY');
            }, 'location']))
            ->where('is_Active', 'Y')
            ->orderBy('location_id')
            ->orderBy('name')
            ->get();

        if ($request->isMethod('POST')) {
            \App\ModelProp\RefFilterProgress::truncate();

            foreach ($request->filter_blocks as $key => $value) {
                $new = new \App\ModelProp\RefFilterProgress();
                $new->type = 'block';
                $new->id = $value;
                $new->save();
            }

            foreach ($request->filter_steps as $key => $value) {
                $new = new \App\ModelProp\RefFilterProgress();
                $new->type = 'step';
                $new->id = $value;
                $new->save();
            }
        }

        $filter_block = \App\ModelProp\RefFilterProgress::where('type', 'block')->get();
        $filter_step = \App\ModelProp\RefFilterProgress::where('type', 'step')->get();

        $filter_steps = [];
        $filter_blocks = [];
        $key_steps = [];
        $key_blocks = [];

        foreach ($filter_step as $key => $value) {
            $filter_steps[] = $value->id;
            $key_steps[$value->id] = null;
        }

        foreach ($filter_block as $key => $value) {
            $filter_blocks[] = $value->id;
            $key_blocks[$value->id] = null;
        }

        $steps = \App\ModelProp\RefSalesStep::with(['sales' => function ($query){
                    $query->where('sales_type', 'KPR');
                    $query->where('ttl_trnsct_paid', '!=', '0');
                    $query->where('status', '!=', 'CANCEL');
                    $query->orderBy('block_id');
                    $query->orderBy('unit_id');
                },
                'sales.sales_step',
                'sales.customer',
                'sales.location',
                'sales.block',
                'sales.unit'
            ])
            ->whereIn('step_id', $filter_steps)
            ->get();

        $blocks = DB::table('prop_ref_unit')
            ->rightJoin('prop_ref_block', function($join) {
                $join->on('prop_ref_unit.location_id', '=', 'prop_ref_block.location_id');
                $join->on('prop_ref_unit.block_id', '=', 'prop_ref_block.block_id');
            })
            ->leftJoin('prop_ref_location', 'prop_ref_block.location_id', '=', 'prop_ref_location.location_id')
            ->select('prop_ref_location.location_id', 'prop_ref_location.name as location_name',
                    'prop_ref_block.block_id', 'prop_ref_block.name as block_name',
                    DB::raw('COUNT(prop_ref_unit.unit_id) as jml'))
            ->where('prop_ref_unit.status', 'READY')
            ->whereIn('prop_ref_block.block_id', $filter_blocks)
            ->groupBy('prop_ref_unit.location_id', 'prop_ref_unit.block_id')
            ->orderBy('prop_ref_location.name', 'ASC')
            ->orderBy('prop_ref_block.name', 'ASC')
            ->get();

        $units = \App\ModelProp\RefUnit::where('status', 'READY')->whereIn('block_id', $filter_blocks)->get();

        $res_units = [];
        foreach ($units as $key => $value) {
            $res_units[$value->block_id][] = $value->number;
        }

        return view('index.progress', compact('ref_step', 'steps', 'ref_block', 'blocks', 'key_steps', 'key_blocks', 'res_units'));
    }

    public function listBooking(Request $request)
    {
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $datas = TrnsctSales::with(['customer', 'location', 'block', 'marketer', 'marketer.sdm'])
            ->where('status', '!=', 'CANCEL')
            ->whereBetween('sales_date', [$start_date, $end_date])
            ->orderBy('sales_date', 'ASC')
            ->paginate(15);

        return view('index.view_list_booking', compact('datas'));  
    }
}
