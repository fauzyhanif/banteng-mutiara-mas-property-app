<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefAccountGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_account_group.index');
    }

    public function listData()
    {
        $datas = \App\ModelFinance\RefAccountGroup::all();
        return DataTables::of($datas)
            ->addColumn('actions_link', function($datas)
            {
                // edits link
                $btn = "<a href='" . route('finance.account_group.form_update', $datas->group_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('finance.ref_account_group.form_store');
    }

    public function store(Request $request)
    {
        $request->validate([
            'group_id'=>'required',
            'name'=>'required',
        ]);

        $new = new \App\ModelFinance\RefAccountGroup();
        $new->group_id = $request->get('group_id');
        $new->name = $request->get('name');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formUpdate($id)
    {
        $group = \App\ModelFinance\RefAccountGroup::findOrFail($id);

        return view('finance.ref_account_group.form_update', \compact('group'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'group_id'=>'required',
            'name'=>'required',
        ]);

        $new = \App\ModelFinance\RefAccountGroup::findOrFail($id);
        $new->group_id = $request->get('group_id');
        $new->name = $request->get('name');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
