<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefCashBankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_cash_bank.index');
    }

    public function listData()
    {
        $cashs = \App\ModelFinance\RefCashBank::orderBy('cash_name', 'ASC')->get();

        return DataTables::of($cashs)
            ->editColumn('cash_name', function($cashs)
            {
                return $cashs->cash_name;
            })
            ->addColumn('actions_link', function($cashs)
            {
                // edits link
                $btn = "<a href='" . route('finance.cash_bank.form_update', $cashs->cash_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                // cashflow
                $btn .= "<a href='" . route('finance.cash_bank.cashflow', $cashs->cash_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-exchange-alt'></i> Cashflow";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('finance.ref_cash_bank.form_store');
    }


    public function store(Request $request)
    {
        $request->validate([
            'cash_name'=>'required',
        ]);

        $new = new \App\ModelFinance\RefCashBank();
        $new->cash_name = $request->get('cash_name');

            if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $cash = \App\ModelFinance\RefCashBank::where("cash_id", "$id")->first();

        return view('finance.ref_cash_bank.form_update', \compact(
            'cash'
        ));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'cash_name'=>'required',
        ]);

        $new = \App\ModelFinance\RefCashBank::findOrFail($id);
        $new->cash_name = $request->get('cash_name');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function cashflow(Request $request, $cash_id)
    {
        $start_date = ($request->has('date')) ? explode(" - ", $request->input('date'))[0] : date('Y-m-01');
        $end_date = ($request->has('date')) ? explode(" - ", $request->input('date'))[1] : date('Y-m-d');

        return view('finance.ref_cash_bank.cashflow', compact('start_date', 'end_date', 'cash_id'));
    }

    public function cashflowTotal(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        // uang keluar
        $uang_keluar = 0;
        $cashflow_kasbon = $this->getCashflowKasbon($cash_id, $start_date, $end_date);
        $cashflow_operasional = $this->getCashflowOperasional($cash_id, $start_date, $end_date);
        $cashflow_penjualan_return = $this->getCashflowPenjualanReturn($cash_id, $start_date, $end_date);
        $uang_keluar = $cashflow_kasbon->sum("kasbon_amount") + $cashflow_operasional->sum("finance_paid_amount") + $cashflow_penjualan_return->sum('sales_return_amount');

        // uang masuk
        $uang_masuk = 0;
        $cashflow_kasbon_paid = $this->getCashflowKasbonPaid($cash_id, $start_date, $end_date);
        $cashflow_penjualan = $this->getCashflowPenjualan($cash_id, $start_date, $end_date);
        $uang_masuk = $cashflow_kasbon_paid->sum("kasbon_paid_amount") + $cashflow_penjualan->sum("sales_paid_amount");

        $arr = [
            "uang_masuk" => number_format($uang_masuk,0,',','.'),
            "uang_keluar" => number_format($uang_keluar,0,',','.'),
        ];

        return response()->json($arr);
    }

    public function cashflowKasbon(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $result = [];
        $cashflow_kasbon = $this->getCashflowKasbon($cash_id, $start_date, $end_date);
        $cashflow_kasbon_paid = $this->getCashflowKasbonPaid($cash_id, $start_date, $end_date);

        foreach ($cashflow_kasbon as $key => $value) {
            $result[] = [
                "date" => date("d/m/Y", strtotime($value->kasbon_date)),
                "name" => $value->sdm->name,
                "desc" => "Kasbon",
                "uang_keluar" => number_format($value->kasbon_amount,0,',','.'),
                "uang_masuk" => "0"
            ];
        }

        foreach ($cashflow_kasbon_paid as $key => $value) {
            $result[] = [
                "date" => date("d/m/Y", strtotime($value->kasbon_paid_date)),
                "name" => $value->kasbon->sdm->name,
                "desc" => "Pembayaran Kasbon",
                "uang_keluar" => "0",
                "uang_masuk" => number_format($value->kasbon_paid_amount,0,',','.'),
            ];
        }

        $result = collect($result)->sortBy('date')->reverse()->toArray();

        return DataTables::of($result)->toJson();
    }

    public function cashflowKasbonTotal(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $cashflow_kasbon = $this->getCashflowKasbon($cash_id, $start_date, $end_date);
        $cashflow_kasbon_paid = $this->getCashflowKasbonPaid($cash_id, $start_date, $end_date);

        $arr = [
            "uang_keluar" => number_format($cashflow_kasbon->sum('kasbon_amount'),0,',','.'),
            "uang_masuk" => number_format($cashflow_kasbon_paid->sum('kasbon_paid_amount'),0,',','.'),
        ];

        return response()->json($arr);
    }

    public function cashflowPenjualan(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $result = [];
        $sales = $this->getCashflowPenjualan($cash_id, $start_date, $end_date);
        $sales_return = $this->getCashflowPenjualanReturn($cash_id, $start_date, $end_date);

        foreach ($sales as $key => $value) {
            $result[] = [
                "date" => date("d/m/Y", strtotime($value->sales_paid_date)),
                "name" => $value->sales_paid_person,
                "desc" => $value->sales_paid_desc,
                "uang_masuk" => number_format($value->sales_paid_amount,0,',','.'),
                "uang_keluar" => "0"
            ];
        }

        foreach ($sales_return as $key => $value) {
            $result[] = [
                "date" => date("d/m/Y", strtotime($value->sales_return_date)),
                "name" => $value->sales_return_person,
                "desc" => $value->sales_return_desc,
                "uang_keluar" => number_format($value->sales_return_amount,0,',','.'),
                "uang_masuk" => "0"
            ];
        }

        $result = collect($result)->sortBy('date')->reverse()->toArray();

        return DataTables::of($result)->toJson();
    }

    public function cashflowPenjualanTotal(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $sales = $this->getCashflowPenjualan($start_date, $end_date);
        $return = $this->getCashflowPenjualanReturn($start_date, $end_date);

        $arr = [
        "uang_masuk" => number_format($sales->sum('sales_paid_amount'),0,',','.'),
        "uang_keluar" => number_format($return->sum('sales_return_amount'),0,',','.'),
        ];

        return response()->json($arr);
    }

    public function cashflowOperasional(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $result = [];
        $operasionals = $this->getCashflowOperasional($cash_id, $start_date, $end_date);

        foreach ($operasionals as $key => $value) {
            $result[] = [
                "date" => date("d/m/Y", strtotime($value->finance_paid_date)),
                "name" => $value->sdm->name,
                "desc" => $value->trnsct->finance_desc,
                "uang_keluar" => number_format($value->finance_paid_amount,0,',','.'),
                "uang_masuk" => "0"
            ];
        }

        return DataTables::of($result)->toJson();
    }

    public function cashflowOperasionalTotal(Request $request)
    {
        $cash_id = $request->get('cash_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $operasional = $this->getCashflowOperasional($cash_id, $start_date, $end_date);

        $arr = [
            "uang_keluar" => number_format($operasional->sum('finance_paid_amount'),0,',','.'),
            "uang_masuk" => "0.00",
        ];

        return response()->json($arr);
    }

    public function getCashflowKasbon($cash_id, $start_date, $end_date)
    {
        $result = \App\ModelFinance\Kasbon::with('sdm')
            ->where("kasbon_cash_id", $cash_id)
            ->where("kasbon_cancel", "0")
            ->whereBetween("kasbon_date", [$start_date, $end_date])
            ->get();

        return $result;
    }

    public function getCashflowKasbonPaid($cash_id, $start_date, $end_date)
    {
        $result = \App\ModelFinance\KasbonPaid::with('kasbon.sdm')
            ->where("kasbon_paid_cash_id", $cash_id)
            ->where("kasbon_paid_cancel", "0")
            ->whereBetween("kasbon_paid_date", [$start_date, $end_date])
            ->get();

        return $result;
    }

    public function getCashflowPenjualan($cash_id, $start_date, $end_date)
    {
        $result = \App\ModelProp\TrnsctSalesPaid::with('sales')
            ->where("sales_paid_cash_id", $cash_id)
            ->where("sales_paid_cancel", "0")
            ->whereBetween("sales_paid_date", [$start_date, $end_date])
            ->orderBy("sales_paid_date", "DESC")
            ->get();

        return $result;
    }

    public function getCashflowPenjualanReturn($cash_id, $start_date, $end_date)
    {
        $result = \App\ModelProp\TrnsctSalesReturn::where("sales_return_cash_id", $cash_id)
            ->where("sales_return_cancel", "0")
            ->whereBetween("sales_return_date", [$start_date, $end_date])
            ->orderBy("sales_return_date", "DESC")
            ->get();

        return $result;
    }

    public function getCashflowOperasional($cash_id, $start_date, $end_date)
    {
        $result = \App\ModelFinance\TrnsctPaid::with('trnsct', 'sdm')
            ->where("finance_paid_cash_id", $cash_id)
            ->where("finance_paid_cancel", "0")
            ->whereBetween("finance_paid_date", [$start_date, $end_date])
            ->get();

        return $result;
    }


}
