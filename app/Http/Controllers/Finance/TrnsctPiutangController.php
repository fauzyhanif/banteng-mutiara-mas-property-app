<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class TrnsctPiutangController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->middleware('auth');
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index(Request $request)
    {
        $status = ($request->has('status')) ? $request->input('status') : 1;
        return view('finance.trnsct_piutang.index', compact('status'));
    }

    public function listData(Request $request)
    {
        $status = $request->get('status');
        if ($status == 1) { // piutang baru
            $datas = $this->listDataBaru();
        } elseif ($status == 2) { // sebagian bayar
            $datas = $this->listDataSebagianBayar();
        } elseif ($status == 3) { // selesai
            $datas = $this->listDataSelesai();
        } elseif ($status == 4) { // dibatalkan
            $datas = $this->listDataBatal();
        }

        return DataTables::of($datas)
            ->addColumn('date', function($datas)
            {
                $text = date("d/m/Y", strtotime($datas->kasbon_date));
                return $text;
            })
            ->addColumn('sisa', function($datas)
            {
                return number_format($datas->kasbon_amount - $datas->kasbon_amount_paid,2,',','.');;
            })
            ->addColumn('actions_link', function($datas)
            {
                // edits link
                $btn = "<a href='" . route('finance.piutang.detail', $datas->kasbon_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i>";
                $btn .= "</a> ";

                // prints link
                $btn .= "<a href='" . route('finance.piutang.print_kasbon', $datas->kasbon_id) . "' class='btn bg-purple btn-xs' target='_blank'>";
                $btn .= "<i class='fas fa-print'></i>";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'date', 'sisa'])
            ->toJson();
    }

    public function listDataBaru()
    {
        $datas = \App\ModelFinance\Kasbon::with('sdm')
            ->where('kasbon_amount_paid', "=", '0')
            ->where('kasbon_cancel', '0')
            ->orderBy('kasbon_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataSebagianBayar()
    {
        $datas = \App\ModelFinance\Kasbon::with('sdm')
            ->where('kasbon_amount_paid', ">", '0')
            ->where('kasbon_amount_paid', "<", DB::raw('kasbon_amount'))
            ->where('kasbon_cancel', '0')
            ->orderBy('kasbon_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataSelesai()
    {
        $datas = \App\ModelFinance\Kasbon::with('sdm')
            ->where('kasbon_amount', DB::raw('kasbon_amount_paid'))
            ->where('kasbon_cancel', '0')
            ->orderBy('kasbon_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataBatal()
    {
       $datas = \App\ModelFinance\Kasbon::with('sdm')
            ->where('kasbon_cancel', '1')
            ->orderBy('kasbon_date', 'ASC')
            ->get();

        return $datas;
    }

    public function formStore(Request $request)
    {
        $sdm = "";
        $accounts = "";
        $ttl_tabungan = 0;
        $ttl_kasbon = 0;
        if ($request->isMethod('post')) {
            $sdm_id = $request->get('search');
            $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);

            $tabungan = DB::table('fin_trnsct')
                ->select(DB::raw('SUM(finance_amount) as ttl_amount, SUM(finance_amount_paid) as ttl_amount_paid'))
                ->where('finance_sdm_id', $sdm_id)
                ->where('finance_type', 'PENGELUARAN')
                ->where('finance_cancel', '0')
                ->where('finance_amount', '>', 'finance_amount_paid')
                ->groupBy('finance_sdm_id')
                ->get();

            if (count($tabungan) > 0) {
                $ttl_tabungan = $tabungan[0]->ttl_amount - $tabungan[0]->ttl_amount_paid;
            }

            $kasbon = DB::table('fin_kasbon')
                ->select(DB::raw('SUM(kasbon_amount) as ttl_amount, SUM(kasbon_amount_paid) as ttl_amount_paid'))
                ->where('kasbon_sdm_id', $sdm_id)
                ->where('kasbon_amount', '>', 'kasbon_amount_paid')
                ->where('kasbon_cancel', '0')
                ->groupBy('kasbon_sdm_id')
                ->get();

            if (count($kasbon) > 0) {
                $ttl_kasbon = $kasbon[0]->ttl_amount - $kasbon[0]->ttl_amount_paid;
            }

            $accounts = \App\ModelFinance\RefCashBank::orderBy('cash_name', 'ASC')->get();

        }
        return view('finance.trnsct_piutang.form_store', compact('sdm', 'ttl_tabungan', 'ttl_kasbon', 'accounts'));
    }

    public function store(Request $request)
    {
        $new = new \App\ModelFinance\Kasbon();
        $new->kasbon_sdm_id = $request->get('sdm_id');
        $new->kasbon_cash_id = $request->get('account_id');
        $new->kasbon_date = $request->get('trnsct_date');
        $new->kasbon_desc = $request->get('description');
        $new->kasbon_amount = str_replace('.', '', $request->get('amount'));

        if ($new->save())
        {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sdm_id' => $request->get('sdm_id'),
                'kasbon_id' => $new->kasbon_id
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formPay(Request $request)
    {
        $sdm = "";
        $piutangs = "";
        $accounts = "";

        if ($request->isMethod('post')) {
            $sdm_id = $request->get('search');
            $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);

            $accounts = \App\ModelFinance\RefCashBank::orderBy('cash_name', 'ASC')->get();

            $piutangs = \App\ModelFinance\Kasbon::where('kasbon_sdm_id', $sdm_id)
                ->where('kasbon_cancel', '0')
                ->where('kasbon_amount', '>', 'kasbon_amount_paid')
                ->orderBy('kasbon_date', 'ASC')
                ->get();
        }

        return view('finance.trnsct_piutang.form_pay', compact('sdm', 'piutangs', 'accounts'));
    }

    public function pay(Request $request)
    {
        $count_piutang_success = 0;
        foreach ($request->get('amount') as $key => $value) {
            if ($value != 0) {
                $piutang = \App\ModelFinance\Kasbon::findOrFail($key);
                $piutang->kasbon_amount_paid += str_replace('.', '', $value);

                if ($piutang->save()) {
                    $count_piutang_success += 1;

                    $kasbon_paid = new \App\ModelFinance\KasbonPaid();
                    $kasbon_paid->kasbon_id = $key;
                    $kasbon_paid->kasbon_paid_date = $request->get('trnsct_date');
                    $kasbon_paid->kasbon_paid_cash_id = $request->get('account_id');
                    $kasbon_paid->kasbon_paid_amount = str_replace('.', '', $value);
                    $kasbon_paid->save();

                }
            }
        }

        if ($count_piutang_success > 0) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sdm_id' => $request->get('sdm_id')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function detail($id)
    {
        $piutang = \App\ModelFinance\Kasbon::with('sdm')->where('kasbon_id', $id)->first();

        $payments = \App\ModelFinance\KasbonPaid::with('cash_bank')
                ->where('kasbon_id', $id)
                ->orderBy('kasbon_paid_date', 'ASC')
                ->get();

        return view('finance.trnsct_piutang.detail', compact('piutang', 'payments'));
    }

    public function printInvoice($jurnal_id)
    {
        $payment = \App\ModelFinance\TrnsctJurnal::with(['sales'])->findOrFail($jurnal_id);
        return view('finance.trnsct_piutang.print_invoice', compact('payment'));
    }

    public function printKasbon($id)
    {
        $kasbon = \App\ModelFinance\Kasbon::with('sdm')->where('kasbon_id', $id)->first();

        return view('finance.trnsct_piutang.print_kasbon', compact('kasbon'));
    }

    public function cancel($kasbon_id)
    {
        $kasbon = \App\ModelFinance\Kasbon::findOrFail($kasbon_id);
        $kasbon->kasbon_cancel = '1';
        $kasbon->save();

        $kasbon_paid = \App\ModelFinance\KasbonPaid::where("kasbon_id", $kasbon_id)->update(["kasbon_paid_cancel" => '1']);

        return redirect()->back();
    }
}
