<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;

class TrnsctJurnalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view("finance.trnsct_jurnal.index");
    }

    public function listData()
    {
        $datas = \App\ModelFinance\TrnsctJurnal::orderBy('jurnal_id', 'DESC')->get();
        return DataTables::of($datas)
            ->editColumn('trnsct_date', function($datas){
                return date("d/m/Y", strtotime($datas->trnsct_date));
            })
            ->editColumn('amount', function($datas){
                return number_format($datas->amount,2,',','.');
            })
            ->addColumn('actions_link', function($datas)
            {
                // detail link
                $btn = "<a href='" . route('finance.jurnal.detail', $datas->id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i>";
                $btn .= "</a> ";

                // edits link
                $btn .= "<a href='" . route('finance.jurnal.form_update', $datas->id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i>";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function detail($jurnal_id)
    {
        $jurnal_header = \App\ModelFinance\TrnsctJurnal::findOrFail($jurnal_id);
        $jurnal_item = \App\ModelFinance\TrnsctJurnalItem::where('jurnal_id', "$jurnal_header->jurnal_id")->get();

        return view("finance.trnsct_jurnal.detail", compact('jurnal_header', 'jurnal_item'));
    }

    public function formStore()
    {
        $accounts = \App\ModelFinance\RefAccount::orderBy('account_id', 'ASC')->get();

        return view("finance.trnsct_jurnal.form_store", compact('accounts'));
    }

    public function mainStore(Request $request)
    {
        $posts = $request->all();
        $posts['sdm_name'] = '-';
        $posts['hutang_piutang_id'] = '';
        $posts['hutang_piutang_type'] = '';
        for ($i=0; $i < count($posts['account_id']); $i++) {
            $posts['jurnal_item'][$i]['account_id'] = $posts['account_id'][$i];
            $posts['jurnal_item'][$i]['debit'] = str_replace(".", "", $posts['debit'][$i]);
            $posts['jurnal_item'][$i]['credit'] = str_replace(".", "", $posts['credit'][$i]);
        }

        $res_from_store = $this->store($posts);
        if ($res_from_store['status'] == true) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'jurnal_id' => $res_from_store['id']
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function store($request)
    {
        $res_store_header = $this->storeHeader($request);
        if ($res_store_header['status'] == true) {
            $this->storeItem($request['jurnal_item'], $res_store_header['jurnal_id']);
        }

        return $res_store_header;
    }

    public function storeHeader($request)
    {
        // get invoice number
        $invoice_num = $this->invoiceNumber($request['flow']);

        $new = new \App\ModelFinance\TrnsctJurnal();
        $new->jurnal_id = $invoice_num;
        $new->trnsct_date = $request['trnsct_date'];
        $new->related_person = $request['sdm_name'];
        $new->description = $request['description'];
        $new->amount = $request['amount'];
        $new->flow = $request['flow'];
        $new->hutang_piutang_id = (isset($request['hutang_piutang_id'])) ? $request['hutang_piutang_id'] : '';
        $new->hutang_piutang_type = (isset($request['hutang_piutang_type'])) ? $request['hutang_piutang_type'] : '';
        $new->sales_id = (isset($request['sales_id'])) ? $request['sales_id'] : '';
        $new->sales_location_id = (isset($request['sales_location_id'])) ? $request['sales_location_id'] : '';
        $new->sales_block_id = (isset($request['sales_block_id'])) ? $request['sales_block_id'] : '';
        $new->sales_unit_id = (isset($request['sales_unit_id'])) ? $request['sales_unit_id'] : '';
        $new->sales_affiliate_num = (isset($request['sales_affiliate_num'])) ? $request['sales_affiliate_num'] : '';
        $new->sales_kpr_credit_in = (isset($request['sales_kpr_credit_in'])) ? $request['sales_kpr_credit_in'] : 'N';
        $new->user = Session::get('auth_nama');

        $res = [];
        if ($new->save()) {
            $res = [
                'status' => true,
                'jurnal_id' => $new->jurnal_id
            ];
        } else {
            $res = [
                'status' => false,
                'jurnal_id' => null
            ];
        }

        return $res;
    }

    public function storeItem($request, $jurnal_id)
    {
        for ($i=0; $i < count($request); $i++) {
            $new = new \App\ModelFinance\TrnsctJurnalItem();
            $new->jurnal_id = $jurnal_id;
            $new->account_id = $request[$i]['account_id'];
            $new->debit = $request[$i]['debit'];
            $new->credit = $request[$i]['credit'];
            $new->save();
        }
    }

    public function invoiceNumber($type)
    {
        $this_month = date('Y-m');
        $booking_id = 'INV/'.$type.'/';

        $last_invoice = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
            ->where('type', "$type")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_invoice) == 0 ) {
            $order_id = "0001";

            $new = new \App\ModelFinance\RefInvoiceNumber();
            $new->type = $type;
            $new->month = "$this_month";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $order_id = sprintf('%04d', $last_invoice[0]->serial_num);
            $update = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
                ->where("type", "$type")
                ->update(["serial_num" => $order_id + 1]);
        }

        $booking_id .= date('Y/m/') . $order_id;
        return $booking_id;
    }

    public function formUpdate($id)
    {
        $jurnal_header = \App\ModelFinance\TrnsctJurnal::findOrFail($id);
        $jurnal_item = \App\ModelFinance\TrnsctJurnalItem::where('jurnal_id', "$jurnal_header->jurnal_id")->get();
        $accounts = \App\ModelFinance\RefAccount::orderBy('account_id', 'ASC')->get();

        return view("finance.trnsct_jurnal.form_update", compact('jurnal_header', 'jurnal_item', 'accounts'));
    }

    public function update(Request $request, $jurnal_header_id)
    {
        $jurnal_header = \App\ModelFinance\TrnsctJurnal::findOrFail($jurnal_header_id);
        $jurnal_header->flow = $request->get('flow');
        $jurnal_header->description = $request->get('description');
        $jurnal_header->amount = $request->get('amount');

        if ($jurnal_header->save()) {
            foreach ($request->get('account_id_old') as $key => $value) {
                $jurnal_item = \App\ModelFinance\TrnsctJurnalItem::findOrFail($key);
                $jurnal_item->account_id = $request->get('account_id_old')[$key];
                $jurnal_item->debit = str_replace(".", "", $request->get('debit_old')[$key]);
                $jurnal_item->credit = str_replace(".", "", $request->get('credit_old')[$key]);
                $jurnal_item->save();
            }

            if ($request->has('account_id')) {
                foreach ($request->get('account_id') as $key => $value) {
                    $jurnal_item = new \App\ModelFinance\TrnsctJurnalItem();
                    $jurnal_item->jurnal_id = $request->get('jurnal_id');
                    $jurnal_item->account_id = $request->get('account_id')[$key];
                    $jurnal_item->debit = str_replace(".", "", $request->get('debit')[$key]);
                    $jurnal_item->credit = str_replace(".", "", $request->get('credit')[$key]);
                    $jurnal_item->save();
                }
            }

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'jurnal_id' => $jurnal_header->id
            ];
        } else {
            $res = [
                'status' => false,
                'text' => 'Item gagal dihapus',
                'jurnal_id' => null
            ];
        }

        return response()->json($res);
    }

    public function removeJurnalItem(Request $request)
    {
        $id = $request->get('jurnalitem_id');
        $jurnal_item = \App\ModelFinance\TrnsctJurnalItem::findOrFail($id);
        $jurnal_header = \App\ModelFinance\TrnsctJurnal::where("jurnal_id", "$jurnal_item->jurnal_id")->first();

        if ($jurnal_item->delete()) {
            $res = [
                'status' => true,
                'text' => 'Item berhasil dihapus',
                'jurnal_id' => $jurnal_header->id
            ];
        } else {
            $res = [
                'status' => false,
                'text' => 'Item gagal dihapus',
                'jurnal_id' => null
            ];
        }

        return response()->json($res);
    }
}
