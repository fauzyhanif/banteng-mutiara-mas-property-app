<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class RefDebtToBankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_debt_to_bank.index');
    }

    public function listData()
    {
        $debts = \App\ModelFinance\RefDebtToBank::with('sdm')->orderBy('sdm_id', 'ASC')->get();

        return DataTables::of($debts)
            ->addColumn('debt_total_html', function($debts)
            {
                return number_format($debts->debt_total,0,',','.');
            })
            ->addColumn('debt_total_paid_html', function($debts)
            {
                return number_format($debts->debt_total_paid,0,',','.');
            })
            ->addColumn('total_remainder_html', function($debts)
            {
                return number_format($debts->debt_total - $debts->debt_total_paid,0,',','.');
            })
            ->addColumn('actions_link', function($debts)
            {
                // edits link
                $btn = "<a href='" . route('finance.hutang_bank.form_update', $debts->debt_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['debt_total_html', 'debt_total_paid_html', 'total_remainder_html', 'actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        $banks = \App\ModelProp\RefBank::with('sdm')->get();

        return view('finance.ref_debt_to_bank.form_store', compact('banks'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'sdm_id'=>'required',
            'debt_desc'=>'required',
            'debt_total'=>'required',
        ]);


        $new = new \App\ModelFinance\RefDebtToBank();
        $new->sdm_id = $request->get('sdm_id');
        $new->debt_desc = $request->get('debt_desc');
        $new->debt_total = \str_replace(".","",$request->get('debt_total'));


        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $debt = \App\ModelFinance\RefDebtToBank::findOrFail($id);
        $banks = \App\ModelProp\RefBank::with('sdm')->get();

        return view('finance.ref_debt_to_bank.form_update', \compact(
            'debt',
            'banks'
        ));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'sdm_id'=>'required',
            'debt_desc'=>'required',
            'debt_total'=>'required',
        ]);

        $new = \App\ModelFinance\RefDebtToBank::findOrFail($id);
        $new->sdm_id = $request->get('sdm_id');
        $new->debt_desc = $request->get('debt_desc');
        $new->debt_total = \str_replace(".","",$request->get('debt_total'));

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

}
