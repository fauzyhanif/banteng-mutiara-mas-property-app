<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_account.index');
    }

    public function listData()
    {
        $accounts = \App\ModelFinance\RefAccount::with('group')
            ->where('active', 'Y')
            ->orderBy('account_id', 'ASC')
            ->get();

        return DataTables::of($accounts)
            ->editColumn('name', function($accounts)
            {
                return $accounts->name;
            })
            ->addColumn('actions_link', function($accounts)
            {
                // edits link
                $btn = "<a href='" . route('finance.account.form_update', $accounts->id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        $groups = \App\ModelFinance\RefAccountGroup::where('active', 'Y')->get();
        return view('finance.ref_account.form_store', compact('groups'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'group_id'=>'required',
            'account_id'=>'required',
            'name'=>'required',
            'post_report'=>'required'
        ]);

        // cek if number unit is exist
        $check_exist_account = $this->checkExistAccount($request->get('account_id'));

        if ($check_exist_account) {
            $new = new \App\ModelFinance\RefAccount();
            $new->group_id = $request->get('group_id');
            $new->account_id = $request->get('account_id');
            $new->name = $request->get('name');
            $new->is_kas_bank = $request->get('is_kas_bank');
            $new->post_report = $request->get('post_report');
            $new->cash_flow_group = $request->get('cash_flow_group');
            $new->debit = $request->get('debit');
            $new->credit = $request->get('credit');

            if ($new->save()) {
                $res = [
                    'status' => 'success',
                    'text' => 'Data berhasil disimpan',
                ];
            } else {
                $res = [
                    'status' => 'failed',
                    'text' => 'Data gagal disimpan',
                ];
            }
        } else {
            $res = [
                'status' => 'failed',
                'text' => "Nomor akun ".$request->get('account_id')." sudah ada di database"
            ];
        }

        return response()->json($res);
    }

    public function checkExistAccount($account_id)
    {
        $data = \App\ModelFinance\RefAccount::where("account_id", "$account_id")->first();

        $res = false;
        if ($data === null) {
            $res = true;
        }

        return $res;
    }

    public function formUpdate($id)
    {
        $account = \App\ModelFinance\RefAccount::findOrFail($id);
        $accounts = \App\ModelFinance\RefAccount::where("group_id", $account->group_id)
            ->where('active', 'Y')
            ->orderBy('account_id', 'ASC')
            ->get();

        $groups = \App\ModelFinance\RefAccountGroup::where('active', 'Y')->get();

        return view('finance.ref_account.form_update', \compact(
            'account',
            'accounts',
            'groups'
        ));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'group_id'=>'required',
            'account_id'=>'required',
            'name'=>'required',
            'post_report'=>'required'
        ]);

        $new = \App\ModelFinance\RefAccount::findOrFail($id);
        $new->group_id = $request->get('group_id');
        $new->name = $request->get('name');
        $new->is_kas_bank = $request->get('is_kas_bank');
        $new->post_report = $request->get('post_report');
        $new->cash_flow_group = $request->get('cash_flow_group');
        $new->debit = $request->get('debit');
        $new->credit = $request->get('credit');
        $new->active = $request->get('active');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }
}
