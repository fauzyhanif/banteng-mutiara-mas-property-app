<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Exception;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;

class TrnsctCostController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->middleware('auth');
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index(Request $request)
    {
        $status = ($request->has('status')) ? $request->input('status') : 1;
        return view('finance.trnsct_cost.index', compact('status'));
    }

    public function listData(Request $request)
    {
        $status = $request->get('status');
        if ($status == 1) { // Belum bayar
            $datas = $this->listDataBaru();
        } elseif ($status == 2) { // sebagian bayar
            $datas = $this->listDataSebagianBayar();
        } elseif ($status == 3) { // lunas
            $datas = $this->listDataSelesai();
        } elseif ($status == 4) { // dibatalkan
            $datas = $this->listDataBatal();
        }

        return DataTables::of($datas)
            ->addColumn('date', function($datas)
            {
                $text = date("d/m/Y", strtotime($datas->finance_date));
                return $text;
            })
            ->addColumn('jumlah', function($datas)
            {
                return number_format($datas->finance_amount,2,',','.');
            })
            ->addColumn('sisa', function($datas)
            {
                return number_format($datas->finance_amount - $datas->finance_amount_paid,2,',','.');;
            })
            ->addColumn('actions_link', function($datas)
            {
                $finance_date = date("d/m/Y", strtotime($datas->finance_date));
                $finance_amount = number_format($datas->finance_amount,0,',','.');
                $finance_amount_paid = number_format($datas->finance_amount_paid,0,',','.');
                $finance_user = $datas->sdm->name;

                // edits link
                $btn = "<a href='" . route('finance.cost.detail', $datas->finance_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i>";
                $btn .= "</a> ";

                // print link
                $btn .= "<button class='btn btn-primary btn-xs' onclick=\"show_list_payment('$datas->finance_id', '$finance_date', '$finance_user', '$datas->finance_desc', '$finance_amount', '$finance_amount_paid')\">";
                $btn .= "<i class='fas fa-print'></i>";
                $btn .= "</button> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'date', 'sisa', 'jumlah'])
            ->toJson();
    }

    public function listDataBaru()
    {
        $datas = \App\ModelFinance\Trnsct::with('sdm')
            ->where('finance_amount_paid', "=", '0')
            ->where('finance_cancel', '0')
            ->orderBy('finance_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataSebagianBayar()
    {
        $datas = \App\ModelFinance\Trnsct::with('sdm')
            ->where('finance_amount_paid', ">", '0')
            ->where('finance_amount_paid', "<", DB::raw('finance_amount'))
            ->where('finance_cancel', '0')
            ->orderBy('finance_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataSelesai()
    {
        $datas = \App\ModelFinance\Trnsct::with('sdm')
            ->where('finance_amount', DB::raw('finance_amount_paid'))
            ->where('finance_cancel', '0')
            ->orderBy('finance_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataBatal()
    {
       $datas = \App\ModelFinance\Trnsct::with('sdm')
            ->where('finance_cancel', '1')
            ->orderBy('finance_date', 'ASC')
            ->get();

        return $datas;
    }

    public function formStore()
    {
        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();
        $accounts = \App\ModelFinance\RefAccount::where('account_type', 'PENGELUARAN')->orderBy('account_name')->get();

        return view('finance.trnsct_cost.form_store', compact('cashes', 'accounts'));
    }

    public function store(Request $request)
    {
        $num = $this->invoiceNumber('COST');

        // store to fin_trnsct
        $finance = new \App\ModelFinance\Trnsct();
        $finance->finance_type = 'PENGELUARAN';
        $finance->finance_date = $request->get('finance_date');
        $finance->finance_sdm_id = $request->get('finance_sdm_id');
        $finance->finance_desc = $request->get('finance_desc');
        $finance->finance_amount = str_replace(".", "", $request->get('finance_amount'));
        $finance->finance_amount_paid = str_replace(".", "", $request->get('finance_amount_paid'));

        // store to fin_trnsct_item
        if ($finance->save()) {
            for ($i=0; $i < count($request->get('finance_item_account_id')); $i++) {
                $finance_item = new \App\ModelFinance\TrnsctItem();
                $finance_item->finance_id = $finance->finance_id;
                $finance_item->finance_item_account_id = $request->get('finance_item_account_id')[$i];
                $finance_item->finance_item_amount = str_replace(".", "", $request->get('finance_item_amount')[$i]);
                $finance_item->save();
            }

            // store to fin_trnsct_paid jika finance_amount_paid > 0
            if (str_replace(".", "", $request->get('finance_amount_paid')) > 0) {
                $finance_paid = new \App\ModelFinance\TrnsctPaid();
                $finance_paid->finance_id = $finance->finance_id;
                $finance_paid->finance_paid_num = $num;
                $finance_paid->finance_paid_date = $request->get('finance_date');
                $finance_paid->finance_paid_cash_id = $request->get('finance_paid_cash_id');
                $finance_paid->finance_paid_amount = str_replace(".", "", $request->get('finance_amount_paid'));
                $finance_paid->finance_paid_sdm_id = $request->get('finance_sdm_id');
                $finance_paid->save();
            }

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'finance_id' => $finance->finance_id,
            ];

        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan, terjadi kesalahan saat input.',
            ];
        }

        return response()->json($res);

    }

    public function detail($finance_id)
    {
        $finance = \App\ModelFinance\Trnsct::findOrFail($finance_id);
        $finance_items = \App\ModelFinance\TrnsctItem::with('account')->where("finance_id", $finance_id)->get();
        $finance_paids = \App\ModelFinance\TrnsctPaid::with('cash_bank')->where("finance_id", $finance_id)->where('finance_paid_cancel', '0')->get();
        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();
        $accounts = \App\ModelFinance\RefAccount::where('account_type', 'PENGELUARAN')->orderBy('account_name')->get();

        return view('finance.trnsct_cost.detail', compact('finance_id', 'finance', 'finance_items', 'finance_paids', 'cashes', 'accounts'));
    }

    public function pay(Request $request)
    {
        DB::beginTransaction();
        try {
            $num = $this->invoiceNumber('COST');

            $finance_paid = new \App\ModelFinance\TrnsctPaid();
            $finance_paid->finance_id = $request->get('finance_id');
            $finance_paid->finance_paid_num = $num;
            $finance_paid->finance_paid_date = $request->get('finance_paid_date');
            $finance_paid->finance_paid_cash_id = $request->get('finance_paid_cash_id');
            $finance_paid->finance_paid_amount = (int) str_replace('.', '', $request->get('finance_paid_amount'));
            $finance_paid->finance_paid_sdm_id = $request->get('finance_paid_sdm_id');
            $finance_paid->save();

            $total_paid = \App\ModelFinance\TrnsctPaid::where('finance_id', $request->get('finance_id'))->where('finance_paid_cancel', '0')->sum('finance_paid_amount');
            $finance = \App\ModelFinance\Trnsct::where('finance_id', $request->finance_id)->update(['finance_amount_paid' => $total_paid]);
           
            DB::commit();
            return redirect()->back();
        } catch (Exception $th) {
            DB::rollBack();
            throw new Exception($th->getMessage(), 400);
        }        
    }

    public function cancel(Request $request)
    {
        $finance = \App\ModelFinance\Trnsct::findOrFail($request->get('finance_id'));
        $finance->finance_cancel = '1';
        $finance->save();

        \App\ModelFinance\TrnsctPaid::where("finance_id", $request->get('finance_id'))->update(["finance_paid_cancel" => "1"]);

        return redirect()->back();
    }

    public function cancelPaid(Request $request)
    {
        DB::beginTransaction();
        try {
            $finance_paid = \App\ModelFinance\TrnsctPaid::findOrFail($request->get('finance_paid_id'));
            $finance_paid->finance_paid_cancel = '1';

            if ($finance_paid->save()) {
                $total_paid = \App\ModelFinance\TrnsctPaid::where('finance_id', $finance_paid->finance_id)->where('finance_paid_cancel', '0')->sum('finance_paid_amount');
                $finance = \App\ModelFinance\Trnsct::findOrFail($finance_paid->finance_id);
                $finance->finance_amount_paid = $total_paid;
                $finance->save();
            }

            DB::commit();
            return redirect()->back();
        } catch (Exception $th) {
            DB::rollBack();
            throw new Exception($th->getMessage(), 400);
        }
    }

    public function invoiceNumber($type)
    {
        $this_month = date('Y-m');
        $booking_id = 'INV/'.$type.'/';

        $last_invoice = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
            ->where('type', "$type")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_invoice) == 0 ) {
            $order_id = "0001";

            $new = new \App\ModelFinance\RefInvoiceNumber();
            $new->type = $type;
            $new->month = "$this_month";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $order_id = sprintf('%04d', $last_invoice[0]->serial_num);
            $update = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
                ->where("type", "$type")
                ->update(["serial_num" => $order_id + 1]);
        }

        $booking_id .= date('Y/m/') . $order_id;
        return $booking_id;
    }

    public function listPayments($finance_id)
    {
        $payments = \App\ModelFinance\TrnsctPaid::with("cash_bank")->where("finance_id", "$finance_id")->orderBy("finance_paid_date", "desc")->get();

        $no = 1;
        $html = "";

        foreach ($payments as $key => $value) {
            $html .= "<tr>";
            $html .= "<td>$no</td>";
            $html .= "<td>".date("d/m/Y", strtotime($value->finance_paid_date))."</td>";
            $html .= "<td>".$value->cash_bank->cash_name."</td>";
            $html .= "<td>".number_format($value->finance_paid_amount,0,',','.')."</td>";
            $html .= "<td><a href='".route('finance.cost.print_payment', $value->finance_paid_id)."' target='_blank' class='btn btn-xs btn-primary text-white'>Cetak</a></td>";

            $no = $no+1;
        }

        return $html;
    }

    public function printPayment($finance_paid_id)
    {
        $payment = \App\ModelFinance\TrnsctPaid::with("cash_bank", "trnsct", "trnsct.sales", "trnsct.sales.customer", "trnsct.sales.block", "sdm")->findOrFail($finance_paid_id);
        return view('finance.trnsct_cost.print_payment', compact('payment'));
    }

    public function updateItem(Request $request)
    {
        $id = $request->id;
        $item = \App\ModelFinance\TrnsctItem::find($id);
        $item->finance_item_account_id = $request->finance_item_account_id;
        $item->save();

        return redirect()->back();
    }
}
