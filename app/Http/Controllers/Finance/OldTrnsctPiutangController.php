<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class TrnsctPiutangController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->middleware('auth');
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index(Request $request)
    {
        $status = ($request->has('status')) ? $request->input('status') : 1;
        return view('finance.trnsct_piutang.index', compact('status'));
    }

    public function listData(Request $request)
    {
        $status = $request->get('status');
        if ($status == 1) { // piutang baru
            $datas = $this->listDataBaru();
        } elseif ($status == 2) { // sebagian bayar
            $datas = $this->listDataSebagianBayar();
        } elseif ($status == 3) { // selesai
            $datas = $this->listDataSelesai();
        } elseif ($status == 4) { // dibatalkan
            $datas = $this->listDataBatal();
        }

        return DataTables::of($datas)
            ->addColumn('sdm_name', function($datas)
            {
                return $datas->sdm->name;
            })
            ->addColumn('date', function($datas)
            {
                $text = date("d/m/Y", strtotime($datas->trnsct_date));
                $text .= "<br>Jatuh tempo : ".date("d/m/Y", strtotime($datas->trnsct_date));
                return $text;
            })
            ->addColumn('sisa', function($datas)
            {
                return number_format($datas->amount - $datas->amount_paid,2,',','.');;
            })
            ->addColumn('actions_link', function($datas)
            {
                // edits link
                $btn = "<a href='" . route('finance.piutang.detail', $datas->hutang_piutang_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'sdm_name', 'date', 'sisa'])
            ->toJson();
    }

    public function listDataBaru()
    {
        $datas = \App\ModelFinance\TrnsctHutangPiutang::with('sdm')
            ->where('type', 'PIUTANG')
            ->where('status', 'OK')
            ->where('amount_paid', "=", '0')
            ->orderBy('trnsct_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataSebagianBayar()
    {
        $datas = \App\ModelFinance\TrnsctHutangPiutang::with('sdm')
            ->where('type', 'PIUTANG')
            ->where('status', 'OK')
            ->where('amount_paid', ">", '0')
            ->where('amount_paid', "<", DB::raw('amount'))
            ->orderBy('trnsct_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataSelesai()
    {
        $datas = \App\ModelFinance\TrnsctHutangPiutang::with('sdm')
            ->where('type', 'PIUTANG')
            ->where('amount', DB::raw('amount_paid'))
            ->where('status', 'OK')
            ->orderBy('trnsct_date', 'ASC')
            ->get();

        return $datas;
    }

    public function listDataBatal()
    {
        $datas = \App\ModelFinance\TrnsctHutangPiutang::with('sdm')
            ->where('type', 'PIUTANG')
            ->where('status', 'CANCEL')
            ->orderBy('trnsct_date', 'ASC')
            ->get();

        return $datas;
    }

    public function formStore(Request $request)
    {
        $sdm = "";
        $accounts = "";
        $ttl_tabungan = 0;
        $ttl_kasbon = 0;
        if ($request->isMethod('post')) {
            $sdm_id = $request->get('search');
            $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);

            $tabungan = DB::table('fin_trnsct_hutang_piutang')
                ->select(DB::raw('SUM(amount) as ttl_amount, SUM(amount_paid) as ttl_amount_paid'))
                ->where('sdm_id', $sdm_id)
                ->where('type', 'HUTANG')
                ->where('amount', '>', 'amount_paid')
                ->groupBy('sdm_id')
                ->get();

            if (count($tabungan) > 0) {
                $ttl_tabungan = $tabungan[0]->ttl_amount - $tabungan[0]->ttl_amount_paid;
            }

            $kasbon = DB::table('fin_trnsct_hutang_piutang')
                ->select(DB::raw('SUM(amount) as ttl_amount, SUM(amount_paid) as ttl_amount_paid'))
                ->where('sdm_id', $sdm_id)
                ->where('type', 'PIUTANG')
                ->where('amount', '>', 'amount_paid')
                ->groupBy('sdm_id')
                ->get();

            if (count($kasbon) > 0) {
                $ttl_kasbon = $kasbon[0]->ttl_amount - $kasbon[0]->ttl_amount_paid;
            }

            $accounts = \App\ModelFinance\RefAccount::where('is_kas_bank', 'Y')
                ->orderBy('account_id', 'ASC')
                ->get();

        }
        return view('finance.trnsct_piutang.form_store', compact('sdm', 'ttl_tabungan', 'ttl_kasbon', 'accounts'));
    }

    public function store(Request $request)
    {
        $new = new \App\ModelFinance\TrnsctHutangPiutang();
        $new->type = 'PIUTANG';
        $new->sdm_id = $request->get('sdm_id');
        $new->trnsct_date = $request->get('trnsct_date');
        $new->due_date = $request->get('due_date');
        $new->description = $request->get('description');
        $new->amount = str_replace('.', '', $request->get('amount'));

        if ($new->save()) {

            // prepare data for jurnal
            $data_post['sdm_id'] = $request->get('sdm_id');
            $data_post['sdm_name'] = $request->get('sdm_name');
            $data_post['trnsct_date'] = $request->get('trnsct_date');
            $data_post['description'] = $request->get('description');
            $data_post['amount'] = str_replace('.', '', $request->get('amount'));
            $data_post['flow'] = 'OUT';
            $data_post['hutang_piutang_id'] = $new->hutang_piutang_id;
            $data_post['hutang_piutang_type'] = 'PIUTANG';
            $data_post['jurnal_item'][0]['account_id'] = $request->get('account_id');
            $data_post['jurnal_item'][0]['credit'] = 0;
            $data_post['jurnal_item'][0]['debit'] = str_replace('.', '', $request->get('amount'));
            $data_post['jurnal_item'][1]['account_id'] = '01.04';
            $data_post['jurnal_item'][1]['credit'] = str_replace('.', '', $request->get('amount'));
            $data_post['jurnal_item'][1]['debit'] = 0;

            $this->TrnsctJurnalController->store($data_post);

            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sdm_id' => $request->get('sdm_id')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formPay(Request $request)
    {
        $sdm = "";
        $piutangs = "";
        $accounts = "";

        if ($request->isMethod('post')) {
            $sdm_id = $request->get('search');
            $sdm = \App\ModelProp\RefSdm::findOrFail($sdm_id);
            $accounts = \App\ModelFinance\RefAccount::where('is_kas_bank', 'Y')
                ->orderBy('account_id', 'ASC')
                ->get();

            $piutangs = \App\ModelFinance\TrnsctHutangPiutang::where('sdm_id', $sdm_id)
                ->where('type', 'PIUTANG')
                ->where('status', 'OK')
                ->where('amount', '>', 'amount_paid')
                ->orderBy('trnsct_date', 'ASC')
                ->get();
        }

        return view('finance.trnsct_piutang.form_pay', compact('sdm', 'piutangs', 'accounts'));
    }

    public function pay(Request $request)
    {
        $count_piutang_success = 0;
        foreach ($request->get('amount') as $key => $value) {
            if ($value != 0) {
                $piutang = \App\ModelFinance\TrnsctHutangPiutang::findOrFail($key);
                $piutang->amount_paid += str_replace('.', '', $value);

                if ($piutang->save()) {
                    // prepare data for jurnal
                    $data_post['sdm_id'] = $request->get('sdm_id');
                    $data_post['sdm_name'] = $request->get('sdm_name');
                    $data_post['trnsct_date'] = $request->get('trnsct_date');
                    $data_post['description'] = $request->get('description');
                    $data_post['amount'] = str_replace('.', '', $value);
                    $data_post['flow'] = 'OUT';
                    $data_post['hutang_piutang_id'] = $key;
                    $data_post['hutang_piutang_type'] = 'PIUTANG_BAYAR';
                    $data_post['jurnal_item'][0]['account_id'] = $request->get('account_id');
                    $data_post['jurnal_item'][0]['credit'] = str_replace('.', '', $value);
                    $data_post['jurnal_item'][0]['debit'] = 0;
                    $data_post['jurnal_item'][1]['account_id'] = '01.04';
                    $data_post['jurnal_item'][1]['debit'] = str_replace('.', '', $value);
                    $data_post['jurnal_item'][1]['credit'] = 0;

                    $this->TrnsctJurnalController->store($data_post);

                    $count_piutang_success += 1;
                }
            }
        }

        if ($count_piutang_success > 0) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'sdm_id' => $request->get('sdm_id')
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function detail($id)
    {
        $piutang = \App\ModelFinance\TrnsctHutangPiutang::with('sdm')->where('hutang_piutang_id', $id)->first();
        $dt_bayar = \App\ModelFinance\TrnsctJurnal::where('hutang_piutang_id', "$id")
            ->where('hutang_piutang_type', 'PIUTANG_BAYAR')
            ->orderBy('jurnal_id', 'DESC')
            ->get();

        return view('finance.trnsct_piutang.detail', compact('piutang', 'dt_bayar'));
    }

    public function printInvoice($jurnal_id)
    {
        $payment = \App\ModelFinance\TrnsctJurnal::with(['sales'])->findOrFail($jurnal_id);
        return view('finance.trnsct_piutang.print_invoice', compact('payment'));
    }
}
