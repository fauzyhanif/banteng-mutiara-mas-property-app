<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class RefAccountAccountingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_account_accounting.index');
    }

    public function listData()
    {
        $accounts = \App\ModelFinance\RefAccountAccounting::where('account_active', 'Y')->get();

        return DataTables::of($accounts)
            ->addColumn('actions_link', function($accounts)
            {
                // edits link
                $btn = "<a href='" . route('finance.account_accounting.form_update', $accounts->account_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formStore()
    {
        return view('finance.ref_account_accounting.form_store');
    }


    public function store(Request $request)
    {
        $request->validate([
            'account_name'=>'required',
        ]);

        $new = new \App\ModelFinance\RefAccountAccounting();
        $new->account_name = $request->get('account_name');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $account = \App\ModelFinance\RefAccountAccounting::where("account_id", "$id")->first();

        return view('finance.ref_account_accounting.form_update', \compact(
            'account'
        ));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'account_name'=>'required',
        ]);

        $new = \App\ModelFinance\RefAccountAccounting::findOrFail($id);
        $new->account_name = $request->get('account_name');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

}
