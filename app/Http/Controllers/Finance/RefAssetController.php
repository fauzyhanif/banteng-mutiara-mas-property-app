<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class RefAssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_asset.index');
    }

    public function listData()
    {
        $assets = \App\ModelFinance\RefAsset::where('asset_active', 'Y')
            ->orderBy('asset_name', 'ASC')
            ->get();

        return DataTables::of($assets)
            ->addColumn('asset_price_html', function($assets)
            {
                return number_format($assets->asset_price,0,',','.');
            })
            ->addColumn('asset_book_value_html', function($assets)
            {
                return number_format($assets->asset_book_value,0,',','.');
            })
            ->addColumn('asset_purchase_date_html', function($assets)
            {
                return date("d-m-Y", strtotime($assets->asset_purchase_date));
            })
            ->addColumn('actions_link', function($assets)
            {
                // edits link
                $btn = "<a href='" . route('finance.asset.form_update', $assets->asset_id) . "' class='btn btn-primary btn-sm'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                // detail link
                $btn .= "<a href='" . route('finance.asset.detail', $assets->asset_id) . "' class='btn btn-info btn-sm'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'asset_price_html', 'asset_book_value_html', 'asset_purchase_date_html'])
            ->toJson();
    }

    public function formStore()
    {
        return view('finance.ref_asset.form_store');
    }


    public function store(Request $request)
    {
        $request->validate([
            'asset_name'=>'required',
            'asset_price'=>'required',
            'asset_purchase_date'=>'required',
        ]);

        $new = new \App\ModelFinance\RefAsset();
        $new->asset_num = $request->get('asset_num');
        $new->asset_name = $request->get('asset_name');
        $new->asset_desc = $request->get('asset_desc');
        $new->asset_price = str_replace(".","",$request->get('asset_price'));
        $new->asset_book_value = str_replace(".","",$request->get('asset_book_value'));
        $new->asset_purchase_date = $request->get('asset_purchase_date');
        $new->asset_depreciation = $request->get('asset_depreciation');
        $new->asset_last_depreciation = $request->get('asset_last_depreciation');
        $new->asset_usage_time = $request->get('asset_usage_time');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $asset = \App\ModelFinance\RefAsset::findOrFail($id);

        return view('finance.ref_asset.form_update', \compact(
            'asset'
        ));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'asset_name'=>'required',
            'asset_price'=>'required',
            'asset_purchase_date'=>'required',
        ]);

        $new = \App\ModelFinance\RefAsset::findOrFail($id);
         $new->asset_num = $request->get('asset_num');
        $new->asset_name = $request->get('asset_name');
        $new->asset_desc = $request->get('asset_desc');
        $new->asset_price = str_replace(".","",$request->get('asset_price'));
        $new->asset_purchase_date = $request->get('asset_purchase_date');
        $new->asset_depreciation = $request->get('asset_depreciation');
        $new->asset_usage_time = $request->get('asset_usage_time');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function print()
    {
        $assets = \App\ModelFinance\RefAsset::where('asset_active', 'Y')
            ->orderBy('asset_name', 'ASC')
            ->get();

        return view('finance.ref_asset.print', \compact(
            'assets'
        ));
    }

    public function formDepreciation(Request $request)
    {
        $this_year = date('Y-12-31');

        $assets = \App\ModelFinance\RefAsset::where(DB::raw('YEAR(asset_last_depreciation)'), '<', date('Y'))
            ->where(DB::raw('YEAR(asset_purchase_date) + asset_usage_time'), '>=', date('Y'))
            ->where('asset_depreciation', 'Y')
            ->get();

        return view('finance.ref_asset.form_depreciation', compact('this_year', 'assets'));
    }

    public function depreciation(Request $request)
    {
        $asset_id = $request->get('asset_id');
        $asset_name = $request->get('asset_name');
        $depreciation_start_date = $request->get('depreciation_start_date');
        $depreciation_end_date = $request->get('depreciation_end_date');
        $depreciation_amount = $request->get('depreciation_amount');

        foreach ($asset_id as $key => $value) {

            // store depreciation
            $new = new \App\ModelFinance\RefAssetDepreciation();
            $new->asset_id = $value;
            $new->depreciation_start_date = $depreciation_start_date[$value];
            $new->depreciation_end_date = $depreciation_end_date[$value];
            $new->depreciation_amount = $depreciation_amount[$value];
            $new->save();

            // update asset
            $update = \App\ModelFinance\RefAsset::findOrFail($value);
            $update->asset_book_value = $update->asset_book_value - $depreciation_amount[$value];
            $update->asset_last_depreciation = $depreciation_end_date[$value];
            $update->save();
        }

        return redirect()->back()->with(['success' => 'Berhasil menjalankan penyusutan.']);
    }

    public function detail($id)
    {
        $asset = \App\ModelFinance\RefAsset::with(['depreciations'])->findOrFail($id);

        return view('finance.ref_asset.detail', \compact(
            'asset'
        ));
    }

}
