<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class TrnsctPengajuanPencairanController extends Controller
{
    protected $TrnsctJurnalController;
    public function __construct(TrnsctJurnalController $TrnsctJurnalController)
    {
        $this->middleware('auth');
        $this->TrnsctJurnalController = $TrnsctJurnalController;
    }

    public function index(Request $request)
    {
        $type = ($request->has('type')) ? $request->input('type') : 'OPERASIONAL';
        $status = ($request->has('status')) ? $request->input('status') : 'OPEN';
        return view('finance.trnsct_pengajuan_pencairan.index', compact('type', 'status'));
    }

    public function listData(Request $request)
    {
        $type = $request->get('type');
        $status = $request->get('status');

        if ($type == 'OPERASIONAL') {
            $datas = \App\ModelFinance\TrnsctPengajuanPencairan::with('finance')
                ->where('submission_type', "OPERASIONAL")
                ->where('submission_status', "$status")
                ->orderBy('submission_deadline', 'ASC')
                ->get();

            return DataTables::of($datas)
                ->addColumn('ket', function($datas)
                {
                    $btn = "<a href='" . route('finance.pengajuan_pencairan.detail', $datas->submission_id) . "'>";
                    $btn .= $datas->finance->finance_desc;
                    $btn .= "</a> ";

                    return $btn;
                })
                ->addColumn('deadline', function($datas)
                {
                    $text = date("d/m/Y", strtotime($datas->submission_deadline));
                    return $text;
                })
                ->addColumn('jumlah', function($datas)
                {
                    return number_format($datas->submission_amount,0,',','.');
                })
                ->addColumn('status', function($datas)
                {
                    if ($datas->submission_status == 'OPEN') {
                        return '<span class="text-green">'.$datas->submission_status.'</span>';
                    } else {
                        return '<span class="text-red">'.$datas->submission_status.'</span>';
                    }

                })
                ->rawColumns(['ket', 'deadline', 'status', 'jumlah'])
                ->toJson();
        } elseif ($type == 'CASHBOND') {
            $datas = \App\ModelFinance\TrnsctPengajuanPencairan::where('submission_type', "CASHBOND")
                ->where('submission_status', "$status")
                ->orderBy('submission_deadline', 'ASC')
                ->get();
            return DataTables::of($datas)
                ->addColumn('ket', function($datas)
                {
                    $btn = "<a href='" . route('finance.pengajuan_pencairan.detail', $datas->submission_id) . "'>Kasbon</a> ";

                    return $btn;
                })
                ->addColumn('deadline', function($datas)
                {
                    $text = date("d/m/Y", strtotime($datas->submission_deadline));
                    return $text;
                })
                ->addColumn('jumlah', function($datas)
                {
                    return number_format($datas->submission_amount,0,',','.');
                })
                ->addColumn('status', function($datas)
                {
                    if ($datas->submission_status == 'OPEN') {
                        return '<span class="text-green">'.$datas->submission_status.'</span>';
                    } else {
                        return '<span class="text-red">'.$datas->submission_status.'</span>';
                    }

                })
                ->rawColumns(['ket', 'deadline', 'status', 'jumlah'])
                ->toJson();
        }
    }


    public function formStore(Request $request)
    {
        return view('finance.trnsct_pengajuan_pencairan.form_store');
    }

    public function formStoreFirstStep()
    {
        return view('finance.trnsct_pengajuan_pencairan.form_store_first_step');
    }

    public function listHutangPerusahaan(Request $request)
    {
        $key = $request->get('key');

        if ($key != null || $key != "") {
            $datas = \App\ModelFinance\Trnsct::with('sdm')
                ->orWhere(function($query) use($key) {
                    $query->where('finance_id', 'like', "%$key%")->where('finance_desc', 'like', "%$key%");
                })
                ->orWhereHas('sdm', function ($query) use($key) {
                    return $query->where('name', 'like', "%$key%");
                })
                ->where("finance_amount", ">", "finance_amount_paid")
                ->orderBy("finance_date", "desc")
                ->paginate(10);
        } else {
            $datas = \App\ModelFinance\Trnsct::with('sdm')
                ->where("finance_amount", ">", "finance_amount_paid")
                ->orderBy("finance_date", "desc")
                ->paginate(10);
        }


        return view('finance.trnsct_pengajuan_pencairan.list_hutang_perusahaan', compact('datas', 'key'));
    }

    public function listSdm(Request $request)
    {
        $key = $request->get('key');

        $datas = \App\ModelProp\RefMandor::with(['sdm' => function ($q) {
            $q->orderBy('name', 'asc');
        }])->paginate(10);

        return view('finance.trnsct_pengajuan_pencairan.list_sdm', compact('datas', 'key'));
    }

    public function formStoreSecondStep(Request $request)
    {
        $step = $request->get('step');
        return view('finance.trnsct_pengajuan_pencairan.form_store_'.$step, compact('step'));
    }

    public function store(Request $request)
    {
        $data = new \App\ModelFinance\TrnsctPengajuanPencairan();
        $data->submission_type = $request->get('submission_type');
        $data->submission_sdm_id = $request->get('submission_deadline');
        $data->submission_deadline = $request->get('submission_deadline');
        $data->submission_department = $request->get('submission_department');
        $data->submission_amount = str_replace(".","", $request->get('submission_amount'));
        $data->submission_bank = $request->get('submission_bank');
        $data->submission_rek_number = $request->get('submission_rek_number');
        $data->submission_status = 'OPEN';

        if ($request->get('finance_id') != '') { $data->finance_id = $request->get('finance_id');}
        if ($request->get('sdm_id') != '') { $data->submission_sdm_id = $request->get('sdm_id');}

        if ($data->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'id' => $data->submission_id,
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function detail($submission_id)
    {
        $data = \App\ModelFinance\TrnsctPengajuanPencairan::with('finance', 'finance.sdm', 'sdm')->findOrFail($submission_id);

        $view = ($data->submission_type == 'OPERASIONAL') ? 'operasional' : 'cashbond_mandor';
        return view('finance.trnsct_pengajuan_pencairan.detail_'.$view, compact('data'));
    }

    public function formUpdate($submission_id)
    {
        $submission = \App\ModelFinance\TrnsctPengajuanPencairan::with('finance', 'finance.sdm')->findOrFail($submission_id);

        $data_support = [];
        if ($submission->submission_type == 'OPERASIONAL') {
            $data_support = \App\ModelFinance\Trnsct::with('sdm')->findOrFail($submission->finance_id);
        }

        $view = ($submission->submission_type == 'OPERASIONAL') ? 'operasional' : 'cashbond_mandor';

        return view('finance.trnsct_pengajuan_pencairan.form_update_'.$view, compact('submission', 'data_support'));
    }

    public function update(Request $request)
    {
        $submission_id = $request->get('submission_id');
        $data = \App\ModelFinance\TrnsctPengajuanPencairan::findOrFail($submission_id);
        $data->submission_deadline = $request->get('submission_deadline');
        $data->submission_department = $request->get('submission_department');
        $data->submission_amount = str_replace(".","", $request->get('submission_amount'));
        $data->submission_bank = $request->get('submission_bank');
        $data->submission_rek_number = $request->get('submission_rek_number');

        if ($data->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
                'id' => $submission_id,
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function delete(Request $request)
    {
        $submission_id = $request->get('submission_id');
        $data = \App\ModelFinance\TrnsctPengajuanPencairan::findOrFail($submission_id);

        if ($data->submission_status == 'OPEN') {
            $data->delete();
            return redirect('/finance/pengajuan_pencairan');
        } else {
            return redirect('/finance/pengajuan_pencairan/detail/'.$submission_id);
        }
    }

    public function formResponse($submission_id)
    {
        $submission = \App\ModelFinance\TrnsctPengajuanPencairan::with('finance', 'finance.sdm', 'sdm')->findOrFail($submission_id);
        $cashes = \App\ModelFinance\RefCashBank::orderBy('cash_name')->get();

        $data_support = [];
        if ($submission->submission_type == 'OPERASIONAL') {
            $data_support = \App\ModelFinance\Trnsct::with('sdm')->findOrFail($submission->finance_id);
        }

        $view = ($submission->submission_type == 'OPERASIONAL') ? 'operasional' : 'cashbond_mandor';

        return view('finance.trnsct_pengajuan_pencairan.form_response_'.$view, compact('submission', 'data_support', 'cashes'));
    }

    public function response(Request $request)
    {
        $submission_id = $request->get('submission_id');

        $res = [
            'status' => 'failed',
            'text' => 'Data gagal disimpan',
        ];

        $submission = \App\ModelFinance\TrnsctPengajuanPencairan::with('finance', 'finance.sdm')->findOrFail($submission_id);
        $submission->submission_status = 'CLOSE';
        $submission->submission_resp_date = $request->get('finance_paid_date');
        $submission->submission_amount_paid = str_replace(".","", $request->get('finance_paid_amount'));

        if ($submission->save()) {
            if ($request->get('submission_type') == 'OPERASIONAL') {
                $num = $this->invoiceNumber('COST');

                $finance_paid = new \App\ModelFinance\TrnsctPaid();
                $finance_paid->finance_id = $submission->finance_id;
                $finance_paid->finance_paid_num = $num;
                $finance_paid->finance_paid_date = $request->get('finance_paid_date');
                $finance_paid->finance_paid_cash_id = $request->get('finance_paid_cash_id');
                $finance_paid->finance_paid_amount = str_replace(".", "", $request->get('finance_paid_amount'));
                $finance_paid->finance_paid_sdm_id = $request->get('finance_sdm_id');

                if ($finance_paid->save()) {
                    $finance = \App\ModelFinance\Trnsct::findOrFail($submission->finance_id);
                    $finance->finance_amount_paid = $finance->finance_amount_paid + str_replace(".", "", $request->get('finance_paid_amount'));
                    $finance->save();

                    $res = [
                        'status' => 'success',
                        'text' => 'Data berhasil disimpan',
                        'id' => $submission_id,
                    ];
                }
            } elseif ($request->get('submission_type') == 'CASHBOND') {
                $kasbon = new \App\ModelFinance\Kasbon();
                $kasbon->kasbon_sdm_id = $request->get('finance_sdm_id');
                $kasbon->kasbon_date = $request->get('finance_paid_date');
                $kasbon->kasbon_desc = 'Cashbond Mandor';
                $kasbon->kasbon_amount = str_replace('.', '', $request->get('finance_paid_amount'));

                if ($kasbon->save()) {
                    $res = [
                        'status' => 'success',
                        'text' => 'Data berhasil disimpan',
                        'id' => $submission_id,
                    ];
                }
            }
        }

        return response()->json($res);

    }

    public function invoiceNumber($type)
    {
        $this_month = date('Y-m');
        $booking_id = 'INV/'.$type.'/';

        $last_invoice = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
            ->where('type', "$type")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_invoice) == 0 ) {
            $order_id = "0001";

            $new = new \App\ModelFinance\RefInvoiceNumber();
            $new->type = $type;
            $new->month = "$this_month";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $order_id = sprintf('%04d', $last_invoice[0]->serial_num);
            $update = \App\ModelFinance\RefInvoiceNumber::where('month', '=', "$this_month")
                ->where("type", "$type")
                ->update(["serial_num" => $order_id + 1]);
        }

        $booking_id .= date('Y/m/') . $order_id;
        return $booking_id;
    }
}
