<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class RefAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('finance.ref_account.index');
    }

    public function listData()
    {
        $accounts = DB::table('fin_ref_account')
                ->leftJoin('fin_trnsct_item', 'fin_trnsct_item.finance_item_account_id', '=', 'fin_ref_account.account_id')
                ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
                ->leftJoin('fin_ref_account_accounting', 'fin_ref_account.account_accounting_id', '=', 'fin_ref_account_accounting.account_id')
                ->selectRaw('fin_ref_account.account_id, fin_ref_account.account_name, fin_ref_account.account_type, fin_ref_account.account_locked,
                    SUM(IF(fin_trnsct.finance_cancel = "0", fin_trnsct_item.finance_item_amount, 0)) AS total, fin_ref_account_accounting.account_name as account_accounting_name')
                ->groupBy('fin_ref_account.account_id')
                ->get();

        return DataTables::of($accounts)
            ->editColumn('account_name', function($accounts)
            {
                return $accounts->account_name;
            })
            ->addColumn('trnsct', function($accounts)
            {

                $res = number_format($accounts->total,0,',','.');

                return $res;
            })
            ->addColumn('actions_link', function($accounts)
            {
                if ($accounts->account_locked == '1') {
                    $btn = "<i class='fas fa-lock'></i> ";
                } else {
                    // edits link
                    $btn = "<a href='" . route('finance.account.form_update', $accounts->account_id) . "' class='btn btn-primary btn-xs'>";
                    $btn .= "<i class='fas fa-edit'></i> Edit";
                    $btn .= "</a> ";

                }

                // transaction link
                $btn .= "<a href='" . route('finance.account.transaction', $accounts->account_id) . "' class='btn bg-purple btn-xs'>";
                $btn .= "<i class='fas fa-exchange-alt'></i> Transaksi";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link', 'trnsct'])
            ->toJson();
    }

    public function formStore()
    {
        $accounts = \App\ModelFinance\RefAccountAccounting::where('account_active', 'Y')->get();
        return view('finance.ref_account.form_store', compact('accounts'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'account_name'=>'required',
            'account_type'=>'required',
            'account_accounting_id'=>'required',
        ]);

        $new = new \App\ModelFinance\RefAccount();
        $new->account_name = $request->get('account_name');
        $new->account_type = $request->get('account_type');
        $new->account_accounting_id = $request->get('account_accounting_id');

            if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($id)
    {
        $account = \App\ModelFinance\RefAccount::where("account_id", "$id")->first();
        $accounts = \App\ModelFinance\RefAccountAccounting::where('account_active', 'Y')->get();

        return view('finance.ref_account.form_update', \compact(
            'account',
            'accounts'
        ));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'account_name'=>'required',
            'account_type'=>'required',
            'account_accounting_id'=>'required',
        ]);

        $new = \App\ModelFinance\RefAccount::findOrFail($id);
        $new->account_name = $request->get('account_name');
        $new->account_type = $request->get('account_type');
        $new->account_accounting_id = $request->get('account_accounting_id');

        if ($new->save()) {
            $res = [
                'status' => 'success',
                'text' => 'Data berhasil disimpan',
            ];
        } else {
            $res = [
                'status' => 'failed',
                'text' => 'Data gagal disimpan',
            ];
        }

        return response()->json($res);
    }

    public function transaction(Request $request, $account_id)
    {
        $start_date = ($request->has('range_date')) ? explode(" - ", $request->input('range_date'))[0] : date('Y-m-01');
        $end_date = ($request->has('range_date')) ? explode(" - ", $request->input('range_date'))[1] : date('Y-m-d');
        $account = \App\ModelFinance\RefAccount::findOrFail($account_id);

        $jumlah = 0;

        $datas = DB::table('fin_trnsct_item')
                ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
                ->selectRaw("SUM(fin_trnsct_item.finance_item_amount) as total")
                ->where("fin_trnsct_item.finance_item_account_id", "$account_id")
                ->where("fin_trnsct.finance_cancel", "0")
                ->whereBetween('finance_date', ["$start_date", "$end_date"])
                ->groupBy('fin_trnsct_item.finance_item_account_id')
                ->get();

        if (count($datas) == 1) {
            $jumlah = $datas[0]->total;
        }

        return view('finance.ref_account.transaction', compact('account_id', 'account', 'start_date', 'end_date', 'jumlah'));
    }

    public function transactionListData(Request $request)
    {
        $account_id = $request->get('account_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $datas = DB::table('fin_trnsct_item')
                ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
                ->leftJoin('prop_ref_sdm', 'fin_trnsct.finance_sdm_id', '=', 'prop_ref_sdm.sdm_id')
                ->leftJoin('fin_ref_account', 'fin_trnsct_item.finance_item_account_id', '=', 'fin_ref_account.account_id')
                ->select('fin_trnsct.finance_id', 'fin_trnsct.finance_date', 'prop_ref_sdm.name as sdm_name', 'fin_trnsct.finance_desc', 'fin_trnsct_item.finance_item_amount')
                ->where("fin_trnsct_item.finance_item_account_id", "$account_id")
                ->where("fin_trnsct.finance_cancel", "0")
                ->whereBetween('fin_trnsct.finance_date', ["$start_date", "$end_date"])
                ->orderBy('fin_trnsct.finance_date', "ASC")
                ->get();

        return DataTables::of($datas)
            ->addColumn('date', function($datas)
            {
                $text = date("d/m/Y", strtotime($datas->finance_date));
                return $text;
            })
            ->addColumn('jumlah', function($datas)
            {
                return number_format($datas->finance_item_amount,2,',','.');
            })
            ->addColumn('actions_link', function($datas)
            {
                // edits link
                $btn = "<a href='" . route('finance.cost.detail', $datas->finance_id) . "' class='btn btn-info btn-xs' target='_blank'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['date', 'jumlah', 'actions_link'])
            ->toJson();
    }

    public function transactionPrint(Request $request)
    {
        $start_date = explode(" - ", $request->get('range_date'))[0];
        $end_date = explode(" - ", $request->get('range_date'))[1];
        $account_id = $request->get('account_id');
        $account = \App\ModelFinance\RefAccount::findOrFail($account_id);

        $datas = DB::table('fin_trnsct_item')
                ->leftJoin('fin_trnsct', 'fin_trnsct_item.finance_id', '=', 'fin_trnsct.finance_id')
                ->leftJoin('prop_ref_sdm', 'fin_trnsct.finance_sdm_id', '=', 'prop_ref_sdm.sdm_id')
                ->leftJoin('fin_ref_account', 'fin_trnsct_item.finance_item_account_id', '=', 'fin_ref_account.account_id')
                ->select('fin_trnsct.finance_date', 'prop_ref_sdm.name as sdm_name', 'fin_trnsct.finance_desc', 'fin_trnsct_item.finance_item_amount')
                ->where("fin_trnsct_item.finance_item_account_id", "$account_id")
                ->whereBetween("fin_trnsct.finance_date", [$start_date, $end_date])
                ->groupBy('fin_trnsct.finance_date')
                ->get();

        return view('finance.ref_account.transaction_print', compact('account_id', 'account', 'start_date', 'end_date', 'datas'));
    }

}
