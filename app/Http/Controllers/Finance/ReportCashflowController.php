<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class ReportCashflowController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $start_date = ($request->has('date')) ? explode(" - ", $request->input('date'))[0] : date('Y-m-01');
    $end_date = ($request->has('date')) ? explode(" - ", $request->input('date'))[1] : date('Y-m-d');
    return view('finance.report_cashflow.index', compact('start_date', 'end_date'));
  }

  public function cashflowTotal(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    // uang keluar
    $uang_keluar = 0;
    $cashflow_kasbon = $this->getCashflowKasbon($start_date, $end_date);
    $cashflow_operasional = $this->getCashflowOperasional($start_date, $end_date);
    $cashflow_penjualan_return = $this->getCashflowPenjualanReturn($start_date, $end_date);
    $uang_keluar = $cashflow_kasbon->sum("kasbon_amount") + $cashflow_operasional->sum("finance_paid_amount") + $cashflow_penjualan_return->sum('sales_return_amount');

    // uang masuk
    $uang_masuk = 0;
    $cashflow_kasbon_paid = $this->getCashflowKasbonPaid($start_date, $end_date);
    $cashflow_penjualan = $this->getCashflowPenjualan($start_date, $end_date);
    $uang_masuk = $cashflow_kasbon_paid->sum("kasbon_paid_amount") + $cashflow_penjualan->sum("sales_paid_amount");

    $arr = [
      "uang_masuk" => number_format($uang_masuk,0,',','.'),
      "uang_keluar" => number_format($uang_keluar,0,',','.'),
    ];

    return response()->json($arr);
  }

  public function cashflowKasbon(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    $result = [];
    $cashflow_kasbon = $this->getCashflowKasbon($start_date, $end_date);
    $cashflow_kasbon_paid = $this->getCashflowKasbonPaid($start_date, $end_date);

    foreach ($cashflow_kasbon as $key => $value) {
      $result[] = [
        "date" => date("d/m/Y", strtotime($value->kasbon_date)),
        "account_name" => $value->cash_bank->cash_name,
        "name" => $value->sdm->name,
        "desc" => "Kasbon",
        "uang_keluar" => number_format($value->kasbon_amount,0,',','.'),
        "uang_masuk" => "0"
      ];
    }

    foreach ($cashflow_kasbon_paid as $key => $value) {
      $result[] = [
        "date" => date("d/m/Y", strtotime($value->kasbon_paid_date)),
        "account_name" => $value->kasbon->cash_bank->cash_name,
        "name" => $value->kasbon->sdm->name,
        "desc" => "Pembayaran Kasbon",
        "uang_keluar" => "0",
        "uang_masuk" => number_format($value->kasbon_paid_amount,0,',','.'),
      ];
    }

    $result = collect($result)->sortBy('date')->reverse()->toArray();

    return DataTables::of($result)->toJson();
  }

  public function cashflowKasbonTotal(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    $cashflow_kasbon = $this->getCashflowKasbon($start_date, $end_date);
    $cashflow_kasbon_paid = $this->getCashflowKasbonPaid($start_date, $end_date);

    $arr = [
      "uang_keluar" => number_format($cashflow_kasbon->sum('kasbon_amount'),0,',','.'),
      "uang_masuk" => number_format($cashflow_kasbon_paid->sum('kasbon_paid_amount'),0,',','.'),
    ];

    return response()->json($arr);
  }

  public function getCashflowKasbon($start_date, $end_date)
  {
    $result = \App\ModelFinance\Kasbon::with(['sdm', 'cash_bank'])
      ->where("kasbon_cancel", "0")
      ->whereBetween("kasbon_date", [$start_date, $end_date])
      ->get();

    return $result;
  }

  public function getCashflowKasbonPaid($start_date, $end_date)
  {
    $result = \App\ModelFinance\KasbonPaid::with(['kasbon.sdm'. 'kasbon.cash_bank'])
      ->where("kasbon_paid_cancel", "0")
      ->whereBetween("kasbon_paid_date", [$start_date, $end_date])
      ->get();

    return $result;
  }

  public function cashflowPenjualan(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    $result = [];
    $sales = $this->getCashflowPenjualan($start_date, $end_date);
    $sales_return = $this->getCashflowPenjualanReturn($start_date, $end_date);

    foreach ($sales as $key => $value) {
      $result[] = [
        "date" => date("d/m/Y", strtotime($value->sales_paid_date)),
        "account_name" => $value->cash_bank->cash_name,
        "name" => $value->sales_paid_person,
        "desc" => $value->sales_paid_desc,
        "uang_masuk" => number_format($value->sales_paid_amount,0,',','.'),
        "uang_keluar" => "0"
      ];
    }

    foreach ($sales_return as $key => $value) {
      $result[] = [
        "date" => date("d/m/Y", strtotime($value->sales_return_date)),
        "account_name" => $value->cash_bank->cash_name,
        "name" => $value->sales_return_person,
        "desc" => $value->sales_return_desc,
        "uang_keluar" => number_format($value->sales_return_amount,0,',','.'),
        "uang_masuk" => "0"
      ];
    }

    $result = collect($result)->sortBy('date')->reverse()->toArray();

    return DataTables::of($result)->toJson();
  }

  public function cashflowPenjualanTotal(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    $sales = $this->getCashflowPenjualan($start_date, $end_date);
    $return = $this->getCashflowPenjualanReturn($start_date, $end_date);

    $arr = [
      "uang_masuk" => number_format($sales->sum('sales_paid_amount'),0,',','.'),
      "uang_keluar" => number_format($return->sum('sales_return_amount'),0,',','.'),
    ];

    return response()->json($arr);
  }

  public function getCashflowPenjualan($start_date, $end_date)
  {
    $result = \App\ModelProp\TrnsctSalesPaid::with(['sales','cash_bank'])
      ->where("sales_paid_cancel", "0")
      ->whereBetween("sales_paid_date", [$start_date, $end_date])
      ->orderBy("sales_paid_date", "DESC")
      ->get();

    return $result;
  }

  public function getCashflowPenjualanReturn($start_date, $end_date)
  {
    $result = \App\ModelProp\TrnsctSalesReturn::with(['cash_bank'])
      ->where("sales_return_cancel", "0")
      ->whereBetween("sales_return_date", [$start_date, $end_date])
      ->orderBy("sales_return_date", "DESC")
      ->get();

    return $result;
  }

  public function cashflowOperasional(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    $result = [];
    $operasionals = $this->getCashflowOperasional($start_date, $end_date);

    foreach ($operasionals as $key => $value) {
      $result[] = [
        "date" => date("d/m/Y", strtotime($value->finance_paid_date)),
        "account_name" => $value->cash_bank->cash_name,
        "name" => $value->sdm->name,
        "desc" => $value->trnsct->finance_desc,
        "uang_keluar" => number_format($value->finance_paid_amount,0,',','.'),
        "uang_masuk" => "0"
      ];
    }

    return DataTables::of($result)->toJson();
  }

  public function cashflowOperasionalTotal(Request $request)
  {
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');

    $operasional = $this->getCashflowOperasional($start_date, $end_date);

    $arr = [
      "uang_keluar" => number_format($operasional->sum('finance_paid_amount'),0,',','.'),
      "uang_masuk" => "0.00",
    ];

    return response()->json($arr);
  }

  

  

  public function getCashflowOperasional($start_date, $end_date)
  {
    $result = \App\ModelFinance\TrnsctPaid::with(['trnsct', 'sdm', 'cash_bank'])
      ->where("finance_paid_cancel", "0")
      ->whereBetween("finance_paid_date", [$start_date, $end_date])
      ->get();

    return $result;
  }
}