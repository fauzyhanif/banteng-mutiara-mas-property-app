<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class TrnsctJurnal extends Model
{
    protected $table = "fin_trnsct_jurnal";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "jurnal_id",
        "trnsct_date",
        "related_person",
        "amount",
        "description",
        "flow",
        "hutang_piutang_id",
        "hutang_piutang_type",
        "user",
        "status"
    ];

    public function sales()
    {
        return $this->belongsTo(\App\ModelProp\TrnsctSales::class, 'sales_id', 'sales_id');
    }
}
