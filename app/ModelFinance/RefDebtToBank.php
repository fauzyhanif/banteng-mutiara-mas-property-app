<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefDebtToBank extends Model
{
    protected $table = "fin_ref_debt_to_bank";
    protected $primaryKey = "debt_id";
    protected $fillable = [
        "debt_id",
        "sdm_id",
        "debt_desc",
        "debt_total",
    ];

    public function sdm()
    {
        return $this->belongsTo(\App\ModelProp\RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
