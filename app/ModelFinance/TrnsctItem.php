<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class Trnsctitem extends Model
{
    protected $table = "fin_trnsct_item";
    protected $primaryKey = "finance_item_id";
    protected $fillable = [
        "finance_item_id",
        "finance_id",
        "finance_item_amount",
        "finance_item_account_id"
    ];

    public function transaction()
    {
        return $this->belongsTo(\App\ModelFinance\Trnsct::class, 'finance_id', 'finance_id');
    }

    public function account()
    {
        return $this->belongsTo(\App\ModelFinance\RefAccount::class, 'finance_item_account_id', 'account_id');
    }
}
