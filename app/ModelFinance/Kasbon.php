<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class Kasbon extends Model
{
    protected $table = "fin_kasbon";
    protected $primaryKey = "kasbon_id";
    protected $fillable = [
        "kasbon_id",
        "kasbon_date",
        "kasbon_sdm_id",
        "kasbon_cash_id",
        "kasbon_desc",
        "kasbon_amount",
        "kasbon_amount_paid",
        "kasbon_cancel",
    ];

    public function sdm()
    {
        return $this->belongsTo(\App\ModelProp\RefSdm::class, 'kasbon_sdm_id', 'sdm_id');
    }

    public function cash_bank()
    {
        return $this->belongsTo(\App\ModelFinance\RefCashBank::class, 'kasbon_cash_id', 'cash_id');
    }
}
