<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefAccountAccounting extends Model
{
    protected $table = "fin_ref_account_accounting";
    protected $primaryKey = "account_id";
    protected $fillable = [
        "account_id",
        "account_name",
        "account_active",
    ];

    public function account()
    {
        return $this->hasMany(\App\ModelFinance\RefAccount::class, 'account_accounting_id', 'account_id');
    }
}
