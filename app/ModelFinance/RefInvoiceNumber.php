<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefInvoiceNumber extends Model
{
    protected $table = "fin_ref_invoice_number";
    protected $fillable = [
        "id",
        "type",
        "month",
        "serial_num"
    ];
}
