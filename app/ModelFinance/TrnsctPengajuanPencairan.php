<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class TrnsctPengajuanPencairan extends Model
{
    protected $table = "fin_trnsct_pengajuan_pencairan";
    protected $primaryKey = "submission_id";
    protected $fillable = [
        "submission_id",
        "submission_type",
        "finance_id",
        "submission_deadline",
        "submission_resp_date",
        "submission_department",
        "submission_amount",
        "submission_amount_paid",
        "submission_bank",
        "submission_rek_number",
        "submission_status",
    ];

    public function finance()
    {
        return $this->belongsTo(Trnsct::class, 'finance_id', 'finance_id');
    }

    public function sdm()
    {
        return $this->belongsTo(\App\ModelProp\RefSdm::class, 'submission_sdm_id', 'sdm_id');
    }
}
