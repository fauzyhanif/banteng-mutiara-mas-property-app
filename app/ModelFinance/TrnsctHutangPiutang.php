<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class TrnsctHutangPiutang extends Model
{
    protected $table = "fin_trnsct_hutang_piutang";
    protected $primaryKey = "hutang_piutang_id";
    protected $fillable = [
        "hutang_piutang_id",
        "type",
        "sdm_id",
        "trnsct_date",
        "due_date",
        "description",
        "amount",
        "amount_paid",
        "status"
    ];

    public function sdm()
    {
        return $this->belongsTo(\App\ModelProp\RefSdm::class, 'sdm_id', 'sdm_id');
    }
}
