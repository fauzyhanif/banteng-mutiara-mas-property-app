<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefAccountGroup extends Model
{
    protected $table = "fin_ref_account_group";
    protected $primaryKey = 'group_id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        "group_id",
        "name",
        "active"
    ];
}
