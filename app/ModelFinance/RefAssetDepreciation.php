<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefAssetDepreciation extends Model
{
    protected $table = "fin_ref_asset_depreciation";
    protected $fillable = [
        "asset_id",
        "depreciation_start_date",
        "depreciation_end_date",
        "depreciation_amount",
    ];

}
