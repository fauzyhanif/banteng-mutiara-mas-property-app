<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefCashBank extends Model
{
    protected $table = "fin_ref_cash_bank";
    protected $primaryKey = "cash_id";
    protected $fillable = [
        "cash_id",
        "cash_name",
    ];
}
