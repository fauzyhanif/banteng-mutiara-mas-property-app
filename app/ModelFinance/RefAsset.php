<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefAsset extends Model
{
    protected $table = "fin_ref_asset";
    protected $primaryKey = "asset_id";
    protected $fillable = [
        "asset_id",
        "asset_num",
        "asset_name",
        "asset_desc",
        "asset_price",
        "asset_book_value",
        "asset_purchase_date",
        "asset_depreciation",
        "asset_usage_time",
        "asset_active",
    ];

    public function depreciations()
    {
        return $this->hasMany(\App\ModelFinance\RefAssetDepreciation::class, 'asset_id', 'asset_id');
    }

}
