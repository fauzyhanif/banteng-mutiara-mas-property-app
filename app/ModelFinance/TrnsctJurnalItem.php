<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class TrnsctJurnalItem extends Model
{
    protected $table = "fin_trnsct_jurnal_item";
    protected $primaryKey = "jurnitem_id";
    protected $fillable = [
        "jurnitem_id",
        "jurnal_id",
        "account_id",
        "debit",
        "credit",
    ];

    public function account()
    {
        return $this->belongsTo(RefAccount::class, 'account_id', 'account_id');
    }
}
