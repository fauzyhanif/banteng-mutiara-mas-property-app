<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class KasbonPaid extends Model
{
    protected $table = "fin_kasbon_paid";
    protected $primaryKey = "kasbon_paid_id";
    protected $fillable = [
        "kasbon_paid_id",
        "kasbon_id",
        "kasbon_paid_date",
        "kasbon_paid_sdm_id",
        "kasbon_paid_cash_id",
        "kasbon_paid_amount",
        "kasbon_paid_cancel",
    ];

    public function kasbon()
    {
        return $this->belongsTo(\App\ModelFinance\Kasbon::class, 'kasbon_id', 'kasbon_id');
    }

    public function cash_bank()
    {
        return $this->belongsTo(\App\ModelFinance\RefCashBank::class, 'kasbon_paid_cash_id', 'cash_id');
    }
}
