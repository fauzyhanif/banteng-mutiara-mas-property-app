<?php

namespace App\ModelFinance;

use App\ModelProp\TrnsctSales;
use Illuminate\Database\Eloquent\Model;

class Trnsct extends Model
{
    protected $table = "fin_trnsct";
    protected $primaryKey = "finance_id";
    protected $fillable = [
        "finance_id",
        "finance_num",
        "finance_date",
        "finance_type",
        "finance_sdm_id",
        "finance_cash_id",
        "finance_amount",
        "finance_amount_paid",
        "finance_cancel",
        "unit_location_id",
        "unit_block_id",
        "unit_unit_id",
        "is_worklist",
        "worklist_id",
        "is_purchase",
        "purchase_id",
    ];

    public function sdm()
    {
        return $this->belongsTo(\App\ModelProp\RefSdm::class, 'finance_sdm_id', 'sdm_id');
    }

    public function transaction()
    {
        return $this->oneToMany(\App\ModelFinance\TrnsctItem::class, 'finance_id', 'finance_id');
    }

    public function sales()
    {
        return $this->belongsTo(TrnsctSales::class, 'sales_id', 'id');    
    }
}
