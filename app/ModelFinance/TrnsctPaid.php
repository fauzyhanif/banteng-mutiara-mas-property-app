<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class TrnsctPaid extends Model
{
    protected $table = "fin_trnsct_paid";
    protected $primaryKey = "finance_paid_id";
    protected $fillable = [
        "finance_paid_id",
        "finance_id",
        "finance_paid_num",
        "finance_paid_date",
        "finance_paid_cash_id",
        "finance_paid_amount",
        "finance_paid_person",
        "finance_paid_cancel",
    ];

    public function cash_bank()
    {
        return $this->belongsTo(\App\ModelFinance\RefCashBank::class, 'finance_paid_cash_id', 'cash_id');
    }

    public function trnsct()
    {
        return $this->belongsTo(\App\ModelFinance\Trnsct::class, 'finance_id', 'finance_id');
    }

    public function sdm()
    {
        return $this->belongsTo(\App\ModelProp\RefSdm::class, 'finance_paid_sdm_id', 'sdm_id');
    }
}
