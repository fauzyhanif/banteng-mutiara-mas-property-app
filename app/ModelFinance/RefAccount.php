<?php

namespace App\ModelFinance;

use Illuminate\Database\Eloquent\Model;

class RefAccount extends Model
{
    protected $table = "fin_ref_account";
    protected $primaryKey = "account_id";
    protected $fillable = [
        "account_id",
        "account_type",
        "account_name",
    ];

    public function trnsctItem()
    {
        return $this->hasMany(\App\ModelFinance\TrnsctItem::class, 'finance_item_account_id', 'account_id');
    }

    public function accounting()
    {
        return $this->belongsTo(\App\ModelFinance\RefAccount::class, 'account_accounting_id', 'account_id');
    }
}
