<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Travel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {

    /////* Cash Sales */////
    Route::get('/reports/cash_sales', 'Reports\CashSalesController@index')->name('reports.cash_sales');
    Route::get('/reports/cash_sales/list_unit', 'Reports\CashSalesController@listUnit')->name('reports.cash_sales.list_unit');
    Route::get('/reports/cash_sales/list_unit_json', 'Reports\CashSalesController@listUnitJson')->name('reports.cash_sales.list_unit_json');

    /////* KPR Sales */////
    Route::get('/reports/kpr_sales', 'Reports\KprSalesController@index')->name('reports.kpr_sales');
    Route::get('/reports/kpr_sales/list_unit', 'Reports\KprSalesController@listUnit')->name('reports.kpr_sales.list_unit');
    Route::get('/reports/kpr_sales/list_unit_json', 'Reports\KprSalesController@listUnitJson')->name('reports.kpr_sales.list_unit_json');

    /////* Summary Unit */////
    Route::get('/reports/unit', 'Reports\UnitController@index')->name('reports.unit');
    Route::get('/reports/unit/list_unit', 'Reports\UnitController@listUnit')->name('reports.unit.list_unit');
    Route::get('/reports/unit/list_unit_json', 'Reports\UnitController@listUnitJson')->name('reports.unit.list_unit_json');

    /////* Foreman Job */////
    Route::get('/reports/foreman_job', 'Reports\ForemanJobController@index')->name('reports.foreman_job');
    Route::get('/reports/foreman_job/list_job', 'Reports\ForemanJobController@listJob')->name('reports.foreman_job.list_job');

    /////* Hutang Piutang */////
    Route::get('/reports/hutang_piutang', 'Reports\HutangPiutangController@index')->name('reports.hutang_piutang');
    Route::get('/reports/hutang_piutang/total', 'Reports\HutangPiutangController@total')->name('reports.hutang_piutang.total');
    Route::get('/reports/hutang_piutang/kasbon', 'Reports\HutangPiutangController@kasbon')->name('reports.hutang_piutang.kasbon');
    Route::get('/reports/hutang_piutang/kasbon_total', 'Reports\HutangPiutangController@kasbonTotal')->name('reports.hutang_piutang.kasbon_total');
    Route::get('/reports/hutang_piutang/penjualan_cash', 'Reports\HutangPiutangController@penjualanCash')->name('reports.hutang_piutang.penjualan_cash');
    Route::get('/reports/hutang_piutang/penjualan_total_cash', 'Reports\HutangPiutangController@penjualanTotalCash')->name('reports.hutang_piutang.penjualan_total_cash');
    Route::get('/reports/hutang_piutang/penjualan_kpr/{id}', 'Reports\HutangPiutangController@penjualanKpr')->name('reports.hutang_piutang.penjualan_kpr');
    Route::get('/reports/hutang_piutang/penjualan_total_kpr/{id}', 'Reports\HutangPiutangController@penjualanTotalKpr')->name('reports.hutang_piutang.penjualan_total_kpr');
    Route::get('/reports/hutang_piutang/operasional', 'Reports\HutangPiutangController@operasional')->name('reports.hutang_piutang.operasional');
    Route::get('/reports/hutang_piutang/operasional_total', 'Reports\HutangPiutangController@operasionalTotal')->name('reports.hutang_piutang.operasional_total');
    Route::get('/reports/hutang_piutang/detail/{id}', 'Reports\HutangPiutangController@detail')->name('reports.hutang_piutang.detail');

    // marketing fee
    Route::get('/reports/marketing_fee', 'Reports\MarketingFeeController@index')->name('reports.marketing_fee');
    Route::get('/reports/marketing_fee/detail', 'Reports\MarketingFeeController@detail')->name('reports.marketing_fee.detail');

    // Accounting
    Route::get('/reports/laporan_akuntansi', 'Reports\AccountingController@index')->name('reports.laporan_akuntansi');
    Route::get('/reports/laporan_akuntansi/laba_rugi', 'Reports\AccountingController@labaRugi')->name('reports.laporan_akuntansi.laba_rugi');
    Route::post('/reports/laporan_akuntansi/laba_rugi/print', 'Reports\AccountingController@labaRugiPrint')->name('reports.laporan_akuntansi.laba_rugi.print');
    Route::post('/reports/laporan_akuntansi/laba_rugi/detail', 'Reports\AccountingController@labaRugiDetail')->name('reports.laporan_akuntansi.laba_rugi.detail');
    Route::get('/reports/laporan_akuntansi/laba_rugi/detail_transaction', 'Reports\AccountingController@labaRugiDetailTransaction')->name('reports.laporan_akuntansi.laba_rugi.detail_transaction');
    Route::get('/reports/laporan_akuntansi/neraca', 'Reports\AccountingController@neraca')->name('reports.laporan_akuntansi.neraca');
    Route::post('/reports/laporan_akuntansi/neraca/print', 'Reports\AccountingController@neracaPrint')->name('reports.laporan_akuntansi.neraca.print');
});
