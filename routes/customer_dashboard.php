<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/customer_dashboard/dashboard', 'CustomerDashboard\DashboardController@index')->name('customer_dashboard.dashboard');
Route::get('/customer_dashboard/dashboard/detail/{id}', 'CustomerDashboard\DashboardController@detail')->name('customer_dashboard.dashboard.detail');
Route::post('/customer_dashboard/dashboard/upload_bukti_bayar', 'CustomerDashboard\DashboardController@uploadBuktiBayar')->name('customer_dashboard.dashboard.upload_bukti_bayar');

Route::get('/customer_dashboard/account/change_password', 'CustomerDashboard\AccountController@changePassword')->name('customer_dashboard.account.change_password');
Route::post('/customer_dashboard/account/do_change_password', 'CustomerDashboard\AccountController@doChangePassword')->name('customer_dashboard.account.do_change_password');
