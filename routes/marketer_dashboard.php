<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth_marketer'])->group(function () {
    Route::get('/marketer_dashboard/reset_password', 'MarketerDashboard\DashboardController@resetPassword')->name('marketer_dashboard.reset_password');
    Route::post('/marketer_dashboard/do_reset_password', 'MarketerDashboard\DashboardController@doResetPassword')->name('marketer_dashboard.do_reset_password');
    Route::get('/marketer_dashboard', 'MarketerDashboard\DashboardController@index')->name('marketer_dashboard');
    Route::get('/marketer_dashboard/booking_online', 'MarketerDashboard\BookingOnlineController@index')->name('marketer_dashboard.booking_online');
    Route::post('/marketer_dashboard/booking_online/store', 'MarketerDashboard\BookingOnlineController@store')->name('marketer_dashboard.booking_online.store');
    Route::get('/marketer_dashboard/booking_online/detail/{id}', 'MarketerDashboard\BookingOnlineController@detail')->name('marketer_dashboard.booking_online.detail');
    Route::get('/marketer_dashboard/booking_online/form_edit/{id}', 'MarketerDashboard\BookingOnlineController@formEdit')->name('marketer_dashboard.booking_online.form_edit');
    Route::post('/marketer_dashboard/booking_online/edit/{id}', 'MarketerDashboard\BookingOnlineController@edit')->name('marketer_dashboard.booking_online.edit');
    Route::get('/marketer_dashboard/affiliasi', 'MarketerDashboard\AffiliasiController@index')->name('marketer_dashboard.affiliasi');
});
