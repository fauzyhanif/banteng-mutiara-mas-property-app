<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('/login/doAuth', 'Auth\LoginController@doAuth')->name('login.doauth');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

////////////////////////
///////* System *///////
////////////////////////
Route::get('/gate', 'System\GateController@index')->name('gate');
Route::get('/gate/show-role/{id}', 'System\GateController@showRole')->name('gate.show.role');
Route::get('/gate/show-module', 'System\GateController@showModule')->name('gate.show.module');
Route::get('/error404', 'System\GateController@error404')->name('error404');
Route::get('/gate/set-auth-role-and-module/{role}/{module}', 'System\GateController@setAuth')->name('gate.set.auth.role.and.module');
Route::get('/get-menu', 'System\GateController@getMenu')->name('get.menu');

Route::get('/prop/component/slct_location', 'ComponentController@slctLocation')->name('prop.component.slct_location');
Route::get('/prop/component/slct_block', 'ComponentController@slctBlock')->name('prop.component.slct_block');
Route::get('/prop/component/slct_unit', 'ComponentController@slctUnit')->name('prop.component.slct_unit');
Route::get('/prop/component/slct_unit_ready', 'ComponentController@slctUnitReady')->name('prop.component.slct_unit_ready');
Route::get('/prop/component/slct_unit_type', 'ComponentController@slctUnitType')->name('prop.component.slct_unit_type');
Route::get('/prop/component/slct_unit_status', 'ComponentController@slctUnitStatus')->name('prop.component.slct_unit_status');
Route::get('/prop/component/slct_mandor', 'ComponentController@slctMandor')->name('prop.component.slct_mandor');
Route::get('/prop/component/slct_store', 'ComponentController@slctStore')->name('prop.component.slct_store');
Route::get('/prop/component/slct_subsidi_type', 'ComponentController@slctSubsidiType')->name('prop.component.slct_subsidi_type');
Route::match(['get', 'post'],'/prop/component/slct_sdm', 'ComponentController@slctSdm')->name('prop.component.slct_sdm');
Route::match(['get', 'post'],'/prop/component/slct_sdm_not_customer', 'ComponentController@slctSdmNotCustomer')->name('prop.component.slct_sdm_not_customer');
Route::match(['get', 'post'],'/prop/component/slct_sdm_not_marketer', 'ComponentController@slctSdmNotMarketer')->name('prop.component.slct_sdm_not_marketer');
Route::match(['get', 'post'],'/prop/component/slct_sdm_not_store', 'ComponentController@slctSdmNotStore')->name('prop.component.slct_sdm_not_store');
Route::match(['get', 'post'],'/prop/component/slct_sdm_not_mandor', 'ComponentController@slctSdmNotMandor')->name('prop.component.slct_sdm_not_mandor');
Route::match(['get', 'post'],'/prop/component/slct_customer', 'ComponentController@slctCustomer')->name('prop.component.slct_customer');
Route::match(['get', 'post'],'/prop/component/slct_marketer', 'ComponentController@slctMarketer')->name('prop.component.slct_marketer');
Route::match(['get', 'post'],'/prop/component/slct_goods', 'ComponentController@slctGoods')->name('prop.component.slct_goods');
Route::get('/prop/component/get_sdm_by_id/{id}', 'ComponentController@getSdmById')->name('prop.component.get_sdm_by_id');

// pendaftaran online marketer (downline)
Route::get('/pendaftaran_marketer', 'MarketerRegistrationController@index')->name('pendaftaran_marketer');
Route::post('/pendaftaran_marketer/store', 'MarketerRegistrationController@store')->name('pendaftaran_marketer.store');

// Booking online
Route::get('/booking_online', 'BookingOnlineController@index')->name('booking_online');
Route::post('/booking_online_checkout', 'BookingOnlineController@checkout')->name('booking_online_checkout');
Route::post('/booking_online/store', 'BookingOnlineController@store')->name('booking_online.store');


Route::middleware(['auth'])->group(function () {
    Route::get('/', 'IndexController@index')->name('home');
    Route::match(['get', 'post'], '/dashboard-progress', 'IndexController@dashboardProgress')->name('dashboard-progress');
    Route::post('/data-dashboard-progress', 'IndexController@dataDashboardProgress')->name('data-dashboard-progress');

    /////* Change Password */////
    Route::match(['get', 'post'], '/auth/change_password', 'Auth\LoginController@changePassword')->name('auth.change_password');

    /////* Module */////
    Route::get('/system/module', 'System\ModuleController@index')->name('system.module');
    Route::get('/system/module/form-add', 'System\ModuleController@formAdd')->name('system.module.form.add');
    Route::post('/system/module/add-new', 'System\ModuleController@addNew')->name('system.module.add.new');
    Route::get('/system/module/form-edit/{id}', 'System\ModuleController@formEdit')->name('system.module.form.edit');
    Route::post('/system/module/edit/{id}', 'System\ModuleController@edit')->name('system.module.edit');

    /////* Role */////
    Route::get('/system/role', 'System\RoleController@index')->name('system.role');
    Route::get('/system/role/form-add', 'System\RoleController@formAdd')->name('system.role.form.add');
    Route::post('/system/role/add-new', 'System\RoleController@addNew')->name('system.role.add.new');
    Route::get('/system/role/form-edit/{id}', 'System\RoleController@formEdit')->name('system.role.form.edit');
    Route::post('/system/role/edit/{id}', 'System\RoleController@edit')->name('system.role.edit');

    /////* User */////
    Route::get('/system/user', 'System\UserController@index')->name('system.user');
    Route::get('/system/user/form-add', 'System\UserController@formAdd')->name('system.user.form.add');
    Route::post('/system/user/add-new', 'System\UserController@addNew')->name('system.user.add.new');
    Route::get('/system/user/form-edit/{id}', 'System\UserController@formEdit')->name('system.user.form.edit');
    Route::post('/system/user/edit/{id}', 'System\UserController@edit')->name('system.user.edit');

    /////* Group menu */////
    Route::get('/system/group-menu', 'System\GroupMenuController@index')->name('system.groupmenu');
    Route::get('/system/group-menu/form-add', 'System\GroupMenuController@formAdd')->name('system.groupmenu.form.add');
    Route::post('/system/group-menu/add-new', 'System\GroupMenuController@addNew')->name('system.groupmenu.add.new');
    Route::get('/system/group-menu/form-edit/{id}', 'System\GroupMenuController@formEdit')->name('system.groupmenu.form.edit');
    Route::post('/system/group-menu/edit/{id}', 'System\GroupMenuController@edit')->name('system.groupmenu.edit');

    /////* Acl */////
    Route::get('/system/acl', 'System\AclController@index')->name('system.acl');
    Route::get('/system/acl/form-add', 'System\AclController@formAdd')->name('system.acl.form.add');
    Route::post('/system/acl/add-new', 'System\AclController@addNew')->name('system.acl.add.new');
    Route::get('/system/acl/form-edit/{id}', 'System\AclController@formEdit')->name('system.acl.form.edit');
    Route::post('/system/acl/edit/{id}', 'System\AclController@edit')->name('system.acl.edit');
    Route::get('/system/acl/delete/{id}', 'System\AclController@delete')->name('system.acl.delete');
});
