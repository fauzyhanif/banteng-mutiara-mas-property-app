<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Travel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {

    /////* Dashboard */////
    Route::get('/dashboard/progres_pembangunan_belum_selesai', 'IndexController@progresPembangunanBelumSelesai')->name('dashboard.progres_pembangunan_belum_selesai');
    Route::get('/dashboard/progres_pembangunan_belum_selesai/list_unit/{id}', 'IndexController@progresPembangunanBelumSelesaiListUnit')->name('dashboard.progres_pembangunan_belum_selesai.list_unit');
    Route::get('/dashboard/progres_pembangunan_selesai', 'IndexController@progresPembangunanSelesai')->name('dashboard.progres_pembangunan_selesai');
    Route::get('/dashboard/progres_pembangunan_selesai/list_unit/{id}', 'IndexController@progresPembangunanSelesaiListUnit')->name('dashboard.progres_pembangunan_selesai.list_unit');
    Route::get('/dashboard/pengajuan_pencairan', 'IndexController@pengajuanPencairan')->name('dashboard.pengajuan_pencairan');
    Route::get('/dashboard/progres_kpr', 'IndexController@progresKpr')->name('dashboard.progres_kpr');
    Route::get('/dashboard/progres_kpr/list_unit', 'IndexController@progresKprListUnit')->name('dashboard.progres_kpr.list_unit');
    Route::get('/dashboard/progres_kpr/list_unit_json', 'IndexController@progresKprListUnitJson')->name('dashboard.progres_kpr.list_unit_json');
    Route::get('/dashboard/progres_kpr/list_unit_cetak', 'IndexController@progresKprListUnitCetak')->name('dashboard.progres_kpr.list_unit_cetak');
    Route::get('/dashboard/operasional', 'IndexController@operasional')->name('dashboard.operasional');
    Route::get('/dashboard/resume_unit_ready', 'IndexController@resumeUnitReady')->name('dashboard.resume_unit_ready');
    Route::get('/dashboard/list_booking', 'IndexController@listBooking')->name('dashboard.list_booking');


    /////* Customer */////
    Route::get('/prop/customer', 'Prop\RefCustomerController@index')->name('prop.customer');
    Route::get('/prop/customer/list_data', 'Prop\RefCustomerController@listData')->name('prop.customer.list_data');
    Route::get('/prop/customer/form_store', 'Prop\RefCustomerController@formStore')->name('prop.customer.form_store');
    Route::post('/prop/customer/store', 'Prop\RefCustomerController@store')->name('prop.customer.store');
    Route::get('/prop/customer/form_update/{id}', 'Prop\RefCustomerController@formUpdate')->name('prop.customer.form_update');
    Route::post('/prop/customer/update/{id}', 'Prop\RefCustomerController@update')->name('prop.customer.update');
    Route::post('/prop/customer/update_from_sales_page/{id}', 'Prop\RefCustomerController@updateFromSalesPage')->name('prop.customer.update_from_sales_page');
    Route::get('/prop/customer/print/{id}', 'Prop\RefCustomerController@print')->name('prop.customer.print');

    /////* Marketer fee */////
    Route::get('/prop/marketer_type', 'Prop\RefMarketerTypeController@index')->name('prop.marketer_type');
    Route::get('/prop/marketer_type/list_data', 'Prop\RefMarketerTypeController@listData')->name('prop.marketer_type.list_data');
    Route::get('/prop/marketer_type/form_store', 'Prop\RefMarketerTypeController@formStore')->name('prop.marketer_type.form_store');
    Route::post('/prop/marketer_type/store', 'Prop\RefMarketerTypeController@store')->name('prop.marketer_type.store');
    Route::get('/prop/marketer_type/form_update/{id}', 'Prop\RefMarketerTypeController@formUpdate')->name('prop.marketer_type.form_update');
    Route::post('/prop/marketer_type/update/{id}', 'Prop\RefMarketerTypeController@update')->name('prop.marketer_type.update');

    /////* marketer */////
    Route::get('/prop/marketer', 'Prop\RefMarketerController@index')->name('prop.marketer');
    Route::get('/prop/marketer/list_data', 'Prop\RefMarketerController@listData')->name('prop.marketer.list_data');
    Route::get('/prop/marketer/form_store', 'Prop\RefMarketerController@formStore')->name('prop.marketer.form_store');
    Route::post('/prop/marketer/store', 'Prop\RefMarketerController@store')->name('prop.marketer.store');
    Route::get('/prop/marketer/form_update/{id}', 'Prop\RefMarketerController@formUpdate')->name('prop.marketer.form_update');
    Route::post('/prop/marketer/update/{id}', 'Prop\RefMarketerController@update')->name('prop.marketer.update');
    Route::post('/prop/marketer/create_user', 'Prop\RefMarketerController@createUser')->name('prop.marketer.create_user');
    Route::post('/prop/marketer/reset_password', 'Prop\RefMarketerController@resetPassword')->name('prop.marketer.reset_password');
    Route::post('/prop/marketer/delete', 'Prop\RefMarketerController@delete')->name('prop.marketer.delete');
    Route::get('/prop/marketer/hak', 'Prop\RefMarketerController@hak')->name('prop.marketer.hak');
    Route::get('/prop/marketer/hak_list', 'Prop\RefMarketerController@hakList')->name('prop.marketer.hak_list');
    Route::get('/prop/marketer/hak_detail', 'Prop\RefMarketerController@getHakDetail')->name('prop.marketer.hak_detail');
    Route::get('/prop/marketer/print_hak_detail/{id}', 'Prop\RefMarketerController@printHakDetail')->name('prop.marketer.print_hak_detail');

    /////* Store */////
    Route::get('/prop/toko_bangunan', 'Prop\RefStoreController@index')->name('prop.toko_bangunan');
    Route::get('/prop/toko_bangunan/list_data', 'Prop\RefStoreController@listData')->name('prop.toko_bangunan.list_data');
    Route::get('/prop/toko_bangunan/form_store', 'Prop\RefStoreController@formStore')->name('prop.toko_bangunan.form_store');
    Route::post('/prop/toko_bangunan/store', 'Prop\RefStoreController@store')->name('prop.toko_bangunan.store');
    Route::get('/prop/toko_bangunan/form_update/{id}', 'Prop\RefStoreController@formUpdate')->name('prop.toko_bangunan.form_update');
    Route::post('/prop/toko_bangunan/update/{id}', 'Prop\RefStoreController@update')->name('prop.toko_bangunan.update');

    /////* Department */////
    Route::get('/prop/department', 'Prop\RefDepartmentController@index')->name('prop.department');
    Route::get('/prop/department/list_data', 'Prop\RefDepartmentController@listData')->name('prop.department.list_data');
    Route::get('/prop/department/form_store', 'Prop\RefDepartmentController@formStore')->name('prop.department.form_store');
    Route::post('/prop/department/store', 'Prop\RefDepartmentController@store')->name('prop.department.store');
    Route::get('/prop/department/form_update/{id}', 'Prop\RefDepartmentController@formUpdate')->name('prop.department.form_update');
    Route::post('/prop/department/update/{id}', 'Prop\RefDepartmentController@update')->name('prop.department.update');

    /////* User */////
    Route::get('/prop/user', 'Prop\RefUserController@index')->name('prop.user');
    Route::get('/prop/user/list_data', 'Prop\RefUserController@listData')->name('prop.user.list_data');
    Route::get('/prop/user/form_store', 'Prop\RefUserController@formStore')->name('prop.user.form_store');
    Route::post('/prop/user/store', 'Prop\RefUserController@store')->name('prop.user.store');
    Route::get('/prop/user/form_update/{id}', 'Prop\RefUserController@formUpdate')->name('prop.user.form_update');
    Route::post('/prop/user/update/{id}', 'Prop\RefUserController@update')->name('prop.user.update');
    Route::get('/prop/user/print/{id}', 'Prop\RefUserController@print')->name('prop.user.print');

    /////* Foreman/Mandor */////
    Route::get('/prop/mandor', 'Prop\RefMandorController@index')->name('prop.mandor');
    Route::get('/prop/mandor/list_data', 'Prop\RefMandorController@listData')->name('prop.mandor.list_data');
    Route::get('/prop/mandor/form_store', 'Prop\RefMandorController@formStore')->name('prop.mandor.form_store');
    Route::post('/prop/mandor/store', 'Prop\RefMandorController@store')->name('prop.mandor.store');
    Route::get('/prop/mandor/form_update/{id}', 'Prop\RefMandorController@formUpdate')->name('prop.mandor.form_update');
    Route::post('/prop/mandor/update/{id}', 'Prop\RefMandorController@update')->name('prop.mandor.update');

    /////* Location */////
    Route::get('/prop/location', 'Prop\RefLocationController@index')->name('prop.location');
    Route::get('/prop/location/list_data', 'Prop\RefLocationController@listData')->name('prop.location.list_data');
    Route::get('/prop/location/form_store', 'Prop\RefLocationController@formStore')->name('prop.location.form_store');
    Route::post('/prop/location/store', 'Prop\RefLocationController@store')->name('prop.location.store');
    Route::get('/prop/location/form_update/{id}', 'Prop\RefLocationController@formUpdate')->name('prop.location.form_update');
    Route::post('/prop/location/update/{id}', 'Prop\RefLocationController@update')->name('prop.location.update');

    /////* Block */////
    Route::get('/prop/block', 'Prop\RefBlockController@index')->name('prop.block');
    Route::get('/prop/block/list_data', 'Prop\RefBlockController@listData')->name('prop.block.list_data');
    Route::get('/prop/block/form_store', 'Prop\RefBlockController@formStore')->name('prop.block.form_store');
    Route::post('/prop/block/store', 'Prop\RefBlockController@store')->name('prop.block.store');
    Route::get('/prop/block/form_update/{id}', 'Prop\RefBlockController@formUpdate')->name('prop.block.form_update');
    Route::post('/prop/block/update/{id}', 'Prop\RefBlockController@update')->name('prop.block.update');

    /////* Unit Type */////
    Route::get('/prop/unit_type', 'Prop\RefUnitTypeController@index')->name('prop.unit_type');
    Route::get('/prop/unit_type/list_data', 'Prop\RefUnitTypeController@listData')->name('prop.unit_type.list_data');
    Route::get('/prop/unit_type/form_store', 'Prop\RefUnitTypeController@formStore')->name('prop.unit_type.form_store');
    Route::post('/prop/unit_type/store', 'Prop\RefUnitTypeController@store')->name('prop.unit_type.store');
    Route::get('/prop/unit_type/form_update/{id}', 'Prop\RefUnitTypeController@formUpdate')->name('prop.unit_type.form_update');
    Route::post('/prop/unit_type/update/{id}', 'Prop\RefUnitTypeController@update')->name('prop.unit_type.update');

    /////* Subsidi Type */////
    Route::get('/prop/subsidi_type', 'Prop\RefSubsidiTypeController@index')->name('prop.subsidi_type');
    Route::get('/prop/subsidi_type/list_data', 'Prop\RefSubsidiTypeController@listData')->name('prop.subsidi_type.list_data');
    Route::get('/prop/subsidi_type/form_store', 'Prop\RefSubsidiTypeController@formStore')->name('prop.subsidi_type.form_store');
    Route::post('/prop/subsidi_type/store', 'Prop\RefSubsidiTypeController@store')->name('prop.subsidi_type.store');
    Route::get('/prop/subsidi_type/form_update/{id}', 'Prop\RefSubsidiTypeController@formUpdate')->name('prop.subsidi_type.form_update');
    Route::post('/prop/subsidi_type/update/{id}', 'Prop\RefSubsidiTypeController@update')->name('prop.subsidi_type.update');

    /////* Price Item */////
    Route::get('/prop/price_item', 'Prop\RefPriceItemController@index')->name('prop.price_item');
    Route::get('/prop/price_item/list_data', 'Prop\RefPriceItemController@listData')->name('prop.price_item.list_data');
    Route::get('/prop/price_item/form_store', 'Prop\RefPriceItemController@formStore')->name('prop.price_item.form_store');
    Route::post('/prop/price_item/store', 'Prop\RefPriceItemController@store')->name('prop.price_item.store');
    Route::get('/prop/price_item/form_update/{id}', 'Prop\RefPriceItemController@formUpdate')->name('prop.price_item.form_update');
    Route::post('/prop/price_item/update/{id}', 'Prop\RefPriceItemController@update')->name('prop.price_item.update');

    /////* Sales Step */////
    Route::get('/prop/sales_step', 'Prop\RefSalesStepController@index')->name('prop.sales_step');
    Route::get('/prop/sales_step/list_data', 'Prop\RefSalesStepController@listData')->name('prop.sales_step.list_data');
    Route::get('/prop/sales_step/form_store', 'Prop\RefSalesStepController@formStore')->name('prop.sales_step.form_store');
    Route::post('/prop/sales_step/store', 'Prop\RefSalesStepController@store')->name('prop.sales_step.store');
    Route::get('/prop/sales_step/form_update/{id}', 'Prop\RefSalesStepController@formUpdate')->name('prop.sales_step.form_update');
    Route::post('/prop/sales_step/update/{id}', 'Prop\RefSalesStepController@update')->name('prop.sales_step.update');

    /////* Certificate Step */////
    Route::get('/prop/certificate_step', 'Prop\RefCertificateStepController@index')->name('prop.certificate_step');
    Route::get('/prop/certificate_step/list_data', 'Prop\RefCertificateStepController@listData')->name('prop.certificate_step.list_data');
    Route::get('/prop/certificate_step/form_store', 'Prop\RefCertificateStepController@formStore')->name('prop.certificate_step.form_store');
    Route::post('/prop/certificate_step/store', 'Prop\RefCertificateStepController@store')->name('prop.certificate_step.store');
    Route::get('/prop/certificate_step/form_update/{id}', 'Prop\RefCertificateStepController@formUpdate')->name('prop.certificate_step.form_update');
    Route::post('/prop/certificate_step/update/{id}', 'Prop\RefCertificateStepController@update')->name('prop.certificate_step.update');

    /////* Bank */////
    Route::get('/prop/bank', 'Prop\RefBankController@index')->name('prop.bank');
    Route::get('/prop/bank/list_data', 'Prop\RefBankController@listData')->name('prop.bank.list_data');
    Route::get('/prop/bank/form_store', 'Prop\RefBankController@formStore')->name('prop.bank.form_store');
    Route::post('/prop/bank/store', 'Prop\RefBankController@store')->name('prop.bank.store');
    Route::get('/prop/bank/form_update/{id}', 'Prop\RefBankController@formUpdate')->name('prop.bank.form_update');
    Route::post('/prop/bank/update/{id}', 'Prop\RefBankController@update')->name('prop.bank.update');

    /////* Unit */////
    Route::get('/prop/unit', 'Prop\RefUnitController@index')->name('prop.unit');
    Route::get('/prop/unit/list_data_json', 'Prop\RefUnitController@listDataJson')->name('prop.unit.list_data_json');
    Route::get('/prop/unit/list_data_ready_for_sale', 'Prop\RefUnitController@listDataReadyForSale')->name('prop.unit.list_data_ready_for_sale');
    Route::get('/prop/unit/detail/{id}', 'Prop\RefUnitController@detail')->name('prop.unit.detail');
    Route::get('/prop/unit/form_worklist/{id}', 'Prop\RefUnitController@formWorklist')->name('prop.unit.form_worklist');
    Route::post('/prop/unit/store_worklist/{id}', 'Prop\RefUnitController@storeWorklist')->name('prop.unit.store_worklist');
    Route::get('/prop/unit/form_store', 'Prop\RefUnitController@formStore')->name('prop.unit.form_store');
    Route::get('/prop/unit/show_price_by_unit_type', 'Prop\RefUnitController@showpriceByUnitType')->name('prop.unit.show_price_by_unit_type');
    Route::post('/prop/unit/store', 'Prop\RefUnitController@store')->name('prop.unit.store');
    Route::get('/prop/unit/form_update/{id}', 'Prop\RefUnitController@formUpdate')->name('prop.unit.form_update');
    Route::post('/prop/unit/update/{id}', 'Prop\RefUnitController@update')->name('prop.unit.update');
    Route::get('/prop/unit/worklist_form/{id}', 'Prop\RefUnitController@worklistForm')->name('prop.unit.worklist_form');
    Route::post('/prop/unit/worklist_store/{id}', 'Prop\RefUnitController@worklistStore')->name('prop.unit.worklist_store');
    Route::post('/prop/unit/worklist_update', 'Prop\RefUnitController@worklistUpdate')->name('prop.unit.worklist_update');
    Route::get('/prop/unit/worklist_delete', 'Prop\RefUnitController@worklistDelete')->name('prop.unit.worklist_update');
    Route::get('/prop/unit/worklist_salary_mandor_store', 'Prop\RefUnitController@worklistSalaryMandorStore')->name('prop.unit.worklist_salary_mandor_store');
    Route::get('/prop/unit/worklist_salary_mandor_cancel', 'Prop\RefUnitController@worklistSalaryMandorCancel')->name('prop.unit.worklist_salary_mandor_cancel');
    Route::post('/prop/unit/delete', 'Prop\RefUnitController@delete')->name('prop.unit.delete');

    /////* Worklist Master */////
    Route::get('/prop/worklist_master', 'Prop\RefWorklistMasterController@index')->name('prop.worklist_master');
    Route::get('/prop/worklist_master/list_data', 'Prop\RefWorklistMasterController@listData')->name('prop.worklist_master.list_data');
    Route::get('/prop/worklist_master/form_store', 'Prop\RefWorklistMasterController@formStore')->name('prop.worklist_master.form_store');
    Route::post('/prop/worklist_master/store', 'Prop\RefWorklistMasterController@store')->name('prop.worklist_master.store');
    Route::get('/prop/worklist_master/form_update/{id}', 'Prop\RefWorklistMasterController@formUpdate')->name('prop.worklist_master.form_update');
    Route::post('/prop/worklist_master/update/{id}', 'Prop\RefWorklistMasterController@update')->name('prop.worklist_master.update');
    Route::get('/prop/worklist_master/get_salary', 'Prop\RefWorklistMasterController@getSalary')->name('prop.worklist_master.get_salary');

    /////* Work List */////
    Route::get('/prop/work_list', 'Prop\RefWorklistController@index')->name('prop.work_list');
    Route::get('/prop/work_list/list_data', 'Prop\RefWorklistController@listData')->name('prop.work_list.list_data');
    Route::get('/prop/work_list/list_data_by_unit', 'Prop\RefWorklistController@listDataByUnit')->name('prop.work_list.list_data_by_unit');
    Route::get('/prop/work_list/form_store', 'Prop\RefWorklistController@formStore')->name('prop.work_list.form_store');
    Route::post('/prop/work_list/store', 'Prop\RefWorklistController@store')->name('prop.work_list.store');
    Route::get('/prop/work_list/form_update/{id}', 'Prop\RefWorklistController@formUpdate')->name('prop.work_list.form_update');
    Route::post('/prop/work_list/update/{id}', 'Prop\RefWorklistController@update')->name('prop.work_list.update');
    Route::post('/prop/work_list/detail', 'Prop\RefWorklistController@detail')->name('prop.work_list.detail');
    Route::post('/prop/work_list/save_salary_mandor', 'Prop\RefWorklistController@saveSalaryMandor')->name('prop.work_list.save_salary_mandor');
    Route::post('/prop/work_list/cancel_salary_mandor', 'Prop\RefWorklistController@cancelSalaryMandor')->name('prop.work_list.cancel_salary_mandor');

    /////* Purchase */////
    Route::get('/prop/purchase', 'Prop\TrnsctPurchaseController@index')->name('prop.purchase');
    Route::get('/prop/purchase/list_data', 'Prop\TrnsctPurchaseController@listData')->name('prop.purchase.list_data');
    Route::get('/prop/purchase/form_store', 'Prop\TrnsctPurchaseController@formStore')->name('prop.purchase.form_store');
    Route::get('/prop/purchase/form_store_item/{id}/{store_id}', 'Prop\TrnsctPurchaseController@formStoreItem')->name('prop.purchase.form_store_item');
    Route::post('/prop/purchase/store', 'Prop\TrnsctPurchaseController@store')->name('prop.purchase.store');
    Route::post('/prop/purchase/store_item', 'Prop\TrnsctPurchaseController@storeItem')->name('prop.purchase.store_item');
    Route::get('/prop/purchase/view_list_item/{id}', 'Prop\TrnsctPurchaseController@viewListItem')->name('prop.purchase.view_list_item');
    Route::get('/prop/purchase/form_update/{id}', 'Prop\TrnsctPurchaseController@formUpdate')->name('prop.purchase.form_update');
    Route::get('/prop/purchase/detail/{id}', 'Prop\TrnsctPurchaseController@detail')->name('prop.purchase.detail');
    Route::get('/prop/purchase/respon/{id}', 'Prop\TrnsctPurchaseController@respon')->name('prop.purchase.respon');
    Route::post('/prop/purchase/respon_store/{id}', 'Prop\TrnsctPurchaseController@responStore')->name('prop.purchase.respon_store');
    Route::get('/prop/purchase/view_list_item_respon_page/{id}', 'Prop\TrnsctPurchaseController@viewListItemResponPage')->name('prop.purchase.view_list_item_respon_page');
    Route::post('/prop/purchase/change_status/{id}', 'Prop\TrnsctPurchaseController@changeStatus')->name('prop.purchase.change_status');
    Route::post('/prop/purchase/update/{id}', 'Prop\TrnsctPurchaseController@update')->name('prop.purchase.update');
    Route::get('/prop/purchase/update/{id}', 'Prop\TrnsctPurchaseController@update')->name('prop.purchase.update');
    Route::post('/prop/purchase/delete_item', 'Prop\TrnsctPurchaseController@deleteItem')->name('prop.purchase.delete_item');
    Route::get('/prop/purchase/print/{id}', 'Prop\TrnsctPurchaseController@print')->name('prop.purchase.print');
    Route::post('/prop/purchase/store_cost', 'Prop\TrnsctPurchaseController@storeCost')->name('prop.purchase.store_cost');

    /////* Goods */////
    Route::get('/prop/goods', 'Prop\RefGoodsController@index')->name('prop.goods');
    Route::get('/prop/goods/list_data', 'Prop\RefGoodsController@listData')->name('prop.goods.list_data');
    Route::get('/prop/goods/form_store', 'Prop\RefGoodsController@formStore')->name('prop.goods.form_store');
    Route::post('/prop/goods/store', 'Prop\RefGoodsController@store')->name('prop.goods.store');
    Route::get('/prop/goods/form_update/{id}', 'Prop\RefGoodsController@formUpdate')->name('prop.goods.form_update');
    Route::post('/prop/goods/update/{id}', 'Prop\RefGoodsController@update')->name('prop.goods.update');

    /////* Goods Flow */////
    Route::get('/prop/trnsct_goods', 'Prop\TrnsctGoodsController@index')->name('prop.trnsct_goods');
    Route::get('/prop/trnsct_goods/list_data', 'Prop\TrnsctGoodsController@listData')->name('prop.trnsct_goods.list_data');
    Route::get('/prop/trnsct_goods/form_store', 'Prop\TrnsctGoodsController@formStore')->name('prop.trnsct_goods.form_store');
    Route::get('/prop/trnsct_goods/form_store_item', 'Prop\TrnsctGoodsController@formStoreItem')->name('prop.trnsct_goods.form_store_item');
    Route::post('/prop/trnsct_goods/store', 'Prop\TrnsctGoodsController@store')->name('prop.trnsct_goods.store');
    Route::post('/prop/trnsct_goods/store_item', 'Prop\TrnsctGoodsController@storeItem')->name('prop.trnsct_goods.store_item');
    Route::get('/prop/trnsct_goods/view_list_item/{id}', 'Prop\TrnsctGoodsController@viewListItem')->name('prop.trnsct_goods.view_list_item');
    Route::get('/prop/trnsct_goods/form_update', 'Prop\TrnsctGoodsController@formUpdate')->name('prop.trnsct_goods.form_update');
    Route::get('/prop/trnsct_goods/detail', 'Prop\TrnsctGoodsController@detail')->name('prop.trnsct_goods.detail');
    Route::post('/prop/trnsct_goods/update/{id}', 'Prop\TrnsctGoodsController@update')->name('prop.trnsct_goods.update');
    Route::get('/prop/trnsct_goods/update/{id}', 'Prop\TrnsctGoodsController@update')->name('prop.trnsct_goods.update');
    Route::post('/prop/trnsct_goods/delete_item', 'Prop\TrnsctGoodsController@deleteItem')->name('prop.trnsct_goods.delete_item');

    /////* Sales */////
    Route::get('/prop/sales', 'Prop\TrnsctSalesController@index')->name('prop.sales');
    Route::match(['get', 'post'], '/prop/sales2', 'Prop\TrnsctSalesController@index2')->name('prop.sales2');
    Route::get('/prop/sales/list_data', 'Prop\TrnsctSalesController@listData')->name('prop.sales.list_data');
    Route::get('/prop/sales/list_unit', 'Prop\TrnsctSalesController@listUnit')->name('prop.sales.list_unit');
    Route::get('/prop/sales/list_unit_second/{location_id}/{block_id}', 'Prop\TrnsctSalesController@listUnitSecond')->name('prop.sales.list_unit_second');
    Route::post('/prop/sales/show_units', 'Prop\TrnsctSalesController@showUnits')->name('prop.sales.show_units');
    Route::post('/prop/sales/form_store', 'Prop\TrnsctSalesController@formStore')->name('prop.sales.form_store');
    Route::post('/prop/sales/store', 'Prop\TrnsctSalesController@store')->name('prop.sales.store');
    Route::get('/prop/sales/form_pay/{id}', 'Prop\TrnsctSalesController@formPay')->name('prop.sales.form_pay');
    Route::post('/prop/sales/pay', 'Prop\TrnsctSalesController@pay')->name('prop.sales.pay');
    Route::get('/prop/sales/print/{id}', 'Prop\TrnsctSalesController@print')->name('prop.sales.print');
    Route::get('/prop/sales/show_affiliate_fee/{id}', 'Prop\TrnsctSalesController@showAffiliateFee')->name('prop.sales.show_affiliate_fee');
    Route::get('/prop/sales/detail/{id}', 'Prop\TrnsctSalesController@detail')->name('prop.sales.detail');
    Route::get('/prop/sales/print_invoice/{id}', 'Prop\TrnsctSalesController@printInvoice')->name('prop.sales.print_invoice');
    Route::get('/prop/sales/print_sppr/{id}', 'Prop\TrnsctSalesController@printSppr')->name('prop.sales.print_sppr');
    Route::post('/prop/sales/delete_sales', 'Prop\TrnsctSalesController@deleteSales')->name('prop.sales.delete_sales');
    Route::post('/prop/sales/switch_unit_list_unit', 'Prop\TrnsctSalesController@switchUnitListUnit')->name('prop.sales.switch_unit_list_unit');
    Route::post('/prop/sales/switch_unit', 'Prop\TrnsctSalesController@switchUnit')->name('prop.sales.switch_unit');
    Route::post('/prop/sales/update_marketer', 'Prop\TrnsctSalesController@updateMarketer')->name('prop.sales.update_marketer');
    Route::post('/prop/sales/update_additional_cost', 'Prop\TrnsctSalesController@updateAdditionalCost')->name('prop.sales.update_additional_cost');
    Route::post('/prop/sales/change_price_for_pajak', 'Prop\TrnsctSalesController@changePriceForPajak')->name('prop.sales.change_price_for_pajak');
    Route::get('/prop/sales/print_marketing_fee_payment', 'Prop\TrnsctSalesController@printMarketingFeePayment')->name('prop.sales.print_marketing_fee_payment');
    Route::get('/prop/sales/print_bill/{id}', 'Prop\TrnsctSalesController@printBill')->name('prop.sales.print_bill');
    Route::post('/prop/sales/upload-document-ktp-kk', 'Prop\TrnsctSalesController@uploadDocumentKtpKk')->name('prop.sales.upload-document-ktp-kk');
    Route::post('/prop/sales/get-marketing-fee', 'Prop\TrnsctSalesController@getMarketingFee')->name('prop.sales.get-marketing-fee');
    Route::post('/prop/sales/update-affiliate-fee', 'Prop\TrnsctSalesController@updateAffiliateFee')->name('prop.sales.update-affiliate-fee');
    


    /////* Sales Detail */////

    // sales step
    Route::post('/prop/sales/detail_sales_step', 'Prop\TrnsctSalesController@detailSalesStep')->name('prop.sales.detail_sales_step');
    Route::post('/prop/sales/detail_sales_step_store', 'Prop\TrnsctSalesController@detailSalesStepStore')->name('prop.sales.detail_sales_step_store');
    Route::post('/prop/sales/detail_sales_step_delete', 'Prop\TrnsctSalesController@detailSalesStepDelete')->name('prop.sales.detail_sales_step_delete');
    Route::post('/prop/sales/detail_sales_step_update', 'Prop\TrnsctSalesController@detailSalesStepUpdate')->name('prop.sales.detail_sales_step_update');
    // certificate step
    Route::post('/prop/sales/detail_certificate_step', 'Prop\TrnsctSalesController@detailCertificateStep')->name('prop.sales.detail_certificate_step');
    Route::post('/prop/sales/detail_certificate_step_store', 'Prop\TrnsctSalesController@detailCertificateStepStore')->name('prop.sales.detail_certificate_step_store');
    Route::post('/prop/sales/detail_certificate_step_delete', 'Prop\TrnsctSalesController@detailCertificateStepDelete')->name('prop.sales.detail_certificate_step_delete');
    Route::post('/prop/sales/detail_certificate_step_update', 'Prop\TrnsctSalesController@detailCertificateStepUpdate')->name('prop.sales.detail_certificate_step_update');
    // certificate position
    Route::post('/prop/sales/detail_certificate_position', 'Prop\TrnsctSalesController@detailCertificatePosition')->name('prop.sales.detail_certificate_position');
    Route::post('/prop/sales/detail_certificate_position_store', 'Prop\TrnsctSalesController@detailCertificatePositionStore')->name('prop.sales.detail_certificate_position_store');
    Route::post('/prop/sales/detail_certificate_position_delete', 'Prop\TrnsctSalesController@detailCertificatePositionDelete')->name('prop.sales.detail_certificate_position_delete');
    Route::post('/prop/sales/detail_certificate_position_update', 'Prop\TrnsctSalesController@detailCertificatePositionUpdate')->name('prop.sales.detail_certificate_position_update');
    
    // cicilan
    Route::post('/prop/sales/detail_installment', 'Prop\TrnsctSalesController@detailInstallment')->name('prop.sales.detail_installment');
    Route::post('/prop/sales/detail_installment_potongan', 'Prop\TrnsctSalesController@detailInstallmentPotongan')->name('prop.sales.detail_installment_potongan');
    Route::post('/prop/sales/detail_installment_store', 'Prop\TrnsctSalesController@detailInstallmentStore')->name('prop.sales.detail_installment_store');
    Route::post('/prop/sales/detail_installment_delete', 'Prop\TrnsctSalesController@detailInstallmentDelete')->name('prop.sales.detail_installment_delete');
    Route::post('/prop/sales/detail_installment_update', 'Prop\TrnsctSalesController@detailInstallmentUpdate')->name('prop.sales.detail_installment_update');
    Route::post('/prop/sales/detail_installment_biaya_tambahan', 'Prop\TrnsctSalesController@detailInstallmentBiayaTambahan')->name('prop.sales.detail_installment_biaya_tambahan');
    Route::post('/prop/sales/detail_installment_bill', 'Prop\TrnsctSalesController@detailInstallmentBill')->name('prop.sales.detail_installment_bill');

    // pencairan marketing fee
    Route::post('/prop/sales/detail_marketing_fee', 'Prop\TrnsctSalesController@detailMarketingFee')->name('prop.sales.detail_marketing_fee');
    Route::post('/prop/sales/detail_marketing_fee_store', 'Prop\TrnsctSalesController@detailMarketingFeeStore')->name('prop.sales.detail_marketing_fee_store');
    Route::post('/prop/sales/detail_marketing_fee_delete', 'Prop\TrnsctSalesController@detailMarketingFeeDelete')->name('prop.sales.detail_marketing_fee_delete');
    Route::post('/prop/sales/detail_marketing_fee_update', 'Prop\TrnsctSalesController@detailMarketingFeeUpdate')->name('prop.sales.detail_marketing_fee_update');
  
    // info kpr
    Route::post('/prop/sales/detail_kpr', 'Prop\TrnsctSalesController@detailKpr')->name('prop.sales.detail_kpr');
    Route::post('/prop/sales/detail_kpr_change_bank', 'Prop\TrnsctSalesController@detailKprChangeBank')->name('prop.sales.detail_kpr_change_bank');
    Route::post('/prop/sales/detail_kpr_store', 'Prop\TrnsctSalesController@detailKprStore')->name('prop.sales.detail_kpr_store');
    Route::post('/prop/sales/detail_kpr_credit_store', 'Prop\TrnsctSalesController@detailKprCreditStore')->name('prop.sales.detail_kpr_credit_store');
    Route::post('/prop/sales/detail_kpr_credit_store_cancel', 'Prop\TrnsctSalesController@detailKprCreditStoreCancel')->name('prop.sales.detail_kpr_credit_store_cancel');
    Route::post('/prop/sales/detail_kpr_return', 'Prop\TrnsctSalesController@detailKprReturn')->name('prop.sales.detail_kpr_return');
    Route::post('/prop/sales/detail_kpr_return_cancel', 'Prop\TrnsctSalesController@detailKprReturnCancel')->name('prop.sales.detail_kpr_return_cancel');
    Route::post('/prop/sales/print_key_handover', 'Prop\TrnsctSalesController@printKeyHandover')->name('prop.sales.print_key_handover');
    Route::post('/prop/sales/print_complain', 'Prop\TrnsctSalesController@printComplain')->name('prop.sales.print_complain');
    Route::post('/prop/sales/print_missing_requirement_document', 'Prop\TrnsctSalesController@printMissingRequirementDocument')->name('prop.sales.print_missing_requirement_document');
    Route::post('/prop/sales/print_warning_letter_2', 'Prop\TrnsctSalesController@printWarningLetter2')->name('prop.sales.print_warning_letter_2');
    Route::get('/prop/sales/print_cancellation_letter/{id}', 'Prop\TrnsctSalesController@printCancellationLetter')->name('prop.sales.print_cancellation_letter');

    // booking online
    Route::get('/prop/booking_online', 'Prop\TrnsctBookingOnlineController@index')->name('prop.booking_online');
    Route::get('/prop/booking_online/list_data', 'Prop\TrnsctBookingOnlineController@listData')->name('prop.booking_online.list_data');
    Route::get('/prop/booking_online/detail/{id}', 'Prop\TrnsctBookingOnlineController@detail')->name('prop.booking_online.detail');
    Route::post('/prop/booking_online/konfirmasi', 'Prop\TrnsctBookingOnlineController@konfirmasi')->name('prop.booking_online.konfirmasi');

    // Print kpr requirements
    Route::get('/prop/print_kpr_requirements/{id}', 'Prop\PrintKprRequirementsController@index')->name('prop.print_kpr_requirements');
    Route::get('/prop/print_kpr_requirements/{id}/print/{lampiran}/{sales_id?}', 'Prop\PrintKprRequirementsController@print')->name('prop.print_kpr_requirements.print');

    // sales export
    Route::get('prop/sales-export', 'Prop\TrnsctSalesExportController@index')->name('prop.sales-export');
    Route::get('prop/sales-export/export', 'Prop\TrnsctSalesExportController@export')->name('prop.sales-export.export');
    Route::get('prop/sales-export/list-data', 'Prop\TrnsctSalesExportController@listData')->name('prop.sales-export.list-data');
    Route::get('prop/sales-export/count', 'Prop\TrnsctSalesExportController@count')->name('prop.sales-export.count');
    Route::post('prop/sales-export/create', 'Prop\TrnsctSalesExportController@create')->name('prop.sales-export.create');
    Route::post('prop/sales-export/delete', 'Prop\TrnsctSalesExportController@delete')->name('prop.sales-export.delete');
    Route::get('prop/sales-export/delete-all', 'Prop\TrnsctSalesExportController@deleteAll')->name('prop.sales-export.delete-all');
});
