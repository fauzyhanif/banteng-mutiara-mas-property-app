<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Travel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {
    /////* Financial account group  */////
    Route::get('/finance/account_group', 'Finance\RefAccountGroupController@index')->name('finance.account_group');
    Route::get('/finance/account_group/list_data', 'Finance\RefAccountGroupController@listData')->name('finance.account_group.list_data');
    Route::get('/finance/account_group/detail/{id}', 'Finance\RefAccountGroupController@detail')->name('finance.account_group.detail');
    Route::get('/finance/account_group/form_store', 'Finance\RefAccountGroupController@formStore')->name('finance.account_group.form_store');
    Route::post('/finance/account_group/store', 'Finance\RefAccountGroupController@store')->name('finance.account_group.store');
    Route::get('/finance/account_group/form_update/{id}', 'Finance\RefAccountGroupController@formUpdate')->name('finance.account_group.form_update');
    Route::post('/finance/account_group/update/{id}', 'Finance\RefAccountGroupController@update')->name('finance.account_group.update');

    /////* Financial account  */////
    Route::get('/finance/account', 'Finance\RefAccountController@index')->name('finance.account');
    Route::get('/finance/account/get_parent/{id}', 'Finance\RefAccountController@getParent')->name('finance.account.get_parent');
    Route::get('/finance/account/list_data', 'Finance\RefAccountController@listData')->name('finance.account.list_data');
    Route::get('/finance/account/detail/{id}', 'Finance\RefAccountController@detail')->name('finance.account.detail');
    Route::get('/finance/account/form_store', 'Finance\RefAccountController@formStore')->name('finance.account.form_store');
    Route::post('/finance/account/store', 'Finance\RefAccountController@store')->name('finance.account.store');
    Route::get('/finance/account/form_update/{id}', 'Finance\RefAccountController@formUpdate')->name('finance.account.form_update');
    Route::post('/finance/account/update/{id}', 'Finance\RefAccountController@update')->name('finance.account.update');
    Route::match(['get', 'post'], '/finance/account/transaction/{id}', 'Finance\RefAccountController@transaction')->name('finance.account.transaction');
    Route::match(['get', 'post'], '/finance/account/transaction_list_data', 'Finance\RefAccountController@transactionListData')->name('finance.account.transaction_list_data');
    Route::post('/finance/account/transaction_print', 'Finance\RefAccountController@transactionPrint')->name('finance.account.transaction_print');

    /////* Financial cash bank  */////
    Route::get('/finance/cash_bank', 'Finance\RefCashBankController@index')->name('finance.cash_bank');
    Route::get('/finance/cash_bank/get_parent/{id}', 'Finance\RefCashBankController@getParent')->name('finance.cash_bank.get_parent');
    Route::get('/finance/cash_bank/list_data', 'Finance\RefCashBankController@listData')->name('finance.cash_bank.list_data');
    Route::get('/finance/cash_bank/detail/{id}', 'Finance\RefCashBankController@detail')->name('finance.cash_bank.detail');
    Route::get('/finance/cash_bank/form_store', 'Finance\RefCashBankController@formStore')->name('finance.cash_bank.form_store');
    Route::post('/finance/cash_bank/store', 'Finance\RefCashBankController@store')->name('finance.cash_bank.store');
    Route::get('/finance/cash_bank/form_update/{id}', 'Finance\RefCashBankController@formUpdate')->name('finance.cash_bank.form_update');
    Route::post('/finance/cash_bank/update/{id}', 'Finance\RefCashBankController@update')->name('finance.cash_bank.update');
    Route::match(['get', 'post'], '/finance/cash_bank/cashflow/{id}', 'Finance\RefCashBankController@cashflow')->name('finance.cash_bank.cashflow');
    Route::get('/finance/cash_bank/cashflow_total', 'Finance\RefCashBankController@cashflowTotal')->name('finance.cash_bank.cashflow_total');
    Route::get('/finance/cash_bank/cashflow_kasbon', 'Finance\RefCashBankController@cashflowKasbon')->name('finance.cash_bank.cashflow_kasbon');
    Route::get('/finance/cash_bank/cashflow_kasbon_total', 'Finance\RefCashBankController@cashflowKasbonTotal')->name('finance.cash_bank.cashflow_kasbon_total');
    Route::get('/finance/cash_bank/cashflow_penjualan', 'Finance\RefCashBankController@cashflowPenjualan')->name('finance.cash_bank.cashflow_penjualan');
    Route::get('/finance/cash_bank/cashflow_penjualan_total', 'Finance\RefCashBankController@cashflowPenjualanTotal')->name('finance.cash_bank.cashflow_penjualan_total');
    Route::get('/finance/cash_bank/cashflow_operasional', 'Finance\RefCashBankController@cashflowOperasional')->name('finance.cash_bank.cashflow_operasional');
    Route::get('/finance/cash_bank/cashflow_operasional_total', 'Finance\RefCashBankController@cashflowOperasionalTotal')->name('finance.cash_bank.cashflow_operasional_total');

    /////* Cashflow general  */////
    Route::match(['get', 'post'], '/finance/cashflow', 'Finance\ReportCashflowController@index')->name('finance.cashflow');
    Route::get('/finance/cashflow_total', 'Finance\ReportCashflowController@cashflowTotal')->name('finance.cashflow_total');
    Route::get('/finance/cashflow_kasbon', 'Finance\ReportCashflowController@cashflowKasbon')->name('finance.cashflow_kasbon');
    Route::get('/finance/cashflow_kasbon_total', 'Finance\ReportCashflowController@cashflowKasbonTotal')->name('finance.cashflow_kasbon_total');
    Route::get('/finance/cashflow_penjualan', 'Finance\ReportCashflowController@cashflowPenjualan')->name('finance.cashflow_penjualan');
    Route::get('/finance/cashflow_penjualan_total', 'Finance\ReportCashflowController@cashflowPenjualanTotal')->name('finance.cashflow_penjualan_total');
    Route::get('/finance/cashflow_operasional', 'Finance\ReportCashflowController@cashflowOperasional')->name('finance.cashflow_operasional');
    Route::get('/finance/cashflow_operasional_total', 'Finance\ReportCashflowController@cashflowOperasionalTotal')->name('finance.cashflow_operasional_total');

    /////* Piutang / Kasbon  */////
    Route::get('/finance/piutang', 'Finance\TrnsctPiutangController@index')->name('finance.piutang');
    Route::get('/finance/piutang/get_parent/{id}', 'Finance\TrnsctPiutangController@getParent')->name('finance.piutang.get_parent');
    Route::get('/finance/piutang/list_data', 'Finance\TrnsctPiutangController@listData')->name('finance.piutang.list_data');
    Route::get('/finance/piutang/detail/{id}', 'Finance\TrnsctPiutangController@detail')->name('finance.piutang.detail');
    Route::match(['get', 'post'],'/finance/piutang/form_store', 'Finance\TrnsctPiutangController@formStore')->name('finance.piutang.form_store');
    Route::post('/finance/piutang/store', 'Finance\TrnsctPiutangController@store')->name('finance.piutang.store');
    Route::get('/finance/piutang/form_update/{id}', 'Finance\TrnsctPiutangController@formUpdate')->name('finance.piutang.form_update');
    Route::post('/finance/piutang/update/{id}', 'Finance\TrnsctPiutangController@update')->name('finance.piutang.update');
    Route::match(['get', 'post'],'/finance/piutang/form_pay', 'Finance\TrnsctPiutangController@formPay')->name('finance.piutang.form_pay');
    Route::post('/finance/piutang/pay', 'Finance\TrnsctPiutangController@pay')->name('finance.piutang.pay');
    Route::get('/finance/piutang/print_invoice/{id}', 'Finance\TrnsctPiutangController@printInvoice')->name('finance.piutang.print_invoice');
    Route::get('/finance/piutang/print_kasbon/{id}', 'Finance\TrnsctPiutangController@printKasbon')->name('finance.piutang.print_kasbon');
    Route::get('/finance/piutang/cancel/{id}', 'Finance\TrnsctPiutangController@cancel')->name('finance.piutang.cancel');

    /////* Hutang  */////
    Route::get('/finance/hutang', 'Finance\TrnsctHutangController@index')->name('finance.hutang');
    Route::get('/finance/hutang/get_parent/{id}', 'Finance\TrnsctHutangController@getParent')->name('finance.hutang.get_parent');
    Route::get('/finance/hutang/list_data', 'Finance\TrnsctHutangController@listData')->name('finance.hutang.list_data');
    Route::get('/finance/hutang/detail/{id}', 'Finance\TrnsctHutangController@detail')->name('finance.hutang.detail');
    Route::match(['get', 'post'],'/finance/hutang/form_store', 'Finance\TrnsctHutangController@formStore')->name('finance.hutang.form_store');
    Route::post('/finance/hutang/store', 'Finance\TrnsctHutangController@store')->name('finance.hutang.store');
    Route::get('/finance/hutang/form_update/{id}', 'Finance\TrnsctHutangController@formUpdate')->name('finance.hutang.form_update');
    Route::post('/finance/hutang/update/{id}', 'Finance\TrnsctHutangController@update')->name('finance.hutang.update');
    Route::match(['get', 'post'],'/finance/hutang/form_pay', 'Finance\TrnsctHutangController@formPay')->name('finance.hutang.form_pay');
    Route::post('/finance/hutang/pay', 'Finance\TrnsctHutangController@pay')->name('finance.hutang.pay');
    Route::get('/finance/hutang/print_invoice/{id}', 'Finance\TrnsctHutangController@printInvoice')->name('finance.hutang.print_invoice');

    /////* Jurnal  */////
    Route::get('/finance/jurnal', 'Finance\TrnsctJurnalController@index')->name('finance.jurnal');
    Route::get('/finance/jurnal/list_data', 'Finance\TrnsctJurnalController@listData')->name('finance.jurnal.list_data');
    Route::get('/finance/jurnal/detail/{id}', 'Finance\TrnsctJurnalController@detail')->name('finance.jurnal.detail');
    Route::get('/finance/jurnal/form_store', 'Finance\TrnsctJurnalController@formStore')->name('finance.jurnal.form_store');
    Route::post('/finance/jurnal/main_store', 'Finance\TrnsctJurnalController@mainStore')->name('finance.jurnal.main_store');
    Route::post('/finance/jurnal/store', 'Finance\TrnsctJurnalController@store')->name('finance.jurnal.store');
    Route::post('/finance/jurnal/store_header', 'Finance\TrnsctJurnalController@storeHeader')->name('finance.jurnal.store_header');
    Route::post('/finance/jurnal/store_item', 'Finance\TrnsctJurnalController@storeItem')->name('finance.jurnal.store_item');
    Route::get('/finance/jurnal/update/{id}', 'Finance\TrnsctJurnalController@formUpdate')->name('finance.jurnal.form_update');
    Route::post('/finance/jurnal/update/{id}', 'Finance\TrnsctJurnalController@update')->name('finance.jurnal.update');
    Route::post('/finance/jurnal/remove_jurnal_item', 'Finance\TrnsctJurnalController@removeJurnalItem')->name('finance.jurnal.remove_jurnal_item');

    /////* Cost  */////
    Route::get('/finance/cost', 'Finance\TrnsctCostController@index')->name('finance.cost');
    Route::get('/finance/cost/list_data', 'Finance\TrnsctCostController@listData')->name('finance.cost.list_data');
    Route::get('/finance/cost/list_payments/{id}', 'Finance\TrnsctCostController@listPayments')->name('finance.cost.list_payments');
    Route::get('/finance/cost/print_payment/{id}', 'Finance\TrnsctCostController@printPayment')->name('finance.cost.print_payment');
    Route::get('/finance/cost/detail/{id}', 'Finance\TrnsctCostController@detail')->name('finance.cost.detail');
    Route::get('/finance/cost/form_store', 'Finance\TrnsctCostController@formStore')->name('finance.cost.form_store');
    Route::post('/finance/cost/store', 'Finance\TrnsctCostController@store')->name('finance.cost.store');
    Route::post('/finance/cost/pay', 'Finance\TrnsctCostController@pay')->name('finance.cost.pay');
    Route::post('/finance/cost/cancel', 'Finance\TrnsctCostController@cancel')->name('finance.cost.cancel');
    Route::post('/finance/cost/cancel_paid', 'Finance\TrnsctCostController@cancelPaid')->name('finance.cost.cancel_paid');
    Route::post('/finance/cost/update_item', 'Finance\TrnsctCostController@updateItem')->name('finance.cost.update_item');


    /////* Pengajuan Pencairan  */////
    Route::get('/finance/pengajuan_pencairan', 'Finance\TrnsctPengajuanPencairanController@index')->name('finance.pengajuan_pencairan');
    Route::get('/finance/pengajuan_pencairan/list_data', 'Finance\TrnsctPengajuanPencairanController@listData')->name('finance.pengajuan_pencairan.list_data');
    Route::get('/finance/pengajuan_pencairan/detail/{id}', 'Finance\TrnsctPengajuanPencairanController@detail')->name('finance.pengajuan_pencairan.detail');
    Route::get('/finance/pengajuan_pencairan/form_store', 'Finance\TrnsctPengajuanPencairanController@formStore')->name('finance.pengajuan_pencairan.form_store');
    Route::post('/finance/pengajuan_pencairan/list_hutang_perusahaan', 'Finance\TrnsctPengajuanPencairanController@listHutangPerusahaan')->name('finance.pengajuan_pencairan.list_hutang_perusahaan');
    Route::post('/finance/pengajuan_pencairan/list_sdm', 'Finance\TrnsctPengajuanPencairanController@listSdm')->name('finance.pengajuan_pencairan.list_sdm');
    Route::get('/finance/pengajuan_pencairan/form_store_first_step', 'Finance\TrnsctPengajuanPencairanController@formStoreFirstStep')->name('finance.pengajuan_pencairan.form_store_first_step');
    Route::get('/finance/pengajuan_pencairan/form_store_second_step', 'Finance\TrnsctPengajuanPencairanController@formStoreSecondStep')->name('finance.pengajuan_pencairan.form_store_second_step');
    Route::post('/finance/pengajuan_pencairan/store', 'Finance\TrnsctPengajuanPencairanController@store')->name('finance.pengajuan_pencairan.store');
    Route::get('/finance/pengajuan_pencairan/form_update/{id}', 'Finance\TrnsctPengajuanPencairanController@formUpdate')->name('finance.pengajuan_pencairan.form_update');
    Route::post('/finance/pengajuan_pencairan/update', 'Finance\TrnsctPengajuanPencairanController@update')->name('finance.pengajuan_pencairan.update');
    Route::post('/finance/pengajuan_pencairan/delete', 'Finance\TrnsctPengajuanPencairanController@delete')->name('finance.pengajuan_pencairan.delete');
    Route::get('/finance/pengajuan_pencairan/form_response/{id}', 'Finance\TrnsctPengajuanPencairanController@formResponse')->name('finance.pengajuan_pencairan.form_response');
    Route::post('/finance/pengajuan_pencairan/response', 'Finance\TrnsctPengajuanPencairanController@response')->name('finance.pengajuan_pencairan.response');

    /////* Financial account accounting  */////
    Route::get('/finance/account_accounting', 'Finance\RefAccountAccountingController@index')->name('finance.account_accounting');
    Route::get('/finance/account_accounting/list_data', 'Finance\RefAccountAccountingController@listData')->name('finance.account_accounting.list_data');
    Route::get('/finance/account_accounting/detail/{id}', 'Finance\RefAccountAccountingController@detail')->name('finance.account_accounting.detail');
    Route::get('/finance/account_accounting/form_store', 'Finance\RefAccountAccountingController@formStore')->name('finance.account_accounting.form_store');
    Route::post('/finance/account_accounting/store', 'Finance\RefAccountAccountingController@store')->name('finance.account_accounting.store');
    Route::get('/finance/account_accounting/form_update/{id}', 'Finance\RefAccountAccountingController@formUpdate')->name('finance.account_accounting.form_update');
    Route::post('/finance/account_accounting/update/{id}', 'Finance\RefAccountAccountingController@update')->name('finance.account_accounting.update');

    /////* Hutang bank  */////
    Route::get('/finance/hutang_bank', 'Finance\RefDebtToBankController@index')->name('finance.hutang_bank');
    Route::get('/finance/hutang_bank/list_data', 'Finance\RefDebtToBankController@listData')->name('finance.hutang_bank.list_data');
    Route::get('/finance/hutang_bank/detail/{id}', 'Finance\RefDebtToBankController@detail')->name('finance.hutang_bank.detail');
    Route::get('/finance/hutang_bank/form_store', 'Finance\RefDebtToBankController@formStore')->name('finance.hutang_bank.form_store');
    Route::post('/finance/hutang_bank/store', 'Finance\RefDebtToBankController@store')->name('finance.hutang_bank.store');
    Route::get('/finance/hutang_bank/form_update/{id}', 'Finance\RefDebtToBankController@formUpdate')->name('finance.hutang_bank.form_update');
    Route::post('/finance/hutang_bank/update/{id}', 'Finance\RefDebtToBankController@update')->name('finance.hutang_bank.update');

    /////* Asset  */////
    Route::get('/finance/asset', 'Finance\RefAssetController@index')->name('finance.asset');
    Route::get('/finance/asset/list_data', 'Finance\RefAssetController@listData')->name('finance.asset.list_data');
    Route::get('/finance/asset/detail/{id}', 'Finance\RefAssetController@detail')->name('finance.asset.detail');
    Route::get('/finance/asset/form_store', 'Finance\RefAssetController@formStore')->name('finance.asset.form_store');
    Route::post('/finance/asset/store', 'Finance\RefAssetController@store')->name('finance.asset.store');
    Route::get('/finance/asset/form_update/{id}', 'Finance\RefAssetController@formUpdate')->name('finance.asset.form_update');
    Route::post('/finance/asset/update/{id}', 'Finance\RefAssetController@update')->name('finance.asset.update');
    Route::get('/finance/asset/print', 'Finance\RefAssetController@print')->name('finance.asset.print');
    Route::get('/finance/asset/form_penyusutan', 'Finance\RefAssetController@formDepreciation')->name('finance.asset.form_penyusutan');
    Route::post('/finance/asset/penyusutan', 'Finance\RefAssetController@depreciation')->name('finance.asset.penyusutan');
    Route::get('/finance/asset/detail/{id}', 'Finance\RefAssetController@detail')->name('finance.asset.detail');
});
